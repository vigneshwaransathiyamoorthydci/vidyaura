package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.PollsResultListResponse;

import java.util.ArrayList;


/**
 * Created by keerthana on 10/1/2018.
 */

public class PollsListAdapter extends RecyclerView.Adapter<PollsListAdapter.MyViewHolder> {

    ArrayList<PollsResultListResponse> pollsResultListResponses;
    private Context context;
    private AddTouchListen addTouchListen;

    public PollsListAdapter(Context context, ArrayList<PollsResultListResponse> pollsResultListResponses) {
        this.context =context;
        this.pollsResultListResponses = pollsResultListResponses;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polls, parent, false);
        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        int pollsPercent=(int) Double.parseDouble(pollsResultListResponses.get(position).getPollsPercent());
        holder.tvPollsName.setText(pollsResultListResponses.get(position).getPollsName());
        holder.tvPollsPercent.setText(pollsResultListResponses.get(position).getPollsPercent()+"%");
        holder.sbPolls.setProgress(pollsPercent);
        holder.tvVotes.setText(pollsResultListResponses.get(position).getPollsVote()+" Votes");

        if(pollsPercent <= 25 ){
            holder.sbPolls.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
        }else if(pollsPercent <= 50){
            holder.sbPolls.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
        } else if(pollsPercent <= 75){
            holder.sbPolls.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
        } else if(pollsPercent > 75){
            holder.sbPolls.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return pollsResultListResponses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvPollsName,tvPollsPercent,tvVotes;
        SeekBar sbPolls;

        public MyViewHolder(View view) {
            super(view);
            tvPollsName = view.findViewById(R.id.tv_polls_name);
            tvPollsPercent = view.findViewById(R.id.tv_polls_percent);
            sbPolls = view.findViewById(R.id.sb_polls);
            tvVotes = view.findViewById(R.id.tv_votes);
            sbPolls.setEnabled(false);
        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}