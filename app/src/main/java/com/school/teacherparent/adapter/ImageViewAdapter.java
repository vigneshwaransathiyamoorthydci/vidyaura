package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.models.SelectedImageList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageViewAdapter extends RecyclerView.Adapter<ImageViewAdapter.MyViewHolder> {

    Context context;
    BitmapFactory.Options btmapOptions;
    List<String>  imagelist;
    String path;
    public ImageViewAdapter.AddTouchListen addTouchListen;

    public ImageViewAdapter(List<String>  imagelist, Context context,String path) {
        this.imagelist = imagelist;
        this.context = context;
        btmapOptions = new BitmapFactory.Options();
        btmapOptions.inSampleSize = 2;
        this.path=path;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_item_selected_image, parent, false);
        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(ImageViewAdapter.AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {



        Picasso.get().load(context.getString(R.string.s3_baseurl)+path+"/"+imagelist.get(position)).into(holder.mainimage);
            holder.itemView.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

        

    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mainimage, close;

        public MyViewHolder(final View view) {
            super(view);
            mainimage = view.findViewById(R.id.imagetemp);
            //close = view.findViewById(R.id.close);
            //close.bringToFront();


        }
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}