package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.StudentList;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/10/2018.
 */


public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {

    private RecyclerView parentRecycler;
    private Context context;
    private ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        parentRecycler = recyclerView;
    }

    public StudentListAdapter(Context context, ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList) {
            this.context=context;
            this.parentsStudListArrayList=parentsStudListArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        RelativeLayout linMain;
        CircularImageView imageCover;
        TextView studentNameTextview;
        TextView studentclassTextview;

        public MyViewHolder(View view) {
            super(view);
            linMain = (RelativeLayout) view.findViewById(R.id.linMain);
            imageCover = (CircularImageView) view.findViewById(R.id.imageCover);
            studentNameTextview=(TextView)view.findViewById(R.id.unameTextview);
            studentclassTextview=(TextView)view.findViewById(R.id.classnameTextview);
            itemView.findViewById(R.id.relCoverFlow).setOnClickListener(this);
        }

        public void showText() {
            int parentHeight = ((View) imageCover.getParent()).getHeight();
            float scale = (parentHeight - studentNameTextview.getHeight()) / (float) imageCover.getHeight();
            imageCover.setPivotX(imageCover.getWidth() * 0.5f);
            imageCover.setPivotY(0);
            studentNameTextview.setVisibility(View.VISIBLE);
            studentclassTextview.setVisibility(View.VISIBLE);
            imageCover.setAlpha(1f);
            /*imageCover.animate().scaleX(scale)
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            studentNameTextview.setVisibility(View.VISIBLE);
                            studentclassTextview.setVisibility(View.VISIBLE);
                            imageCover.setAlpha(1f);
                            //imageCover.setColorFilter(Color.BLACK);
                        }
                    })
                    .scaleY(scale).setDuration(200)
                    .start();*/
        }

        public void hideText() {
            //imageCover.setColorFilter(ContextCompat.getColor(imageCover.getContext(), R.color.gray));
            studentNameTextview.setVisibility(View.INVISIBLE);
            studentclassTextview.setVisibility(View.INVISIBLE);
            imageCover.setAlpha(0.3f);
            /*imageCover.animate().scaleX(1f).scaleY(1f)
                    .setDuration(200)
                    .start();*/
        }

        @Override
        public void onClick(View v) {
            parentRecycler.smoothScrollToPosition(getAdapterPosition());
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cover, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.studentNameTextview.setText(parentsStudListArrayList.get(position).getFname());
        holder.studentclassTextview.setText(parentsStudListArrayList.get(position).getClassName()+" " +parentsStudListArrayList.get(position).getSection());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path)+"/"+
                parentsStudListArrayList.get(position).getStudent_photo();
        Picasso.get().load(url).placeholder(R.drawable.ic_user).into(holder.imageCover, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.imageCover.setImageResource(R.drawable.ic_user);
            }
        });
        //holder.imageCover.setBackgroundResource(R.mipmap.student_image);

    }

    @Override
    public int getItemCount() {
        return parentsStudListArrayList.size();
    }

}
