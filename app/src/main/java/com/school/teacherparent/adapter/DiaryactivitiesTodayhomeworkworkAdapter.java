package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.ClassworkActivity;
import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.HomeWorkFragment;
import com.school.teacherparent.fragment.HomeworkDetailsFragment;
import com.school.teacherparent.fragment.ParentDiaryFragment;
import com.school.teacherparent.fragment.SubClassFragment;
import com.school.teacherparent.models.DiaryClassworkList;
import com.school.teacherparent.models.DiaryHomeworkList;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.school.teacherparent.fragment.ParentDiaryFragment.studentID;

public class DiaryactivitiesTodayhomeworkworkAdapter extends BaseAdapter
{
    private Context Context;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    ArrayList<DiaryHomeworkList> classworkListstopicListTodayDue;
    String chapter_name;
    String classname;
    int classID;
    int sectionID;
    int subjectID;
    int selectedSchoolID;
    int studID;
    String homeWorkID;
    public DiaryactivitiesTodayhomeworkworkAdapter(Context context, ArrayList<DiaryHomeworkList> classworkListstopicListTodayDue , String chapter_name,
                                                   String classname, int classID, int sectionID, int subjectID,
                                                   int selectedSchoolID, int studID,String homeWorkID) {
        this.Context = context;
        this.classworkListstopicListTodayDue=classworkListstopicListTodayDue;
        this.chapter_name=chapter_name;
        this.classname=classname;
        this.classID=classID;
        this.sectionID=sectionID;
        this.subjectID=subjectID;
        this.selectedSchoolID=selectedSchoolID;
        this.studID=studID;
        this.homeWorkID=homeWorkID;
    }

    @Override
    public int getCount() {
        return classworkListstopicListTodayDue.size();
    }

    @Override
    public Object getItem(int position) {
        return classworkListstopicListTodayDue.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    Context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.di_sub_itemtwo, null);

        }
        TextView chapterNametextview = (TextView) convertView.findViewById(R.id.chapterNametextview);
        TextView topicTextview = (TextView) convertView.findViewById(R.id.topicTextview);
        TextView duedate = (TextView) convertView.findViewById(R.id.duedate);
        ImageView homeworkImage=(ImageView)convertView.findViewById(R.id.homeworkImage);
        LinearLayout extraSpaceLineraLayout=(LinearLayout)convertView.findViewById(R.id.extraSpaceLineraLayout);
        extraSpaceLineraLayout.setVisibility(View.GONE);
        topicTextview.setText(classworkListstopicListTodayDue.get(position).getTopicDetails());
        chapterNametextview.setText(classworkListstopicListTodayDue.get(position).getChapterName());
        duedate.setText(classworkListstopicListTodayDue.get(position).getAssigned_on());

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //editor.putInt("ParentHomeWork", 1);
                //editor.putInt("ParentHomeWorkID", studentID);
                //editor.apply();

                Context.startActivity(new Intent(Context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
                        .putExtra("classname", classname)
                        .putExtra("homeWorkID", homeWorkID)
                        .putExtra("classID", classID)
                        .putExtra("sectionID", sectionID)
                        .putExtra("subjectID", subjectID)
                        .putExtra("selectedSchoolID", selectedSchoolID)
                        .putExtra("studID", studID));

                //Context.startActivity(new Intent(Context, HomeWorkActivity.class));
//                Fragment fragment = new SubClassFragment();
//                Bundle bundle=new Bundle();
//                bundle.putString("classname",classList.get(position).getClassName()+classList.get(position).getSection());
//                bundle.putString("classwrkid",String.valueOf(classList.get(position).getClassworkID()));
//                bundle.putString("classid",String.valueOf(classList.get(position).getClassID()));
//                bundle.putString("sectionid",String.valueOf(classList.get(position).getSectionID()));
//                bundle.putString("subjectid",String.valueOf(classList.get(position).getSubjectID()));
//                fragment.setArguments(bundle);
//                // bundle.putString("");
//                replaceFragment(fragment);

            }
        });


        return convertView;
    }
}
