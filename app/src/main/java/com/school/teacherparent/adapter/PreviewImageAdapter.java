package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;

import java.io.File;
import java.util.List;

public class PreviewImageAdapter extends RecyclerView.Adapter<PreviewImageAdapter.ViewHolder> {

    private Context mContext;
    private List<String> mPaths;
    private int state;

    public PreviewImageAdapter(Context c, List<String> mPathss, int statee) {
        mContext = c;
        mPaths = mPathss;
        state = statee;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_preview, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        if (state == 1) {
            final File imgFile = new File(mPaths.get(position));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                holder.imageView.setImageBitmap(myBitmap);
            }
        } else if (state == 2) {
            RequestOptions error = new RequestOptions()
                    .placeholder(R.drawable.gallery)
                    .error(R.drawable.gallery)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(mContext.getApplicationContext()).load(mPaths.get(position)).apply(error).into(holder.imageView);
        }
    }

    /*public int getCount() {
        return mPaths.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }*/

    @Override
    public int getItemCount() {
        return mPaths.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        public ViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.grid_item_image);
        }
    }

    // create a new ImageView for each item referenced by the Adapter
    /*public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if (convertView == null) {
            gridView = new View(mContext);
            gridView = inflater.inflate(R.layout.item_image_preview, null);
            ImageView imageView = (ImageView) gridView.findViewById(R.id.grid_item_image);
            if (state == 1) {
                final File imgFile = new File(mPaths.get(position));
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imageView.setImageBitmap(myBitmap);
                }
            } else if (state == 2) {
                RequestOptions error = new RequestOptions()
                        .placeholder(R.drawable.gallery)
                        .error(R.drawable.gallery)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);

                Glide.with(mContext.getApplicationContext()).load(mPaths.get(position)).apply(error).into(imageView);
            }

        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }*/

}
