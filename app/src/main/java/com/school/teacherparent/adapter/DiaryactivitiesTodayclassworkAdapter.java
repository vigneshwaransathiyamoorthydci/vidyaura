package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.ClassDetailListActivity;
import com.school.teacherparent.activity.ClassworkActivity;
import com.school.teacherparent.activity.ExamDetailListActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.ParentClassWorkFragment;
import com.school.teacherparent.fragment.SubClassforParentFragment;
import com.school.teacherparent.models.DiaryClassworkList;
import com.school.teacherparent.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.school.teacherparent.fragment.ParentDiaryFragment.studentID;

public class DiaryactivitiesTodayclassworkAdapter extends BaseAdapter
{
    private Context Context;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;


    ArrayList<DiaryClassworkList> classworkListstopicListTodayDue;
    String chapter_name;
    String classname;
    int classID;
    int sectionID;
    int subjectID;
    int selectedSchoolID;
    int studID;
    String classworkID;
    public DiaryactivitiesTodayclassworkAdapter(Context context, ArrayList<DiaryClassworkList> classworkListstopicListTodayDue , String chapter_name,
                                                String classname, int classID, int sectionID, int subjectID,
                                                int selectedSchoolID, int studID,String classworkID) {
        this.Context = context;
        this.classworkListstopicListTodayDue=classworkListstopicListTodayDue;
        this.chapter_name=chapter_name;
        this.classname=classname;
        this.classID=classID;
        this.sectionID=sectionID;
        this.subjectID=subjectID;
        this.selectedSchoolID=selectedSchoolID;
        this.studID=studID;
        this.classworkID=classworkID;
    }

    @Override
    public int getCount() {
        return classworkListstopicListTodayDue.size();
    }

    @Override
    public Object getItem(int position) {
        return classworkListstopicListTodayDue.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    Context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.diary_sub_itemone, null);

        }
        TextView chapterNametextview = (TextView) convertView.findViewById(R.id.chapterNametextview);
        TextView topicTextview = (TextView) convertView.findViewById(R.id.topicTextview);
        TextView duedate = (TextView) convertView.findViewById(R.id.duedate);
        ImageView homeworkImage=(ImageView)convertView.findViewById(R.id.homeworkImage);
        LinearLayout extraSpaceLineraLayout=(LinearLayout)convertView.findViewById(R.id.extraSpaceLineraLayout);
        extraSpaceLineraLayout.setVisibility(View.GONE);
        topicTextview.setText(classworkListstopicListTodayDue.get(position).getTopicDetails());
        chapterNametextview.setText(classworkListstopicListTodayDue.get(position).getChapterName().get(0).getChapter_name());
        duedate.setText(classworkListstopicListTodayDue.get(position).getCompleted_on());

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                    Context.startActivity(new Intent(Context, ClassworkActivity.class));
                } else  if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                    Intent intent = new Intent(Context, ClassDetailListActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("classname", classname);
                    bundle.putString("classwrkid", String.valueOf(classworkID));
                    bundle.putString("classid", String.valueOf(classID));
                    bundle.putString("sectionid", String.valueOf(sectionID));
                    bundle.putString("subjectid", String.valueOf(subjectID));
                    bundle.putInt("studentid", studentID);
                    bundle.putInt("selectedSchoolID", selectedSchoolID);
                    intent.putExtras(bundle);
                    Context.startActivity(intent);
                }
            }
        });


//        if (classwiseAssignmentList.size()==position+1)
//        {
//            extraSpaceLineraLayout.setVisibility(View.VISIBLE);
//        }

//        for (int y = 0; y < classwiseAssignmentList.get(position).getGetTopicDetails().size(); y++) {
//
//
//
//            topicTextview.setText(classwiseAssignmentList.get(position).getGetTopicDetails().get(y).getTopic_name());
//            chapterNametextview.setText(chapter_name);
//            duedate.setText(Context.getString(R.string.today));
//            }

        return convertView;
    }
}
