package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.ResultActivity;
import com.school.teacherparent.fragment.ExamDetailListFragment;
import com.school.teacherparent.fragment.ExamHistoryDetailListFragment;
import com.school.teacherparent.fragment.ExamHistoryFragment;
import com.school.teacherparent.fragment.ParentExamHistoryFragment;
import com.school.teacherparent.models.ExamHistoryList;
import com.school.teacherparent.models.ExamUpcomingList;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by keerthana on 10/11/2018.
 */


public class ExamHistoryAdapter extends RecyclerView.Adapter<ExamHistoryAdapter.MyViewHolder> {

    ArrayList<ExamHistoryList> examHistoryListArrayList;
    private Context context;
    ExamHistoryFragment examHistoryFragment;
    ParentExamHistoryFragment parentExamHistoryFragment;
    int userType;

    public ExamHistoryAdapter(ArrayList<ExamHistoryList> examHistoryListArrayList, Context context, ExamHistoryFragment examHistoryFragment, int userType) {
        this.examHistoryListArrayList = examHistoryListArrayList;
        this.context =context;
        this.examHistoryFragment=examHistoryFragment;
        this.userType=userType;
    }

    public ExamHistoryAdapter(ArrayList<ExamHistoryList> examHistoryListArrayList, Context context, ParentExamHistoryFragment parentExamHistoryFragment, int userType) {
        this.examHistoryListArrayList = examHistoryListArrayList;
        this.context =context;
        this.parentExamHistoryFragment=parentExamHistoryFragment;
        this.userType=userType;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private LinearLayout lin;
        private TextView title,school,counts,duration,startdate,enddate,viewresult,tvExamLabel;

        private CircularImageView img;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            school=(TextView)view.findViewById(R.id.school);
            counts=(TextView)view.findViewById(R.id.counts);
            duration=(TextView)view.findViewById(R.id.duration);
            startdate=(TextView)view.findViewById(R.id.startdate);
            enddate=(TextView)view.findViewById(R.id.enddate);
            viewresult=(TextView)view.findViewById(R.id.viewresult);
            img=(CircularImageView)view.findViewById(R.id.img);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            tvExamLabel=(TextView)view.findViewById(R.id.tv_exam_label);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_history_exam_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ExamHistoryList movie = examHistoryListArrayList.get(position);
        //holder.img.setImageResource(Constants.imgid[position]);
        holder.title.setText(movie.getExam_title());
        holder.school.setText(movie.getSchool_name());
        //holder.counts.setText(movie.getCounts());

        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date startDate= null;
        Date endDate= null;
        try {
            startDate = spf.parse(movie.getStart_date());
            endDate = spf.parse(movie.getEnd_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd MMM yyyy");
        String Start = spf.format(startDate);
        String End = spf.format(endDate);

        holder.startdate.setText(Start);
        holder.enddate.setText(End);
        holder.duration.setText(movie.getExam_duration()+" "+context.getString(R.string.hours));

        if (userType == 1) {
            holder.tvExamLabel.setText("Exam Attendees");
            if (movie.getExamwiseClassList() != null) {
                if (movie.getExamwiseClassList().size() > 1) {
                    holder.counts.setText("" + movie.getExamwiseClassList().size() + "-" + context.getString(R.string.classes));
                } else {
                    for (int i = 0; i < examHistoryListArrayList.get(position).getExamwiseClassList().size(); i++) {
                        holder.counts.setText("Class " + movie.getExamwiseClassList().get(i).getClassName()+" "+movie.getExamwiseClassList().get(i).getSection());
                    }
                }
            }
        } else if (userType == 2) {
            holder.tvExamLabel.setText("Exam Venue");
            holder.counts.setText("Class " +movie.getClassName()+" " +movie.getSection());
        }

        holder.viewresult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, ResultActivity.class).
                        putExtra("examterm",examHistoryListArrayList.get(position).getTerm_id()));
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ExamHistoryDetailListFragment();
                Bundle bundle=new Bundle();
                if (userType == 1) {
                    bundle.putSerializable("classlist",movie.getExamwiseClassList());
                    bundle.putInt("termid",movie.getTerm_id());
                    bundle.putString("exam_title",movie.getExam_title());
                    fragment.setArguments(bundle);
                    examHistoryFragment.replaceFragment(fragment);
                } else if (userType == 2) {
                    bundle.putInt("termid",movie.getTerm_id());
                    bundle.putString("exam_title",movie.getExam_title());
                    bundle.putInt("classid",movie.getClassroom_id());
                    bundle.putInt("sectionid",movie.getClasssection_id());
                    fragment.setArguments(bundle);
                    parentExamHistoryFragment.replaceFragment(fragment);
                }


            }
        });
        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_schools)+ movie.getLogo();

        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.img, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.img.setImageResource(R.drawable.nopreview);
            }
        });
    }

    @Override
    public int getItemCount() {
        return examHistoryListArrayList.size();
    }

    /*@Override
    public long getItemId(int position) {
        return position;
    }
*/
    @Override
    public int getItemViewType(int position) {
        return position;
    }

}