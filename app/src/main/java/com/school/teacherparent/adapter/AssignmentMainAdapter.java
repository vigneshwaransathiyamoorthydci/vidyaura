package com.school.teacherparent.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AssignmentListResponse;
import com.school.teacherparent.models.ClassworkList;
import com.school.teacherparent.models.ClassworkListAssHome;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.MyListView;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.school.teacherparent.fragment.HomeWorkFragment.selected;
import static com.school.teacherparent.fragment.HomeWorkFragment.selectedSchool;


/**
 * Created by keerthana on 9/27/2018.
 */

public class AssignmentMainAdapter extends RecyclerView.Adapter<AssignmentMainAdapter.MyViewHolder> {

    private ImageView img;
    private TextView name,std,school;
    ArrayList<AssignmentListResponse.assignmentsList> homeworkArrayList;
    private Context context;
    BaseActivity baseActivity;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private int userType;

    public AssignmentMainAdapter(ArrayList<AssignmentListResponse.assignmentsList> homeworkArrayList, Context context, BaseActivity baseActivity, int userType) {
        this.homeworkArrayList = homeworkArrayList;
        this.context =context;
        this.baseActivity=baseActivity;
        this.userType=userType;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin,lt_due_today;
        private  TextView subname,classname,homeworkduetodaylabel,todayshomeworkLabel,subtitle1,subtitle2,date1,date2,className,homeworkcountsTextview;
        private  ImageView menu,img2,img3;
        CircularImageView subjectImage;
        public TextView counts;
        MyListView duoetodayListview,duotommrowListview;



        public MyViewHolder(View view) {
            super(view);


            counts = (TextView) view.findViewById(R.id.counts);
            menu=(ImageView)view.findViewById(R.id.menu);
            img2=(ImageView)view.findViewById(R.id.img2);
            img3=(ImageView)view.findViewById(R.id.img3);
            todayshomeworkLabel = (TextView) view.findViewById(R.id.todayshomeworkLabel);
            classname = (TextView) view.findViewById(R.id.classname);
            subname = (TextView) view.findViewById(R.id.subname);

            date1 = (TextView) view.findViewById(R.id.date1);
            date2 = (TextView) view.findViewById(R.id.date2);
            subtitle1 = (TextView) view.findViewById(R.id.subtitle1);
            homeworkduetodaylabel = (TextView) view.findViewById(R.id.homeworkduetodaylabel);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            lt_due_today=(LinearLayout)view.findViewById(R.id.lt_due_today);
            duoetodayListview=(MyListView)view.findViewById(R.id.duoetodayListview);
            duotommrowListview=(MyListView)view.findViewById(R.id.duotommrowListview);
            subjectImage=(CircularImageView) view.findViewById(R.id.subjectImage);
            className=(TextView) view.findViewById(R.id.className);
            homeworkcountsTextview=(TextView)view.findViewById(R.id.homeworkcountsTextview);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.assignment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AssignmentListResponse.assignmentsList homeworkList = homeworkArrayList.get(position);
//        holder.classname.setText(context.getString(R.string.class_name)+homeworkList.getClasswiseHomework().get(position).getClassName()
//                +homeworkList.getClasswiseHomework().get(position).getSection());
//        holder.subname.setText(homeworkList.getClasswiseHomework().get(position).getSubjectName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (homeworkList.getClasswiseAssignments().size() > 0) {
                String assignmentID="";

                for (int j=0;j<homeworkList.getClasswiseAssignments().size();j++)
                {
                    assignmentID=assignmentID.concat
                            (String.valueOf((homeworkList.getClasswiseAssignments().get(j).getAssignmentID())).concat(","));
                }
                assignmentID = assignmentID.substring(0, assignmentID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseAssignments().size(); i++) {
//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(i).getClassName()
//                                    + homeworkList.getClasswiseAssignments().get(i).getSection()).putExtra("homeWorkID", assignmentID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(0).getClassName()
                                + homeworkList.getClasswiseAssignments().get(0).getSection()).putExtra("homeWorkID", assignmentID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
                        ).putExtra("subjectID", homeworkList.getSubjectID()).putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));
            } }
        });

        holder.duoetodayListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (homeworkList.getClasswiseAssignments().size() > 0) {
                String assignmentID="";

                for (int j=0;j<homeworkList.getClasswiseAssignments().size();j++)
                {
                    assignmentID=assignmentID.concat
                            (String.valueOf((homeworkList.getClasswiseAssignments().get(j).getAssignmentID())).concat(","));
                }
                assignmentID = assignmentID.substring(0, assignmentID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseAssignments().size(); i++) {
//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(i).getClassName()
//                                    + homeworkList.getClasswiseAssignments().get(i).getSection()).putExtra("homeWorkID", assignmentID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(0).getClassName()
                                + homeworkList.getClasswiseAssignments().get(0).getSection()).putExtra("homeWorkID", assignmentID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
                        ).putExtra("subjectID", homeworkList.getSubjectID()).putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));
            }
            }
        });

        holder.duotommrowListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (homeworkList.getClasswiseAssignments().size() > 0) {
                String assignmentID="";

                for (int j=0;j<homeworkList.getClasswiseAssignments().size();j++)
                {
                    assignmentID=assignmentID.concat
                            (String.valueOf((homeworkList.getClasswiseAssignments().get(j).getAssignmentID())).concat(","));
                }
                assignmentID = assignmentID.substring(0, assignmentID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseAssignments().size(); i++) {
//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(i).getClassName()
//                                    + homeworkList.getClasswiseAssignments().get(i).getSection()).putExtra("homeWorkID", assignmentID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseAssignments().get(0).getClassName()
                                + homeworkList.getClasswiseAssignments().get(0).getSection()).putExtra("homeWorkID", assignmentID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
                        ).putExtra("subjectID", homeworkList.getSubjectID()).putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));
            }
            }
        });

        if (userType == 1) {
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path) + "/" + homeworkArrayList.get(position).getClass_image();
            Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.subjectImage.setImageResource(R.drawable.nopreview);
                }
            });
        } else if (userType == 2) {
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path) + "/" + "";
            Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.subjectImage.setImageResource(R.drawable.nopreview);
                }
            });
        }

        //holder.subjectImage.setImageResource(Constants.imgid[position]);
       // holder.className.setText(homeworkList.getClassName()+homeworkList.getSection());
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
//                            context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", fData.getFeed_id()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {


                        }
                    }



                });

            }
        });

        ArrayList<ClassworkListAssHome> classworkListAssToday = new ArrayList<>();
        ArrayList<ClassworkListAssHome> classworkListAssTmrw = new ArrayList<>();
        for (int i = 0; i < homeworkList.getClasswiseAssignments().size(); i++) {
            if (userType == 1) {
                holder.classname.setText(context.getString(R.string.class_name) + homeworkList.getClasswiseAssignments().get(i).getClassName()
                        + homeworkList.getClasswiseAssignments().get(i).getSection());
            } else if (userType == 2) {
                holder.classname.setVisibility(View.GONE);
            }
            holder.subname.setText(homeworkList.getClasswiseAssignments().get(i).getSubjectName());
            holder.homeworkcountsTextview.setText(""+homeworkList.getClasswiseAssignments().get(i).getTotalAssignmentsCount());
            if (homeworkList.getClasswiseAssignments().get(i).getIsUserPostedHomework()==0)
            {
                holder.menu.setVisibility(View.GONE);
            }
            else
            {
                holder.menu.setVisibility(View.VISIBLE);
            }


            if (homeworkList.getClasswiseAssignments().get(i).getIsDueDateToday() == 1) {

                classworkListAssToday.add(new ClassworkListAssHome(homeworkList.getClasswiseAssignments().get(i).getChapter_name(), homeworkList.getClasswiseAssignments().get(i).getAssigned_on(), homeworkList.getClasswiseAssignments().get(i).getDueDate(), homeworkList.getClasswiseAssignments().get(i).getTopicDetails(),homeworkList.getClasswiseAssignments().get(i).getMark()));

            } else {
                classworkListAssTmrw.add(new ClassworkListAssHome(homeworkList.getClasswiseAssignments().get(i).getChapter_name(), homeworkList.getClasswiseAssignments().get(i).getAssigned_on(), homeworkList.getClasswiseAssignments().get(i).getDueDate(), homeworkList.getClasswiseAssignments().get(i).getTopicDetails(),homeworkList.getClasswiseAssignments().get(i).getMark()));

            }
           ArrayList<ClassworkList> classworkListstopicListTodayDue=new ArrayList<>();
            ArrayList<ClassworkList> classworkListstopicListTommroDue=new ArrayList<>();
            for (int j=0;j<homeworkList.getClasswiseAssignments().size();j++)
            {

                if (homeworkList.getClasswiseAssignments().get(j).getIsDueDateToday()==1)
                {
//                    HomesuboneAdapter homesuboneAdapter = new HomesuboneAdapter(context,homeworkList.getClasswiseHomework());
//                    holder.duoetodayListview.setAdapter(homesuboneAdapter);
                    classworkListstopicListTodayDue.add(new ClassworkList(homeworkList.getClasswiseAssignments().get(j).getTopicDetails(),
                            homeworkList.getClasswiseAssignments().get(j).getMark()));

                }
                else
                {
                    classworkListstopicListTommroDue.add(new ClassworkList(homeworkList.getClasswiseAssignments().get(j).getTopicDetails(),
                            homeworkList.getClasswiseAssignments().get(j).getMark()));
                }
            }

            /*HomeAssignmentoneAdapter homesuboneAdapter =
                    new HomeAssignmentoneAdapter(context,classworkListstopicListTodayDue,
                    homeworkList.getClasswiseAssignments().get(i).getChapter_name()
                    ,homeworkList.getClasswiseAssignments().get(i).getAssigned_on(),homeworkList.getClasswiseAssignments().get(i).getMark());
            holder.duoetodayListview.setAdapter(homesuboneAdapter);
            HomeAssignmenttwooAdapter homesubtwooAdapter = new HomeAssignmenttwooAdapter(context,classworkListstopicListTommroDue,homeworkList.getClasswiseAssignments().get(i).getChapter_name(),
                    homeworkList.getClasswiseAssignments().get(i).getDueDate(),homeworkList.getClasswiseAssignments().get(i).getAssigned_on(),homeworkList.getClasswiseAssignments().get(i).getMark());
            holder.duotommrowListview.setAdapter(homesubtwooAdapter);*/
            //Util.setListViewHeightBasedOnChildren(holder.duotommrowListview);
            //Util.setListViewHeightBasedOnChildren(holder.duoetodayListview);

            if (classworkListstopicListTodayDue.size()<=0)
            {
                holder.homeworkduetodaylabel.setVisibility(View.GONE);
                holder.lt_due_today.setVisibility(View.GONE);
            }

            if (classworkListstopicListTommroDue.size()<=0)
            {
                holder.todayshomeworkLabel.setVisibility(View.GONE);
            }


        }

        HomeAssignmentoneAdapter homesuboneAdapter =
                new HomeAssignmentoneAdapter(context,classworkListAssToday);
        holder.duoetodayListview.setAdapter(homesuboneAdapter);
        HomeAssignmenttwooAdapter homesubtwooAdapter = new HomeAssignmenttwooAdapter(context, classworkListAssTmrw);
        holder.duotommrowListview.setAdapter(homesubtwooAdapter);

        }

    @Override
    public int getItemCount() {
        return homeworkArrayList .size();
    }

}