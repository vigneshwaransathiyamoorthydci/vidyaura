package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ParentsOfList;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/25/2018.
 */


public class ParentsOfAdapter extends RecyclerView.Adapter<ParentsOfAdapter.MyViewHolder> {

    private  ImageView img;
    private TextView name,std,school;

    private Context context;
    List<ParentsOfList> parentsOfLists;
    public ParentsOfAdapter(List<ParentsOfList> parentsOfLists, Context context) {
        this.parentsOfLists=parentsOfLists;
        this.context=context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name,std,school;
        public ImageView img;

        public MyViewHolder(View view) {
            super(view);

            img=(ImageView)view.findViewById(R.id.user);
            name = (TextView) view.findViewById(R.id.name);
            std = (TextView) view.findViewById(R.id.std);
            school = (TextView) view.findViewById(R.id.school);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.parentsof_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ParentsOfList parentsOfList = parentsOfLists.get(position);
        holder.name.setText(parentsOfList.getFname()+" "+parentsOfList.getLname());
        holder.std.setText(parentsOfList.getClassName()+" "+parentsOfList.getSection());
        //holder.school.setText(parentsOfList.getSchool());


    }

    @Override
    public int getItemCount() {
        return parentsOfLists.size();
    }
}