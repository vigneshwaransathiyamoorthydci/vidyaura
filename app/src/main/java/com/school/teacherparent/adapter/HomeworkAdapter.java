package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.R;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ClassworkList;
import com.school.teacherparent.models.ClassworkListAssHome;
import com.school.teacherparent.models.DeleteFeedParms;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.MyListView;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

import static com.school.teacherparent.fragment.HomeWorkFragment.selected;
import static com.school.teacherparent.fragment.HomeWorkFragment.selectedSchool;


/**
 * Created by keerthana on 9/27/2018.
 */

public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ArrayList<HomeworkListResponse.homeworksList> homeworkArrayList;
    BaseActivity baseActivity;
    private ImageView img;
    private TextView name, std, school;
    private Context context;
    int userType;
    // public HomeworkAdapter.AddTouchListen addTouchListen;

    public HomeworkAdapter(ArrayList<HomeworkListResponse.homeworksList> homeworkArrayList, Context context, BaseActivity baseActivity, int userType) {
        this.homeworkArrayList = homeworkArrayList;
        this.context = context;
        this.baseActivity = baseActivity;
        this.userType = userType;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item, parent, false);

        return new MyViewHolder(itemView);
    }
    //
//    public void setOnClickListen(AddTouchListen addTouchListen)
//
//    {
//        this.addTouchListen=addTouchListen;
//
//    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeworkListResponse.homeworksList homeworkList = homeworkArrayList.get(position);
//        holder.classname.setText(context.getString(R.string.class_name)+homeworkList.getClasswiseHomework().get(position).getClassName()
//                +homeworkList.getClasswiseHomework().get(position).getSection());
//        holder.subname.setText(homeworkList.getClasswiseHomework().get(position).getSubjectName());

        if (userType == 1) {
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+homeworkArrayList.get(position).getClass_image();
            Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.subjectImage.setImageResource(R.drawable.nopreview);
                }
            });
        } else if (userType == 2) {
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path) + "/" + "";
            Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.subjectImage.setImageResource(R.drawable.nopreview);
                }
            });
        }

        //holder.subjectImage.setImageResource(Constants.imgid[position]);
        //holder.className.setText(homeworkList.getClassName() + homeworkList.getSection());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String homeWorkID = "";

                for (int j = 0; j < homeworkList.getClasswiseHomework().size(); j++) {
                    homeWorkID = homeWorkID.concat(String.valueOf((homeworkList.getClasswiseHomework().get(j).getHomeworkID())).concat(","));
                }
                homeWorkID = homeWorkID.substring(0, homeWorkID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseHomework().size(); i++) {

//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(i).getClassName()
//                                    + homeworkList.getClasswiseHomework().get(i).getSection()).putExtra("homeWorkID", homeWorkID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(0).getClassName()
                                + homeworkList.getClasswiseHomework().get(0).getSection()).putExtra("homeWorkID", homeWorkID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID())
                        .putExtra("subjectID", homeworkList.getSubjectID())
                        .putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));


//                        context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
//                        .putExtra("classname",context.getString(R.string.class_name)+" "+homeworkList.getClasswiseHomework().get(position).getClassName()
//                        + homeworkList.getClasswiseHomework().get(position).getSection()).putExtra("homeWorkID",homeWorkID)
//                .putExtra("classID",homeworkList.getClassID()).putExtra("sectionID",homeworkList.getSectionID()
//                                ).putExtra("subjectID",homeworkList.getSubjectID()));
            }
        });

        holder.duoetodayListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String homeWorkID = "";

                for (int j = 0; j < homeworkList.getClasswiseHomework().size(); j++) {
                    homeWorkID = homeWorkID.concat(String.valueOf((homeworkList.getClasswiseHomework().get(j).getHomeworkID())).concat(","));
                }
                homeWorkID = homeWorkID.substring(0, homeWorkID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseHomework().size(); i++) {

//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(i).getClassName()
//                                    + homeworkList.getClasswiseHomework().get(i).getSection()).putExtra("homeWorkID", homeWorkID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(0).getClassName()
                                + homeworkList.getClasswiseHomework().get(0).getSection()).putExtra("homeWorkID", homeWorkID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID())
                        .putExtra("subjectID", homeworkList.getSubjectID())
                        .putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));
            }
        });

        holder.duotommrowListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String homeWorkID = "";

                for (int j = 0; j < homeworkList.getClasswiseHomework().size(); j++) {
                    homeWorkID = homeWorkID.concat(String.valueOf((homeworkList.getClasswiseHomework().get(j).getHomeworkID())).concat(","));
                }
                homeWorkID = homeWorkID.substring(0, homeWorkID.length() - 1);
                for (int i = 0; i < homeworkList.getClasswiseHomework().size(); i++) {

//                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
//                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(i).getClassName()
//                                    + homeworkList.getClasswiseHomework().get(i).getSection()).putExtra("homeWorkID", homeWorkID)
//                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
//                            ).putExtra("subjectID", homeworkList.getSubjectID()));
                }
                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
                        .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(0).getClassName()
                                + homeworkList.getClasswiseHomework().get(0).getSection()).putExtra("homeWorkID", homeWorkID)
                        .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID())
                        .putExtra("subjectID", homeworkList.getSubjectID())
                        .putExtra("studID", selected()).putExtra("selectedSchoolID", selectedSchool()));
            }
        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
//                            context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", fData.getFeed_id()));

                        } else {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {


                        }
                    }


                });

            }
        });

        ArrayList<ClassworkListAssHome> classworkListAssHomeToday = new ArrayList<>();
        ArrayList<ClassworkListAssHome> classworkListAssHomeTmrw = new ArrayList<>();
        for (int i = 0; i < homeworkList.getClasswiseHomework().size(); i++) {
            if (userType == 1) {
                holder.classname.setText(context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(i).getClassName()
                        + homeworkList.getClasswiseHomework().get(i).getSection());
            } else if (userType == 2) {
                holder.classname.setVisibility(View.GONE);
            }
            holder.subname.setText(homeworkList.getClasswiseHomework().get(i).getSubjectName());
            holder.homeworkcountsTextview.setText("" + homeworkList.getClasswiseHomework().get(i).getTotalHomeworkCount());
            if (homeworkList.getClasswiseHomework().get(i).getIsUserPostedHomework() == 0) {
                holder.menu.setVisibility(View.GONE);
            } else {
                holder.menu.setVisibility(View.GONE);
            }
            ArrayList<ClassworkList> classworkListstopicListTodayDue = new ArrayList<>();
            ArrayList<ClassworkList> classworkListstopicListTommroDue = new ArrayList<>();
            for (int j = 0; j < homeworkList.getClasswiseHomework().size(); j++) {

                if (homeworkList.getClasswiseHomework().get(j).getIsDueDateToday() == 1) {

                    classworkListstopicListTodayDue.add(new ClassworkList(homeworkList.getClasswiseHomework().get(j).getTopicDetails(), 0));

                } else {
                    classworkListstopicListTommroDue.add(new ClassworkList(homeworkList.getClasswiseHomework().get(j).getTopicDetails(), 0));
                }

            }

            if (homeworkList.getClasswiseHomework().get(i).getIsDueDateToday() == 1) {

                classworkListAssHomeToday.add(new ClassworkListAssHome(homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getAssigned_on(), homeworkList.getClasswiseHomework().get(i).getDueDate(),homeworkList.getClasswiseHomework().get(i).getTopicDetails(),0));

            } else {
                classworkListAssHomeTmrw.add(new ClassworkListAssHome(homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getAssigned_on(), homeworkList.getClasswiseHomework().get(i).getDueDate(), homeworkList.getClasswiseHomework().get(i).getTopicDetails(),0));

            }
        /*    //HomesuboneAdapter homesuboneAdapter = new HomesuboneAdapter(context, classworkListstopicListTodayDue, homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getAssigned_on());
            HomesuboneAdapter homesuboneAdapter = new HomesuboneAdapter(context,classworkListAssHomeToday);
            holder.duoetodayListview.setAdapter(homesuboneAdapter);
            //HomesubtwooAdapter homesubtwooAdapter = new HomesubtwooAdapter(context, classworkListstopicListTommroDue, homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getDueDate(), homeworkList.getClasswiseHomework().get(i).getAssigned_on());
            HomesubtwooAdapter homesubtwooAdapter = new HomesubtwooAdapter(context, classworkListAssHomeTmrw);
            holder.duotommrowListview.setAdapter(homesubtwooAdapter);
            //Util.setListViewHeightBasedOnChildren(holder.duotommrowListview);
            //Util.setListViewHeightBasedOnChildren(holder.duoetodayListview);*/

            if (classworkListstopicListTodayDue.size() <= 0) {
                holder.lt_due_today.setVisibility(View.GONE);
                holder.homeworkduetodaylabel.setVisibility(View.GONE);
            }

            if (classworkListstopicListTommroDue.size() <= 0) {
                holder.todayshomeworkLabel.setVisibility(View.GONE);
            }


        }


        //HomesuboneAdapter homesuboneAdapter = new HomesuboneAdapter(context, classworkListstopicListTodayDue, homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getAssigned_on());
        HomesuboneAdapter homesuboneAdapter = new HomesuboneAdapter(context,classworkListAssHomeToday);
        holder.duoetodayListview.setAdapter(homesuboneAdapter);
        //HomesubtwooAdapter homesubtwooAdapter = new HomesubtwooAdapter(context, classworkListstopicListTommroDue, homeworkList.getClasswiseHomework().get(i).getChapter_name(), homeworkList.getClasswiseHomework().get(i).getDueDate(), homeworkList.getClasswiseHomework().get(i).getAssigned_on());
        HomesubtwooAdapter homesubtwooAdapter = new HomesubtwooAdapter(context, classworkListAssHomeTmrw);
        holder.duotommrowListview.setAdapter(homesubtwooAdapter);
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//
//
//            @Override
//            public void onClick(View view) {
//
//                if(addTouchListen!=null)
//                {
//                    addTouchListen.OnTouchClick(position);
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return homeworkArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView counts;
        MyListView duoetodayListview, duotommrowListview;
        private LinearLayout lin,lt_due_today;
        private TextView subname, classname, homeworkduetodaylabel, todayshomeworkLabel, subtitle1, subtitle2, date1, date2, className, homeworkcountsTextview;
        private ImageView menu, img2, img3;
        CircularImageView subjectImage;


        public MyViewHolder(View view) {
            super(view);


            counts = (TextView) view.findViewById(R.id.counts);
            menu = (ImageView) view.findViewById(R.id.menu);
            img2 = (ImageView) view.findViewById(R.id.img2);
            img3 = (ImageView) view.findViewById(R.id.img3);
            todayshomeworkLabel = (TextView) view.findViewById(R.id.todayshomeworkLabel);
            classname = (TextView) view.findViewById(R.id.classname);
            subname = (TextView) view.findViewById(R.id.subname);

            date1 = (TextView) view.findViewById(R.id.date1);
            date2 = (TextView) view.findViewById(R.id.date2);
            subtitle1 = (TextView) view.findViewById(R.id.subtitle1);
            homeworkduetodaylabel = (TextView) view.findViewById(R.id.homeworkduetodaylabel);
            lt_due_today = (LinearLayout) view.findViewById(R.id.lt_due_today);
            lin = (LinearLayout) view.findViewById(R.id.lin);
            duoetodayListview = (MyListView) view.findViewById(R.id.duoetodayListview);
            duotommrowListview = (MyListView) view.findViewById(R.id.duotommrowListview);
            subjectImage = (CircularImageView) view.findViewById(R.id.subjectImage);
            className = (TextView) view.findViewById(R.id.className);
            homeworkcountsTextview = (TextView) view.findViewById(R.id.homeworkcountsTextview);
        }
    }

//    public static class AddTouchListen {
//
//        public void OnTouchClick(int position) {
//
//        }
//    }
}