package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.models.StudentResultList;

import java.util.ArrayList;


/**
 * Created by keerthana on 10/5/2018.
 */


public class AddResultAdapter extends RecyclerView.Adapter<AddResultAdapter.MyViewHolder> {

    private ArrayList<StudentResultList> studentResultLists=new ArrayList<>();
    private Context context;
    private int selectedPosition=-1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        EditText markEditText;
        public TextView  name;
        ImageView img;


        public MyViewHolder(View view) {
            super(view);

            img=(ImageView)view.findViewById(R.id.img);
            name=(TextView)view.findViewById(R.id.name);
            markEditText=(EditText)view.findViewById(R.id.markEditText);
        }
    }


    public AddResultAdapter(ArrayList<StudentResultList> studentResultLists, Context context) {
        this.studentResultLists = studentResultLists;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_add_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final StudentResultList studentResultList = studentResultLists.get(position);
      holder.name.setText(studentResultList.getFname()+" "+studentResultList.getLname());
        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/"+ studentResultLists.get(position).getStudent_photo();
        Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.img);




        holder.markEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String mark= String.valueOf(s);
                if(s.length() != 0) {
                    studentResultList.setMark(Integer.parseInt(mark));
                }
                else
                {
                    studentResultList.setMark(0);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return studentResultLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}