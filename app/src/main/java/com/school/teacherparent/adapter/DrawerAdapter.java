package com.school.teacherparent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.school.teacherparent.R;


public class DrawerAdapter extends BaseAdapter {

    Context ctx;

    //    String[] values = new String[]
//            {
//                    "Home", "Attendance", "Recent Activities", "Assignment", "Results", "Circulars", "Polls", "Pay Fees", "Language", "Logout"
//                    ,};
    private ListView listView;
    private Context context;
    String a[];
    Integer[] imgs;

    /*Integer[] imgs = new Integer[]
            {
                    R.mipmap.school,
                    R.mipmap.timetable,
                    R.mipmap.gallery,
                    R.mipmap.settings,
                    R.mipmap.help,

                        *//*android.R.drawable.ic_menu_camera,
                        android.R.drawable.ic_menu_gallery,
                        android.R.drawable.ic_menu_manage,
                        android. R.drawable.ic_menu_slideshow,
                        android. R.drawable.ic_menu_share,
                        android.R.drawable.ic_menu_camera,
                        android.R.drawable.ic_menu_share,
                        android. R.drawable.ic_menu_slideshow,
                        android.R.drawable.ic_menu_camera,

                        // R.drawable.chat,
                        android. R.drawable.ic_menu_camera,*//*


            };*/

    public DrawerAdapter(Context ctx, String[] sidemenu, Integer[] imgs) {
        this.ctx = ctx;
        this.a = sidemenu;
        this.imgs = imgs;
    }

    @Override
    public int getCount() {
        return a.length;
    }

    @Override
    public Object getItem(int position) {
        return a[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {

            LayoutInflater vi = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.drawer_main, null);
        }


//              listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                //                    Intent ieventreport = new Intent(context,MainActivity.class);
////                    context.startActivity(ieventreport);
//                if (position == 0) {
//                   // Toast.makeText()
//                }
//
//                }
//        });
//
//


        TextView buttonFlat = (TextView) rowView.findViewById(R.id.text);

        buttonFlat.setText(a[position]);

        ImageView img = (ImageView) rowView.findViewById(R.id.img);

        img.setBackgroundResource(imgs[position]);
        img.setScaleType(ImageView.ScaleType.FIT_XY);


        return rowView;

    }

}


