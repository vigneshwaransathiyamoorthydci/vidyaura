package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.joooonho.SelectableRoundedImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.Photoslist;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 11/12/2018.
 */

public class PhotosGalleryAdapter extends RecyclerView.Adapter<PhotosGalleryAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
     List<Photoslist> photoList;
    private Context context;

    private PhotosGalleryAdapter.AddTouchListen addTouchListen;
    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout header;
        ImageView img;
        public VideoView v;


        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.image);
            //header=(LinearLayout)view.findViewById(R.id.header);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


    public PhotosGalleryAdapter(List<Photoslist> photoList, Context context) {
        this.photoList = photoList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photos_gallery_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(PhotosGalleryAdapter.AddTouchListen addTouchListen) {

        this.addTouchListen=addTouchListen;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Photoslist gallerylist = photoList.get(position);

        holder.img.setVisibility(View.VISIBLE);

            String url=context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+gallerylist.getImage();
            /*Picasso.get().load(url).into(holder.img, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.img.setImageResource(R.mipmap.galleryimg);
                }
            });*/

        RequestOptions error = new RequestOptions()
                /*.centerCrop()*/
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.img);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });


        }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
}




