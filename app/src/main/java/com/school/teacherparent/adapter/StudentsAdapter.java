package com.school.teacherparent.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.ChatWithParentListFragment;
import com.school.teacherparent.fragment.ChatWithTeacherListFragment;
import com.school.teacherparent.models.ClassHandled;
import com.school.teacherparent.models.GroupChatResponse;
import com.school.teacherparent.models.StudentsListforChat;
import com.school.teacherparent.models.StudentsListforChatResponse;
import com.school.teacherparent.models.TeachersListforChatResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {

    Context context;
    private List<StudentsListforChatResponse.chatStudentsList> chatStudentsListArrayList;
    private List<TeachersListforChatResponse.chatTeachersList> chatTeachersListArrayList;
    private List<GroupChatResponse.groupList> groupListArrayList;
    private int userType;
    public AddTouchListen addTouchListen;
    private int state;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private ClassHandled teachersListforChatResponse;
    private ArrayList<ClassHandled.chatTeachersDetailsList> chatTeachersDetailsLists = new ArrayList<>();
    ArrayList<String> name = new ArrayList<>();
    private ChatWithParentListFragment chatWithParentListFragment;
    private List<TeachersListforChatResponse.chatTeachersList> chatTeachersList;
    private List<GroupChatResponse.groupList> groupListArray;
    private ChatWithTeacherListFragment chatWithTeacherListFragment;

    public StudentsAdapter(Context context, List<StudentsListforChatResponse.chatStudentsList> chatStudentsListArrayList, int state, ChatWithParentListFragment chatWithParentListFragment) {
        this.context = context;
        this.chatStudentsListArrayList = chatStudentsListArrayList;
        this.state = state;
        this.chatWithParentListFragment = chatWithParentListFragment;
    }

    public StudentsAdapter(List<TeachersListforChatResponse.chatTeachersList> chatTeachersListArrayList, Context context, int state, ChatWithParentListFragment chatWithParentListFragment) {
        this.chatTeachersListArrayList = chatTeachersListArrayList;
        this.context = context;
        this.state = state;
        this.chatWithParentListFragment = chatWithParentListFragment;
    }

    public StudentsAdapter(Context context, int state, List<GroupChatResponse.groupList> groupListArrayList, ChatWithParentListFragment chatWithParentListFragment) {
        this.context = context;
        this.state = state;
        this.groupListArrayList = groupListArrayList;
        this.chatWithParentListFragment = chatWithParentListFragment;
    }

    public StudentsAdapter(Context context, List<TeachersListforChatResponse.chatTeachersList> chatTeachersList, int state) {
        this.context = context;
        this.state = state;
        this.chatTeachersList = chatTeachersList;
    }

    public StudentsAdapter(Context context, int state, List<GroupChatResponse.groupList> groupListArray,ChatWithTeacherListFragment chatWithTeacherListFragment) {
        this.context = context;
        this.state = state;
        this.groupListArray = groupListArray;
        this.chatWithTeacherListFragment = chatWithTeacherListFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_students_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        if (state == 1) {
            holder.tvUserName.setText(chatStudentsListArrayList.get(position).getFname() + " " + chatStudentsListArrayList.get(position).getLname());
            //holder.tvClassName.setText(chatStudentsListArrayList.get(position).getReceiverClass());
            //holder.tvRoll.setText(chatStudentsListArrayList.get(position).getReceiverRole());
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/" + chatStudentsListArrayList.get(position).getStudent_photo();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.ivProfileImage);
        } else if (state == 2) {
            holder.llClass.setVisibility(View.GONE);
            holder.ivTeachersInfo.setVisibility(View.VISIBLE);
            holder.tvUserName.setText(chatTeachersListArrayList.get(position).getFname()+" "+chatTeachersListArrayList.get(position).getLname());
            holder.tvClassName.setText(chatTeachersListArrayList.get(position).getTeacherClass());
            //holder.tvRoll.setText(chatTeachersListArrayList.get(position).getTeacherRole());
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + chatTeachersListArrayList.get(position).getEmp_photo();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.ivProfileImage);

            holder.ivTeachersInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_teacher_info);

                    ImageView ivClose = dialog.findViewById(R.id.iv_close);
                    ImageView ivTeacherImage = dialog.findViewById(R.id.iv_teacher_image);
                    TextView tvTeacherName = dialog.findViewById(R.id.tv_teacher_name);
                    TextView tvSubjectHandled = dialog.findViewById(R.id.tv_subject_handled);
                    RecyclerView rvSubjectHandled = dialog.findViewById(R.id.rv_subject_handled);
                    TextView tvNoResult = dialog.findViewById(R.id.tv_noresult);
                    LinearLayout ll_class = dialog.findViewById(R.id.ll_class);
                    rvSubjectHandled.setHasFixedSize(true);
                    rvSubjectHandled.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

                    tvTeacherName.setText(chatTeachersListArrayList.get(position).getFname()+" "+chatTeachersListArrayList.get(position).getLname());
                    Glide.with(context.getApplicationContext()).load(url).apply(error).into(ivTeacherImage);
                    //tvSubjectHandled.setVisibility(View.GONE);
                    //tvSubjectHandled.setText("Grade 1A - English");
                    chatTeachersDetailsLists.clear();

                    if (Util.isNetworkAvailable()) {
                        StudentsListforChat studentsListforChat = new StudentsListforChat();
                        studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, null));
                        studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                        studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                        studentsListforChat.setTeacherID(chatTeachersListArrayList.get(position).getTeacherEncryptedID());

                        vidyauraAPI.getTeachersDetailsforChat(studentsListforChat).enqueue(new Callback<ClassHandled>() {
                            @Override
                            public void onResponse(Call<ClassHandled> call, Response<ClassHandled> response) {
                                if (response.body() != null) {
                                    teachersListforChatResponse = response.body();
                                        if (teachersListforChatResponse.getStatus() == Util.STATUS_SUCCESS) {
                                            if (response.body().getChatTeachersDetailsList().size() > 0) {
                                                rvSubjectHandled.setVisibility(View.VISIBLE);
                                                tvNoResult.setVisibility(View.GONE);
                                                ll_class.setVisibility(View.VISIBLE);
                                                chatTeachersDetailsLists = response.body().getChatTeachersDetailsList();
                                                ClassHandleAdapter classHandleAdapter = new ClassHandleAdapter(context,chatTeachersDetailsLists);
                                                rvSubjectHandled.setAdapter(classHandleAdapter);
                                                classHandleAdapter.notifyDataSetChanged();
                                            } else {
                                                ll_class.setVisibility(View.GONE);
                                                rvSubjectHandled.setVisibility(View.GONE);
                                                tvNoResult.setVisibility(View.VISIBLE);
                                            }
                                        } else {
                                            ll_class.setVisibility(View.GONE);
                                            rvSubjectHandled.setVisibility(View.GONE);
                                            tvNoResult.setVisibility(View.VISIBLE);
                                            Toast.makeText(context, teachersListforChatResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                } else {
                                    Toast.makeText(context, context.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<ClassHandled> call, Throwable t) {
                                Toast.makeText(context, context.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                            }
                        });

                    } else {
                        Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                    }

                    dialog.show();
                    ivClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }
            });

        } else if (state == 3) {
            holder.llClass.setVisibility(View.VISIBLE);
            holder.ivTeachersInfo.setVisibility(View.VISIBLE);
            holder.tvUserName.setText(groupListArrayList.get(position).getGroupName());

            int pos = groupListArrayList.get(position).getDataPosition();
            name.clear();
            for (int i=0;i<groupListArrayList.get(position).getUserList().size();i++) {
                if (pos == groupListArrayList.get(position).getUserList().get(i).getPosition()) {
                    name.add(groupListArrayList.get(position).getUserList().get(i).getData());
                }
            }

            holder.tvClassName.setText(Arrays.toString(new ArrayList[]{name}).replaceAll("\\[|\\]", ""));
            /*if (groupListArrayList.get(position).getTeacherClass().size() > 0) {
                holder.tvClassName.setText(Arrays.toString(new ArrayList[]{groupListArrayList.get(position).getTeacherClass()}).replaceAll("\\[|\\]", ""));
            } else {
                holder.tvClassName.setVisibility(View.GONE);
            }*/

            holder.tvRoll.setVisibility(View.GONE);
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_group_path) + "/" + groupListArrayList.get(position).getGroupImage();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.ivProfileImage);

            holder.ivTeachersInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chatWithParentListFragment.teacherInfo(pos);
                }
            });

        } else if (state == 4) {
            holder.llClass.setVisibility(View.VISIBLE);
            holder.tvRoll.setVisibility(View.VISIBLE);
            holder.tvUserName.setText(chatTeachersList.get(position).getFname()+" "+chatTeachersList.get(position).getLname());
            holder.tvClassName.setText(chatTeachersList.get(position).getTeacherClass());
            holder.tvRoll.setText(chatTeachersList.get(position).getTeacherRole());

            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + chatTeachersList.get(position).getEmp_photo();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.ivProfileImage);
        } else if (state == 5) {
            holder.llClass.setVisibility(View.VISIBLE);
            holder.ivTeachersInfo.setVisibility(View.VISIBLE);
            holder.tvUserName.setText(groupListArray.get(position).getGroupName());

            int pos = groupListArray.get(position).getDataPosition();
            name.clear();
            for (int i=0;i<groupListArray.get(position).getUserList().size();i++) {
                if (pos == groupListArray.get(position).getUserList().get(i).getPosition()) {
                    name.add(groupListArray.get(position).getUserList().get(i).getData());
                }
            }

            holder.tvClassName.setText(Arrays.toString(new ArrayList[]{name}).replaceAll("\\[|\\]", ""));
            /*if (groupListArrayList.get(position).getTeacherClass().size() > 0) {
                holder.tvClassName.setText(Arrays.toString(new ArrayList[]{groupListArrayList.get(position).getTeacherClass()}).replaceAll("\\[|\\]", ""));
            } else {
                holder.tvClassName.setVisibility(View.GONE);
            }*/

            holder.tvRoll.setVisibility(View.GONE);
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_group_path) + "/" + groupListArray.get(position).getGroupImage();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.ivProfileImage);

            holder.ivTeachersInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chatWithTeacherListFragment.teacherInfo(pos);
                }
            });

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), NewMessageActivity.class));
                Intent intent=new Intent(context, MessageActivity.class);
                intent.putExtra("ReceiverID", chatStudentsListArrayList.get(0).getReceiverUserID());
                intent.putExtra("ReceiverName", chatStudentsListArrayList.get(0).getReceiverName());
                intent.putExtra("ReceiverImage", chatStudentsListArrayList.get(0).getReceiverImage());
                intent.putExtra("ReceiverFcmKey", chatStudentsListArrayList.get(0).getReceiverFcmKey());
                context.startActivity(intent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (state == 1) {
            count = (null != chatStudentsListArrayList ? chatStudentsListArrayList.size() : 0);
        } else if (state == 2) {
            count = (null != chatTeachersListArrayList ? chatTeachersListArrayList.size() : 0);
        } else if (state == 3) {
            count = (null != groupListArrayList ? groupListArrayList.size() : 0);
        } else if (state == 4) {
            count = (null != chatTeachersList ? chatTeachersList.size() : 0);
        } else if (state == 5) {
            count = (null != groupListArray ? groupListArray.size() : 0);
        }
        return count;
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProfileImage,ivTeachersInfo;
        TextView tvUserName, tvClassName, tvRoll;
        LinearLayout llClass;

        public ViewHolder(View view) {
            super(view);
            ivProfileImage = view.findViewById(R.id.iv_profile_image);
            tvUserName = view.findViewById(R.id.tv_username);
            llClass = view.findViewById(R.id.ll_class);
            tvClassName = view.findViewById(R.id.tv_class_name);
            tvRoll = view.findViewById(R.id.tv_roll);
            ivTeachersInfo = view.findViewById(R.id.iv_teachers_info);
        }
    }
}
