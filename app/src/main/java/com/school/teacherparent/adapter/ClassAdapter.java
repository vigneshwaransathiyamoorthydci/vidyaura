package com.school.teacherparent.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;


import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by keerthana on 9/20/2018.
 */

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.MyViewHolder> {

    private List<ClassworkLists.ClassworkList> classList=new ArrayList<>();
    private final LayoutInflater inflater;
    private Context context;
    private AddTouchListen addTouchListen;



    public ClassAdapter(List<ClassworkLists.ClassworkList> classList, Context context) {

        this.classList=classList;
        inflater = LayoutInflater.from(context);
        this.context=context;


    }

    public String getDateFormatforatten(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    /*public ClassAdapter(List<ClassDTO> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // private ProgressBar prg1,prg2;
        private  TextView counts,className;
        private  CircularImageView classNameImgeview;
        private  ImageView img2;
        private  TextView title,content,status1,status2,status3;
        LinearLayout header,dynamic_data;
        SeekBar t1;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title1);
            content = (TextView) view.findViewById(R.id.content1);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);
            img2=(ImageView)view.findViewById(R.id.img2);
            status1=(TextView)view.findViewById(R.id.status1);
            status2=(TextView)view.findViewById(R.id.status2);
            status3=(TextView)view.findViewById(R.id.status3);
            counts=(TextView)view.findViewById(R.id.counts);
            header=view.findViewById(R.id.header);
            dynamic_data=(LinearLayout) view.findViewById(R.id.dynamic_data);
            className=view.findViewById(R.id.className);
            t1=view.findViewById(R.id.t1);
            int intMaxNoOfChild = 0;
            for (int index = 0; index < classList.size(); index++) {
                int intMaxSizeTemp = classList.get(index).getTodaysClassworkProgress().size();

                if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
            }

            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                RelativeLayout layout = (RelativeLayout) inflater.
                        inflate(R.layout.item_class_work, null, false);
//                TextView textView = new TextView(context);
//                textView.setId(indexView);
//                textView.setTextColor(Color.BLACK);
//                textView.setPadding(0, 20, 0, 20);
//                textView.setGravity(Gravity.CENTER);
//                textView.setBackgroundResource(R.color.colorPrimary);
                LinearLayout.LayoutParams layoutParams = new
                        LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0,10,0,0);
                dynamic_data.addView(layout, layoutParams);

            }

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.class_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.t1.setEnabled(true);
        ClassworkLists.ClassworkList classworkList = classList.get(position);
        holder.title.setText(context.getString(R.string.class_name)+" "+classworkList.getClassName()+classworkList.getSection());
        holder.content.setText(classworkList.getSubjectName());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+classList.get(position).getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.classNameImgeview.setImageResource(Constants.imgid[position]);
        // holder.className.setText(classworkList.getClassName() + classworkList.getSection());
//        holder.img2.setImageResource(Integer.parseInt(classworkList.getImg2()));
//        holder.status2.setText(classworkList.getStatus2());
//        holder.status1.setText(classworkList.getStatus1());
        holder.counts.setText(classworkList.getClassworkCompletedPercentage()+"%");
        if (classworkList.getClassworkCompletedPercentage()!=null&&!
                classworkList.getClassworkCompletedPercentage().equals("")){
            int value=Math.round(Float.parseFloat(classworkList.getClassworkCompletedPercentage()));
            holder.t1.setProgress(value);
        }

        int noOfChildTextViews = holder.dynamic_data.getChildCount();
        int noOfChild = classworkList.getTodaysClassworkProgress().size();
        if (noOfChild < noOfChildTextViews) {
            for (int index = noOfChild; index < noOfChildTextViews; index++) {
//                TextView currentTextView = (TextView) holder.lin.getChildAt(index);
//                currentTextView.setVisibility(View.GONE);
                RelativeLayout relativeLayout = (RelativeLayout) holder.dynamic_data.getChildAt(index);
                relativeLayout.setVisibility(View.GONE);
                //currentTextView.setText(classworkList.getDummy().get(index).getSubject());
            }
        }


        for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
            RelativeLayout relativeLayout = (RelativeLayout) holder.dynamic_data.getChildAt(textViewIndex);
//
//            LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//            LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//            marksLinear.setVisibility(View.GONE);
//            totalLinear.setVisibility(View.VISIBLE);
            TextView status1 = (TextView) relativeLayout.findViewById(R.id.status1);
            TextView status2 = (TextView) relativeLayout.findViewById(R.id.status2);
            TextView status3 = (TextView) relativeLayout.findViewById(R.id.status3);


//            if (classworkList.getSyllabusList().get(textViewIndex).getStatus()==1)
//            {
            if (classworkList.getTodaysClassworkProgress().get(textViewIndex).getChapterName() != null) {
                if (classworkList.getTodaysClassworkProgress().get(textViewIndex).getChapterName().size() > 0) {
                    status1.setText(classworkList.getTodaysClassworkProgress().get(textViewIndex).getChapterName().get(0).getChapter_name());
                }
            }
            status1.setVisibility(View.VISIBLE);
            status2.setText(classworkList.getTodaysClassworkProgress().get(textViewIndex).getTopicDetails());


            //chapterincompletetextview.setText(context.getString(R.string.Chapter)+":"+" "+classworkList.getDummy().get(textViewIndex).getChapter());
            status3.setText(getDateFormatforatten(classworkList.getTodaysClassworkProgress().get(textViewIndex).getTopicCompletedDate()));

//            }
//            else
//            {
//                chaptercompletetextview.setVisibility(View.GONE);
//                chapterincompletetextview.setText(context.getString(R.string.Chapter)+":"+" "+classworkList.getSyllabusList().get(textViewIndex).getChapterTopicDetails());
//                averagetextview.setText(context.getString(R.string.topic)+":"+" "+classworkList.getSyllabusList().get(textViewIndex).getChapterTopicDetails());
//            }

//                martksTextview.setText(classworkList.getDummy().get(textViewIndex).getMarks1());
//            if (classworkList.getDummy().get(textViewIndex).getSubject().equals("Total"))
//            {
//                LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//                LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//                marksLinear.setVisibility(View.GONE);
//                totalLinear.setVisibility(View.VISIBLE);
//
//                TextView totaltextviewlabel=(TextView)relativeLayout.findViewById(R.id.totaltextviewlabel);
//                TextView averagetextview=(TextView)relativeLayout.findViewById(R.id.averagetextview);
//                TextView totalTextview=(TextView)relativeLayout.findViewById(R.id.totaltextview);
//
//
//            }
//            else
//            {
//                LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//                LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//                totalLinear.setVisibility(View.GONE);
//                marksLinear.setVisibility(View.VISIBLE);
//                TextView subjectTextview=(TextView)relativeLayout.findViewById(R.id.subject);
//                TextView martksTextview=(TextView)relativeLayout.findViewById(R.id.stmarks);
//                TextView totalTextview=(TextView)relativeLayout.findViewById(R.id.marks1);
//                //relativeLayout.getChildCount();
//                //TextView currentTextView = (TextView) holder.lin.getChildAt(textViewIndex);
//                subjectTextview.setText(classworkList.getDummy().get(textViewIndex).getSubject());
//                martksTextview.setText(classworkList.getDummy().get(textViewIndex).getMarks1());
//            }

                /*currentTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
        }



        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
        holder.t1.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}