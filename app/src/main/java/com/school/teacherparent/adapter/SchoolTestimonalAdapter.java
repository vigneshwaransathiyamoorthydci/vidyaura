package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.CircularList;
import com.school.teacherparent.models.DeleteCircularParms;
import com.school.teacherparent.models.SchoolDetailsModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import at.blogc.android.views.ExpandableTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 9/12/2018.
 */

public  class SchoolTestimonalAdapter extends RecyclerView.Adapter<SchoolTestimonalAdapter.MyViewHolder> {


    ArrayList<SchoolDetailsModel.testimonials>  circularListResponseArrayList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title,content;
        RatingBar ratingBar1;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            content = (TextView) view.findViewById(R.id.content);
            ratingBar1 = (RatingBar) view.findViewById(R.id.ratingBar1);

        }
    }







    public SchoolTestimonalAdapter(ArrayList<SchoolDetailsModel.testimonials> circularListResponseArrayList, Context context) {
        this.circularListResponseArrayList = circularListResponseArrayList;
        this.context =context;
       // this.baseActivity=baseActivity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_testimonal_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

//    public void setOnClickListen(AddTouchListener addTouchListen) {
//        this.addTouchListen = addTouchListen;
//    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final SchoolDetailsModel.testimonials circularListdata = circularListResponseArrayList.get(position);
        holder.title.setText("Testimonial by"+" "+circularListdata.getName());
        holder.content.setText(circularListdata.getTestimonial());
        holder.ratingBar1.setRating(circularListdata.getRating());
    }


    @Override
    public int getItemCount() {
        return circularListResponseArrayList.size();
    }


    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }

    public void hideProgress() {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.hideProgress();
        }
    }



}