package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.R;
import com.school.teacherparent.models.SelectedImageList;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import cn.jzvd.JzvdStd;

public class SelectedImageAdapter extends RecyclerView.Adapter<SelectedImageAdapter.MyViewHolder> {

    Context context;
    BitmapFactory.Options btmapOptions;
    private List<SelectedImageList> imagelist;
    OnImageRemoved onImageRemoved;

    public SelectedImageAdapter(List<SelectedImageList> imagelist, Context context, OnImageRemoved onImageRemoved) {
        this.imagelist = imagelist;
        this.context = context;
        this.onImageRemoved=onImageRemoved;
        btmapOptions = new BitmapFactory.Options();
        btmapOptions.inSampleSize = 2;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_selected_image, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d("uri", "onBindViewHolder: "+imagelist.get(position));

        if (imagelist.get(position).getImage().contains("amazonaws.com")){
            Picasso.get().load(imagelist.get(position).getImage()).into(holder.mainimage);
            Log.d("loadusr", "onBindViewHolder: url");
        }
        else {


            Bitmap bm = BitmapFactory.decodeFile(imagelist.get(position).getImage(),
                    btmapOptions);
            holder.mainimage.setImageBitmap(bm);
            Log.d("loadusr", "onBindViewHolder: bitmap");

        }

//
//        Uri uri =  Uri.parse(imagelist.get(position));
//        Picasso.get().load(uri).into(holder.mainimage);
        

    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mainimage, close;

        public MyViewHolder(final View view) {
            super(view);
            mainimage = view.findViewById(R.id.imagetemp);
            close = view.findViewById(R.id.close);

            close.bringToFront();
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageRemoved.onimageremoved(getAdapterPosition(),v);
                }
            });

        }
    }
}