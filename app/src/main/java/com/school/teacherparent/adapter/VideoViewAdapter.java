package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.joooonho.SelectableRoundedImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.VideoViewDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 11/15/2018.
 */


public class VideoViewAdapter extends RecyclerView.Adapter<VideoViewAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
    private List<VideoViewDTO> videoList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;

    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout header;
        ImageView playicon;
        SelectableRoundedImageView movie_image;
        public VideoView v;


        public MyViewHolder(View view) {
            super(view);
            movie_image=(SelectableRoundedImageView) view.findViewById(R.id.movie_image);
            header=view.findViewById(R.id.header);

           playicon=(ImageView)view.findViewById(R.id.playicon);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


    public VideoViewAdapter(List<VideoViewDTO> videoList,Context context) {
        this.videoList = videoList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_view_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        VideoViewDTO movie = videoList.get(position);

        holder.movie_image.setImageResource(Integer.parseInt(movie.getImg()));
       // holder.playicon.setImageResource(Integer.parseInt(movie.getPlay()));
      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/



        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return videoList.size();
    }
}
