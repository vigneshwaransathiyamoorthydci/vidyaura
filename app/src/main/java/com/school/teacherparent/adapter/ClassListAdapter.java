package com.school.teacherparent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;



import com.school.teacherparent.R;
import com.school.teacherparent.models.ListofClass;

import java.util.List;



public class ClassListAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;

    public ClassListAdapter(List<ListofClass> classResultsItemList, Context context) {
        this.classResultsItemList = classResultsItemList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    List<ListofClass> classResultsItemList;
    Context context;


    @Override
    public int getCount() {
        return classResultsItemList.size();
    }

    @Override
    public ListofClass getItem(int position) {
        return classResultsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.spinner_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textSpinnerItemOne.setText(classResultsItemList.get(position).getClassname()+"-"+classResultsItemList.get(position).getClasssection());
        return convertView;
    }

    static class ViewHolder {

        TextView textSpinnerItemOne;


        public ViewHolder(View convertView) {
            textSpinnerItemOne=convertView.findViewById(R.id.spinnerTarget);
        }
    }
}
