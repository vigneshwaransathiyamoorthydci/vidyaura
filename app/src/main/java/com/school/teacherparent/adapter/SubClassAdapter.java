package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ClassworkDetailsModel;
import com.school.teacherparent.models.DeleteClassworkParms;
import com.school.teacherparent.R;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 9/21/2018.
 */

public class SubClassAdapter extends RecyclerView.Adapter<SubClassAdapter.MyViewHolder> {

    private List<ClassworkDetailsModel.ClassworkDetail> subclassList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    BaseActivity baseActivity;

    public SubClassAdapter(List<ClassworkDetailsModel.ClassworkDetail> subclassList,Context context,BaseActivity baseActivity) {

        this.subclassList=subclassList;
        this.context=context;
        this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

    }

    /*public ClassAdapter(List<ClassDTO> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView counts,className;
        private ImageView menuImageview,subimg2;
        private CircularImageView classNameImgeview;
        private  TextView title,content,status,subtitle,descriptionTextview;
        public LinearLayout lin;
        RecyclerView image_recycle;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle=(TextView)view.findViewById(R.id.subtitle);
            content = (TextView) view.findViewById(R.id.content);
            menuImageview=(ImageView)view.findViewById(R.id.menuImageview);

            status=(TextView)view.findViewById(R.id.status);
            subimg2=(ImageView) view.findViewById(R.id.subimg2);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            image_recycle = view.findViewById(R.id.image_recycle);
            className=view.findViewById(R.id.className);
            classNameImgeview=(CircularImageView) view.findViewById(R.id.classNameImgeview);
            descriptionTextview=(TextView)view.findViewById(R.id.descriptionTextview);
            //image_recycle.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 3);
            image_recycle.setLayoutManager(mLayoutManager);
            //image_recycle.addItemDecoration(new DividerItemDecoration(context,  DividerItemDecoration.VERTICAL));



        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_class_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ClassworkDetailsModel.ClassworkDetail classworkDetail = subclassList.get(position);
        holder.title.setText(classworkDetail.getSubjectName());
        holder.content.setText(context.getString(R.string.topic_se)+" "+classworkDetail.getTopicDetails());
       // holder.img.setImageResource(Integer.parseInt(classworkDetail.getImg()));
      //  holder.subimg1.setImageResource(Integer.parseInt(classworkDetail.getSubimg1()));
      //  holder.subimg2.setImageResource(Integer.parseInt(classworkDetail.getSubimg2()));
        holder.subtitle.setText(classworkDetail.getChapterName().get(0).getChapter_name());
       // holder.status.setText(classworkDetail.getStatus1());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+subclassList.get(position).getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.classNameImgeview.setImageResource(Constants.imgid[position]);
       // holder.className.setText(classworkDetail.getClassName() + classworkDetail.getSection());
        holder.descriptionTextview.setText(context.getString(R.string.description)+": "+classworkDetail.getDescription());
        if (classworkDetail.getIsUserPostedClasswork()==0)
        {
            holder.menuImageview.setVisibility(View.GONE);
        }
        else
        {
            holder.menuImageview.setVisibility(View.VISIBLE);
        }
        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

        holder.menuImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "updateclasswork")
                                    .putExtra("classworkID", classworkDetail.getClassworkID()).putExtra("classID",classworkDetail.getClassID())
                                    .putExtra("subjectID",classworkDetail.getSubjectID()).putExtra("chapterID",classworkDetail.getChapter_id()).
                                            putExtra("topicID",classworkDetail.getTopic_id()).putExtra("sectionID",classworkDetail.getSectionID()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            baseActivity.showProgress();
                            DeleteClassworkParms deleteClassworkParms=new DeleteClassworkParms();
                            deleteClassworkParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            deleteClassworkParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            deleteClassworkParms.setClassworkID(String.valueOf(classworkDetail.getClassworkID()));
                            deleteClassworkParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));


                            vidyauraAPI.deleteClasswork(deleteClassworkParms).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    baseActivity.hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                //feedsFragment.getfeedList();
                                                subclassList.remove(position);
                                                notifyDataSetChanged();


                                            } else {
                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(context, SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(i);
                                            ((Activity)context).finish();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    baseActivity.hideProgress();
                                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });


                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }
                    }



                });

            }
        });

        ImageViewAdapter imageList = new ImageViewAdapter(classworkDetail.getAttachmentsList(), context,context.getString(R.string.s3_classwork_path));
        holder.image_recycle.setAdapter(imageList);

        imageList.setOnClickListen(new ImageViewAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Log.d("Vignesh","IMP"+classworkDetail.getAttachmentsList().get(position));
                initiatepopupwindow(classworkDetail.getAttachmentsList().get(position),"png", position, classworkDetail.getAttachmentsList());
            }
        });

    }

    @Override
    public int getItemCount() {
        return subclassList.size();
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }



    private void initiatepopupwindow(String mediapath, String media_type, int position, List<String>  imagelist) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd video_selected=alertLayout.findViewById(R.id.video_selected);;
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(context, 3, imagelist));
        viewPager.setCurrentItem(position);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();

        if (imagelist.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        /*if (media_type.equals("png")) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classwork_path)+"/"+mediapath;
            Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    imageView.setImageResource(R.mipmap.school);
                }
            });
        }*/

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }

}