package com.school.teacherparent.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.ListofClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;



public class CircularComposeclassAdpater extends BaseAdapter {

    private final LayoutInflater mInflater;
    List<ListofClass> classList;
    Context context;
    public ArrayList<String> arraylistDistrictId = new ArrayList<String>();
    public ArrayList<String> arraylistDistrictName = new ArrayList<String>();
    private boolean[] mSelected;

    public ArrayList<String> arraylistDistrictIdTest = new ArrayList<String>();

    public CircularComposeclassAdpater(List<ListofClass> classList, Context context) {
        this.classList = classList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arraylistDistrictIdTest = arraylistDistrictIdTest;
        mSelected = new boolean[classList.size()];


    }


    @Override
    public int getCount() {
        return classList.size();
    }

    @Override
    public ListofClass getItem(int position) {
        return classList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
       final TextView className = (TextView) convertView.findViewById(R.id.text_checkbox);
        className.setText(classList.get(position).getClassname() + " " + classList.get(position).getClasssection());
        final CheckBox checkbox_spinner_item_one = (CheckBox) convertView.findViewById(R.id.checkbox_spinner_item_one);

        if (classList.get(position).getSelectedID() == 0) {
            checkbox_spinner_item_one.setChecked(false);
        } else {
            checkbox_spinner_item_one.setChecked(true);
        }



        className.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classList.get(position).getSelectedID() == 0) {


                    classList.get(position).setSelectedID(classList.get(position).getClassid() + classList.get(position).getClasssectionid());
                    checkbox_spinner_item_one.setChecked(true);

                } else {
                    classList.get(position).setSelectedID(0);
                    checkbox_spinner_item_one.setChecked(false);
                }
            }
        });
        checkbox_spinner_item_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // mSelected[position] = isChecked;

                    classList.get(position).setSelectedID(classList.get(position).getClassid() + classList.get(position).getClasssectionid());

                } else {
                    classList.get(position).setSelectedID(0);
                }


            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return convertView;
    }

}




