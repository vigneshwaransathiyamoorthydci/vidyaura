package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.AttendanceList;
import com.school.teacherparent.models.AttendanceListResponse;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by keerthana on 10/10/2018.
 */


public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {


    private Context context;
    ArrayList<AttendanceList> attendanceListArrayList;

    public AddTouchListen addTouchListen;
    public AttendanceAdapter(ArrayList<AttendanceList> attendanceListArrayList, Context context) {
        this.context=context;
        this.attendanceListArrayList=attendanceListArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img1,img2,img3;
        private SwitchCompat switch_compat;
        private TextView name,noofleavecount,noofabsentcount,absentTextview,leaveTextview,present_count,absent_count;
        private RelativeLayout relative_class_spinner,relative_sub_spinner;
        public TextView title, subtitle, time,stu_counts,class_text;
        ImageView img,present_img,absent_img;
        CircularImageView subjectImage;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content,className;
        private Spinner sub_spinner,class_spinner;

        public MyViewHolder(View view) {
            super(view);
            img=(ImageView) view.findViewById(R.id.img);
            img1=(ImageView) view.findViewById(R.id.img1);
            img2=(ImageView) view.findViewById(R.id.img2);
            img3=(ImageView) view.findViewById(R.id.img3);
            noofleavecount=(TextView)view.findViewById(R.id.leavecount);
            noofabsentcount=(TextView)view.findViewById(R.id.absentcount);

            present_count=(TextView)view.findViewById(R.id.present_count);
            absent_count=(TextView)view.findViewById(R.id.absent_count);
            present_img=(ImageView) view.findViewById(R.id.present_img);
            absent_img=(ImageView) view.findViewById(R.id.absent_img);
            stu_counts=(TextView)view.findViewById(R.id.stu_counts);
            class_text=(TextView)view.findViewById(R.id.class_text);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            subjectImage=(CircularImageView)view.findViewById(R.id.subjectImage);
            className=(TextView) view.findViewById(R.id.className);
            absentTextview=(TextView)view.findViewById(R.id.absentTextview);
            leaveTextview=(TextView)view.findViewById(R.id.leaveTextview);

        }

    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attendance_fragment_item, parent, false);



        return new MyViewHolder(itemView);
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AttendanceList attendanceList = attendanceListArrayList.get(position);

        String url;
        if (attendanceList.isGroupAttendance()) {
            holder.class_text.setText(attendanceList.getIconName());
            url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+attendanceListArrayList.get(position).getGroupimage();
        } else {
            holder.class_text.setText(attendanceList.getClassName()+""+attendanceList.getSection());
            url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+attendanceListArrayList.get(position).getClass_image();
        }

        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.subjectImage.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.subjectImage.setImageResource(Constants.imgid[position]);
        //holder.className.setText(attendanceList.getIconName()+attendanceList.getSection());
        holder.stu_counts.setText(attendanceList.getTotalStudentsCount()+" "+context.getString(R.string.students));
        int noofleave=0,noofabsent=0;
        for (int y=0;y<attendanceListArrayList.get(position).getTodaysAbsenteesList().size();y++)

        {
            if (attendanceListArrayList.get(position).getTodaysAbsenteesList().get(y).getAttendance_type()==null)
            {
                noofabsent=noofabsent+1;
            }
            else if (attendanceListArrayList.get(position).getTodaysAbsenteesList().get(y).getAttendance_type().equals("Leave"))
            {
                noofleave=noofleave+1;

            }
            else if (attendanceListArrayList.get(position).getTodaysAbsenteesList().get(y).getAttendance_type().equals("OD"))
            {
                noofleave=noofleave+1;
            }
            else
            {
                noofabsent=noofabsent+1;
            }
        }
        holder.noofleavecount.setText(""+noofleave);
        holder.noofabsentcount.setText(""+noofabsent);

//        holder.text1.setText(movie.getText1());
//        holder.text2.setText(movie.getText2());
        holder.present_count.setText(attendanceList.getStudentsPresentCount());
        holder.absent_count.setText(attendanceList.getStudentsAbsentCount());

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return attendanceListArrayList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}
