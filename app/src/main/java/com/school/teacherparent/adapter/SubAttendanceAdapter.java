package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.models.AttendancedetailsResponse;
import com.school.teacherparent.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;


/**
 * Created by keerthana on 10/11/2018.
 */


public class SubAttendanceAdapter extends RecyclerView.Adapter<SubAttendanceAdapter.MyViewHolder> {


    private Context context;
    public AddTouchListen addTouchListen;
    ArrayList<AttendancedetailsResponse.attendanceDetailsList> attendanceDetailsListArrayList;
    int sessionTYPE;



    public SubAttendanceAdapter(ArrayList<AttendancedetailsResponse.attendanceDetailsList> attendanceDetailsListArrayList, Context context, int sessionTYPE) {
        this.context=context;
        this.attendanceDetailsListArrayList=attendanceDetailsListArrayList;
        this.sessionTYPE=sessionTYPE;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView stu_name,percentage;;
        private ImageView stu_img, subimg1, statusImageview;
        public LinearLayout lin;
        ToggleSwitch toggleSwitch;
        private CircularImageView classNameImgeview;
        public MyViewHolder(View view) {
            super(view);
          // stu_img=(ImageView)view.findViewById(R.id.stu_img);
           stu_name=(TextView)view.findViewById(R.id.stu_name);
           percentage=(TextView)view.findViewById(R.id.percentage);
           lin=(LinearLayout)view.findViewById(R.id.lin);
            statusImageview=(ImageView)view.findViewById(R.id.statusImageview);
            toggleSwitch=(ToggleSwitch)view.findViewById(R.id.toggleSwitch);
            ArrayList<String> labels = new ArrayList<>();
            labels.add("P");
            labels.add("A");
            toggleSwitch.setLabels(labels);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_attendance_fragment_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen = addTouchListen;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AttendancedetailsResponse.attendanceDetailsList attendanceDetailsList = attendanceDetailsListArrayList.get(position);
       /* holder.stu_img.setImageResource(Integer.parseInt(movie.getStu_img()));
        holder.status.setImageResource(Integer.parseInt(movie.getStatus()));*/
        holder.percentage.setText(attendanceDetailsList.getAttendancePercentage()+"%");
        holder.stu_name.setText(attendanceDetailsList.getFname()+" "+attendanceDetailsList.getLname());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path)+"/"+
                attendanceDetailsList.getStudent_photo();
        Picasso.get().load(url).placeholder(R.drawable.ic_user).into(holder.classNameImgeview, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.ic_user);
            }
        });
        if (attendanceDetailsList.getStudentsList().get(0).getAttendance_type().equals("OD"))
        {
            holder.statusImageview.setImageResource(R.mipmap.onduty_icon);
        } else if (attendanceDetailsList.getStudentsList().get(0).getAttendance_type().equals("Leave")) {

            if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 0 &&
                    attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 0) {
                holder.statusImageview.setImageResource(R.mipmap.leave_icons);
            } else {
                if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 0
                        && attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 1) {
                        holder.statusImageview.setImageResource(R.mipmap.lp_icon);
                } else if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 1
                        && attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 0)

                {
                    holder.statusImageview.setImageResource(R.mipmap.pl_icon);
                }/* else if (attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 0) {
                    holder.statusImageview.setImageResource(R.mipmap.pa_icon);
                }*/


            }

        }
        else {
            if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 1 &&
                    attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 1) {
                holder.statusImageview.setImageResource(R.mipmap.present_icon);
            } else {
                if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 0
                        && attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 0) {
                    holder.statusImageview.setImageResource(R.mipmap.absent_icon);
                } else if (attendanceDetailsList.getStudentsList().get(0).getForenoon_session() == 0)

                {
                    holder.statusImageview.setImageResource(R.mipmap.ap_icon);
                } else if (attendanceDetailsList.getStudentsList().get(0).getAfternoon_session() == 0) {
                    holder.statusImageview.setImageResource(R.mipmap.pa_icon);
                }


            }
        }

        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return attendanceDetailsListArrayList.size();
    }

    public interface AddTouchListen {
        public void OnTouchClick(int position);

    }
}