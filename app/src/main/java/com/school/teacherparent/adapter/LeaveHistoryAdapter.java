package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.AttendanceListResponse;
import com.school.teacherparent.models.LeaveHistoryListResponse;
import com.school.teacherparent.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Callback;

/**
 * Created by keerthana on 10/10/2018.
 */


public class LeaveHistoryAdapter extends RecyclerView.Adapter<LeaveHistoryAdapter.MyViewHolder> {


    private Context context;
    ArrayList<LeaveHistoryListResponse.leaveHistory>  leaveHistoryArrayList=new ArrayList<>();


    public LeaveHistoryAdapter(Context context, ArrayList<LeaveHistoryListResponse.leaveHistory>  leaveHistoryArrayList) {
        this.context=context;
        this.leaveHistoryArrayList=leaveHistoryArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView leavedateTextview,leaveStatusTextview,leavereasonTextview;


        public MyViewHolder(View view) {
            super(view);

            leavedateTextview=(TextView)view.findViewById(R.id.leavedateTextview);
            leaveStatusTextview=(TextView)view.findViewById(R.id.leaveStatusTextview);

            leavereasonTextview=(TextView)view.findViewById(R.id.leavereasonTextview);


        }

    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_history_list_item, parent, false);



        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        LeaveHistoryListResponse.leaveHistory leaveHistory = leaveHistoryArrayList.get(position);
        holder.leavedateTextview.setText(getDateFormatforhomework(leaveHistory.getFrom_date()));
        holder.leaveStatusTextview.setText(leaveHistory.getLeave_status());
        holder.leavereasonTextview.setText(leaveHistory.getReason());



    }

    @Override
    public int getItemCount() {
        return leaveHistoryArrayList.size();
    }

    public String getDateFormatforhomework(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}
