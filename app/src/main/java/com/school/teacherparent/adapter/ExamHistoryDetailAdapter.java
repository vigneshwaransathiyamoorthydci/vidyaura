package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ExamHistorydetailList;
import com.school.teacherparent.models.UpcomingExamdetailList;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by keerthana on 10/8/2018.
 */


public class ExamHistoryDetailAdapter extends RecyclerView.Adapter<ExamHistoryDetailAdapter.MyViewHolder> {

    ArrayList<ExamHistorydetailList> upcomingExamdetailLists;
    private Context context;



    public ExamHistoryDetailAdapter(ArrayList<ExamHistorydetailList> upcomingExamdetailLists, Context context) {
        this.upcomingExamdetailLists = upcomingExamdetailLists;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  LinearLayout footer;
        public ImageView img,menu;
        CircularImageView classNameImgeview;
       public TextView subject,syllabus,topic,date,tvStartTime,desc,className,tvEndTime;

        public MyViewHolder(View view) {
            super(view);
            subject = (TextView) view.findViewById(R.id.subject);
            tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
            tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
            img=(ImageView)view.findViewById(R.id.img);
            date=(TextView)view.findViewById(R.id.date);
            desc=(TextView)view.findViewById(R.id.desc);
            syllabus=(TextView)view.findViewById(R.id.syllabus);
            topic=(TextView)view.findViewById(R.id.topic);
            footer=(LinearLayout)view.findViewById(R.id.footer);
            menu=(ImageView)view.findViewById(R.id.menu);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);
            className=view.findViewById(R.id.className);
        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ExamHistorydetailList movie = upcomingExamdetailLists.get(position);

        String url = context.getString(R.string.s3_baseurl) +
                context.getString(R.string.s3_classworksection_path)+"/"+upcomingExamdetailLists.get(position).getClassImage();
        System.out.println("url ==> "+url);
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.classNameImgeview.setImageResource(Constants.imgid[position]);
       // holder.className.setText(movie.getClassName());
        holder.tvStartTime.setText(getTimeFormat(movie.getStart_time()));
        holder.tvEndTime.setText(getTimeFormat(movie.getEnd_time()));
        //holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.date.setText(getDateFormat(movie.getStart_time()));
        holder.subject.setText(movie.getName());
        holder.desc.setVisibility(View.GONE);
        if (movie.getChapterName().size()>0) {
            for (int y=0;y<movie.getChapterName().size();y++)
            {
                holder.syllabus.setText(movie.getChapterName().get(y).getChapter_name());
            }

        }
        else
        {
            holder.syllabus.setText("No chapter");
        }

        if (movie.getTopicName().size()>0) {
            for (int y=0;y<movie.getTopicName().size();y++)
            {
                holder.topic.setText(movie.getTopicName().get(y).getTopic_name());
            }

        }
        else
        {
            holder.topic.setText("No Topic");
        }

        if (movie.getIsUserPostedExam()==1)
        {
            holder.menu.setVisibility(View.GONE );
        }
        else
        {
            holder.menu.setVisibility(View.GONE);
        }




    }

    @Override
    public int getItemCount() {
        return upcomingExamdetailLists.size();
    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String getTimeFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}