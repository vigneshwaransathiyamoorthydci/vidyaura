package com.school.teacherparent.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

public class NewsImageSliderAdapter extends PagerAdapter {


    private List<String> newArrayList;
    private LayoutInflater inflater;
    private Context context;


    public NewsImageSliderAdapter(Context context, List<String> newList) {
        this.context = context;
        this.newArrayList = newList;
        inflater = LayoutInflater.from(context);
        notifyDataSetChanged();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return newArrayList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.news_image_slider_item, view, false);

        assert imageLayout != null;
        final ImageView ivNewsImage = imageLayout.findViewById(R.id.iv_news_image);

        //ivNewsImage.setImageResource(newArrayList.get(position));

        Glide.with(context).load(context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_schools)+newArrayList.get(position)).into(ivNewsImage);

        view.addView(imageLayout, 0);


        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
