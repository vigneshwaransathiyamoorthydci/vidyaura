package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.EventParentListActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.DeleteEventParmas;
import com.school.teacherparent.models.EventList;
import com.school.teacherparent.models.EventListParms;
import com.school.teacherparent.models.EventParentListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import at.blogc.android.views.ExpandableTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keerthana on 1/29/2019.
 */

public class EventsParentAdapter extends RecyclerView.Adapter<EventsParentAdapter.MyViewHolder> {


    public   RelativeLayout accepted_layout;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    EventParentListActivity eventParentListActivity;
    ArrayList<EventList> eventsListArrayList;
    private Context context;
    AddTouchListener addTouchListen;
    ArrayList<String> invitesArray;
    Activity activity;
    BaseActivity baseActivity;
    private String text;
    public TextView AcceptedTextview;
    private EventParentListResponse eventparentListResponse;
    int stt;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView img,menu,invite_iconImageview;
        public TextView desc,date,time,inviteesTextviewtop,inviteesTextviewtoplabel,venueTextview;
        public TextView dateTextview,AcceptedTextview,maybeTextview,declineTextview;
        ExpandableTextView expandableTextView;
        LinearLayout invitesLinearLayout,eventpolls;
        ListView invitesListview;
        public RelativeLayout accepted_layout,maybe_layout,declined_layout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            dateTextview=(TextView)view.findViewById(R.id.dateTextview);
            img=(ImageView) view.findViewById(R.id.img);
            menu=(ImageView) view.findViewById(R.id.menu);
            date=(TextView)view.findViewById(R.id.date);
            time=(TextView)view.findViewById(R.id.time);
            expandableTextView =(ExpandableTextView) view.findViewById(R.id.expandableTextViewDescription);
            expandableTextView.setInterpolator(new OvershootInterpolator());
            invitesLinearLayout=(LinearLayout)view.findViewById(R.id.inviteesLinear);
            inviteesTextviewtop=(TextView)view.findViewById(R.id.inviteesTextviewtop);
            inviteesTextviewtoplabel=(TextView)view.findViewById(R.id.inviteesTextviewtoplabel);
            invitesListview=(ListView)view.findViewById(R.id.invitesListview);
            venueTextview=(TextView)view.findViewById(R.id.venueTextview);
            eventpolls=(LinearLayout)view.findViewById(R.id.eventpolls);
            AcceptedTextview=(TextView)view.findViewById(R.id.AcceptedTextview);
            accepted_layout=(RelativeLayout)view.findViewById(R.id.accepted_layout);
            maybe_layout=(RelativeLayout)view.findViewById(R.id.maybe_layout);
            declined_layout=(RelativeLayout)view.findViewById(R.id.declined_layout);
            maybeTextview=(TextView)view.findViewById(R.id.maybeTextview);
            declineTextview=(TextView)view.findViewById(R.id.declineTextview);
            invite_iconImageview=(ImageView)view.findViewById(R.id.invite_iconImageview);
            eventpolls=(LinearLayout) view.findViewById(R.id.eventpolls);

        }
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    public EventsParentAdapter(ArrayList<EventList> eventsListArrayList, Context context, EventParentListActivity eventParentListActivity, Activity activity) {
        this.eventsListArrayList = eventsListArrayList;
        this.context =context;
        invitesArray=new ArrayList<>();
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        this.activity = activity;
        this.eventParentListActivity=eventParentListActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final EventList eventlistDate = eventsListArrayList.get(position);
        holder.title.setText(eventlistDate.getTitle());
        holder.desc.setText(eventlistDate.getDescription());
        holder.venueTextview.setText(eventlistDate.getVenue());
        holder.dateTextview.setText(getDateFormat(eventlistDate.getEvent_date()));
//        holder.img.setImageResource(Integer.parseInt(eventlistDate.getImg()));
        holder.date.setText(getDateFormat(eventlistDate.getEvent_date()));
        holder.time.setText(getTimeFormat(eventlistDate.getEvent_date()));
        holder.expandableTextView.setText(eventlistDate.getDescription());

        final String error_img = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_schools) + sharedPreferences.getString(Constants.SCHOOL_LOGO, "");
        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        if (eventsListArrayList.get(position).getUserDetails().size() > 0) {
            final String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + eventsListArrayList.get(position).getUserDetails().get(0).getEmp_photo();
            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.img);
            /*  Picasso.get().load(url).
                    into(holder.img, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Picasso.get().load(error).into(holder.img);

                        }
                    });*/
        } else {
            Glide.with(context.getApplicationContext()).load(error_img).apply(error).into(holder.img);
        }

        if (eventlistDate.getEventAttendeesStatus() == 1) {
            holder.AcceptedTextview.setText("ACCEPTED");
            holder.maybeTextview.setText("MAYBE");
            holder.declineTextview.setText("DECLINE");
        } else if (eventlistDate.getEventAttendeesStatus() == 2) {
            holder.AcceptedTextview.setText("ACCEPT");
            holder.maybeTextview.setText("MAYBE");
            holder.declineTextview.setText("DECLINE");
        } else if (eventlistDate.getEventAttendeesStatus() == 3) {
            holder.AcceptedTextview.setText("ACCEPT");
            holder.maybeTextview.setText("MAYBE");
            holder.declineTextview.setText("DECLINED");
        } else {
            holder.AcceptedTextview.setText("ACCEPT");
            holder.maybeTextview.setText("MAYBE");
            holder.declineTextview.setText("DECLINE");
        }

        /*holder.eventpolls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    if(position==0){
                    addTouchListen.onTouchClick(position,holder.AcceptedTextview,holder.accepted_layout);

                   // AcceptedTextview.setTextColor(v.getResources().getColor(R.color.white));
                }}
            }
        });*/
        holder.accepted_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addTouchListen.onTouchClick(position,holder.AcceptedTextview,holder.accepted_layout);
                int success = eventParentListActivity.pollForEventsAPI((eventsListArrayList.get(position).getEventID()), "Accepted",holder.AcceptedTextview,1);
                if (success == 200) {
                    holder.AcceptedTextview.setText("ACCEPTED");
                    holder.maybeTextview.setText("MAYBE");
                    holder.declineTextview.setText("DECLINE");
                }
            }
        });
        holder.maybe_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addTouchListen.onTouchClick(position,holder.maybeTextview,holder.maybe_layout);
                int success = eventParentListActivity.pollForEventsAPI((eventsListArrayList.get(position).getEventID()), "Maybe",holder.maybeTextview,2);
                if (success == 200) {
                    holder.AcceptedTextview.setText("ACCEPT");
                    holder.maybeTextview.setText("MAYBE");
                    holder.declineTextview.setText("DECLINE");
                }
            }
        });
        holder.declined_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addTouchListen.onTouchClick(position,holder.declineTextview,holder.declined_layout);
                int success = eventParentListActivity.pollForEventsAPI((eventsListArrayList.get(position).getEventID()), "Declined",holder.declineTextview,3);
                if (success == 200) {
                    holder.AcceptedTextview.setText("ACCEPT");
                    holder.maybeTextview.setText("MAYBE");
                    holder.declineTextview.setText("DECLINED");
                }
            }
        });
        holder.invitesLinearLayout.setVisibility(View.GONE);
        if (eventlistDate.getIsUserPostedEvent()==0)
        {
            holder.menu.setVisibility(View.GONE);
        }
        else
        {
            holder.menu.setVisibility(View.VISIBLE);
        }
        if (eventlistDate.getRSVP()==0)
        {
            holder.eventpolls.setVisibility(View.GONE);
        }
        else
        {

            //holder.AcceptedTextview.setText(""+eventlistDate.getEventAcceptedCount()+" "+context.getString(R.string.accepted));
            //holder.maybeTextview.setText(""+eventlistDate.getEventMaybeCount()+" "+context.getString(R.string.maybe));
            //holder.declineTextview.setText(""+eventlistDate.getEventDeclinedCount()+" "+context.getString(R.string.declined));
        }
        EventInviteAdapter eventsInvitesAdapter=new EventInviteAdapter(context,eventlistDate.getClassNames(),2);
        holder.invitesListview.setAdapter(eventsInvitesAdapter);
//        holder.menu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                final PopupMenu popup = new PopupMenu(context, holder.menu);
//                //inflating menu_icon from xml resource
//                popup.inflate(R.menu.comment_item_menu);
//                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
//                {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        switch (item.getItemId()) {
//                            case R.id.edit:
//                                //handle menu1 click
//                                    popup.dismiss();
//                                context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "events").putExtra("eventID", eventlistDate.getEventID()));
//                                return true;
//                            case R.id.delete:
//                                //handle menu2 click
//                                popup.dismiss();
//                                if (Util.isNetworkAvailable()) {
//
//                                    if (activity instanceof BaseActivity) {
//                                        baseActivity = (BaseActivity) activity;
//                                    }
//                                    baseActivity.showProgress();
//                                    DeleteEventParmas deleteEventParmas=new DeleteEventParmas();
//                                    deleteEventParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
//                                    deleteEventParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
//                                    deleteEventParmas.setEventID(String.valueOf(eventlistDate.getEventID()));
//                                    deleteEventParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
//                                    vidyauraAPI.deleteEvent(deleteEventParmas).enqueue(new Callback<AddFeedResponse>() {
//                                        @Override
//                                        public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
//                                            baseActivity.hideProgress();
//                                            if (response.body()!=null)
//                                            {
//                                                AddFeedResponse feedLikeResponse=response.body();
//                                                if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//
//
//                                                    if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {
//
//                                                        Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                                                        //feedsFragment.getfeedList();
//                                                        eventsListArrayList.remove(position);
//                                                        notifyDataSetChanged();
//
//
//                                                    } else {
//                                                        Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                                                    }
//
//                                                }
//                                                else
//                                                {
//                                                    Intent i = new Intent(context, SecurityPinActivity.class);
//                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                                    context.startActivity(i);
//                                                    ((Activity)context).finish();
//                                                }
//                                            }
//                                            else
//                                            {
//                                                Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onFailure(Call<AddFeedResponse> call, Throwable t) {
//                                            baseActivity.hideProgress();
//                                            Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//
//                                }
//
//
//
//
//                                else
//                                {
//                                    Toast.makeText(context,context.getString(R.string.PleaseheckyourNetwork),Toast.LENGTH_SHORT).show();
//
//                                }
//                                return true;
//
//                            default:
//                                return false;
//                        }
//                    }
//                });
//                //displaying the popup
//                popup.show();
//            }
//
//        });

        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        eventParentListActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(eventParentListActivity, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "events").putExtra("eventID", eventlistDate.getEventID()));
                        dialog.dismiss();

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Util.isNetworkAvailable()) {

                            if (activity instanceof BaseActivity) {
                                baseActivity = (BaseActivity) activity;
                            }
                            baseActivity.showProgress();
                            DeleteEventParmas deleteEventParmas=new DeleteEventParmas();
                            deleteEventParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            deleteEventParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            deleteEventParmas.setEventID(String.valueOf(eventlistDate.getEventID()));
                            deleteEventParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
                            vidyauraAPI.deleteEvent(deleteEventParmas).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    baseActivity.hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!= Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                //feedsFragment.getfeedList();
                                                eventsListArrayList.remove(position);
                                                notifyDataSetChanged();


                                            } else {
                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(context, SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(i);
                                            ((Activity)context).finish();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    baseActivity.hideProgress();
                                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }




                        else
                        {
                            Toast.makeText(context,context.getString(R.string.PleaseheckyourNetwork),Toast.LENGTH_SHORT).show();

                        }


                        dialog.dismiss();
                    }



                });

            }
        });

        if (eventlistDate.getClassNames().size()>0) {
            holder.inviteesTextviewtop.setText(""+eventlistDate.getClassNames().size() + " " + context.getString(R.string.classes));
        }
/*
//expandable list
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.expandableTextView.toggle();
                if (holder.expandableTextView.isExpanded())
                {
                    holder.invitesLinearLayout.setVisibility(View.GONE);
                    holder.inviteesTextviewtop.setVisibility(View.VISIBLE);
                    holder.inviteesTextviewtoplabel.setVisibility(View.VISIBLE);
                    holder.invite_iconImageview.setVisibility(View.VISIBLE);


                }
                else
                {
                    holder.invitesLinearLayout.setVisibility(View.VISIBLE);
                    holder.inviteesTextviewtop.setVisibility(View.GONE);
                    holder.inviteesTextviewtoplabel.setVisibility(View.GONE);
                    holder.invite_iconImageview.setVisibility(View.GONE);
                }
            }
        });
*/

        holder.invitesListview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

        });
    }

    public int pollForEventsAPI(int eventsID, String eventAttendancestatus, final TextView textView, final int state) {
        if (Util.isNetworkAvailable()) {
            //loading=false;
            //showProgress();
            EventListParms eventListParms=new EventListParms();
            eventListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            eventListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            eventListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            eventListParms.setEventsID(String.valueOf(eventsID));
            eventListParms.setEventAttendeesStatus(eventAttendancestatus);
            vidyauraAPI.pollsforevents(eventListParms).enqueue(new Callback<EventParentListResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<EventParentListResponse> call, Response<EventParentListResponse> response) {
                    //hideProgress();

                    if (response.body() != null) {
                        eventparentListResponse = response.body();
                        if (eventparentListResponse.getStatus()!= Util.STATUS_TOKENEXPIRE) {
                            // eventsListArrayList.clear();
                            if (eventparentListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                stt = eventparentListResponse.getStatus();
                                Toast.makeText(context,eventparentListResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                if (state == 1){
                                    textView.setText("ACCEPTED");
                                } else if (state == 2) {
                                    textView.setText("MAYBE");
                                } else if (state == 3) {
                                    textView.setText("DECLINED");
                                }
                            }
                            else {
                                //loading=false;

                                Toast.makeText(context, eventparentListResponse.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                        else
                        {
                            //loading=false;
                            Intent i = new Intent(context, SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(i);
                            //finishAffinity();

                        }

                    }
                    else
                    {
                        //loading=false;
                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<EventParentListResponse> call, Throwable t) {
                    //hideProgress();
                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
        return stt;
    }

    public interface AddTouchListener {
        void onTouchClick(int position, TextView text, RelativeLayout accepted_layout);
    }
    @Override
    public int getItemCount() {

        return eventsListArrayList.size();
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getTimeFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}