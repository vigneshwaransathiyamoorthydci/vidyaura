package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.R;
import com.school.teacherparent.models.DiaryClassworkList;
import com.school.teacherparent.models.DiaryHomeworkList;
import com.school.teacherparent.models.DiaryListResponse;
import com.school.teacherparent.models.FaqResponse;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 9/11/2018.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.ViewHolder> {

    private List<FaqResponse.getFAQDetails> diaryDetailsLists;
    private Context context;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView question_name, answer_name;


        public ViewHolder(View view) {
            super(view);
            question_name= (TextView) view.findViewById(R.id.faqquestionTextview);
            answer_name= (TextView) view.findViewById(R.id.faqanswerTextview);



        }
    }



    public FaqAdapter(ArrayList<FaqResponse.getFAQDetails> diaryDetailsLists, Context context) {
        this.diaryDetailsLists = diaryDetailsLists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faq_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        FaqResponse.getFAQDetails detailsList=diaryDetailsLists.get(position);
        holder.question_name.setText(detailsList.getQuestion());
        holder.answer_name.setText(detailsList.getAnswer());



    }

    @Override
    public int getItemCount() {
        //return showing.size();
        return diaryDetailsLists.size();
    }


}

