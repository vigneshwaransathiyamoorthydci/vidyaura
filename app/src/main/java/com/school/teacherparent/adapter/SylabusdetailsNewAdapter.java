package com.school.teacherparent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.SyllabusDetailsModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/4/2018.
 */


public class SylabusdetailsNewAdapter extends RecyclerView.Adapter<SylabusdetailsNewAdapter.MyViewHolder> {

    private List<SyllabusDetailsModel.SyllabusDetailsList> resultList = new ArrayList<>();
    private Context context;
    private List<SyllabusDetailsModel.SyllabusList> TopicList = new ArrayList<>();
    SylabusdetailsTopicNewAdapter sylabusdetailsTopicNewAdapter;

    public SylabusdetailsNewAdapter(List<SyllabusDetailsModel.SyllabusDetailsList> restuList, Context context) {
        this.resultList = restuList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student_syllabus_new, parent, false);
        return new MyViewHolder(itemView);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        TopicList.clear();
        SyllabusDetailsModel.SyllabusDetailsList movie = resultList.get(position);
        holder.sylabus_term.setText(movie.getTermName());
        System.out.println("hiiiiii"+movie.getTermName()+"-"+movie.getSyllabusList());
        TopicList.addAll(movie.getSyllabusList());
        sylabusdetailsTopicNewAdapter = new SylabusdetailsTopicNewAdapter(TopicList, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rv_chapter.setLayoutManager(mLayoutManager);
        holder.rv_chapter.setItemAnimator(new DefaultItemAnimator());
        holder.rv_chapter.setAdapter(sylabusdetailsTopicNewAdapter);

    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView sylabus_term;
        private ImageView iv_syllabus_term;
        private RecyclerView rv_chapter;


        public MyViewHolder(View view) {
            super(view);

            context = itemView.getContext();
            iv_syllabus_term = (ImageView) view.findViewById(R.id.iv_syllabus_term);
            sylabus_term = (TextView) view.findViewById(R.id.sylabus_term);
            rv_chapter = (RecyclerView) view.findViewById(R.id.recycle_chapter);

        }
    }


}