package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.TimeTableModel.TimetableList;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 9/26/2018.
 */

public class TimeTableforparentsAdapter extends RecyclerView.Adapter<TimeTableforparentsAdapter.MyViewHolder> {

    private List<TimetableList> timeList=new ArrayList<>();
    private Context context;

    public TimeTableforparentsAdapter(List<TimetableList> timeList, Context context) {
        this.timeList=timeList;
        this.context=context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView classtext, subtext, time,period,className;
        ImageView classnameImageview;
        private CircularImageView classNameImgeview;

        public MyViewHolder(View view) {
            super(view);
            classtext = (TextView) view.findViewById(R.id.class_text);
            time = (TextView) view.findViewById(R.id.time);
            //classnameImageview=(ImageView)view.findViewById(R.id.classNameImgeview);
            subtext=(TextView)view.findViewById(R.id.sub_text);
            period=(TextView)view.findViewById(R.id.period);
            className=(TextView)view.findViewById(R.id.className);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);

        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_table_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TimetableList movie = timeList.get(position);

        if (movie.getBreakTiming()!=null && movie.getBreakTiming().size()>0) {
            holder.time.setText(removeLastChar(movie.getBreakTiming().get(0).getStart_time()) + "-" + removeLastChar(movie.getBreakTiming().get(0).getEnd_time()));
        }
        if (movie.getSubjectID()==0)
        {
            holder.classtext.setText(movie.getPeriod());
            holder.period.setText("");
            holder.period.setVisibility(View.GONE);
            holder.subtext.setVisibility(View.GONE);
            if (movie.getPeriod().toLowerCase().contains("lunch"))
            {
                holder.classNameImgeview.setImageResource(R.drawable.ic_lunch);
            }
            else
            {
                holder.classNameImgeview.setImageResource(R.drawable.ic_coffee);
            }

        }
        else
        {
            holder.classtext.setText(movie.getAlt_name()+movie.getSection());
            holder.period.setText(context.getString(R.string.peroid)+movie.getPeriod());
            holder.subtext.setText(movie.getName());

            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+
                    movie.getClass_image();
            Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.classNameImgeview.setImageResource(R.drawable.nopreview);
                }
            });
        }


       // holder.classnameImageview.setImageResource(Constants.imgid[position]);
        //holder.className.setText(movie.getClassName() + movie.getSection());
      //  holder.img.setImageResource(Integer.parseInt(movie.getImg()));


    }
    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 2);
    }
    @Override
    public int getItemCount() {
        return timeList.size();
    }
}