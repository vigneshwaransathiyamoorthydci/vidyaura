package com.school.teacherparent.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.PollsViewFragment;
import com.school.teacherparent.models.PollsListParms;
import com.school.teacherparent.models.PollsResultListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 10/1/2018.
 */

public class PollsViewListAdapter extends RecyclerView.Adapter<PollsViewListAdapter.MyViewHolder> {

    ArrayList<PollsResultListResponse.getPolls.pollList> pollsResultListResponses;
    private Context context;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    PollsViewFragment pollsViewFragment;

    PollsResultListResponse pollsResultListResponse;
    private AddTouchListen addTouchListen;

    public PollsViewListAdapter(Context context, ArrayList<PollsResultListResponse.getPolls.pollList> pollsResultListResponses, PollsViewFragment pollsViewFragment) {
        this.context = context;
        this.pollsResultListResponses = pollsResultListResponses;
        this.pollsViewFragment = pollsViewFragment;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_polls_view, parent, false);
        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.rbOption1.setTypeface(ProximaNovaFont.getInstance(context).getSemiBoldTypeFace());
        holder.rbOption2.setTypeface(ProximaNovaFont.getInstance(context).getSemiBoldTypeFace());
        holder.rbOption3.setTypeface(ProximaNovaFont.getInstance(context).getSemiBoldTypeFace());
        holder.rbOption4.setTypeface(ProximaNovaFont.getInstance(context).getSemiBoldTypeFace());
        holder.rbOption5.setTypeface(ProximaNovaFont.getInstance(context).getSemiBoldTypeFace());

        PollsResultListResponse.getPolls.pollList pollList = pollsResultListResponses.get(position);
        holder.tvPollsQuestion.setText(pollList.getQuestion());


        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= null;
        try {
            newDate = spf.parse(pollList.getPublish_date());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("MMMM yyyy");
        String date = spf.format(newDate);

        holder.tvDate.setText(date);

        if(pollList.getIsParentVoted().size()>0){

            holder.rgOptionVote.setVisibility(View.GONE);
            holder.btnSubmit.setVisibility(View.GONE);
            holder.ltVoted.setVisibility(View.VISIBLE);

            if(pollList.getPollsOptions().size() == 1){

                holder.ltSeekOption1.setVisibility(View.VISIBLE);

                //set Option Color
                if (pollList.getPollsOptions().get(0).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName1.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.black));
                }

                //set Option
                holder.tvPollsName1.setText(pollList.getPollsOptions().get(0).getOptions());

                //set OptionPercentage
                holder.tvPollsPercent1.setText(pollList.getPollsOptions().get(0).getOptionsPercentage()+"%");

                int pollsPercent=(int) Double.parseDouble(pollList.getPollsOptions().get(0).getOptionsPercentage());

                holder.sbPolls1.setProgress(pollsPercent);

                //set OptionPercentage Color
                if(pollsPercent <= 25 ){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent <= 50){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent <= 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent > 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

            }else if(pollList.getPollsOptions().size() == 2){

                holder.ltSeekOption1.setVisibility(View.VISIBLE);
                holder.ltSeekOption2.setVisibility(View.VISIBLE);

                //set Option Color
                if (pollList.getPollsOptions().get(0).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName1.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());

                } else {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(1).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName2.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.black));
                }

                //set Option
                holder.tvPollsName1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.tvPollsName2.setText(pollList.getPollsOptions().get(1).getOptions());

                //set OptionPercentage
                holder.tvPollsPercent1.setText(pollList.getPollsOptions().get(0).getOptionsPercentage()+"%");
                holder.tvPollsPercent2.setText(pollList.getPollsOptions().get(1).getOptionsPercentage()+"%");

                int pollsPercent1=(int) Double.parseDouble(pollList.getPollsOptions().get(0).getOptionsPercentage());
                int pollsPercent2=(int) Double.parseDouble(pollList.getPollsOptions().get(1).getOptionsPercentage());

                holder.sbPolls1.setProgress(pollsPercent1);
                holder.sbPolls2.setProgress(pollsPercent2);

                //set OptionPercentage Color
                if(pollsPercent1 <= 25 ){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent1 <= 50){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent1 <= 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent1 > 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent2 <= 25 ){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent2 <= 50){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent2 <= 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent2 > 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

            }else if(pollList.getPollsOptions().size() == 3){

                holder.ltSeekOption1.setVisibility(View.VISIBLE);
                holder.ltSeekOption2.setVisibility(View.VISIBLE);
                holder.ltSeekOption3.setVisibility(View.VISIBLE);

                //set Option Color
                if (pollList.getPollsOptions().get(0).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName1.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(1).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName2.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(2).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName3.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.black));
                }

                //set Option
                holder.tvPollsName1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.tvPollsName2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.tvPollsName3.setText(pollList.getPollsOptions().get(2).getOptions());

                //set OptionPercentage
                holder.tvPollsPercent1.setText(pollList.getPollsOptions().get(0).getOptionsPercentage()+"%");
                holder.tvPollsPercent2.setText(pollList.getPollsOptions().get(1).getOptionsPercentage()+"%");
                holder.tvPollsPercent3.setText(pollList.getPollsOptions().get(2).getOptionsPercentage()+"%");

                int pollsPercent1=(int) Double.parseDouble(pollList.getPollsOptions().get(0).getOptionsPercentage());
                int pollsPercent2=(int) Double.parseDouble(pollList.getPollsOptions().get(1).getOptionsPercentage());
                int pollsPercent3=(int) Double.parseDouble(pollList.getPollsOptions().get(2).getOptionsPercentage());

                holder.sbPolls1.setProgress(pollsPercent1);
                holder.sbPolls2.setProgress(pollsPercent2);
                holder.sbPolls3.setProgress(pollsPercent3);

                //set OptionPercentage Color
                if(pollsPercent1 <= 25 ){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent1 <= 50){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent1 <= 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent1 > 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent2 <= 25 ){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent2 <= 50){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent2 <= 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent2 > 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));

                }

                if(pollsPercent3 <= 25 ){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent3 <= 50){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent3 <= 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent3 > 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }


            }else if(pollList.getPollsOptions().size() == 4){

                holder.ltSeekOption1.setVisibility(View.VISIBLE);
                holder.ltSeekOption2.setVisibility(View.VISIBLE);
                holder.ltSeekOption3.setVisibility(View.VISIBLE);
                holder.ltSeekOption4.setVisibility(View.VISIBLE);

                //set Option Color
                if (pollList.getPollsOptions().get(0).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName1.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(1).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName2.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(2).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName3.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(3).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName4.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName4.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName4.setTextColor(context.getResources().getColor(R.color.black));
                }

                //set Option
                holder.tvPollsName1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.tvPollsName2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.tvPollsName3.setText(pollList.getPollsOptions().get(2).getOptions());
                holder.tvPollsName4.setText(pollList.getPollsOptions().get(3).getOptions());

                //set OptionPercentage
                holder.tvPollsPercent1.setText(pollList.getPollsOptions().get(0).getOptionsPercentage()+"%");
                holder.tvPollsPercent2.setText(pollList.getPollsOptions().get(1).getOptionsPercentage()+"%");
                holder.tvPollsPercent3.setText(pollList.getPollsOptions().get(2).getOptionsPercentage()+"%");
                holder.tvPollsPercent4.setText(pollList.getPollsOptions().get(3).getOptionsPercentage()+"%");

                int pollsPercent1=(int) Double.parseDouble(pollList.getPollsOptions().get(0).getOptionsPercentage());
                int pollsPercent2=(int) Double.parseDouble(pollList.getPollsOptions().get(1).getOptionsPercentage());
                int pollsPercent3=(int) Double.parseDouble(pollList.getPollsOptions().get(2).getOptionsPercentage());
                int pollsPercent4=(int) Double.parseDouble(pollList.getPollsOptions().get(3).getOptionsPercentage());

                holder.sbPolls1.setProgress(pollsPercent1);
                holder.sbPolls2.setProgress(pollsPercent2);
                holder.sbPolls3.setProgress(pollsPercent3);
                holder.sbPolls4.setProgress(pollsPercent4);

                //set OptionPercentage Color
                if(pollsPercent1 <= 25 ){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent1 <= 50){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent1 <= 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent1 > 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent2 <= 25 ){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent2 <= 50){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent2 <= 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent2 > 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent3 <= 25 ){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent3 <= 50){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent3 <= 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent3 > 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent4 <= 25 ){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent4 <= 50){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent4 <= 75){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent4 > 75){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

            }else if(pollList.getPollsOptions().size() == 5){

                holder.ltSeekOption1.setVisibility(View.VISIBLE);
                holder.ltSeekOption2.setVisibility(View.VISIBLE);
                holder.ltSeekOption3.setVisibility(View.VISIBLE);
                holder.ltSeekOption4.setVisibility(View.VISIBLE);
                holder.ltSeekOption5.setVisibility(View.VISIBLE);

                //set Option Color
                if (pollList.getPollsOptions().get(0).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName1.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName1.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(1).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName2.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName2.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(2).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName3.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName3.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(3).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName4.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName4.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName4.setTextColor(context.getResources().getColor(R.color.black));
                }
                if (pollList.getPollsOptions().get(4).getPollsOptionsID() == pollList.getIsParentVoted().get(0).getPollOption_id()) {
                    holder.tvPollsName5.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.tvPollsName5.setTypeface(ProximaNovaFont.getInstance(context).getLightTypeFace());
                } else {
                    holder.tvPollsName5.setTextColor(context.getResources().getColor(R.color.black));
                }

                //set Option
                holder.tvPollsName1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.tvPollsName2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.tvPollsName3.setText(pollList.getPollsOptions().get(2).getOptions());
                holder.tvPollsName4.setText(pollList.getPollsOptions().get(3).getOptions());
                holder.tvPollsName5.setText(pollList.getPollsOptions().get(4).getOptions());

                //set OptionPercentage
                holder.tvPollsPercent1.setText(pollList.getPollsOptions().get(0).getOptionsPercentage()+"%");
                holder.tvPollsPercent2.setText(pollList.getPollsOptions().get(1).getOptionsPercentage()+"%");
                holder.tvPollsPercent3.setText(pollList.getPollsOptions().get(2).getOptionsPercentage()+"%");
                holder.tvPollsPercent4.setText(pollList.getPollsOptions().get(3).getOptionsPercentage()+"%");
                holder.tvPollsPercent5.setText(pollList.getPollsOptions().get(4).getOptionsPercentage()+"%");

                int pollsPercent1=(int) Double.parseDouble(pollList.getPollsOptions().get(0).getOptionsPercentage());
                int pollsPercent2=(int) Double.parseDouble(pollList.getPollsOptions().get(1).getOptionsPercentage());
                int pollsPercent3=(int) Double.parseDouble(pollList.getPollsOptions().get(2).getOptionsPercentage());
                int pollsPercent4=(int) Double.parseDouble(pollList.getPollsOptions().get(3).getOptionsPercentage());
                int pollsPercent5=(int) Double.parseDouble(pollList.getPollsOptions().get(4).getOptionsPercentage());

                holder.sbPolls1.setProgress(pollsPercent1);
                holder.sbPolls2.setProgress(pollsPercent2);
                holder.sbPolls3.setProgress(pollsPercent3);
                holder.sbPolls4.setProgress(pollsPercent4);
                holder.sbPolls5.setProgress(pollsPercent5);

                //set OptionPercentage Color
                if(pollsPercent1 <= 25 ){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent1 <= 50){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent1 <= 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent1 > 75){
                    holder.sbPolls1.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent2 <= 25 ){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent2 <= 50){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent2 <= 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent2 > 75){
                    holder.sbPolls2.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent3 <= 25 ){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent3 <= 50){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent3 <= 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent3 > 75){
                    holder.sbPolls3.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent4 <= 25 ){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent4 <= 50){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent4 <= 75){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent4 > 75){
                    holder.sbPolls4.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

                if(pollsPercent5 <= 25 ){
                    holder.sbPolls5.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan25)));
                }else if(pollsPercent5 <= 50){
                    holder.sbPolls5.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan50)));
                } else if(pollsPercent5 <= 75){
                    holder.sbPolls5.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.lessthan75)));
                } else if(pollsPercent5 > 75){
                    holder.sbPolls5.setProgressTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.greaterthan75)));
                }

            }else{

            }
        }else{

            holder.rgOptionVote.setVisibility(View.VISIBLE);
            holder.btnSubmit.setVisibility(View.VISIBLE);
            holder.ltVoted.setVisibility(View.GONE);

            if(pollList.getPollsOptions().size() == 1){

                holder.rbOption1.setVisibility(View.VISIBLE);

                holder.rbOption1.setText(pollList.getPollsOptions().get(0).getOptions());

            }else if(pollList.getPollsOptions().size() == 2){

                holder.rbOption1.setVisibility(View.VISIBLE);
                holder.rbOption2.setVisibility(View.VISIBLE);

                holder.rbOption1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.rbOption2.setText(pollList.getPollsOptions().get(1).getOptions());

            }else if(pollList.getPollsOptions().size() == 3){

                holder.rbOption1.setVisibility(View.VISIBLE);
                holder.rbOption2.setVisibility(View.VISIBLE);
                holder.rbOption3.setVisibility(View.VISIBLE);

                holder.rbOption1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.rbOption2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.rbOption3.setText(pollList.getPollsOptions().get(2).getOptions());

            }else if(pollList.getPollsOptions().size() == 4){

                holder.rbOption1.setVisibility(View.VISIBLE);
                holder.rbOption2.setVisibility(View.VISIBLE);
                holder.rbOption3.setVisibility(View.VISIBLE);
                holder.rbOption4.setVisibility(View.VISIBLE);

                holder.rbOption1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.rbOption2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.rbOption3.setText(pollList.getPollsOptions().get(2).getOptions());
                holder.rbOption4.setText(pollList.getPollsOptions().get(3).getOptions());

            }else if(pollList.getPollsOptions().size() == 5){

                holder.rbOption1.setVisibility(View.VISIBLE);
                holder.rbOption2.setVisibility(View.VISIBLE);
                holder.rbOption3.setVisibility(View.VISIBLE);
                holder.rbOption4.setVisibility(View.VISIBLE);
                holder.rbOption5.setVisibility(View.VISIBLE);

                holder.rbOption1.setText(pollList.getPollsOptions().get(0).getOptions());
                holder.rbOption2.setText(pollList.getPollsOptions().get(1).getOptions());
                holder.rbOption3.setText(pollList.getPollsOptions().get(2).getOptions());
                holder.rbOption4.setText(pollList.getPollsOptions().get(3).getOptions());
                holder.rbOption5.setText(pollList.getPollsOptions().get(4).getOptions());
            }else{

            }
        }

        holder.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.rbOption1.isChecked()) {
                    pollsViewFragment.updatePolls(pollList.getPollsOptions().get(0).getPoll_id(),+pollList.getPollsOptions().get(0).getPollsOptionsID(),pollList.getSchool_id());
                } else if (holder.rbOption2.isChecked()) {
                    pollsViewFragment.updatePolls(pollList.getPollsOptions().get(1).getPoll_id(),+pollList.getPollsOptions().get(1).getPollsOptionsID(),pollList.getSchool_id());
                } else if (holder.rbOption3.isChecked()) {
                    pollsViewFragment.updatePolls(pollList.getPollsOptions().get(2).getPoll_id(),+pollList.getPollsOptions().get(2).getPollsOptionsID(),pollList.getSchool_id());
                } else if (holder.rbOption4.isChecked()) {
                    pollsViewFragment.updatePolls(pollList.getPollsOptions().get(3).getPoll_id(),+pollList.getPollsOptions().get(3).getPollsOptionsID(),pollList.getSchool_id());
                } else if (holder.rbOption5.isChecked()) {
                    pollsViewFragment.updatePolls(pollList.getPollsOptions().get(4).getPoll_id(),+pollList.getPollsOptions().get(4).getPollsOptionsID(),pollList.getSchool_id());
                } else {
                    Toast.makeText(context, "Please select any one options", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }


    public void hideProgress() {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.hideProgress();
        }
    }


  /*  private void updatePolls(int pollId,int pollOptionId) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            PollsListParms pollsListParms = new PollsListParms();
            pollsListParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            pollsListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            pollsListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            pollsListParms.setPollID(pollId);
            pollsListParms.setPollOptionID(pollOptionId);

            vidyauraAPI.updatePolls(pollsListParms).enqueue(new Callback<PollsResultListResponse>() {
                @Override
                public void onResponse(Call<PollsResultListResponse> call, Response<PollsResultListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        pollsResultListResponse = response.body();
                        System.out.println("polls response ==> "+gson.toJson(response.body()));
                        if (pollsResultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                        } else {
                            hideProgress();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<PollsResultListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(context, context.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            hideProgress();
            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public int getItemCount() {
        return pollsResultListResponses.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvPollsQuestion, tvPollsQuestion2, tvDate,tvPollsName1,tvPollsPercent1,
        tvPollsName2,tvPollsPercent2,tvPollsName3,tvPollsPercent3,tvPollsName4,tvPollsPercent4,tvPollsName5,tvPollsPercent5;
        RadioGroup rgOptionVote;
        LinearLayout ltVoted,ltSeekOption1,ltSeekOption2,ltSeekOption3,ltSeekOption4,ltSeekOption5;
        Button btnSubmit;
        SeekBar sbPolls1,sbPolls2,sbPolls3,sbPolls4,sbPolls5;
        RadioButton rbOption1, rbOption2, rbOption3, rbOption4, rbOption5;

        public MyViewHolder(View view) {
            super(view);
            tvPollsQuestion = view.findViewById(R.id.tv_polls_question);
            tvPollsQuestion2 = view.findViewById(R.id.tv_polls_question2);
            tvDate = view.findViewById(R.id.tv_date);
            ltVoted = view.findViewById(R.id.lt_voted);

            ltSeekOption1 = view.findViewById(R.id.seek_option1);
            sbPolls1 = view.findViewById(R.id.sb_polls1);
            tvPollsName1 = view.findViewById(R.id.tv_polls_name1);
            tvPollsPercent1 = view.findViewById(R.id.tv_polls_percent1);
            sbPolls1.setEnabled(false);

            ltSeekOption2 = view.findViewById(R.id.seek_option2);
            sbPolls2 = view.findViewById(R.id.sb_polls2);
            tvPollsName2 = view.findViewById(R.id.tv_polls_name2);
            tvPollsPercent2 = view.findViewById(R.id.tv_polls_percent2);
            sbPolls2.setEnabled(false);

            ltSeekOption3 = view.findViewById(R.id.seek_option3);
            sbPolls3 = view.findViewById(R.id.sb_polls3);
            tvPollsName3 = view.findViewById(R.id.tv_polls_name3);
            tvPollsPercent3 = view.findViewById(R.id.tv_polls_percent3);
            sbPolls3.setEnabled(false);


            ltSeekOption4 = view.findViewById(R.id.seek_option4);
            sbPolls4 = view.findViewById(R.id.sb_polls4);
            tvPollsName4 = view.findViewById(R.id.tv_polls_name4);
            tvPollsPercent4 = view.findViewById(R.id.tv_polls_percent4);
            sbPolls4.setEnabled(false);

            ltSeekOption5 = view.findViewById(R.id.seek_option5);
            sbPolls5 = view.findViewById(R.id.sb_polls5);
            tvPollsName5 = view.findViewById(R.id.tv_polls_name5);
            tvPollsPercent5 = view.findViewById(R.id.tv_polls_percent5);
            sbPolls5.setEnabled(false);

            rgOptionVote = view.findViewById(R.id.rg_option_vote);
            rbOption1 = view.findViewById(R.id.rb_option1);
            rbOption2 = view.findViewById(R.id.rb_option2);
            rbOption3 = view.findViewById(R.id.rb_option3);
            rbOption4 = view.findViewById(R.id.rb_option4);
            rbOption5 = view.findViewById(R.id.rb_option5);
            btnSubmit = view.findViewById(R.id.btn_submit);
        }
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }

}