package com.school.teacherparent.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ResultList;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/1/2018.
 */

public class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.MyViewHolder> {

    List<ResultList> resultListArray;
    private Context context;
    private AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  CircularImageView class_img;
        private  CircularImageView stu_img;
        private  TextView class_name,stu_name,percent,counts,className;
        LinearLayout topperLinearlayout;
        public MyViewHolder(View view) {
            super(view);
            class_name = (TextView) view.findViewById(R.id.classname);
            stu_name = (TextView) view.findViewById(R.id.stuname);
            class_img=(CircularImageView)view.findViewById(R.id.class_img);
            stu_img=(CircularImageView)view.findViewById(R.id.stu_img);
            percent=(TextView)view.findViewById(R.id.percent);
            counts=(TextView)view.findViewById(R.id.count);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            className=(TextView)view.findViewById(R.id.className);
            topperLinearlayout=(LinearLayout) view.findViewById(R.id.topperLinearlayout);
        }
    }


    public ResultListAdapter(List<ResultList> resultListArray, Context context) {
        this.resultListArray = resultListArray;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_list_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ResultList resultList = resultListArray.get(position);
       holder.class_name.setText(resultList.getAlt_name()+" "+resultList.getSection());
        if (resultList.getClassAverage().equals("0"))
        {
            holder.topperLinearlayout.setVisibility(View.GONE);
        }
        else
        {
            holder.topperLinearlayout.setVisibility(View.VISIBLE);
        }
            holder.stu_name.setText(resultList.getFname()+" "+resultList.getLname());


        holder.percent.setText(resultList.getClassAverage()+" "+"%");
        holder.counts.setText(resultList.getTotalStudentsCount()+" "+context.getString(R.string.students));

        String urll = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+resultListArray.get(position).getClassImage();
        Picasso.get().load(urll).placeholder(R.drawable.nopreview).into(holder.class_img, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.class_img.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.class_img.setImageResource(Constants.imgid[position]);
        //holder.className.setText(resultList.getClassName()+" "+resultList.getSection());
        //holder.stu_img.setImageResource(Integer.parseInt(movie.getStu_img()));

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

        if (resultList.getStudent_photo()!=null)
        {
            String url=context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_student_path)+"/"+resultList.getStudent_photo();

            Picasso.get().load(url).


                    into( holder.stu_img, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                }
                            },1000);


                        }

                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onError(Exception e) {
                            holder.stu_img.setImageDrawable(context.getDrawable(R.drawable.ic_user));
                        }
                    });

        }

        else
        {
            holder.stu_img.setImageDrawable(context.getDrawable(R.drawable.ic_user));
        }

    }

    @Override
    public int getItemCount() {
        return resultListArray.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}