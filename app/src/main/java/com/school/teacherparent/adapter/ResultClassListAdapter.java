package com.school.teacherparent.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.fragment.ResultClassListFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.fragment.ResultStudentFragment;
import com.school.teacherparent.models.ResultDetailsResponse;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by keerthana on 10/1/2018.
 */


public class ResultClassListAdapter extends RecyclerView.Adapter<ResultClassListAdapter.MyViewHolder> {

    private List<ResultDetailsResponse.resultDetailsList> resultDetailsLists = new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;
    ResultClassListFragment resultClassListFragment;
    int classID,sectionID;
    String altname,section;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        CircularImageView img;
        RelativeLayout lin;
        public TextView name, counts, date;
        public TextView content;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            counts = (TextView) view.findViewById(R.id.counts);
            img = (CircularImageView) view.findViewById(R.id.profile);
            lin = (RelativeLayout) view.findViewById(R.id.lin);

        }
    }


    public ResultClassListAdapter(ArrayList<ResultDetailsResponse.resultDetailsList> resultDetailsLists, Context context, ResultClassListFragment resultClassListFragment, int classID, int sectionID, String altname, String section) {
        this.resultDetailsLists = resultDetailsLists;
        this.context = context;
        this.resultClassListFragment=resultClassListFragment;
        this.classID=classID;
        this.sectionID=sectionID;
        this.altname=altname;
        this.section=section;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_class_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ResultDetailsResponse.resultDetailsList resultDetailsList = resultDetailsLists.get(position);
        holder.name.setText(resultDetailsList.getFname() + " " + resultDetailsList.getLname());
        holder.counts.setText("" + resultDetailsList.getMarks());
        //holder.img.setImageResource(Integer.parseInt(movie.getImg()));

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/" + resultDetailsList.getStudent_photo();
        Picasso.get().load(url).placeholder(R.drawable.ic_user).into(holder.img, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.img.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_user));
            }
        });

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //resultClassListFragment.resetSpinner();
                Fragment fragment = new ResultStudentFragment();
                Bundle bundleau = new Bundle();
                bundleau.putInt("classID",classID);
                bundleau.putInt("sectionID",sectionID);
                bundleau.putInt("studID", resultDetailsList.getStudID());
                bundleau.putString("studName", resultDetailsList.getFname()+" "+resultDetailsList.getLname());
                bundleau.putString("classname",altname);
                bundleau.putString("section",section);
                bundleau.putString("studentProfile",resultDetailsList.getStudent_photo());
                fragment.setArguments(bundleau);
                resultClassListFragment.replaceFragment(fragment);

            }
        });

    }

    @Override
    public int getItemCount() {
        return resultDetailsLists.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}