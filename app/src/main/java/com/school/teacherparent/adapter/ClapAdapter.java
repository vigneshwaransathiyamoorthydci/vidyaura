package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.models.FeedClap;
import com.school.teacherparent.models.FeedClapListResponse;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by keerthana on 10/18/2018.
 */

public class ClapAdapter extends RecyclerView.Adapter<ClapAdapter.MyViewHolder> {

    ArrayList<FeedClap> feedclapListResponsearray;
    private Context context;
    int feedId;


    public ClapAdapter( ArrayList<FeedClap> feedclapListResponsearray, Context context,int feedId) {

        this.feedclapListResponsearray=feedclapListResponsearray;
        this.context=context;
        this.feedId=feedId;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {


        private CircularImageView user_img;
        private TextView user_name;

        public MyViewHolder(View view) {
            super(view);
            user_img=view.findViewById(R.id.user_img);
            user_name=view.findViewById(R.id.user_name);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.claps_recycle_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        FeedClap fcData = feedclapListResponsearray.get(position);
        /*if (fcData.getFeedLikedByUserDetails().size()>0)
        {*/
            /*if(!isNullOrEmpty(fcData.getLname()))
            {
                holder.user_name.setText(fcData.getFeedLikedByUserDetails().get(0).getFname() + " " + fcData.getFeedLikedByUserDetails().get(0).getLname());
            }
            else
            {*/
                holder.user_name.setText(fcData.getUserName().replace(" null",""));
            /*}*/

            // holder.user_img.setImageResource(Integer.parseInt(movie.getStu_img()));
            if (fcData.getUserImage() != null) {
                String url = null;
                if (fcData.getLike_user_type().equals("teachers")) {
                    url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + fcData.getUserImage();
                } else if (fcData.getLike_user_type().equals("parents")) {
                    url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/" + fcData.getUserImage();
                }

                Picasso.get().load(url).


                        into(holder.user_img, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {


                                    }
                                }, 1000);


                            }

                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onError(Exception e) {
                                holder.user_img.setImageDrawable(context.getDrawable(R.drawable.ic_user));
                            }
                        });

            } else {
                holder.user_img.setImageDrawable(context.getDrawable(R.drawable.ic_user));
            }
        /*}*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", feedId));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }
    @Override
    public int getItemCount() {
        return feedclapListResponsearray.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}