package com.school.teacherparent.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.MessageList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {

    Context context;
    private ArrayList<MessageList> messageListArrayList;
    int userType;
    private String userID;

    public MessageListAdapter(Context context, ArrayList<MessageList> messageListArrayList, String userID) {
        this.context = context;
        this.messageListArrayList = messageListArrayList;
        this.userID = userID;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        String time[] = messageListArrayList.get(position).getSentTime().split(" ");

        final String timee = time[1];
        String sentTime = null;

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(timee);
            sentTime = new SimpleDateFormat("KK:mm aa").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        RequestOptions profileError = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        RequestOptions imageError = new RequestOptions()
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        if (messageListArrayList.get(position).getSenderUserID().equals(userID)) {
            holder.rlSenderLayout.setVisibility(View.VISIBLE);
            if (!messageListArrayList.get(position).getSenderImage().equals("")) {
                Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(profileError).into(holder.civSenderImage);
            }
            if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                holder.ivSenderAttachment.setVisibility(View.VISIBLE);
                Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(imageError).into(holder.ivSenderAttachment);
                holder.ivSenderAttachment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                    }
                });

            }
            if (!messageListArrayList.get(position).getMessage().equals("")) {
                holder.tvSenderMessage.setText(Html.fromHtml(messageListArrayList.get(position).getMessage()));
            } else {
                holder.tvSenderMessage.setVisibility(View.GONE);
            }
            if (sentTime.contains("PM")) {
                String aa[] = sentTime.split(":");
                if (aa[0].equals("00")) {
                    holder.tvSenderTime.setText("12:"+aa[1]);
                } else {
                    holder.tvSenderTime.setText(sentTime);
                }
            } else {
                holder.tvSenderTime.setText(sentTime);
            }

        } else {
            holder.rlReceiverLayout.setVisibility(View.VISIBLE);
            if (!messageListArrayList.get(position).getSenderImage().equals("")) {
                Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(profileError).into(holder.civReceiverImage);
            }
            if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                holder.ivReceiverAttachment.setVisibility(View.VISIBLE);
                Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(imageError).into(holder.ivReceiverAttachment);
                holder.ivReceiverAttachment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                    }
                });
            }
            if (!messageListArrayList.get(position).getMessage().equals("")) {
                holder.tvReceiverMessage.setText(Html.fromHtml(messageListArrayList.get(position).getMessage()));
            } else {
                holder.tvReceiverMessage.setVisibility(View.GONE);
            }
            if (sentTime.contains("PM")) {
                String aa[] = sentTime.split(":");
                if (aa[0].equals("00")) {
                    holder.tvReceiverTime.setText("12:"+aa[1]);
                } else {
                    holder.tvReceiverTime.setText(sentTime);
                }
            } else {
                holder.tvReceiverTime.setText(sentTime);
            }
        }

        /*if (userType == 1) {
            if (messageListArrayList.get(position).getState() == 1) {
                holder.rlSenderLayout.setVisibility(View.VISIBLE);
                if (!messageListArrayList.get(position).getSenderImage().equals("")) {

                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.ic_user)
                            .error(R.drawable.ic_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(error).into(holder.civSenderImage);

                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getSenderImage()).
                            into(holder.civSenderImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.civSenderImage.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                    holder.ivSenderAttachment.setVisibility(View.VISIBLE);
                    //holder.ivSenderAttachment.setAdapter(new PreviewImageAdapter(context, messageListArrayList.get(position).getImageUrl(),2));
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.gallery)
                            .error(R.drawable.gallery)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(error).into(holder.ivSenderAttachment);

                    holder.ivSenderAttachment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                        }
                    });

                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getImageUrl()).
                            into(holder.ivSenderAttachment, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.ivSenderAttachment.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (!messageListArrayList.get(position).getMessage().equals("")) {
                    holder.tvSenderMessage.setText(messageListArrayList.get(position).getMessage());
                } else {
                    holder.tvSenderMessage.setVisibility(View.GONE);
                }
                holder.tvSenderTime.setText(time[1]);
            } else if (messageListArrayList.get(position).getState() == 2) {
                holder.rlReceiverLayout.setVisibility(View.VISIBLE);
                if (!messageListArrayList.get(position).getSenderImage().equals("")) {
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.ic_user)
                            .error(R.drawable.ic_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(error).into(holder.civReceiverImage);
                    //Picasso.get().load(messageListArrayList.get(position).getReceiverImage()).into(holder.civReceiverImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getSenderImage()).
                            into(holder.civReceiverImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.civReceiverImage.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                    holder.ivReceiverAttachment.setVisibility(View.VISIBLE);
                    //holder.ivReceiverAttachment.setAdapter(new PreviewImageAdapter(context, messageListArrayList.get(position).getImageUrl(),2));
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.gallery)
                            .error(R.drawable.gallery)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(error).into(holder.ivReceiverAttachment);

                    holder.ivReceiverAttachment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                        }
                    });

                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getImageUrl()).
                            into(holder.ivReceiverAttachment, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.ivReceiverAttachment.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (!messageListArrayList.get(position).getMessage().equals("")) {
                    holder.tvReceiverMessage.setText(messageListArrayList.get(position).getMessage());
                } else {
                    holder.tvReceiverMessage.setVisibility(View.GONE);
                }
                holder.tvReceiverTime.setText(time[1]);
            } else {
                holder.rlSenderLayout.setVisibility(View.GONE);
                holder.rlReceiverLayout.setVisibility(View.GONE);
            }
        } else if (userType == 2) {
            if (messageListArrayList.get(position).getState() == 2) {
                holder.rlSenderLayout.setVisibility(View.VISIBLE);
                System.out.println("getSenderImage ==> "+messageListArrayList.get(position).getSenderImage());
                if (!messageListArrayList.get(position).getSenderImage().equals("")) {
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.ic_user)
                            .error(R.drawable.ic_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(error).into(holder.civSenderImage);
                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getSenderImage()).
                            into(holder.civSenderImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.civSenderImage.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                    holder.ivSenderAttachment.setVisibility(View.VISIBLE);
                    //holder.ivSenderAttachment.setAdapter(new PreviewImageAdapter(context, messageListArrayList.get(position).getImageUrl(),2));
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.gallery)
                            .error(R.drawable.gallery)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(error).into(holder.ivSenderAttachment);

                    holder.ivSenderAttachment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                        }
                    });

                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getImageUrl()).
                            into(holder.ivSenderAttachment, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.ivSenderAttachment.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (!messageListArrayList.get(position).getMessage().equals("")) {
                    holder.tvSenderMessage.setText(messageListArrayList.get(position).getMessage());
                } else {
                    holder.tvSenderMessage.setVisibility(View.GONE);
                }
                holder.tvSenderTime.setText(time[1]);
            } else if (messageListArrayList.get(position).getState() == 1) {
                holder.rlReceiverLayout.setVisibility(View.VISIBLE);
                System.out.println("getReceiverImage ==> "+messageListArrayList.get(position).getReceiverImage());
                if (!messageListArrayList.get(position).getSenderImage().equals("")) {
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.ic_user)
                            .error(R.drawable.ic_user)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getSenderImage()).apply(error).into(holder.civReceiverImage);
                    //Picasso.get().load(messageListArrayList.get(position).getReceiverImage()).into(holder.civReceiverImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getSenderImage()).
                            into(holder.civReceiverImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.civReceiverImage.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (messageListArrayList.get(position).getImageUrl() != null && messageListArrayList.get(position).getImageUrl().size() > 0) {
                    holder.ivReceiverAttachment.setVisibility(View.VISIBLE);
                    //holder.ivReceiverAttachment.setAdapter(new PreviewImageAdapter(context, messageListArrayList.get(position).getImageUrl(),2));
                    RequestOptions error = new RequestOptions()
                            *//*.centerCrop()*//*
                            .placeholder(R.drawable.gallery)
                            .error(R.drawable.gallery)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH);

                    Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getImageUrl().get(0)).apply(error).into(holder.ivReceiverAttachment);

                    holder.ivReceiverAttachment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            initiatepopupwindow(messageListArrayList.get(position).getImageUrl());
                        }
                    });

                    //Picasso.get().load(messageListArrayList.get(position).getSenderImage()).into(holder.civSenderImage);
                    *//*Picasso.get().load(messageListArrayList.get(position).getImageUrl()).
                            into(holder.ivReceiverAttachment, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                        }
                                    }, 1000);
                                }

                                @Override
                                public void onError(Exception e) {
                                    holder.ivReceiverAttachment.setImageResource(R.drawable.ic_user);
                                }
                            });*//*
                }
                if (!messageListArrayList.get(position).getMessage().equals("")) {
                    holder.tvReceiverMessage.setText(messageListArrayList.get(position).getMessage());
                } else {
                    holder.tvReceiverMessage.setVisibility(View.GONE);
                }
                holder.tvReceiverTime.setText(time[1]);
            } else {
                holder.rlSenderLayout.setVisibility(View.GONE);
                holder.rlReceiverLayout.setVisibility(View.GONE);
            }
        }*/

    }

    private void initiatepopupwindow(List<String> imagelist) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.show_image_message, null);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(context, 8, imagelist));
        viewPager.setCurrentItem(0);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();

        if (imagelist.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
            }
        });

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }

    @Override
    public int getItemCount() {
        return (null != messageListArrayList ? messageListArrayList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircularImageView civReceiverImage, civSenderImage;
        RelativeLayout rlReceiverLayout, rlSenderLayout;
        CardView cvReceiverMessage, cvSenderMessage;
        TextView tvReceiverMessage, tvReceiverTime, tvSenderMessage, tvSenderTime;
        ImageView ivReceiverAttachment, ivSenderAttachment;

        public ViewHolder(View view) {
            super(view);
            civReceiverImage = view.findViewById(R.id.civ_receiver_image);
            rlReceiverLayout = view.findViewById(R.id.rl_receiver_layout);
            cvReceiverMessage = view.findViewById(R.id.cv_receiver_message);
            tvReceiverMessage = view.findViewById(R.id.tv_receiver_message);
            ivReceiverAttachment = view.findViewById(R.id.iv_receiver_attachment);
            tvReceiverTime = view.findViewById(R.id.tv_receiver_time);
            civSenderImage = view.findViewById(R.id.civ_sender_image);
            rlSenderLayout = view.findViewById(R.id.rl_sender_layout);
            cvSenderMessage = view.findViewById(R.id.cv_sender_message);
            tvSenderMessage = view.findViewById(R.id.tv_sender_message);
            ivSenderAttachment = view.findViewById(R.id.iv_sender_attachment);
            tvSenderTime = view.findViewById(R.id.tv_sender_time);

            //ivReceiverAttachment.setLayoutManager(new GridLayoutManager(context,2));
            //ivSenderAttachment.setLayoutManager(new GridLayoutManager(context,2));
        }
    }

}
