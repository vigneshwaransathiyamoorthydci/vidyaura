package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.CircularList;
import com.school.teacherparent.models.CircularListResponse;
import com.school.teacherparent.R;
import com.school.teacherparent.models.DeleteCircularParms;
import com.school.teacherparent.models.DeleteFeedParms;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import at.blogc.android.views.ExpandableTextView;
import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 9/12/2018.
 */

public  class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.MyViewHolder> {

    private  LinearLayout top;
    private  ImageView menuu;
    ArrayList<CircularList>  circularListResponseArrayList;
    private Context context;
    private int[] android_versionnames;
    private int row_index;
    private int focusedItem = 0;
    private int position;
    private View itemView;
    private LinearLayout lin;
    private MotionEvent event;
    private View v;
    private View view;
    private Context con;
    private PopupWindow pwindo;
    private AddTouchListener addTouchListen;
    BaseActivity baseActivity;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  View view_border;
        private  CardView mCardViewBottom;
        public TextView title;
        CircularImageView img;
        public TextView desc;
        public TextView content;
        public LinearLayout lin1;
        private TextView tv1;
        public ImageView menuu;
        ExpandableTextView expandableTextView;
        RecyclerView image_recycle;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            content=(TextView)view.findViewById(R.id.content);
            lin1=(LinearLayout)view.findViewById(R.id.lin1);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            top=(LinearLayout)view.findViewById(R.id.top);
            menuu=(ImageView)view.findViewById(R.id.menu);
            view_border=(View)view.findViewById(R.id.view_border);
            img = view.findViewById(R.id.img);
            mCardViewBottom = (CardView)view.findViewById(R.id.card_view_bottom);
            //mCardViewBottom.setCardBackgroundColor(context.getResources().getColor(R.color.gray_background));
            expandableTextView =(ExpandableTextView) view.findViewById(R.id.expandableTextViewDescription);
            expandableTextView.setInterpolator(new OvershootInterpolator());
            image_recycle = view.findViewById(R.id.image_recycle);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 3);
            image_recycle.setLayoutManager(mLayoutManager);
        }
    }







    public CircularAdapter(ArrayList<CircularList>  circularListResponseArrayList, Context context) {
        this.circularListResponseArrayList = circularListResponseArrayList;
        this.context =context;
       // this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.circular_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

//    public void setOnClickListen(AddTouchListener addTouchListen) {
//        this.addTouchListen = addTouchListen;
//    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       // holder.tv1.setText(android_versionnames[position]);
        final CircularList circularListdata = circularListResponseArrayList.get(position);
        holder.title.setText(circularListdata.getTitle());
        //holder.desc.setText(circularListdata.getDescription());
        holder.content.setText(getDateFormat(circularListdata.getCreated_at())+" "+context.getString(R.string.at)+" "+getTimefromdate(circularListdata.getCreated_at()));
        holder.expandableTextView.setText(circularListdata.getDescription());

        final String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_schools) + sharedPreferences.getString(Constants.SCHOOL_LOGO, "");
        final String url1 = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_schools) + circularListdata.getSchoolLogo();
        Picasso.get().load(url1).
                into(holder.img, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(R.mipmap.school).into(holder.img);

                    }
                });

        //holder.img.setImageURI();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            holder.expandableTextView.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
//        }
        if (circularListdata.getIsUserPostedCircular()==1)
        {
            holder.menuu.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.menuu.setVisibility(View.GONE);
        }
        holder.desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.expandableTextView.toggle();
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.expandableTextView.toggle();
            }
        });
     //   menuu=(ImageView)v.findViewById(R.id.menu);
        holder.menuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);

                if ("".equals("")) {
                    delete.setVisibility(View.GONE);
                }

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "circular").putExtra("circuId", circularListdata.getId()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            showProgress();
                            DeleteCircularParms deleteCircularParms=new DeleteCircularParms();
                            deleteCircularParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            deleteCircularParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            deleteCircularParms.setCircularsID(String.valueOf(circularListResponseArrayList.get(position).getId()));
                            deleteCircularParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));


                            vidyauraAPI.deleteCircular(deleteCircularParms).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                //feedsFragment.getfeedList();
                                                circularListResponseArrayList.remove(position);
                                                notifyDataSetChanged();


                                            } else {
                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(context, SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(i);
                                            ((Activity)context).finish();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                   // baseActivity.hideProgress();
                                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }



                });
            }
        });

        if(circularListdata.getAttachmentsList().size()>0) {
            ImageViewAdapter imageList = new ImageViewAdapter(circularListdata.getAttachmentsList(), context, context.getString(R.string.s3_circular_path));
            holder.image_recycle.setAdapter(imageList);
            imageList.setOnClickListen(new ImageViewAdapter.AddTouchListen() {
                @Override
                public void OnTouchClick(int position) {
                    initiatepopupwindow(circularListdata.getAttachmentsList().get(position), "png", position, circularListdata.getAttachmentsList());
                }
            });
        }else{
            holder.image_recycle.setVisibility(View.GONE);
        }


//        if(position==0)
//        {
//            holder.menuu.setVisibility(View.VISIBLE);
//
//        }
//        if (position==1)
//        {
//            holder.view_border.setVisibility(View.VISIBLE);
//
//            lin.setBackgroundColor(Color.parseColor("#ffffff"));
//            /*lin.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    if(event.getAction() == MotionEvent.ACTION_DOWN)
//                    {
//                        v.setBackgroundColor(Color.parseColor("#ffffff"));
//                    }
//                    if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
//                    {
//                        v.setBackgroundColor(Color.TRANSPARENT);
//                    }
//                    return false;
//                }
//            });*/
//        }


    }

  /*  private void alertdialogbox() {


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_label_editor, null);
        dialogBuilder.setView(dialogView);

        TextView editText = (EditText) dialogView.findViewById(R.id.edit);
        editText.setText("test label");
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

*/





      /*  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
        alertDialogBuilder.setMessage("Are you sure you want to delete?");


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/


    @Override
    public int getItemCount() {
        return circularListResponseArrayList.size();
    }


    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }

    public void hideProgress() {
        BaseActivity activity = (BaseActivity) context;
        if (activity != null) {
            activity.hideProgress();
        }
    }


    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getTimefromdate(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return simpleDateFormat.format(dob);
    }

    private void initiatepopupwindow(String mediapath, String media_type, int position, List<String>  imagelist) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd video_selected=alertLayout.findViewById(R.id.video_selected);;
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(context, 6, imagelist));
        viewPager.setCurrentItem(position);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
            }
        });

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }
}