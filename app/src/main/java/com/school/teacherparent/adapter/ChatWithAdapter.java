package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.models.MessageList;

import java.util.ArrayList;

public class ChatWithAdapter extends RecyclerView.Adapter<ChatWithAdapter.ViewHolder> {

    Context context;
    private ArrayList<MessageList> newUserListArrayList;
    private int userType;

    public ChatWithAdapter(Context context, ArrayList<MessageList> newUserListArrayList) {
        this.context = context;
        this.newUserListArrayList = newUserListArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.tvUserName.setText(newUserListArrayList.get(position).getReceiverName());
        holder.tvClassName.setText(newUserListArrayList.get(position).getReceiverClass());
        holder.tvRoll.setText(newUserListArrayList.get(position).getReceiverRole());
        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context.getApplicationContext()).load(newUserListArrayList.get(position).getReceiverImage()).apply(error).into(holder.ivProfileImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), NewMessageActivity.class));
                Intent intent=new Intent(context, MessageActivity.class);
                intent.putExtra("ReceiverID", newUserListArrayList.get(0).getReceiverUserID());
                intent.putExtra("ReceiverName", newUserListArrayList.get(0).getReceiverName());
                intent.putExtra("ReceiverImage", newUserListArrayList.get(0).getReceiverImage());
                intent.putExtra("ReceiverFcmKey", newUserListArrayList.get(0).getReceiverFcmKey());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != newUserListArrayList ? newUserListArrayList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProfileImage;
        TextView tvUserName, tvClassName, tvRoll;

        public ViewHolder(View view) {
            super(view);
            ivProfileImage = view.findViewById(R.id.iv_profile_image);
            tvUserName = view.findViewById(R.id.tv_username);
            tvClassName = view.findViewById(R.id.tv_class_name);
            tvRoll = view.findViewById(R.id.tv_roll);
        }
    }
}
