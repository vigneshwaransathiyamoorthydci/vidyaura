package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.models.Photoslist;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.jzvd.JzvdStd;

/**
 * Created by keerthana on 11/12/2018.
 */

public class PhotosandVideoGalleryAdapter extends RecyclerView.Adapter<PhotosandVideoGalleryAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
     List<GalleryListResponse.albumList> photoList;
    private Context context;
    private PhotosandVideoGalleryAdapter.AddTouchListen addTouchListen;



    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout header;
        ImageView img,playImageview;
        public VideoView v;

        JzvdStd video_selected;
        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.image);
            //header=(LinearLayout)view.findViewById(R.id.header);
            video_selected=(JzvdStd)view.findViewById(R.id.video_selected);
            playImageview=(ImageView)view.findViewById(R.id.playImageview);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


    public PhotosandVideoGalleryAdapter(List<GalleryListResponse.albumList> photoList, Context context) {
        this.photoList = photoList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photos_gallery_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {

        this.addTouchListen=addTouchListen;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        GalleryListResponse.albumList gallerylist = photoList.get(position);

        if (gallerylist.getMedia_type().equals("mp4"))
        {
            holder.video_selected.setVisibility(View.GONE);
            holder.img.setVisibility(View.VISIBLE);
            holder.playImageview.setVisibility(View.VISIBLE);

//            holder.video_selected.setUp((context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getMedia_path())
//                    , Jzvd.SCREEN_WINDOW_NORMAL, "");
//            holder.video_selected.setSystemTimeAndBattery();
//
//            RequestOptions requestOptions = new RequestOptions();
//            requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
//            requestOptions.error(R.mipmap.icon_video_thumbnail);
//            Glide.with(context)
//                    .load(context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getMedia_path())
//                    .apply(requestOptions)
//                    .thumbnail(Glide.with(context).load(context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getMedia_path()))
//                    .into(holder.video_selected.thumbImageView);

            long positionInMillis=1000;
            long interval = positionInMillis * 1000;
            RequestOptions options = new RequestOptions().frame(interval);
            Glide.with(context).asBitmap()
                    .load(context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+
                            gallerylist.getMedia_path())
                    .apply(options).into(holder.img);;



        }
        else {
            holder.video_selected.setVisibility(View.GONE);
            holder.img.setVisibility(View.VISIBLE);
            holder.playImageview.setVisibility(View.INVISIBLE);
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + gallerylist.getMedia_path();
            Picasso.get().load(url).into(holder.img, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    holder.img.setImageResource(R.mipmap.school);
                }
            });


        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

        }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return photoList.size();
    }
}




