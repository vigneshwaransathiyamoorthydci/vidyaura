package com.school.teacherparent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.SyllabusDetailsModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/4/2018.
 */


public class SylabusdetailsTopicNewAdapter extends RecyclerView.Adapter<SylabusdetailsTopicNewAdapter.MyViewHolder> {

    private Context context;
    private List<SyllabusDetailsModel.SyllabusList> TopicList = new ArrayList<>();

    public SylabusdetailsTopicNewAdapter(List<SyllabusDetailsModel.SyllabusList> TopicList, Context context) {
        this.TopicList = TopicList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sylabusdetails_topic_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SyllabusDetailsModel.SyllabusList movie = TopicList.get(position);
        for (int i=0;i<movie.getChapterTopicDetails().size();i++){

            StringBuilder stringBuilder = new StringBuilder();
            for(int j=0;j< movie.getChapterTopicDetails().size();j++){
                stringBuilder.append(movie.getChapterTopicDetails().get(j).getTopic_name()).append(", ");
            }
            holder.topic.setText("Topic: "+stringBuilder.substring(0, stringBuilder.length() - 2));
            holder.chapter.setText("Chapter: "+movie.getChapterTopicDetails().get(i).getChapter_name());
        }
    }

    @Override
    public int getItemCount() {
        return TopicList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {


        private TextView chapter,topic;

        public MyViewHolder(View view) {
            super(view);

            context = itemView.getContext();
            chapter = (TextView) view.findViewById(R.id.chapter);
            topic = (TextView) view.findViewById(R.id.topic);


        }
    }


}