package com.school.teacherparent.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.SyllabusDetailsModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/4/2018.
 */


public class SylabusdetailsAdapter extends RecyclerView.Adapter<SylabusdetailsAdapter.MyViewHolder> {

    private final LayoutInflater inflater;
    private final int VIEW_TYPE_TEXTVIEW = 0;
    private final int VIEW_TYPE_ITEM_1 = 1;
    private final int VIEW_TYPE_ITEM_2 = 2;
    public AddTouchListen addTouchListen;
    int intMaxNoOfChild = 0;
    private List<SyllabusDetailsModel.SyllabusList> restuList = new ArrayList<>();
    private Context context;

    public SylabusdetailsAdapter(List<SyllabusDetailsModel.SyllabusList> restuList, Context context) {
        this.restuList = restuList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student_syllabus, parent, false);

//        for (int index = 0; index < restuList.size(); index++) {
//            int intMaxSizeTemp = restuList.get(index).getDummy().size();
//            if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
//        }
        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen = addTouchListen;

    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SyllabusDetailsModel.SyllabusList movie = restuList.get(position);


        int noOfChildTextViews = holder.lin.getChildCount();
        int noOfChild = movie.getChapterTopicDetails().size();

        if (noOfChild < noOfChildTextViews) {
            for (int index = noOfChild; index < noOfChildTextViews; index++) {
//                TextView currentTextView = (TextView) holder.lin.getChildAt(index);
//                currentTextView.setVisibility(View.GONE);
                RelativeLayout relativeLayout = (RelativeLayout) holder.lin.getChildAt(index);
                relativeLayout.setVisibility(View.GONE);

                //currentTextView.setText(movie.getDummy().get(index).getSubject());
            }
        }


     /*for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {*/
            RelativeLayout relativeLayout = (RelativeLayout) holder.lin.getChildAt(0);
        holder.examTitletextview.setText(movie.getChapterTopicDetails().get(0).getTermName());
//
//            LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//            LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//            marksLinear.setVisibility(View.GONE);
//            totalLinear.setVisibility(View.VISIBLE);
            TextView chaptercompletetextview = relativeLayout.findViewById(R.id.chaptercompletetextview);
            TextView chapterincompletetextview = relativeLayout.findViewById(R.id.chapterincompletetextview);
            TextView averagetextview =  relativeLayout.findViewById(R.id.stmarks);

//            if (movie.getSyllabusList().get(textViewIndex).getStatus()==1)
//            {
            chaptercompletetextview.setText(context.getString(R.string.Chapter) + ":" + " " + movie.getChapterTopicDetails().get(0).getChapter_name());
            chapterincompletetextview.setVisibility(View.GONE);
            //chapterincompletetextview.setText(context.getString(R.string.Chapter)+":"+" "+movie.getDummy().get(textViewIndex).getChapter());
            StringBuilder stringBuilder = new StringBuilder();
            for(int i=0;i< movie.getChapterTopicDetails().size();i++){
                stringBuilder.append(movie.getChapterTopicDetails().get(i).getTopic_name()+", ");
            }

            averagetextview.setText(context.getString(R.string.topic) + ":" + " " + stringBuilder);

//            }
//            else
//            {
//                chaptercompletetextview.setVisibility(View.GONE);
//                chapterincompletetextview.setText(context.getString(R.string.Chapter)+":"+" "+movie.getSyllabusList().get(textViewIndex).getChapterTopicDetails());
//                averagetextview.setText(context.getString(R.string.topic)+":"+" "+movie.getSyllabusList().get(textViewIndex).getChapterTopicDetails());
//            }

//                martksTextview.setText(movie.getDummy().get(textViewIndex).getMarks1());
//            if (movie.getDummy().get(textViewIndex).getSubject().equals("Total"))
//            {
//                LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//                LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//                marksLinear.setVisibility(View.GONE);
//                totalLinear.setVisibility(View.VISIBLE);
//
//                TextView totaltextviewlabel=(TextView)relativeLayout.findViewById(R.id.totaltextviewlabel);
//                TextView averagetextview=(TextView)relativeLayout.findViewById(R.id.averagetextview);
//                TextView totalTextview=(TextView)relativeLayout.findViewById(R.id.totaltextview);
//
//
//            }
//            else
//            {
//                LinearLayout totalLinear=(LinearLayout)relativeLayout.findViewById(R.id.linearTotal);
//                LinearLayout marksLinear=(LinearLayout)relativeLayout.findViewById(R.id.marksLinear);
//                totalLinear.setVisibility(View.GONE);
//                marksLinear.setVisibility(View.VISIBLE);
//                TextView subjectTextview=(TextView)relativeLayout.findViewById(R.id.subject);
//                TextView martksTextview=(TextView)relativeLayout.findViewById(R.id.stmarks);
//                TextView totalTextview=(TextView)relativeLayout.findViewById(R.id.marks1);
//                //relativeLayout.getChildCount();
//                //TextView currentTextView = (TextView) holder.lin.getChildAt(textViewIndex);
//                subjectTextview.setText(movie.getDummy().get(textViewIndex).getSubject());
//                martksTextview.setText(movie.getDummy().get(textViewIndex).getMarks1());
//            }

                /*currentTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
   /*}*/
//
//        holder.subject.setText(movie.getSubject());
//        holder.total.setText(movie.getMarks1());
//        holder.stmarks.setText(movie.getStmarks());
//        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
//
//
//        holder.lin.setOnClickListener(new View.OnClickListener() {
//
//
//            @Override
//            public void onClick(View view) {
//
//                if (addTouchListen != null) {
//                    addTouchListen.OnTouchClick(position);
//                }
//            }
//        });
//        if (movie.getSubject().equals("Total"))
//        {
//            holder.img.setVisibility(View.GONE);
//            holder.itemView.setBackgroundColor(R.color.colorPrimary);
//
//        }


    }

    @Override
    public int getItemCount() {
        return restuList.size();
    }

    public interface AddTouchListen {
        public void OnTouchClick(int position);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private LinearLayout lin;
        private TextView examTitletextview;


        public MyViewHolder(View view) {
            super(view);
            context = itemView.getContext();

            examTitletextview = (TextView) view.findViewById(R.id.sylabustitleTextview);
//            img = (ImageView) view.findViewById(R.id.img);
//            total = (TextView) view.findViewById(R.id.marks1);
//            stmarks = (TextView) view.findViewById(R.id.stmarks);
            lin = (LinearLayout) view.findViewById(R.id.header);

            for (int index = 0; index < restuList.size(); index++) {
                int intMaxSizeTemp = restuList.get(index).getChapterTopicDetails().size();


                if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
            }

            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.sylabusdetails_student_item, null, false);
//                TextView textView = new TextView(context);
//                textView.setId(indexView);
//                textView.setTextColor(Color.BLACK);
//                textView.setPadding(0, 20, 0, 20);
//                textView.setGravity(Gravity.CENTER);
//                textView.setBackgroundResource(R.color.colorPrimary);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lin.addView(layout, layoutParams);

            }

        }
    }


}