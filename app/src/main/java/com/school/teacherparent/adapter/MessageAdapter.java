package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.activity.GroupMessageActivity;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.activity.NewMessageActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChatMessageParams;
import com.school.teacherparent.R;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

/**
 * Created by harini on 9/10/2018.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    AddTouchListener addTouchListen;
    //private List<ChatMessageParams> messagelist = new ArrayList<>();
    private ArrayList<MessageList> messageListArrayList;
    private ArrayList<String> recFcmKeyList = new ArrayList<>();
    private Context context;
    ArrayList<String> count1 = new ArrayList<>();
    ArrayList<String> count2 = new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public MessageAdapter(ArrayList<MessageList> messageListArrayList, Context context) {
        //this.messagelist = showing;
        this.messageListArrayList = messageListArrayList;
        this.context = context;
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        /*ChatMessageParams movie = messagelist.get(position);
        holder.title.setText(movie.getToName());
        holder.date.setText(movie.getSentTime());
        holder.content.setText(movie.getText());*/
        // holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        String date[] = messageListArrayList.get(position).getSentTime().split(" ");

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        final FirebaseDatabase database = FirebaseDatabase.getInstance();

        System.out.println("getMessage ==> "+messageListArrayList.get(position).getMessage());

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        try {
            date1 = fmt.parse(messageListArrayList.get(position).getSentTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yy");
        fmtOut.format(date1);

        holder.date.setText(covertTimeToText(new Date(Long.parseLong(""+messageListArrayList.get(position).getTimeStamp())),fmtOut.format(date1)));
        if (!messageListArrayList.get(position).getMessage().equals("")) {
            holder.content.setText(messageListArrayList.get(position).getMessage());
        } else {
            holder.content.setText("image");
        }
        System.out.println("msg getUnreadCount ==> "+messageListArrayList.get(position).getUnreadCount());
        if (messageListArrayList.get(position).getUnreadCount()>0) {
            holder.tvUnreadCount.setVisibility(View.VISIBLE);
            holder.tvUnreadCount.setText(""+messageListArrayList.get(position).getUnreadCount());
            holder.title.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.date.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.tvUnreadCount.setVisibility(View.GONE);
            holder.title.setTextColor(context.getResources().getColor(R.color.black));
            holder.date.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (!messageListArrayList.get(position).isThisGroup()) {

            final DatabaseReference ref = database.getReference("users").child(messageListArrayList.get(position).getReceiverUserID());
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String receiverName = (String) dataSnapshot.child("receiverName").getValue();
                        String receiverImage = (String) dataSnapshot.child("receiverImage").getValue();
                        String receiverUserID = (String) dataSnapshot.child("receiverUserID").getValue();
                        String receiverFcmKey = (String) dataSnapshot.child("receiverFcmKey").getValue();
                        String receiverRole = (String) dataSnapshot.child("receiverRole").getValue();
                        String receiverClass = (String) dataSnapshot.child("receiverClass").getValue();

                        holder.title.setText(receiverName);
                        RequestOptions error = new RequestOptions()
                                /*.centerCrop()*/
                                .placeholder(R.drawable.ic_user)
                                .error(R.drawable.ic_user)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .priority(Priority.HIGH);
                        if (!receiverImage.equals("")) {
                            Glide.with(context.getApplicationContext()).load(receiverImage).apply(error).into(holder.profile_image);
                        } else {
                            Glide.with(context.getApplicationContext()).load(R.drawable.ic_user).apply(error).into(holder.profile_image);
                        }

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!messageListArrayList.get(position).isThisGroup()) {
                                    Intent intent = new Intent(context, MessageActivity.class);
                                    intent.putExtra("ReceiverID", receiverUserID);
                                    intent.putExtra("ReceiverName", receiverName);
                                    intent.putExtra("ReceiverImage", receiverImage);
                                    intent.putExtra("ReceiverFcmKey", receiverFcmKey);
                                    context.startActivity(intent);
                                }
                            }
                        });

                    } else {
                        Glide.with(context.getApplicationContext()).load(R.drawable.ic_user).into(holder.profile_image);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        } else {
            holder.title.setText(Html.fromHtml(messageListArrayList.get(position).getGroupName()));
                RequestOptions error = new RequestOptions()
                        /*.centerCrop()*/
                        .placeholder(R.drawable.ic_user)
                        .error(R.drawable.ic_user)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);

                Glide.with(context.getApplicationContext()).load(messageListArrayList.get(position).getGroupImage()).apply(error).into(holder.profile_image);
        }


        if (messageListArrayList.get(position).isThisGroup()) {
            recFcmKeyList.clear();
            for (int i = 0; i < messageListArrayList.get(position).getReceiverUserIDD().size(); i++) {

                final DatabaseReference ref = database.getReference("users").child(messageListArrayList.get(position).getReceiverUserIDD().get(i));
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            String receiverFcmKey = (String) dataSnapshot.child("receiverFcmKey").getValue();
                            System.out.println("receiverFcmKey ==> " + receiverFcmKey);
                            if (!recFcmKeyList.contains(receiverFcmKey)) {
                                recFcmKeyList.add(receiverFcmKey);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

            }

        }


        /*if (messageListArrayList.get(position).isThisGroup()) {
            final DatabaseReference ref_message = database.getReference("group_message").child(messageListArrayList.get(position).getMsgID());
            ref_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    count1.clear();
                    if (dataSnapshot.exists()) {
                        String messageeee = null;
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            String readBy = (String) dataSnapshot.child(key).child("readBy").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            //messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                            if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                count1.add(receiverUserID);
                            }
                        }

                        if (count1.size() > 0) {
                            holder.tvUnreadCount.setVisibility(View.VISIBLE);
                            holder.tvUnreadCount.setText("" + count1.size());
                        } else {
                            holder.tvUnreadCount.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } else {
            final DatabaseReference ref_message = database.getReference("message").child(messageListArrayList.get(position).getMsgID());
            ref_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    count2.clear();
                    if (dataSnapshot.exists()) {
                        String messageeee = null;
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                            if (sharedPreferences.getString(Constants.USERID, null).equals(receiverUserID) && readStatus == 0) {
                                count2.add(receiverUserID);
                            }
                        }

                        if (count2.size() > 0) {
                            holder.tvUnreadCount.setVisibility(View.VISIBLE);
                            holder.tvUnreadCount.setText("" + count2.size());
                        } else {
                            holder.tvUnreadCount.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }*/


         /*Picasso.get().load(messageListArrayList.get(position).getReceiverImage()).
                    into(holder.profile_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                }
                            }, 1000);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.profile_image.setImageResource(R.drawable.ic_user);
                        }
                    });*/


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageListArrayList.get(position).isThisGroup()) {
                    System.out.println("recFcmKeyList ==> "+recFcmKeyList.size());
                   /* Intent intent = new Intent(context, MessageActivity.class);
                    intent.putExtra("ReceiverID", messageListArrayList.get(position).getReceiverUserID());
                    intent.putExtra("ReceiverName", messageListArrayList.get(position).getReceiverName());
                    intent.putExtra("ReceiverImage", messageListArrayList.get(position).getReceiverImage());
                    intent.putExtra("ReceiverFcmKey", messageListArrayList.get(position).getReceiverFcmKey());
                    context.startActivity(intent);
                } else {*/
                    Intent intent = new Intent(context, GroupMessageActivity.class);
                    intent.putExtra("GroupName", messageListArrayList.get(position).getGroupName());
                    intent.putExtra("GroupImage", messageListArrayList.get(position).getGroupImage());
                    intent.putExtra("GroupID", messageListArrayList.get(position).getGroupID());
                    intent.putExtra("ArrayReceiverID", messageListArrayList.get(position).getReceiverUserIDD());
                    intent.putExtra("ArrayReceiverName", messageListArrayList.get(position).getReceiverName());
                    intent.putExtra("ArrayUserList", messageListArrayList.get(position).getReceiverName());
                    intent.putExtra("ArrayReceiverImage", messageListArrayList.get(position).getReceiverImage());
                    intent.putStringArrayListExtra("ArrayReceiverFcmKey", recFcmKeyList);
                    context.startActivity(intent);
                }
            }
        });

        /*holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (messageListArrayList.get(position).isThisGroup()) {
                    Toast.makeText(context, "" + messageListArrayList.get(position).getGroupName(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "" + messageListArrayList.get(position).getReceiverName(), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });*/

    }

    public String covertTimeToText(Date pasTime,String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //Date pasTime = dateFormat.parse(dataDate);

        Date nowTime = new Date();

        long dateDiff = nowTime.getTime() - pasTime.getTime();

        long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
        long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
        long hour   = TimeUnit.MILLISECONDS.toHours(dateDiff);
        long day  = TimeUnit.MILLISECONDS.toDays(dateDiff);

        if (second < 60) {
            convTime = "Just Now"/*second+" Seconds "+suffix*/;
        } else if (minute < 60) {
            convTime = minute+" Minutes "+suffix;
        } else if (hour < 24) {
            convTime = hour+" Hours "+suffix;
        } else if (day >= 2) {
            String date[] = dataDate.split(" ");
            convTime = date[0];
            /*if (day > 30) {
                convTime = (day / 30)+" Months "+suffix;
            } else if (day > 360) {
                convTime = (day / 360)+" Years "+suffix;
            } else {
                convTime = (day / 7) + " Week "+suffix;
            }*/
        } else if (day < 2) {
            convTime = day+" Days "+suffix;
        }

        return convTime;

    }

    @Override
    public int getItemCount() {
        return messageListArrayList.size();
        // return 7;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, content, date,tvUnreadCount;
        ImageView profile_image;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            profile_image = (ImageView) view.findViewById(R.id.profile_image);
            content = (TextView) view.findViewById(R.id.detail_name);
            tvUnreadCount = (TextView) view.findViewById(R.id.tv_unread_count);
        }
    }


}


