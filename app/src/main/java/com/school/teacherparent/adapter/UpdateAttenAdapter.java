package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AttendanceUpdateStudentList;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;


/**
 * Created by keerthana on 10/10/2018.
 */


public class UpdateAttenAdapter extends RecyclerView.Adapter<UpdateAttenAdapter.MyViewHolder> {


    private Context context;
    public AddTouchListen addTouchListen;
    List<AttendanceUpdateStudentList> studentLists;

    String sessionTYPE;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;


    public UpdateAttenAdapter(List<AttendanceUpdateStudentList> studentLists, Context context, String sessionTYPE) {
            this.context=context;
            this.studentLists=studentLists;
            this.sessionTYPE=sessionTYPE;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  SwitchCompat switch_compat;
        private  TextView name;
        private RelativeLayout relative_class_spinner,relative_sub_spinner;
        public TextView title, subtitle, time;
        ImageView img,odImageview;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;
        private Spinner sub_spinner,class_spinner;
        RadioButton presentradiobutton,absentradiobutton;
        RadioGroup toggle;
        private CircularImageView classNameImgeview;
        //LabeledSwitch labeledSwitch ;
        public MyViewHolder(View view) {
            super(view);
            name=(TextView)view.findViewById(R.id.name);
           // img=(ImageView) view.findViewById(R.id.img);
            switch_compat=(SwitchCompat)view.findViewById(R.id.switch_compat);
            presentradiobutton=(RadioButton)view.findViewById(R.id.presentradiobutton);
            absentradiobutton=(RadioButton)view.findViewById(R.id.absentradiobutton);
            odImageview=(ImageView)view.findViewById(R.id.odImageview);
            toggle=(RadioGroup)view.findViewById(R.id.toggle);
            //labeledSwitch =view.findViewById(R.id.attendanceSwitch);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);



        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_attendance_view, parent, false);



        return new MyViewHolder(itemView);
    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       final AttendanceUpdateStudentList studentList = studentLists.get(position);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        if (studentList.getStudentsList().size()>0) {
            if (studentList.getStudentsList().get(0).getAttendance_type().equals("OD")) {
                if (studentList.getStudentsList().get(0).getTeacherIncharge() == sharedPreferences.getInt(Constants.ID,0)) {
                    holder.odImageview.setVisibility(View.GONE);
                    holder.toggle.setVisibility(View.VISIBLE);
                } else {
                    holder.odImageview.setImageDrawable(context.getResources().getDrawable(R.mipmap.onduty_icon));
                    holder.odImageview.setVisibility(View.VISIBLE);
                    holder.toggle.setVisibility(View.GONE);
                }
            } else if (studentList.getStudentsList().get(0).getAttendance_type().equals("Leave")) {
                holder.odImageview.setVisibility(View.VISIBLE);
                holder.toggle.setVisibility(View.GONE);
                if (studentList.getStudentsList().get(0).getIs_halfday() == 1) {
                    String sees = studentList.getStudentsList().get(0).getLeave_session();
                    String aaa = "";
                    if (sees.equals("AN")) {
                        aaa = "0";
                    } else if (sees.equals("FN")){
                        aaa = "1";
                    } else {

                    }

                    if (sessionTYPE.equals(aaa)) {
                        holder.odImageview.setImageDrawable(context.getResources().getDrawable(R.mipmap.leave_icons));
                    }/* else if ("FN".equals(studentList.getStudentsList().get(0).getLeave_session())) {
                        holder.odImageview.setImageDrawable(context.getResources().getDrawable(R.mipmap.leave_icons));
                    }*/ else {
                        holder.odImageview.setVisibility(View.GONE);
                        holder.toggle.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.odImageview.setImageDrawable(context.getResources().getDrawable(R.mipmap.leave_icons));
                }
                /**/
            } else {
                holder.odImageview.setVisibility(View.GONE);
                holder.toggle.setVisibility(View.VISIBLE);
            }
        }
      holder.name.setText(studentList.getFname()+" "+studentList.getLname());
      //holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path)+"/"+
                studentList.getStudent_photo();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });
        holder.presentradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked)
                    {
                        holder.presentradiobutton.setChecked(true);
                        holder.absentradiobutton.setChecked(false);
                        studentList.setSelectedID(studentList.getStudID());

                    }
            }
        });
        holder.absentradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    holder.absentradiobutton.setChecked(true);
                    holder.presentradiobutton.setChecked(false);
                    studentList.setSelectedID(0);
                }
            }
        });

            if (studentList.getSelectedID()==studentList.getStudID())
            {
                holder.presentradiobutton.setChecked(true);

            }
            else
            {
                holder.absentradiobutton.setChecked(true);
            }


    }

    @Override
    public int getItemCount() {
        return studentLists.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}
