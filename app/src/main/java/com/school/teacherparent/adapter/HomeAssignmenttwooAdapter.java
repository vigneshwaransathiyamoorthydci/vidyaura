package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassworkListAssHome;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HomeAssignmenttwooAdapter extends BaseAdapter
{
    private Context Context;
    ArrayList<ClassworkListAssHome> classwiseAssignmentList;
    public HomeAssignmenttwooAdapter(Context context, ArrayList<ClassworkListAssHome> classwiseHomeworktopicList) {
        this.Context = context;
        this.classwiseAssignmentList =classwiseHomeworktopicList;
    }



    @Override
    public int getCount() {
        return classwiseAssignmentList.size();
    }

    @Override
    public Object getItem(int position) {
        return classwiseAssignmentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    Context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.home_sub_itemone, null);

        }
        TextView chapterNametextview = (TextView) convertView.findViewById(R.id.chapterNametextview);
        TextView topicTextview = (TextView) convertView.findViewById(R.id.topicTextview);
        TextView duedate = (TextView) convertView.findViewById(R.id.duedate);
        TextView assigndate = (TextView) convertView.findViewById(R.id.assigndate);
        ImageView homeworkImage=(ImageView)convertView.findViewById(R.id.homeworkImage);
        TextView dueDateLabel=(TextView)convertView.findViewById(R.id.dueDateLabel);
        LinearLayout extraSpaceLineraLayout=(LinearLayout)convertView.findViewById(R.id.extraSpaceLineraLayout);
        extraSpaceLineraLayout.setVisibility(View.GONE);

        topicTextview.setText(classwiseAssignmentList.get(position).getTopic());
        chapterNametextview.setText(classwiseAssignmentList.get(position).getChapterName());
        duedate.setText(getDateFormat(classwiseAssignmentList.get(position).getDueDate()));
        assigndate.setText(getDateFormat(classwiseAssignmentList.get(position).getAssignedOn()));
        TextView markLabel = (TextView) convertView.findViewById(R.id.markLabel);
        markLabel.setText(""+classwiseAssignmentList.get(position).getMarks()+Context.getString(R.string.mark));

        if (classwiseAssignmentList.size()==position+1)
        {
            extraSpaceLineraLayout.setVisibility(View.GONE);
        }

        return convertView;
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd/MM/yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}
