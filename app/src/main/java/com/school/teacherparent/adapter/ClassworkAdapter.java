package com.school.teacherparent.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ClassworkList;
import com.school.teacherparent.models.ClassworkListResponse;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;


/**
 * Created by keerthana on 9/27/2018.
 */

public class ClassworkAdapter extends RecyclerView.Adapter<ClassworkAdapter.MyViewHolder> {


    ArrayList<ClassworkListResponse.classworkList>  homeworkArrayList;
    private Context context;
    BaseActivity baseActivity;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;


    public ClassworkAdapter(ArrayList<ClassworkListResponse.classworkList> homeworkArrayList, Context context, BaseActivity baseActivity) {
        this.homeworkArrayList = homeworkArrayList;
        this.context =context;
        this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  TextView subname,classname,homeworkduetodaylabel,todayshomeworkLabel,subtitle1,subtitle2,date1,date2,className,homeworkcountsTextview;
        private  ImageView menu,img2,img3,subjectImage;
        public TextView counts;
        ListView duoetodayListview,duotommrowListview;



        public MyViewHolder(View view) {
            super(view);


            counts = (TextView) view.findViewById(R.id.counts);
            menu=(ImageView)view.findViewById(R.id.menu);
            img2=(ImageView)view.findViewById(R.id.img2);
            img3=(ImageView)view.findViewById(R.id.img3);
            todayshomeworkLabel = (TextView) view.findViewById(R.id.todayshomeworkLabel);
            classname = (TextView) view.findViewById(R.id.classname);
            subname = (TextView) view.findViewById(R.id.subname);

            date1 = (TextView) view.findViewById(R.id.date1);
            date2 = (TextView) view.findViewById(R.id.date2);
            subtitle1 = (TextView) view.findViewById(R.id.subtitle1);
            homeworkduetodaylabel = (TextView) view.findViewById(R.id.homeworkduetodaylabel);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            duoetodayListview=(ListView)view.findViewById(R.id.duoetodayListview);
            duotommrowListview=(ListView)view.findViewById(R.id.duotommrowListview);
            subjectImage=(ImageView)view.findViewById(R.id.subjectImage);
            className=(TextView) view.findViewById(R.id.className);
            homeworkcountsTextview=(TextView)view.findViewById(R.id.homeworkcountsTextview);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classwork_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ClassworkListResponse.classworkList homeworkList = homeworkArrayList.get(position);
//        holder.classname.setText(context.getString(R.string.class_name)+homeworkList.getClasswiseHomework().get(position).getClassName()
//                +homeworkList.getClasswiseHomework().get(position).getSection());
//        holder.subname.setText(homeworkList.getClasswiseHomework().get(position).getSubjectName());
        holder.subjectImage.setImageResource(Constants.imgid[position]);
        holder.className.setText(homeworkList.getClassName()+homeworkList.getSection());

//        for (int i = 0; i < homeworkList.getTodaysClassworkProgress().size(); i++) {
//            holder.classname.setText(context.getString(R.string.class_name) + homeworkList.getTodaysClassworkProgress().get(i).getChapterName()
//                    + homeworkList.getTodaysClassworkProgress().get(i).getSection());
//            holder.subname.setText(homeworkList.getClasswiseHomework().get(i).getSubjectName());
//            holder.homeworkcountsTextview.setText(""+homeworkList.getClasswiseHomework().get(i).getTotalHomeworkCount());
//
//
//
//
//
//
//
//
            }














    @Override
    public int getItemCount() {
        return homeworkArrayList .size();
    }

}