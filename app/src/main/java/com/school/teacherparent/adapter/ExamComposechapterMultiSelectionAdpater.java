package com.school.teacherparent.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.models.ListofChapterforDialog;

import java.util.List;


public class ExamComposechapterMultiSelectionAdpater extends BaseAdapter {

    private final LayoutInflater mInflater;
    List<ListofChapterforDialog> chapterList;
    List<ListofChapterforDialog> allchapterList;
    Context context;
    private boolean[]  mSelected ;
    AddClassWorkFragment addClassWorkFragment;
    private ClassAdapter.AddTouchListen addTouchListen;
    Dialog chaDialog;
    Button dialogChapterButton;
    CheckBox selectAllCheckbox;
    public ExamComposechapterMultiSelectionAdpater(List<ListofChapterforDialog> chapterList, Context context, Dialog chapterDialog, Button dialogChapterButton, List<ListofChapterforDialog> allchapterList, CheckBox selectAllCheckbox) {
        this.chapterList = chapterList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSelected = new boolean[chapterList.size()];
        addClassWorkFragment=new AddClassWorkFragment();
        this.chaDialog=chapterDialog;
        this.dialogChapterButton=dialogChapterButton;
        this.allchapterList=allchapterList;
        this.selectAllCheckbox=selectAllCheckbox;


    }


    @Override
    public int getCount() {
        return chapterList.size();
    }

    @Override
    public ListofChapterforDialog getItem(int position) {
        return chapterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    public void setOnClickListen(ClassAdapter.AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
            TextView txt = (TextView) convertView.findViewById(R.id.text_checkbox);
            txt.setText(chapterList.get(position).getChapter_name());
            final CheckBox checkbox_spinner_item_one=(CheckBox)convertView.findViewById(R.id.checkbox_spinner_item_one);

            if (chapterList.get(position).getSelectedID()==0)
            {
                checkbox_spinner_item_one.setChecked(false);
            }
            else
            {
                checkbox_spinner_item_one.setChecked(true);
            }
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    if (chapterList.get(position).getSelectedID()==0)
                    {
                        checkbox_spinner_item_one.setChecked(true);
                        chapterList.get(position).setSelectedID(chapterList.get(position).getId());
                        for (int u=0;u<chapterList.size();u++)
                        {
                            if (chapterList.get(u).getSelectedID()==chapterList.get(u).getId())
                            {
                                selectAllCheckbox.setChecked(true);
                                continue;
                            }
                            else
                            {
                                selectAllCheckbox.setChecked(false);
                                break;
                            }
                        }
                    }
                    else
                    {
                        checkbox_spinner_item_one.setChecked(false);
                        chapterList.get(position).setSelectedID(0);
                        selectAllCheckbox.setChecked(false);
                    }

                }
            });
            checkbox_spinner_item_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    if (isChecked) {

                        chapterList.get(position).setSelectedID(chapterList.get(position).getId());
                        for (int u=0;u<chapterList.size();u++)
                        {
                            if (chapterList.get(u).getSelectedID()==chapterList.get(u).getId())
                            {
                                selectAllCheckbox.setChecked(true);
                                continue;
                            }
                            else
                            {
                                selectAllCheckbox.setChecked(false);
                                break;
                            }
                        }
                    }
                    else
                    {

                        chapterList.get(position).setSelectedID(0);
                        selectAllCheckbox.setChecked(false);

                    }



                }
            });



        selectAllCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    System.out.println("Checked");
                    for (int y = 0; y < chapterList.size(); y++) {
                        chapterList.get(y).setSelectedID(chapterList.get(y).getId());
                    }
                    notifyDataSetChanged();
                } else {
                    System.out.println("Un-Checked");
                    for (int y = 0; y < chapterList.size(); y++) {
                        chapterList.get(y).setSelectedID(0);
                    }
                    notifyDataSetChanged();
                }
            }
        });
            return convertView;
        }


    public boolean[] getSelected() {
        return this.mSelected;
    }
    }




