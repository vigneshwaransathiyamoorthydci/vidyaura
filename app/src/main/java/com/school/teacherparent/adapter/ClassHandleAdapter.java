package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassHandled;
import com.school.teacherparent.models.TeachersListforChatResponse;

import java.util.ArrayList;

class ClassHandleAdapter extends RecyclerView.Adapter<ClassHandleAdapter.ViewHolder> {

    Context context;
    ArrayList<ClassHandled.chatTeachersDetailsList> chatTeachersDetailsLists;

    public ClassHandleAdapter(Context context, ArrayList<ClassHandled.chatTeachersDetailsList> chatTeachersDetailsLists) {
        this.context = context;
        this.chatTeachersDetailsLists = chatTeachersDetailsLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_class_handle_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            holder.tvSubjectName.setText(chatTeachersDetailsLists.get(position).getSubjectName());
            holder.tvClassName.setText(chatTeachersDetailsLists.get(position).getAltName()+chatTeachersDetailsLists.get(position).getSectionName());
    }

    @Override
    public int getItemCount() {
        return chatTeachersDetailsLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvClassName,tvSubjectName;

        public ViewHolder(View view) {
            super(view);
            tvClassName = view.findViewById(R.id.tv_class_name);
            tvSubjectName = view.findViewById(R.id.tv_subject_name);
        }
    }
}
