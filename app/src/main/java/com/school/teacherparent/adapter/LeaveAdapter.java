package com.school.teacherparent.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.models.LeaveListModel;
import com.school.teacherparent.models.LeaveModel;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.MyViewHolder> {

    private List<LeaveModel.LeaveDetailsList> leaveList=new ArrayList<>();
    private Context context;
    LeaveListActivity leaveListActivity;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time,timeTextview;
        ImageView studentImageview,approvalEdit;
        LinearLayout lin,todateLinearlayout;
        public TextView messageparent,desc,subdesc,fromdateTitle,date,approvalStatusTitle,approvalStatus,classnameTextview,todate;
        private CircularImageView classNameImgeview;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
            time = (TextView) view.findViewById(R.id.time);
           // studentImageview=(ImageView)view.findViewById(R.id.studentImageview);
            messageparent =(TextView)view.findViewById(R.id.messageparent);
            desc=(TextView)view.findViewById(R.id.desc);
           subdesc=(TextView)view.findViewById(R.id.subdesc);
           date=(TextView)view.findViewById(R.id.date);
           fromdateTitle=(TextView)view.findViewById(R.id.fromdateTitle);
           approvalStatusTitle=(TextView)view.findViewById(R.id.approval_status_title);
            approvalStatus=(TextView)view.findViewById(R.id.approval_status);
            approvalEdit=(ImageView)view.findViewById(R.id.approval_edit);



           time=(TextView)view.findViewById(R.id.time);
            classnameTextview=(TextView)view.findViewById(R.id.classnameTextview);
            timeTextview=(TextView)view.findViewById(R.id.timeTextview);
            todate=(TextView)view.findViewById(R.id.todate);
            todateLinearlayout=(LinearLayout)view.findViewById(R.id.todateLinearlayout);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);
        }
    }


    public LeaveAdapter(List<LeaveModel.LeaveDetailsList> leaveList, Context context, LeaveListActivity leaveListActivity) {
        this.leaveList = leaveList;
        this.context =context;
        this.leaveListActivity = leaveListActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        LeaveModel.LeaveDetailsList leaveDetailsList = leaveList.get(position);
        holder.title.setText(leaveDetailsList.getStudentDetails().getFname()+" "+leaveDetailsList.getStudentDetails().getLname());
      //  holder.subtitle.setText(leaveDetailsList.get());
        holder.desc.setText(leaveDetailsList.getLeaveReason());
        holder.subdesc.setText("Reason:  "+leaveDetailsList.getDescription());

        if(leaveDetailsList.getFrom_date().trim().equals(leaveDetailsList.getTo_date().trim())){
            holder.todateLinearlayout.setVisibility(View.GONE);
            holder.fromdateTitle.setText("Date:");
            holder.date.setText(getDateFormatforhomework(leaveDetailsList.getFrom_date()));
        }else{
            holder.todateLinearlayout.setVisibility(View.VISIBLE);
            holder.fromdateTitle.setText("From:");
            holder.date.setText(getDateFormatforhomework(leaveDetailsList.getFrom_date()));
            holder.todate.setText(getDateFormatforhomework(leaveDetailsList.getTo_date()));
        }

        holder.messageparent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leaveDetailsList.getParentDetails() != null) {
                    Intent intent = new Intent(context, MessageActivity.class);
                    intent.putExtra("ReceiverID", leaveDetailsList.getParentEncryptedID());
                    intent.putExtra("ReceiverName", leaveDetailsList.getParentDetails().getFname());
                    intent.putExtra("ReceiverImage", leaveDetailsList.getParentDetails().getPhoto());
                    intent.putExtra("ReceiverFcmKey", leaveDetailsList.getParentDetails().getFcm_key());
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "No Parent data found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.classnameTextview.setText(leaveDetailsList.getStudentDetails().getAlt_name()+" "+leaveDetailsList.getStudentDetails().getSection());
      //  holder.time.setText(leaveDetailsList.getTime());
       // holder.studentImageview.setImageResource(R.mipmap.student_image);

        switch (leaveDetailsList.getLeave_status()) {
            case "In progress":
                holder.approvalStatus.setText("In progress");
                holder.approvalStatus.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                holder.approvalEdit.setVisibility(View.VISIBLE);
                break;
            case "Approved":
                holder.approvalStatus.setText("Approved");
                holder.approvalStatus.setTextColor(context.getResources().getColor(R.color.pdlg_color_green));
                holder.approvalEdit.setVisibility(View.GONE);
                break;
            case "Declined":
                holder.approvalStatus.setText("Declined");
                holder.approvalStatus.setTextColor(context.getResources().getColor(R.color.red));
                holder.approvalEdit.setVisibility(View.GONE);
                break;
        }



        if (leaveDetailsList.getStudentDetails().getStudent_photo()!=null)
        {
            String url=context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_student_path)+"/"+leaveDetailsList.getStudentDetails().getStudent_photo();

            RequestOptions error = new RequestOptions()
                    /*.centerCrop()*/
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context.getApplicationContext()).load(url).apply(error).into(holder.classNameImgeview);

          /*  Picasso.get().load(url).


                    into( holder.classNameImgeview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                }
                            },1000);


                        }

                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onError(Exception e) {
                            holder.classNameImgeview.setImageDrawable(context.getDrawable(R.drawable.ic_user));
                        }
                    });*/

        }
        else
        {
            holder.classNameImgeview.setImageDrawable(context.getDrawable(R.drawable.ic_user));
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

            Date oldDate = dateFormat.parse(leaveDetailsList.getCreated_at());
            System.out.println(oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.before(currentDate)) {

//                Log.e("oldDate", "is previous date");
//                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
                if (seconds <= 100) {
                    holder.timeTextview.setText("Just now");
                } else if (minutes <= 10) {
                    holder.timeTextview.setText(minutes+ " "+"Minute ago");
                } else if (hours <= 24) {
                    switch ((int) hours) {
                        case 0:
                            holder.timeTextview.setText("1 Hour ago");
                            break;
                        case 1:
                            holder.timeTextview.setText("1 Hour ago");
                            break;
                        case 2:
                            holder.timeTextview.setText("2 Hour ago");
                            break;
                        case 3:
                            holder.timeTextview.setText("3 Hour ago");
                            break;
                        case 4:
                            holder.timeTextview.setText("4 Hour ago");
                            break;
                        case 5:
                            holder.timeTextview.setText("5 Hour ago");
                            break;
                        case 6:
                            holder.timeTextview.setText("6 Hour ago");
                            break;
                        case 7:
                            holder.timeTextview.setText("7 Hour ago");
                            break;
                        case 8:
                            holder.timeTextview.setText("8 Hour ago");
                            break;
                        case 9:
                            holder.timeTextview.setText("9 Hour ago");
                            break;
                        case 10:
                            holder.timeTextview.setText("10 Hour ago");
                            break;
                        case 11:
                            holder.timeTextview.setText("11 Hour ago");
                            break;
                        case 12:
                            holder.timeTextview.setText("12 Hour ago");
                            break;
                        case 13:
                            holder.timeTextview.setText("13 Hour ago");
                            break;
                        case 14:
                            holder.timeTextview.setText("14 Hour ago");
                            break;
                        case 15:
                            holder.timeTextview.setText("15 Hour ago");
                            break;
                        case 16:
                            holder.timeTextview.setText("16 Hour ago");
                            break;
                        case 17:
                            holder.timeTextview.setText("17 Hour ago");
                            break;
                        case 18:
                            holder.timeTextview.setText("18 Hour ago");
                            break;
                        case 19:
                            holder.timeTextview.setText("19 Hour ago");
                            break;
                        case 20:
                            holder.timeTextview.setText("20 Hour ago");
                            break;
                        case 21:
                            holder.timeTextview.setText("21 Hour ago");
                            break;
                        case 22:
                            holder.timeTextview.setText("22 Hour ago");
                            break;
                        case 23:
                            holder.timeTextview.setText("23 Hour ago");
                            break;
                        case 24:
                            holder.timeTextview.setText("24 Hour ago");
                            break;

                    }
                } else if (days <= 2) {
                    switch ((int) days) {
                        case 0:
                            holder.timeTextview.setText("1 Day ago");
                            break;
                        case 1:
                            holder.timeTextview.setText("2 Day ago");
                            break;
                        case 2:
                            holder.timeTextview.setText("3 Day ago");
                            break;
                    }

                } else {
                    holder.timeTextview.setText(getDateFormatforhomework(leaveDetailsList.getCreated_at()));
                }
            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        }

        holder.approvalEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approvalDialog(leaveDetailsList.getLeaveID(),leaveDetailsList.getLeave_status());
            }
        });

    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }

    private void approvalDialog(int leaveId,String leaveStatus) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_leave_approval);
        dialog.show();
        TextView tvHead = dialog.findViewById(R.id.tv_head);
        Button btnApprovalStatus = dialog.findViewById(R.id.btn_approval);
        EditText etRemarks = dialog.findViewById(R.id.et_remarks);
        CheckBox checkboxApprove = dialog.findViewById(R.id.checkbox_approve);
        TextView textApprove = dialog.findViewById(R.id.text_approve);
        CheckBox checkboxReject = dialog.findViewById(R.id.checkbox_reject);
        TextView textReject = dialog.findViewById(R.id.text_reject);
        ImageView ivClose = dialog.findViewById(R.id.iv_close);

        tvHead.setText("Leave Approval");
        textApprove.setText("Approve");
        textReject.setText("Declined");
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        checkboxApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkboxApprove.setChecked(true);
                checkboxReject.setChecked(false);
            }
        });

        checkboxReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkboxApprove.setChecked(false);
                checkboxReject.setChecked(true);

            }
        });

        btnApprovalStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etRemarks.getText().toString().trim().length() > 1 &&(checkboxApprove.isChecked() || checkboxReject.isChecked())){
                    etRemarks.setError(null);
                    if(checkboxApprove.isChecked()){
                        leaveListActivity.updateLeave(leaveId,"Approved",etRemarks.getText().toString());
                    dialog.dismiss();}
                    else if(checkboxReject.isChecked()){
                        leaveListActivity.updateLeave(leaveId,"Declined",etRemarks.getText().toString());
                    dialog.dismiss();}
                }else{
                    if (etRemarks.getText().toString().trim().length() <= 1) {
                        etRemarks.setError("Enter Remarks");
                        Toast.makeText(context, "Enter Remarks", Toast.LENGTH_SHORT).show();
                    } else if (checkboxApprove.isChecked() && checkboxReject.isChecked()){
                        etRemarks.setError(null);
                        Toast.makeText(context, "Please select any one option", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public String getDateFormatforhomework(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd/MM/yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}