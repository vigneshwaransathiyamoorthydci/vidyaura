package com.school.teacherparent.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.models.ListofChapterforDialog;

import java.util.List;


public class ExamComposechapterAdpater extends BaseAdapter {

    private final LayoutInflater mInflater;
    List<ListofChapterforDialog> chapterList;
    List<ListofChapterforDialog> allchapterList;
    Context context;
    private boolean[]  mSelected ;
    AddClassWorkFragment addClassWorkFragment;
    private ClassAdapter.AddTouchListen addTouchListen;
    Dialog chaDialog;
    Button dialogChapterButton;

    public ExamComposechapterAdpater(List<ListofChapterforDialog> chapterList, Context context, Dialog chapterDialog, Button dialogChapterButton, List<ListofChapterforDialog> allchapterList) {
        this.chapterList = chapterList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSelected = new boolean[chapterList.size()];
        addClassWorkFragment=new AddClassWorkFragment();
        this.chaDialog=chapterDialog;
        this.dialogChapterButton=dialogChapterButton;
        this.allchapterList=allchapterList;


    }


    @Override
    public int getCount() {
        return chapterList.size();
    }

    @Override
    public ListofChapterforDialog getItem(int position) {
        return chapterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    public void setOnClickListen(ClassAdapter.AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

            convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
            TextView txt = (TextView) convertView.findViewById(R.id.text_checkbox);
            txt.setText(chapterList.get(position).getChapter_name());
            final CheckBox checkbox_spinner_item_one=(CheckBox)convertView.findViewById(R.id.checkbox_spinner_item_one);

            if (chapterList.get(position).getSelectedID()==0)
            {
                checkbox_spinner_item_one.setChecked(false);
            }
            else
            {
                checkbox_spinner_item_one.setChecked(true);
            }
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    for (int y=0;y<allchapterList.size();y++)
                        {
                            if (allchapterList.get(y).getSelectedID()!=0)
                            {
                                allchapterList.get(y).setSelectedID(0);
                                checkbox_spinner_item_one.setChecked(false);
                            }
                        }

                    if (chapterList.get(position).getSelectedID()==0)
                    {
                        checkbox_spinner_item_one.setChecked(true);
                        chapterList.get(position).setSelectedID(chapterList.get(position).getId());
                    }
                    else
                    {
                        checkbox_spinner_item_one.setChecked(false);
                        chapterList.get(position).setSelectedID(0);
                    }
                    //chaDialog.hide();
                    //dialogChapterButton.performClick();
                }
            });
            checkbox_spinner_item_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    for (int y=0;y<allchapterList.size();y++)
                    {
                        if (allchapterList.get(y).getSelectedID()!=0)
                        {
                            allchapterList.get(y).setSelectedID(0);
                            checkbox_spinner_item_one.setChecked(false);
                        }
                    }
                    if (isChecked) {
                        checkbox_spinner_item_one.setChecked(true);
                        chapterList.get(position).setSelectedID(chapterList.get(position).getId());
                    }
                    else
                    {
                        checkbox_spinner_item_one.setChecked(false);
                        chapterList.get(position).setSelectedID(0);

                    }
                    chaDialog.hide();
                    dialogChapterButton.performClick();


                }
            });

            return convertView;
        }


    public boolean[] getSelected() {
        return this.mSelected;
    }
    }




