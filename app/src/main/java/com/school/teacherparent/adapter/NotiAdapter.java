package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.models.NotiDTO;
import com.school.teacherparent.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by keerthana on 9/18/2018.
 */

public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.MyViewHolder> {

    private  CardView mCardViewBottom;
    private  LinearLayout top;
    private List<NotiDTO> notiList=new ArrayList<>();
    private Context context;
    private LinearLayout header;
    public AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, time;
        ImageView notiimg,clientimg,whiteimg,userimg;
        CircularImageView img;
        LinearLayout lin;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            img=(CircularImageView)view.findViewById(R.id.img);
            desc=(TextView)view.findViewById(R.id.desc);
            header=(LinearLayout)view.findViewById(R.id.header);
            notiimg=(ImageView)view.findViewById(R.id.noti);
            mCardViewBottom = (CardView)view.findViewById(R.id.top);
            mCardViewBottom.setCardBackgroundColor(Color.parseColor("#ffffff"));
            whiteimg=(ImageView)view.findViewById(R.id.white);
           // userimg=(ImageView) view.findViewById(R.id.userwhiteimg);

            clientimg=(ImageView)view.findViewById(R.id.clientimg);
        }
    }


    public NotiAdapter(List<NotiDTO> notiList, Context context) {
        this.notiList = notiList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotiDTO notify = notiList.get(position);
        if (notify.getType().equals("message")) {
            if (notify.isGroup()) {
                holder.title.setText(notify.getSenderName().trim() + " sent a group message");
            } else {
                holder.title.setText(notify.getSenderName().trim() + " sent you a message");
            }
        } else {
            holder.title.setText(notify.getNotification_message());
        }


        holder.desc.setText(Html.fromHtml("<span style=\\\"background-color: rgb(0, 0, 255); color: rgb(247, 247, 247) ; font-weight: bold;\\\">class coordinators<\\/span>"+notify.getDescription()));

        if (!notify.getImageurl().equals("")) {
            RequestOptions error = new RequestOptions()
                    /*.centerCrop()*/
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context.getApplicationContext()).load(notify.getImageurl()).apply(error).into(holder.img);
        }

        if (notify.getType_document() != null) {
            if (!notify.getType_document().equals("")) {
                System.out.println("getType_document ==> "+notify.getType_document());
                RequestOptions error = new RequestOptions()
                        /*.centerCrop()*/
                        .placeholder(R.drawable.gallery)
                        .error(R.drawable.gallery)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);
                holder.clientimg.setVisibility(View.VISIBLE);
                if (!notify.getType().equals("message")) {
                    if (notify.getType_document().contains("png") || notify.getType_document().contains("jpg") || notify.getType_document().contains("jpeg")) {
                        Glide.with(context.getApplicationContext()).load(notify.getType_document()).apply(error).into(holder.clientimg);
                    } else if (notify.getType_document().contains("mp4") || notify.getType_document().contains("3gp")) {
                        Glide.with(context.getApplicationContext()).load(notify.getType_document()).apply(error).into(holder.clientimg);
                    } else {
                        Glide.with(context.getApplicationContext()).load(R.drawable.audio).apply(error).into(holder.clientimg);
                    }
                } else {
                    Glide.with(context.getApplicationContext()).load(notify.getType_document()).apply(error).into(holder.clientimg);
                }
            }
        }
        System.out.println("getCreated_at ===> 2 "+notify.getCreated_at());
        if (notify.getType().equals("message")) {
            try {
            SimpleDateFormat dateFormatParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String targetDate = notify.getCreated_at();
            Date dateString = dateFormatParse.parse(targetDate);
            String date[] = notify.getCreated_at().split(" ");
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = fmt.parse(date[0]);

                SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yy");
                fmtOut.format(date1);
            holder.time.setText(covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), fmtOut.format(date1)));
                //System.out.println("getCreated_at ==> 1 "+covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), date[0]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {

            try {
                SimpleDateFormat dateFormatParse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String targetDate = notify.getCreated_at();
                Date dateString = dateFormatParse.parse(targetDate);
                String date[] = notify.getCreated_at().split(" ");
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = fmt.parse(date[0]);

                SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yy");
                fmtOut.format(date1);
                holder.time.setText(covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), fmtOut.format(date1)));
                //System.out.println("getCreated_at ==> 2 "+covertTimeToText(new Date(Long.parseLong("" + dateString.getTime())), date[0]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

/*
        if(position==0)
        {
            holder.img.setVisibility(View.GONE);
            holder.clientimg.setVisibility(View.GONE);
            holder.notiimg.setVisibility(View.VISIBLE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
          header.setBackgroundColor(Color.parseColor("#f2f9ff"));

        }
        if (position==1)
        {
            holder.img.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.VISIBLE);
            holder.clientimg.setVisibility(View.GONE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
 header.setBackgroundColor(Color.parseColor("#f2f9ff"));


        }
        if (position==2 || position==4 || position ==5)
        {
            holder.img.setVisibility(View.GONE);
            holder.whiteimg.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.GONE);
            holder.clientimg.setVisibility(View.GONE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
    //  lin.setBackgroundColor(Color.parseColor("#ffffff"));

        }
        if (position==3)
        {
            holder.img.setVisibility(View.VISIBLE);
            holder.notiimg.setVisibility(View.GONE);
            holder.whiteimg.setVisibility(View.VISIBLE);
            holder.clientimg.setVisibility(View.VISIBLE);
            holder.desc.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
     // lin.setBackgroundColor(Color.parseColor("#ffffff"));
        }*/
    }

    public String covertTimeToText(Date pasTime, String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //Date pasTime = dateFormat.parse(dataDate);

        Date nowTime = new Date();

        long dateDiff = nowTime.getTime() - pasTime.getTime();

        long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
        long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
        long hour   = TimeUnit.MILLISECONDS.toHours(dateDiff);
        long day  = TimeUnit.MILLISECONDS.toDays(dateDiff);

        if (second < 60) {
            convTime = "Just Now"/*second+" Seconds "+suffix*/;
        } else if (minute < 60) {
            convTime = minute+" Minutes "+suffix;
        } else if (hour < 24) {
            convTime = hour+" Hours "+suffix;
        } else if (day >= 2) {
            //String date[] = dataDate.split(" & ");
            convTime = dataDate;
            /*if (day > 30) {
                convTime = (day / 30)+" Months "+suffix;
            } else if (day > 360) {
                convTime = (day / 360)+" Years "+suffix;
            } else {
                convTime = (day / 7) + " Week "+suffix;
            }*/
        } else if (day < 2) {
            convTime = day+" Days "+suffix;
        }

        return convTime;

    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return notiList.size();
    }
}