package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.MyclassesResponse;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 9/25/2018.
 */

public class MyClassAdapter extends RecyclerView.Adapter<MyClassAdapter.MyViewHolder> {



    private Context context;
    List<MyclassesResponse.classesProfileDetails> classList;
    public MyClassAdapter(List<MyclassesResponse.classesProfileDetails> classList, Context context) {
        this.classList=classList;
        this.context=context;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView classes,counts,timing;
        CircularImageView subjectImage;


        public MyViewHolder(View view) {
            super(view);


            classes = (TextView) view.findViewById(R.id.classes);
            counts = (TextView) view.findViewById(R.id.counts);
            timing = (TextView) view.findViewById(R.id.timing);
            subjectImage = (CircularImageView) view.findViewById(R.id.subjectImage);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myclass_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        MyclassesResponse.classesProfileDetails classesProfileDetails = classList.get(position);
        //holder.subjectImage.setImageResource(Constants.imgid[position]);
        holder.classes.setText(classesProfileDetails.getName());
        holder.counts.setText(classesProfileDetails.getClassName()+""+classesProfileDetails.getSection());
        holder.timing.setText(context.getString(R.string.student_count)+classesProfileDetails.getStudCount());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+
                classesProfileDetails.getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.subjectImage.setImageResource(R.drawable.nopreview);
            }
        });

    }

    @Override
    public int getItemCount() {
        return classList.size();
    }
}