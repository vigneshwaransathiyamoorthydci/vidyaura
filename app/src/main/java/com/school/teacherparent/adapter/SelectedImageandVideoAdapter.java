package com.school.teacherparent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.R;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SelectedImageandVideoList;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class SelectedImageandVideoAdapter extends RecyclerView.Adapter<SelectedImageandVideoAdapter.MyViewHolder> {

    Context context;
    BitmapFactory.Options btmapOptions;
    private List<SelectedImageandVideoList> imagelist;
    OnImageRemoved onImageRemoved;

    public SelectedImageandVideoAdapter(List<SelectedImageandVideoList> imagelist, Context context, OnImageRemoved onImageRemoved) {
        this.imagelist = imagelist;
        this.context = context;
        this.onImageRemoved=onImageRemoved;
        btmapOptions = new BitmapFactory.Options();
        btmapOptions.inSampleSize = 2;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_selected_image_video, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.d("uri", "onBindViewHolder: "+imagelist.get(position));

        if (imagelist.get(position).getImage().contains("amazonaws.com")){
            Picasso.get().load(imagelist.get(position).getImage()).into(holder.mainimage);
            Log.d("loadusr", "onBindViewHolder: url");
        }
        else {

            if (imagelist.get(position).getExtension().equals("image")) {
                Bitmap bm = BitmapFactory.decodeFile(imagelist.get(position).getImage(),
                        btmapOptions);
                holder.mainimage.setVisibility(View.VISIBLE);
                holder.video_selected.setVisibility(View.INVISIBLE);
                holder.mainimage.setImageBitmap(bm);
            }
            else
            {
                holder.mainimage.setVisibility(View.INVISIBLE);
                holder.video_selected.setVisibility(View.VISIBLE);
                holder.video_selected.setUp(String.valueOf(Uri.fromFile(new File(imagelist.get(position).getImage()))), ""
                , Jzvd.SCREEN_WINDOW_NORMAL);
            }
            Log.d("loadusr", "onBindViewHolder: bitmap");

        }

//
//        Uri uri =  Uri.parse(imagelist.get(position));
//        Picasso.get().load(uri).into(holder.mainimage);
        

    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mainimage, close;
        JzvdStd video_selected;

        public MyViewHolder(final View view) {
            super(view);
            mainimage = view.findViewById(R.id.imagetemp);
            close = view.findViewById(R.id.close);
            video_selected=view.findViewById(R.id.video_selected);
            close.bringToFront();
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onImageRemoved.onimageremoved(getAdapterPosition(),v);
                }
            });

        }
    }
}