package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.models.CommentList;
import com.school.teacherparent.models.CommentReplyList;
import com.school.teacherparent.models.CommentReplyResponse;
import com.school.teacherparent.models.FeedClapListResponse;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by keerthana on 10/18/2018.
 */

public class CommentReplyAdapter extends RecyclerView.Adapter<CommentReplyAdapter.MyViewHolder> {

    List<CommentReplyList> commentReplyList;
    private Context context;
    int feedId;
    int userType;

    public CommentReplyAdapter( List<CommentReplyList> commentReplyList, Context context, int feedId, int userType) {

        this.commentReplyList=commentReplyList;
        this.context=context;
        this.feedId=feedId;
        this.userType=userType;
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name_textview, feedcomment_time_textview, comment_textview, replycount_textview, commentclapcount_textview, commentsCount, shareCount,replies_textview,reply_textview;
        public ImageView feedcomment_imageview,comment_clapimageview;

        public MyViewHolder(View view) {
            super(view);
            feedcomment_imageview=view.findViewById(R.id.feedcomment_imageview);
            name_textview=view.findViewById(R.id.name_textview);
            feedcomment_time_textview=view.findViewById(R.id.feedcomment_time_textview);
            comment_textview=view.findViewById(R.id.comment_textview);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_reply_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CommentReplyList commentReplydata = commentReplyList.get(position);

        holder.comment_textview.setText(Html.fromHtml(commentReplydata.getComment()));

        if(!isNullOrEmpty(commentReplydata.getLname()))
        {
            holder.name_textview.setText(commentReplydata.getFname()+" "+commentReplydata.getLname());
        }
        else
        {
            holder.name_textview.setText(commentReplydata.getFname());
        }
        if (commentReplydata.getEmp_photo()!=null)
        {
            String url = null;
            if (commentReplydata.getType().equals("teachers")) {
                url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + commentReplydata.getEmp_photo();
            } else if (commentReplydata.getType().equals("parents")) {
                url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/" + commentReplydata.getEmp_photo();
            }

            Picasso.get().load(url).


                    into( holder.feedcomment_imageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                }
                            },1000);


                        }

                        @Override
                        public void onError(Exception e) {
                            holder.feedcomment_imageview.setImageResource(R.drawable.ic_user);
                        }
                    });

        }
        else
        {
            holder.feedcomment_imageview.setImageResource(R.drawable.ic_user);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        try {


            Date oldDate = dateFormat.parse(commentReplydata.getPublish_date());
            System.out.println(oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.before(currentDate)) {

//                Log.e("oldDate", "is previous date");
//                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
                if (seconds <= 100) {
                    holder.feedcomment_time_textview.setText("Just now");
                } else if (minutes <= 10) {
                    holder.feedcomment_time_textview.setText("Minute ago");
                } else if (hours <= 24) {
                    switch ((int) hours) {
                        case 0:
                            holder.feedcomment_time_textview.setText("1 Hour ago");
                            break;
                        case 1:
                            holder.feedcomment_time_textview.setText("2 Hour ago");
                            break;
                        case 2:
                            holder.feedcomment_time_textview.setText("3 Hour ago");
                            break;
                        case 3:
                            break;
                        case 4:
                            holder.feedcomment_time_textview.setText("4 Hour ago");
                            break;
                        case 5:
                            holder.feedcomment_time_textview.setText("5 Hour ago");
                            break;
                        case 6:
                            holder.feedcomment_time_textview.setText("6 Hour ago");
                            break;
                        case 7:
                            holder.feedcomment_time_textview.setText("7 Hour ago");
                            break;
                        case 8:
                            holder.feedcomment_time_textview.setText("8 Hour ago");
                            break;
                        case 9:
                            holder.feedcomment_time_textview.setText("9 Hour ago");
                            break;
                        case 10:
                            holder.feedcomment_time_textview.setText("10 Hour ago");
                            break;
                        case 11:
                            holder.feedcomment_time_textview.setText("11 Hour ago");
                            break;
                        case 12:
                            holder.feedcomment_time_textview.setText("12 Hour ago");
                            break;
                        case 13:
                            holder.feedcomment_time_textview.setText("13 Hour ago");
                            break;
                        case 14:
                            holder.feedcomment_time_textview.setText("14 Hour ago");
                            break;
                        case 15:
                            holder.feedcomment_time_textview.setText("15 Hour ago");
                            break;
                        case 16:
                            holder.feedcomment_time_textview.setText("16 Hour ago");
                            break;
                        case 17:
                            holder.feedcomment_time_textview.setText("17 Hour ago");
                            break;
                        case 18:
                            holder.feedcomment_time_textview.setText("18 Hour ago");
                            break;
                        case 19:
                            holder.feedcomment_time_textview.setText("19 Hour ago");
                            break;
                        case 20:
                            holder.feedcomment_time_textview.setText("20 Hour ago");
                            break;
                        case 21:
                            holder.feedcomment_time_textview.setText("21 Hour ago");
                            break;
                        case 22:
                            holder.feedcomment_time_textview.setText("22 Hour ago");
                            break;
                        case 23:
                            holder.feedcomment_time_textview.setText("23 Hour ago");
                            break;
                        case 24:
                            holder.feedcomment_time_textview.setText("24 Hour ago");
                            break;

                    }
                } else if (days <= 2) {
                    switch ((int) days) {
                        case 0:
                            holder.feedcomment_time_textview.setText("1 Day ago");
                            break;
                        case 1:
                            holder.feedcomment_time_textview.setText("2 Day ago");
                            break;
                        case 2:
                            holder.feedcomment_time_textview.setText("3 Day ago");
                            break;
                    }

                } else {
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = dateFormat1.parse(commentReplydata.getPublish_date());
                    holder.feedcomment_time_textview.setText(dateFormat.format(date1));
                }
            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        }




    }

    @Override
    public int getItemCount() {
        return commentReplyList.size();
    }


}