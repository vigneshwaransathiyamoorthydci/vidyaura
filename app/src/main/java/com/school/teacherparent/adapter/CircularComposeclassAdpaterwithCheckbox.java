package com.school.teacherparent.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ListofClass;

import java.util.ArrayList;
import java.util.List;


public class CircularComposeclassAdpaterwithCheckbox extends BaseAdapter {

    private final LayoutInflater mInflater;
    List<ListofClass> classList;
    Context context;
    public ArrayList<String> arraylistDistrictId = new ArrayList<String>();
    public ArrayList<String> arraylistDistrictName = new ArrayList<String>();
    private boolean[] mSelected;

    public ArrayList<String> arraylistDistrictIdTest = new ArrayList<String>();
    CheckBox selectAllCheckbox;
    public CircularComposeclassAdpaterwithCheckbox(List<ListofClass> classList, Context context,CheckBox selectAllCheckbox) {
        this.classList = classList;
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arraylistDistrictIdTest = arraylistDistrictIdTest;
        mSelected = new boolean[classList.size()];
        this.selectAllCheckbox=selectAllCheckbox;


    }


    @Override
    public int getCount() {
        return classList.size();
    }

    @Override
    public ListofClass getItem(int position) {
        return classList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = mInflater.inflate(R.layout.custom_spinner_item_checkbox, null);
       final TextView className = (TextView) convertView.findViewById(R.id.text_checkbox);
        className.setText(classList.get(position).getClassname() + " " + classList.get(position).getClasssection());
        final CheckBox checkbox_spinner_item_one = (CheckBox) convertView.findViewById(R.id.checkbox_spinner_item_one);

        if (classList.get(position).getSelectedID() == 0) {
            checkbox_spinner_item_one.setChecked(false);
        } else {
            checkbox_spinner_item_one.setChecked(true);
        }



        className.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (classList.get(position).getSelectedID() == 0) {


                    classList.get(position).setSelectedID(classList.get(position).getClassid() + classList.get(position).getClasssectionid());
                    checkbox_spinner_item_one.setChecked(true);
                    for (int u=0;u<classList.size();u++)
                    {
                        if (classList.get(u).getSelectedID()==classList.get(u).getClassid() +classList.get(u).getClasssectionid() )
                        {
                            selectAllCheckbox.setChecked(true);
                            continue;
                        }
                        else
                        {
                            selectAllCheckbox.setChecked(false);
                            break;
                        }
                    }

                } else {
                    classList.get(position).setSelectedID(0);
                    checkbox_spinner_item_one.setChecked(false);
                    selectAllCheckbox.setChecked(false);
                }
            }
        });
        checkbox_spinner_item_one.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // mSelected[position] = isChecked;

                    classList.get(position).setSelectedID(classList.get(position).getClassid() + classList.get(position).getClasssectionid());

                    for (int u=0;u<classList.size();u++)
                    {
                        if (classList.get(u).getSelectedID()==classList.get(u).getClassid() +classList.get(u).getClasssectionid() )
                        {
                            selectAllCheckbox.setChecked(true);
                            continue;
                        }
                        else
                        {
                            selectAllCheckbox.setChecked(false);
                            break;
                        }
                    }

                } else {
                    classList.get(position).setSelectedID(0);
                    selectAllCheckbox.setChecked(false);
                }


            }
        });

        selectAllCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CompoundButton) view).isChecked()){
                    System.out.println("Checked");
                    for (int y = 0; y < classList.size(); y++) {
                        classList.get(y).setSelectedID(classList.get(y).getClassid() + classList.get(y).getClasssectionid());
                    }
                    notifyDataSetChanged();
                } else {
                    System.out.println("Un-Checked");
                    for (int y = 0; y < classList.size(); y++) {
                        classList.get(y).setSelectedID(0);
                    }
                    notifyDataSetChanged();
                }
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return convertView;
    }

}




