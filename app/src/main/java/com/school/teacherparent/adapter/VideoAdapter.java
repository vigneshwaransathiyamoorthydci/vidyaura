package com.school.teacherparent.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.school.teacherparent.R;
import com.school.teacherparent.models.Photoslist;
import com.school.teacherparent.models.VideoDTO;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.jzvd.JzvdStd;

/**
 * Created by keerthana on 11/12/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
    List<Photoslist> photoList;
    private Context context;

    private VideoAdapter.AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private  FullscreenVideoLayout video;
        private LinearLayout header;
        ImageView img,playImageview;
        JzvdStd video_selected;


        public MyViewHolder(View view) {
            super(view);
            video_selected=(JzvdStd)view.findViewById(R.id.video_selected);
            img=(ImageView)view.findViewById(R.id.image);
            playImageview=(ImageView)view.findViewById(R.id.playImageview);
        }
    }


    public VideoAdapter(List<Photoslist> photoList, Context context) {
        this.photoList = photoList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(VideoAdapter.AddTouchListen addTouchListen) {

        this.addTouchListen=addTouchListen;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Photoslist gallerylist = photoList.get(position);

        JzvdStd.releaseAllVideos();


            holder.img.setVisibility(View.VISIBLE);
            holder.playImageview.setVisibility(View.VISIBLE);


//            holder.video_selected.setUp((context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getImage())
//                    , Jzvd.SCREEN_WINDOW_NORMAL, "");
//            holder.video_selected.setSystemTimeAndBattery();
//
//            RequestOptions requestOptions = new RequestOptions();
//            requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
//            requestOptions.error(R.mipmap.icon_video_thumbnail);
//            Glide.with(context)
//                    .load(context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getImage())
//                    .apply(requestOptions)
//                    .thumbnail(Glide.with(context).load(context.getString(R.string.s3_baseurl) +
//                            context.getString(R.string.s3_feeds_path) +"/"+
//                            gallerylist.getImage()))
//                    .into(holder.video_selected.thumbImageView);



        long positionInMillis=1000;
        long interval = positionInMillis * 1000;
        RequestOptions options = new RequestOptions().frame(interval);
        Glide.with(context).asBitmap()
                .load(context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+
                        gallerylist.getImage())
                .apply(options).into(holder.img);




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });


    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
}
