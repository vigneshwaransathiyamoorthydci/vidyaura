package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.CommentList;
import com.school.teacherparent.models.FeedCommentLikeResponse;
import com.school.teacherparent.models.FeedLikeParmas;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 8/27/2018.
 */

public class FeedscommentviewAdapter extends RecyclerView.Adapter<FeedscommentviewAdapter.ViewHolder> {

    private  List<CommentList>  feedcommentListResponsearray;
    private Context context;
    BaseActivity baseActivity;
    @Inject
    public VidyAPI vidyauraAPI;
    int feedID;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name_textview, feedcomment_time_textview, comment_textview, replycount_textview, commentclapcount_textview, commentsCount, shareCount,replies_textview,reply_textview;
        public ImageView feedcomment_imageview,comment_clapimageview;



        public ViewHolder(View view) {
            super(view);
            feedcomment_imageview=view.findViewById(R.id.feedcomment_imageview);
            name_textview=view.findViewById(R.id.name_textview);
            feedcomment_time_textview=view.findViewById(R.id.feedcomment_time_textview);
            comment_textview=view.findViewById(R.id.comment_textview);
            replycount_textview=view.findViewById(R.id.replycount_textview);
            comment_clapimageview=view.findViewById(R.id.comment_clapimageview);
            commentclapcount_textview = view.findViewById(R.id.commentclapcount_textview);
            replies_textview=view.findViewById(R.id.replies_textview);
            reply_textview=view.findViewById(R.id.reply_textview);


        }
    }




    public FeedscommentviewAdapter( List<CommentList> feedcommentListResponsearray, Context context,BaseActivity baseActivity,int feedID) {
        this.feedcommentListResponsearray = feedcommentListResponsearray;
        this.context = context;
        this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        this.feedID=feedID;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_comment_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CommentList fcomData = feedcommentListResponsearray.get(position);
        if(!isNullOrEmpty(fcomData.getLname()))
        {
            holder.name_textview.setText(fcomData.getFname() + " " + fcomData.getLname());
        }
        else
        {
            holder.name_textview.setText(fcomData.getFname());
        }


        holder.comment_textview.setText(Html.fromHtml(fcomData.getComment()));
        //holder.replies_textview.setText(fcomData.getFeedReplyCommentCount() + " " + context.getString(R.string.replies));
        if (fcomData.getFeedReplyCommentCount()==0 ||fcomData.getFeedReplyCommentCount()==1)
        {
            holder.replies_textview.setText(""+fcomData.getFeedReplyCommentCount()+" "+context.getString(R.string.replies));

        }
        else if (fcomData.getFeedReplyCommentCount()>=2 && fcomData.getFeedReplyCommentCount()<=98)
        {
            holder.replies_textview.setText(""+fcomData.getFeedReplyCommentCount()+" "+context.getString(R.string.replies));


        }
        else if (fcomData.getFeedReplyCommentCount()>=99 && fcomData.getFeedReplyCommentCount()<=999)
        {
            holder.replies_textview.setText(""+fcomData.getFeedReplyCommentCount()+" "+context.getString(R.string.replies));

        }
        else
        {
            holder.replies_textview.setText("999+"+context.getString(R.string.replies));

        }
        holder.commentclapcount_textview.setText(fcomData.getFeedCommentClapsCount());
        holder.replies_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "commentreply").putExtra("feedID", fcomData.getFeed_id()).putExtra("commID", fcomData.getComment_ID()));
                } else {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.reply_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "commentreply").putExtra("feedID", fcomData.getFeed_id()).putExtra("commID", fcomData.getComment_ID()));
                } else {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (fcomData.getIsUserLikedComment().equals("1")) {

            holder.comment_clapimageview.setImageResource(R.mipmap.clap);
        } else {

            holder.comment_clapimageview.setImageResource(R.mipmap.unclap_icon);
        }
        holder.commentclapcount_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.comment_clapimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    baseActivity.showProgress();
                    FeedLikeParmas feedLikeParmas = new FeedLikeParmas();
                    feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                    feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                    feedLikeParmas.setFeedID(String.valueOf(feedID));
                    feedLikeParmas.setCommentID(String.valueOf(fcomData.getComment_ID()));
                    feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                    if (fcomData.getIsUserLikedComment().equals("0")) {
                        feedLikeParmas.setClapStatus("1");
                    } else {
                        feedLikeParmas.setClapStatus("0");
                    }

                    vidyauraAPI.updateFeedcommentClaps(feedLikeParmas).enqueue(new Callback<FeedCommentLikeResponse>() {
                        @Override
                        public void onResponse(Call<FeedCommentLikeResponse> call, Response<FeedCommentLikeResponse> response) {
                            baseActivity.hideProgress();
                            if (response.body() != null) {
                                FeedCommentLikeResponse feedLikeResponse = response.body();
                                if (feedLikeResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {


                                    if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                        //Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        if (feedLikeResponse.getClapStatus().equals("1")) {

                                            fcomData.setIsUserLikedComment("1");
                                            fcomData.setFeedCommentClapsCount(String.valueOf(feedLikeResponse.getCommentsLikeCount()));
                                            holder.commentclapcount_textview.setText(fcomData.getFeedCommentClapsCount());
                                            holder.comment_clapimageview.setImageResource(R.mipmap.clap);
                                        } else {

                                            fcomData.setIsUserLikedComment("0");
                                            fcomData.setFeedCommentClapsCount(String.valueOf(feedLikeResponse.getCommentsLikeCount()));
                                            holder.commentclapcount_textview.setText(fcomData.getFeedCommentClapsCount());
                                            holder.comment_clapimageview.setImageResource(R.mipmap.unclap_icon);
                                        }
                                    } else {
                                        Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                                } else {
                                    Intent i = new Intent(context, SecurityPinActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(i);
                                    ((Activity) context).finish();
                                }
                            } else {
                                Toast.makeText(context, context.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<FeedCommentLikeResponse> call, Throwable t) {
                            baseActivity.hideProgress();
                            Toast.makeText(context, context.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (fcomData.getEmp_photo() != null) {
            String url = null;
            if (fcomData.getType().equals("teachers")) {
                url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + fcomData.getEmp_photo();
            } else if (fcomData.getType().equals("parents")) {
                url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_student_path) + "/" + fcomData.getEmp_photo();
            }
            Picasso.get().load(url).


                    into(holder.feedcomment_imageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                }
                            }, 1000);


                        }

                        @Override
                        public void onError(Exception e) {
                            holder.feedcomment_imageview.setImageResource(R.drawable.ic_user);
                        }
                    });

        }
        else
        {
            holder.feedcomment_imageview.setImageResource(R.drawable.ic_user);
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        try {

            Date oldDate = dateFormat.parse(fcomData.getCreated_at());
            System.out.println(oldDate);

            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.before(currentDate)) {

//                Log.e("oldDate", "is previous date");
//                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
//                        + " hours: " + hours + " days: " + days);
                if (seconds <= 100) {
                    holder.feedcomment_time_textview.setText("Just now");
                } else if (minutes <= 10) {
                    holder.feedcomment_time_textview.setText("Minute ago");
                } else if (hours <= 24) {
                    switch ((int) hours) {
                        case 0:
                            holder.feedcomment_time_textview.setText("1 Hour ago");
                            break;
                        case 1:
                            holder.feedcomment_time_textview.setText("1 Hour ago");
                            break;
                        case 2:
                            holder.feedcomment_time_textview.setText("2 Hour ago");
                            break;
                        case 3:
                            holder.feedcomment_time_textview.setText("3 Hour ago");
                            break;
                        case 4:
                            holder.feedcomment_time_textview.setText("4 Hour ago");
                            break;
                        case 5:
                            holder.feedcomment_time_textview.setText("5 Hour ago");
                            break;
                        case 6:
                            holder.feedcomment_time_textview.setText("6 Hour ago");
                            break;
                        case 7:
                            holder.feedcomment_time_textview.setText("7 Hour ago");
                            break;
                        case 8:
                            holder.feedcomment_time_textview.setText("8 Hour ago");
                            break;
                        case 9:
                            holder.feedcomment_time_textview.setText("9 Hour ago");
                            break;
                        case 10:
                            holder.feedcomment_time_textview.setText("10 Hour ago");
                            break;
                        case 11:
                            holder.feedcomment_time_textview.setText("11 Hour ago");
                            break;
                        case 12:
                            holder.feedcomment_time_textview.setText("12 Hour ago");
                            break;
                        case 13:
                            holder.feedcomment_time_textview.setText("13 Hour ago");
                            break;
                        case 14:
                            holder.feedcomment_time_textview.setText("14 Hour ago");
                            break;
                        case 15:
                            holder.feedcomment_time_textview.setText("15 Hour ago");
                            break;
                        case 16:
                            holder.feedcomment_time_textview.setText("16 Hour ago");
                            break;
                        case 17:
                            holder.feedcomment_time_textview.setText("17 Hour ago");
                            break;
                        case 18:
                            holder.feedcomment_time_textview.setText("18 Hour ago");
                            break;
                        case 19:
                            holder.feedcomment_time_textview.setText("19 Hour ago");
                            break;
                        case 20:
                            holder.feedcomment_time_textview.setText("20 Hour ago");
                            break;
                        case 21:
                            holder.feedcomment_time_textview.setText("21 Hour ago");
                            break;
                        case 22:
                            holder.feedcomment_time_textview.setText("22 Hour ago");
                            break;
                        case 23:
                            holder.feedcomment_time_textview.setText("23 Hour ago");
                            break;
                        case 24:
                            holder.feedcomment_time_textview.setText("24 Hour ago");
                            break;

                    }
                } else if (days <= 2) {
                    switch ((int) days) {
                        case 0:
                            holder.feedcomment_time_textview.setText("1 Day ago");
                            break;
                        case 1:
                            holder.feedcomment_time_textview.setText("2 Day ago");
                            break;
                        case 2:
                            holder.feedcomment_time_textview.setText("3 Day ago");
                            break;
                    }

                } else {
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = dateFormat1.parse(fcomData.getCreated_at());
                    holder.feedcomment_time_textview.setText(dateFormat1.format(date1));
                }
            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        }





}

    @Override
    public int getItemCount() {
        return feedcommentListResponsearray.size();

    }
    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }

}

