package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.DeleteExamParms;
import com.school.teacherparent.R;
import com.school.teacherparent.models.UpcomingExamdetailList;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 10/8/2018.
 */


public class ExamDetailAdapter extends RecyclerView.Adapter<ExamDetailAdapter.MyViewHolder> {

    ArrayList<UpcomingExamdetailList> upcomingExamdetailLists;
    private Context context;
    BaseActivity baseActivity;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;


    public ExamDetailAdapter( ArrayList<UpcomingExamdetailList> upcomingExamdetailLists,Context context,   BaseActivity baseActivity) {
        this.upcomingExamdetailLists = upcomingExamdetailLists;
        this.context =context;
        this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private  LinearLayout footer;
        public ImageView img,menu;
       public TextView subject,syllabus,topic,date,tvStartTime,desc,tvEndTime;
        CircularImageView classNameImgeview;
        public MyViewHolder(View view) {
            super(view);
            subject = (TextView) view.findViewById(R.id.subject);
            tvStartTime = (TextView) view.findViewById(R.id.tv_start_time);
            tvEndTime = (TextView) view.findViewById(R.id.tv_end_time);
            //img=(ImageView)view.findViewById(R.id.img);
            date=(TextView)view.findViewById(R.id.date);
            desc=(TextView)view.findViewById(R.id.desc);
            syllabus=(TextView)view.findViewById(R.id.syllabus);
            topic=(TextView)view.findViewById(R.id.topic);
            footer=(LinearLayout)view.findViewById(R.id.footer);
            menu=(ImageView)view.findViewById(R.id.menu);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);
        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exam_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final UpcomingExamdetailList movie = upcomingExamdetailLists.get(position);

        holder.tvStartTime.setText(getTimeFormat(movie.getStart_time()));
        holder.tvEndTime.setText(getTimeFormat(movie.getEnd_time()));
        //holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.date.setText(getDateFormat(movie.getStart_time()));
        holder.desc.setVisibility(View.GONE);
        holder.subject.setText(movie.getName());
        if (movie.getChapterName().size()>0) {
            String chapterName = "";
            for (int y=0;y<movie.getChapterName().size();y++)
            {
                chapterName=chapterName.concat(movie.getChapterName().get(y).getChapter_name()).concat(",");

            }
            chapterName = chapterName.substring(0, chapterName.length() - 1);
            holder.syllabus.setText(chapterName);
        }else{
            holder.syllabus.setText("No chapter");
        }

        if(movie.getTopicName().size()>0) {
            String topicName = "";
            for (int y=0;y<movie.getTopicName().size();y++)
            {

                topicName=topicName.concat(movie.getTopicName().get(y).getTopic_name()).concat(",");
            }
            topicName = topicName.substring(0, topicName.length() - 1);
            holder.topic.setText(context.getString(R.string.topic_se)+" "+topicName);

        }else{
            holder.topic.setText("No Topic");
        }

        if (movie.getIsUserPostedExam()==1)
        {
            holder.menu.setVisibility(View.VISIBLE);
        }else{
            holder.menu.setVisibility(View.GONE);
        }
        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+ movie.getClassImage();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            Log.d("KLNCE","ExamSchedule_id"+movie.getId());

                            context.startActivity(new Intent(context, SidemenuDetailActivity.class)
                                    .putExtra("type", "updateexam").putExtra("examID", movie.getExamSchedule_id()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            baseActivity.showProgress();
                            DeleteExamParms deleteExamParms=new DeleteExamParms();
                            deleteExamParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            deleteExamParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            deleteExamParms.setExamScheduleID(movie.getExamSchedule_id());
                            deleteExamParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));


                            vidyauraAPI.deleteExamSchedule(deleteExamParms).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    baseActivity.hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                //feedsFragment.getfeedList();
                                                upcomingExamdetailLists.remove(position);
                                                notifyDataSetChanged();


                                            } else {
                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(context, SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(i);
                                            ((Activity)context).finish();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    baseActivity.hideProgress();
                                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }



                });

            }
        });


    }

    @Override
    public int getItemCount() {
        return upcomingExamdetailLists.size();
    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String getTimeFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}