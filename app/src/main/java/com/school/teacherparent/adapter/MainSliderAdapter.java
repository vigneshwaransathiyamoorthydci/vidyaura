package com.school.teacherparent.adapter;

import android.content.Context;

import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MainSliderAdapter extends SliderAdapter {

    Context context;
    private List<String> stringArrayList;

    public MainSliderAdapter(Context context, List<String> stringArrayList) {
        this.context = context;
        this.stringArrayList = stringArrayList;
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        System.out.println("url =====> "+context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+stringArrayList.get(position));
        viewHolder.bindImageSlide(context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+stringArrayList.get(position));
        /*switch (position) {
            case 0:
                viewHolder.bindImageSlide("https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg");
                break;
            case 1:
                viewHolder.bindImageSlide("https://assets.materialup.com/uploads/20ded50d-cc85-4e72-9ce3-452671cf7a6d/preview.jpg");
                break;
            case 2:
                viewHolder.bindImageSlide("https://assets.materialup.com/uploads/76d63bbc-54a1-450a-a462-d90056be881b/preview.png");
                break;
        }*/
    }

}