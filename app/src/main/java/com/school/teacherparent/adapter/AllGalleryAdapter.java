package com.school.teacherparent.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.VideoBitmapDecoder;
import com.bumptech.glide.request.RequestOptions;
import com.danikula.videocache.HttpProxyCacheServer;
import com.joooonho.SelectableRoundedImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AllGalleryDTO;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.FontTextViewBold;
import com.school.teacherparent.utils.FontTextViewSemibold;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;

/**
 * Created by keerthana on 11/12/2018.
 */

public class AllGalleryAdapter extends RecyclerView.Adapter<AllGalleryAdapter.MyViewHolder> {


    List<Gallerylist> gallerylists;
    private Context context;
    private AddTouchListen addTouchListen;
    private HttpProxyCacheServer proxy;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private  FontTextViewSemibold counts;
        private  FontTextViewBold program;
        private  CardView cardview;
        private LinearLayout header;
        ImageView img;
        public VideoView v;
        JzvdStd video_selected;

        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.image);

            counts=(FontTextViewSemibold)view.findViewById(R.id.counts);
            program=(FontTextViewBold)view.findViewById(R.id.program);

            video_selected=(JzvdStd)view.findViewById(R.id.video_selected);

        }
    }


    public AllGalleryAdapter(List<Gallerylist> gallerylists, Context context) {
        this.gallerylists = gallerylists;
        this.context =context;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        //proxy = context.getProxy(this);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_gallery_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Gallerylist gallerylist = gallerylists.get(position);

        RequestOptions imageError = new RequestOptions()
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        //holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        int photoscount = 0,videoCount = 0;
        for (int y=0;y<gallerylist.getAlbumList().size();y++)
        {
            if (gallerylist.getAlbumList().get(y).getMedia_type().equals("mp4"))
            {
                videoCount=videoCount+1;

            }
            else
            {
                photoscount=photoscount+1;
            }
        }
        holder.counts.setText(""+photoscount+" "+context.getString(R.string.photos)+" "+videoCount+" "+context.getString(R.string.video));
        holder.program.setText(gallerylist.getTitle());

        if (gallerylist.getAlbumList().size()>0)
        {

            if (photoscount>0)
            {
                for (int y=0;y<gallerylist.getAlbumList().size();y++)
                {
                    if (gallerylist.getAlbumList().get(y).getMedia_type().equals("png") || gallerylist.getAlbumList().get(y).getMedia_type().equals("jpg") || gallerylist.getAlbumList().get(y).getMedia_type().equals("jpeg"))
                    {
                        holder.video_selected.setVisibility(View.INVISIBLE);
                        holder.img.setVisibility(View.VISIBLE);
                        String url=context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+gallerylist.getAlbumList().get(y).getMedia_path();
                        Glide.with(context.getApplicationContext()).load(url).apply(imageError).into(holder.img);
                        /*Picasso.get().load(url).into(holder.img, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {
                                holder.img.setImageResource(R.mipmap.galleryimg);
                            }
                        });*/
                        break;
                    }
                }
            }


            else
            {
                for (int y=0;y<gallerylist.getAlbumList().size();y++) {

                    holder.video_selected.setVisibility(View.INVISIBLE);
                    holder.img.setVisibility(View.VISIBLE);
                    long positionInMillis=1000;
                    long interval = positionInMillis * 1000;
                    RequestOptions options = new RequestOptions().frame(interval);
                    Glide.with(context).asBitmap()
                            .load(context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+gallerylist.getAlbumList().get(y).getMedia_path())
                           .apply(options).into(holder.img);;


                    break;

                }

            }











        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return gallerylists.size();
    }
}
