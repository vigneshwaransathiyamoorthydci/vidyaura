package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.FeesCondition;
import com.school.teacherparent.models.FeesListResponse;

import java.util.ArrayList;


public class FeesConditionAdapter extends RecyclerView.Adapter<FeesConditionAdapter.MyViewHolder> {

    Context context;
    private ArrayList<FeesCondition.feesDetails> conditionArrayList;

    public FeesConditionAdapter(Context context, ArrayList<FeesCondition.feesDetails> conditionArrayList) {
        this.context = context;
        this.conditionArrayList = conditionArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView1, textView2, textView3, textView4;

        public MyViewHolder(View view) {
            super(view);
            textView1 = (TextView) view.findViewById(R.id.textView1);
            textView2 = (TextView) view.findViewById(R.id.textView2);
            textView3 = (TextView) view.findViewById(R.id.textView3);
            textView4 = (TextView) view.findViewById(R.id.textView4);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fees_condition, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        System.out.println("getFeesTypeName ==> 1 "+conditionArrayList.get(position).getFeesTypeName());
        holder.textView1.setText(conditionArrayList.get(position).getFeesTypeName());
        holder.textView2.setText(""+conditionArrayList.get(position).getTotal_amount());
        holder.textView3.setText(""+conditionArrayList.get(position).getDiscountAmount());
        holder.textView4.setText(""+(conditionArrayList.get(position).getTotal_amount() - conditionArrayList.get(position).getDiscountAmount()));
    }

    @Override
    public int getItemCount() {
        return conditionArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*@Override
    public int getCount() {
        return conditionArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return conditionArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.item_fees_condition, null);
        }

        TextView textView1 = (TextView) rowView.findViewById(R.id.textView1);
        TextView textView2 = (TextView) rowView.findViewById(R.id.textView2);
        TextView textView3 = (TextView) rowView.findViewById(R.id.textView3);
        TextView textView4 = (TextView) rowView.findViewById(R.id.textView4);
        //buttonFlat.setText("*" + conditionArrayList.get(position));
        System.out.println("getFeesTypeName ==> 1 "+conditionArrayList.get(position).getFeesTypeName());
        textView1.setText(conditionArrayList.get(position).getFeesTypeName());
        textView2.setText(""+conditionArrayList.get(position).getTotal_amount());
        textView3.setText(""+conditionArrayList.get(position).getDiscountAmount());
        textView4.setText(""+(conditionArrayList.get(position).getTotal_amount() - conditionArrayList.get(position).getDiscountAmount()));

        return rowView;

    }*/

}


