package com.school.teacherparent.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.FeedsFragment;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.DeleteFeedParms;
import com.school.teacherparent.models.FeedLikeParmas;
import com.school.teacherparent.models.FeedLikeResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 8/27/2018.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.ViewHolder> {

    ArrayList<FeedList> feedList;
    private Context context;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    BaseActivity baseActivity;
    FeedsFragment feedsFragment;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView feedtitle, feedDescription, status, percentage, claps, clapsCount, commentsCount,
                shareCount,uname,schoolname,feeddate;
        public ImageView feedmenuticon, settings,clapImageview,feed_image,profile_image;
        LinearLayout share_lay,commentLinear,clapLinearlayout;
        ViewPager viewPager;

        public ViewHolder(View view) {
            super(view);
            share_lay = view.findViewById(R.id.share_lay);
            clapsCount = view.findViewById(R.id.claps_count);
            commentsCount = view.findViewById(R.id.comments_count);
            shareCount = view.findViewById(R.id.share_count);
            feedtitle = (TextView) view.findViewById(R.id.feedtitleTextview);
            feedDescription = (TextView) view.findViewById(R.id.feedDescriptionTextview);
           /* filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);

*/
            feed_image=view.findViewById(R.id.feed_image);
            viewPager=view.findViewById(R.id.viewpager);
            uname=view.findViewById(R.id.uname);
            schoolname=view.findViewById(R.id.schoolname);
            feeddate=view.findViewById(R.id.feeddate);
            commentLinear=view.findViewById(R.id.commentLinear);
            feedmenuticon=view.findViewById(R.id.feedmenuticon);
            claps=view.findViewById(R.id.claps);
            clapImageview=view.findViewById(R.id.clapImageview);
            clapLinearlayout=view.findViewById(R.id.clapLinearlayout);
            profile_image=view.findViewById(R.id.profile_image);
        }
    }





    public FeedsAdapter(ArrayList<FeedList> feedList, Context context, BaseActivity baseActivity, FeedsFragment feedsFragment) {
        this.feedList = feedList;
        this.context = context;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        this.baseActivity=baseActivity;
        this.feedsFragment=feedsFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feeds_item, parent, false);
        return new ViewHolder(itemView);
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        secureTokenSharedPreferences = context.getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();

        final FeedList fData=feedList.get(position);
        if (fData.getAuthor_type().equals("Admin"))
        {
            holder.uname.setText(fData.getName());
        }
        else
        {
            holder.uname.setText(fData.getFname()+" "+fData.getLname());
        }
        holder.feedtitle.setText(fData.getTitle());
        holder.feedDescription.setText(fData.getDescription());

        holder.schoolname.setText(fData.getSchool_name());
        if (fData.getFeedLikeCount()==0){
            holder.clapsCount.setText(context.getString(R.string.clap));
            holder.claps.setText(context.getString(R.string.clap));
        }
        else if (fData.getFeedLikeCount()==1)
        {
            holder.clapsCount.setText(""+fData.getFeedLikeCount()+" "+context.getString(R.string.clap));
            holder.claps.setText(context.getString(R.string.clap));
        }
        else if (fData.getFeedLikeCount()>=2 && fData.getFeedLikeCount()<=98)
        {
            holder.clapsCount.setText(""+fData.getFeedLikeCount()+" "+context.getString(R.string.claps));
            holder.claps.setText(context.getString(R.string.claps));

        }
        else if (fData.getFeedLikeCount()>=99 && fData.getFeedLikeCount()<=999)
        {
            holder.clapsCount.setText(""+fData.getFeedLikeCount()+" "+context.getString(R.string.claps));
            holder.claps.setText(context.getString(R.string.claps));
        }
        else
        {
            holder.clapsCount.setText("999+"+context.getString(R.string.claps));
            holder.claps.setText(context.getString(R.string.claps));
        }

        if (fData.getFeedCommentCount()==0 ||fData.getFeedCommentCount()==1)
        {
            holder.commentsCount.setText(""+fData.getFeedCommentCount()+" "+context.getString(R.string.comment));

        }
        else if (fData.getFeedCommentCount()>=2 && fData.getFeedCommentCount()<=98)
        {
            holder.commentsCount.setText(""+fData.getFeedCommentCount()+" "+context.getString(R.string.comments));


        }
        else if (fData.getFeedCommentCount()>=99 && fData.getFeedCommentCount()<=999)
        {
            holder.commentsCount.setText(""+fData.getFeedCommentCount()+" "+context.getString(R.string.comments));

        }
        else
        {
            holder.commentsCount.setText("999+"+context.getString(R.string.comments));

        }
        //holder.shareCount.setText(""+fData.getFeedShareCount()+" "+context.getString(R.string.shares));
        if (fData.getFeedShareCount()==0 ||fData.getFeedShareCount()==1)
        {
            holder.shareCount.setText(""+fData.getFeedShareCount()+" "+context.getString(R.string.share));

        }
        else if (fData.getFeedShareCount()>=2 && fData.getFeedShareCount()<=98)
        {
            holder.shareCount.setText(""+fData.getFeedShareCount()+" "+context.getString(R.string.shares));


        }
        else if (fData.getFeedShareCount()>=99 && fData.getFeedShareCount()<=999)
        {
            holder.shareCount.setText(""+fData.getFeedShareCount()+" "+context.getString(R.string.shares));

        }
        else
        {
            holder.shareCount.setText("999+"+context.getString(R.string.shares));

        }


        holder.feeddate.setText(getDateFormat(fData.getPublish_date()));


        if (fData.getIsFeedPostedBy()==1)
        {
            holder.feedmenuticon.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.feedmenuticon.setVisibility(View.VISIBLE);
        }

        holder.share_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Vidyaura")
                        .setMessage("Are you sure you want to share?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                if (Util.isNetworkAvailable()) {
                                    DeleteFeedParms feedLikeParmas=new DeleteFeedParms();
                                    feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
                                    feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                                    feedLikeParmas.setFeedID(String.valueOf(fData.getFeed_id()));

                                    vidyauraAPI.updateFeedShare(feedLikeParmas).enqueue(new Callback<AddFeedResponse>() {
                                        @Override
                                        public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                            if (response.body()!=null)
                                            {
                                                AddFeedResponse feedLikeResponse=response.body();
                                                if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                                                    if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {
                                                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
                                                        share.setType("text/plain");
                                                        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                                        if (fData.getTitle() != null) {
                                                            share.putExtra(Intent.EXTRA_SUBJECT, fData.getTitle());
                                                        }
                                                        if (fData.getAttachmentsList() != null) {
                                                            share.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + fData.getAttachmentsList().get(0));
                                                        }
                                                        context.startActivity(Intent.createChooser(share, "Share link!"));

                                                    } else {
                                                        Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                                else
                                                {
                                                    Intent i = new Intent(context, SecurityPinActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    context.startActivity(i);
                                                }
                                            }
                                            else
                                            {
                                                Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                            Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                                else {
                                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();

                                }

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        holder.feedmenuticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                final TextView report = alertLayout.findViewById(R.id.report);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                if (fData.getIsFeedPostedBy()==1)
                {
                    edit.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.VISIBLE);
                    report.setVisibility(View.VISIBLE);
                }
                else
                {
                    edit.setVisibility(View.GONE);
                    delete.setVisibility(View.GONE);
                    report.setVisibility(View.VISIBLE);
                }
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", fData.getFeed_id()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        DeleteOrReportFeed(0,position,fData.getFeed_id(),"");
                        }



                });
                report.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        //DeleteOrReportFeed(1,position,fData.getFeed_id());
                        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View alertLayout = inflater.inflate(R.layout.layout_feed_report_comment, null);
                        final EditText edit = alertLayout.findViewById(R.id.edittext_report);
                        final Button reportButton = alertLayout.findViewById(R.id.report_button);
                        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                        alert.setView(alertLayout);
                        final AlertDialog dialog = alert.create();
                        dialog.show();
                        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                        reportButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edit.getText().toString().length()>0)
                                {
                                    DeleteOrReportFeed(1,position,fData.getFeed_id(),edit.getText().toString().trim());
                                    dialog.dismiss();
                                }
                                else
                                {
                                    edit.setError(context.getString(R.string.enter_report));
                                }
                            }
                        });
                    }
                });

            }
        });
        if (fData.getIsUserLiked()==1)
        {
            holder.claps.setTextColor(context.getResources().getColor(R.color.text_color_light));


            holder.clapImageview.setImageResource(R.mipmap.clap);
        }
        else
        {
            holder.claps.setTextColor(context.getResources().getColor(R.color.text_color_light));
            holder.clapImageview.setImageResource(R.mipmap.unclap_icon);
        }




        holder.feedtitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.feedDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.commentLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.commentsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.feed_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });
        holder.schoolname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.uname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.feeddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "feedcommentview").putExtra("feedID", fData.getFeed_id()));
                }
                else
                {
                    Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (fData.getAuthor_type().equals("Admin")) {
            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_schools) + fData.getSchoolLogo();
            System.out.println("getAuthor_type ==> 1 "+url);

            Picasso.get().load(url).


                    into(holder.profile_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                }
                            }, 1000);


                        }

                        @Override
                        public void onError(Exception e) {
                            holder.profile_image.setImageDrawable(context.getDrawable(R.drawable.ic_user));
                        }
                    });
        } else {
            System.out.println("getAuthor_type ==> 2 "+position+" "+fData.getAuthor_type());
            if (fData.getEmp_photo() != null) {
                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_employee) + "/" + fData.getEmp_photo();

                Picasso.get().load(url).


                        into(holder.profile_image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {


                                    }
                                }, 1000);


                            }

                            @Override
                            public void onError(Exception e) {
                                holder.profile_image.setImageDrawable(context.getDrawable(R.drawable.ic_user));
                            }
                        });

            } else {
                holder.profile_image.setImageDrawable(context.getDrawable(R.drawable.ic_user));
            }
        }

        if (fData.getAttachmentsList()!=null&&fData.getAttachmentsList().size()!=0){
            String url=context.getString(R.string.s3_baseurl)+context.getString(R.string.s3_feeds_path)+"/"+fData.getAttachmentsList().get(0);

            //holder.viewPager.setAdapter(new CustomPagerAdapter(context, 7,  fData.getAttachmentsList()));

            //picassoImageHolder(holder.feed_image,context.getString(R.string.s3_feeds_path),"/"+fData.getAttachmentsList().get(0));

            RequestOptions imageError = new RequestOptions()
                    //.placeholder(R.drawable.gallery)
                    .error(R.drawable.gallery)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);
            if (url.contains("jpg") || url.contains("jpeg") || url.contains("png")) {
                Glide.with(context.getApplicationContext()).load(url).apply(imageError).into(holder.feed_image);
            } else {
                RequestOptions imageErrorr = new RequestOptions()
                        .error(R.drawable.audio)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH);
                Glide.with(context.getApplicationContext()).load(R.drawable.audio).apply(imageErrorr).into(holder.feed_image);
            }

            /*Picasso.get().load(url).


                    into(holder.feed_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
//                                        BitmapDrawable drawable = (BitmapDrawable) holder.feed_image.getDrawable();
//                                        Bitmap bitmap = drawable.getBitmap();
//                                        int bitmapwidth=0;
//                                        int bitmpheight=0;
//                                        bitmapwidth=bitmap.getWidth();
//                                        bitmpheight=bitmap.getHeight();
//                                        LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)holder.feed_image.getLayoutParams();
//                                        if (bitmapwidth==0||bitmpheight==0){
//                                            return;
//                                        }
//                                        int newwidth=holder.feed_image.getWidth();
//                                        int newheight=newwidth*bitmpheight/bitmapwidth;
//                                        params.height=newheight;
//                                        holder.feed_image.setLayoutParams(params);

                                    }
                                    catch (Exception e){

                                    }

                                }
                            },1000);


                        }

                        @Override
                        public void onError(Exception e) {
                            holder.feed_image.setImageDrawable(context.getDrawable(R.drawable.client));
                        }
                    });*/
        }






        holder.clapLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    UpdateClap(String.valueOf(fData.getFeed_id()),fData,position,holder.claps, holder.clapImageview,
                            holder.clapsCount);
            }
        });

        holder.clapsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fData.getFeedLikeCount()>0) {
                    if (Util.isNetworkAvailable()) {
                        context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "claps").putExtra("feedID", fData.getFeed_id()));
                    }
                    else
                    {
                        Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        //return showing.size();
        return feedList.size();
    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public void DeleteOrReportFeed(final int isDeleteorReport, final int position, final int feedId, final String reportReason)
    {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            DeleteFeedParms feedLikeParmas=new DeleteFeedParms();
            feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedLikeParmas.setFeedID(String.valueOf(feedId));
            feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedLikeParmas.setIsReportorDelete(isDeleteorReport);
            feedLikeParmas.setReportReason(reportReason);


            vidyauraAPI.deleteFeed(feedLikeParmas).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    baseActivity.hideProgress();
                    if (response.body()!=null)
                    {
                        AddFeedResponse feedLikeResponse=response.body();
                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //feedsFragment.getfeedList();
                                feedList.remove(position);
                                notifyDataSetChanged();


                            } else {
                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            baseActivity.hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedLikeResponse.getToken()).commit();
                            DeleteOrReportFeed(isDeleteorReport,position,feedId,reportReason);
                        }
                    }
                    else
                    {
                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public  void UpdateClap(final String feedID, final FeedList fData, final int position, final TextView claps,
                            final ImageView clapImageview, final TextView clapsCount)
    {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            FeedLikeParmas feedLikeParmas=new FeedLikeParmas();
            feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedLikeParmas.setFeedID(feedID);
            feedLikeParmas.setCommentID("0");
            feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            if (fData.getIsUserLiked()==0)
            {
                feedLikeParmas.setClapStatus("1");
            }
            else
            {
                feedLikeParmas.setClapStatus("0");
            }

            vidyauraAPI.updateFeedClaps(feedLikeParmas).enqueue(new Callback<FeedLikeResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<FeedLikeResponse> call, Response<FeedLikeResponse> response) {
                    baseActivity.hideProgress();
                    if (response.body()!=null)
                    {
                        FeedLikeResponse feedLikeResponse=response.body();
                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                // Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                if (feedLikeResponse.getClapStatus() == 1) {
                                    claps.setTextColor(context.getResources().getColor(R.color.text_color_light));
                                    feedList.get(position).setIsUserLiked(1);
                                    feedList.get(position).setFeedLikeCount(feedLikeResponse.getFeedLikeCount());
                                    //holder.clapsCount.setText("" + feedLikeResponse.getFeedLikeCount() + " " + context.getString(R.string.claps));


                                    clapImageview.setImageResource(R.mipmap.clap);
                                    if (feedLikeResponse.getFeedLikeCount()==0 ||feedLikeResponse.getFeedLikeCount()==1)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.clap));
                                        claps.setText(context.getString(R.string.clap));
                                    }
                                    else if (feedLikeResponse.getFeedLikeCount()>=2 && feedLikeResponse.getFeedLikeCount()<=98)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                    else if (feedLikeResponse.getFeedLikeCount()>=99 && feedLikeResponse.getFeedLikeCount()<=999)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                    else
                                    {
                                        clapsCount.setText("999+"+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                } else {
                                    claps.setTextColor(context.getResources().getColor(R.color.text_color_light));
                                    feedList.get(position).setIsUserLiked(0);
                                    feedList.get(position).setFeedLikeCount(feedLikeResponse.getFeedLikeCount());
                                    //holder.clapsCount.setText("" + feedLikeResponse.getFeedLikeCount() + " " + context.getString(R.string.claps));
                                    claps.setText(context.getString(R.string.claps));
                                    clapImageview.setImageResource(R.mipmap.unclap_icon);
                                    if (feedLikeResponse.getFeedLikeCount()==0 ||feedLikeResponse.getFeedLikeCount()==1)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.clap));
                                        claps.setText(context.getString(R.string.clap));
                                    }
                                    else if (feedLikeResponse.getFeedLikeCount()>=2 && feedLikeResponse.getFeedLikeCount()<=98)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                    else if (feedLikeResponse.getFeedLikeCount()>=99 && feedLikeResponse.getFeedLikeCount()<=999)
                                    {
                                        clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                    else
                                    {
                                        clapsCount.setText("999+"+context.getString(R.string.claps));
                                        claps.setText(context.getString(R.string.claps));
                                    }
                                }
                            } else {
                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
//                                        Intent i = new Intent(context, SecurityPinActivity.class);
//                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        context.startActivity(i);
//                                        ((Activity)context).finish();

                            baseActivity.hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedLikeResponse.getToken()).commit();
                            UpdateClap(feedID,fData,position,claps,clapImageview,clapsCount);




                        }
                    }
                    else
                    {
                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<FeedLikeResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

//    public void picassoImageHolder(final ImageView imageViewProPic,
//                                   String imageName,
//                                    String imageUrl) {
//
//        Target target = new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
//
//                    imageViewProPic.setImageDrawable(image);
//            }
//
//            @Override
//            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//
//                    imageViewProPic.setImageDrawable(errorDrawable);
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                    imageViewProPic.setImageDrawable(placeHolderDrawable);
//            }
//        };
//
//            imageViewProPic.setTag(target);
//        Picasso.get().load(context.getString(R.string.s3_baseurl) + imageUrl + imageName)
//                .placeholder(imageViewProPic).error(R.drawable.client).into(target);
//    }
}

