package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.models.SyllabusListModel;
import com.school.teacherparent.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by keerthana on 10/19/2018.
 */

public class SyllabusListAdapter extends RecyclerView.Adapter<SyllabusListAdapter.MyViewHolder> {

    private List<SyllabusListModel.SyllabusCompletedPercentage> syllabusList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;
    private ProgressBar prg1;
    private ProgressBar Prg1;
    private ProgressBar prg2;

    public SyllabusListAdapter(Context context, List<SyllabusListModel.SyllabusCompletedPercentage> syllabusList) {
        this.context = context;
        this.syllabusList=syllabusList;
    }

    /*public ClassAdapter(List<ClassDTO> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        // private ProgressBar prg1,prg2;
        private TextView counts,desc,className;
        private ImageView img,img2;
        CircularImageView subjectImage;
        private  TextView title,content,status1,status2;
        private SeekBar t1;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title1);
            content = (TextView) view.findViewById(R.id.content1);
            img=(ImageView)view.findViewById(R.id.img);
            counts=(TextView)view.findViewById(R.id.counts);
            desc=(TextView)view.findViewById(R.id.desc);
            t1=view.findViewById(R.id.t1);
            subjectImage=(CircularImageView) view.findViewById(R.id.subjectImage);
            className=(TextView) view.findViewById(R.id.className);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.syllabus_list_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.t1.setEnabled(true);
        SyllabusListModel.SyllabusCompletedPercentage movie = syllabusList.get(position);
        holder.title.setText("Class "+movie.getClassName()+movie.getSection());
        holder.content.setText(movie.getSubjectName());
      //  holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.counts.setText(String.valueOf(movie.getSyllabusList())+"%");
        if (movie.getSyllabusList()!=null){
            int value=Math.round(Float.parseFloat(movie.getSyllabusList()));
            holder.t1.setProgress(value);
        }

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+syllabusList.get(position).getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.subjectImage.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.subjectImage.setImageResource(Constants.imgid[position]);
       // holder.className.setText(movie.getClassName()+movie.getSection());


        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
        holder.t1.setEnabled(false);


    }

    @Override
    public int getItemCount() {
        return syllabusList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}