package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.PaymentActivity;
import com.school.teacherparent.models.FeesListParms;

import java.util.ArrayList;

public class FeesListAdapter extends RecyclerView.Adapter<FeesListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<FeesListParms> feesListParmsArrayList;
    PaymentActivity paymentActivity;
    public AddTouchListen addTouchListen;

    public FeesListAdapter(Context context, ArrayList<FeesListParms> feesListParmsArrayList, PaymentActivity paymentActivity) {
        this.context = context;
        this.feesListParmsArrayList = feesListParmsArrayList;
        this.paymentActivity = paymentActivity;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvFeesType, tvFeesAmount, tvDueDate, tvFeesPaid;
        Button btnPaynow;

        public MyViewHolder(View view) {
            super(view);
            tvFeesType = view.findViewById(R.id.tv_fees_type);
            tvFeesAmount = view.findViewById(R.id.tv_fees_amount);
            tvDueDate = view.findViewById(R.id.tv_due_date);
            btnPaynow = view.findViewById(R.id.btn_paynow);
            tvFeesPaid = view.findViewById(R.id.tv_fees_paid);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fees, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tvFeesType.setText(feesListParmsArrayList.get(position).getFeesType());
        holder.tvFeesAmount.setText(String.valueOf(feesListParmsArrayList.get(position).getFeesDueAmount() - feesListParmsArrayList.get(position).getFeesDueDiscount()));
        if (!feesListParmsArrayList.get(position).getPaid_status().equals("Pending")) {
            holder.tvDueDate.setText("Paid on " + feesListParmsArrayList.get(position).getFeesPaidDate());
            holder.btnPaynow.setVisibility(View.GONE);
            holder.tvFeesPaid.setVisibility(View.VISIBLE);
            holder.tvFeesPaid.setText(feesListParmsArrayList.get(position).getTransactionId());
        } else {
            holder.tvDueDate.setText("Due on " + feesListParmsArrayList.get(position).getFeesDueDate());
            holder.tvFeesPaid.setVisibility(View.GONE);
            holder.btnPaynow.setVisibility(View.VISIBLE);
        }

        holder.btnPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentActivity.startPayment(feesListParmsArrayList.get(position).getFeesDueAmount(), feesListParmsArrayList.get(position).getFeesDueDiscount(),String.valueOf(feesListParmsArrayList.get(position).getTerm_id()),feesListParmsArrayList.get(position).getUserName(),String.valueOf((feesListParmsArrayList.get(position).getFeesDueAmount() - feesListParmsArrayList.get(position).getFeesDueDiscount())*100),"","");
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

    }

    public void OnsetClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);
    }

    @Override
    public int getItemCount() {
        return feesListParmsArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}