package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.EventListResponse;

import java.util.ArrayList;

public class HomesubtwoAdapter extends BaseAdapter
{
    private Context Context;
    private ArrayList<EventListResponse.className> showing;
    public HomesubtwoAdapter(android.content.Context context, ArrayList<EventListResponse.className> showing) {
        this.Context = context;
        this.showing=showing;
    }

    @Override
    public int getCount() {
        return showing.size();
    }

    @Override
    public Object getItem(int position) {
        return showing.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    Context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.home_sub_itemtwo, null);

        }
        TextView textArtTitle = (TextView) convertView.findViewById(R.id.classs_name);
        //textArtTitle.setText(showing.get(position).getClassName()+" "+showing.get(position).getSection());
        return convertView;
    }
}
