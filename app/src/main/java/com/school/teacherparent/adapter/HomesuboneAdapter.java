package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassworkList;
import com.school.teacherparent.models.ClassworkListAssHome;
import com.school.teacherparent.models.EventListResponse;
import com.school.teacherparent.models.HomeworkListResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HomesuboneAdapter extends BaseAdapter
{
     Context context;

    ArrayList<ClassworkListAssHome> classwiseHomeworkList;

    public HomesuboneAdapter(Context context, ArrayList<ClassworkListAssHome> classwiseHomeworkList) {
        this.context = context;
        this.classwiseHomeworkList=classwiseHomeworkList;
    }

    @Override
    public int getCount() {
        return classwiseHomeworkList.size();
    }

    @Override
    public Object getItem(int position) {
        return classwiseHomeworkList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.home_sub_itemone, null);

        }
        TextView chapterNametextview = (TextView) convertView.findViewById(R.id.chapterNametextview);
        TextView topicTextview = (TextView) convertView.findViewById(R.id.topicTextview);
        TextView duedate = (TextView) convertView.findViewById(R.id.duedate);
        TextView assigndate = (TextView) convertView.findViewById(R.id.assigndate);
        TextView mark = (TextView) convertView.findViewById(R.id.markLabel);
        mark.setVisibility(View.GONE);
        ImageView homeworkImage=(ImageView)convertView.findViewById(R.id.homeworkImage);
        LinearLayout extraSpaceLineraLayout=(LinearLayout)convertView.findViewById(R.id.extraSpaceLineraLayout);
        extraSpaceLineraLayout.setVisibility(View.GONE);
        topicTextview.setText(context.getString(R.string.topic_se)+" "+classwiseHomeworkList.get(position).getTopic());
        chapterNametextview.setText(classwiseHomeworkList.get(position).getChapterName());
        assigndate.setText(getDateFormat(classwiseHomeworkList.get(position).getAssignedOn()));
        duedate.setText(context.getString(R.string.today));


//        if (classwiseAssignmentList.size()==position+1)
//        {
//            extraSpaceLineraLayout.setVisibility(View.VISIBLE);
//        }

//        for (int y = 0; y < classwiseAssignmentList.get(position).getGetTopicDetails().size(); y++) {
//
//
//
//            topicTextview.setText(classwiseAssignmentList.get(position).getGetTopicDetails().get(y).getTopic_name());
//            chapterNametextview.setText(chapter_name);
//            duedate.setText(Context.getString(R.string.today));
//            }

        return convertView;
    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd/MM/yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}
