package com.school.teacherparent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.Interface.AddTouchListener;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassworkList;
import com.school.teacherparent.models.DiaryClassworkList;
import com.school.teacherparent.models.DiaryHomeworkList;
import com.school.teacherparent.models.DiaryListResponse;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.school.teacherparent.fragment.ParentDiaryFragment.getStudentID;

/**
 * Created by harini on 9/11/2018.
 */

public class DiaryAdapter extends RecyclerView.Adapter<DiaryAdapter.ViewHolder> {

    private List<DiaryListResponse.diaryDetailsList> diaryDetailsLists;
    private Context context;
    AddTouchListener addTouchListen;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView subject_name, classs_name, periodtime, period, chapternameTextiview,title,title1,className;
        public ImageView class_work,home_work,subject_image;
        ListView activitiesoftodayclassworkListview,activitiesoftodayhomeworkListview,todayactivitiesclassworkListview,todayactivitieshomeworkListview;
        ExpansionLayout expansionLayout;
        private  CircularImageView classNameImgeview;

        public ViewHolder(View view) {
            super(view);
            subject_name= (TextView) view.findViewById(R.id.subject_name);
            classs_name= (TextView) view.findViewById(R.id.classs_name);
            periodtime= (TextView) view.findViewById(R.id.periodtime);
            period= (TextView) view.findViewById(R.id.period);
            //chapternameTextiview= (TextView) view.findViewById(R.id.chapternameTextiview);
            activitiesoftodayclassworkListview=(ListView)view.findViewById(R.id.activitiesoftodayclassworkListview);
            activitiesoftodayhomeworkListview=(ListView)view.findViewById(R.id.activitiesoftodayhomeworkListview);
            todayactivitiesclassworkListview=(ListView)view.findViewById(R.id.todayactivitiesclassworkListview);
            todayactivitieshomeworkListview=(ListView)view.findViewById(R.id.todayactivitieshomeworkListview);
            class_work=(ImageView) view.findViewById(R.id.class_work);
            home_work=(ImageView) view.findViewById(R.id.home_work);
            title= (TextView) view.findViewById(R.id.title);
            title1= (TextView) view.findViewById(R.id.title1);
            expansionLayout = view.findViewById(R.id.expansionLayout);
           // subject_image=view.findViewById(R.id.subjectImage);
            classNameImgeview=(CircularImageView)view.findViewById(R.id.classNameImgeview);
            className= (TextView) view.findViewById(R.id.className);
           /* filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
*/
        }
    }

    public void setOnClickListen(AddTouchListener addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public DiaryAdapter(List<DiaryListResponse.diaryDetailsList> diaryDetailsLists, Context context) {
        this.diaryDetailsLists = diaryDetailsLists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.diary_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        DiaryListResponse.diaryDetailsList detailsList=diaryDetailsLists.get(position);
        holder.subject_name.setText(detailsList.getName());
        holder.classs_name.setText(detailsList.getAlt_name()+" "+detailsList.getSection());
        //holder.subject_image.setImageResource(Constants.imgid[position]);


        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+
                detailsList.getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.classNameImgeview, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.classNameImgeview.setImageResource(R.drawable.nopreview);
            }
        });

        // holder.className.setText(detailsList.getClassName()+detailsList.getSection());
        if (detailsList.getPeriodTiming().size()>0) {
            holder.periodtime.setText(detailsList.getPeriodTiming().get(0).getStart_time());
            holder.period.setText(ordinal(Integer.parseInt(detailsList.getPeriod()))+" "+context.getString(R.string.period));
        }

        ArrayList<DiaryClassworkList> classworkListstopicListTommroDue = new ArrayList<>();
        ArrayList<DiaryClassworkList> classworkListstopicListTodayDue = new ArrayList<>();
        String tdyClassID="";
        String tmrClassID="";
        for (int j = 0; j < detailsList.getTodaysClassworkProgress().size(); j++) {

            if (detailsList.getTodaysClassworkProgress().get(j).getTodaysActivity()!=1) {

                classworkListstopicListTodayDue.add(new DiaryClassworkList(detailsList.getTodaysClassworkProgress().get(j).getTopicDetails(),detailsList.getTodaysClassworkProgress().get(j).getChapterName()
                        ,detailsList.getTodaysClassworkProgress().get(j).getCompleted_on()));
                tdyClassID = tdyClassID+detailsList.getTodaysClassworkProgress().get(j).getClassworkID();

            } else {
                classworkListstopicListTommroDue.add(new DiaryClassworkList(detailsList.getTodaysClassworkProgress().get(j).getTopicDetails(),detailsList.getTodaysClassworkProgress().get(j).getChapterName()
                        ,detailsList.getTodaysClassworkProgress().get(j).getCompleted_on()));
                tmrClassID = tmrClassID+detailsList.getTodaysClassworkProgress().get(j).getClassworkID();
            }


        }


        if (classworkListstopicListTommroDue.size()>0)
        {
            //holder.activitiesoftodayclassworkListview.setVisibility(View.VISIBLE);
            holder.class_work.setVisibility(View.VISIBLE);
           // holder.expansionLayout.setEnabled(true);
        }
        else if (classworkListstopicListTodayDue.size()>0)
        {
            holder.class_work.setVisibility(View.VISIBLE);
           // holder.expansionLayout.setEnabled(true);
        }
        else
        {
            holder.class_work.setVisibility(View.INVISIBLE);
        }

        DiaryactivitiesTodayclassworkAdapter diaryactivitiesTodayAdapter = new DiaryactivitiesTodayclassworkAdapter(context, classworkListstopicListTodayDue,"",
                context.getString(R.string.class_name) + " " +detailsList.getClassName()+" "+detailsList.getSection(),
                detailsList.getClass_id(),
                detailsList.getSection_id(),
                detailsList.getSubjectID(),
                detailsList.getSchool_id(),
                getStudentID(),
                tdyClassID);
        holder.activitiesoftodayclassworkListview.setAdapter(diaryactivitiesTodayAdapter);
       // Util.setListViewHeightBasedOnChildren(holder.activitiesoftodayclassworkListview);


        DiaryactivitiesTodayclassworkAdapter diaryactivitiesTodayAdapter1 = new DiaryactivitiesTodayclassworkAdapter(context, classworkListstopicListTommroDue,"",
                context.getString(R.string.class_name) + " " +detailsList.getClassName()+" "+detailsList.getSection(),
                detailsList.getClass_id(),
                detailsList.getSection_id(),
                detailsList.getSubjectID(),
                detailsList.getSchool_id(),
                getStudentID(),
                tmrClassID);
        holder.todayactivitiesclassworkListview.setAdapter(diaryactivitiesTodayAdapter1);



        // Util.setListViewHeightBasedOnChildren(holder.todayactivitiesclassworkListview);


//        holder.activitiesoftodayclassworkListview.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//
//        });
//



        ArrayList<DiaryHomeworkList> homeListstopicListTommroDue = new ArrayList<>();
        ArrayList<DiaryHomeworkList> homeListstopicListTodayDue = new ArrayList<>();
        String tdyHomeWorkID = "";
        String tmrHomeWorkID = "";
        for (int j = 0; j < detailsList.getTodaysHomeworkProgress().size(); j++) {

            if (detailsList.getTodaysHomeworkProgress().get(j).getTodaysActivity()!=1) {

                homeListstopicListTodayDue.add(new DiaryHomeworkList(detailsList.getTodaysHomeworkProgress().get(j).getTopicDetails(),detailsList.getTodaysHomeworkProgress().get(j).getChapter_name()
                ,detailsList.getTodaysHomeworkProgress().get(j).getAssigned_on()));
                tdyHomeWorkID = tdyHomeWorkID.concat(String.valueOf((detailsList.getTodaysHomeworkProgress().get(j).getHomeworkID())).concat(","));

            } else {
                homeListstopicListTommroDue.add(new DiaryHomeworkList(detailsList.getTodaysHomeworkProgress().get(j).getTopicDetails(),detailsList.getTodaysHomeworkProgress().get(j).getChapter_name()
                        ,detailsList.getTodaysHomeworkProgress().get(j).getAssigned_on()));
                tmrHomeWorkID = tmrHomeWorkID.concat(String.valueOf((detailsList.getTodaysHomeworkProgress().get(j).getHomeworkID())).concat(","));
            }


        }

        if (homeListstopicListTodayDue.size()>0)
        {
            //holder.activitiesoftodayclassworkListview.setVisibility(View.VISIBLE);
            holder.home_work.setVisibility(View.VISIBLE);
            //holder.expansionLayout.setEnabled(true);
        }
        else if (homeListstopicListTommroDue.size()>0)
        {
            holder.home_work.setVisibility(View.VISIBLE);
           // holder.expansionLayout.setEnabled(true);
        }
        else
        {
            holder.home_work.setVisibility(View.INVISIBLE);
        }
        DiaryactivitiesTodayhomeworkworkAdapter diaryactivitiesTodayhomeworkworkAdapter = new DiaryactivitiesTodayhomeworkworkAdapter(context, homeListstopicListTodayDue,"",
                context.getString(R.string.class_name) + " " +detailsList.getClassName()+" "+detailsList.getSection(),
                detailsList.getClass_id(),
                detailsList.getSection_id(),
                detailsList.getSubjectID(),
                detailsList.getSchool_id(),
                getStudentID(),
                tdyHomeWorkID);
        holder.activitiesoftodayhomeworkListview.setAdapter(diaryactivitiesTodayhomeworkworkAdapter);
       // Util.setListViewHeightBasedOnChildren(holder.activitiesoftodayhomeworkListview);

        DiaryactivitiesTodayhomeworkworkAdapter diaryactivitiesTodayhomeworkworkAdapter1 = new DiaryactivitiesTodayhomeworkworkAdapter(context, homeListstopicListTommroDue,"",
                context.getString(R.string.class_name) + " " +detailsList.getClassName()+" "+detailsList.getSection(),
                detailsList.getClass_id(),
                detailsList.getSection_id(),
                detailsList.getSubjectID(),
                detailsList.getSchool_id(),
                getStudentID(),
                tmrHomeWorkID);
        holder.todayactivitieshomeworkListview.setAdapter(diaryactivitiesTodayhomeworkworkAdapter1);




        if (classworkListstopicListTommroDue.size()<=0 && classworkListstopicListTodayDue.size()<=0 &&
                homeListstopicListTodayDue.size()<=0 && homeListstopicListTommroDue.size()<=0)
        {

            holder.expansionLayout.setEnabled(false);
            holder.home_work.setVisibility(View.INVISIBLE);
            holder.class_work.setVisibility(View.INVISIBLE);
        }

       else
        {

            holder.expansionLayout.setEnabled(true);

        }

        if(classworkListstopicListTodayDue.size()>0 || homeListstopicListTodayDue.size()>0)
        {
            holder.title.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.title.setVisibility(View.GONE);
        }

        if(classworkListstopicListTommroDue.size()>0 || homeListstopicListTommroDue.size()>0)
        {
            holder.title1.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.title1.setVisibility(View.GONE);
        }



    }

    @Override
    public int getItemCount() {
        //return showing.size();
        return diaryDetailsLists.size();
    }



    public static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];

        }
    }

}

