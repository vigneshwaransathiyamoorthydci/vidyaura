package com.school.teacherparent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.models.EventListResponse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class EventInviteAdapter extends BaseAdapter
{
    private Context Context;
    private ArrayList<EventListResponse.className> showing;
    private int state;

    public EventInviteAdapter(android.content.Context context, ArrayList<EventListResponse.className> showing, int state) {
        this.Context = context;
        this.showing=showing;
        this.state=state;
    }

    @Override
    public int getCount() {
        int arraySize = showing.size();
        if (arraySize > 5 && state == 1) {
            arraySize = 5;
        } else if (state == 2) {
            arraySize = showing.size();
        }
        return arraySize;
    }

    @Override
    public Object getItem(int position) {
        return showing.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    Context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.event_invites_list_item, null);

        }
        TextView textArtTitle = (TextView) convertView.findViewById(R.id.classs_name);
        textArtTitle.setText(showing.get(position).getClassName()+" "+showing.get(position).getSection());
        return convertView;
    }
}
