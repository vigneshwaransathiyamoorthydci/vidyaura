package com.school.teacherparent.adapter;

import android.support.v4.view.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.Photoslist;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import android.widget.ImageView;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;

public class CustomPagerAdapter extends PagerAdapter {
    private Context context;
    private List<Photoslist> photoslists;
    private ArrayList<GalleryListResponse.albumList> gallerylists;
    private List<String> imagelist;
    private LayoutInflater layoutInflater;
    private int state;

    public CustomPagerAdapter(Context context, List<Photoslist> photoslists, int state) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.photoslists = photoslists;
        this.state = state;
    }

    public CustomPagerAdapter(Context context, ArrayList<GalleryListResponse.albumList> gallerylists, int state) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.gallerylists = gallerylists;
        this.state = state;
    }

    public CustomPagerAdapter(Context context, int state, List<String> imagelist) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
        this.imagelist = imagelist;
        this.state = state;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (state == 1) {
            count = photoslists.size();
        } else if (state == 2) {
            count = gallerylists.size();
        } else if (state == 3 || state == 4 || state == 5 || state == 6 || state == 7 || state == 8) {
            count = imagelist.size();
        }
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view;
        if (state == 7 || state == 8) {
            view = this.layoutInflater.inflate(R.layout.image_video_feed, container, false);
        } else {
            view = this.layoutInflater.inflate(R.layout.image_video, container, false);
        }

        final ImageView imageView = view.findViewById(R.id.imageview);
        JzvdStd video_selected = view.findViewById(R.id.video_selected);
        video_selected.fullscreenButton.setVisibility(View.GONE);
        video_selected.setAllControlsVisiblity(View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
        JzvdStd.releaseAllVideos();
        System.out.println("url ==> " + state);
        if (state == 1) {
            if (photoslists.get(position).getImage().contains("jpg") || photoslists.get(position).getImage().contains("png") || photoslists.get(position).getImage().contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + photoslists.get(position).getImage();
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            } else {
                video_selected.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);

                video_selected.setUp((context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" + photoslists.get(position).getImage()), ""
                        , Jzvd.SCREEN_WINDOW_NORMAL);
                video_selected.setSystemTimeAndBattery();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                requestOptions.error(R.mipmap.icon_video_thumbnail);
                Glide.with(context)
                        .load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                photoslists.get(position).getImage())
                        .apply(requestOptions)
                        .thumbnail(Glide.with(context).load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                photoslists.get(position).getImage()))
                        .into(video_selected.thumbImageView);
            }
        } else if (state == 2) {
            if (gallerylists.get(position).getMedia_type().contains("jpg") || gallerylists.get(position).getMedia_type().contains("png") || gallerylists.get(position).getMedia_type().contains("jpeg")) {

                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + gallerylists.get(position).getMedia_path();
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            } else {
                video_selected.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);

                video_selected.setUp((context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                gallerylists.get(position).getMedia_path()), ""
                        , Jzvd.SCREEN_WINDOW_NORMAL);
                video_selected.setSystemTimeAndBattery();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                requestOptions.error(R.mipmap.icon_video_thumbnail);
                Glide.with(context)
                        .load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                gallerylists.get(position).getMedia_path())
                        .apply(requestOptions)
                        .thumbnail(Glide.with(context).load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                gallerylists.get(position).getMedia_path()))
                        .into(video_selected.thumbImageView);
            }

        } else if (state == 3) {
            if (imagelist.get(position).contains("jpg") || imagelist.get(position).contains("png") || imagelist.get(position).contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classwork_path) + "/" + imagelist.get(position);
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            }
        } else if (state == 4) {
            if (imagelist.get(position).contains("jpg") || imagelist.get(position).contains("png") || imagelist.get(position).contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_homework_path) + "/" + imagelist.get(position);
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            }
        } else if (state == 5) {
            if (imagelist.get(position).contains("jpg") || imagelist.get(position).contains("png") || imagelist.get(position).contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_assignment_path) + "/" + imagelist.get(position);
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            }
        } else if (state == 6) {
            if (imagelist.get(position).contains("jpg") || imagelist.get(position).contains("png") || imagelist.get(position).contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_circular_path) + "/" + imagelist.get(position);
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });
            }
        } else if (state == 7) {
            if (imagelist.get(position).contains("jpg") || imagelist.get(position).contains("png") || imagelist.get(position).contains("jpeg")) {
                video_selected.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_feeds_path) + "/" + imagelist.get(position);
                Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setImageResource(R.drawable.nopreview);
                    }
                });

            } else {
                video_selected.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);

                video_selected.setUp((context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" + imagelist.get(position)), ""
                        , Jzvd.SCREEN_WINDOW_NORMAL);
                video_selected.setSystemTimeAndBattery();

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
                requestOptions.error(R.mipmap.icon_video_thumbnail);
                Glide.with(context)
                        .load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                imagelist.get(position))
                        .apply(requestOptions)
                        .thumbnail(Glide.with(context).load(context.getString(R.string.s3_baseurl) +
                                context.getString(R.string.s3_feeds_path) + "/" +
                                imagelist.get(position)))
                        .into(video_selected.thumbImageView);
            }
        } else if (state == 8) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            RequestOptions error = new RequestOptions()
                    .placeholder(R.drawable.gallery)
                    .error(R.drawable.gallery)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context.getApplicationContext()).load(imagelist.get(position)).apply(error).into(imageView);

        }


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}