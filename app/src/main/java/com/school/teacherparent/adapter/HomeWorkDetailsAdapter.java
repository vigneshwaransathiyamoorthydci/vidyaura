package com.school.teacherparent.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.DeleteHomeworkParms;
import com.school.teacherparent.models.HomeDetailsResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 9/20/2018.
 */

public class HomeWorkDetailsAdapter extends RecyclerView.Adapter<HomeWorkDetailsAdapter.MyViewHolder> {

    ArrayList<HomeDetailsResponse.homeworkDetailsList> homeworkDetailsListArrayList;
    private Context context;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    BaseActivity baseActivity;
    public HomeWorkDetailsAdapter(ArrayList<HomeDetailsResponse.homeworkDetailsList> homeworkDetailsListArrayList, Context context, BaseActivity baseActivity) {
        this.homeworkDetailsListArrayList=homeworkDetailsListArrayList;
        this.context=context;
        this.baseActivity=baseActivity;
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

       // private ProgressBar prg1,prg2;
        private  TextView counts;
        private  ImageView menu;
        CircularImageView subjectImage;
        private  TextView subjectName,dueDate,chapterName,topicName,className,descriptionTextview;
        LinearLayout header;
        GridView imageGridview;
        RecyclerView image_recycle;
        public MyViewHolder(View view) {
            super(view);
            subjectName = (TextView) view.findViewById(R.id.subjectName);
            dueDate = (TextView) view.findViewById(R.id.dueDate);
            chapterName=(TextView)view.findViewById(R.id.chapterName);
            topicName=(TextView)view.findViewById(R.id.topicName);
            imageGridview=(GridView)view.findViewById(R.id.imageGridview);
            header=view.findViewById(R.id.header);
            menu=view.findViewById(R.id.menu);
            subjectImage=(CircularImageView) view.findViewById(R.id.subjectImage);
            className=(TextView) view.findViewById(R.id.className);
            descriptionTextview=(TextView) view.findViewById(R.id.descriptionTextview);
            image_recycle = view.findViewById(R.id.image_recycle);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 3);
            image_recycle.setLayoutManager(mLayoutManager);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.homedetails_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeDetailsResponse.homeworkDetailsList homeworkDetailsList = homeworkDetailsListArrayList.get(position);
        if (homeworkDetailsList.getIsUserPostedHomework()==0)
        {
            holder.menu.setVisibility(View.GONE);
        }
        else
        {
            holder.menu.setVisibility(View.VISIBLE);
        }
        holder.subjectName.setText(homeworkDetailsList.getSubjectName());
        holder.chapterName.setText(homeworkDetailsList.getChapterName().get(0).getChapter_name());
        holder.topicName.setText(context.getString(R.string.topic_se)+" "+homeworkDetailsList.getTopicDetails());
        holder.dueDate.setText(homeworkDetailsList.getDueDate());

        String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_classworksection_path)+"/"+homeworkDetailsListArrayList.get(position).getClass_image();
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(holder.subjectImage, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                holder.subjectImage.setImageResource(R.drawable.nopreview);
            }
        });

        //holder.subjectImage.setImageResource(Constants.imgid[position]);
       // holder.className.setText(homeworkDetailsList.getClassName()+homeworkDetailsList.getSection());
        holder.descriptionTextview.setText(context.getString(R.string.description)+": "+homeworkDetailsList.getDescription());
        holder.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            context.startActivity(new Intent(context, SidemenuDetailActivity.class)
                                    .putExtra("type", "homeworkupdate").putExtra("mode","edit")
                            .putExtra("homeworkID",homeworkDetailsList.getHomeworkID()));

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            baseActivity.showProgress();
                            DeleteHomeworkParms deleteHomeworkParms=new DeleteHomeworkParms();
                            deleteHomeworkParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            deleteHomeworkParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            deleteHomeworkParms.setHomeworkID(String.valueOf(homeworkDetailsList.getHomeworkID()));
                            deleteHomeworkParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));


                            vidyauraAPI.deleteHomework(deleteHomeworkParms).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    baseActivity.hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                //feedsFragment.getfeedList();
                                                homeworkDetailsListArrayList.remove(position);
                                                notifyDataSetChanged();


                                            } else {
                                                Toast.makeText(context, feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(context, SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            context.startActivity(i);
                                            ((Activity)context).finish();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    baseActivity.hideProgress();
                                    Toast.makeText(context,context.getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else
                        {
                            Toast.makeText(context, context.getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }
                    }



                });

            }
        });
        ImageViewAdapter imageList = new ImageViewAdapter(homeworkDetailsList.getAttachmentsList(), context,context.getString(R.string.s3_homework_path));
        holder.image_recycle.setAdapter(imageList);
        imageList.setOnClickListen(new ImageViewAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Log.d("Vignesh","IMP"+homeworkDetailsList.getAttachmentsList().get(position));
                initiatepopupwindow(homeworkDetailsList.getAttachmentsList().get(position),"png", position, homeworkDetailsList.getAttachmentsList());
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeworkDetailsListArrayList.size();
    }

    private void initiatepopupwindow(String mediapath, String media_type, int position, List<String> imagelist) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd video_selected=alertLayout.findViewById(R.id.video_selected);;
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(context, 4, imagelist));
        viewPager.setCurrentItem(position);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();

        if (imagelist.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
            }
        });
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        /*if (media_type.equals("png")) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            String url = context.getString(R.string.s3_baseurl) + context.getString(R.string.s3_homework_path)+"/"+mediapath;
            Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    imageView.setImageResource(R.mipmap.school);
                }
            });
        }*/

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }
}