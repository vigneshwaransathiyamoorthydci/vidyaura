package com.school.teacherparent.retrofit;


import com.school.teacherparent.models.*;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface VidyAPI {

    @FormUrlEncoded
    @POST("checkuserExists")
    Call<IsUserExists> checkuserExists(@Field("phoneNumber") String phoneNumber, @Field("headerType") int headerType);


    @FormUrlEncoded
    @POST("updateSecurityPin")
    Call<SecurityPinDTO> createSecutiryPin(@Field("phoneNumber") String phoneNumber, @Field("userType") String userType, @Field("securityPin") String securityPin, @Field("checkUserType") int checkUserType);


    @POST("login")
    Call<UserLoginResponse> userLogin(@Body UserLoginParam userLoginParam);

    @POST("login")
    Call<ParentsUserLoginResponse> parentsuserLogin(@Body UserLoginParam userLoginParam);

    @POST("getFeedList")
    Call<FeedListResponse> getFeedList(@Body FeedListParams feedListParams);


    @POST("getFeedSearchList")
    Call<FeedListResponse> getFeedSearchList(@Body FeedListParamsforSearch feedListParams);

    @POST("composeFeed")
    Call<AddFeedResponse> addFeed(@Body AddFeedParams addFeedParams);


    @POST("changeTeacherAvatar")
    Call<UpdateProfileResponse> changeTeacherAvatar(@Body AddProfileParams addProfileParams);


    @POST("updateFeed")
    Call<AddFeedResponse> updateFeed(@Body UpdateFeedParams addFeedParams);

    @POST("updateFeedClaps")
    Call<FeedLikeResponse> updateFeedClaps(@Body FeedLikeParmas feedLikeParmas);

    @POST("updateFeedClaps")
    Call<FeedCommentLikeResponse> updateFeedcommentClaps(@Body FeedLikeParmas feedLikeParmas);

    @POST("feedClapsList")
    Call<FeedClapListResponse> getfeedClapsList(@Body FeedclapsParam feedclapsParam);

    @POST("getFeedCommentList")
    Call<FeedCommentListResponse> getFeedCommentList(@Body FeedCommentListParam feedCommentListParam);


    @FormUrlEncoded
    @POST("getFeedCommentList")
    Call<FeedCommentListResponse> getcommentListByfield(@Field("phoneNumber") String phoneNumber, @Field("userType") String userID, @Field("userID") String userType, @Field("page[size]") String page_size, @Field("page[number]") String page_number, @Field("schoolID") String schoolID, @Field("feedID") String feedID);

    @POST("composeFeedComment")
    Call<FeedCommentcomposeResponse> composeFeedComment(@Body FeedCommentcomposeParam feedCommentcomposeParam);

    @POST("deleteFeed")
    Call<AddFeedResponse> deleteFeed(@Body DeleteFeedParms deleteFeedParms);


    @POST("getFeedDetailsById")
    Call<FeedDetailsByIDResponse> getFeedDetails(@Body FeedDetailsParams feedDetailsParams);


    @POST("getCommentReplyList")
    Call<FeedCommentreplyListResponse> getCommentReplyList(@Body GetCommentReplyParam getCommentReplyParam);

    @FormUrlEncoded
    @POST("getCommentReplyList")
    Call<FeedCommentreplyListResponse> getcommentReplyListByfield(@Field("userID") String userID, @Field("userType") String userType, @Field("page[size]") String page_size, @Field("page[number]") String page_number, @Field("schoolID") String schoolID, @Field("feedID") String feedID, @Field("commentID") String commentID);

    @POST("composeReplyToComment")
    Call<CommentReplyAddedResponse> composeReplyToComment(@Body FeedCommentReplyComposeParam feedCommentcompos);


    @FormUrlEncoded
    @POST("emptySecurityPin")
    Call<EmptySecurityPinResponse> forgetSecutiyPin(@Field("phoneNumber") String phoneNumber, @Field("userType") String userType, @Field("schoolID") String schoolID);


    @FormUrlEncoded
    @POST("getCircularList")
    Call<CircularListResponse> getcircularByfield(@Field("userID") String userID, @Field("userType") String userType, @Field("schoolID") String schoolID, @Field("limit") int page_size, @Field("offset") int page_number);

    @FormUrlEncoded
    @POST("getCircularList")
    Call<CircularListResponse> getcircularByfieldParent(@Field("userID") String userID, @Field("userType") String userType, @Field("schoolID") String schoolID, @Field("limit") int page_size, @Field("offset") int page_number, @Field("studID") int studID);

    @POST("composeCircular")
    Call<AddFeedResponse> composeCircular(@Body AddcircularParam addcircularParam);

    @POST("getTeachersClasses")
    Call<ClassListResponse> getClassList(@Body GetclassListParams getclassListParams);

    @POST("getAllClasses ")
    Call<ClassListResponseforEvents> getAllClasses(@Body GetclassListParams getclassListParams);


    @POST("updateCirculars")
    Call<AddFeedResponse> updateCirculars(@Body UpdatecircularParam updatecircularParam);


    @POST("deleteCircular")
    Call<AddFeedResponse> deleteCircular(@Body DeleteCircularParms deleteCircularParms);


    @POST("getCircularsDetailsById")
    Call<CircularDetailsResponse> getCircularsDetailsById(@Body CircularDetailsParams circularDetailsParams);


    @POST("getUpcomingExamsList")
    Call<ExamUpcomingResponse> getUpcomingExamsList(@Body UpcomingExamListParams upcomingExamListParams);


    @POST("getExamsHistoryList")
    Call<ExamHistoryResponse> getExamsHistoryList(@Body UpcomingExamListParams upcomingExamListParams);

    @POST("getUpcomingExamDetailsByClass")
    Call<UpcomingExamDetailsResponse> getUpcomingExamDetailsByClass(@Body GetUpcomingExamDetailsParam getUpcomingExamDetailsParam);

    @POST("getExamHistoryDetailsByClass")
    Call<ExamHistoryDetailsResponse> getExamHistoryDetailsByClass(@Body GetUpcomingExamDetailsParam getUpcomingExamDetailsParam);


    @POST("getExamTermsList")
    Call<ExamTermResponse> getExamTermsList(@Body GetExamTermParam getExamTermParam);

    @POST("getTeachersSubjects")
    Call<SubjectListResponse> getTeachersSubjects(@Body GetSubjectListParam getSubjectListParam);


    @POST("getChaptersList")
    Call<ChapterListResponse> getExamChaptersList(@Body GetChapterListParam getChapterListParam);


    @POST("getTopicsList")
    Call<TopicListResponse> getTopicsList(@Body GetTopicListParam getTopicListParam);

    @POST("addExamByClass")
    Call<AddFeedResponse> addExamByClass(@Body AddExambyClassParam addExambyClassParam);


    @POST("getEventList")
    Call<EventListResponse> getEventList(@Body EventListParms eventListParms);


    @POST("addEvent")
    Call<AddFeedResponse> addEvent(@Body AddEventParams addEventParams);


    @POST("deleteEvent")
    Call<AddFeedResponse> deleteEvent(@Body DeleteEventParmas eventParmas);


    @POST("updateEvent")
    Call<AddFeedResponse> updateEvent(@Body UpdateEventParams updateEventParams);


    @POST("getEventDetailsById")
    Call<EventdetailsResponse> getEventDetailsById(@Body DeleteEventParmas eventParmas);

    @FormUrlEncoded
    @POST("getExamType")
    Call<ExamTypeResponse> getExamType(@Field("userType") String usertype,@Field("schoolID") String schoolID);

    @POST("deleteExamSchedule")
    Call<AddFeedResponse> deleteExamSchedule(@Body DeleteExamParms examParms);


    @POST("getExamDetailsById")
    Call<ExamDetailByIdResponse> getExamDetailsById(@Body ExamDetailsParams examDetailsParams);

    @POST("getHomeworkList")
    Call<HomeworkListResponse> getHomeworkList(@Body HomeworkListParms homeworkListParms);


    @POST("getAssignmentList")
    Call<AssignmentListResponse> getAssignmentList(@Body HomeworkListParms homeworkListParms);

    @POST("addHomework")
    Call<AddFeedResponse> addHomework(@Body AddhomeworkParams addhomeworkParams);

    @POST("addAssignment")
    Call<AddFeedResponse> addAssignment(@Body AddAssignmentParams addAssignmentParams);


    @POST("getHomeworkDetailsList")
    Call<HomeDetailsResponse> getHomeworkDetailsList(@Body HomeworkdetailsParams homeworkdetailsParams);

    @POST("deleteHomework")
    Call<AddFeedResponse> deleteHomework(@Body DeleteHomeworkParms deleteHomeworkParms);

    @POST("getAcademicTerms")
    Call<AcademicTermResponse> getAcademicTerms(@Body GetExamTermParam getExamTermParam);

    @POST("getHomeworkDetailsById")
    Call<HomeworkDetailsByIDResponse> getHomeworkDetailsById(@Body getHomeworkDetailParam getHomeworkDetailParam);


    @POST("getAssignmentDetailsById")
    Call<AssignmentDetailsByIDResponse> getAssignmentDetailsById(@Body getAssignmentDetailParam getAssignmentDetailParam);


    @POST("updateHomework")
    Call<AddFeedResponse> updateHomework(@Body UpdatehomeworkParams updatehomeworkParams);

    @POST("getAssignmentDetailsList")
    Call<AssignmentDetailsResponse> getAssignmentDetailsList(@Body AssignmentdetailsParams assignmentdetailsParams);

    @POST("deleteAssignment")
    Call<AddFeedResponse> deleteAssignment(@Body DeleteAssignmentParms deleteAssignmentParms);

    @POST("updateAssignment")
    Call<AddFeedResponse> updateAssignment(@Body UpdateAssignmentParams updateAssignmentParams);

    @POST("updateExamByClass")
    Call<AddFeedResponse> updateExamByClass(@Body UpdateExambyClassParam updateExambyClassParam);

    @POST("addClasswork")
    Call<AddFeedResponse> addClasswork(@Body AddClassworkParams addClassworkParams);

    @POST("getClassworkList")
    Call<ClassworkListResponse> getClassworkList(@Body GetClassworkListParam getClassworkListParam);


    @POST("getAttendanceList")
    Call<AttendanceListResponse> getAttendanceList(@Body AttendanceListParams attendanceListParams);

    @POST("getAttendanceList")
    Call<AttendanceListResponseforParents> getAttendanceListforParents(@Body AttendanceListParams attendanceListParams);


    @POST("getAttendanceDetailsList")
    Call<AttendancedetailsResponse> getAttendanceDetailsList(@Body AttendancedetailsParams attendancedetailsParams);


    @POST("getClasswiseStudentList")
    Call<StudentListResponse> getClasswiseStudentList(@Body StudentListbyclassParam studentListbyclassParam);


    @POST("addAttendance")
    Call<AddFeedResponse> addAttendance(@Body AddAttendanceParams addAttendanceParams);

    @POST("getSyllabusList")
    Call<SyllabusListModel> getSyllabusList(@Body FeedListParams feedListParams);

    @POST("getClassworkList")
    Call<ClassworkLists> getClassworkList(@Body FeedListParams feedListParams);


    @POST("getClassworkDetailsList")
    Call<ClassworkDetailsModel> getClassworkDetails(@Body ClassWorkDetailsParms feedListParams);

    @POST("getSyllabusDetailsList")
    Call<SyllabusDetailsModel> getSyllabusdetails(@Body SyllabusDetailParms sylabusdetails);

    @POST("getLeaveDetailsList")
    Call<LeaveModel> getLeaveList(@Body LeaveParms leaveParms);


    @POST("getSyllabusClasses")
    Call<SyllabusClassListResponse> getSyllabusClasses(@Body GetclassListParams getclassListParams);


    @POST("getSyllabusSubjects")
    Call<SyllabusSubjectListResponse> getSyllabusSubjects(@Body GetSubjectListParam getSubjectListParam);


    @POST("getSyllabusChapters")
    Call<SyllabusChapterListResponse> getSyllabusChapters(@Body GetChapterListParam getChapterListParam);

    @POST("getSyllabusTopics")
    Call<SyllabusTopicListResponse> getSyllabusTopics(@Body GetTopicListParam getTopicListParam);

    @POST("deleteClasswork")
    Call<AddFeedResponse> deleteClasswork(@Body DeleteClassworkParms deleteClassworkParms);


    @POST("getClassworkDetailsById")
    Call<ClassworkdetailsResponse> getClassworkDetailsById(@Body DeleteClassworkParms deleteClassworkParms);

    @POST("updateAttendance")
    Call<AddFeedResponse> updateAttendance(@Body AddAttendanceParams addAttendanceParams);

    @POST("getResultsList")
    Call<ResultListResponse> getResultsList(@Body ResultListParams resultListParams);


    @POST("getResultDetailsList")
    Call<ResultDetailsResponse> getResultDetailsList(@Body GetResultDetailsParams getResultDetailsParams);

    @POST("getStudentResultDetails")
    Call<StudentResultDetailsListResponse> getStudentResultDetails(@Body GetStudentResultListParams getStudentResultListParams);

    @POST("getClassesForResults")
    Call<ClassListforResultResponse> getClassesForResults(@Body GetClassForResultparam getClassForResultparam);

    @POST("getSubjectsForResults")
    Call<ResultSubjectListResponse> getSubjectsForResults(@Body GetSubjectListforResultParam getSubjectListParam);


    @POST("getStudentsListForResults")
    Call<StudentListforResultResponse> getStudentsListForResults(@Body StudentListParams studentListParams);


    @POST("addStudentResult")
    Call<AddFeedResponse> addStudentResult(@Body AddResultParams addResultParams);

    @FormUrlEncoded
    @POST("addStudentResult")
    Call<AddFeedResponse> addStudentResultfield(@Field("classID") int classID, @Field("schoolID") String schoolID, @Field("userType") String userType,
                                                @Field("sectionID") int sectionID, @Field("userID") String userID, @Field("examTermID") int examTermID, @Field("studMarksList") String studMarksList,
                                                @Field("subjectID") int subjectID);


    @POST("getTimetableDetails")
    Call<TimeTableModel> getTimetableList(@Body TimetableParms timetableParms);


    @POST("getDiaryDetailsList")
    Call<DiaryListResponse> getDiaryDetailsList(@Body TimetableParms timetableParms);

    @POST("updateClasswork")
    Call<AddFeedResponse> updateClasswork(@Body UpdateClassworkParams updateClassworkParams);


    @POST("getTeachersClassesProfile")
    Call<MyclassesResponse> getTeachersClassesProfile(@Body FeedListParams feedListParams);

    @POST("getTeachersStudentsProfile")
    Call<ParentsofResponse> getTeachersStudentsProfile(@Body FeedListParams feedListParams);

    @POST("getGalleryList")
    Call<GalleryListResponse> getGalleryList(@Body GalleryParams galleryParams);

    @FormUrlEncoded
    @POST("getFAQDetails")
    Call<FaqResponse> getFAQDetails(@Field("userType") String usertype);

    @FormUrlEncoded
    @POST("getFeedback")
    Call<FeedbackDetailsResponse> getFeedback(@Field("userType") String usertype, @Field("schoolID") String schoolid);

    @FormUrlEncoded
    @POST("getPrivacyAboutDetails")
    Call<PrivacyandAboutResponse> getPrivacyAboutDetails(@Field("userType") String usertype);

    @FormUrlEncoded
    @POST("getSchoolDetails")
    Call<SchoolDetailsModel> getSchoolDetail(@Field("schoolID") String schollid, @Field("userType") String usertype);


    @POST("addGallery")
    Call<AddFeedResponse> addGallery(@Body AddGalleryParams addGalleryParams);


    @FormUrlEncoded
    @POST("checkSecurityPin")
    Call<EmptySecurityPinResponse> checkSecurityPin(@Field("phoneNumber") String phoneNumber, @Field("userType") String userType, @Field("securityPin") String securityPin);


    @POST("updateAppSettings")
    Call<EmptySecurityPinResponse> updateAppSettings(@Body AppsettingParmas appsettingParmas);


    @POST("getParentsStudList")
    Call<ChildListResponseResponse> getParentsStudList(@Body GetStudentListParam getStudentListParam);


    @POST("applyLeave")
    Call<AddFeedResponse> applyLeave(@Body AddLeaveParams addLeaveParams);


    @POST("studLeaveHistory")
    Call<LeaveHistoryListResponse> studLeaveHistory(@Body GetStudentHistoryParam getStudentHistoryParam);

    @POST("pollForEvents")
    Call<EventParentListResponse> pollsforevents(@Body EventListParms eventListParms);

    @POST("getPollsList")
    Call<PollsResultListResponse> getPollsList(@Body PollsListParms pollsListParms);

    @POST("getStudentsListforChat")
    Call<StudentsListforChatResponse> getStudentsListforChat(@Body StudentsListforChat studentsListforChat);

    @POST("getStudentsParentsList")
    Call<StudentsParentsListResponse> getStudentsParentsList(@Body StudentsListforChat studentsListforChat);

    @POST("getTeachersListforChat")
    Call<TeachersListforChatResponse> getTeachersListforChat(@Body StudentsListforChat studentsListforChat);

    @POST("getTeachersDetailsforChat")
    Call<ClassHandled> getTeachersDetailsforChat(@Body StudentsListforChat studentsListforChat);

    @POST("getStudentFees")
    Call<FeesListResponse> getStudentFees(@Body GetStudentListParam getStudentListParam);

    @POST("getGroupsListforChat")
    Call<GroupListforChatResponse> getGroupsListforChat(@Body StudentsListforChat studentsListforChat);

    @POST("updateLeaveRequest")
    Call<AddFeedResponse> updateLeaveRequest(@Body UpdateLeaveParams updateLeaveParams);

    @POST("updatePolls")
    Call<PollsResultListResponse> updatePolls(@Body PollsListParms pollsListParms);

    @POST("getTeachersClasses")
    Call<ClassGroupListResponse> getTeachersClasses(@Body GetclassListParams getclassListParams);

    @POST("getGroupwiseStudentList")
    Call<StudentListResponse> getGroupwiseStudentList(@Body StudentListbyclassParam studentListbyclassParam);

    @POST("addGroupAttendance")
    Call<AddFeedResponse> addGroupAttendance(@Body AddAttendanceParams addAttendanceParams);

    @POST("getGroupAttendanceDetailsList")
    Call<AttendancedetailsResponse> getGroupAttendanceDetailsList(@Body AttendancedetailsParams attendancedetailsParams);

    @POST("updateGroupAttendance")
    Call<AddFeedResponse> updateGroupAttendance(@Body AddAttendanceParams addAttendanceParams);

    @POST("sendNotification")
    Call<AddFeedResponse> sendNotification(@Body SendNotification sendNotification);

    @POST("getNotification")
    Call<NotificationList> getNotification(@Body SendNotification sendNotification);

    @POST("updateFeedShare")
    Call<AddFeedResponse> updateFeedShare(@Body DeleteFeedParms deleteFeedParms);

}

