package com.school.teacherparent.Interface;

/**
 * Created by harini on 9/10/2018.
 */

public interface AddTouchListener {
    void onTouchClick(int position);
}
