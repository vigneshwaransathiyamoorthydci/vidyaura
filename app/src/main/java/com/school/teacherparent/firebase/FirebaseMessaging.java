package com.school.teacherparent.firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.SpannableString;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import javax.inject.Inject;

/**
 * Created by keerthana on 8/24/2018.
 */

public class FirebaseMessaging extends FirebaseMessagingService {

    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String TAG = "FirebaseMessagings";
    Bitmap bitmap;
    String message;
    String type = "";
    String imageUri = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        Log.d("FirebaseMessagings", "onMessageReceived: " + remoteMessage.getNotification());
        //sendNotification(remoteMessage.getNotification());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //The message which i send will have keys named [message, image, AnotherActivity] and corresponding values.
        //You can change as per the requirement.

        //message will contain the Push Message
        if (remoteMessage.getData().get("notification") != null) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("notification"));
                if (jsonObject.has("title")) {
                    message = jsonObject.getString("title");
                }/*else if(jsonObject.has("activityName")){
                    message = jsonObject.getString("activityName");
                }*/

                type = jsonObject.getString("notifType");
                Log.e("type", "" + type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //message = remoteMessage.getData().get("message");
        }
        if (remoteMessage.getData().get("image") != null) {
            imageUri = remoteMessage.getData().get("image");
        }
        if (remoteMessage.getData().get("message") != null) {

        }

        //imageUri will contain URL of the image to be displayed with Notification

        //If the key AnotherActivity has  value as True then when the user taps on notification, in the app AnotherActivity will be opened.
        //If the key AnotherActivity has  value as False then when the user taps on notification, in the app MainActivity will be opened.
        // String TrueOrFlase = remoteMessage.getData().get("AnotherActivity");

        Gson gson = new Gson();
        //System.out.println("getNotification ==> "+remoteMessage.getData().get("icon"));
        //System.out.println("getNotification ==> "+remoteMessage.getData().get("title"));
        System.out.println("getNotification ==> "+remoteMessage.getData().get("title"));
        if (remoteMessage.getData().get("title").contains("message")) {
            bitmap = getBitmapfromUrl(remoteMessage.getData().get("icon"));
        } else {
            String logo = getString(R.string.s3_baseurl) + getString(R.string.s3_schools)+remoteMessage.getData().get("icon");
            bitmap = getBitmapfromUrl(logo);
        }

        //bitmap = getBitmapfromUrl(imageUri);

        sendNotification(remoteMessage.getData().get("title"), bitmap, type);

    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    //This method is only generating push icon_notification
    //It is same as we did in earlier posts
    private void sendNotification(String msg, Bitmap icon, String type) {

        String channelId = "School";
        String channelName = msg;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //  Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.icon_notify);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
         *//*NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);*//*
            NotificationChannel allChannel = new NotificationChannel(
                    channelId, channelName, NotificationManager.IMPORTANCE_HIGH);


            ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                    .createNotificationChannel(allChannel);
        }*/

        String formattedBody = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            formattedBody = String.valueOf(new SpannableString(Html.fromHtml(msg, Html.FROM_HTML_MODE_LEGACY)));
        } else {
            formattedBody = String.valueOf(new SpannableString(Html.fromHtml(msg)));
        }

        System.out.println("formatted ==> "+formattedBody);

        if (msg.equals("Your account has been deleted")) {

            //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                    .setLargeIcon(icon)
                    .setSmallIcon(R.drawable.notification_background)
                    .setContentTitle("School")
                    .setContentText(formattedBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(channelId, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationBuilder.setChannelId(channelId);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

            VidyauraApplication.getContext().getComponent().inject(this);
            sharedPreferences.edit().clear().commit();
            Intent i = new Intent(this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        } else {

            Intent intent = new Intent(this, NotificationActivity.class);
            intent.putExtra("type", type);
            intent.putExtra("msg", formattedBody);
            //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                    .setLargeIcon(icon)
                    .setSmallIcon(R.drawable.notification_background)
                    .setContentTitle("School")
                    .setContentText(formattedBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(channelId, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationBuilder.setChannelId(channelId);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

            int notificationID = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
            notificationManager.notify(notificationID, notificationBuilder.build());

            Intent intentNo = new Intent("notificationListener");
            intentNo.putExtra("notify", true);
            sendBroadcast(intentNo);

        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

}
