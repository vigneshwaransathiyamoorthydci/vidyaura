package com.school.teacherparent.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.danikula.videocache.HttpProxyCacheServer;
import com.school.teacherparent.BuildConfig;
import com.school.teacherparent.dagger.AppModule;
import com.school.teacherparent.dagger.ApplicationComponent;
import com.school.teacherparent.dagger.DaggerApplicationComponent;
import com.school.teacherparent.retrofit.RetrofitModule;

import javax.inject.Inject;

public class VidyauraApplication extends MultiDexApplication
{

    @Inject
    public SharedPreferences sharedPreferences;
    private static VidyauraApplication mInstance;
    public static VidyauraApplication getContext() {
        return mInstance;
    }
    private ApplicationComponent mComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(BuildConfig.VIDYAURA_BASE_URL,getContext()))
                .build();
        mComponent.inject(this);
        AWSMobileClient.getInstance().initialize(this).execute();

    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static VidyauraApplication from(@NonNull Context context) {
        return (VidyauraApplication) context.getApplicationContext();
    }
    private HttpProxyCacheServer proxy;
    public static HttpProxyCacheServer getProxy(Context context) {
        VidyauraApplication app = (VidyauraApplication) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;

    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }

}
