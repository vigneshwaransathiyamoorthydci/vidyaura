package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class UpdateProfileResponse
{
    String message;
    String statusText;
    int status;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ArrayList<userDetails> userDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<UpdateProfileResponse.userDetails> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(ArrayList<UpdateProfileResponse.userDetails> userDetails) {
        this.userDetails = userDetails;
    }

    public class userDetails
    {


        String Emp_photo;

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }
    }


}

