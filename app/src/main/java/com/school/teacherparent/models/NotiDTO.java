package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 9/18/2018.
 */

public class NotiDTO {

    private String Id;
    private String Type;
    private int Type_id;
    private String Section_id;
    private String Notification_message;
    private int School_id;
    private String created_at;
    private int Classroom_id;
    private String Classroom_name;
    private String Section_name;
    private int Subject_id;
    private int studentID;
    private String Exam_title;
    private String User_type;
    private String Imageurl;
    private String title;
    private String description;
    private String Type_document;
    private boolean isGroup;

    private String receiverUserID;
    private String receiverName;
    private String receiverImage;
    private String receiverFcmKey;

    private String senderImage;
    private String senderName;
    private String senderUserID;
    private String senderFcmKey;

    private ArrayList<String> receiverFcmKeyy;
    private ArrayList<String> receiverUserIDD;

    private String groupName;
    private String groupImage;
    private String groupID;

    private examwiseClassList examwiseClassList;

    public class examwiseClassList
    {
        private int Classroom_id;
        private String ClassName;
        private String Section;
        private int Classsection_id;

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }
    }

    public NotiDTO(String id, String type, int type_id, String section_id, String notification_message, int school_id, String created_at, int classroom_id, String classroom_name, String section_name, int subject_id, int studentID, String exam_title, String user_type, String imageurl, String title, String description, String type_document, boolean isGroup, String receiverUserID, String receiverName, String receiverImage, String receiverFcmKey, String senderImage, String senderName, String senderUserID, String senderFcmKey, String groupName, String groupImage, String groupID,ArrayList<String> receiverFcmKeyy,ArrayList<String> receiverUserIDD) {
        Id = id;
        Type = type;
        Type_id = type_id;
        Section_id = section_id;
        Notification_message = notification_message;
        School_id = school_id;
        this.created_at = created_at;
        Classroom_id = classroom_id;
        Classroom_name = classroom_name;
        Section_name = section_name;
        Subject_id = subject_id;
        this.studentID = studentID;
        Exam_title = exam_title;
        User_type = user_type;
        Imageurl = imageurl;
        this.title = title;
        this.description = description;
        Type_document = type_document;
        this.isGroup = isGroup;
        this.receiverUserID = receiverUserID;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverFcmKey = receiverFcmKey;
        this.senderImage = senderImage;
        this.senderName = senderName;
        this.senderUserID = senderUserID;
        this.senderFcmKey = senderFcmKey;
        this.groupName = groupName;
        this.groupImage = groupImage;
        this.groupID = groupID;
        this.receiverFcmKeyy = receiverFcmKeyy;
        this.receiverUserIDD = receiverUserIDD;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getType_id() {
        return Type_id;
    }

    public void setType_id(int type_id) {
        Type_id = type_id;
    }

    public String getSection_id() {
        return Section_id;
    }

    public void setSection_id(String section_id) {
        Section_id = section_id;
    }

    public String getNotification_message() {
        return Notification_message;
    }

    public void setNotification_message(String notification_message) {
        Notification_message = notification_message;
    }

    public int getSchool_id() {
        return School_id;
    }

    public void setSchool_id(int school_id) {
        School_id = school_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getClassroom_id() {
        return Classroom_id;
    }

    public void setClassroom_id(int classroom_id) {
        Classroom_id = classroom_id;
    }

    public String getClassroom_name() {
        return Classroom_name;
    }

    public void setClassroom_name(String classroom_name) {
        Classroom_name = classroom_name;
    }

    public String getSection_name() {
        return Section_name;
    }

    public void setSection_name(String section_name) {
        Section_name = section_name;
    }

    public int getSubject_id() {
        return Subject_id;
    }

    public void setSubject_id(int subject_id) {
        Subject_id = subject_id;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getExam_title() {
        return Exam_title;
    }

    public void setExam_title(String exam_title) {
        Exam_title = exam_title;
    }

    public String getUser_type() {
        return User_type;
    }

    public void setUser_type(String user_type) {
        User_type = user_type;
    }

    public String getImageurl() {
        return Imageurl;
    }

    public void setImageurl(String imageurl) {
        Imageurl = imageurl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_document() {
        return Type_document;
    }

    public void setType_document(String type_document) {
        Type_document = type_document;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getReceiverUserID() {
        return receiverUserID;
    }

    public void setReceiverUserID(String receiverUserID) {
        this.receiverUserID = receiverUserID;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverImage() {
        return receiverImage;
    }

    public void setReceiverImage(String receiverImage) {
        this.receiverImage = receiverImage;
    }

    public String getReceiverFcmKey() {
        return receiverFcmKey;
    }

    public void setReceiverFcmKey(String receiverFcmKey) {
        this.receiverFcmKey = receiverFcmKey;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderUserID() {
        return senderUserID;
    }

    public void setSenderUserID(String senderUserID) {
        this.senderUserID = senderUserID;
    }

    public String getSenderFcmKey() {
        return senderFcmKey;
    }

    public void setSenderFcmKey(String senderFcmKey) {
        this.senderFcmKey = senderFcmKey;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public NotiDTO.examwiseClassList getExamwiseClassList() {
        return examwiseClassList;
    }

    public void setExamwiseClassList(NotiDTO.examwiseClassList examwiseClassList) {
        this.examwiseClassList = examwiseClassList;
    }

    public ArrayList<String> getReceiverFcmKeyy() {
        return receiverFcmKeyy;
    }

    public void setReceiverFcmKeyy(ArrayList<String> receiverFcmKeyy) {
        this.receiverFcmKeyy = receiverFcmKeyy;
    }

    public ArrayList<String> getReceiverUserIDD() {
        return receiverUserIDD;
    }

    public void setReceiverUserIDD(ArrayList<String> receiverUserIDD) {
        this.receiverUserIDD = receiverUserIDD;
    }
/*private String senderName;
    private String senderImage;
    private String senderUserID;
    private String senderFcmKey;

    private String receiverName;
    private String receiverImage;
    private String receiverUserID;
    private String receiverFcmKey;

    String message;
    private List<String> imageUrl;
    private int readStatus;
    private String sentTime;
    private Long timeStamp;
    private int state;

    private int unreadCount;
    private boolean isThisGroup;

    private String groupName;
    private String groupImage;
    private String groupID;

    private String readBy;

    public NotiDTO(String senderName, String senderImage, String senderUserID, String senderFcmKey, String receiverName, String receiverImage, String receiverUserID, String receiverFcmKey, String message, int readStatus, String sentTime, Long timeStamp, int state, int unreadCount, boolean isThisGroup) {
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.senderUserID = senderUserID;
        this.senderFcmKey = senderFcmKey;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserID = receiverUserID;
        this.receiverFcmKey = receiverFcmKey;
        this.message = message;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.timeStamp = timeStamp;
        this.state = state;
        this.unreadCount = unreadCount;
        this.isThisGroup = isThisGroup;
    }

    public NotiDTO(String senderName, String senderImage, String senderUserID, String senderFcmKey, String receiverName, String receiverImage, String receiverUserID, String receiverFcmKey, String message, int readStatus, String sentTime, Long timeStamp, int state, int unreadCount, boolean isThisGroup, String groupName, String groupImage, String groupID, String readBy, List<String> imageUrl) {
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.senderUserID = senderUserID;
        this.senderFcmKey = senderFcmKey;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserID = receiverUserID;
        this.receiverFcmKey = receiverFcmKey;
        this.message = message;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.timeStamp = timeStamp;
        this.state = state;
        this.unreadCount = unreadCount;
        this.isThisGroup = isThisGroup;
        this.groupName = groupName;
        this.groupImage = groupImage;
        this.groupID = groupID;
        this.readBy = readBy;
        this.imageUrl = imageUrl;
    }*/

}