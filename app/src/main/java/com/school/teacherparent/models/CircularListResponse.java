package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;


public class CircularListResponse {
    private String message;
    private String statusText;
    private int status;
    public ArrayList<circularList> circularList;

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<CircularListResponse.circularList> getCircularList() {
        return circularList;
    }

    public void setCircularList(ArrayList<CircularListResponse.circularList> circularList) {
        this.circularList = circularList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public  class circularList
    {
        private int schoolID;
        private String schoolName;
        private String schoolLogo;
        private int Id;
        private String Circular_no;
        private String Title;
        private String Description;
        private String Classroom_id;
        private String Author_type;
        private int Created_by;
        private int isUserPostedCircular;
        private String created_at;
        List<String>attachmentsList;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getSchoolID() {
            return schoolID;
        }

        public void setSchoolID(int schoolID) {
            this.schoolID = schoolID;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getSchoolLogo() {
            return schoolLogo;
        }

        public void setSchoolLogo(String schoolLogo) {
            this.schoolLogo = schoolLogo;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getCircular_no() {
            return Circular_no;
        }

        public void setCircular_no(String circular_no) {
            Circular_no = circular_no;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(String classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getAuthor_type() {
            return Author_type;
        }

        public void setAuthor_type(String author_type) {
            Author_type = author_type;
        }

        public int getCreated_by() {
            return Created_by;
        }

        public void setCreated_by(int created_by) {
            Created_by = created_by;
        }

        public int getIsUserPostedCircular() {
            return isUserPostedCircular;
        }

        public void setIsUserPostedCircular(int isUserPostedCircular) {
            this.isUserPostedCircular = isUserPostedCircular;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }








}