package com.school.teacherparent.models;

import java.util.ArrayList;

public class ExamHistoryResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<examsHistoryList> examsHistoryList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ExamHistoryResponse.examsHistoryList> getExamsHistoryList() {
        return examsHistoryList;
    }

    public void setExamsHistoryList(ArrayList<ExamHistoryResponse.examsHistoryList> examsHistoryList) {
        this.examsHistoryList = examsHistoryList;
    }

    public class  examsHistoryList
    {
        private int Term_id;
        private String Exam_title;
        private String Exam_duration;
        private String End_date;
        private String Start_date;

        private int Classroom_id;
        private String ClassName;
        private String Section;
        private int Classsection_id;

        public String getStart_date() {
            return Start_date;
        }

        public void setStart_date(String start_date) {
            Start_date = start_date;
        }

        private String School_name;
        private String Logo;
        ArrayList<examwiseClassList> examwiseClassList;

        public ArrayList<ExamHistoryResponse.examwiseClassList> getExamwiseClassList() {
            return examwiseClassList;
        }

        public void setExamwiseClassList(ArrayList<ExamHistoryResponse.examwiseClassList> examwiseClassList) {
            this.examwiseClassList = examwiseClassList;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public String getExam_title() {
            return Exam_title;
        }

        public void setExam_title(String exam_title) {
            Exam_title = exam_title;
        }

        public String getExam_duration() {
            return Exam_duration;
        }

        public void setExam_duration(String exam_duration) {
            Exam_duration = exam_duration;
        }

        public String getEnd_date() {
            return End_date;
        }

        public void setEnd_date(String end_date) {
            End_date = end_date;
        }

        public String getSchool_name() {
            return School_name;
        }

        public void setSchool_name(String school_name) {
            School_name = school_name;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }
    }

    public class examwiseClassList
    {
        private int Classroom_id;
        private String ClassName;
        private String Section;
        private int Classsection_id;

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }
    }

}
