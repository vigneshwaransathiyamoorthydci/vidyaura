package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentListResponse
{

    String message;
    String statusText;
    int status;
    ArrayList<classwiseStudentsList> classwiseStudentsList;
    ArrayList<groupwiseStudentsList> groupwiseStudentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<StudentListResponse.classwiseStudentsList> getClasswiseStudentsList() {
        return classwiseStudentsList;
    }

    public void setClasswiseStudentsList(ArrayList<StudentListResponse.classwiseStudentsList> classwiseStudentsList) {
        this.classwiseStudentsList = classwiseStudentsList;
    }

    public ArrayList<StudentListResponse.groupwiseStudentsList> getGroupwiseStudentsList() {
        return groupwiseStudentsList;
    }

    public void setGroupwiseStudentsList(ArrayList<StudentListResponse.groupwiseStudentsList> groupwiseStudentsList) {
        this.groupwiseStudentsList = groupwiseStudentsList;
    }

    public class  attendanceDetails
    {
        String Attendance_type;
        int Od_id;
        int teacherIncharge;

        public int getOd_id() {
            return Od_id;
        }

        public void setOd_id(int od_id) {
            Od_id = od_id;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }

        public int getTeacherIncharge() {
            return teacherIncharge;
        }

        public void setTeacherIncharge(int teacherIncharge) {
            this.teacherIncharge = teacherIncharge;
        }
    }

    public class classwiseStudentsList
    {

            String Fname;
            String Lname;
            String Student_photo;
            int studID;
        ArrayList<attendanceDetails> attendanceDetails;

        public ArrayList<StudentListResponse.attendanceDetails> getAttendanceDetails() {
            return attendanceDetails;
        }

        public void setAttendanceDetails(ArrayList<StudentListResponse.attendanceDetails> attendanceDetails) {
            this.attendanceDetails = attendanceDetails;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }
    }

    public class groupwiseStudentsList {
        String Fname;
        String Lname;
        String Student_photo;
        int studID;
        ArrayList<attendanceDetails> attendanceDetails;

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public ArrayList<StudentListResponse.attendanceDetails> getAttendanceDetails() {
            return attendanceDetails;
        }

        public void setAttendanceDetails(ArrayList<StudentListResponse.attendanceDetails> attendanceDetails) {
            this.attendanceDetails = attendanceDetails;
        }
    }

}
