package com.school.teacherparent.models;

import java.util.ArrayList;

public class AttendanceListResponse
{
    String message;
    String statusText;
    int status;
    ArrayList<attendanceList> attendanceList;
    ArrayList<groupattendanceList> groupattendanceList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AttendanceListResponse.attendanceList> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(ArrayList<AttendanceListResponse.attendanceList> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public ArrayList<AttendanceListResponse.groupattendanceList> getGroupattendanceList() {
        return groupattendanceList;
    }

    public void setGroupattendanceList(ArrayList<AttendanceListResponse.groupattendanceList> groupattendanceList) {
        this.groupattendanceList = groupattendanceList;
    }

    public class attendanceList
    {
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        String totalStudentsCount;
        String studentsPresentCount;
        String studentsAbsentCount;
        String iconName;
        private String Class_image;
        private int attendanceSessionCount;

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        ArrayList<todaysAbsenteesList> todaysAbsenteesList;

        public ArrayList<AttendanceListResponse.todaysAbsenteesList> getTodaysAbsenteesList() {
            return todaysAbsenteesList;
        }

        public void setTodaysAbsenteesList(ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList) {
            this.todaysAbsenteesList = todaysAbsenteesList;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getTotalStudentsCount() {
            return totalStudentsCount;
        }

        public void setTotalStudentsCount(String totalStudentsCount) {
            this.totalStudentsCount = totalStudentsCount;
        }

        public String getStudentsPresentCount() {
            return studentsPresentCount;
        }

        public void setStudentsPresentCount(String studentsPresentCount) {
            this.studentsPresentCount = studentsPresentCount;
        }

        public String getStudentsAbsentCount() {
            return studentsAbsentCount;
        }

        public void setStudentsAbsentCount(String studentsAbsentCount) {
            this.studentsAbsentCount = studentsAbsentCount;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public int getAttendanceSessionCount() {
            return attendanceSessionCount;
        }

        public void setAttendanceSessionCount(int attendanceSessionCount) {
            this.attendanceSessionCount = attendanceSessionCount;
        }
    }
    public class  todaysAbsenteesList
    {
        int studID;
        String Attendance_type;
        int attendanceID;

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }

        public int getAttendanceID() {
            return attendanceID;
        }

        public void setAttendanceID(int attendanceID) {
            this.attendanceID = attendanceID;
        }
    }

    public class groupattendanceList {

        int Group_id;
        String iconName;
        String groupimage;
        int attendanceSessionCount;
        String totalStudentsCount;
        String studentsPresentCount;
        String studentsAbsentCount;
        ArrayList<todaysAbsenteesList> todaysAbsenteesList;

        public int getGroup_id() {
            return Group_id;
        }

        public void setGroup_id(int group_id) {
            Group_id = group_id;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getGroupimage() {
            return groupimage;
        }

        public void setGroupimage(String groupimage) {
            this.groupimage = groupimage;
        }

        public int getAttendanceSessionCount() {
            return attendanceSessionCount;
        }

        public void setAttendanceSessionCount(int attendanceSessionCount) {
            this.attendanceSessionCount = attendanceSessionCount;
        }

        public String getTotalStudentsCount() {
            return totalStudentsCount;
        }

        public void setTotalStudentsCount(String totalStudentsCount) {
            this.totalStudentsCount = totalStudentsCount;
        }

        public String getStudentsPresentCount() {
            return studentsPresentCount;
        }

        public void setStudentsPresentCount(String studentsPresentCount) {
            this.studentsPresentCount = studentsPresentCount;
        }

        public String getStudentsAbsentCount() {
            return studentsAbsentCount;
        }

        public void setStudentsAbsentCount(String studentsAbsentCount) {
            this.studentsAbsentCount = studentsAbsentCount;
        }

        public ArrayList<AttendanceListResponse.todaysAbsenteesList> getTodaysAbsenteesList() {
            return todaysAbsenteesList;
        }

        public void setTodaysAbsenteesList(ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList) {
            this.todaysAbsenteesList = todaysAbsenteesList;
        }
    }

}
