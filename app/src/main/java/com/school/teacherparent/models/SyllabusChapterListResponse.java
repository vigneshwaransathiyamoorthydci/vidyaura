package com.school.teacherparent.models;

import java.util.ArrayList;

public class SyllabusChapterListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<syllabusChaptersList> syllabusChaptersList;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<SyllabusChapterListResponse.syllabusChaptersList> getSyllabusChaptersList() {
        return syllabusChaptersList;
    }

    public void setSyllabusChaptersList(ArrayList<SyllabusChapterListResponse.syllabusChaptersList> syllabusChaptersList) {
        this.syllabusChaptersList = syllabusChaptersList;
    }

    public class syllabusChaptersList
    {
        private String Chapter_name;
        private String Section;
        private int classID;
        private int subjectID;
        int chapterID;

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public int getChapterID() {
            return chapterID;
        }

        public void setChapterID(int chapterID) {
            this.chapterID = chapterID;
        }
    }
}
