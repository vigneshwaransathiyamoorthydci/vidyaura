package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class SchoolDetailsModel {
    public String message;
    public String statusText;
    public Integer status;
    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<SchoolDetail> schoolDetails = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SchoolDetail> getSchoolDetails() {
        return schoolDetails;
    }

    public void setSchoolDetails(List<SchoolDetail> schoolDetails) {
        this.schoolDetails = schoolDetails;
    }

    public class SchoolDetail {

        public Integer Id;
        public String School_name;

        public String Logo;
        public String Description;
        public String Principal_name;
        public String Principal_image;
        public String Principal_message;
        public  ArrayList<testimonials> testimonials;
        List<String> attachmentArr;

        public List<String> getAttachmentArr() {
            return attachmentArr;
        }

        public void setAttachmentArr(List<String> attachmentArr) {
            this.attachmentArr = attachmentArr;
        }

        public ArrayList<SchoolDetailsModel.testimonials> getTestimonials() {
            return testimonials;
        }

        public void setTestimonials(ArrayList<SchoolDetailsModel.testimonials> testimonials) {
            this.testimonials = testimonials;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getPrincipal_name() {
            return Principal_name;
        }

        public void setPrincipal_name(String principal_name) {
            Principal_name = principal_name;
        }

        public String getPrincipal_image() {
            return Principal_image;
        }

        public void setPrincipal_image(String principal_image) {
            Principal_image = principal_image;
        }

        public String getPrincipal_message() {
            return Principal_message;
        }

        public void setPrincipal_message(String principal_message) {
            Principal_message = principal_message;
        }

        public Integer getId() {
            return Id;
        }

        public void setId(Integer id) {
            Id = id;
        }

        public String getSchool_name() {
            return School_name;
        }

        public void setSchool_name(String school_name) {
            School_name = school_name;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }



    }

    public class testimonials
    {
        int Id;
        String Name;
        String Position;
        String Testimonial;
        int Rating;
        int School_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getPosition() {
            return Position;
        }

        public void setPosition(String position) {
            Position = position;
        }

        public String getTestimonial() {
            return Testimonial;
        }

        public void setTestimonial(String testimonial) {
            Testimonial = testimonial;
        }

        public int getRating() {
            return Rating;
        }

        public void setRating(int rating) {
            Rating = rating;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }
    }
}
