package com.school.teacherparent.models;

import java.util.ArrayList;

public class FeesListParms {

    private String userName;
    private String feesType;
    private int feesDueAmount;
    private int feesDueDiscount;
    private String feesDueDate;
    private String feesPaidDate;
    private String Paid_status;
    private String transactionId;
    private int Term_id;

    public FeesListParms(String userName, String feesType, int feesDueAmount, int feesDueDiscount, String feesDueDate, String feesPaidDate, String Paid_status, String transactionId,int Term_id) {
        this.userName = userName;
        this.feesType = feesType;
        this.feesDueAmount = feesDueAmount;
        this.feesDueDiscount = feesDueDiscount;
        this.feesDueDate = feesDueDate;
        this.feesPaidDate = feesPaidDate;
        this.Paid_status = Paid_status;
        this.transactionId = transactionId;
        this.Term_id = Term_id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFeesType() {
        return feesType;
    }

    public void setFeesType(String feesType) {
        this.feesType = feesType;
    }

    public int getFeesDueAmount() {
        return feesDueAmount;
    }

    public void setFeesDueAmount(int feesDueAmount) {
        this.feesDueAmount = feesDueAmount;
    }

    public int getFeesDueDiscount() {
        return feesDueDiscount;
    }

    public void setFeesDueDiscount(int feesDueDiscount) {
        this.feesDueDiscount = feesDueDiscount;
    }

    public String getFeesDueDate() {
        return feesDueDate;
    }

    public void setFeesDueDate(String feesDueDate) {
        this.feesDueDate = feesDueDate;
    }

    public String getFeesPaidDate() {
        return feesPaidDate;
    }

    public void setFeesPaidDate(String feesPaidDate) {
        this.feesPaidDate = feesPaidDate;
    }

    public String getPaid_status() {
        return Paid_status;
    }

    public void setPaid_status(String paid_status) {
        Paid_status = paid_status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getTerm_id() {
        return Term_id;
    }

    public void setTerm_id(int term_id) {
        Term_id = term_id;
    }
}
