package com.school.teacherparent.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ExamHistoryList
{
    private int Term_id;
    private String Exam_title;
    private String Exam_duration;
    private String Start_date;
    private String End_date;
    private String School_name;
    private String Logo;
    ArrayList<ExamHistoryResponse.examwiseClassList> examwiseClassList;

    private int Classroom_id;
    private String ClassName;
    private String Section;
    private int Classsection_id;

    public ExamHistoryList(int term_id, String exam_title, String exam_duration, String start_date, String end_date, String school_name, String logo,   ArrayList<ExamHistoryResponse.examwiseClassList> examwiseClassList) {
        Term_id = term_id;
        Exam_title = exam_title;
        Exam_duration = exam_duration;
        Start_date = start_date;
        End_date = end_date;
        School_name = school_name;
        Logo = logo;
        this.examwiseClassList = examwiseClassList;
    }

    public ExamHistoryList(int term_id, String exam_title, String exam_duration, String start_date, String end_date, String school_name, String logo, int classroom_id, String className, String section, int classsection_id) {
        Term_id = term_id;
        Exam_title = exam_title;
        Exam_duration = exam_duration;
        Start_date = start_date;
        End_date = end_date;
        School_name = school_name;
        Logo = logo;
        Classroom_id = classroom_id;
        ClassName = className;
        Section = section;
        Classsection_id = classsection_id;
    }

    public class examwiseClassList implements Serializable
    {
        private int Classroom_id;
        private String ClassName;
        private String Section;
        private int Classsection_id;

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }
    }
    public int getTerm_id() {
        return Term_id;
    }

    public void setTerm_id(int term_id) {
        Term_id = term_id;
    }

    public String getExam_title() {
        return Exam_title;
    }

    public void setExam_title(String exam_title) {
        Exam_title = exam_title;
    }

    public String getExam_duration() {
        return Exam_duration;
    }

    public void setExam_duration(String exam_duration) {
        Exam_duration = exam_duration;
    }

    public String getStart_date() {
        return Start_date;
    }

    public void setStart_date(String start_date) {
        Start_date = start_date;
    }

    public String getEnd_date() {
        return End_date;
    }

    public void setEnd_date(String end_date) {
        End_date = end_date;
    }

    public String getSchool_name() {
        return School_name;
    }

    public void setSchool_name(String school_name) {
        School_name = school_name;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public ArrayList<ExamHistoryResponse.examwiseClassList> getExamwiseClassList() {
        return examwiseClassList;
    }

    public void setExamwiseClassList(ArrayList<ExamHistoryResponse.examwiseClassList> examwiseClassList) {
        this.examwiseClassList = examwiseClassList;
    }

    public int getClassroom_id() {
        return Classroom_id;
    }

    public void setClassroom_id(int classroom_id) {
        Classroom_id = classroom_id;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public int getClasssection_id() {
        return Classsection_id;
    }

    public void setClasssection_id(int classsection_id) {
        Classsection_id = classsection_id;
    }
}
