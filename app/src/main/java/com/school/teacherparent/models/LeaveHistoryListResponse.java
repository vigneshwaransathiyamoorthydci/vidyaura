package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class LeaveHistoryListResponse
{

    private String message;
    private String statusText;
    private int status;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<studLeaveHistory> studLeaveHistory;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<LeaveHistoryListResponse.studLeaveHistory> getStudLeaveHistory() {
        return studLeaveHistory;
    }

    public void setStudLeaveHistory(ArrayList<LeaveHistoryListResponse.studLeaveHistory> studLeaveHistory) {
        this.studLeaveHistory = studLeaveHistory;
    }

    public class studLeaveHistory
    {



            int Class_id;
            int Section_id;
            int Father_id;
            int Mother_id;
            int Guardian_Id;
            String Fname;
            String Lname;
            int studID;
            String ClassName;
            String Section;
            ArrayList<leaveHistory>leaveHistory;

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getFather_id() {
            return Father_id;
        }

        public void setFather_id(int father_id) {
            Father_id = father_id;
        }

        public int getMother_id() {
            return Mother_id;
        }

        public void setMother_id(int mother_id) {
            Mother_id = mother_id;
        }

        public int getGuardian_Id() {
            return Guardian_Id;
        }

        public void setGuardian_Id(int guardian_Id) {
            Guardian_Id = guardian_Id;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public ArrayList<LeaveHistoryListResponse.leaveHistory> getLeaveHistory() {
            return leaveHistory;
        }

        public void setLeaveHistory(ArrayList<LeaveHistoryListResponse.leaveHistory> leaveHistory) {
            this.leaveHistory = leaveHistory;
        }
    }

        public  class  leaveHistory {
            private int Id;
            int Academic_year;
            int Class_id;
            int Section_id;
            int Parents_id;
            int Student_id;
            int Days_count;
            int Is_halfday;
            String Leave_session;
            String From_date;
            String To_date;
            String Leave_status;
            String Teachers_remark;
            int School_id;
            String Reason;

            public String getReason() {
                return Reason;
            }

            public void setReason(String reason) {
                Reason = reason;
            }

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public int getAcademic_year() {
                return Academic_year;
            }

            public void setAcademic_year(int academic_year) {
                Academic_year = academic_year;
            }

            public int getClass_id() {
                return Class_id;
            }

            public void setClass_id(int class_id) {
                Class_id = class_id;
            }

            public int getSection_id() {
                return Section_id;
            }

            public void setSection_id(int section_id) {
                Section_id = section_id;
            }

            public int getParents_id() {
                return Parents_id;
            }

            public void setParents_id(int parents_id) {
                Parents_id = parents_id;
            }

            public int getStudent_id() {
                return Student_id;
            }

            public void setStudent_id(int student_id) {
                Student_id = student_id;
            }

            public int getDays_count() {
                return Days_count;
            }

            public void setDays_count(int days_count) {
                Days_count = days_count;
            }

            public int getIs_halfday() {
                return Is_halfday;
            }

            public void setIs_halfday(int is_halfday) {
                Is_halfday = is_halfday;
            }

            public String getLeave_session() {
                return Leave_session;
            }

            public void setLeave_session(String leave_session) {
                Leave_session = leave_session;
            }

            public String getFrom_date() {
                return From_date;
            }

            public void setFrom_date(String from_date) {
                From_date = from_date;
            }

            public String getTo_date() {
                return To_date;
            }

            public void setTo_date(String to_date) {
                To_date = to_date;
            }

            public String getLeave_status() {
                return Leave_status;
            }

            public void setLeave_status(String leave_status) {
                Leave_status = leave_status;
            }

            public String getTeachers_remark() {
                return Teachers_remark;
            }

            public void setTeachers_remark(String teachers_remark) {
                Teachers_remark = teachers_remark;
            }

            public int getSchool_id() {
                return School_id;
            }

            public void setSchool_id(int school_id) {
                School_id = school_id;
            }
        }
    }


