package com.school.teacherparent.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ExamDetailByIdResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<examScheduleList> examScheduleList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ExamDetailByIdResponse.examScheduleList> getExamScheduleList() {
        return examScheduleList;
    }

    public void setExamScheduleList(ArrayList<ExamDetailByIdResponse.examScheduleList> examScheduleList) {
        this.examScheduleList = examScheduleList;
    }

    public class  examScheduleList
    {
        int Id;
        String Exam_duration;
        String Start_time;
        String End_time;
        int Classsection_id;
        int Classroom_id;
        String Chapter_id;
        String Topic_id;

        int Term_id;
        int Subject_id;
        int Exam_type;
        int Marks;
        int Min_mark;
        int Class_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getExam_duration() {
            return Exam_duration;
        }

        public void setExam_duration(String exam_duration) {
            Exam_duration = exam_duration;
        }

        public String getStart_time() {
            return Start_time;
        }

        public void setStart_time(String start_time) {
            Start_time = start_time;
        }

        public String getEnd_time() {
            return End_time;
        }

        public void setEnd_time(String end_time) {
            End_time = end_time;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getExam_type() {
            return Exam_type;
        }

        public void setExam_type(int exam_type) {
            Exam_type = exam_type;
        }

        public int getMarks() {
            return Marks;
        }

        public void setMarks(int marks) {
            Marks = marks;
        }

        public int getMin_mark() {
            return Min_mark;
        }

        public void setMin_mark(int min_mark) {
            Min_mark = min_mark;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }
    }

    private class  topicName
    {
        int Id;
        int Class_id;
        int Subject_id;
        int Chapter_id;
        String Topic_name;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(int chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }
    }

    private class  chapterName
    {
        int Id;
        int Class_id;
        int Subject_id;
        String Chapter_name;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }
    }


}
