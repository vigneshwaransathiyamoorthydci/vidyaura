package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class FeedListResponse
{

    private String message;
    private String statusText;
    private int status;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<feedResult> feedResult;

    public ArrayList<FeedListResponse.feedResult> getFeedResult() {
        return feedResult;
    }

    public void setFeedResult(ArrayList<FeedListResponse.feedResult> feedResult) {
        this.feedResult = feedResult;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public class feedResult
    {



            private int Id;
            private String Title;
            private String Description;
            private int Author_id;
            private String Author_type;
            private String Visibility;
            private String Created_at;
            private String Salutation;
            private String Fname;
            private String Lname;
            private String Emp_photo;
            private int Feed_id;
            private int feedLikeCount;
            private int feedCommentCount;
            private int feedShareCount;
            private int isUserLiked;
            private int isUserCommented;
            private String schoolName;
            private String School_code;
            private String Logo;
            private String name;
            private int isFeedPostedBy;
            private String Publish_date;
            private String schoolLogo;
            private List<String> attachmentsList;

        public String getSchoolLogo() {
            return schoolLogo;
        }

        public void setSchoolLogo(String schoolLogo) {
            this.schoolLogo = schoolLogo;
        }

        private ArrayList<userDetails> userDetails;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public ArrayList<FeedListResponse.feedResult.userDetails> getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(ArrayList<FeedListResponse.feedResult.userDetails> userDetails) {
            this.userDetails = userDetails;
        }

        public  class  userDetails
            {
                private int Id;
                private String Fname;
                private String Lname;
                private String Emp_photo;

                public int getId() {
                    return Id;
                }

                public void setId(int id) {
                    Id = id;
                }

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getLname() {
                    return Lname;
                }

                public void setLname(String lname) {
                    Lname = lname;
                }

                public String getEmp_photo() {
                    return Emp_photo;
                }

                public void setEmp_photo(String emp_photo) {
                    Emp_photo = emp_photo;
                }
            }

            public String getPublish_date() {
                return Publish_date;
            }

            public void setPublish_date(String publish_date) {
                Publish_date = publish_date;
            }

            public int getIsFeedPostedBy() {
                return isFeedPostedBy;
            }

            public void setIsFeedPostedBy(int isFeedPostedBy) {
                this.isFeedPostedBy = isFeedPostedBy;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getFeed_id() {
                return Feed_id;
            }

            public int getFeedLikeCount() {
                return feedLikeCount;
            }

            public void setFeedLikeCount(int feedLikeCount) {
                this.feedLikeCount = feedLikeCount;
            }

            public int getFeedCommentCount() {
                return feedCommentCount;
            }

            public void setFeedCommentCount(int feedCommentCount) {
                this.feedCommentCount = feedCommentCount;
            }

            public int getFeedShareCount() {
                return feedShareCount;
            }

            public void setFeedShareCount(int feedShareCount) {
                this.feedShareCount = feedShareCount;
            }

            public int getIsUserLiked() {
                return isUserLiked;
            }

            public void setIsUserLiked(int isUserLiked) {
                this.isUserLiked = isUserLiked;
            }

            public int getIsUserCommented() {
                return isUserCommented;
            }

            public void setIsUserCommented(int isUserCommented) {
                this.isUserCommented = isUserCommented;
            }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public String getSchool_code() {
                return School_code;
            }

            public void setSchool_code(String school_code) {
                School_code = school_code;
            }

            public String getLogo() {
                return Logo;
            }

            public void setLogo(String logo) {
                Logo = logo;
            }

            public void setFeed_id(int feed_id) {
                Feed_id = feed_id;
            }

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String title) {
                Title = title;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String description) {
                Description = description;
            }

            public int getAuthor_id() {
                return Author_id;
            }

            public void setAuthor_id(int author_id) {
                Author_id = author_id;
            }

            public String getAuthor_type() {
                return Author_type;
            }

            public void setAuthor_type(String author_type) {
                Author_type = author_type;
            }

            public String getVisibility() {
                return Visibility;
            }

            public void setVisibility(String visibility) {
                Visibility = visibility;
            }

            public String getCreated_at() {
                return Created_at;
            }

            public void setCreated_at(String created_at) {
                Created_at = created_at;
            }

            public String getSalutation() {
                return Salutation;
            }

            public void setSalutation(String salutation) {
                Salutation = salutation;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }

            public String getEmp_photo() {
                return Emp_photo;
            }

            public void setEmp_photo(String emp_photo) {
                Emp_photo = emp_photo;
            }
        }
    }


