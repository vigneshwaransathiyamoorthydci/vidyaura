package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class MessageList {

    private String senderName;
    private String senderImage;
    private String senderUserID;
    private String senderFcmKey;

    private String receiverName;
    private String receiverImage;
    private String receiverUserID;
    private String receiverFcmKey;
    private ArrayList<String> receiverFcmKeyy;
    private ArrayList<String> receiverUserIDD;

    String message;
    private List<String> imageUrl;
    private int readStatus;
    private String sentTime;
    private Long timeStamp;
    private int state;

    private String receiverRole;
    private String receiverClass;

    private int unreadCount;
    private boolean isThisGroup;

    private String groupName;
    private String groupImage;
    private String groupID;

    private String readBy;
    private String msgID;

    public ArrayList<String> getReceiverFcmKeyy() {
        return receiverFcmKeyy;
    }

    public void setReceiverFcmKeyy(ArrayList<String> receiverFcmKeyy) {
        this.receiverFcmKeyy = receiverFcmKeyy;
    }

    //single message
    public MessageList(boolean isThisGroup,String senderName, String senderImage, String senderUserID, String senderFcmKey, String receiverName, String receiverImage, String receiverUserID, String receiverFcmKey, String message, List<String> imageUrl, int readStatus, String sentTime, Long timeStamp, int state) {
        this.isThisGroup = isThisGroup;
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.senderUserID = senderUserID;
        this.senderFcmKey = senderFcmKey;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserID = receiverUserID;
        this.receiverFcmKey = receiverFcmKey;
        this.message = message;
        this.imageUrl = imageUrl;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.timeStamp = timeStamp;
        this.state = state;
    }

    //group message
    public MessageList(boolean isThisGroup, String groupID, String groupName, String groupImage, ArrayList<String> receiverFcmKeyy, String senderName, String senderImage, String senderUserID, String receiverName, String receiverImage, ArrayList<String> receiverUserIDD, String message, List<String> imageUrl, int readStatus, String sentTime, Long timeStamp, int state, String readBy) {
        this.isThisGroup = isThisGroup;
        this.groupID = groupID;
        this.groupName = groupName;
        this.groupImage = groupImage;
        this.receiverFcmKeyy = receiverFcmKeyy;
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.senderUserID = senderUserID;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserIDD = receiverUserIDD;
        this.message = message;
        this.imageUrl = imageUrl;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.timeStamp = timeStamp;
        this.state = state;
        this.readBy = readBy;
    }

    public MessageList(String senderName, String senderImage, String receiverName, String receiverImage, String message, List<String> imageUrl, int readStatus, String sentTime, Long timeStamp, int state) {
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.message = message;
        this.imageUrl = imageUrl;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.timeStamp = timeStamp;
        this.state = state;
    }

    public MessageList(boolean isThisGroup, String groupID,String groupName,String groupImage, String receiverName, String receiverImage,String receiverUserID,ArrayList<String> receiverUserIDD, ArrayList<String> receiverFcmKeyy, String message, int readStatus, String sentTime, int unreadCount, Long timeStamp) {
        this.isThisGroup = isThisGroup;
        this.groupID = groupID;
        this.groupName = groupName;
        this.groupImage = groupImage;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserID = receiverUserID;
        this.receiverUserIDD = receiverUserIDD;
        this.receiverFcmKeyy = receiverFcmKeyy;
        this.message = message;
        this.readStatus = readStatus;
        this.sentTime = sentTime;
        this.unreadCount = unreadCount;
        this.timeStamp = timeStamp;
    }

    public MessageList(String receiverName, String receiverImage, String receiverUserID, String receiverFcmKey, String receiverRole, String receiverClass) {
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.receiverUserID = receiverUserID;
        this.receiverFcmKey = receiverFcmKey;
        this.receiverRole = receiverRole;
        this.receiverClass = receiverClass;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getSenderUserID() {
        return senderUserID;
    }

    public void setSenderUserID(String senderUserID) {
        this.senderUserID = senderUserID;
    }

    public String getSenderFcmKey() {
        return senderFcmKey;
    }

    public void setSenderFcmKey(String senderFcmKey) {
        this.senderFcmKey = senderFcmKey;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverImage() {
        return receiverImage;
    }

    public void setReceiverImage(String receiverImage) {
        this.receiverImage = receiverImage;
    }

    public String getReceiverUserID() {
        return receiverUserID;
    }

    public void setReceiverUserID(String receiverUserID) {
        this.receiverUserID = receiverUserID;
    }

    public String getReceiverFcmKey() {
        return receiverFcmKey;
    }

    public void setReceiverFcmKey(String receiverFcmKey) {
        this.receiverFcmKey = receiverFcmKey;
    }

    public ArrayList<String> getReceiverUserIDD() {
        return receiverUserIDD;
    }

    public void setReceiverUserIDD(ArrayList<String> receiverUserIDD) {
        this.receiverUserIDD = receiverUserIDD;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getReceiverRole() {
        return receiverRole;
    }

    public void setReceiverRole(String receiverRole) {
        this.receiverRole = receiverRole;
    }

    public String getReceiverClass() {
        return receiverClass;
    }

    public void setReceiverClass(String receiverClass) {
        this.receiverClass = receiverClass;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public boolean isThisGroup() {
        return isThisGroup;
    }

    public void setThisGroup(boolean thisGroup) {
        isThisGroup = thisGroup;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupImage() {
        return groupImage;
    }

    public void setGroupImage(String groupImage) {
        this.groupImage = groupImage;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getReadBy() {
        return readBy;
    }

    public void setReadBy(String readBy) {
        this.readBy = readBy;
    }

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }
}
