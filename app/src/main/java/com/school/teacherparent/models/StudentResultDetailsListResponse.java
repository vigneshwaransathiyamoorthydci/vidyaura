package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentResultDetailsListResponse {
    String message;
    String statusText;
    int status;
    ArrayList<studentResultsDetailsList> studentResultsDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<StudentResultDetailsListResponse.studentResultsDetailsList> getStudentResultsDetailsList() {
        return studentResultsDetailsList;
    }

    public void setStudentResultsDetailsList(ArrayList<StudentResultDetailsListResponse.studentResultsDetailsList> studentResultsDetailsList) {
        this.studentResultsDetailsList = studentResultsDetailsList;
    }

    public  class studentResultsDetailsList
    {
        int Examterm_id;
        String Exam_title;
        int Class_id;
        int Section_id;
        int Student_id;
        String Fname;
        String Lname;
        String Student_photo;
        ArrayList<studentMarks> studentMarks;

        public int getExamterm_id() {
            return Examterm_id;
        }

        public void setExamterm_id(int examterm_id) {
            Examterm_id = examterm_id;
        }

        public String getExam_title() {
            return Exam_title;
        }

        public void setExam_title(String exam_title) {
            Exam_title = exam_title;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getStudent_id() {
            return Student_id;
        }

        public void setStudent_id(int student_id) {
            Student_id = student_id;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public ArrayList<StudentResultDetailsListResponse.studentResultsDetailsList.studentMarks> getStudentMarks() {
            return studentMarks;
        }

        public void setStudentMarks(ArrayList<StudentResultDetailsListResponse.studentResultsDetailsList.studentMarks> studentMarks) {
            this.studentMarks = studentMarks;
        }

        public class studentMarks
        {
            int Subject_id;
            String Name;
            int Marks;
            String Exam_title;

            public int getSubject_id() {
                return Subject_id;
            }

            public void setSubject_id(int subject_id) {
                Subject_id = subject_id;
            }

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                Name = name;
            }

            public int getMarks() {
                return Marks;
            }

            public void setMarks(int marks) {
                Marks = marks;
            }

            public String getExam_title() {
                return Exam_title;
            }

            public void setExam_title(String exam_title) {
                Exam_title = exam_title;
            }
        }
    }


}

