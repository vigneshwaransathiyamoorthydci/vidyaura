package com.school.teacherparent.models;

public class ClassGroupList {

    String classname;
    private int isGroup;
    private int classID;
    private int sectionID;
    private int groupID;

    public ClassGroupList(String classname, int isGroup, int classID, int sectionID, int groupID) {
        this.classname = classname;
        this.isGroup = isGroup;
        this.classID = classID;
        this.sectionID = sectionID;
        this.groupID = groupID;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }
}
