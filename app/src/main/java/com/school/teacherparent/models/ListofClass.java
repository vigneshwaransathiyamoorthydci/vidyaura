package com.school.teacherparent.models;

public class ListofClass
{
    private int classid;
    private String classname;
    private String classsection;
    private int classsectionid;
    public boolean selectedclass;
    public int selectedID;

    public int getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }

    public boolean isSelectedclass() {
        return selectedclass;
    }

    public void setSelectedclass(boolean selectedclass) {
        this.selectedclass = selectedclass;
    }

    public ListofClass(int classid, String classname, String classsection, int classsectionid, Boolean selectedclass,int selectedID) {
        this.classid = classid;
        this.classname = classname;
        this.classsection = classsection;
        this.classsectionid = classsectionid;
        this.selectedclass=selectedclass;
        this.selectedID=selectedID;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getClasssection() {
        return classsection;
    }

    public void setClasssection(String classsection) {
        this.classsection = classsection;
    }

    public int getClasssectionid() {
        return classsectionid;
    }

    public void setClasssectionid(int classsectionid) {
        this.classsectionid = classsectionid;
    }
}
