package com.school.teacherparent.models;

public class GetResultDetailsParams
{
    private String userID;
    private String userType;
    private String schoolID;
    private String classID;
    private String sectionID;
    private String examTermID;
    private String subjectID;

    public String getExamTermID() {
        return examTermID;
    }

    public void setExamTermID(String examTermID) {
        this.examTermID = examTermID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }
}
