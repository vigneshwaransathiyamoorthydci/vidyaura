package com.school.teacherparent.models;

public class CommentList {

    private int Id;
    private int Feed_id;
    private String Comment;
    private String Fname;
    private String Lname;
    private String Emp_photo;
    private String Created_at;
    private String Title;
    private String Description;
    private int isFeedPostedBy;
    private int feedLikeCount;
    private int feedCommentCount;
    private int feedShareCount;
    private int isUserLiked;
    private int isUserCommented;
    private String Publish_date;
    private String School_name;
    private int feedReplyCommentCount;
    private String feedCommentClapsCount;
    private String Type;

    public String getFeedCommentClapsCount() {
        return feedCommentClapsCount;
    }

    public void setFeedCommentClapsCount(String feedCommentClapsCount) {
        this.feedCommentClapsCount = feedCommentClapsCount;
    }

    public int getFeedReplyCommentCount() {
        return feedReplyCommentCount;
    }

    public void setFeedReplyCommentCount(int feedReplyCommentCount) {
        this.feedReplyCommentCount = feedReplyCommentCount;
    }

    public String isUserLikedComment;

    public String getIsUserLikedComment() {
        return isUserLikedComment;
    }

    public void setIsUserLikedComment(String isUserLikedComment) {
        this.isUserLikedComment = isUserLikedComment;
    }
    public  int Comment_ID;

    public int getComment_ID() {
        return Comment_ID;
    }

    public void setComment_ID(int comment_ID) {
        Comment_ID = comment_ID;
    }

    public CommentList(int id, int feed_id, String comment, String fname, String lname, String emp_photo, String created_at, String title, String description, int isFeedPostedBy, int feedLikeCount, int feedCommentCount, int feedShareCount, int isUserLiked, int isUserCommented, String publish_date, String school_name, int feedReplyCommentCount, String feedCommentClapsCount, String isUserLikedComment,int Comment_ID) {
        this.isUserLikedComment=isUserLikedComment;
        this.Comment_ID=Comment_ID;
        Id = id;
        Feed_id = feed_id;
        Comment = comment;
        Fname = fname;
        Lname = lname;
        Emp_photo = emp_photo;
        Created_at = created_at;
        Title = title;
        Description = description;
        this.isFeedPostedBy = isFeedPostedBy;
        this.feedLikeCount = feedLikeCount;
        this.feedCommentCount = feedCommentCount;
        this.feedShareCount = feedShareCount;
        this.isUserLiked = isUserLiked;
        this.isUserCommented = isUserCommented;
        Publish_date = publish_date;
        School_name = school_name;
        this.feedReplyCommentCount=feedReplyCommentCount;
        this.feedCommentClapsCount=feedCommentClapsCount;
    }

    public CommentList(int id, int feed_id, String comment, String fname, String lname, String emp_photo, String created_at, String title, String description, int isFeedPostedBy, int feedLikeCount, int feedCommentCount, int feedShareCount, int isUserLiked, int isUserCommented, String publish_date, String school_name, int feedReplyCommentCount, String feedCommentClapsCount, String isUserLikedComment,int Comment_ID, String type) {
        this.isUserLikedComment=isUserLikedComment;
        this.Comment_ID=Comment_ID;
        Id = id;
        Feed_id = feed_id;
        Comment = comment;
        Fname = fname;
        Lname = lname;
        Emp_photo = emp_photo;
        Created_at = created_at;
        Title = title;
        Description = description;
        this.isFeedPostedBy = isFeedPostedBy;
        this.feedLikeCount = feedLikeCount;
        this.feedCommentCount = feedCommentCount;
        this.feedShareCount = feedShareCount;
        this.isUserLiked = isUserLiked;
        this.isUserCommented = isUserCommented;
        Publish_date = publish_date;
        School_name = school_name;
        this.feedReplyCommentCount=feedReplyCommentCount;
        this.feedCommentClapsCount=feedCommentClapsCount;
        this.Type=type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getFeed_id() {
        return Feed_id;
    }

    public void setFeed_id(int feed_id) {
        Feed_id = feed_id;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getEmp_photo() {
        return Emp_photo;
    }

    public void setEmp_photo(String emp_photo) {
        Emp_photo = emp_photo;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getIsFeedPostedBy() {
        return isFeedPostedBy;
    }

    public void setIsFeedPostedBy(int isFeedPostedBy) {
        this.isFeedPostedBy = isFeedPostedBy;
    }

    public int getFeedLikeCount() {
        return feedLikeCount;
    }

    public void setFeedLikeCount(int feedLikeCount) {
        this.feedLikeCount = feedLikeCount;
    }

    public int getFeedCommentCount() {
        return feedCommentCount;
    }

    public void setFeedCommentCount(int feedCommentCount) {
        this.feedCommentCount = feedCommentCount;
    }

    public int getFeedShareCount() {
        return feedShareCount;
    }

    public void setFeedShareCount(int feedShareCount) {
        this.feedShareCount = feedShareCount;
    }

    public int getIsUserLiked() {
        return isUserLiked;
    }

    public void setIsUserLiked(int isUserLiked) {
        this.isUserLiked = isUserLiked;
    }

    public int getIsUserCommented() {
        return isUserCommented;
    }

    public void setIsUserCommented(int isUserCommented) {
        this.isUserCommented = isUserCommented;
    }

    public String getPublish_date() {
        return Publish_date;
    }

    public void setPublish_date(String publish_date) {
        Publish_date = publish_date;
    }

    public String getSchool_name() {
        return School_name;
    }

    public void setSchool_name(String school_name) {
        School_name = school_name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
