package com.school.teacherparent.models;

import java.util.ArrayList;

public class UpcomingExamdetailList
{
    private int Id;
    private String Exam_duration;
    private String Start_time;
    private String End_time;
    private int Subject_id;
    private int Classroom_id;
    private int Classsection_id;
    private int Term_id;
    private int Class_id;
    private String Name;
    private int isUserPostedExam;
    ArrayList<UpcomingExamDetailsResponse.topicName> topicName;
    ArrayList<UpcomingExamDetailsResponse.chapterName> chapterName;
    private int ExamSchedule_id;
    String classImage;

    public String getClassImage() {
        return classImage;
    }

    public void setClassImage(String classImage) {
        this.classImage = classImage;
    }

    public int getExamSchedule_id() {
        return ExamSchedule_id;
    }

    public void setExamSchedule_id(int examSchedule_id) {
        ExamSchedule_id = examSchedule_id;
    }

    public UpcomingExamdetailList(int id, String exam_duration, String start_time, String end_time, int subject_id, int classroom_id, int classsection_id, int term_id, int class_id, String name, int isUserPostedExam, ArrayList<UpcomingExamDetailsResponse.topicName> topicName, ArrayList<UpcomingExamDetailsResponse.chapterName> chapterName, int ExamSchedule_id, String classImage) {
        Id = id;
        Exam_duration = exam_duration;
        Start_time = start_time;
        End_time = end_time;
        Subject_id = subject_id;
        Classroom_id = classroom_id;
        Classsection_id = classsection_id;
        Term_id = term_id;
        Class_id = class_id;
        Name = name;
        this.isUserPostedExam = isUserPostedExam;
        this.topicName = topicName;
        this.chapterName = chapterName;
        this.ExamSchedule_id=ExamSchedule_id;
        this.classImage=classImage;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getExam_duration() {
        return Exam_duration;
    }

    public void setExam_duration(String exam_duration) {
        Exam_duration = exam_duration;
    }

    public String getStart_time() {
        return Start_time;
    }

    public void setStart_time(String start_time) {
        Start_time = start_time;
    }

    public String getEnd_time() {
        return End_time;
    }

    public void setEnd_time(String end_time) {
        End_time = end_time;
    }

    public int getSubject_id() {
        return Subject_id;
    }

    public void setSubject_id(int subject_id) {
        Subject_id = subject_id;
    }

    public int getClassroom_id() {
        return Classroom_id;
    }

    public void setClassroom_id(int classroom_id) {
        Classroom_id = classroom_id;
    }

    public int getClasssection_id() {
        return Classsection_id;
    }

    public void setClasssection_id(int classsection_id) {
        Classsection_id = classsection_id;
    }

    public int getTerm_id() {
        return Term_id;
    }

    public void setTerm_id(int term_id) {
        Term_id = term_id;
    }

    public int getClass_id() {
        return Class_id;
    }

    public void setClass_id(int class_id) {
        Class_id = class_id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getIsUserPostedExam() {
        return isUserPostedExam;
    }

    public void setIsUserPostedExam(int isUserPostedExam) {
        this.isUserPostedExam = isUserPostedExam;
    }

    public ArrayList<UpcomingExamDetailsResponse.topicName> getTopicName() {
        return topicName;
    }

    public void setTopicName(ArrayList<UpcomingExamDetailsResponse.topicName> topicName) {
        this.topicName = topicName;
    }

    public ArrayList<UpcomingExamDetailsResponse.chapterName> getChapterName() {
        return chapterName;
    }

    public void setChapterName(ArrayList<UpcomingExamDetailsResponse.chapterName> chapterName) {
        this.chapterName = chapterName;
    }
}
