package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class CircularDetailsResponse
{
    private String message;
    private String statusText;
    private int status;
    public ArrayList<circularsResult> circularsResult;

    public ArrayList<CircularDetailsResponse.circularsResult> getCircularsResult() {
        return circularsResult;
    }

    public void setCircularsResult(ArrayList<CircularDetailsResponse.circularsResult> circularsResult) {
        this.circularsResult = circularsResult;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public class  circularsResult
    {
        private int Id;
        private String Circular_no;
        private String Title;
        private String Description;
        private String Classroom_id;
        private String Author_type;
        private int Created_by;
        private String Send_to;
        private String Publish_date;
        private String Document;
        private String Visibility;
        List<String> attachmentsList;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getCircular_no() {
            return Circular_no;
        }

        public void setCircular_no(String circular_no) {
            Circular_no = circular_no;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(String classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getAuthor_type() {
            return Author_type;
        }

        public void setAuthor_type(String author_type) {
            Author_type = author_type;
        }

        public int getCreated_by() {
            return Created_by;
        }

        public void setCreated_by(int created_by) {
            Created_by = created_by;
        }

        public String getSend_to() {
            return Send_to;
        }

        public void setSend_to(String send_to) {
            Send_to = send_to;
        }

        public String getPublish_date() {
            return Publish_date;
        }

        public void setPublish_date(String publish_date) {
            Publish_date = publish_date;
        }

        public String getDocument() {
            return Document;
        }

        public void setDocument(String document) {
            Document = document;
        }

        public String getVisibility() {
            return Visibility;
        }

        public void setVisibility(String visibility) {
            Visibility = visibility;
        }
    }

}
