package com.school.teacherparent.models;

public class DeleteClassworkParms
{
    private String schoolID;
    private String userID;
    private String classworkID;
    private String userType;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getClassworkID() {
        return classworkID;
    }

    public void setClassworkID(String classworkID) {
        this.classworkID = classworkID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
