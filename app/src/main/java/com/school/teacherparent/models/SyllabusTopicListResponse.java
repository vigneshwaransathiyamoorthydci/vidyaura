package com.school.teacherparent.models;

import java.util.ArrayList;

public class SyllabusTopicListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<syllabusTopicsList> syllabusTopicsList;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<syllabusTopicsList> getSyllabusTopicsList() {
        return syllabusTopicsList;
    }

    public void setSyllabusTopicsList(ArrayList<syllabusTopicsList> syllabusTopicsList) {
        this.syllabusTopicsList = syllabusTopicsList;
    }

    public class syllabusTopicsList
    {
        private int topicID;
        private String Topic_name;

        public int getTopicID() {
            return topicID;
        }

        public void setTopicID(int topicID) {
            this.topicID = topicID;
        }

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }
    }
}
