package com.school.teacherparent.models;

import java.util.ArrayList;

public class AttendancedetailsResponse
{

    String message;
    String statusText;
    int status;
    ArrayList<attendanceDetailsList> attendanceDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AttendancedetailsResponse.attendanceDetailsList> getAttendanceDetailsList() {
        return attendanceDetailsList;
    }

    public void setAttendanceDetailsList(ArrayList<AttendancedetailsResponse.attendanceDetailsList> attendanceDetailsList) {
        this.attendanceDetailsList = attendanceDetailsList;
    }

    public class attendanceDetailsList
    {
            int classID;
            int studID;
            int Guardian_Id;
            int Father_id;
            int Mother_id;
            String Fname;
            String Lname;
            String ClassName;
            int sectionID;
            String Section;
            String attendancePercentage;
            ArrayList<studentsList> studentsList;
            String totalStudentsCount;
            String studentsPresentCount;
            String studentsAbsentCount;
            String iconName;
            String Class_image;
        String Student_photo;
        String Group_name;

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getTotalStudentsCount() {
            return totalStudentsCount;
        }

        public void setTotalStudentsCount(String totalStudentsCount) {
            this.totalStudentsCount = totalStudentsCount;
        }

        public String getStudentsPresentCount() {
            return studentsPresentCount;
        }

        public void setStudentsPresentCount(String studentsPresentCount) {
            this.studentsPresentCount = studentsPresentCount;
        }

        public String getStudentsAbsentCount() {
            return studentsAbsentCount;
        }

        public void setStudentsAbsentCount(String studentsAbsentCount) {
            this.studentsAbsentCount = studentsAbsentCount;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public int getGuardian_Id() {
            return Guardian_Id;
        }

        public void setGuardian_Id(int guardian_Id) {
            Guardian_Id = guardian_Id;
        }

        public int getFather_id() {
            return Father_id;
        }

        public void setFather_id(int father_id) {
            Father_id = father_id;
        }

        public int getMother_id() {
            return Mother_id;
        }

        public void setMother_id(int mother_id) {
            Mother_id = mother_id;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getAttendancePercentage() {
            return attendancePercentage;
        }

        public void setAttendancePercentage(String attendancePercentage) {
            this.attendancePercentage = attendancePercentage;
        }

        public ArrayList<AttendancedetailsResponse.studentsList> getStudentsList() {
            return studentsList;
        }

        public void setStudentsList(ArrayList<AttendancedetailsResponse.studentsList> studentsList) {
            this.studentsList = studentsList;
        }

        public String getGroup_name() {
            return Group_name;
        }

        public void setGroup_name(String group_name) {
            Group_name = group_name;
        }
    }

    public class studentsList
    {
       int Forenoon_session;
       int Afternoon_session;
       String Attendance_type;
       int teacherIncharge;
       int Is_halfday;
       String Leave_session;

        public int getForenoon_session() {
            return Forenoon_session;
        }

        public void setForenoon_session(int forenoon_session) {
            Forenoon_session = forenoon_session;
        }

        public int getAfternoon_session() {
            return Afternoon_session;
        }

        public void setAfternoon_session(int afternoon_session) {
            Afternoon_session = afternoon_session;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }

        public int getTeacherIncharge() {
            return teacherIncharge;
        }

        public void setTeacherIncharge(int teacherIncharge) {
            this.teacherIncharge = teacherIncharge;
        }

        public int getIs_halfday() {
            return Is_halfday;
        }

        public void setIs_halfday(int is_halfday) {
            Is_halfday = is_halfday;
        }

        public String getLeave_session() {
            return Leave_session;
        }

        public void setLeave_session(String leave_session) {
            Leave_session = leave_session;
        }
    }
}
