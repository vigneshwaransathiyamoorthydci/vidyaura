package com.school.teacherparent.models;

import java.util.ArrayList;

public class GroupChatResponse {

    String message;
    String statusText;
    int status;
    private ArrayList<groupList> groupList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<groupList> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<groupList> groupList) {
        this.groupList = groupList;
    }

    public static class groupList {
        int dataPosition;
        String GroupName;
        String GroupImage;
        String GroupID;
        ArrayList<UserData> TeacherID;
        ArrayList<UserData> TeacherName;
        ArrayList<UserData> TeacherImage;
        ArrayList<UserData> TeacherFcmKey;
        ArrayList<UserData> TeacherRole;
        ArrayList<UserData> TeacherClass;
        ArrayList<UserData> UserList;

        public groupList(int dataPosition, String groupName, String groupImage, String groupID, ArrayList<UserData> teacherID, ArrayList<UserData> teacherName, ArrayList<UserData> teacherImage, ArrayList<UserData> teacherFcmKey, ArrayList<UserData> teacherRole, ArrayList<UserData> teacherClass, ArrayList<UserData> userList) {
            this.dataPosition = dataPosition;
            GroupName = groupName;
            GroupImage = groupImage;
            GroupID = groupID;
            TeacherID = teacherID;
            TeacherName = teacherName;
            TeacherImage = teacherImage;
            TeacherFcmKey = teacherFcmKey;
            TeacherRole = teacherRole;
            TeacherClass = teacherClass;
            UserList = userList;
        }

        public int getDataPosition() {
            return dataPosition;
        }

        public void setDataPosition(int dataPosition) {
            this.dataPosition = dataPosition;
        }

        public String getGroupName() {
            return GroupName;
        }

        public void setGroupName(String groupName) {
            GroupName = groupName;
        }

        public String getGroupImage() {
            return GroupImage;
        }

        public void setGroupImage(String groupImage) {
            GroupImage = groupImage;
        }

        public String getGroupID() {
            return GroupID;
        }

        public void setGroupID(String groupID) {
            GroupID = groupID;
        }

        public ArrayList<UserData> getTeacherID() {
            return TeacherID;
        }

        public void setTeacherID(ArrayList<UserData> teacherID) {
            TeacherID = teacherID;
        }

        public ArrayList<UserData> getTeacherName() {
            return TeacherName;
        }

        public void setTeacherName(ArrayList<UserData> teacherName) {
            TeacherName = teacherName;
        }

        public ArrayList<UserData> getTeacherImage() {
            return TeacherImage;
        }

        public void setTeacherImage(ArrayList<UserData> teacherImage) {
            TeacherImage = teacherImage;
        }

        public ArrayList<UserData> getTeacherFcmKey() {
            return TeacherFcmKey;
        }

        public void setTeacherFcmKey(ArrayList<UserData> teacherFcmKey) {
            TeacherFcmKey = teacherFcmKey;
        }

        public ArrayList<UserData> getTeacherRole() {
            return TeacherRole;
        }

        public void setTeacherRole(ArrayList<UserData> teacherRole) {
            TeacherRole = teacherRole;
        }

        public ArrayList<UserData> getTeacherClass() {
            return TeacherClass;
        }

        public void setTeacherClass(ArrayList<UserData> teacherClass) {
            TeacherClass = teacherClass;
        }

        public ArrayList<UserData> getUserList() {
            return UserList;
        }

        public void setUserList(ArrayList<UserData> userList) {
            UserList = userList;
        }
    }

}
