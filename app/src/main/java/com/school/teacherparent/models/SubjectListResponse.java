package com.school.teacherparent.models;

import java.util.ArrayList;

public class SubjectListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<teachersSubjectsList> teachersSubjectsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<SubjectListResponse.teachersSubjectsList> getTeachersSubjectsList() {
        return teachersSubjectsList;
    }

    public void setTeachersSubjectsList(ArrayList<SubjectListResponse.teachersSubjectsList> teachersSubjectsList) {
        this.teachersSubjectsList = teachersSubjectsList;
    }

    public  class teachersSubjectsList
    {
        private int Id;
        private String Name;
        private int Subject_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }
    }
}
