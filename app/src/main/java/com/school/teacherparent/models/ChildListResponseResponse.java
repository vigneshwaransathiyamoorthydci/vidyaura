package com.school.teacherparent.models;

import java.util.ArrayList;

public class ChildListResponseResponse
{
    private String message;
    private String statusText;
    private int status;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ArrayList<parentsStudList> parentsStudList;

    public ArrayList<parentsStudList> getParentsStudList() {
        return parentsStudList;
    }

    public void setParentsStudList(ArrayList<parentsStudList> parentsStudList) {
        this.parentsStudList = parentsStudList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }




    public  class parentsStudList
    {
        int studRank;


        public int getStudRank() {
            return studRank;
        }

        public void setStudRank(int studRank) {
            this.studRank = studRank;
        }

        int Class_id;
      int Section_id;
      int Father_id;
      int Mother_id;
      int Guardian_Id;
      String Fname;
      String Lname;
      int studID;
      String ClassName;
      String Section;
        String Student_photo;
        String Dob;
        String Admission_date;
        String Admission_no;
        int School_id;

        public String getDob() {
            return Dob;
        }

        public void setDob(String dob) {
            Dob = dob;
        }

        public String getAdmission_date() {
            return Admission_date;
        }

        public void setAdmission_date(String admission_date) {
            Admission_date = admission_date;
        }

        public String getAdmission_no() {
            return Admission_no;
        }

        public void setAdmission_no(String admission_no) {
            Admission_no = admission_no;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getFather_id() {
            return Father_id;
        }

        public void setFather_id(int father_id) {
            Father_id = father_id;
        }

        public int getMother_id() {
            return Mother_id;
        }

        public void setMother_id(int mother_id) {
            Mother_id = mother_id;
        }

        public int getGuardian_Id() {
            return Guardian_Id;
        }

        public void setGuardian_Id(int guardian_Id) {
            Guardian_Id = guardian_Id;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }
    }
}
