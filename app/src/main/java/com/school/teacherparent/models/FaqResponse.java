package com.school.teacherparent.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FaqResponse
{
    String message;
    String statusText;
    int status;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ArrayList<getFAQDetails> getFAQDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<FaqResponse.getFAQDetails> getGetFAQDetails() {
        return getFAQDetails;
    }

    public void setGetFAQDetails(ArrayList<FaqResponse.getFAQDetails> getFAQDetails) {
        this.getFAQDetails = getFAQDetails;
    }

    public class  getFAQDetails
    {
        int Id;
        String Type;
        String Question;
        String Answer;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getQuestion() {
            return Question;
        }

        public void setQuestion(String question) {
            Question = question;
        }

        public String getAnswer() {
            return Answer;
        }

        public void setAnswer(String answer) {
            Answer = answer;
        }
    }

}
