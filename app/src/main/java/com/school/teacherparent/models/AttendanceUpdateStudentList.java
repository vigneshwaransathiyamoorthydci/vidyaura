package com.school.teacherparent.models;

import java.util.ArrayList;

public class AttendanceUpdateStudentList
{
    int studID;
    String Fname;
    String Lname;
    String Student_photo;
    int selectedID;
    int afternoonPresent;
    int forenoonPresent;

    public int getAfternoonPresent() {
        return afternoonPresent;
    }

    public void setAfternoonPresent(int afternoonPresent) {
        this.afternoonPresent = afternoonPresent;
    }
    ArrayList<AttendancedetailsResponse.studentsList> studentsList;

    public ArrayList<AttendancedetailsResponse.studentsList> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(ArrayList<AttendancedetailsResponse.studentsList> studentsList) {
        this.studentsList = studentsList;
    }

    public class studentsList
    {
        int Forenoon_session;
        int Afternoon_session;
        String Attendance_type;

        public int getForenoon_session() {
            return Forenoon_session;
        }

        public void setForenoon_session(int forenoon_session) {
            Forenoon_session = forenoon_session;
        }

        public int getAfternoon_session() {
            return Afternoon_session;
        }

        public void setAfternoon_session(int afternoon_session) {
            Afternoon_session = afternoon_session;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }
    }

    public int getForenoonPresent() {
        return forenoonPresent;
    }

    public void setForenoonPresent(int forenoonPresent) {
        this.forenoonPresent = forenoonPresent;
    }

    public AttendanceUpdateStudentList(int studID, String fname, String lname, String student_photo, int selectedID, int afternoonPresent, int forenoonPresent, ArrayList<AttendancedetailsResponse.studentsList> studentsList) {
        this.studID = studID;
        Fname = fname;
        Lname = lname;
        Student_photo = student_photo;
        this.selectedID = selectedID;
        this.afternoonPresent = afternoonPresent;
        this.forenoonPresent = forenoonPresent;
        this.studentsList = studentsList;
    }

    public int getStudID() {
        return studID;
    }

    public void setStudID(int studID) {
        this.studID = studID;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getStudent_photo() {
        return Student_photo;
    }

    public void setStudent_photo(String student_photo) {
        Student_photo = student_photo;
    }

    public int getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }
}
