package com.school.teacherparent.models;

import java.util.ArrayList;

public class ChapterListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<chaptersList> chaptersList;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ChapterListResponse.chaptersList> getChaptersList() {
        return chaptersList;
    }

    public void setChaptersList(ArrayList<ChapterListResponse.chaptersList> chaptersList) {
        this.chaptersList = chaptersList;
    }

    public class chaptersList
    {
        private int Id;

        private String Chapter_name;
        private String Section;
        private int Class_id;
        private int Subject_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }
    }
}
