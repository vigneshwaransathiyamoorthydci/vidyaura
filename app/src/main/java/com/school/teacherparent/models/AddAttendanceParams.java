package com.school.teacherparent.models;

public class AddAttendanceParams
{
    private String userID;
    private String userType;
    private String classID;
    private String sectionID;
    private String schoolID;
    private String studentID;
    private String attendanceDate;
    private String sessionType;
    private String groupID;
    private String ODStudentID;
    private String LeaveStudentID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getODStudentID() {
        return ODStudentID;
    }

    public void setODStudentID(String ODStudentID) {
        this.ODStudentID = ODStudentID;
    }

    public String getLeaveStudentID() {
        return LeaveStudentID;
    }

    public void setLeaveStudentID(String leaveStudentID) {
        LeaveStudentID = leaveStudentID;
    }
}
