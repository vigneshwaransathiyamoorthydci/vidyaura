package com.school.teacherparent.models;

import java.util.ArrayList;

public class ClassGroupListResponse
{
    private String $userID;
    private String message;
    private String statusText;
    private int status;
    private String token;
    ArrayList<teachersClassesList> teachersClassesList;
    ArrayList<teachersGroupList> teachersGroupList;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ClassGroupListResponse.teachersClassesList> getTeachersClassesList() {
        return teachersClassesList;
    }

    public void setTeachersClassesList(ArrayList<ClassGroupListResponse.teachersClassesList> teachersClassesList) {
        this.teachersClassesList = teachersClassesList;
    }

    public String get$userID() {
        return $userID;
    }

    public void set$userID(String $userID) {
        this.$userID = $userID;
    }

    public ArrayList<teachersGroupList> getTeachersGroupList() {
        return teachersGroupList;
    }

    public void setTeachersGroupList(ArrayList<teachersGroupList> teachersGroupList) {
        this.teachersGroupList = teachersGroupList;
    }

    public class teachersClassesList
    {
        private String ClassName;
        private String Section;
        private int classID;
        private int sectionID;
        private int Isgroup;

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public int getIsgroup() {
            return Isgroup;
        }

        public void setIsgroup(int isgroup) {
            Isgroup = isgroup;
        }
    }

    public class teachersGroupList {
        int Id;
        String Group_name;
        String Groupicon;
        int Isgroup;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getGroup_name() {
            return Group_name;
        }

        public void setGroup_name(String group_name) {
            Group_name = group_name;
        }

        public String getGroupicon() {
            return Groupicon;
        }

        public void setGroupicon(String groupicon) {
            Groupicon = groupicon;
        }

        public int getIsgroup() {
            return Isgroup;
        }

        public void setIsgroup(int isgroup) {
            Isgroup = isgroup;
        }
    }
}
