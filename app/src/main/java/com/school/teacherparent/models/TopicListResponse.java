package com.school.teacherparent.models;

import java.util.ArrayList;

public class TopicListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<topicsList> topicsList;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<TopicListResponse.topicsList> getTopicsList() {
        return topicsList;
    }

    public void setTopicsList(ArrayList<TopicListResponse.topicsList> topicsList) {
        this.topicsList = topicsList;
    }

    public class topicsList
    {
        private int Id;
        private String Topic_name;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }
    }
}
