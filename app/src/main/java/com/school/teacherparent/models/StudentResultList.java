package com.school.teacherparent.models;

public class StudentResultList
{
    int studID;
    String Fname;
    String Lname;
    String Student_photo;
    int mark;

    public StudentResultList(int studID, String fname, String lname, String student_photo, int mark) {
        this.studID = studID;
        Fname = fname;
        Lname = lname;
        Student_photo = student_photo;
        this.mark = mark;
    }

    public int getStudID() {
        return studID;
    }

    public void setStudID(int studID) {
        this.studID = studID;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getStudent_photo() {
        return Student_photo;
    }

    public void setStudent_photo(String student_photo) {
        Student_photo = student_photo;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
