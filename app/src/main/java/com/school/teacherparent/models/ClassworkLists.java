package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class ClassworkLists {

    public String message;
    public String statusText;
    public Integer status;
    public List<ClassworkList> classworkList = new ArrayList<ClassworkList>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ClassworkList> getClassworkList() {
        return classworkList;
    }

    public void setClassworkList(List<ClassworkList> classworkList) {
        this.classworkList = classworkList;
    }

    public class ClassworkList {

        public Integer classID;

        public String ClassName;
        public int classworkID;
        public Integer sectionID;
        public String Section;
        public Integer subjectID;
        public String subjectName;
        public String classworkCompletedPercentage;
        public List<TodaysClassworkProgress> todaysClassworkProgress = new ArrayList<TodaysClassworkProgress>();
        private String Class_image;

        public int getClassworkID() {
            return classworkID;
        }

        public void setClassworkID(int classworkID) {
            this.classworkID = classworkID;
        }

        public Integer getClassID() {
            return classID;
        }

        public void setClassID(Integer classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public Integer getSectionID() {
            return sectionID;
        }

        public void setSectionID(Integer sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getClassworkCompletedPercentage() {
            return classworkCompletedPercentage;
        }

        public void setClassworkCompletedPercentage(String classworkCompletedPercentage) {
            this.classworkCompletedPercentage = classworkCompletedPercentage;
        }

        public List<TodaysClassworkProgress> getTodaysClassworkProgress() {
            return todaysClassworkProgress;
        }

        public void setTodaysClassworkProgress(List<TodaysClassworkProgress> todaysClassworkProgress) {
            this.todaysClassworkProgress = todaysClassworkProgress;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }

    public class TodaysClassworkProgress {

        public String Topic_id;
        public String Chapter_id;
        public String topicCompletedDate;
        public List<ChapterName> chapterName = new ArrayList<ChapterName>();
        public String topicDetails;

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopicCompletedDate() {
            return topicCompletedDate;
        }

        public void setTopicCompletedDate(String topicCompletedDate) {
            this.topicCompletedDate = topicCompletedDate;
        }

        public List<ChapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(List<ChapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }
    }
    public class ChapterName {

        public String Chapter_name;

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }
    }
}
