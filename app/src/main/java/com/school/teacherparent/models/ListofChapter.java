package com.school.teacherparent.models;

public class ListofChapter
{
    private int Id;
    private int Subject_id;
    private String Chapter_name;
    private String classsection;
    private int classsectionid;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getSubject_id() {
        return Subject_id;
    }

    public void setSubject_id(int subject_id) {
        Subject_id = subject_id;
    }

    public String getChapter_name() {
        return Chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        Chapter_name = chapter_name;
    }

    public String getClasssection() {
        return classsection;
    }

    public void setClasssection(String classsection) {
        this.classsection = classsection;
    }

    public int getClasssectionid() {
        return classsectionid;
    }

    public void setClasssectionid(int classsectionid) {
        this.classsectionid = classsectionid;
    }

    public ListofChapter(int id, int subject_id, String chapter_name) {
        Id = id;
        Subject_id = subject_id;
        Chapter_name = chapter_name;

    }
}
