package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class AssignmentDetailsResponse
{
    String message;
    String statusText;
    int status;
    ArrayList<assignmentDetailsList> assignmentDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AssignmentDetailsResponse.assignmentDetailsList> getAssignmentDetailsList() {
        return assignmentDetailsList;
    }

    public void setAssignmentDetailsList(ArrayList<AssignmentDetailsResponse.assignmentDetailsList> assignmentDetailsList) {
        this.assignmentDetailsList = assignmentDetailsList;
    }

    public class  assignmentDetailsList
    {
        int assignmentID;
        String Description;
        String documentName;
        String Topic_id;
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        int subjectID;
        String subjectName;
        int isUserPostedAssignment;
        String topicDetails;
        String DueDate;
        List<String> attachmentsList;
        private String Class_image;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

        ArrayList<chapterName> chapterName;

        public ArrayList<AssignmentDetailsResponse.chapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(ArrayList<AssignmentDetailsResponse.chapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public int getAssignmentID() {
            return assignmentID;
        }

        public void setAssignmentID(int assignmentID) {
            this.assignmentID = assignmentID;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public int getIsUserPostedAssignment() {
            return isUserPostedAssignment;
        }

        public void setIsUserPostedAssignment(int isUserPostedAssignment) {
            this.isUserPostedAssignment = isUserPostedAssignment;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }

    public class chapterName
    {
        String Chapter_name;

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }
    }



}
