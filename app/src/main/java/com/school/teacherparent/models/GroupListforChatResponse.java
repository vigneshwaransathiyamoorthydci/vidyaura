package com.school.teacherparent.models;

import java.util.ArrayList;

public class GroupListforChatResponse {

    String message;
    String statusText;
    int status;
    private ArrayList<chatGroupList> chatGroupList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<GroupListforChatResponse.chatGroupList> getChatGroupList() {
        return chatGroupList;
    }

    public void setChatGroupList(ArrayList<GroupListforChatResponse.chatGroupList> chatGroupList) {
        this.chatGroupList = chatGroupList;
    }

    public static class chatGroupList {
        String groupID;
        String Group_name;
        String Description;
        String Student_id;
        String Teacher_id;
        String Groupicon;
        String encryptedGroupID;
        ArrayList<studentDetails> studentDetails;
        ArrayList<staffDetails> staffDetails;

        public String getGroupID() {
            return groupID;
        }

        public void setGroupID(String groupID) {
            this.groupID = groupID;
        }

        public String getGroup_name() {
            return Group_name;
        }

        public void setGroup_name(String group_name) {
            Group_name = group_name;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getStudent_id() {
            return Student_id;
        }

        public void setStudent_id(String student_id) {
            Student_id = student_id;
        }

        public String getTeacher_id() {
            return Teacher_id;
        }

        public void setTeacher_id(String teacher_id) {
            Teacher_id = teacher_id;
        }

        public String getGroupicon() {
            return Groupicon;
        }

        public void setGroupicon(String groupicon) {
            Groupicon = groupicon;
        }

        public String getEncryptedGroupID() {
            return encryptedGroupID;
        }

        public void setEncryptedGroupID(String encryptedGroupID) {
            this.encryptedGroupID = encryptedGroupID;
        }

        public ArrayList<studentDetails> getStudentDetails() {
            return studentDetails;
        }

        public void setStudentDetails(ArrayList<studentDetails> studentDetails) {
            this.studentDetails = studentDetails;
        }

        public ArrayList<staffDetails> getStaffDetails() {
            return staffDetails;
        }

        public void setStaffDetails(ArrayList<staffDetails> staffDetails) {
            this.staffDetails = staffDetails;
        }

        public class studentDetails {
            int studID;
            String Fname;
            String Lname;
            String Student_photo;
            int Father_id;
            int Mother_id;
            int Guardian_Id;
            String fatherEncryptedID;
            String motherEncryptedID;
            String guardianEncryptedID;
            fatherDetails fatherDetails;
            motherDetails motherDetails;
            guardianDetails guardianDetails;

            public int getStudID() {
                return studID;
            }

            public void setStudID(int studID) {
                this.studID = studID;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }

            public String getStudent_photo() {
                return Student_photo;
            }

            public void setStudent_photo(String student_photo) {
                Student_photo = student_photo;
            }

            public int getFather_id() {
                return Father_id;
            }

            public void setFather_id(int father_id) {
                Father_id = father_id;
            }

            public int getMother_id() {
                return Mother_id;
            }

            public void setMother_id(int mother_id) {
                Mother_id = mother_id;
            }

            public int getGuardian_Id() {
                return Guardian_Id;
            }

            public void setGuardian_Id(int guardian_Id) {
                Guardian_Id = guardian_Id;
            }

            public String getFatherEncryptedID() {
                return fatherEncryptedID;
            }

            public void setFatherEncryptedID(String fatherEncryptedID) {
                this.fatherEncryptedID = fatherEncryptedID;
            }

            public String getMotherEncryptedID() {
                return motherEncryptedID;
            }

            public void setMotherEncryptedID(String motherEncryptedID) {
                this.motherEncryptedID = motherEncryptedID;
            }

            public String getGuardianEncryptedID() {
                return guardianEncryptedID;
            }

            public void setGuardianEncryptedID(String guardianEncryptedID) {
                this.guardianEncryptedID = guardianEncryptedID;
            }

            public GroupListforChatResponse.chatGroupList.studentDetails.fatherDetails getFatherDetails() {
                return fatherDetails;
            }

            public void setFatherDetails(GroupListforChatResponse.chatGroupList.studentDetails.fatherDetails fatherDetails) {
                this.fatherDetails = fatherDetails;
            }

            public GroupListforChatResponse.chatGroupList.studentDetails.motherDetails getMotherDetails() {
                return motherDetails;
            }

            public void setMotherDetails(GroupListforChatResponse.chatGroupList.studentDetails.motherDetails motherDetails) {
                this.motherDetails = motherDetails;
            }

            public GroupListforChatResponse.chatGroupList.studentDetails.guardianDetails getGuardianDetails() {
                return guardianDetails;
            }

            public void setGuardianDetails(GroupListforChatResponse.chatGroupList.studentDetails.guardianDetails guardianDetails) {
                this.guardianDetails = guardianDetails;
            }

            public class fatherDetails {

                String Fname;
                String Type;
                String Mobile;
                String Photo;
                String Fcm_key;
                String Device_name;

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getType() {
                    return Type;
                }

                public void setType(String type) {
                    Type = type;
                }

                public String getMobile() {
                    return Mobile;
                }

                public void setMobile(String mobile) {
                    Mobile = mobile;
                }

                public String getPhoto() {
                    return Photo;
                }

                public void setPhoto(String photo) {
                    Photo = photo;
                }

                public String getFcm_key() {
                    return Fcm_key;
                }

                public void setFcm_key(String fcm_key) {
                    Fcm_key = fcm_key;
                }

                public String getDevice_name() {
                    return Device_name;
                }

                public void setDevice_name(String device_name) {
                    Device_name = device_name;
                }

            }

            public class motherDetails {

                String Fname;
                String Type;
                String Mobile;
                String Photo;
                String Fcm_key;
                String Device_name;

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getType() {
                    return Type;
                }

                public void setType(String type) {
                    Type = type;
                }

                public String getMobile() {
                    return Mobile;
                }

                public void setMobile(String mobile) {
                    Mobile = mobile;
                }

                public String getPhoto() {
                    return Photo;
                }

                public void setPhoto(String photo) {
                    Photo = photo;
                }

                public String getFcm_key() {
                    return Fcm_key;
                }

                public void setFcm_key(String fcm_key) {
                    Fcm_key = fcm_key;
                }

                public String getDevice_name() {
                    return Device_name;
                }

                public void setDevice_name(String device_name) {
                    Device_name = device_name;
                }

            }

            public class guardianDetails {

                String Fname;
                String Type;
                String Mobile;
                String Photo;
                String Fcm_key;
                String Device_name;

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getType() {
                    return Type;
                }

                public void setType(String type) {
                    Type = type;
                }

                public String getMobile() {
                    return Mobile;
                }

                public void setMobile(String mobile) {
                    Mobile = mobile;
                }

                public String getPhoto() {
                    return Photo;
                }

                public void setPhoto(String photo) {
                    Photo = photo;
                }

                public String getFcm_key() {
                    return Fcm_key;
                }

                public void setFcm_key(String fcm_key) {
                    Fcm_key = fcm_key;
                }

                public String getDevice_name() {
                    return Device_name;
                }

                public void setDevice_name(String device_name) {
                    Device_name = device_name;
                }

            }
        }

        public class staffDetails {
            int Id;
            String Fname;
            String Lname;
            String Emp_photo;
            String Fcm_key;
            String teacherEncryptedID;

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }

            public String getEmp_photo() {
                return Emp_photo;
            }

            public void setEmp_photo(String emp_photo) {
                Emp_photo = emp_photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }

            public String getTeacherEncryptedID() {
                return teacherEncryptedID;
            }

            public void setTeacherEncryptedID(String teacherEncryptedID) {
                this.teacherEncryptedID = teacherEncryptedID;
            }
        }
    }

}
