package com.school.teacherparent.models;

public class DeleteCircularParms
{
    private String circularsID;
    private String userID;
    private String schoolID;
    private String userType;

    public String getCircularsID() {
        return circularsID;
    }

    public void setCircularsID(String circularsID) {
        this.circularsID = circularsID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
