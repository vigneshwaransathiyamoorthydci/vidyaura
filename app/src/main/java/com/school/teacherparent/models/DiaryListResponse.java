package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class DiaryListResponse
{
    public String message;
    public String statusText;
    public Integer status;
    ArrayList<diaryDetailsList> diaryDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<DiaryListResponse.diaryDetailsList> getDiaryDetailsList() {
        return diaryDetailsList;
    }

    public void setDiaryDetailsList(ArrayList<DiaryListResponse.diaryDetailsList> diaryDetailsList) {
        this.diaryDetailsList = diaryDetailsList;
    }

    public class  todaysHomeworkProgress {
        String Topic_id;
        String Chapter_id;
        String Completed_on;
        int classworkID;
        int homeworkID;
        int todaysActivity;
        String Assigned_on;
        String Chapter_name;
        String topicDetails;

        public int getHomeworkID() {
            return homeworkID;
        }

        public void setHomeworkID(int homeworkID) {
            this.homeworkID = homeworkID;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getCompleted_on() {
            return Completed_on;
        }

        public void setCompleted_on(String completed_on) {
            Completed_on = completed_on;
        }

        public int getClassworkID() {
            return classworkID;
        }

        public void setClassworkID(int classworkID) {
            this.classworkID = classworkID;
        }

        public int getTodaysActivity() {
            return todaysActivity;
        }

        public void setTodaysActivity(int todaysActivity) {
            this.todaysActivity = todaysActivity;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getAssigned_on() {
            return Assigned_on;
        }

        public void setAssigned_on(String assigned_on) {
            Assigned_on = assigned_on;
        }
    }


    public class  todaysClassworkProgress
    {
        String Topic_id;
        String Chapter_id;
        String Completed_on;
        int classworkID;
        int todaysActivity;
        ArrayList<chapterName> chapterName;

        public ArrayList<todaysClassworkProgress.chapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(ArrayList<todaysClassworkProgress.chapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getCompleted_on() {
            return Completed_on;
        }

        public void setCompleted_on(String completed_on) {
            Completed_on = completed_on;
        }

        public int getClassworkID() {
            return classworkID;
        }

        public void setClassworkID(int classworkID) {
            this.classworkID = classworkID;
        }

        public int getTodaysActivity() {
            return todaysActivity;
        }

        public void setTodaysActivity(int todaysActivity) {
            this.todaysActivity = todaysActivity;
        }

        public class chapterName
        {
            String Chapter_name;

            public String getChapter_name() {
                return Chapter_name;
            }

            public void setChapter_name(String chapter_name) {
                Chapter_name = chapter_name;
            }
        }

       String topicDetails;

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }
    }



    public class diaryDetailsList {

        public Integer Class_id;
        public Integer Section_id;
        public Integer subjectID;
        public String ClassName;
        public String Section;
        public String Name;
        public Integer School_id;
        public String Period;
        public String Alt_name;
        String Class_image;

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public ArrayList<periodTiming> periodTiming;

        public ArrayList<DiaryListResponse.periodTiming> getPeriodTiming() {
            return periodTiming;
        }
        ArrayList<todaysClassworkProgress> todaysClassworkProgress;
            ArrayList<todaysHomeworkProgress> todaysHomeworkProgress;

        public ArrayList<DiaryListResponse.todaysHomeworkProgress> getTodaysHomeworkProgress() {
            return todaysHomeworkProgress;
        }

        public void setTodaysHomeworkProgress(ArrayList<DiaryListResponse.todaysHomeworkProgress> todaysHomeworkProgress) {
            this.todaysHomeworkProgress = todaysHomeworkProgress;
        }

        public ArrayList<DiaryListResponse.todaysClassworkProgress> getTodaysClassworkProgress() {
            return todaysClassworkProgress;
        }

        public void setTodaysClassworkProgress(ArrayList<DiaryListResponse.todaysClassworkProgress> todaysClassworkProgress) {
            this.todaysClassworkProgress = todaysClassworkProgress;
        }

        public void setPeriodTiming(ArrayList<DiaryListResponse.periodTiming> periodTiming) {
            this.periodTiming = periodTiming;
        }

        public Integer getClass_id() {
            return Class_id;
        }

        public void setClass_id(Integer class_id) {
            Class_id = class_id;
        }

        public String getAlt_name() {
            return Alt_name;
        }

        public void setAlt_name(String alt_name) {
            Alt_name = alt_name;
        }

        public Integer getSection_id() {
            return Section_id;
        }

        public void setSection_id(Integer section_id) {
            Section_id = section_id;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public Integer getSchool_id() {
            return School_id;
        }

        public void setSchool_id(Integer school_id) {
            School_id = school_id;
        }

        public String getPeriod() {
            return Period;
        }

        public void setPeriod(String period) {
            this.Period = period;
        }

    }

    public class periodTiming
    {
        String Start_time;
        String End_time;
        String Period;

        public String getStart_time() {
            return Start_time;
        }

        public void setStart_time(String start_time) {
            Start_time = start_time;
        }

        public String getEnd_time() {
            return End_time;
        }

        public void setEnd_time(String end_time) {
            End_time = end_time;
        }

        public String getPeriod() {
            return Period;
        }

        public void setPeriod(String period) {
            Period = period;
        }
    }

}
