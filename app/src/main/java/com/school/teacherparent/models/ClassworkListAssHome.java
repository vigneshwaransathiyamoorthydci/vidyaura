package com.school.teacherparent.models;

import java.util.List;

public class ClassworkListAssHome
{
    String chapterName;
    String AssignedOn;
    String DueDate;
    String Topic;
    int marks;

    public List<ClassworkList> classworkList = null;

    public ClassworkListAssHome(String chapterName, String assignedOn, String dueDate, List<ClassworkList> classworkList) {
        this.chapterName = chapterName;
        AssignedOn = assignedOn;
        DueDate = dueDate;
        this.classworkList = classworkList;
    }

    public ClassworkListAssHome(String chapterName, String assignedOn, String dueDate, String topic, int marks) {
        this.chapterName = chapterName;
        AssignedOn = assignedOn;
        DueDate = dueDate;
        Topic = topic;
        this.marks = marks;
    }

    public List<ClassworkList> getClassworkList() {
        return classworkList;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getAssignedOn() {
        return AssignedOn;
    }

    public void setAssignedOn(String assignedOn) {
        AssignedOn = assignedOn;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public void setClassworkList(List<ClassworkList> classworkList) {
        this.classworkList = classworkList;
    }
}
