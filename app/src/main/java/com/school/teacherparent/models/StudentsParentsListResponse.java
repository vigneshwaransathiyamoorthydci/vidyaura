package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentsParentsListResponse {

    String message;
    String statusText;
    int status;
    ArrayList<chatStudentsParentsList> chatStudentsParentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<StudentsParentsListResponse.chatStudentsParentsList> getChatStudentsParentsList() {
        return chatStudentsParentsList;
    }

    public void setChatStudentsParentsList(ArrayList<StudentsParentsListResponse.chatStudentsParentsList> chatStudentsParentsList) {
        this.chatStudentsParentsList = chatStudentsParentsList;
    }

    public static class chatStudentsParentsList {
        int studID;
        String Fname;
        String Lname;
        String Student_photo;
        int Father_id;
        int Mother_id;
        int Guardian_Id;
        String fatherEncryptedID;
        String motherEncryptedID;
        String guardianEncryptedID;
        ArrayList<fatherDetails> fatherDetails;
        ArrayList<motherDetails> motherDetails;
        ArrayList<guardianDetails> guardianDetails;

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getFather_id() {
            return Father_id;
        }

        public void setFather_id(int father_id) {
            Father_id = father_id;
        }

        public int getMother_id() {
            return Mother_id;
        }

        public void setMother_id(int mother_id) {
            Mother_id = mother_id;
        }

        public int getGuardian_Id() {
            return Guardian_Id;
        }

        public void setGuardian_Id(int guardian_Id) {
            Guardian_Id = guardian_Id;
        }

        public String getFatherEncryptedID() {
            return fatherEncryptedID;
        }

        public void setFatherEncryptedID(String fatherEncryptedID) {
            this.fatherEncryptedID = fatherEncryptedID;
        }

        public String getMotherEncryptedID() {
            return motherEncryptedID;
        }

        public void setMotherEncryptedID(String motherEncryptedID) {
            this.motherEncryptedID = motherEncryptedID;
        }

        public String getGuardianEncryptedID() {
            return guardianEncryptedID;
        }

        public void setGuardianEncryptedID(String guardianEncryptedID) {
            this.guardianEncryptedID = guardianEncryptedID;
        }

        public ArrayList<fatherDetails> getFatherDetails() {
            return fatherDetails;
        }

        public void setFatherDetails(ArrayList<fatherDetails> fatherDetails) {
            this.fatherDetails = fatherDetails;
        }

        public ArrayList<motherDetails> getMotherDetails() {
            return motherDetails;
        }

        public void setMotherDetails(ArrayList<motherDetails> motherDetails) {
            this.motherDetails = motherDetails;
        }

        public ArrayList<guardianDetails> getGuardianDetails() {
            return guardianDetails;
        }

        public void setGuardianDetails(ArrayList<guardianDetails> guardianDetails) {
            this.guardianDetails = guardianDetails;
        }


        public static class fatherDetails {

            String Fname;
            String Type;
            String Mobile;
            String Photo;
            String Fcm_key;
            String Device_name;
            String userID;

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }

            public String getMobile() {
                return Mobile;
            }

            public void setMobile(String mobile) {
                Mobile = mobile;
            }

            public String getPhoto() {
                return Photo;
            }

            public void setPhoto(String photo) {
                Photo = photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }

            public String getDevice_name() {
                return Device_name;
            }

            public void setDevice_name(String device_name) {
                Device_name = device_name;
            }

            public String getUserID() {
                return userID;
            }

            public void setUserID(String userID) {
                this.userID = userID;
            }
        }

        public static class motherDetails {

            String Fname;
            String Type;
            String Mobile;
            String Photo;
            String Fcm_key;
            String Device_name;
            String userID;

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }

            public String getMobile() {
                return Mobile;
            }

            public void setMobile(String mobile) {
                Mobile = mobile;
            }

            public String getPhoto() {
                return Photo;
            }

            public void setPhoto(String photo) {
                Photo = photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }

            public String getDevice_name() {
                return Device_name;
            }

            public void setDevice_name(String device_name) {
                Device_name = device_name;
            }

            public String getUserID() {
                return userID;
            }

            public void setUserID(String userID) {
                this.userID = userID;
            }
        }

        public static class guardianDetails {

            String Fname;
            String Type;
            String Mobile;
            String Photo;
            String Fcm_key;
            String Device_name;
            String userID;

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }

            public String getMobile() {
                return Mobile;
            }

            public void setMobile(String mobile) {
                Mobile = mobile;
            }

            public String getPhoto() {
                return Photo;
            }

            public void setPhoto(String photo) {
                Photo = photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }

            public String getDevice_name() {
                return Device_name;
            }

            public void setDevice_name(String device_name) {
                Device_name = device_name;
            }

            public String getUserID() {
                return userID;
            }

            public void setUserID(String userID) {
                this.userID = userID;
            }
        }

    }

}
