package com.school.teacherparent.models;

import java.util.List;

public class CircularList
{
    private int schoolID;
    private String schoolName;
    private String schoolLogo;
    private int Id;
    private String Circular_no;
    private String Title;
    private String Description;
    private String Classroom_id;
    private String Author_type;
    private int Created_by;
    private int isUserPostedCircular;
    private String created_at;
    List<String> attachmentsList;

    public CircularList(int schoolID, String schoolName, String schoolLogo, int id, String circular_no, String title, String description, String classroom_id, String author_type, int created_by, int isUserPostedCircular, String created_at,List<String> attachmentsList) {
        this.schoolID = schoolID;
        this.schoolName = schoolName;
        this.schoolLogo = schoolLogo;
        Id = id;
        Circular_no = circular_no;
        Title = title;
        Description = description;
        Classroom_id = classroom_id;
        Author_type = author_type;
        Created_by = created_by;
        this.isUserPostedCircular = isUserPostedCircular;
        this.created_at = created_at;
        this.attachmentsList=attachmentsList;
    }

    public List<String> getAttachmentsList() {
        return attachmentsList;
    }

    public void setAttachmentsList(List<String> attachmentsList) {
        this.attachmentsList = attachmentsList;
    }

    public int getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(int schoolID) {
        this.schoolID = schoolID;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getCircular_no() {
        return Circular_no;
    }

    public void setCircular_no(String circular_no) {
        Circular_no = circular_no;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getClassroom_id() {
        return Classroom_id;
    }

    public void setClassroom_id(String classroom_id) {
        Classroom_id = classroom_id;
    }

    public String getAuthor_type() {
        return Author_type;
    }

    public void setAuthor_type(String author_type) {
        Author_type = author_type;
    }

    public int getCreated_by() {
        return Created_by;
    }

    public void setCreated_by(int created_by) {
        Created_by = created_by;
    }

    public int getIsUserPostedCircular() {
        return isUserPostedCircular;
    }

    public void setIsUserPostedCircular(int isUserPostedCircular) {
        this.isUserPostedCircular = isUserPostedCircular;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
