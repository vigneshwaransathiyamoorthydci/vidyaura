package com.school.teacherparent.models;

public class AddResultParams
{

    private String classID;
    private String schoolID;
    private String userType;
    private String sectionID;
    private String userID;
    private String examTermID;
    String studMarksList;
    String subjectID;

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getExamTermID() {
        return examTermID;
    }

    public void setExamTermID(String examTermID) {
        this.examTermID = examTermID;
    }

    public String getStudMarksList() {
        return studMarksList;
    }

    public void setStudMarksList(String studMarksList) {
        this.studMarksList = studMarksList;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }
}
