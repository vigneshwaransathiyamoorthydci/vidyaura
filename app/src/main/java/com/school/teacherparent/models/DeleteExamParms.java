package com.school.teacherparent.models;

public class DeleteExamParms
{
    private String schoolID;
    private String userID;
    private int examScheduleID;
    private String userType;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getExamScheduleID() {
        return examScheduleID;
    }

    public void setExamScheduleID(int examScheduleID) {
        this.examScheduleID = examScheduleID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
