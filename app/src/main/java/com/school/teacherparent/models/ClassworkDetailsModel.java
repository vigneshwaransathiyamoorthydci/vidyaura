package com.school.teacherparent.models;

import java.util.List;

public class ClassworkDetailsModel {

    public String message;
    public String statusText;
    public Integer status;
    public List<ClassworkDetail> classworkDetailsList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ClassworkDetail> getClassworkDetails() {
        return classworkDetailsList;
    }

    public void setClassworkDetails(List<ClassworkDetail> classworkDetails) {
        this.classworkDetailsList = classworkDetails;
    }

    public class ClassworkDetail {

        public Integer classworkID;
        public Object documentName;
        public String Chapter_id;
        public String Topic_id;
        public Integer classID;
        public String ClassName;
        public Integer sectionID;
        public String Section;
        public Integer subjectID;
        public String subjectName;
        public int isUserPostedClasswork;
        private List<String> attachmentsList;
        private String Description;
        private String Class_image;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getIsUserPostedClasswork() {
            return isUserPostedClasswork;
        }

        public void setIsUserPostedClasswork(int isUserPostedClasswork) {
            this.isUserPostedClasswork = isUserPostedClasswork;
        }

        public List<ChapterName> chapterName = null;
        public String topicDetails;

        public Integer getClassworkID() {
            return classworkID;
        }

        public void setClassworkID(Integer classworkID) {
            this.classworkID = classworkID;
        }

        public Object getDocumentName() {
            return documentName;
        }

        public void setDocumentName(Object documentName) {
            this.documentName = documentName;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public Integer getClassID() {
            return classID;
        }

        public void setClassID(Integer classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public Integer getSectionID() {
            return sectionID;
        }

        public void setSectionID(Integer sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public List<ChapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(List<ChapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }
    public class ChapterName {

        public String Chapter_name;

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }
    }
}
