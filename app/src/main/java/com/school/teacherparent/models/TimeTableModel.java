package com.school.teacherparent.models;

import java.util.List;

public class TimeTableModel {

    public String message;
    public String statusText;
    public Integer status;
    public List<TimetableList> timetableList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<TimetableList> getTimetableList() {
        return timetableList;
    }

    public void setTimetableList(List<TimetableList> timetableList) {
        this.timetableList = timetableList;
    }

    public class TimetableList {

        public Integer Class_id;
        public Integer Section_id;
        public Integer subjectID;
        public String ClassName;
        public String Section;
        public String Name;
        public Integer School_id;
        public String Period;
        public List<BreakTiming> breakTiming = null;
        public String Alt_name;
        String Class_image;

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public Integer getClass_id() {
            return Class_id;
        }

        public void setClass_id(Integer class_id) {
            Class_id = class_id;
        }

        public String getAlt_name() {
            return Alt_name;
        }

        public void setAlt_name(String alt_name) {
            Alt_name = alt_name;
        }

        public Integer getSection_id() {
            return Section_id;
        }

        public void setSection_id(Integer section_id) {
            Section_id = section_id;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public Integer getSchool_id() {
            return School_id;
        }

        public void setSchool_id(Integer school_id) {
            School_id = school_id;
        }

        public String getPeriod() {
            return Period;
        }

        public void setPeriod(String period) {
            this.Period = period;
        }

        public List<BreakTiming> getBreakTiming() {
            return breakTiming;
        }

        public void setBreakTiming(List<BreakTiming> breakTiming) {
            this.breakTiming = breakTiming;
        }
        public class BreakTiming {

            public String Start_time;
            public String End_time;
            public String Period;

            public String getStart_time() {
                return Start_time;
            }

            public void setStart_time(String start_time) {
                Start_time = start_time;
            }

            public String getEnd_time() {
                return End_time;
            }

            public void setEnd_time(String end_time) {
                End_time = end_time;
            }

            public String getPeriod() {
                return Period;
            }

            public void setPeriod(String period) {
                Period = period;
            }
        }
    }


}
