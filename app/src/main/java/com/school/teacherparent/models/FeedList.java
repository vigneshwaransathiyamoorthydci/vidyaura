package com.school.teacherparent.models;

import java.util.List;

public class FeedList
{

    public FeedList(int id, String title, String description, int author_id,
                    String author_type, String visibility, String created_at, String salutation,
                    String fname, String lname, String emp_photo, int feed_id, int feedLikeCount,
                    int feedCommentCount, int feedShareCount, int isUserLiked, int isUserCommented,
                    String school_name, String school_code, String logo, String name, int isFeedPostedBy, String publish_date,List<String>attachmentsList, String schoolLogoo) {
        Id = id;
        Title = title;
        Description = description;
        Author_id = author_id;
        Author_type = author_type;
        Visibility = visibility;
        Created_at = created_at;
        Salutation = salutation;
        Fname = fname;
        Lname = lname;
        Emp_photo = emp_photo;
        Feed_id = feed_id;
        this.feedLikeCount = feedLikeCount;
        this.feedCommentCount = feedCommentCount;
        this.feedShareCount = feedShareCount;
        this.isUserLiked = isUserLiked;
        this.isUserCommented = isUserCommented;
        School_name = school_name;
        School_code = school_code;
        Logo = logo;
        this.name = name;
        this.isFeedPostedBy = isFeedPostedBy;
        this.attachmentsList=attachmentsList;
        Publish_date = publish_date;
        schoolLogo = schoolLogoo;
    }

    private int Id;
    private List<String> attachmentsList;
    private String Title;
    private String Description;
    private int Author_id;
    private String Author_type;
    private String Visibility;
    private String Created_at;
    private String Salutation;
    private String Fname;
    private String Lname;
    private String Emp_photo;
    private int Feed_id;
    private int feedLikeCount;
    private int feedCommentCount;
    private int feedShareCount;
    private int isUserLiked;
    private int isUserCommented;
    private String School_name;
    private String School_code;
    private String Logo;
    private String name;
    private int isFeedPostedBy;
    private String Publish_date;
    private String schoolLogo;

    public List<String> getAttachmentsList() {
        return attachmentsList;
    }

    public void setAttachmentsList(List<String> attachmentsList) {
        this.attachmentsList = attachmentsList;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getAuthor_id() {
        return Author_id;
    }

    public void setAuthor_id(int author_id) {
        Author_id = author_id;
    }

    public String getAuthor_type() {
        return Author_type;
    }

    public void setAuthor_type(String author_type) {
        Author_type = author_type;
    }

    public String getVisibility() {
        return Visibility;
    }

    public void setVisibility(String visibility) {
        Visibility = visibility;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(String salutation) {
        Salutation = salutation;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getEmp_photo() {
        return Emp_photo;
    }

    public void setEmp_photo(String emp_photo) {
        Emp_photo = emp_photo;
    }

    public int getFeed_id() {
        return Feed_id;
    }

    public void setFeed_id(int feed_id) {
        Feed_id = feed_id;
    }

    public int getFeedLikeCount() {
        return feedLikeCount;
    }

    public void setFeedLikeCount(int feedLikeCount) {
        this.feedLikeCount = feedLikeCount;
    }

    public int getFeedCommentCount() {
        return feedCommentCount;
    }

    public void setFeedCommentCount(int feedCommentCount) {
        this.feedCommentCount = feedCommentCount;
    }

    public int getFeedShareCount() {
        return feedShareCount;
    }

    public void setFeedShareCount(int feedShareCount) {
        this.feedShareCount = feedShareCount;
    }

    public int getIsUserLiked() {
        return isUserLiked;
    }

    public void setIsUserLiked(int isUserLiked) {
        this.isUserLiked = isUserLiked;
    }

    public int getIsUserCommented() {
        return isUserCommented;
    }

    public void setIsUserCommented(int isUserCommented) {
        this.isUserCommented = isUserCommented;
    }

    public String getSchool_name() {
        return School_name;
    }

    public void setSchool_name(String school_name) {
        School_name = school_name;
    }

    public String getSchool_code() {
        return School_code;
    }

    public void setSchool_code(String school_code) {
        School_code = school_code;
    }

    public String getLogo() {
        return Logo;
    }

    public void setLogo(String logo) {
        Logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsFeedPostedBy() {
        return isFeedPostedBy;
    }

    public void setIsFeedPostedBy(int isFeedPostedBy) {
        this.isFeedPostedBy = isFeedPostedBy;
    }

    public String getPublish_date() {
        return Publish_date;
    }

    public void setPublish_date(String publish_date) {
        Publish_date = publish_date;
    }
}
