package com.school.teacherparent.models;

public class UpdatecircularParam
{
        private String userID;
        private String title;
        private String circularDesc;
        private String classID;
        private String sendTo;
        private String documentName;
        private String schoolID;
        private String userType;
        private int circularsID;

    public int getCircularsID() {
        return circularsID;
    }

    public void setCircularsID(int circularsID) {
        this.circularsID = circularsID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCircularDesc() {
        return circularDesc;
    }

    public void setCircularDesc(String circularDesc) {
        this.circularDesc = circularDesc;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
