package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class HomeDetailsResponse
{
    String message;
    String statusText;
    int status;
    ArrayList<homeworkDetailsList> homeworkDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<HomeDetailsResponse.homeworkDetailsList> getHomeworkDetailsList() {
        return homeworkDetailsList;
    }

    public void setHomeworkDetailsList(ArrayList<HomeDetailsResponse.homeworkDetailsList> homeworkDetailsList) {
        this.homeworkDetailsList = homeworkDetailsList;
    }

    public class  homeworkDetailsList
    {
        int homeworkID;
        String documentName;
        String Topic_id;
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        int subjectID;
        String subjectName;
        int isUserPostedHomework;
        String topicDetails;
        String DueDate;
        List<String> attachmentsList;
        String Description;
        private String Class_image;

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

        ArrayList<chapterName> chapterName;

        public ArrayList<HomeDetailsResponse.chapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(ArrayList<HomeDetailsResponse.chapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public int getHomeworkID() {
            return homeworkID;
        }

        public void setHomeworkID(int homeworkID) {
            this.homeworkID = homeworkID;
        }

        public String getDocumentName() {
            return documentName;
        }

        public void setDocumentName(String documentName) {
            this.documentName = documentName;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public int getIsUserPostedHomework() {
            return isUserPostedHomework;
        }

        public void setIsUserPostedHomework(int isUserPostedHomework) {
            this.isUserPostedHomework = isUserPostedHomework;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }

    public class chapterName
    {
        String Chapter_name;

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }
    }



}
