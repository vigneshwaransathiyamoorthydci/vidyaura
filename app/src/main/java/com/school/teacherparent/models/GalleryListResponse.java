package com.school.teacherparent.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GalleryListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<galleryListResult> galleryListResult;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<galleryListResult> getGalleryListResult() {
        return galleryListResult;
    }

    public void setGalleryListResult(ArrayList<galleryListResult> galleryListResult) {
        this.galleryListResult = galleryListResult;
    }

    public class galleryListResult
    {
        int Id;
        String Title;
        String Description;
        ArrayList<albumList> albumList;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public ArrayList<GalleryListResponse.albumList> getAlbumList() {
            return albumList;
        }

        public void setAlbumList(ArrayList<GalleryListResponse.albumList> albumList) {
            this.albumList = albumList;
        }
    }

    public class albumList implements Serializable
    {
        private int Gallery_id;
        private String Media_path;
        private String Media_type;

        public int getGallery_id() {
            return Gallery_id;
        }

        public void setGallery_id(int gallery_id) {
            Gallery_id = gallery_id;
        }

        public String getMedia_path() {
            return Media_path;
        }

        public void setMedia_path(String media_path) {
            Media_path = media_path;
        }

        public String getMedia_type() {
            return Media_type;
        }

        public void setMedia_type(String media_type) {
            Media_type = media_type;
        }
    }


}
