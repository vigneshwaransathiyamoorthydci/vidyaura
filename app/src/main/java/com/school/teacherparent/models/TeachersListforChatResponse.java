package com.school.teacherparent.models;

import java.util.ArrayList;

public class TeachersListforChatResponse {

    String message;
    String statusText;
    int status;
    private ArrayList<chatTeachersList> chatTeachersList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<chatTeachersList> getChatTeachersList() {
        return chatTeachersList;
    }

    public void setChatTeachersList(ArrayList<chatTeachersList> chatTeachersList) {
        this.chatTeachersList = chatTeachersList;
    }

    public static class chatTeachersList {
        String teacherEncryptedID;
        String Fname;
        String Lname;
        String Emp_photo;
        String Fcm_key;
        String teacherID;
        String subjectName;
        String altName;
        String sectionName;
        String TeacherRole;
        String TeacherClass;


        public chatTeachersList(String teacherEncryptedID, String fname, String lname, String emp_photo, String fcm_key, String teacherID, String teacherRole, String teacherClass) {
            this.teacherEncryptedID = teacherEncryptedID;
            Fname = fname;
            Lname = lname;
            Emp_photo = emp_photo;
            Fcm_key = fcm_key;
            this.teacherID = teacherID;
            TeacherRole = teacherRole;
            TeacherClass = teacherClass;
        }

        public String getTeacherEncryptedID() {
            return teacherEncryptedID;
        }

        public void setTeacherEncryptedID(String teacherEncryptedID) {
            this.teacherEncryptedID = teacherEncryptedID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }

        public String getFcm_key() {
            return Fcm_key;
        }

        public void setFcm_key(String fcm_key) {
            Fcm_key = fcm_key;
        }

        public String getTeacherID() {
            return teacherID;
        }

        public void setTeacherID(String teacherID) {
            this.teacherID = teacherID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getAltName() {
            return altName;
        }

        public void setAltName(String altName) {
            this.altName = altName;
        }

        public String getSectionName() {
            return sectionName;
        }

        public void setSectionName(String sectionName) {
            this.sectionName = sectionName;
        }

        public String getTeacherRole() {
            return TeacherRole;
        }

        public void setTeacherRole(String teacherRole) {
            TeacherRole = teacherRole;
        }

        public String getTeacherClass() {
            return TeacherClass;
        }

        public void setTeacherClass(String teacherClass) {
            TeacherClass = teacherClass;
        }
    }

}
