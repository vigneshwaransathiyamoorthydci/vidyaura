package com.school.teacherparent.models;

import java.util.ArrayList;

public class DiaryHomeworkList
{
    String topicDetails;
   String chapterName;
   String Assigned_on;

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public DiaryHomeworkList(String topicDetails, String chapterName, String Assigned_on) {
        this.topicDetails = topicDetails;
        this.chapterName = chapterName;
        this.Assigned_on = Assigned_on;
    }

    public String getTopicDetails() {
        return topicDetails;
    }

    public void setTopicDetails(String topicDetails) {
        this.topicDetails = topicDetails;
    }

    public String getAssigned_on() {
        return Assigned_on;
    }

    public void setAssigned_on(String assigned_on) {
        Assigned_on = assigned_on;
    }
}
