package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class FeedCommentListResponse
{
    private String message;
    private String statusText;
    private int status;
    public ArrayList<data> feedDetailsList;

    public ArrayList<data> getFeedDetailsList() {
        return feedDetailsList;
    }

    public void setFeedDetailsList(ArrayList<data> feedDetailsList) {
        this.feedDetailsList = feedDetailsList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private feedCommentsList feedCommentsList;

    public FeedCommentListResponse.feedCommentsList getFeedCommentsList() {
        return feedCommentsList;
    }

    public void setFeedCommentsList(FeedCommentListResponse.feedCommentsList feedCommentsList) {
        this.feedCommentsList = feedCommentsList;
    }

    public  class feedPostedByUserDetails
    {
        private String Fname;
        private String Lname;
        private String Emp_photo;

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }
    }

    public  class feedCommentedByUserDetails
    {
        private String Fname;
        private String Lname;
        private String Emp_photo;

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }
    }

    public class    data {
        private int Id;
        private int Feed_id;
        private String Comment;
        private String Fname;
        private String Lname;
        private String Emp_photo;
        private String Created_at;
        private String Title;
        private String Description;
        private int isFeedPostedBy;
        private int feedLikeCount;
        private int feedCommentCount;
        private int feedShareCount;
        private int isUserLiked;
        private int isUserCommented;
        private String Publish_date;
        private String School_name;
        private String Author_type;
        private String name;
        private String Logo;
        private List<String> attachmentsList;

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }
        ArrayList<feedCommentedByUserDetails> feedCommentedByUserDetails;

        public ArrayList<FeedCommentListResponse.feedCommentedByUserDetails> getFeedCommentedByUserDetails() {
            return feedCommentedByUserDetails;
        }

        public void setFeedCommentedByUserDetails(ArrayList<FeedCommentListResponse.feedCommentedByUserDetails> feedCommentedByUserDetails) {
            this.feedCommentedByUserDetails = feedCommentedByUserDetails;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        feedPostedByUserDetails feedPostedByUserDetails;

        public FeedCommentListResponse.feedPostedByUserDetails getFeedPostedByUserDetails() {
            return feedPostedByUserDetails;
        }

        public void setFeedPostedByUserDetails(FeedCommentListResponse.feedPostedByUserDetails feedPostedByUserDetails) {
            this.feedPostedByUserDetails = feedPostedByUserDetails;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor_type() {
            return Author_type;
        }

        public void setAuthor_type(String author_type) {
            Author_type = author_type;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getFeed_id() {
            return Feed_id;
        }

        public void setFeed_id(int feed_id) {
            Feed_id = feed_id;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String comment) {
            Comment = comment;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }

        public String getCreated_at() {
            return Created_at;
        }

        public void setCreated_at(String created_at) {
            Created_at = created_at;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public int getIsFeedPostedBy() {
            return isFeedPostedBy;
        }

        public void setIsFeedPostedBy(int isFeedPostedBy) {
            this.isFeedPostedBy = isFeedPostedBy;
        }

        public int getFeedLikeCount() {
            return feedLikeCount;
        }

        public void setFeedLikeCount(int feedLikeCount) {
            this.feedLikeCount = feedLikeCount;
        }

        public int getFeedCommentCount() {
            return feedCommentCount;
        }

        public void setFeedCommentCount(int feedCommentCount) {
            this.feedCommentCount = feedCommentCount;
        }

        public int getFeedShareCount() {
            return feedShareCount;
        }

        public void setFeedShareCount(int feedShareCount) {
            this.feedShareCount = feedShareCount;
        }

        public int getIsUserLiked() {
            return isUserLiked;
        }

        public void setIsUserLiked(int isUserLiked) {
            this.isUserLiked = isUserLiked;
        }

        public int getIsUserCommented() {
            return isUserCommented;
        }

        public void setIsUserCommented(int isUserCommented) {
            this.isUserCommented = isUserCommented;
        }

        public String getPublish_date() {
            return Publish_date;
        }

        public void setPublish_date(String publish_date) {
            Publish_date = publish_date;
        }

        public String getSchool_name() {
            return School_name;
        }

        public void setSchool_name(String school_name) {
            School_name = school_name;
        }
    }

    public class feedCommentsList
    {
        private int current_page;

        public ArrayList<data> data;

        public ArrayList<FeedCommentListResponse.feedCommentsList.data> getData() {
            return data;
        }

        public void setData(ArrayList<FeedCommentListResponse.feedCommentsList.data> data) {
            this.data = data;
        }

        public class  data
        {
            public int getComment_ID() {
                return Comment_ID;
            }

            public void setComment_ID(int comment_ID) {
                Comment_ID = comment_ID;
            }

            private int Comment_ID;
            private int Id;
            private int Feed_id;
            private String Comment;
            private String Fname;
            private String Lname;
            private String Emp_photo;
            private String Created_at;
            private String Title;
            private String Description;
            private int isFeedPostedBy;
            private int feedLikeCount;
            private int feedCommentCount;
            private int feedShareCount;
            private int isUserLiked;
            private int isUserCommented;
            private String Publish_date;
            private String School_name;
            private int feedReplyCommentCount;
            private String feedCommentClapsCount;
            private String isUserLikedComment;
            private List<String> attachmentsList;
            feedPostedByUserDetails feedPostedByUserDetails;
            private String commentedDateTime;
            private String Comment_user_type;
            private String Logo;
            private String Author_type;
            ArrayList<feedCommentedByUserDetails> feedCommentedByUserDetails;

            public String getLogo() {
                return Logo;
            }

            public void setLogo(String logo) {
                Logo = logo;
            }

            public String getAuthor_type() {
                return Author_type;
            }

            public void setAuthor_type(String author_type) {
                Author_type = author_type;
            }

            public String getComment_user_type() {
                return Comment_user_type;
            }

            public void setComment_user_type(String comment_user_type) {
                Comment_user_type = comment_user_type;
            }

            public ArrayList<FeedCommentListResponse.feedCommentedByUserDetails> getFeedCommentedByUserDetails() {
                return feedCommentedByUserDetails;
            }

            public void setFeedCommentedByUserDetails(ArrayList<FeedCommentListResponse.feedCommentedByUserDetails> feedCommentedByUserDetails) {
                this.feedCommentedByUserDetails = feedCommentedByUserDetails;
            }

            public String getCommentedDateTime() {
                return commentedDateTime;
            }

            public void setCommentedDateTime(String commentedDateTime) {
                this.commentedDateTime = commentedDateTime;
            }

            public FeedCommentListResponse.feedPostedByUserDetails getFeedPostedByUserDetails() {
                return feedPostedByUserDetails;
            }

            public void setFeedPostedByUserDetails(FeedCommentListResponse.feedPostedByUserDetails feedPostedByUserDetails) {
                this.feedPostedByUserDetails = feedPostedByUserDetails;
            }

            public List<String> getAttachmentsList() {
                return attachmentsList;
            }

            public void setAttachmentsList(List<String> attachmentsList) {
                this.attachmentsList = attachmentsList;
            }

            public String getIsUserLikedComment() {
                return isUserLikedComment;
            }

            public void setIsUserLikedComment(String isUserLikedComment) {
                this.isUserLikedComment = isUserLikedComment;
            }

            public String getFeedCommentClapsCount() {
                return feedCommentClapsCount;
            }

            public void setFeedCommentClapsCount(String feedCommentClapsCount) {
                this.feedCommentClapsCount = feedCommentClapsCount;
            }

            public int getFeedReplyCommentCount() {
                return feedReplyCommentCount;
            }

            public void setFeedReplyCommentCount(int feedReplyCommentCount) {
                this.feedReplyCommentCount = feedReplyCommentCount;
            }

            public String getSchool_name() {
                return School_name;
            }

            public void setSchool_name(String school_name) {
                School_name = school_name;
            }

            public String getPublish_date() {
                return Publish_date;
            }

            public void setPublish_date(String publish_date) {
                Publish_date = publish_date;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String title) {
                Title = title;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String description) {
                Description = description;
            }

            public int getIsFeedPostedBy() {
                return isFeedPostedBy;
            }

            public void setIsFeedPostedBy(int isFeedPostedBy) {
                this.isFeedPostedBy = isFeedPostedBy;
            }

            public int getFeedLikeCount() {
                return feedLikeCount;
            }

            public void setFeedLikeCount(int feedLikeCount) {
                this.feedLikeCount = feedLikeCount;
            }

            public int getFeedCommentCount() {
                return feedCommentCount;
            }

            public void setFeedCommentCount(int feedCommentCount) {
                this.feedCommentCount = feedCommentCount;
            }

            public int getFeedShareCount() {
                return feedShareCount;
            }

            public void setFeedShareCount(int feedShareCount) {
                this.feedShareCount = feedShareCount;
            }

            public int getIsUserLiked() {
                return isUserLiked;
            }

            public void setIsUserLiked(int isUserLiked) {
                this.isUserLiked = isUserLiked;
            }

            public int getIsUserCommented() {
                return isUserCommented;
            }

            public void setIsUserCommented(int isUserCommented) {
                this.isUserCommented = isUserCommented;
            }

            public String getCreated_at() {
                return Created_at;
            }

            public void setCreated_at(String created_at) {
                Created_at = created_at;
            }

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public int getFeed_id() {
                return Feed_id;
            }

            public void setFeed_id(int feed_id) {
                Feed_id = feed_id;
            }

            public String getComment() {
                return Comment;
            }

            public void setComment(String comment) {
                Comment = comment;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }

            public String getEmp_photo() {
                return Emp_photo;
            }

            public void setEmp_photo(String emp_photo) {
                Emp_photo = emp_photo;
            }
        }
    }

}
