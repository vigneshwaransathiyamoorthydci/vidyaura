package com.school.teacherparent.models;

import java.util.ArrayList;

public class ExamTermResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<examTermsList> examTermsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ExamTermResponse.examTermsList> getExamTermsList() {
        return examTermsList;
    }

    public void setExamTermsList(ArrayList<ExamTermResponse.examTermsList> examTermsList) {
        this.examTermsList = examTermsList;
    }

    public  class examTermsList
    {
        private int Id;
        private String Exam_title;
        String Start_date;
        String End_date;

        public String getStart_date() {
            return Start_date;
        }

        public void setStart_date(String start_date) {
            Start_date = start_date;
        }

        public String getEnd_date() {
            return End_date;
        }

        public void setEnd_date(String end_date) {
            End_date = end_date;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getExam_title() {
            return Exam_title;
        }

        public void setExam_title(String exam_title) {
            Exam_title = exam_title;
        }
    }
}
