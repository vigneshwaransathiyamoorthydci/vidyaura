package com.school.teacherparent.models;

import java.util.ArrayList;

public class FeedCommentreplyListResponse
{
    private String message;
    private String statusText;
    private int status;
    public feedCommentsList feedCommentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public FeedCommentreplyListResponse.feedCommentsList getFeedCommentsList() {
        return feedCommentsList;
    }

    public void setFeedCommentsList(FeedCommentreplyListResponse.feedCommentsList feedCommentsList) {
        this.feedCommentsList = feedCommentsList;
    }

    public static class feedCommentsList
    {
        private int current_page;

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }
        public ArrayList<data> data;

        public ArrayList<FeedCommentreplyListResponse.feedCommentsList.data> getData() {
            return data;
        }

        public void setData(ArrayList<FeedCommentreplyListResponse.feedCommentsList.data> data) {
            this.data = data;
        }

        public class  data
        {

            public int getComment_id() {
                return Comment_id;
            }

            public void setComment_id(int comment_id) {
                Comment_id = comment_id;
            }

            public String getReplyComment() {
                return ReplyComment;
            }

            public void setReplyComment(String replyComment) {
                ReplyComment = replyComment;
            }

            private int Comment_id;
            private int Id;
            private int Feed_id;
            private String Created_at;
            private String ReplyComment;
            private String Reply_user_type;
            ArrayList<feedReplyedByUserDetails> feedReplyedByUserDetails;

            public String getReply_user_type() {
                return Reply_user_type;
            }

            public void setReply_user_type(String reply_user_type) {
                Reply_user_type = reply_user_type;
            }

            public ArrayList<FeedCommentreplyListResponse.feedCommentsList.data.feedReplyedByUserDetails> getFeedReplyedByUserDetails() {
                return feedReplyedByUserDetails;
            }

            public void setFeedReplyedByUserDetails(ArrayList<FeedCommentreplyListResponse.feedCommentsList.data.feedReplyedByUserDetails> feedReplyedByUserDetails) {
                this.feedReplyedByUserDetails = feedReplyedByUserDetails;
            }

            public class  feedReplyedByUserDetails
            {
                private String Fname;
                private String Lname;
                private String Emp_photo;
                private String Photo;

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getLname() {
                    return Lname;
                }

                public void setLname(String lname) {
                    Lname = lname;
                }

                public String getEmp_photo() {
                    return Emp_photo;
                }

                public void setEmp_photo(String emp_photo) {
                    Emp_photo = emp_photo;
                }

                public String getPhoto() {
                    return Photo;
                }

                public void setPhoto(String photo) {
                    Photo = photo;
                }
            }


            public String getCreated_at() {
                return Created_at;
            }

            public void setCreated_at(String created_at) {
                Created_at = created_at;
            }

            public int getId() {
                return Id;
            }

            public void setId(int id) {
                Id = id;
            }

            public int getFeed_id() {
                return Feed_id;
            }

            public void setFeed_id(int feed_id) {
                Feed_id = feed_id;
            }


        }
    }
}
