package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class UserLoginResponse
{

    public ArrayList<data> data;

    public ArrayList<appSettings> appSettings;

    public ArrayList<UserLoginResponse.appSettings> getAppSettings() {
        return appSettings;
    }

    public void setAppSettings(ArrayList<UserLoginResponse.appSettings> appSettings) {
        this.appSettings = appSettings;
    }

    public String message;
    public String statusText;
    public int status;
    public String encyptedUserID;
    public int hasChildren;
    private String schoolLogo;
    int totalLevelPoints;
        public  teacherLevel teacherLevel;

    public UserLoginResponse.teacherLevel getTeacherLevel() {
        return teacherLevel;
    }

    public void setTeacherLevel(UserLoginResponse.teacherLevel teacherLevel) {
        this.teacherLevel = teacherLevel;
    }

    public int getTotalLevelPoints() {
        return totalLevelPoints;
    }

    public void setTotalLevelPoints(int totalLevelPoints) {
        this.totalLevelPoints = totalLevelPoints;
    }

    public int getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(int hasChildren) {
        this.hasChildren = hasChildren;
    }

    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String securedToken;

    public String getSecuredToken() {
        return securedToken;
    }

    public void setSecuredToken(String securedToken) {
        this.securedToken = securedToken;
    }

    public String getEncyptedUserID() {
        return encyptedUserID;
    }

    public void setEncyptedUserID(String encyptedUserID) {
        this.encyptedUserID = encyptedUserID;
    }

    public ArrayList<UserLoginResponse.data> getData() {
        return data;
    }

    public void setData(ArrayList<UserLoginResponse.data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }

    public class appSettings
    {
        int NotificationStatus;
        int MessageStatus;
        int SecurityPinStatus;

        public int getNotificationStatus() {
            return NotificationStatus;
        }

        public void setNotificationStatus(int notificationStatus) {
            NotificationStatus = notificationStatus;
        }

        public int getMessageStatus() {
            return MessageStatus;
        }

        public void setMessageStatus(int messageStatus) {
            MessageStatus = messageStatus;
        }

        public int getSecurityPinStatus() {
            return SecurityPinStatus;
        }

        public void setSecurityPinStatus(int securityPinStatus) {
            SecurityPinStatus = securityPinStatus;
        }
    }

public class teacherLevel
{
    String Name;
    String Description;
    String Badge_image;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getBadge_image() {
        return Badge_image;
    }

    public void setBadge_image(String badge_image) {
        Badge_image = badge_image;
    }
}

    public class data
    {

        public int Id;
        public String Status;
        public String Fname;
        public String Lname;
        public String Emp_id;
        public String Phone;
        public String Email;
        public String Dob;
        public String Gender;
        public String Emp_photo;
        public String Salutation;
        public String SecurityPin;
        public String Class_handled;
        public String Joining_date;
        public int School_id;
        public String Type;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getEmp_id() {
            return Emp_id;
        }

        public void setEmp_id(String emp_id) {
            Emp_id = emp_id;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getDob() {
            return Dob;
        }

        public void setDob(String dob) {
            Dob = dob;
        }

        public String getGender() {
            return Gender;
        }

        public void setGender(String gender) {
            Gender = gender;
        }

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }

        public String getSalutation() {
            return Salutation;
        }

        public void setSalutation(String salutation) {
            Salutation = salutation;
        }

        public String getSecurityPin() {
            return SecurityPin;
        }

        public void setSecurityPin(String securityPin) {
            SecurityPin = securityPin;
        }

        public String getClass_handled() {
            return Class_handled;
        }

        public void setClass_handled(String class_handled) {
            Class_handled = class_handled;
        }

        public String getJoining_date() {
            return Joining_date;
        }

        public void setJoining_date(String joining_date) {
            Joining_date = joining_date;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }
    }

}
