package com.school.teacherparent.models;

public class ResultList
{
    int Class_id;
    int Section_id;
    String totalStudentsCount;
    String classAverage;
    int Total_marks;
    int Student_id;
    String Fname;
    String Lname;
    String Student_photo;
    String Alt_name;
    String ClassName;
    String Section;
    private String classImage;
    int sstudID;
    String sFname;
    String sLname;
    String sStudent_photo;

    public int getSstudID() {
        return sstudID;
    }

    public void setSstudID(int sstudID) {
        this.sstudID = sstudID;
    }

    public String getsFname() {
        return sFname;
    }

    public void setsFname(String sFname) {
        this.sFname = sFname;
    }

    public String getsLname() {
        return sLname;
    }

    public void setsLname(String sLname) {
        this.sLname = sLname;
    }

    public String getsStudent_photo() {
        return sStudent_photo;
    }

    public void setsStudent_photo(String sStudent_photo) {
        this.sStudent_photo = sStudent_photo;
    }

    public String getAlt_name() {
        return Alt_name;
    }

    public void setAlt_name(String alt_name) {
        Alt_name = alt_name;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public ResultList(int class_id, int section_id, String totalStudentsCount, String classAverage, int total_marks,
                      int student_id, String fname, String lname, String student_photo, String alt_name, String className,
                      String section, String classImage,int sstudID,String sFname,String sLname,String sStudent_photo) {
        Class_id = class_id;
        Section_id = section_id;
        this.totalStudentsCount = totalStudentsCount;
        this.classAverage = classAverage;
        Total_marks = total_marks;
        Student_id = student_id;
        Fname = fname;
        Lname = lname;
        Student_photo = student_photo;
        Alt_name = alt_name;
        ClassName = className;
        Section = section;
        this.classImage = classImage;
        this.sstudID=sstudID;
        this.sFname=sFname;
        this.sLname=sLname;
        this.sStudent_photo=sStudent_photo;
    }


    public int getClass_id() {
        return Class_id;
    }

    public void setClass_id(int class_id) {
        Class_id = class_id;
    }

    public int getSection_id() {
        return Section_id;
    }

    public void setSection_id(int section_id) {
        Section_id = section_id;
    }

    public String getTotalStudentsCount() {
        return totalStudentsCount;
    }

    public void setTotalStudentsCount(String totalStudentsCount) {
        this.totalStudentsCount = totalStudentsCount;
    }

    public String getClassAverage() {
        return classAverage;
    }

    public void setClassAverage(String classAverage) {
        this.classAverage = classAverage;
    }

    public int getTotal_marks() {
        return Total_marks;
    }

    public void setTotal_marks(int total_marks) {
        Total_marks = total_marks;
    }

    public int getStudent_id() {
        return Student_id;
    }

    public void setStudent_id(int student_id) {
        Student_id = student_id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getStudent_photo() {
        return Student_photo;
    }

    public void setStudent_photo(String student_photo) {
        Student_photo = student_photo;
    }

    public String getClassImage() {
        return classImage;
    }

    public void setClassImage(String classImage) {
        this.classImage = classImage;
    }
}
