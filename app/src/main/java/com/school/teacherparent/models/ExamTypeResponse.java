package com.school.teacherparent.models;

import java.util.ArrayList;

public class ExamTypeResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<examTypeList>  examTypeList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ExamTypeResponse.examTypeList> getExamTypeList() {
        return examTypeList;
    }

    public void setExamTypeList(ArrayList<ExamTypeResponse.examTypeList> examTypeList) {
        this.examTypeList = examTypeList;
    }

    public class examTypeList
    {
        int Id;
        String  Name;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }

}
