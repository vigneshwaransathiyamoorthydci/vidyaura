package com.school.teacherparent.models;

import java.util.ArrayList;

public class FeesCondition {

    int Term_id;
    int Total_amount;
    String term_name;
    int DiscountedAmount;
    ArrayList<FeesListResponse.feesList.feesDetails> feesDetails;


    public FeesCondition(int total_amount, int discountedAmount, String term_name, int term_id, ArrayList<FeesListResponse.feesList.feesDetails> feesDetails) {
        Term_id = term_id;
        Total_amount = total_amount;
        this.term_name = term_name;
        DiscountedAmount = discountedAmount;
        this.feesDetails = feesDetails;
    }

    public int getTerm_id() {
        return Term_id;
    }

    public void setTerm_id(int term_id) {
        Term_id = term_id;
    }

    public int getTotal_amount() {
        return Total_amount;
    }

    public void setTotal_amount(int total_amount) {
        Total_amount = total_amount;
    }

    public String getTerm_name() {
        return term_name;
    }

    public void setTerm_name(String term_name) {
        this.term_name = term_name;
    }

    public int getDiscountedAmount() {
        return DiscountedAmount;
    }

    public void setDiscountedAmount(int discountedAmount) {
        DiscountedAmount = discountedAmount;
    }

    public ArrayList<FeesListResponse.feesList.feesDetails> getFeesDetails() {
        return feesDetails;
    }

    public void setFeesDetails(ArrayList<FeesListResponse.feesList.feesDetails> feesDetails) {
        this.feesDetails = feesDetails;
    }

    public static class feesDetails {

        int feesPaidID;
        int Class_id;
        int Section_id;
        int Term_id;
        int Fees_type_id;
        int Total_amount;
        int Discount_amount;
        int Fees_status_id;
        String Paid_status;
        int Student_id;
        String Paid_date;
        int discountAmount;
        String feesTypeName;
        String Transaction_unique_id;
        String Payment_transaction_id;

        public feesDetails(int feesPaidID, int class_id, int section_id, int term_id, int fees_type_id, int total_amount, int discount_amount, int fees_status_id, String paid_status, int student_id, String paid_date, int discountAmount, String feesTypeName, String transaction_unique_id, String payment_transaction_id) {
            this.feesPaidID = feesPaidID;
            Class_id = class_id;
            Section_id = section_id;
            Term_id = term_id;
            Fees_type_id = fees_type_id;
            Total_amount = total_amount;
            Discount_amount = discount_amount;
            Fees_status_id = fees_status_id;
            Paid_status = paid_status;
            Student_id = student_id;
            Paid_date = paid_date;
            this.discountAmount = discountAmount;
            this.feesTypeName = feesTypeName;
            Transaction_unique_id = transaction_unique_id;
            Payment_transaction_id = payment_transaction_id;
        }

        public int getFeesPaidID() {
            return feesPaidID;
        }

        public void setFeesPaidID(int feesPaidID) {
            this.feesPaidID = feesPaidID;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getFees_type_id() {
            return Fees_type_id;
        }

        public void setFees_type_id(int fees_type_id) {
            Fees_type_id = fees_type_id;
        }

        public int getTotal_amount() {
            return Total_amount;
        }

        public void setTotal_amount(int total_amount) {
            Total_amount = total_amount;
        }

        public int getDiscount_amount() {
            return Discount_amount;
        }

        public void setDiscount_amount(int discount_amount) {
            Discount_amount = discount_amount;
        }

        public int getFees_status_id() {
            return Fees_status_id;
        }

        public void setFees_status_id(int fees_status_id) {
            Fees_status_id = fees_status_id;
        }

        public String getPaid_status() {
            return Paid_status;
        }

        public void setPaid_status(String paid_status) {
            Paid_status = paid_status;
        }

        public int getStudent_id() {
            return Student_id;
        }

        public void setStudent_id(int student_id) {
            Student_id = student_id;
        }

        public String getPaid_date() {
            return Paid_date;
        }

        public void setPaid_date(String paid_date) {
            Paid_date = paid_date;
        }

        public int getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(int discountAmount) {
            this.discountAmount = discountAmount;
        }

        public String getFeesTypeName() {
            return feesTypeName;
        }

        public void setFeesTypeName(String feesTypeName) {
            this.feesTypeName = feesTypeName;
        }

        public String getTransaction_unique_id() {
            return Transaction_unique_id;
        }

        public void setTransaction_unique_id(String transaction_unique_id) {
            Transaction_unique_id = transaction_unique_id;
        }

        public String getPayment_transaction_id() {
            return Payment_transaction_id;
        }

        public void setPayment_transaction_id(String payment_transaction_id) {
            Payment_transaction_id = payment_transaction_id;
        }
    }

}
