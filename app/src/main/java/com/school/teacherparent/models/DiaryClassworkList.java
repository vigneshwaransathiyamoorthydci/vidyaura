package com.school.teacherparent.models;

import java.util.ArrayList;

public class DiaryClassworkList
{
    String topicDetails;
    String Completed_on;
    ArrayList<DiaryListResponse.todaysClassworkProgress.chapterName> chapterName;

    public ArrayList<DiaryListResponse.todaysClassworkProgress.chapterName> getChapterName() {
        return chapterName;
    }

    public void setChapterName(ArrayList<DiaryListResponse.todaysClassworkProgress.chapterName> chapterName) {
        this.chapterName = chapterName;
    }

    public DiaryClassworkList(String topicDetails, ArrayList<DiaryListResponse.todaysClassworkProgress.chapterName> chapterName, String Completed_on) {
        this.topicDetails = topicDetails;
        this.chapterName = chapterName;
        this.Completed_on = Completed_on;
    }

    public String getCompleted_on() {
        return Completed_on;
    }

    public void setCompleted_on(String completed_on) {
        Completed_on = completed_on;
    }

    public String getTopicDetails() {
        return topicDetails;
    }

    public void setTopicDetails(String topicDetails) {
        this.topicDetails = topicDetails;
    }
}
