package com.school.teacherparent.models;

/**
 * Created by keerthana on 11/12/2018.
 */

public class AllGalleryDTO {
    private String img,video,program,counts;




    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }



}
