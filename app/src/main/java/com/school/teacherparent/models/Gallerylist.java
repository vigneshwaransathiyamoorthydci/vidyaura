package com.school.teacherparent.models;

import java.util.ArrayList;

public class Gallerylist
{
    int Id;
    String Title;
    String Description;
    ArrayList<GalleryListResponse.albumList> albumList;

    public Gallerylist(int id, String title, String description, ArrayList<GalleryListResponse.albumList> albumList) {
        Id = id;
        Title = title;
        Description = description;
        this.albumList = albumList;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public ArrayList<GalleryListResponse.albumList> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(ArrayList<GalleryListResponse.albumList> albumList) {
        this.albumList = albumList;
    }
}
