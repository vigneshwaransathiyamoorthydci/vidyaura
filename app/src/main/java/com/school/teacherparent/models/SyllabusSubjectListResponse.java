package com.school.teacherparent.models;

import java.util.ArrayList;

public class SyllabusSubjectListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<syllabusSubjectsList> syllabusSubjectsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<SyllabusSubjectListResponse.syllabusSubjectsList> getSyllabusSubjectsList() {
        return syllabusSubjectsList;
    }

    public void setSyllabusSubjectsList(ArrayList<SyllabusSubjectListResponse.syllabusSubjectsList> syllabusSubjectsList) {
        this.syllabusSubjectsList = syllabusSubjectsList;
    }

    public  class syllabusSubjectsList
    {
        private int Id;
        private String Name;
        private int subjectID;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }
    }
}
