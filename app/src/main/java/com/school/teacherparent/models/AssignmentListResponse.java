package com.school.teacherparent.models;

import java.util.ArrayList;

public class AssignmentListResponse {
    String message;
    String statusText;
    int status;
    ArrayList<assignmentsList> assignmentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AssignmentListResponse.assignmentsList> getAssignmentsList() {
        return assignmentsList;
    }

    public void setAssignmentsList(ArrayList<AssignmentListResponse.assignmentsList> assignmentsList) {
        this.assignmentsList = assignmentsList;
    }

    public  class assignmentsList
    {
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        int subjectID;
        String subjectName;
        private String Class_image;

        ArrayList<classwiseAssignments> classwiseAssignments;

        public ArrayList<AssignmentListResponse.classwiseAssignments> getClasswiseAssignments() {
            return classwiseAssignments;
        }

        public void setClasswiseAssignments(ArrayList<AssignmentListResponse.classwiseAssignments> classwiseAssignments) {
            this.classwiseAssignments = classwiseAssignments;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }
    public class classwiseAssignments
    {
        int isDueDateToday;
        int isUserPostedHomework;
        String DueDate;
        String Assigned_on;
        String subjectName;
        String ClassName;
        String Section;
        String Chapter_name;
        String topicDetails;
        int assignmentID;
        int totalAssignmentsCount;
        int Mark;

        public int getMark() {
            return Mark;
        }

        public void setMark(int mark) {
            Mark = mark;
        }

        public int getTotalAssignmentsCount() {
            return totalAssignmentsCount;
        }

        public void setTotalAssignmentsCount(int totalAssignmentsCount) {
            this.totalAssignmentsCount = totalAssignmentsCount;
        }

        public int getAssignmentID() {
            return assignmentID;
        }

        public void setAssignmentID(int assignmentID) {
            this.assignmentID = assignmentID;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

//        ArrayList<topicDetails> topicDetails;

        public int getIsDueDateToday() {
            return isDueDateToday;
        }

        public void setIsDueDateToday(int isDueDateToday) {
            this.isDueDateToday = isDueDateToday;
        }

        public int getIsUserPostedHomework() {
            return isUserPostedHomework;
        }

        public void setIsUserPostedHomework(int isUserPostedHomework) {
            this.isUserPostedHomework = isUserPostedHomework;
        }

        public String getAssigned_on() {
            return Assigned_on;
        }

        public void setAssigned_on(String assigned_on) {
            Assigned_on = assigned_on;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

//        public ArrayList<HomeworkListResponse.topicDetails> getTopicDetails() {
//            return topicDetails;
//        }
//
//        public void setTopicDetails(ArrayList<HomeworkListResponse.topicDetails> topicDetails) {
//            this.topicDetails = topicDetails;
//        }
    }


}

