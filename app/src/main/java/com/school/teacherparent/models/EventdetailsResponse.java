package com.school.teacherparent.models;

import java.util.ArrayList;

public class EventdetailsResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<eventDetails> eventDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<EventdetailsResponse.eventDetails> getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(ArrayList<EventdetailsResponse.eventDetails> eventDetails) {
        this.eventDetails = eventDetails;
    }

    public class  eventDetails
    {
        int Id;
        int RSVP;
        String Title;
        String Description;
        String Event_date;
        String Venue;
        String Class_id;
        String Visibility;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getRSVP() {
            return RSVP;
        }

        public void setRSVP(int RSVP) {
            this.RSVP = RSVP;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getEvent_date() {
            return Event_date;
        }

        public void setEvent_date(String event_date) {
            Event_date = event_date;
        }

        public String getVenue() {
            return Venue;
        }

        public void setVenue(String venue) {
            Venue = venue;
        }

        public String getClass_id() {
            return Class_id;
        }

        public void setClass_id(String class_id) {
            Class_id = class_id;
        }

        public String getVisibility() {
            return Visibility;
        }

        public void setVisibility(String visibility) {
            Visibility = visibility;
        }
    }
}
