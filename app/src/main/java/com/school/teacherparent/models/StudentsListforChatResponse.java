package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentsListforChatResponse {

    String message;
    String statusText;
    int status;
    ArrayList<chatStudentsList> chatStudentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<StudentsListforChatResponse.chatStudentsList> getChatStudentsList() {
        return chatStudentsList;
    }

    public void setChatStudentsList(ArrayList<StudentsListforChatResponse.chatStudentsList> chatStudentsList) {
        this.chatStudentsList = chatStudentsList;
    }

    public static class chatStudentsList {
        String studID;
        String Fname;
        String Lname;
        String Student_photo;

        public chatStudentsList(String studID, String fname, String lname, String student_photo) {
            this.studID = studID;
            Fname = fname;
            Lname = lname;
            Student_photo = student_photo;
        }

        public String getStudID() {
            return studID;
        }

        public void setStudID(String studID) {
            this.studID = studID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }
    }

}
