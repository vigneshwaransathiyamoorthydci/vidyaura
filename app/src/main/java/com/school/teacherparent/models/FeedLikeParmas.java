package com.school.teacherparent.models;

public class FeedLikeParmas {

    private String userID;
    private String feedID;
    private String userType;
    private String schoolID;
    private String commentID;
    private String clapStatus;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFeedID() {
        return feedID;
    }

    public void setFeedID(String feedID) {
        this.feedID = feedID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public String getClapStatus() {
        return clapStatus;
    }

    public void setClapStatus(String clapStatus) {
        this.clapStatus = clapStatus;
    }
}
