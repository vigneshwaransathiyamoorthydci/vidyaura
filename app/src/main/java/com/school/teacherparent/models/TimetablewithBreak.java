package com.school.teacherparent.models;

public class TimetablewithBreak {

    public String classimage ;
    public String className;
    public String classSection;
    public String peroidstartTime;
    public String peroidendTime;
    public String classSubject;
    public String peroid;
    public Object timeMillsecond;

    public Object getTimeMillsecond() {
        return timeMillsecond;
    }

    public void setTimeMillsecond(Object timeMillsecond) {
        this.timeMillsecond = timeMillsecond;
    }

    public TimetablewithBreak(String classimage, String className, String classSection, String peroidstartTime, String peroidendTime, String classSubject, String peroid, Object timeMillsecond) {
        this.classimage = classimage;
        this.className = className;
        this.classSection = classSection;
        this.peroidstartTime = peroidstartTime;
        this.peroidendTime = peroidendTime;
        this.classSubject = classSubject;
        this.peroid = peroid;
        this.timeMillsecond = timeMillsecond;
    }

    public String getClassimage() {
        return classimage;
    }

    public void setClassimage(String classimage) {
        this.classimage = classimage;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassSection() {
        return classSection;
    }

    public void setClassSection(String classSection) {
        this.classSection = classSection;
    }

    public String getPeroidstartTime() {
        return peroidstartTime;
    }

    public void setPeroidstartTime(String peroidstartTime) {
        this.peroidstartTime = peroidstartTime;
    }

    public String getPeroidendTime() {
        return peroidendTime;
    }

    public void setPeroidendTime(String peroidendTime) {
        this.peroidendTime = peroidendTime;
    }

    public String getClassSubject() {
        return classSubject;
    }

    public void setClassSubject(String classSubject) {
        this.classSubject = classSubject;
    }

    public String getPeroid() {
        return peroid;
    }

    public void setPeroid(String peroid) {
        this.peroid = peroid;
    }
}
