package com.school.teacherparent.models;

public class FeedLikeResponse
{
    private String message;
    private String statusText;
    private int status;
    private int clapStatus;
    private int feedLikeCount;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getFeedLikeCount() {
        return feedLikeCount;
    }

    public void setFeedLikeCount(int feedLikeCount) {
        this.feedLikeCount = feedLikeCount;
    }

    public int getClapStatus() {
        return clapStatus;
    }

    public void setClapStatus(int clapStatus) {
        this.clapStatus = clapStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
