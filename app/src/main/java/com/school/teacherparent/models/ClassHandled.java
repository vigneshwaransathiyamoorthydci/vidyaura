package com.school.teacherparent.models;

import java.util.ArrayList;

public class ClassHandled {

    String message;
    String statusText;
    int status;
    private ArrayList<chatTeachersDetailsList> chatTeachersDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ClassHandled.chatTeachersDetailsList> getChatTeachersDetailsList() {
        return chatTeachersDetailsList;
    }

    public void setChatTeachersDetailsList(ArrayList<ClassHandled.chatTeachersDetailsList> chatTeachersDetailsList) {
        this.chatTeachersDetailsList = chatTeachersDetailsList;
    }

    public class chatTeachersDetailsList {

        private int Id;
        private int Subject_id;
        private int Classroom_id;
        private int Section_id;
        private int Teacher_id;
        private int School_id;
        private String Status;
        private String Created_at;
        private String Updated_at;
        private String teacherName;
        private String subjectName;
        private String className;
        private String altName;
        private String sectionName;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getTeacher_id() {
            return Teacher_id;
        }

        public void setTeacher_id(int teacher_id) {
            Teacher_id = teacher_id;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getCreated_at() {
            return Created_at;
        }

        public void setCreated_at(String created_at) {
            Created_at = created_at;
        }

        public String getUpdated_at() {
            return Updated_at;
        }

        public void setUpdated_at(String updated_at) {
            Updated_at = updated_at;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public String getAltName() {
            return altName;
        }

        public void setAltName(String altName) {
            this.altName = altName;
        }

        public String getSectionName() {
            return sectionName;
        }

        public void setSectionName(String sectionName) {
            this.sectionName = sectionName;
        }
    }

}
