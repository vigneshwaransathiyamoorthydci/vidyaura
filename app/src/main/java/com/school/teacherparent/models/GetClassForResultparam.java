package com.school.teacherparent.models;

public class GetClassForResultparam
{

    private String userType;
    private String userID;
    private String schoolID;
    private String examTermID;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getExamTermID() {
        return examTermID;
    }

    public void setExamTermID(String examTermID) {
        this.examTermID = examTermID;
    }
}
