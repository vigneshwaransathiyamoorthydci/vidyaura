package com.school.teacherparent.models;

public class AddClassworkParams
{
    private String userID;
    private String userType;
    private String schoolID;
   private String termID;
   private String subjectID;
   private String classID;
   private String sectionID;
   private String chapterID;
   private String topicID;
   private String classworkDesc;
   private String completedON;
    private String document;

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getTermID() {
        return termID;
    }

    public void setTermID(String termID) {
        this.termID = termID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getChapterID() {
        return chapterID;
    }

    public void setChapterID(String chapterID) {
        this.chapterID = chapterID;
    }

    public String getTopicID() {
        return topicID;
    }

    public void setTopicID(String topicID) {
        this.topicID = topicID;
    }

    public String getClassworkDesc() {
        return classworkDesc;
    }

    public void setClassworkDesc(String classworkDesc) {
        this.classworkDesc = classworkDesc;
    }

    public String getCompletedON() {
        return completedON;
    }

    public void setCompletedON(String completedON) {
        this.completedON = completedON;
    }
}
