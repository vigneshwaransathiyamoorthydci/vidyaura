package com.school.teacherparent.models;

import java.util.ArrayList;

public class AcademicTermResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<academicTermsList> academicTermsList;

    public ArrayList<AcademicTermResponse.academicTermsList> getAcademicTermsList() {
        return academicTermsList;
    }

    public void setAcademicTermsList(ArrayList<AcademicTermResponse.academicTermsList> academicTermsList) {
        this.academicTermsList = academicTermsList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public  class academicTermsList
    {
        private int Id;
        private String Name;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
