package com.school.teacherparent.models;

import java.util.ArrayList;

public class ClassListResponse
{
    private String message;
    private String statusText;
    private int status;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ArrayList<teachersClassesList> teachersClassesList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ClassListResponse.teachersClassesList> getTeachersClassesList() {
        return teachersClassesList;
    }

    public void setTeachersClassesList(ArrayList<ClassListResponse.teachersClassesList> teachersClassesList) {
        this.teachersClassesList = teachersClassesList;
    }

    public class teachersClassesList
    {
        private String ClassName;
        private String Section;
        private int classID;
        private int sectionID;

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }
    }
}
