package com.school.teacherparent.models;

public class FeedCommentcomposeResponse
{
    private String message;
    private String commentID;
    private String statusText;
    private int status;
    private int feedCommentCount;
    private String commentPostedTime;

    public String getCommentPostedTime() {
        return commentPostedTime;
    }

    public void setCommentPostedTime(String commentPostedTime) {
        this.commentPostedTime = commentPostedTime;
    }

    public int getFeedCommentCount() {
        return feedCommentCount;
    }

    public void setFeedCommentCount(int feedCommentCount) {
        this.feedCommentCount = feedCommentCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
