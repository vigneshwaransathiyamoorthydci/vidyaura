package com.school.teacherparent.models;

import java.util.List;

public class SyllabusListModel {
    private String message;
    private String statusText;
    private Integer status;
    private List<SyllabusCompletedPercentage> syllabusList = null;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SyllabusCompletedPercentage> getSyllabusCompletedPercentage() {
        return syllabusList;
    }

    public void setSyllabusCompletedPercentage(List<SyllabusCompletedPercentage> syllabusCompletedPercentage) {
        this.syllabusList = syllabusCompletedPercentage;
    }

    public class SyllabusCompletedPercentage{
        private Integer classID;
        private String ClassName;
        private Integer sectionID;
        private String Section;
        private Integer subjectID;
        private String subjectName;
        private String syllabusCompletedPercentage;
        private String Class_image;

        public String getSyllabusList() {
            return syllabusCompletedPercentage;
        }

        public void setSyllabusList(String syllabusList) {
            this.syllabusCompletedPercentage = syllabusList;
        }

        public Integer getClassID() {
            return classID;
        }

        public void setClassID(Integer classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            this.ClassName = className;
        }

        public Integer getSectionID() {
            return sectionID;
        }

        public void setSectionID(Integer sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            this.Section = section;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }
}
