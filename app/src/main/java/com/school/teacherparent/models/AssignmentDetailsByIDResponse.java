package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class AssignmentDetailsByIDResponse
{
    public String  message;
    public String statusText;
    public int status;
    ArrayList<assignmentDetails> assignmentDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AssignmentDetailsByIDResponse.assignmentDetails> getAssignmentDetails() {
        return assignmentDetails;
    }

    public void setAssignmentDetails(ArrayList<AssignmentDetailsByIDResponse.assignmentDetails> assignmentDetails) {
        this.assignmentDetails = assignmentDetails;
    }

    public class assignmentDetails
    {
        int Id;
        String Title;
        int Academic_year;
        int Term_id;
        int Subject_id;
        int Class_id;
        int Section_id;
        String Topic_id;
        String Description;
        String Document;
        String Chapter_id;
        String DueDate;
        int Mark;
        List<String> attachmentsList;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getMark() {
            return Mark;
        }

        public void setMark(int mark) {
            Mark = mark;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public int getAcademic_year() {
            return Academic_year;
        }

        public void setAcademic_year(int academic_year) {
            Academic_year = academic_year;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getDocument() {
            return Document;
        }

        public void setDocument(String document) {
            Document = document;
        }
    }

}
