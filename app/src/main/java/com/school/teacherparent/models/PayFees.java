package com.school.teacherparent.models;

public class PayFees {

    String feesStatusID;
    String GatewayTxID;
    String totalAmnt;
    String discountAmnt;
    String classID;
    String sectionID;
    String studentID;
    String schoolID;
    String userType;
    String userID;

    public String getFeesStatusID() {
        return feesStatusID;
    }

    public void setFeesStatusID(String feesStatusID) {
        this.feesStatusID = feesStatusID;
    }

    public String getGatewayTxID() {
        return GatewayTxID;
    }

    public void setGatewayTxID(String gatewayTxID) {
        GatewayTxID = gatewayTxID;
    }

    public String getTotalAmnt() {
        return totalAmnt;
    }

    public void setTotalAmnt(String totalAmnt) {
        this.totalAmnt = totalAmnt;
    }

    public String getDiscountAmnt() {
        return discountAmnt;
    }

    public void setDiscountAmnt(String discountAmnt) {
        this.discountAmnt = discountAmnt;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
