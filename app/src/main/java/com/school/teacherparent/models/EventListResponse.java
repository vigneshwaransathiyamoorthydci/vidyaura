package com.school.teacherparent.models;

import java.util.ArrayList;

public class EventListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<eventsList> eventsList;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<EventListResponse.eventsList> getEventsList() {
        return eventsList;
    }

    public void setEventsList(ArrayList<EventListResponse.eventsList> eventsList) {
        this.eventsList = eventsList;
    }

    public class eventsList
    {
        private int eventID;
        private int Id;
        private int RSVP;
        private String Title;
        private String Description;
        private String Event_date;
        private String Venue;
        private String Class_id;
        private String eventAcceptedCount;
        private String eventMaybeCount;
        private String eventDeclinedCount;
        private String notResponded;
        private int isUserPostedEvent;
        private String Author_type;
        private String adminName;
        private int eventAttendeesStatus;
        public  ArrayList<String> classNameAlist;

        public ArrayList<String> getClassNameAlist() {
            return classNameAlist;
        }

        public void setClassNameAlist(ArrayList<String> classNameAlist) {
            this.classNameAlist = classNameAlist;
        }
        public String getAdminName() {
            return adminName;
        }

        public void setAdminName(String adminName) {
            this.adminName = adminName;
        }

        public String getAuthor_type() {
            return Author_type;
        }

        public void setAuthor_type(String author_type) {
            Author_type = author_type;
        }

        public ArrayList<className> className;
        public ArrayList<userDetails> userDetails;

        public int getEventID() {
            return eventID;
        }

        public void setEventID(int eventID) {
            this.eventID = eventID;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getRSVP() {
            return RSVP;
        }

        public void setRSVP(int RSVP) {
            this.RSVP = RSVP;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getEvent_date() {
            return Event_date;
        }

        public void setEvent_date(String event_date) {
            Event_date = event_date;
        }

        public String getVenue() {
            return Venue;
        }

        public void setVenue(String venue) {
            Venue = venue;
        }

        public String getClass_id() {
            return Class_id;
        }

        public void setClass_id(String class_id) {
            Class_id = class_id;
        }

        public String getEventAcceptedCount() {
            return eventAcceptedCount;
        }

        public void setEventAcceptedCount(String eventAcceptedCount) {
            this.eventAcceptedCount = eventAcceptedCount;
        }

        public String getEventMaybeCount() {
            return eventMaybeCount;
        }

        public void setEventMaybeCount(String eventMaybeCount) {
            this.eventMaybeCount = eventMaybeCount;
        }

        public String getEventDeclinedCount() {
            return eventDeclinedCount;
        }

        public void setEventDeclinedCount(String eventDeclinedCount) {
            this.eventDeclinedCount = eventDeclinedCount;
        }

        public String getNotResponded() {
            return notResponded;
        }

        public void setNotResponded(String notResponded) {
            this.notResponded = notResponded;
        }

        public int getIsUserPostedEvent() {
            return isUserPostedEvent;
        }

        public void setIsUserPostedEvent(int isUserPostedEvent) {
            this.isUserPostedEvent = isUserPostedEvent;
        }

        public ArrayList<EventListResponse.className> getClassName() {
            return className;
        }

        public void setClassName(ArrayList<EventListResponse.className> className) {
            this.className = className;
        }

        public ArrayList<EventListResponse.userDetails> getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(ArrayList<EventListResponse.userDetails> userDetails) {
            this.userDetails = userDetails;
        }

        public int getEventAttendeesStatus() {
            return eventAttendeesStatus;
        }

        public void setEventAttendeesStatus(int eventAttendeesStatus) {
            this.eventAttendeesStatus = eventAttendeesStatus;
        }
    }

    public class className
    {
        private int Id;
        private String ClassName;
        private String Alt_name;
        private int Group_id;
        private String Section;

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getAlt_name() {
            return Alt_name;
        }

        public void setAlt_name(String alt_name) {
            Alt_name = alt_name;
        }

        public int getGroup_id() {
            return Group_id;
        }

        public void setGroup_id(int group_id) {
            Group_id = group_id;
        }
    }

    public class  userDetails
    {
        int Id;
        private String Fname;
        private String Lname;
        private String Emp_photo;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getEmp_photo() {
            return Emp_photo;
        }

        public void setEmp_photo(String emp_photo) {
            Emp_photo = emp_photo;
        }
    }

}
