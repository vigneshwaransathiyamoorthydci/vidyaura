package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class FeedDetailsByIDResponse
{
    private String message;
    private String statusText;
    private int status;
    private ArrayList<feedResult> feedResult;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<FeedDetailsByIDResponse.feedResult> getFeedResult() {
        return feedResult;
    }

    public void setFeedResult(ArrayList<FeedDetailsByIDResponse.feedResult> feedResult) {
        this.feedResult = feedResult;
    }

    public class feedResult
    {
        private int Id;
        private String Title;
        private String Description;
        private String Visibility;
        private String Documents;
        private int Add_gallery;
        private String Gallery_id;
        private int  School_id;
        private List<String> attachmentsList;
        String Section_id;

        public String getSection_id() {
            return Section_id;
        }

        public void setSection_id(String section_id) {
            Section_id = section_id;
        }

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getVisibility() {
            return Visibility;
        }

        public void setVisibility(String visibility) {
            Visibility = visibility;
        }

        public String getDocuments() {
            return Documents;
        }

        public void setDocuments(String documents) {
            Documents = documents;
        }

        public int getAdd_gallery() {
            return Add_gallery;
        }

        public void setAdd_gallery(int add_gallery) {
            Add_gallery = add_gallery;
        }

        public String getGallery_id() {
            return Gallery_id;
        }

        public void setGallery_id(String gallery_id) {
            Gallery_id = gallery_id;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }
    }
}
