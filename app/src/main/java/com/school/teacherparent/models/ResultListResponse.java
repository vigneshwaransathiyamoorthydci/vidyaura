package com.school.teacherparent.models;

import java.util.ArrayList;

public class ResultListResponse
{

    String message;
    String statusText;
    int status;

    public class studentDetails
    {
        int studID;
        String Fname;
        String Lname;
        String Student_photo;

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }
    }

    ArrayList<resultList> resultList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ResultListResponse.resultList> getResultList() {
        return resultList;
    }

    public void setResultList(ArrayList<ResultListResponse.resultList> resultList) {
        this.resultList = resultList;
    }

    public class resultList
    {
        studentDetails studentDetails;

        public ResultListResponse.studentDetails getStudentDetails() {
            return studentDetails;
        }

        public void setStudentDetails(ResultListResponse.studentDetails studentDetails) {
            this.studentDetails = studentDetails;
        }

        int Class_id;
         int Section_id;
         String totalStudentsCount;
         String classAverage;
       ArrayList< topperInClass> topperInClass;
        String Alt_name;
        String ClassName;
        String Section;
        private String Class_image;
        String schoolName;
        String Student_photo;

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public String getSchoolName() {
            return schoolName;
        }

        public void setSchoolName(String schoolName) {
            this.schoolName = schoolName;
        }

        public ArrayList<ResultListResponse.resultList.topperInClass> getTopperInClass() {
            return topperInClass;
        }

        public void setTopperInClass(ArrayList<ResultListResponse.resultList.topperInClass> topperInClass) {
            this.topperInClass = topperInClass;
        }

        public String getAlt_name() {
            return Alt_name;
        }

        public void setAlt_name(String alt_name) {
            Alt_name = alt_name;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }



        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public String getTotalStudentsCount() {
            return totalStudentsCount;
        }

        public void setTotalStudentsCount(String totalStudentsCount) {
            this.totalStudentsCount = totalStudentsCount;
        }

        public String getClassAverage() {
            return classAverage;
        }

        public void setClassAverage(String classAverage) {
            this.classAverage = classAverage;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public class topperInClass
         {
             int Total_marks;
             int Student_id;
             String Fname;
             String Lname;
             String Student_photo;

             public int getTotal_marks() {
                 return Total_marks;
             }

             public void setTotal_marks(int total_marks) {
                 Total_marks = total_marks;
             }

             public int getStudent_id() {
                 return Student_id;
             }

             public void setStudent_id(int student_id) {
                 Student_id = student_id;
             }

             public String getFname() {
                 return Fname;
             }

             public void setFname(String fname) {
                 Fname = fname;
             }

             public String getLname() {
                 return Lname;
             }

             public void setLname(String lname) {
                 Lname = lname;
             }

             public String getStudent_photo() {
                 return Student_photo;
             }

             public void setStudent_photo(String student_photo) {
                 Student_photo = student_photo;
             }
         }


    }


}
