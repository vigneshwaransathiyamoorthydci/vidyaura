package com.school.teacherparent.models;

public class AddLeaveParams
{
    private String userID;
    private String userType;
    private String classID;
    private String sectionID;
    private String schoolID;
    private String studID;
    private String attendanceDate;
    long daysCount;
    String fromDate;
    String toDate;
    String reason;
    String description;
    String leaveSession;
    int isHalfDay;
    int isSessionType;

    public int getIsHalfDay() {
        return isHalfDay;
    }

    public void setIsHalfDay(int isHalfDay) {
        this.isHalfDay = isHalfDay;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getStudID() {
        return studID;
    }

    public void setStudID(String studID) {
        this.studID = studID;
    }

    public String getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public long getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(long daysCount) {
        this.daysCount = daysCount;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLeaveSession() {
        return leaveSession;
    }

    public void setLeaveSession(String leaveSession) {
        this.leaveSession = leaveSession;
    }

    public int getIsSessionType() {
        return isSessionType;
    }

    public void setIsSessionType(int isSessionType) {
        this.isSessionType = isSessionType;
    }
}
