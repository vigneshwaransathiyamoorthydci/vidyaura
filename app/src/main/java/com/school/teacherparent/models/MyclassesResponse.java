package com.school.teacherparent.models;

import java.util.ArrayList;

public class MyclassesResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<classesProfileDetails> classesProfileDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<MyclassesResponse.classesProfileDetails> getClassesProfileDetails() {
        return classesProfileDetails;
    }

    public void setClassesProfileDetails(ArrayList<MyclassesResponse.classesProfileDetails> classesProfileDetails) {
        this.classesProfileDetails = classesProfileDetails;
    }

    public class classesProfileDetails
    {
        String iconName;
        String ClassName;
        int classID;
        int sectionID;
        String Section;
        int subjectID;
        String Name;
        String studCount;
        String Class_image;

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }

        public String getIconName() {
            return iconName;
        }

        public void setIconName(String iconName) {
            this.iconName = iconName;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getStudCount() {
            return studCount;
        }

        public void setStudCount(String studCount) {
            this.studCount = studCount;
        }
    }
}
