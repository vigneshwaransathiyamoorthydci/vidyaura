package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class IsUserExists
{
    public String  message;
    public String statusText;
    public int status;
    public int userExist;
    public String phoneNumber;
    public String securityPin;
    public String tokenTeacher;
    public String tokenParent;

    public String getTokenTeacher() {
        return tokenTeacher;
    }

    public void setTokenTeacher(String tokenTeacher) {
        this.tokenTeacher = tokenTeacher;
    }

    public String getTokenParent() {
        return tokenParent;
    }

    public void setTokenParent(String tokenParent) {
        this.tokenParent = tokenParent;
    }

    public ArrayList<IsUserExists.appSettings> getAppSettings() {
        return appSettings;
    }

    public void setAppSettings(ArrayList<IsUserExists.appSettings> appSettings) {
        this.appSettings = appSettings;
    }

    public ArrayList<appSettings> appSettings;

    public String getSecurityPin() {
        return securityPin;
    }

    public void setSecurityPin(String securityPin) {
        this.securityPin = securityPin;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUserExist() {
        return userExist;
    }

    public void setUserExist(int userExist) {
        this.userExist = userExist;
    }
    //    public List<data> data;
//
//    public List<IsUserExists.data> getData() {
//        return data;
//    }
//
//    public void setData(List<IsUserExists.data> data) {
//        this.data = data;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public String getStatusText() {
//        return statusText;
//    }
//
//    public void setStatusText(String statusText) {
//        this.statusText = statusText;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//
//
//    public class data
//    {
//
//        public int Id;
//        public String status;
//        public String Fname;
//        public String Lname;
//        public String Emp_id;
//        public String Phone;
//        public String Email;
//        public String Dob;
//        public String Gender;
//        public String Emp_photo;
//        public String Salutation;
//        public String SecurityPin;
//
//        public String getSecurityPin() {
//            return SecurityPin;
//        }
//
//        public void setSecurityPin(String securityPin) {
//            SecurityPin = securityPin;
//        }
//
//        public String getSalutation() {
//            return Salutation;
//        }
//
//        public void setSalutation(String salutation) {
//            Salutation = salutation;
//        }
//
//        public String Class_handled;
//        public String Joining_date;
//
//        public String getClass_handled() {
//            return Class_handled;
//        }
//
//        public void setClass_handled(String class_handled) {
//            Class_handled = class_handled;
//        }
//
//        public String getJoining_date() {
//            return Joining_date;
//        }
//
//        public void setJoining_date(String joining_date) {
//            Joining_date = joining_date;
//        }
//
//        public int getId() {
//            return Id;
//        }
//
//        public void setId(int id) {
//            Id = id;
//        }
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public String getFname() {
//            return Fname;
//        }
//
//        public void setFname(String fname) {
//            Fname = fname;
//        }
//
//        public String getLname() {
//            return Lname;
//        }
//
//        public void setLname(String lname) {
//            Lname = lname;
//        }
//
//        public String getEmp_id() {
//            return Emp_id;
//        }
//
//        public void setEmp_id(String emp_id) {
//            Emp_id = emp_id;
//        }
//
//        public String getPhone() {
//            return Phone;
//        }
//
//        public void setPhone(String phone) {
//            Phone = phone;
//        }
//
//        public String getEmail() {
//            return Email;
//        }
//
//        public void setEmail(String email) {
//            Email = email;
//        }
//
//        public String getDob() {
//            return Dob;
//        }
//
//        public void setDob(String dob) {
//            Dob = dob;
//        }
//
//        public String getGender() {
//            return Gender;
//        }
//
//        public void setGender(String gender) {
//            Gender = gender;
//        }
//
//        public String getEmp_photo() {
//            return Emp_photo;
//        }
//
//        public void setEmp_photo(String emp_photo) {
//            Emp_photo = emp_photo;
//        }
//    }

    public class appSettings
    {
        int NotificationStatus;
        int MessageStatus;
        int SecurityPinStatus;

        public int getNotificationStatus() {
            return NotificationStatus;
        }

        public void setNotificationStatus(int notificationStatus) {
            NotificationStatus = notificationStatus;
        }

        public int getMessageStatus() {
            return MessageStatus;
        }

        public void setMessageStatus(int messageStatus) {
            MessageStatus = messageStatus;
        }

        public int getSecurityPinStatus() {
            return SecurityPinStatus;
        }

        public void setSecurityPinStatus(int securityPinStatus) {
            SecurityPinStatus = securityPinStatus;
        }
    }

}
