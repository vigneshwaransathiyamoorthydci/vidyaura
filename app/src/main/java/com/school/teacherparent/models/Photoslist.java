package com.school.teacherparent.models;

import java.util.ArrayList;

public class Photoslist
{
    int Id;
    String Title;
    String Description;
 String image;

    public Photoslist(int id, String title, String description,String image) {
        Id = id;
        Title = title;
        Description = description;
        this.image = image;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
