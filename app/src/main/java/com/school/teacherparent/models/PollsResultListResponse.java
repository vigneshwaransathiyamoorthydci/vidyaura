package com.school.teacherparent.models;

import java.util.ArrayList;

public class PollsResultListResponse {

    /////////////////////////////////////////////////////////////////
    private String pollsName;
    private String pollsPercent;
    private String pollsVote;
    private int pollsCategoryID;

    public String getPollsName() {
        return pollsName;
    }

    public void setPollsName(String pollsName) {
        this.pollsName = pollsName;
    }

    public String getPollsPercent() {
        return pollsPercent;
    }

    public void setPollsPercent(String pollsPercent) {
        this.pollsPercent = pollsPercent;
    }

    public String getPollsVote() {
        return pollsVote;
    }

    public void setPollsVote(String pollsVote) {
        this.pollsVote = pollsVote;
    }

    public int getPollsCategoryID() {
        return pollsCategoryID;
    }

    public void setPollsCategoryID(int pollsCategoryID) {
        this.pollsCategoryID = pollsCategoryID;
    }

    public PollsResultListResponse(String pollsName, String pollsPercent, String pollsVote, int pollsCategoryID) {
        this.pollsName = pollsName;
        this.pollsPercent = pollsPercent;
        this.pollsVote = pollsVote;
        this.pollsCategoryID = pollsCategoryID;
    }

    //////////////////////////////////////////////////////////////////

    String message;
    String statusText;
    int status;

    public ArrayList<getPolls> getPolls;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<getPolls> getGetPolls() {
        return getPolls;
    }

    public void setGetPolls(ArrayList<getPolls> getPolls) {
        this.getPolls = getPolls;
    }

    public class getPolls {
        int Poll_category_id;
        String categoryName;
        String categoryNoOfVotes;
        String categoryPercentage;
        ArrayList<pollList> pollList;

        public int getPoll_category_id() {
            return Poll_category_id;
        }

        public void setPoll_category_id(int poll_category_id) {
            Poll_category_id = poll_category_id;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryNoOfVotes() {
            return categoryNoOfVotes;
        }

        public void setCategoryNoOfVotes(String categoryNoOfVotes) {
            this.categoryNoOfVotes = categoryNoOfVotes;
        }

        public String getCategoryPercentage() {
            return categoryPercentage;
        }

        public void setCategoryPercentage(String categoryPercentage) {
            this.categoryPercentage = categoryPercentage;
        }

        public ArrayList<PollsResultListResponse.getPolls.pollList> getPollList() {
            return pollList;
        }

        public void setPollList(ArrayList<PollsResultListResponse.getPolls.pollList> pollList) {
            this.pollList = pollList;
        }

        public class pollList {
            int pollsID;
            String Question;
            int Poll_category_id;
            String Publish_date;
            ArrayList<isParentVoted> isParentVoted;
            String totalVotesCount;
            int School_id;
            ArrayList<pollsOptions> pollsOptions;

            public int getPollsID() {
                return pollsID;
            }

            public void setPollsID(int pollsID) {
                this.pollsID = pollsID;
            }

            public String getQuestion() {
                return Question;
            }

            public void setQuestion(String question) {
                Question = question;
            }

            public int getPoll_category_id() {
                return Poll_category_id;
            }

            public void setPoll_category_id(int poll_category_id) {
                Poll_category_id = poll_category_id;
            }

            public String getPublish_date() {
                return Publish_date;
            }

            public void setPublish_date(String publish_date) {
                Publish_date = publish_date;
            }

            public ArrayList<isParentVoted> getIsParentVoted() {
                return isParentVoted;
            }

            public void setIsParentVoted(ArrayList<isParentVoted> isParentVoted) {
                this.isParentVoted = isParentVoted;
            }

            public String getTotalVotesCount() {
                return totalVotesCount;
            }

            public void setTotalVotesCount(String totalVotesCount) {
                this.totalVotesCount = totalVotesCount;
            }

            public ArrayList<pollsOptions> getPollsOptions() {
                return pollsOptions;
            }

            public void setPollsOptions(ArrayList<pollsOptions> pollsOptions) {
                this.pollsOptions = pollsOptions;
            }

            public int getSchool_id() {
                return School_id;
            }

            public void setSchool_id(int school_id) {
                School_id = school_id;
            }

            public class isParentVoted {
                int Poll_id;
                int PollOption_id;

                public int getPoll_id() {
                    return Poll_id;
                }

                public void setPoll_id(int poll_id) {
                    Poll_id = poll_id;
                }

                public int getPollOption_id() {
                    return PollOption_id;
                }

                public void setPollOption_id(int pollOption_id) {
                    PollOption_id = pollOption_id;
                }
            }

            public class pollsOptions {
                int pollsOptionsID;
                String Options;
                int Poll_id;
                int noOfVotes;
                String optionsPercentage;

                public int getPollsOptionsID() {
                    return pollsOptionsID;
                }

                public void setPollsOptionsID(int pollsOptionsID) {
                    this.pollsOptionsID = pollsOptionsID;
                }

                public String getOptions() {
                    return Options;
                }

                public void setOptions(String options) {
                    Options = options;
                }

                public int getPoll_id() {
                    return Poll_id;
                }

                public void setPoll_id(int poll_id) {
                    Poll_id = poll_id;
                }

                public int getNoOfVotes() {
                    return noOfVotes;
                }

                public void setNoOfVotes(int noOfVotes) {
                    this.noOfVotes = noOfVotes;
                }

                public String getOptionsPercentage() {
                    return optionsPercentage;
                }

                public void setOptionsPercentage(String optionsPercentage) {
                    this.optionsPercentage = optionsPercentage;
                }
            }

        }
    }

}


