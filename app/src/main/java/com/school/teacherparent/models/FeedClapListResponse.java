package com.school.teacherparent.models;

import java.util.ArrayList;

public class FeedClapListResponse
{
    private String message;
    private String statusText;
    private int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public  feedClapsList feedClapsList;

    public FeedClapListResponse.feedClapsList getFeedClapsList() {
        return feedClapsList;
    }

    public void setFeedClapsList(FeedClapListResponse.feedClapsList feedClapsList) {
        this.feedClapsList = feedClapsList;
    }

    public class  feedClapsList
    {
        private int current_page;

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }
        ArrayList<data> data;

        public ArrayList<data> getData() {
            return data;
        }

        public void setData(ArrayList<data> data) {
            this.data = data;
        }

        public class data
        {

            ArrayList<feedLikedByUserDetails> feedLikedByUserDetails;

            private int Id;
                private String Fname;
                private String Lname;
                private String Emp_photo;
                private String Like_user_type;

                public int getId() {
                    return Id;
                }

                public void setId(int id) {
                    Id = id;
                }

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getLname() {
                    return Lname;
                }

                public void setLname(String lname) {
                    Lname = lname;
                }

                public String getEmp_photo() {
                    return Emp_photo;
                }

                public void setEmp_photo(String emp_photo) {
                    Emp_photo = emp_photo;
                }

            public String getLike_user_type() {
                return Like_user_type;
            }

            public void setLike_user_type(String like_user_type) {
                Like_user_type = like_user_type;
            }

            public ArrayList<feedLikedByUserDetails> getFeedLikedByUserDetails() {
                return feedLikedByUserDetails;
            }

            public void setFeedLikedByUserDetails(ArrayList<feedLikedByUserDetails> feedLikedByUserDetails) {
                this.feedLikedByUserDetails = feedLikedByUserDetails;
            }

            public class  feedLikedByUserDetails
            {
                private int Id;
                private String Fname;
                private String Lname;
                private String Emp_photo;

                public feedLikedByUserDetails(int id, String fname, String lname, String emp_photo) {
                    Id = id;
                    Fname = fname;
                    Lname = lname;
                    Emp_photo = emp_photo;
                }

                public int getId() {
                    return Id;
                }

                public void setId(int id) {
                    Id = id;
                }

                public String getFname() {
                    return Fname;
                }

                public void setFname(String fname) {
                    Fname = fname;
                }

                public String getLname() {
                    return Lname;
                }

                public void setLname(String lname) {
                    Lname = lname;
                }

                public String getEmp_photo() {
                    return Emp_photo;
                }

                public void setEmp_photo(String emp_photo) {
                    Emp_photo = emp_photo;
                }
            }

        }



    }

}
