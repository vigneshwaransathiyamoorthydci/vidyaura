package com.school.teacherparent.models;

public class CommentReplyAddedResponse
{
    private String message;
    private String replyID;
    private int feedCommentCount;
    private String statusText;
    private int status;
    private String replyCommentTime;

    public String getReplyCommentTime() {
        return replyCommentTime;
    }

    public void setReplyCommentTime(String replyCommentTime) {
        this.replyCommentTime = replyCommentTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReplyID() {
        return replyID;
    }

    public void setReplyID(String replyID) {
        this.replyID = replyID;
    }

    public int getFeedCommentCount() {
        return feedCommentCount;
    }

    public void setFeedCommentCount(int feedCommentCount) {
        this.feedCommentCount = feedCommentCount;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
