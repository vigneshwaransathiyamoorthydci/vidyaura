package com.school.teacherparent.models;

import java.util.ArrayList;

public class HomeworkListResponse {
    String message;
    String statusText;
    int status;
    ArrayList<homeworksList> homeworksList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<HomeworkListResponse.homeworksList> getHomeworksList() {
        return homeworksList;
    }

    public void setHomeworksList(ArrayList<HomeworkListResponse.homeworksList> homeworksList) {
        this.homeworksList = homeworksList;
    }

    public  class homeworksList
    {
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        int subjectID;
        String subjectName;
        ArrayList<classwiseHomework> classwiseHomework;
        private String Class_image;

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public ArrayList<HomeworkListResponse.classwiseHomework> getClasswiseHomework() {
            return classwiseHomework;
        }

        public void setClasswiseHomework(ArrayList<HomeworkListResponse.classwiseHomework> classwiseHomework) {
            this.classwiseHomework = classwiseHomework;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }

    public class classwiseHomework
    {
            int isDueDateToday;
            int isUserPostedHomework;
            String DueDate;
            String Assigned_on;
            String subjectName;
            String ClassName;
            String Section;
            String Chapter_name;
        String topicDetails;
        int homeworkID;
        int totalHomeworkCount;

        public int getTotalHomeworkCount() {
            return totalHomeworkCount;
        }

        public void setTotalHomeworkCount(int totalHomeworkCount) {
            this.totalHomeworkCount = totalHomeworkCount;
        }

        public int getHomeworkID() {
            return homeworkID;
        }

        public void setHomeworkID(int homeworkID) {
            this.homeworkID = homeworkID;
        }

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

//        ArrayList<topicDetails> topicDetails;

        public int getIsDueDateToday() {
            return isDueDateToday;
        }

        public void setIsDueDateToday(int isDueDateToday) {
            this.isDueDateToday = isDueDateToday;
        }

        public int getIsUserPostedHomework() {
            return isUserPostedHomework;
        }

        public void setIsUserPostedHomework(int isUserPostedHomework) {
            this.isUserPostedHomework = isUserPostedHomework;
        }

        public String getAssigned_on() {
            return Assigned_on;
        }

        public void setAssigned_on(String assigned_on) {
            Assigned_on = assigned_on;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

//        public ArrayList<HomeworkListResponse.topicDetails> getTopicDetails() {
//            return topicDetails;
//        }
//
//        public void setTopicDetails(ArrayList<HomeworkListResponse.topicDetails> topicDetails) {
//            this.topicDetails = topicDetails;
//        }
    }

    public class topicDetails
    {
        String Topic_name;

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }
    }
}

