package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentListforResultResponse
{

    String message;
    String statusText;
    int status;
    ArrayList<resultStudentsList> resultStudentsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<StudentListforResultResponse.resultStudentsList> getResultStudentsList() {
        return resultStudentsList;
    }

    public void setResultStudentsList(ArrayList<StudentListforResultResponse.resultStudentsList> resultStudentsList) {
        this.resultStudentsList = resultStudentsList;
    }

    public class resultStudentsList
    {

            String Fname;
            String Lname;
            String Student_photo;
            int studID;

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }
    }


}
