package com.school.teacherparent.models;

import java.util.ArrayList;

public class StudentList
{
    int studID;
    String Fname;
    String Lname;
    String Student_photo;
    int selectedID;
    ArrayList<StudentListResponse.attendanceDetails> attendanceDetails;

    public ArrayList<StudentListResponse.attendanceDetails> getAttendanceDetails() {
        return attendanceDetails;
    }

    public void setAttendanceDetails(ArrayList<StudentListResponse.attendanceDetails> attendanceDetails) {
        this.attendanceDetails = attendanceDetails;
    }



    public StudentList(int studID, String fname, String lname, String student_photo, int selectedID, ArrayList<StudentListResponse.attendanceDetails> attendanceDetails) {
        this.studID = studID;
        Fname = fname;
        Lname = lname;
        Student_photo = student_photo;
        this.selectedID = selectedID;
        this.attendanceDetails = attendanceDetails;
    }

    public class  attendanceDetails
    {
        String Attendance_type;
        int Od_id;

        public int getOd_id() {
            return Od_id;
        }

        public void setOd_id(int od_id) {
            Od_id = od_id;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }
    }
    public int getStudID() {
        return studID;
    }

    public void setStudID(int studID) {
        this.studID = studID;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getStudent_photo() {
        return Student_photo;
    }

    public void setStudent_photo(String student_photo) {
        Student_photo = student_photo;
    }

    public int getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }
}
