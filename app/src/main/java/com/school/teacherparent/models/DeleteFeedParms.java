package com.school.teacherparent.models;

public class DeleteFeedParms
{
    private String feedID;
    private String userID;
    private String schoolID;
    private String userType;
    private int isReportorDelete;
    private String token;
    String reportReason;

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIsReportorDelete() {
        return isReportorDelete;
    }

    public void setIsReportorDelete(int isReportorDelete) {
        this.isReportorDelete = isReportorDelete;
    }

    public String getFeedID() {
        return feedID;
    }

    public void setFeedID(String feedID) {
        this.feedID = feedID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
