package com.school.teacherparent.models;

public class ExamDetailsParams
{

    private String userType;
    private String userID;
    private String schoolID;
    private String examScheduleID;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getExamScheduleID() {
        return examScheduleID;
    }

    public void setExamScheduleID(String examScheduleID) {
        this.examScheduleID = examScheduleID;
    }
}
