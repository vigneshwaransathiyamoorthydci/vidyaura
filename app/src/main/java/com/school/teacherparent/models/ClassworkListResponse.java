package com.school.teacherparent.models;

import java.util.ArrayList;

public class ClassworkListResponse {
    String message;
    String statusText;
    int status;
    ArrayList<classworkList> classworkList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ClassworkListResponse.classworkList> getClassworkList() {
        return classworkList;
    }

    public void setClassworkList(ArrayList<ClassworkListResponse.classworkList> classworkList) {
        this.classworkList = classworkList;
    }

    public  class classworkList
    {
        int classID;
        String ClassName;
        int sectionID;
        String Section;
        int subjectID;
        String subjectName;
       String  classworkCompletedPercentage;
        ArrayList<todaysClassworkProgress> todaysClassworkProgress;
        String topicDetails;

        public String getTopicDetails() {
            return topicDetails;
        }

        public void setTopicDetails(String topicDetails) {
            this.topicDetails = topicDetails;
        }

        public String getClassworkCompletedPercentage() {
            return classworkCompletedPercentage;
        }

        public void setClassworkCompletedPercentage(String classworkCompletedPercentage) {
            this.classworkCompletedPercentage = classworkCompletedPercentage;
        }

        public ArrayList<ClassworkListResponse.todaysClassworkProgress> getTodaysClassworkProgress() {
            return todaysClassworkProgress;
        }

        public void setTodaysClassworkProgress(ArrayList<ClassworkListResponse.todaysClassworkProgress> todaysClassworkProgress) {
            this.todaysClassworkProgress = todaysClassworkProgress;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public int getSectionID() {
            return sectionID;
        }

        public void setSectionID(int sectionID) {
            this.sectionID = sectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public int getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(int subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }


    }

    public class todaysClassworkProgress
    {

        String Topic_id;
        String Chapter_id;
        ArrayList<chapterName> chapterName;

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public ArrayList<ClassworkListResponse.chapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(ArrayList<ClassworkListResponse.chapterName> chapterName) {
            this.chapterName = chapterName;
        }
    }
public class chapterName
{
    String Chapter_name;

    public String getChapter_name() {
        return Chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        Chapter_name = chapter_name;
    }
}

    public class topicDetails
    {
        String Topic_name;

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }
    }
}

