package com.school.teacherparent.models;

import java.util.ArrayList;

public class ParentsofResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<studentsProfileDetails> studentsProfileDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ParentsofResponse.studentsProfileDetails> getStudentsProfileDetails() {
        return studentsProfileDetails;
    }

    public void setStudentsProfileDetails(ArrayList<ParentsofResponse.studentsProfileDetails> studentsProfileDetails) {
        this.studentsProfileDetails = studentsProfileDetails;
    }

    public class studentsProfileDetails
    {
        ArrayList<studList> studList;

        public ArrayList<ParentsofResponse.studentsProfileDetails.studList> getStudList() {
            return studList;
        }

        public void setStudList(ArrayList<ParentsofResponse.studentsProfileDetails.studList> studList) {
            this.studList = studList;
        }

        public class  studList
        {
            int Class_id;
            int Section_id;
            String Fname;
            String Lname;
            String ClassName;
            String Section;
                String iconName;

            public String getIconName() {
                return iconName;
            }

            public void setIconName(String iconName) {
                this.iconName = iconName;
            }

            public String getClassName() {
                return ClassName;
            }

            public void setClassName(String className) {
                ClassName = className;
            }

            public String getSection() {
                return Section;
            }

            public void setSection(String section) {
                Section = section;
            }

            public int getClass_id() {
                return Class_id;
            }

            public void setClass_id(int class_id) {
                Class_id = class_id;
            }

            public int getSection_id() {
                return Section_id;
            }

            public void setSection_id(int section_id) {
                Section_id = section_id;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }
        }
    }
}
