package com.school.teacherparent.models;

public class SelectedImageList
{
    private String image;
    private  boolean isedit;

    public SelectedImageList(String image, boolean isedit) {
        this.image = image;
        this.isedit = isedit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getisIsedit() {
        return isedit;
    }

    public void setIsedit(boolean isedit) {
        this.isedit = isedit;
    }
}
