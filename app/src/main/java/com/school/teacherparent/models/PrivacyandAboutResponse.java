package com.school.teacherparent.models;

import java.util.ArrayList;

public class PrivacyandAboutResponse
{
    String message;
    String statusText;
    int status;
    String token;
ArrayList<getPrivacyAboutDetails>getPrivacyAboutDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<PrivacyandAboutResponse.getPrivacyAboutDetails> getGetPrivacyAboutDetails() {
        return getPrivacyAboutDetails;
    }

    public void setGetPrivacyAboutDetails(ArrayList<PrivacyandAboutResponse.getPrivacyAboutDetails> getPrivacyAboutDetails) {
        this.getPrivacyAboutDetails = getPrivacyAboutDetails;
    }

    public class getPrivacyAboutDetails
    {
        String About;
        String Privacy_policy;

        public String getAbout() {
            return About;
        }

        public void setAbout(String about) {
            About = about;
        }

        public String getPrivacy_policy() {
            return Privacy_policy;
        }

        public void setPrivacy_policy(String privacy_policy) {
            Privacy_policy = privacy_policy;
        }
    }

}
