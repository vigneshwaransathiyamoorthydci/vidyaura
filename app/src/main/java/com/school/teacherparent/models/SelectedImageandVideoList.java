package com.school.teacherparent.models;

public class SelectedImageandVideoList
{
    private String image;
    private  boolean isedit;
    private String extension;
    public SelectedImageandVideoList(String image, boolean isedit,String extension) {
        this.image = image;
        this.isedit = isedit;
        this.extension=extension;
    }

    public boolean isIsedit() {
        return isedit;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getisIsedit() {
        return isedit;
    }

    public void setIsedit(boolean isedit) {
        this.isedit = isedit;
    }
}
