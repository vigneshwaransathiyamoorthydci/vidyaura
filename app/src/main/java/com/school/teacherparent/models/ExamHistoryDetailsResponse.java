package com.school.teacherparent.models;

import java.util.ArrayList;

public class ExamHistoryDetailsResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<ExamHistoryDetailsByClassList> examHistoryDetailsByClassList;

    public ArrayList<ExamHistoryDetailsByClassList> getExamHistoryDetailsByClassList() {
        return examHistoryDetailsByClassList;
    }

    public void setExamHistoryDetailsByClassList(ArrayList<ExamHistoryDetailsByClassList> examHistoryDetailsByClassList) {
        this.examHistoryDetailsByClassList = examHistoryDetailsByClassList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public class topicName
    {
        private int Id;
        private int Class_id;
        private int Subject_id;
        private int Chapter_id;
        private String Topic_name;
        private int School_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(int chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            Topic_name = topic_name;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }
    }

    public class chapterName
    {
        private int Id;
        private int Class_id;
        private int Subject_id;
        private int Chapter_id;
        private String Chapter_name;
        private int School_id;

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(int chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            Chapter_name = chapter_name;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }
    }

    public class  ExamHistoryDetailsByClassList
    {

        private int Id;
        private String Exam_duration;
        private String Start_time;
        private String End_time;
        private int Subject_id;
        private int Classroom_id;
        private int Classsection_id;
        private int Term_id;
        private int Class_id;
        private String Name;
        private int isUserPostedExam;
        ArrayList<topicName> topicName;
        ArrayList<chapterName> chapterName;
        private String classImage;

        public ArrayList<ExamHistoryDetailsResponse.chapterName> getChapterName() {
            return chapterName;
        }

        public void setChapterName(ArrayList<ExamHistoryDetailsResponse.chapterName> chapterName) {
            this.chapterName = chapterName;
        }

        public ArrayList<ExamHistoryDetailsResponse.topicName> getTopicName() {
            return topicName;
        }

        public void setTopicName(ArrayList<ExamHistoryDetailsResponse.topicName> topicName) {
            this.topicName = topicName;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getExam_duration() {
            return Exam_duration;
        }

        public void setExam_duration(String exam_duration) {
            Exam_duration = exam_duration;
        }

        public String getStart_time() {
            return Start_time;
        }

        public void setStart_time(String start_time) {
            Start_time = start_time;
        }

        public String getEnd_time() {
            return End_time;
        }

        public void setEnd_time(String end_time) {
            End_time = end_time;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public int getClasssection_id() {
            return Classsection_id;
        }

        public void setClasssection_id(int classsection_id) {
            Classsection_id = classsection_id;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getIsUserPostedExam() {
            return isUserPostedExam;
        }

        public void setIsUserPostedExam(int isUserPostedExam) {
            this.isUserPostedExam = isUserPostedExam;
        }

        public String getClassImage() {
            return classImage;
        }

        public void setClassImage(String classImage) {
            this.classImage = classImage;
        }
    }

}
