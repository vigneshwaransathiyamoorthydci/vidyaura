package com.school.teacherparent.models;

public class FeedbackDetailsResponse
{
    String message;
    String statusText;
    int status;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    getFeedbackDetails getFeedbackDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public FeedbackDetailsResponse.getFeedbackDetails getGetFeedbackDetails() {
        return getFeedbackDetails;
    }

    public void setGetFeedbackDetails(FeedbackDetailsResponse.getFeedbackDetails getFeedbackDetails) {
        this.getFeedbackDetails = getFeedbackDetails;
    }

    public class  getFeedbackDetails
    {
        int id;
        String name;
        String email;
        int school_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getSchool_id() {
            return school_id;
        }

        public void setSchool_id(int school_id) {
            this.school_id = school_id;
        }
    }

}
