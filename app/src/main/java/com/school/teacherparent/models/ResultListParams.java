package com.school.teacherparent.models;

public class ResultListParams
{

    private String userType;
    private String userID;
    private String examTermID;
    private String schoolID;
    private int studID;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getExamTermID() {
        return examTermID;
    }

    public void setExamTermID(String examTermID) {
        this.examTermID = examTermID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public int getStudID() {
        return studID;
    }

    public void setStudID(int studID) {
        this.studID = studID;
    }
}
