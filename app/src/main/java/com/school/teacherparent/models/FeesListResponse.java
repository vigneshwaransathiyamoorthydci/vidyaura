package com.school.teacherparent.models;

import java.util.ArrayList;

public class FeesListResponse {

    String message;
    String statusText;
    int status;
    private ArrayList<feesList> feesList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<feesList> getFeesList() {
        return feesList;
    }

    public void setFeesList(ArrayList<feesList> feesList) {
        this.feesList = feesList;
    }

    public class feesList {

        //int feesID;
        int Term_id;
        //int Fee_type;
        int Total_amount;
        //String Due_date;
        //int amountToBePaid;
        String PaidDate;
        String DueDate;
        String term_name;
        int DiscountedAmount;
        ArrayList<feesDetails> feesDetails;

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getTotal_amount() {
            return Total_amount;
        }

        public void setTotal_amount(int total_amount) {
            Total_amount = total_amount;
        }

        public String getTerm_name() {
            return term_name;
        }

        public void setTerm_name(String term_name) {
            this.term_name = term_name;
        }

        public int getDiscountedAmount() {
            return DiscountedAmount;
        }

        public void setDiscountedAmount(int discountedAmount) {
            DiscountedAmount = discountedAmount;
        }

        public ArrayList<feesDetails> getFeesDetails() {
            return feesDetails;
        }

        public void setFeesDetails(ArrayList<feesDetails> feesDetails) {
            this.feesDetails = feesDetails;
        }

        public String getPaidDate() {
            return PaidDate;
        }

        public void setPaidDate(String paidDate) {
            PaidDate = paidDate;
        }

        public String getDueDate() {
            return DueDate;
        }

        public void setDueDate(String dueDate) {
            DueDate = dueDate;
        }

        public class feesDetails {

            /*int Id;
            int Student_id;
            int Academic_year;
            int Class_id;
            int Section_id;
            int Fees_type_id;
            int Total_amount;
            int Discount_amount;
            int Paid_amount;
            String Description;
            String Payment_type_id;
            String Paid_date;
            String Paid_status;
            String Transaction_id;
            String Transaction_status;
            String Status;
            int School_id;
            String created_at;
            String updated_at;*/

            int feesPaidID;
            int Class_id;
            int Section_id;
            int Term_id;
            int Fees_type_id;
            int Total_amount;
            int Discount_amount;
            int Fees_status_id;
            String Paid_status;
            int Student_id;
            String Paid_date;
            int discountAmount;
            String feesTypeName;
            String Transaction_unique_id;
            String Payment_transaction_id;

            public int getFeesPaidID() {
                return feesPaidID;
            }

            public void setFeesPaidID(int feesPaidID) {
                this.feesPaidID = feesPaidID;
            }

            public int getClass_id() {
                return Class_id;
            }

            public void setClass_id(int class_id) {
                Class_id = class_id;
            }

            public int getSection_id() {
                return Section_id;
            }

            public void setSection_id(int section_id) {
                Section_id = section_id;
            }

            public int getTerm_id() {
                return Term_id;
            }

            public void setTerm_id(int term_id) {
                Term_id = term_id;
            }

            public int getFees_type_id() {
                return Fees_type_id;
            }

            public void setFees_type_id(int fees_type_id) {
                Fees_type_id = fees_type_id;
            }

            public int getTotal_amount() {
                return Total_amount;
            }

            public void setTotal_amount(int total_amount) {
                Total_amount = total_amount;
            }

            public int getDiscount_amount() {
                return Discount_amount;
            }

            public void setDiscount_amount(int discount_amount) {
                Discount_amount = discount_amount;
            }

            public int getFees_status_id() {
                return Fees_status_id;
            }

            public void setFees_status_id(int fees_status_id) {
                Fees_status_id = fees_status_id;
            }

            public String getPaid_status() {
                return Paid_status;
            }

            public void setPaid_status(String paid_status) {
                Paid_status = paid_status;
            }

            public int getStudent_id() {
                return Student_id;
            }

            public void setStudent_id(int student_id) {
                Student_id = student_id;
            }

            public String getPaid_date() {
                return Paid_date;
            }

            public void setPaid_date(String paid_date) {
                Paid_date = paid_date;
            }

            public int getDiscountAmount() {
                return discountAmount;
            }

            public void setDiscountAmount(int discountAmount) {
                this.discountAmount = discountAmount;
            }

            public String getFeesTypeName() {
                return feesTypeName;
            }

            public void setFeesTypeName(String feesTypeName) {
                this.feesTypeName = feesTypeName;
            }

            public String getTransaction_unique_id() {
                return Transaction_unique_id;
            }

            public void setTransaction_unique_id(String transaction_unique_id) {
                Transaction_unique_id = transaction_unique_id;
            }

            public String getPayment_transaction_id() {
                return Payment_transaction_id;
            }

            public void setPayment_transaction_id(String payment_transaction_id) {
                Payment_transaction_id = payment_transaction_id;
            }
        }

    }

}
