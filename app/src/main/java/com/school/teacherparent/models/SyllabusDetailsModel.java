package com.school.teacherparent.models;

import java.util.List;

public class SyllabusDetailsModel {

    private String message;
    private String statusText;
    private Integer status;
    private List<SyllabusDetailsList> syllabusDetailsList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SyllabusDetailsList> getSyllabusDetailsList() {
        return syllabusDetailsList;
    }

    public void setSyllabusDetailsList(List<SyllabusDetailsList> syllabusDetailsList) {
        this.syllabusDetailsList = syllabusDetailsList;
    }

    public class SyllabusDetailsList {

        private Integer classID;
        private String ClassName;
        private Integer SectionID;
        private String Section;
        private Integer subjectID;
        private String subjectName;
        private Integer termID;
        private String termName;
        private List<SyllabusList> syllabusList = null;
        private String Class_image;

        public Integer getClassID() {
            return classID;
        }

        public void setClassID(Integer classID) {
            this.classID = classID;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String ClassName) {
            this.ClassName = ClassName;
        }

        public Integer getSectionID() {
            return SectionID;
        }

        public void setSectionID(Integer SectionID) {
            this.SectionID = SectionID;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String Section) {
            this.Section = Section;
        }

        public Integer getSubjectID() {
            return subjectID;
        }

        public void setSubjectID(Integer subjectID) {
            this.subjectID = subjectID;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public Integer getTermID() {
            return termID;
        }

        public void setTermID(Integer termID) {
            this.termID = termID;
        }

        public String getTermName() {
            return termName;
        }

        public void setTermName(String termName) {
            this.termName = termName;
        }

        public List<SyllabusList> getSyllabusList() {
            return syllabusList;
        }

        public void setSyllabusList(List<SyllabusList> syllabusList) {
            this.syllabusList = syllabusList;
        }

        public String getClass_image() {
            return Class_image;
        }

        public void setClass_image(String class_image) {
            Class_image = class_image;
        }
    }
    public class SyllabusList {

        private Integer Chapter_id;
        private List<ChapterTopicDetail> chapterTopicDetails = null;

        public Integer getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(Integer Chapter_id) {
            this.Chapter_id = Chapter_id;
        }

        public List<ChapterTopicDetail> getChapterTopicDetails() {
            return chapterTopicDetails;
        }

        public void setChapterTopicDetails(List<ChapterTopicDetail> chapterTopicDetails) {
            this.chapterTopicDetails = chapterTopicDetails;
        }
    }

    public class ChapterTopicDetail {

        private String Chapter_name;
        private String Topic_name;
        private String termID;
        private String termName;

        public String getTermID() {
            return termID;
        }

        public void setTermID(String termID) {
            this.termID = termID;
        }

        public String getTermName() {
            return termName;
        }

        public void setTermName(String termName) {
            this.termName = termName;
        }

        public String getChapter_name() {
            return Chapter_name;
        }

        public void setChapter_name(String chapter_name) {
            this.Chapter_name = chapter_name;
        }

        public String getTopic_name() {
            return Topic_name;
        }

        public void setTopic_name(String topic_name) {
            this.Topic_name = topic_name;
        }
    }
}
