package com.school.teacherparent.models;

import java.util.ArrayList;

public class CommentReplyResponse
{
    private String message;
    private String statusText;
    private int status;
    public feedCommentsList feedCommentsList;

    public CommentReplyResponse.feedCommentsList getFeedCommentsList() {
        return feedCommentsList;
    }

    public void setFeedCommentsList(CommentReplyResponse.feedCommentsList feedCommentsList) {
        this.feedCommentsList = feedCommentsList;
    }

    public class  feedCommentsList
    {

    }
    public ArrayList<data> data;

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<CommentReplyResponse.data> getData() {
        return data;
    }

    public void setData(ArrayList<CommentReplyResponse.data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class  data
    {
        public int commentReplyID;
        public String commentDescription;
        public int userID;
        public String userImage;
        public int feedID;
        public String userName;

        public int getCommentReplyID() {
            return commentReplyID;
        }

        public void setCommentReplyID(int commentReplyID) {
            this.commentReplyID = commentReplyID;
        }

        public String getCommentDescription() {
            return commentDescription;
        }

        public void setCommentDescription(String commentDescription) {
            this.commentDescription = commentDescription;
        }

        public int getUserID() {
            return userID;
        }

        public void setUserID(int userID) {
            this.userID = userID;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        public int getFeedID() {
            return feedID;
        }

        public void setFeedID(int feedID) {
            this.feedID = feedID;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
