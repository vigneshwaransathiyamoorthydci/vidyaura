package com.school.teacherparent.models;

import java.util.ArrayList;

public class AttendanceListResponseforParents
{
    String message;
    String statusText;
    int status;
    String attendancePercentage;
    String token;

    public String getAttendancePercentage() {
        return attendancePercentage;
    }

    public void setAttendancePercentage(String attendancePercentage) {
        this.attendancePercentage = attendancePercentage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    ArrayList<attendanceList> attendanceList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<AttendanceListResponseforParents.attendanceList> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(ArrayList<AttendanceListResponseforParents.attendanceList> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public class attendanceList
    {
        int attendanceID;
        int Student_id;
        String checkLeaveDate;
        int classID;
        String isLeaveOrAbsent;
        String isHoliday;

        public String getIsHoliday() {
            return isHoliday;
        }

        public void setIsHoliday(String isHoliday) {
            this.isHoliday = isHoliday;
        }

        public int getAttendanceID() {
            return attendanceID;
        }

        public void setAttendanceID(int attendanceID) {
            this.attendanceID = attendanceID;
        }

        public int getStudent_id() {
            return Student_id;
        }

        public void setStudent_id(int student_id) {
            Student_id = student_id;
        }

        public String getCheckLeaveDate() {
            return checkLeaveDate;
        }

        public void setCheckLeaveDate(String checkLeaveDate) {
            this.checkLeaveDate = checkLeaveDate;
        }

        public int getClassID() {
            return classID;
        }

        public void setClassID(int classID) {
            this.classID = classID;
        }

        public String getIsLeaveOrAbsent() {
            return isLeaveOrAbsent;
        }

        public void setIsLeaveOrAbsent(String isLeaveOrAbsent) {
            this.isLeaveOrAbsent = isLeaveOrAbsent;
        }
    }

}
