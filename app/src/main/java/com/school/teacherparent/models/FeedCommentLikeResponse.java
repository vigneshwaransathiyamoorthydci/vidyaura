package com.school.teacherparent.models;

public class FeedCommentLikeResponse
{
    public String message;
    public int feedLikeCount;
    public int isUserFeedLiked;
    public int commentsLikeCount;
    public int isUserCommentLiked;
    public String clapStatus;
    public String statusText;
    public int status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getFeedLikeCount() {
        return feedLikeCount;
    }

    public void setFeedLikeCount(int feedLikeCount) {
        this.feedLikeCount = feedLikeCount;
    }

    public int getIsUserFeedLiked() {
        return isUserFeedLiked;
    }

    public void setIsUserFeedLiked(int isUserFeedLiked) {
        this.isUserFeedLiked = isUserFeedLiked;
    }

    public int getCommentsLikeCount() {
        return commentsLikeCount;
    }

    public void setCommentsLikeCount(int commentsLikeCount) {
        this.commentsLikeCount = commentsLikeCount;
    }

    public int getIsUserCommentLiked() {
        return isUserCommentLiked;
    }

    public void setIsUserCommentLiked(int isUserCommentLiked) {
        this.isUserCommentLiked = isUserCommentLiked;
    }

    public String getClapStatus() {
        return clapStatus;
    }

    public void setClapStatus(String clapStatus) {
        this.clapStatus = clapStatus;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
