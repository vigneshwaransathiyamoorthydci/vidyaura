package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class LeaveListModel {
    public String message;
    public String statusText;
    public Integer status;
    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<LeaveDetailsList> leaveDetailsList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<LeaveDetailsList> getLeaveDetailsList() {
        return leaveDetailsList;
    }

    public void setLeaveDetailsList(List<LeaveDetailsList> leaveDetailsList) {
        this.leaveDetailsList = leaveDetailsList;
    }

    public class LeaveDetailsList {

        public Integer studID;
        public String Fname;
        public String Lname;
        public Integer Father_id;
        public Integer Mother_id;
        public Integer Guardian_Id;
        public String Student_photo;
        public Integer attendanceID;
        public String Attendance_type;
        public String Reason;
        public String Attendance_date;
        public Integer Forenoon_session;
        public Integer Afternoon_session;
        public String leaveTaken;
        String ClassName;
        String Section;
        String Alt_name;
        String created_at;

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getAlt_name() {
            return Alt_name;
        }

        public void setAlt_name(String alt_name) {
            Alt_name = alt_name;
        }

        public String getClassName() {
            return ClassName;
        }

        public void setClassName(String className) {
            ClassName = className;
        }

        public String getSection() {
            return Section;
        }

        public void setSection(String section) {
            Section = section;
        }

        public List<leaveDetails> leaveDetails;

        public Integer getStudID() {
            return studID;
        }

        public void setStudID(Integer studID) {
            this.studID = studID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public Integer getFather_id() {
            return Father_id;
        }

        public void setFather_id(Integer father_id) {
            Father_id = father_id;
        }

        public Integer getMother_id() {
            return Mother_id;
        }

        public void setMother_id(Integer mother_id) {
            Mother_id = mother_id;
        }

        public Integer getGuardian_Id() {
            return Guardian_Id;
        }

        public void setGuardian_Id(Integer guardian_Id) {
            Guardian_Id = guardian_Id;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public Integer getAttendanceID() {
            return attendanceID;
        }

        public void setAttendanceID(Integer attendanceID) {
            this.attendanceID = attendanceID;
        }

        public String getAttendance_type() {
            return Attendance_type;
        }

        public void setAttendance_type(String attendance_type) {
            Attendance_type = attendance_type;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String reason) {
            Reason = reason;
        }

        public String getAttendance_date() {
            return Attendance_date;
        }

        public void setAttendance_date(String attendance_date) {
            Attendance_date = attendance_date;
        }

        public Integer getForenoon_session() {
            return Forenoon_session;
        }

        public void setForenoon_session(Integer forenoon_session) {
            Forenoon_session = forenoon_session;
        }

        public Integer getAfternoon_session() {
            return Afternoon_session;
        }

        public void setAfternoon_session(Integer afternoon_session) {
            Afternoon_session = afternoon_session;
        }

        public String getLeaveTaken() {
            return leaveTaken;
        }

        public void setLeaveTaken(String leaveTaken) {
            this.leaveTaken = leaveTaken;
        }

        public List<leaveDetails> getLeaveDetails() {
            return leaveDetails;
        }

        public void setLeaveDetails(List<leaveDetails> leaveDetails) {
            this.leaveDetails = leaveDetails;
        }
    }
    public class leaveDetails {

        public Integer leaveID;
        public String From_date;
        public String To_date;
        public String leaveReason;
        public String Leave_status;
        public String Description;
        public String Parents_id;
        public parentDetails parentDetails;
        public String parentEncryptedID;

        public Integer getLeaveID() {
            return leaveID;
        }

        public void setLeaveID(Integer leaveID) {
            this.leaveID = leaveID;
        }

        public String getFrom_date() {
            return From_date;
        }

        public void setFrom_date(String from_date) {
            From_date = from_date;
        }

        public String getTo_date() {
            return To_date;
        }

        public void setTo_date(String to_date) {
            To_date = to_date;
        }

        public String getLeaveReason() {
            return leaveReason;
        }

        public void setLeaveReason(String leaveReason) {
            this.leaveReason = leaveReason;
        }

        public String getLeave_status() {
            return Leave_status;
        }

        public void setLeave_status(String leave_status) {
            Leave_status = leave_status;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getParents_id() {
            return Parents_id;
        }

        public void setParents_id(String parents_id) {
            Parents_id = parents_id;
        }

        public leaveDetails.parentDetails getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(leaveDetails.parentDetails parentDetails) {
            this.parentDetails = parentDetails;
        }

        public String getParentEncryptedID() {
            return parentEncryptedID;
        }

        public void setParentEncryptedID(String parentEncryptedID) {
            this.parentEncryptedID = parentEncryptedID;
        }

        public class parentDetails {
            int parentID;
            String Fname;
            String Type;
            String Mobile;
            String Photo;
            String Fcm_key;

            public int getParentID() {
                return parentID;
            }

            public void setParentID(int parentID) {
                this.parentID = parentID;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }

            public String getMobile() {
                return Mobile;
            }

            public void setMobile(String mobile) {
                Mobile = mobile;
            }

            public String getPhoto() {
                return Photo;
            }

            public void setPhoto(String photo) {
                Photo = photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }
        }
    }
}