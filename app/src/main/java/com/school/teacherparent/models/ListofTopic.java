package com.school.teacherparent.models;

public class ListofTopic
{
    private int Id;
    private int Subject_id;
    private int Chapter_id;
    private String Chapter_name;
    private String classsection;
    private int classsectionid;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getSubject_id() {
        return Subject_id;
    }

    public void setSubject_id(int subject_id) {
        Subject_id = subject_id;
    }

    public int getChapter_id() {
        return Chapter_id;
    }

    public void setChapter_id(int chapter_id) {
        Chapter_id = chapter_id;
    }

    public String getChapter_name() {
        return Chapter_name;
    }

    public void setChapter_name(String chapter_name) {
        Chapter_name = chapter_name;
    }

    public String getClasssection() {
        return classsection;
    }

    public void setClasssection(String classsection) {
        this.classsection = classsection;
    }

    public int getClasssectionid() {
        return classsectionid;
    }

    public void setClasssectionid(int classsectionid) {
        this.classsectionid = classsectionid;
    }

    public ListofTopic(int id, String chapter_name) {
        Id = id;

        Chapter_name = chapter_name;

    }
}
