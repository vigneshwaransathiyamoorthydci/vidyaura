package com.school.teacherparent.models;

import java.util.ArrayList;

public class ResultSubjectListResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<resultSubjectsList> resultSubjectsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<resultSubjectsList> getResultSubjectsList() {
        return resultSubjectsList;
    }

    public void setResultSubjectsList(ArrayList<resultSubjectsList> resultSubjectsList) {
        this.resultSubjectsList = resultSubjectsList;
    }

    public  class resultSubjectsList
    {
        private int Subject_id;
        private String Name;

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }
    }
}
