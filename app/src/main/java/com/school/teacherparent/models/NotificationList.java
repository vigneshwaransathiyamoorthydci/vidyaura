package com.school.teacherparent.models;

import java.util.ArrayList;

public class NotificationList {

    String message;
    String statusText;
    int status;
    private ArrayList<notification> notification;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<notification> getNotification() {
        return notification;
    }

    public void setNotification(ArrayList<notification> notification) {
        this.notification = notification;
    }

    public class notification {

        String Id;
        String Type;
        int Type_id;
        String Visibility;
        String Receiver_type;
        String Section_id;
        String User_id;
        String Notification_message;
        int Created_by;
        int Status;
        int Admin_status;
        int School_id;
        String created_at;
        String updated_at;
        int Classroom_id;
        String Classroom_name;
        String Section_name;
        int Subject_id;
        int studentID;
        String Exam_title;
        String User_type;
        String Imageurl;
        String Title;
        String Description;
        String Type_document;
        public examwiseClassList examwiseClassList;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public int getType_id() {
            return Type_id;
        }

        public void setType_id(int type_id) {
            Type_id = type_id;
        }

        public String getVisibility() {
            return Visibility;
        }

        public void setVisibility(String visibility) {
            Visibility = visibility;
        }

        public String getReceiver_type() {
            return Receiver_type;
        }

        public void setReceiver_type(String receiver_type) {
            Receiver_type = receiver_type;
        }

        public String getSection_id() {
            return Section_id;
        }

        public void setSection_id(String section_id) {
            Section_id = section_id;
        }

        public String getUser_id() {
            return User_id;
        }

        public void setUser_id(String user_id) {
            User_id = user_id;
        }

        public String getNotification_message() {
            return Notification_message;
        }

        public void setNotification_message(String notification_message) {
            Notification_message = notification_message;
        }

        public int getCreated_by() {
            return Created_by;
        }

        public void setCreated_by(int created_by) {
            Created_by = created_by;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public int getAdmin_status() {
            return Admin_status;
        }

        public void setAdmin_status(int admin_status) {
            Admin_status = admin_status;
        }

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getClassroom_id() {
            return Classroom_id;
        }

        public void setClassroom_id(int classroom_id) {
            Classroom_id = classroom_id;
        }

        public String getClassroom_name() {
            return Classroom_name;
        }

        public void setClassroom_name(String classroom_name) {
            Classroom_name = classroom_name;
        }

        public String getSection_name() {
            return Section_name;
        }

        public void setSection_name(String section_name) {
            Section_name = section_name;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getStudentID() {
            return studentID;
        }

        public void setStudentID(int studentID) {
            this.studentID = studentID;
        }

        public String getExam_title() {
            return Exam_title;
        }

        public void setExam_title(String exam_title) {
            Exam_title = exam_title;
        }

        public String getUser_type() {
            return User_type;
        }

        public void setUser_type(String user_type) {
            User_type = user_type;
        }

        public String getImageurl() {
            return Imageurl;
        }

        public void setImageurl(String imageurl) {
            Imageurl = imageurl;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getType_document() {
            return Type_document;
        }

        public void setType_document(String type_document) {
            Type_document = type_document;
        }

        public examwiseClassList getExamwiseClassList() {
            return examwiseClassList;
        }

        public void setExamwiseClassList(examwiseClassList examwiseClassList) {
            this.examwiseClassList = examwiseClassList;
        }

        public class examwiseClassList
        {
            private int Classroom_id;
            private String ClassName;
            private String Section;
            private int Classsection_id;

            public int getClassroom_id() {
                return Classroom_id;
            }

            public void setClassroom_id(int classroom_id) {
                Classroom_id = classroom_id;
            }

            public String getClassName() {
                return ClassName;
            }

            public void setClassName(String className) {
                ClassName = className;
            }

            public String getSection() {
                return Section;
            }

            public void setSection(String section) {
                Section = section;
            }

            public int getClasssection_id() {
                return Classsection_id;
            }

            public void setClasssection_id(int classsection_id) {
                Classsection_id = classsection_id;
            }
        }

    }
}
