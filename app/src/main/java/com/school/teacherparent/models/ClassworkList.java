package com.school.teacherparent.models;

import java.util.ArrayList;

public class ClassworkList
{
    String topicDetails;
        int marks;

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public ClassworkList(String topicDetails, int marks) {
        this.topicDetails = topicDetails;
        this.marks = marks;
    }

    public String getTopicDetails() {
        return topicDetails;
    }

    public void setTopicDetails(String topicDetails) {
        this.topicDetails = topicDetails;
    }
}
