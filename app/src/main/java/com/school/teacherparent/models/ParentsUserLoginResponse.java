package com.school.teacherparent.models;

import java.util.ArrayList;

public class ParentsUserLoginResponse
{

    public ArrayList<data> data;

    public ArrayList<appSettings> appSettings;

    public ArrayList<ParentsUserLoginResponse.appSettings> getAppSettings() {
        return appSettings;
    }

    public void setAppSettings(ArrayList<ParentsUserLoginResponse.appSettings> appSettings) {
        this.appSettings = appSettings;
    }

    /*public String schoolLogo;*/
    public String message;
    public String statusText;
    public int status;
    public String encyptedUserID;

    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String securedToken;

    public String getSecuredToken() {
        return securedToken;
    }

    public void setSecuredToken(String securedToken) {
        this.securedToken = securedToken;
    }

    public String getEncyptedUserID() {
        return encyptedUserID;
    }

    public void setEncyptedUserID(String encyptedUserID) {
        this.encyptedUserID = encyptedUserID;
    }

    public ArrayList<ParentsUserLoginResponse.data> getData() {
        return data;
    }

    public void setData(ArrayList<ParentsUserLoginResponse.data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /*public String getSchoolLogo() {
        return schoolLogo;
    }

    public void setSchoolLogo(String schoolLogo) {
        this.schoolLogo = schoolLogo;
    }*/

    public class appSettings
    {
        int NotificationStatus;
        int MessageStatus;
        int SecurityPinStatus;

        public int getNotificationStatus() {
            return NotificationStatus;
        }

        public void setNotificationStatus(int notificationStatus) {
            NotificationStatus = notificationStatus;
        }

        public int getMessageStatus() {
            return MessageStatus;
        }

        public void setMessageStatus(int messageStatus) {
            MessageStatus = messageStatus;
        }

        public int getSecurityPinStatus() {
            return SecurityPinStatus;
        }

        public void setSecurityPinStatus(int securityPinStatus) {
            SecurityPinStatus = securityPinStatus;
        }
    }


    public class data
    {

        public int Id;
        public String Type;
        public String Fname;
        public String Lname;
        public String Phone;
        public String Email;
        public String Photo;
        public String Mobile;
        String Education;
        String Occupation;
        String relation;
        String SecurityPin;
        int School_id;

        public int getSchool_id() {
            return School_id;
        }

        public void setSchool_id(int school_id) {
            School_id = school_id;
        }

        public String getSecurityPin() {
            return SecurityPin;
        }

        public void setSecurityPin(String securityPin) {
            SecurityPin = securityPin;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getPhoto() {
            return Photo;
        }

        public void setPhoto(String photo) {
            Photo = photo;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String mobile) {
            Mobile = mobile;
        }

        public String getEducation() {
            return Education;
        }

        public void setEducation(String education) {
            Education = education;
        }

        public String getOccupation() {
            return Occupation;
        }

        public void setOccupation(String occupation) {
            Occupation = occupation;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }
    }

}
