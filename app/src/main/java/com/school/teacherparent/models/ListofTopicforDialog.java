package com.school.teacherparent.models;

public class ListofTopicforDialog
{
    private int Id;
    private int Subject_id;
    private int Chapter_id;
    private String topic_name;
    private String classsection;
    private int classsectionid;
    public int selectedID;

    public int getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getSubject_id() {
        return Subject_id;
    }

    public void setSubject_id(int subject_id) {
        Subject_id = subject_id;
    }

    public int getChapter_id() {
        return Chapter_id;
    }

    public void setChapter_id(int chapter_id) {
        Chapter_id = chapter_id;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getClasssection() {
        return classsection;
    }

    public void setClasssection(String classsection) {
        this.classsection = classsection;
    }

    public int getClasssectionid() {
        return classsectionid;
    }

    public void setClasssectionid(int classsectionid) {
        this.classsectionid = classsectionid;
    }

    public ListofTopicforDialog(int id, String chapter_name, int selectedID) {
        Id = id;
        topic_name = chapter_name;
        this.selectedID = selectedID;
    }
}
