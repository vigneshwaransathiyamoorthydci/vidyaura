package com.school.teacherparent.models;

public class ParentsOfList
{
    int Class_id;
    int Section_id;
    String Fname;
    String Lname;
    String ClassName;
    String Section;

    public ParentsOfList(int class_id, int section_id, String fname, String lname, String className, String section) {
        Class_id = class_id;
        Section_id = section_id;
        Fname = fname;
        Lname = lname;
        ClassName = className;
        Section = section;
    }

    public int getClass_id() {
        return Class_id;
    }

    public void setClass_id(int class_id) {
        Class_id = class_id;
    }

    public int getSection_id() {
        return Section_id;
    }

    public void setSection_id(int section_id) {
        Section_id = section_id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }
}
