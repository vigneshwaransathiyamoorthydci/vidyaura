package com.school.teacherparent.models;

public class FeedClap {

    private String userName;
    private String userImage;
    private String Like_user_type;

    public FeedClap(String userName, String userImage, String like_user_type) {
        this.userName = userName;
        this.userImage = userImage;
        Like_user_type = like_user_type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getLike_user_type() {
        return Like_user_type;
    }

    public void setLike_user_type(String like_user_type) {
        Like_user_type = like_user_type;
    }
}
