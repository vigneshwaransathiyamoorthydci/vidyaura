package com.school.teacherparent.models;

import java.util.ArrayList;

public class ResultDetailsResponse
{
    private String message;
    private String statusText;
    private int status;
    ArrayList<resultDetailsList> resultDetailsList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ResultDetailsResponse.resultDetailsList> getResultDetailsList() {
        return resultDetailsList;
    }

    public void setResultDetailsList(ArrayList<ResultDetailsResponse.resultDetailsList> resultDetailsList) {
        this.resultDetailsList = resultDetailsList;
    }

    public class  resultDetailsList
    {
        int studID;
        String Fname;
        String Lname;
        String Student_photo;
        int Marks;
        int Subject_id;

        public int getStudID() {
            return studID;
        }

        public void setStudID(int studID) {
            this.studID = studID;
        }

        public String getFname() {
            return Fname;
        }

        public void setFname(String fname) {
            Fname = fname;
        }

        public String getLname() {
            return Lname;
        }

        public void setLname(String lname) {
            Lname = lname;
        }

        public String getStudent_photo() {
            return Student_photo;
        }

        public void setStudent_photo(String student_photo) {
            Student_photo = student_photo;
        }

        public int getMarks() {
            return Marks;
        }

        public void setMarks(int marks) {
            Marks = marks;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }
    }

}
