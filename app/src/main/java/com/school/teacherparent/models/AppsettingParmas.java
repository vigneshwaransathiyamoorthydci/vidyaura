package com.school.teacherparent.models;

public class AppsettingParmas
{
    String phoneNumber;
    String userType;
    int notificationStatus;
    int messageStatus;
    int securityPinStatus;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(int notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public int getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(int messageStatus) {
        this.messageStatus = messageStatus;
    }

    public int getSecurityPinStatus() {
        return securityPinStatus;
    }

    public void setSecurityPinStatus(int securityPinStatus) {
        this.securityPinStatus = securityPinStatus;
    }
}
