package com.school.teacherparent.models;

public class CircularDetailsParams
{

    private String userType;
    private String userID;
    private String circularsID;
    private String schoolID;

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getCircularsID() {
        return circularsID;
    }

    public void setCircularsID(String circularsID) {
        this.circularsID = circularsID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
