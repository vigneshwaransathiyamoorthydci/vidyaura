package com.school.teacherparent.models;

import java.util.ArrayList;
import java.util.List;

public class ClassworkdetailsResponse
{
    String message;
    String statusText;
    int status;
    ArrayList<classworkDetails> classworkDetails;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<ClassworkdetailsResponse.classworkDetails> getClassworkDetails() {
        return classworkDetails;
    }

    public void setClassworkDetails(ArrayList<ClassworkdetailsResponse.classworkDetails> classworkDetails) {
        this.classworkDetails = classworkDetails;
    }

    public class classworkDetails
    {

        int Id;
        int Academic_year;
        int Term_id;
        int Class_id;
        int Section_id;
        int Subject_id;
        int Teacher_id;
        String Chapter_id;
        String Topic_id;
        String Description;
        String Documents;
        String Completed_on;
        String School_id;
        private List<String> attachmentsList;

        public List<String> getAttachmentsList() {
            return attachmentsList;
        }

        public void setAttachmentsList(List<String> attachmentsList) {
            this.attachmentsList = attachmentsList;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        public int getAcademic_year() {
            return Academic_year;
        }

        public void setAcademic_year(int academic_year) {
            Academic_year = academic_year;
        }

        public int getTerm_id() {
            return Term_id;
        }

        public void setTerm_id(int term_id) {
            Term_id = term_id;
        }

        public int getClass_id() {
            return Class_id;
        }

        public void setClass_id(int class_id) {
            Class_id = class_id;
        }

        public int getSection_id() {
            return Section_id;
        }

        public void setSection_id(int section_id) {
            Section_id = section_id;
        }

        public int getSubject_id() {
            return Subject_id;
        }

        public void setSubject_id(int subject_id) {
            Subject_id = subject_id;
        }

        public int getTeacher_id() {
            return Teacher_id;
        }

        public void setTeacher_id(int teacher_id) {
            Teacher_id = teacher_id;
        }

        public String getChapter_id() {
            return Chapter_id;
        }

        public void setChapter_id(String chapter_id) {
            Chapter_id = chapter_id;
        }

        public String getTopic_id() {
            return Topic_id;
        }

        public void setTopic_id(String topic_id) {
            Topic_id = topic_id;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getDocuments() {
            return Documents;
        }

        public void setDocuments(String documents) {
            Documents = documents;
        }

        public String getCompleted_on() {
            return Completed_on;
        }

        public void setCompleted_on(String completed_on) {
            Completed_on = completed_on;
        }

        public String getSchool_id() {
            return School_id;
        }

        public void setSchool_id(String school_id) {
            School_id = school_id;
        }
    }


}

