package com.school.teacherparent.models;

public class UpdateFeedParams
{
    public String title ;
    public String feedDesc;
    public String userID;
    public String feedAttachment;
    public String addGallery;
    public String schoolID;
    public String userType;
    public String visibility;
    private String feedID;
    public String feedMultipleAttachment=null;
    String sectionIDs;

    public String getSectionIDs() {
        return sectionIDs;
    }

    public void setSectionIDs(String sectionIDs) {
        this.sectionIDs = sectionIDs;
    }

    public String getFeedMultipleAttachment() {
        return feedMultipleAttachment;
    }

    public void setFeedMultipleAttachment(String feedMultipleAttachment) {
        this.feedMultipleAttachment = feedMultipleAttachment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFeedDesc() {
        return feedDesc;
    }

    public void setFeedDesc(String feedDesc) {
        this.feedDesc = feedDesc;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFeedAttachment() {
        return feedAttachment;
    }

    public void setFeedAttachment(String feedAttachment) {
        this.feedAttachment = feedAttachment;
    }

    public String getAddGallery() {
        return addGallery;
    }

    public void setAddGallery(String addGallery) {
        this.addGallery = addGallery;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getFeedID() {
        return feedID;
    }

    public void setFeedID(String feedID) {
        this.feedID = feedID;
    }
}
