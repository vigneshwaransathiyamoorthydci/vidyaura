package com.school.teacherparent.models;

public class PollsListParms {

    private int id;
    private String opt;
    private String userID;
    private String schoolID;
    private String userType;
    private String studID;
    private int pollsID;
    private int pollsOptionsID;

    public PollsListParms() {

    }

    public PollsListParms(int id, String opt) {
        this.id = id;
        this.opt = opt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getStudID() {
        return studID;
    }

    public void setStudID(String studID) {
        this.studID = studID;
    }

    public int getPollsID() {
        return pollsID;
    }

    public void setPollsID(int pollsID) {
        this.pollsID = pollsID;
    }

    public int getPollsOptionsID() {
        return pollsOptionsID;
    }

    public void setPollsOptionsID(int pollsOptionsID) {
        this.pollsOptionsID = pollsOptionsID;
    }
}
