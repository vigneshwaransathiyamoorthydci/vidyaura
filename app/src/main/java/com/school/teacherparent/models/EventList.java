package com.school.teacherparent.models;

import java.util.ArrayList;

public class EventList
{
    private int eventID;
    private int Id;
    private int RSVP;
    private String Title;
    private String Description;
    private String Event_date;
    private String Venue;
    private String Class_id;
    private String adminName;
    private String eventAcceptedCount;
    private String eventMaybeCount;
    private String eventDeclinedCount;
    private String notResponded;
    private int isUserPostedEvent;
    private int eventAttendeesStatus;
    ArrayList<EventListResponse.userDetails> userDetails;
    ArrayList<EventListResponse.className> classNames;
    public  ArrayList<String> classNameAlist;

    public ArrayList<String> getClassNameAlist() {
        return classNameAlist;
    }

    public void setClassNameAlist(ArrayList<String> classNameAlist) {
        this.classNameAlist = classNameAlist;
    }

    public EventList(int eventID, int id, int RSVP, String title, String description, String event_date, String venue, String class_id, String adminName, String eventAcceptedCount, String eventMaybeCount, String eventDeclinedCount, String notResponded, int isUserPostedEvent, ArrayList<EventListResponse.userDetails> userDetails, ArrayList<EventListResponse.className> classNames, ArrayList<String> classNameAlist) {
        this.eventID = eventID;
        Id = id;
        this.RSVP = RSVP;
        Title = title;
        Description = description;
        Event_date = event_date;
        Venue = venue;
        Class_id = class_id;
        this.adminName = adminName;
        this.eventAcceptedCount = eventAcceptedCount;
        this.eventMaybeCount = eventMaybeCount;
        this.eventDeclinedCount = eventDeclinedCount;
        this.notResponded = notResponded;
        this.isUserPostedEvent = isUserPostedEvent;
        this.userDetails = userDetails;
        this.classNames = classNames;
        this.classNameAlist=classNameAlist;
    }

    public EventList(int eventID, int id, int RSVP, String title, String description, String event_date, String venue, String class_id, String adminName, String eventAcceptedCount, String eventMaybeCount, String eventDeclinedCount, String notResponded, int isUserPostedEvent,ArrayList<EventListResponse.userDetails> userDetails, ArrayList<EventListResponse.className> classNames, ArrayList<String> classNameAlist,int eventAttendeesStatus) {
        this.eventID = eventID;
        Id = id;
        this.RSVP = RSVP;
        Title = title;
        Description = description;
        Event_date = event_date;
        Venue = venue;
        Class_id = class_id;
        this.adminName = adminName;
        this.eventAcceptedCount = eventAcceptedCount;
        this.eventMaybeCount = eventMaybeCount;
        this.eventDeclinedCount = eventDeclinedCount;
        this.notResponded = notResponded;
        this.isUserPostedEvent = isUserPostedEvent;
        this.userDetails = userDetails;
        this.classNames = classNames;
        this.classNameAlist=classNameAlist;
        this.eventAttendeesStatus=eventAttendeesStatus;

    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getRSVP() {
        return RSVP;
    }

    public void setRSVP(int RSVP) {
        this.RSVP = RSVP;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEvent_date() {
        return Event_date;
    }

    public void setEvent_date(String event_date) {
        Event_date = event_date;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

    public String getClass_id() {
        return Class_id;
    }

    public void setClass_id(String class_id) {
        Class_id = class_id;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getEventAcceptedCount() {
        return eventAcceptedCount;
    }

    public void setEventAcceptedCount(String eventAcceptedCount) {
        this.eventAcceptedCount = eventAcceptedCount;
    }

    public String getEventMaybeCount() {
        return eventMaybeCount;
    }

    public void setEventMaybeCount(String eventMaybeCount) {
        this.eventMaybeCount = eventMaybeCount;
    }

    public String getEventDeclinedCount() {
        return eventDeclinedCount;
    }

    public void setEventDeclinedCount(String eventDeclinedCount) {
        this.eventDeclinedCount = eventDeclinedCount;
    }

    public String getNotResponded() {
        return notResponded;
    }

    public void setNotResponded(String notResponded) {
        this.notResponded = notResponded;
    }

    public int getIsUserPostedEvent() {
        return isUserPostedEvent;
    }

    public void setIsUserPostedEvent(int isUserPostedEvent) {
        this.isUserPostedEvent = isUserPostedEvent;
    }

    public int getEventAttendeesStatus() {
        return eventAttendeesStatus;
    }

    public void setEventAttendeesStatus(int eventAttendeesStatus) {
        this.eventAttendeesStatus = eventAttendeesStatus;
    }

    public ArrayList<EventListResponse.userDetails> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(ArrayList<EventListResponse.userDetails> userDetails) {
        this.userDetails = userDetails;
    }

    public ArrayList<EventListResponse.className> getClassNames() {
        return classNames;
    }

    public void setClassNames(ArrayList<EventListResponse.className> classNames) {
        this.classNames = classNames;
    }
}
