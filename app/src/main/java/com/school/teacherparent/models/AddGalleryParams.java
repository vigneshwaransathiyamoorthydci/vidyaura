package com.school.teacherparent.models;

public class AddGalleryParams
{

    private String schoolID;
    private String userID;
    public String albumAttachment;
    String albumName;
    String visibility;
    String userType;
    String sectionIDs;

    public String getSectionIDs() {
        return sectionIDs;
    }

    public void setSectionIDs(String sectionIDs) {
        this.sectionIDs = sectionIDs;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAlbumAttachment() {
        return albumAttachment;
    }

    public void setAlbumAttachment(String albumAttachment) {
        this.albumAttachment = albumAttachment;
    }
}
