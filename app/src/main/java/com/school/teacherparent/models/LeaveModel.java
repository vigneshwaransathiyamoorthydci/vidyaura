package com.school.teacherparent.models;

import java.util.List;

public class LeaveModel {
    public String message;
    public String statusText;
    public Integer status;
    public String token;
    public List<LeaveDetailsList> leaveDetailsList = null;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<LeaveDetailsList> getLeaveDetailsList() {
        return leaveDetailsList;
    }

    public void setLeaveDetailsList(List<LeaveDetailsList> leaveDetailsList) {
        this.leaveDetailsList = leaveDetailsList;
    }

    public class LeaveDetailsList {

        public Integer leaveID;
        public String From_date;
        public String To_date;
        public String leaveReason;
        public String Leave_status;
        public String Description;
        public String Parents_id;
        public parentDetails parentDetails;
        public studentDetails studentDetails;
        public String parentEncryptedID;
        public String created_at;

        public Integer getLeaveID() {
            return leaveID;
        }

        public void setLeaveID(Integer leaveID) {
            this.leaveID = leaveID;
        }

        public String getFrom_date() {
            return From_date;
        }

        public void setFrom_date(String from_date) {
            From_date = from_date;
        }

        public String getTo_date() {
            return To_date;
        }

        public void setTo_date(String to_date) {
            To_date = to_date;
        }

        public String getLeaveReason() {
            return leaveReason;
        }

        public void setLeaveReason(String leaveReason) {
            this.leaveReason = leaveReason;
        }

        public String getLeave_status() {
            return Leave_status;
        }

        public void setLeave_status(String leave_status) {
            Leave_status = leave_status;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getParents_id() {
            return Parents_id;
        }

        public void setParents_id(String parents_id) {
            Parents_id = parents_id;
        }

        public parentDetails getParentDetails() {
            return parentDetails;
        }

        public void setParentDetails(parentDetails parentDetails) {
            this.parentDetails = parentDetails;
        }

        public LeaveDetailsList.studentDetails getStudentDetails() {
            return studentDetails;
        }

        public void setStudentDetails(LeaveDetailsList.studentDetails studentDetails) {
            this.studentDetails = studentDetails;
        }

        public String getParentEncryptedID() {
            return parentEncryptedID;
        }

        public void setParentEncryptedID(String parentEncryptedID) {
            this.parentEncryptedID = parentEncryptedID;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public class parentDetails {
            int parentID;
            String Fname;
            String Type;
            String Mobile;
            String Photo;
            String Fcm_key;

            public int getParentID() {
                return parentID;
            }

            public void setParentID(int parentID) {
                this.parentID = parentID;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getType() {
                return Type;
            }

            public void setType(String type) {
                Type = type;
            }

            public String getMobile() {
                return Mobile;
            }

            public void setMobile(String mobile) {
                Mobile = mobile;
            }

            public String getPhoto() {
                return Photo;
            }

            public void setPhoto(String photo) {
                Photo = photo;
            }

            public String getFcm_key() {
                return Fcm_key;
            }

            public void setFcm_key(String fcm_key) {
                Fcm_key = fcm_key;
            }
        }

        public class studentDetails {

            int studID;
            String Fname;
            String Lname;
            String Student_photo;
            String Alt_name;
            String ClassName;
            int sectionID;
            String Section;
            String Class_image;

            public int getStudID() {
                return studID;
            }

            public void setStudID(int studID) {
                this.studID = studID;
            }

            public String getFname() {
                return Fname;
            }

            public void setFname(String fname) {
                Fname = fname;
            }

            public String getLname() {
                return Lname;
            }

            public void setLname(String lname) {
                Lname = lname;
            }

            public String getStudent_photo() {
                return Student_photo;
            }

            public void setStudent_photo(String student_photo) {
                Student_photo = student_photo;
            }

            public String getAlt_name() {
                return Alt_name;
            }

            public void setAlt_name(String alt_name) {
                Alt_name = alt_name;
            }

            public String getClassName() {
                return ClassName;
            }

            public void setClassName(String className) {
                ClassName = className;
            }

            public int getSectionID() {
                return sectionID;
            }

            public void setSectionID(int sectionID) {
                this.sectionID = sectionID;
            }

            public String getSection() {
                return Section;
            }

            public void setSection(String section) {
                Section = section;
            }

            public String getClass_image() {
                return Class_image;
            }

            public void setClass_image(String class_image) {
                Class_image = class_image;
            }
        }

    }

}