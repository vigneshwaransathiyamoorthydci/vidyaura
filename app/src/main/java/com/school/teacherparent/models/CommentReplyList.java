package com.school.teacherparent.models;

public class CommentReplyList {

    private int Id;
    private int Feed_id;
    private String Comment;
    private String Fname;
    private String Lname;
    private String Emp_photo;
    private String Publish_date;
    private String Type;

    public CommentReplyList(int id, int feed_id, String comment, String fname, String lname, String emp_photo, String publish_date) {
        Id = id;
        Feed_id = feed_id;
        Comment = comment;
        Fname = fname;
        Lname = lname;
        Emp_photo = emp_photo;
        Publish_date = publish_date;
    }

    public CommentReplyList(int id, int feed_id, String comment, String fname, String lname, String emp_photo, String publish_date, String type) {
        Id = id;
        Feed_id = feed_id;
        Comment = comment;
        Fname = fname;
        Lname = lname;
        Emp_photo = emp_photo;
        Publish_date = publish_date;
        Type = type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getFeed_id() {
        return Feed_id;
    }

    public void setFeed_id(int feed_id) {
        Feed_id = feed_id;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getEmp_photo() {
        return Emp_photo;
    }

    public void setEmp_photo(String emp_photo) {
        Emp_photo = emp_photo;
    }

    public String getPublish_date() {
        return Publish_date;
    }

    public void setPublish_date(String publish_date) {
        Publish_date = publish_date;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
