package com.school.teacherparent.models;

import java.util.ArrayList;

public class AttendanceList {

    boolean isGroupAttendance;
    int classID;
    String ClassName;
    int sectionID;
    String Section;
    String totalStudentsCount;
    String studentsPresentCount;
    String studentsAbsentCount;
    String iconName;
    private String Class_image;
    private int attendanceSessionCount;
    ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList;

    int Group_id;
    //String iconName;
    String groupimage;
    //int attendanceSessionCount;
    //String totalStudentsCount;
    //String studentsPresentCount;
    //String studentsAbsentCount;
    //ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList;


    public AttendanceList(boolean isGroupAttendance, int classID, String className, int sectionID, String section, String totalStudentsCount, String studentsPresentCount, String studentsAbsentCount, String iconName, String class_image, int attendanceSessionCount, ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList, int group_id, String groupimage) {
        this.isGroupAttendance = isGroupAttendance;
        this.classID = classID;
        ClassName = className;
        this.sectionID = sectionID;
        Section = section;
        this.totalStudentsCount = totalStudentsCount;
        this.studentsPresentCount = studentsPresentCount;
        this.studentsAbsentCount = studentsAbsentCount;
        this.iconName = iconName;
        Class_image = class_image;
        this.attendanceSessionCount = attendanceSessionCount;
        this.todaysAbsenteesList = todaysAbsenteesList;
        Group_id = group_id;
        this.groupimage = groupimage;
    }

    public boolean isGroupAttendance() {
        return isGroupAttendance;
    }

    public void setGroupAttendance(boolean groupAttendance) {
        isGroupAttendance = groupAttendance;
    }

    public int getClassID() {
        return classID;
    }

    public void setClassID(int classID) {
        this.classID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public int getSectionID() {
        return sectionID;
    }

    public void setSectionID(int sectionID) {
        this.sectionID = sectionID;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public String getTotalStudentsCount() {
        return totalStudentsCount;
    }

    public void setTotalStudentsCount(String totalStudentsCount) {
        this.totalStudentsCount = totalStudentsCount;
    }

    public String getStudentsPresentCount() {
        return studentsPresentCount;
    }

    public void setStudentsPresentCount(String studentsPresentCount) {
        this.studentsPresentCount = studentsPresentCount;
    }

    public String getStudentsAbsentCount() {
        return studentsAbsentCount;
    }

    public void setStudentsAbsentCount(String studentsAbsentCount) {
        this.studentsAbsentCount = studentsAbsentCount;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getClass_image() {
        return Class_image;
    }

    public void setClass_image(String class_image) {
        Class_image = class_image;
    }

    public int getAttendanceSessionCount() {
        return attendanceSessionCount;
    }

    public void setAttendanceSessionCount(int attendanceSessionCount) {
        this.attendanceSessionCount = attendanceSessionCount;
    }

    public ArrayList<AttendanceListResponse.todaysAbsenteesList> getTodaysAbsenteesList() {
        return todaysAbsenteesList;
    }

    public void setTodaysAbsenteesList(ArrayList<AttendanceListResponse.todaysAbsenteesList> todaysAbsenteesList) {
        this.todaysAbsenteesList = todaysAbsenteesList;
    }

    public int getGroup_id() {
        return Group_id;
    }

    public void setGroup_id(int group_id) {
        Group_id = group_id;
    }

    public String getGroupimage() {
        return groupimage;
    }

    public void setGroupimage(String groupimage) {
        this.groupimage = groupimage;
    }
}
