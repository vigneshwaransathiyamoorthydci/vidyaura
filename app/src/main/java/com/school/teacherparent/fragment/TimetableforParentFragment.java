package com.school.teacherparent.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.adapter.TimeTableAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.calender.DayDateMonthYearModel;
import com.school.teacherparent.calender.HorizontalCalendarListener;
import com.school.teacherparent.calender.HorizontalCalendarView;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.models.TimetableParms;
import com.school.teacherparent.models.TimetablewithBreak;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.calender.HorizontalCalendarView.tv_month;

/**
 * Created by harini on 9/26/2018.
 */

public class TimetableforParentFragment extends BaseFragment implements HorizontalCalendarListener,
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    RecyclerView recyclerView;

    int offset = 0;
    SwipeRefreshLayout timelineswipelayout;
    CalendarView calender_view;
    private TimeTableAdapter mAdapter;
    private List<TimeTableModel.TimetableList> timeList = new ArrayList<>();
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    TextView calendardayTextview;
    Calendar calendar;
    String formattedDate;
    Date c;
    String selectedCalenderDate;
    ArrayList<TimetablewithBreak>  timetablewithBreakArrayList;
    DiscreteScrollView dsvStudentList;
    ChildListResponseResponse childListResponseResponse;
    int count=0;
    ArrayList<ChildListResponseResponse.parentsStudList>  parentsStudListArrayList;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    private int mYear, mMonth, mDay, mHour, mMinute,studentID,selectedSchoolID;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {

        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            currentPage = 0;

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    String daynam;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_timetable_parent, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.timetabel));
        SidemenuDetailActivity.calendarImageview.setVisibility(View.VISIBLE);

        recyclerView = view.findViewById(R.id.timetable_recycle);
        calendardayTextview=view.findViewById(R.id.calendardayTextview);
        mAdapter = new TimeTableAdapter(timeList,getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        timetablewithBreakArrayList=new ArrayList<>();
        c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        final Calendar calendarr = Calendar.getInstance();
        mYear = calendarr.get(Calendar.YEAR);
        mMonth = calendarr.get(Calendar.MONTH);
        mDay = calendarr.get(Calendar.DAY_OF_MONTH);


        final DateFormat dateFormat = new SimpleDateFormat("MMMM-EEE-yyyy-MM-dd");
        final String currentDate=dateFormat.format(new Date());

        calendar = Calendar.getInstance();

        calendardayTextview.setText("Today");

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        SidemenuDetailActivity.calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ondateselect", "onDateSelected: " );

                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                String today = formt.format(c);
                                if (formattedDate.equals(today)) {
                                    calendardayTextview.setText("Today");
                                } else {
                                    calendardayTextview.setText(getDateFormatforatten(formattedDate));
                                }

                                Date date = null;
                                try {
                                    date = (Date)df.parse(formattedDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                daynam = (String) android.text.format.DateFormat.format("EEEE", date.getTime());
                                selecteddayname(daynam);




                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();


            }
        });


        Date date = calendar.getTime();
         daynam=(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        //selecteddayname(daynam);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        parentsStudListArrayList=new ArrayList<>();
        getStudentList();
        return view;
    }

    @Override
    public void updateMonthOnScroll(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
    }

    @Override
    public void newDateSelected(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
        String str_date=selectedDate.date+"-"+selectedDate.monthNumeric+"-"+selectedDate.year;
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = (Date)formatter.parse(str_date);
        String dayname = (String) android.text.format.DateFormat.format("EEEE", date.getTime());
        selecteddayname(dayname);
        String selected = (String) android.text.format.DateFormat.format("MMMM dd", date.getTime());
        String today = (String) android.text.format.DateFormat.format("MMMM dd", calendar.getTime());
        if (selected.equals(today)) {
            calendardayTextview.setText("Today");
        } else {
            calendardayTextview.setText(selected);
        }
    }

    public void selecteddayname(String dayname) {
        switch (dayname) {
            case "Monday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.MON);

                break;
            case "Tuesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.TUES);

                break;
            case "Wednesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.WED);

                break;
            case "Thursday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.THURS);

                break;
            case "Friday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.FRI);

                break;
            case "Saturday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SAT);

                break;
            case "Sunday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SUN);

                break;
        }
    }


    public void getTimetableList(String dayname) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = false;
            TimetableParms timetableParms = new TimetableParms();
            timetableParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            timetableParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            timetableParms.setSchoolID(String.valueOf(selectedSchoolID));
            timetableParms.setTimetableDay(dayname);
            timetableParms.setStudID(studentID);
            timetablewithBreakArrayList=new ArrayList<>();
            Gson gson = new Gson();
            String input = gson.toJson(timetableParms);

            vidyauraAPI.getTimetableList(timetableParms).enqueue(new Callback<TimeTableModel>() {
                @Override
                public void onResponse(Call<TimeTableModel> call, Response<TimeTableModel> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                        TimeTableModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getTimetableList() != null &&
                                        response.body().getTimetableList().size() != 0) {
                                    recyclerView.setVisibility(View.VISIBLE);


                                   // timeList.addAll(data.getTimetableList());


                                    List<String> l = new ArrayList<String>();
                                    for (int i=0;i<data.getTimetableList().size();i++)
                                    {
                                        //l.add(data.getTimetableList().get(i).getBreakTiming().get(0).getStart_time());
                                        if (data.getTimetableList().get(i).getBreakTiming() != null) {
                                            if (data.getTimetableList().get(i).getBreakTiming().size() > 0) {
                                                timeList.add(data.getTimetableList().get(i));
                                            }
                                        }
                                    }



                                    Collections.sort(timeList, new Comparator() {
                                        @Override
                                        public int compare(Object o1, Object o2) {
                                            TimeTableModel.TimetableList p1 = (TimeTableModel.TimetableList) o1;
                                            TimeTableModel.TimetableList p2 = (TimeTableModel.TimetableList) o2;

                                            try {
                                                return new SimpleDateFormat("hh:mm a").parse(p1.getBreakTiming().get(0).getStart_time()).compareTo
                                                        (new SimpleDateFormat("hh:mm a").parse(p2.getBreakTiming().get(0).getStart_time()));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                                return 0;
                                            // return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));

                                        }

                                    });
                                    mAdapter.notifyDataSetChanged();

//                                    Collections.sort(l, new Comparator<String>() {
//
//                                        @Override
//                                        public int compare(String o1, String o2) {
//                                            try {
//                                                return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
//                                            } catch (ParseException e) {
//                                                return 0;
//                                            }
//                                        }
//                                    });
//                                    System.out.println(l);


                                    // loading = true;
                                } else {
                                    timeList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    // loading = false;
                                }
                            } else {
                                timeList.clear();
                                mAdapter.notifyDataSetChanged();
                                //  if (!loading) {
                                Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                // }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        timeList.clear();
                        mAdapter.notifyDataSetChanged();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TimeTableModel> call, Throwable t) {
                    timeList.clear();
                    mAdapter.notifyDataSetChanged();
                    hideProgress();
                    isLoading = true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));

            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size()>0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setupViewPager();


                            }
                            else
                            {
                                Toast.makeText(getActivity(),childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            } }
                            else
                            {
                                Toast.makeText(getActivity(),childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }


                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();

                            getStudentList();
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void setupViewPager() {

        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
        } else {
            dsvStudentList.scrollToPosition(0);
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());


    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            studentID=parentsStudListArrayList.get(adapterPosition).getStudID();
            selectedSchoolID=parentsStudListArrayList.get(adapterPosition).getSchool_id();
            selecteddayname(daynam);

        }
    }


}
