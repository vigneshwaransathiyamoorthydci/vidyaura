package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.PollsListAdapter;
import com.school.teacherparent.adapter.PollsViewListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.PollsListParms;
import com.school.teacherparent.models.PollsResultListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import org.w3c.dom.Text;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PollsViewFragment extends Fragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    RecyclerView rvPollsViewList;
    TextView tvNoPollsQn,tvPollsTitle;
    Button btnSubmit;
    PollsViewListAdapter pollsViewListAdapter;
    ArrayList<PollsResultListResponse.getPolls.pollList> pollsListParmsArrayList = new ArrayList<>();
    PollsResultListResponse pollsResultListResponse;
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polls_view, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.polls));
        rvPollsViewList = view.findViewById(R.id.rv_polls_viewList);
        tvNoPollsQn = view.findViewById(R.id.tv_nopollsqn);
        tvPollsTitle = view.findViewById(R.id.tv_polls_title);

        btnSubmit = view.findViewById(R.id.btn_submit);

        rvPollsViewList.setHasFixedSize(true);
        rvPollsViewList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        pollsViewListAdapter = new PollsViewListAdapter(getContext(), pollsListParmsArrayList, PollsViewFragment.this);
        //pollsListAdapter = new PollsListAdapter(getContext(),pollsListParmsArrayList);
        rvPollsViewList.setAdapter(pollsViewListAdapter);
        pollsViewListAdapter.notifyDataSetChanged();
        int selectedPollsCategoryID = 0;
        bundle = this.getArguments();
        if (bundle != null) {
            selectedPollsCategoryID = bundle.getInt("SelectedPollsCategoryID", 0);
            System.out.println("SelectedPollsCategoryID ==> " + selectedPollsCategoryID);
        }
        getPollsList(selectedPollsCategoryID);
    }
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }


    public void hideProgress() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
        }
    }

    private void getPollsList(int id) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            PollsListParms pollsListParms = new PollsListParms();
            pollsListParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            pollsListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            pollsListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getPollsList(pollsListParms).enqueue(new Callback<PollsResultListResponse>() {
                @Override
                public void onResponse(Call<PollsResultListResponse> call, Response<PollsResultListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        pollsListParmsArrayList.clear();
                        pollsResultListResponse = response.body();
                        System.out.println("polls response ==> "+gson.toJson(response.body()));
                        if (pollsResultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (pollsResultListResponse.getPolls.size() > 0) {
                                for (int i=0;i<pollsResultListResponse.getPolls.size();i++) {
                                    if(pollsResultListResponse.getPolls.get(i).getPoll_category_id() == id){
                                        tvPollsTitle.setText(pollsResultListResponse.getPolls.get(i).getCategoryName() );
                                        pollsListParmsArrayList.addAll(pollsResultListResponse.getPolls.get(i).getPollList());
                                    }

                                }
                                if (pollsListParmsArrayList.size() > 0) {

                                    rvPollsViewList.setVisibility(View.VISIBLE);
                                    tvNoPollsQn.setVisibility(View.GONE);
                                    pollsViewListAdapter.notifyDataSetChanged();
                                } else {
                                    rvPollsViewList.setVisibility(View.GONE);
                                    tvNoPollsQn.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Toast.makeText(getContext(), pollsResultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            hideProgress();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<PollsResultListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            hideProgress();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void updatePolls(int pollId,int pollOptionId,int School_id) {
        if (Util.isNetworkAvailable()) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(getContext());
            }
            builder.setTitle("Confirmation")
                    .setMessage("Are you sure you want to poll the question?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            showProgress();
                            PollsListParms pollsListParms = new PollsListParms();
                            pollsListParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                            pollsListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                            pollsListParms.setSchoolID(String.valueOf(School_id));
                            pollsListParms.setPollsID(pollId);
                            pollsListParms.setPollsOptionsID(pollOptionId);

                            vidyauraAPI.updatePolls(pollsListParms).enqueue(new Callback<PollsResultListResponse>() {
                                @Override
                                public void onResponse(Call<PollsResultListResponse> call, Response<PollsResultListResponse> response) {
                                    hideProgress();
                                    if (response.body() != null) {
                                        Gson gson = new Gson();
                                        pollsResultListResponse = response.body();
                                        System.out.println("polls update response ==> "+gson.toJson(response.body()));
                                        if (pollsResultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                                            if (bundle != null) {
                                                getPollsList( bundle.getInt("SelectedPollsCategoryID", 0));
                                            }
                                        } else {
                                            hideProgress();
                                        }
                                    } else {

                                    }
                                }

                                @Override
                                public void onFailure(Call<PollsResultListResponse> call, Throwable t) {
                                    hideProgress();
                                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            hideProgress();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void performAction() {

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

       /* btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<PollsListParms> arrayList = getData();
                String message = "";
// get the value of selected answers from custom adapter
                for (int i = 0; i < arrayList.size(); i++) {
                    message = message + "\n" + arrayList.get(i).getId() + " " + arrayList.get(i).getOpt();
                }
// display the message on screen with the help of Toast.
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                *//*PollsViewListAdapter.selectedAnswers.clear();
                PollsViewListAdapter.stringArrayList.clear();*//*

            }
        });*/
    }
}
