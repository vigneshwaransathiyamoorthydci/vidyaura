package com.school.teacherparent.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddAttenAdapter;
import com.school.teacherparent.adapter.SpinAdapter;
import com.school.teacherparent.adapter.SubAttendanceAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddAssignmentParams;

import com.school.teacherparent.R;
import com.school.teacherparent.models.AddAttendanceParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AttendancedetailsParams;
import com.school.teacherparent.models.AttendancedetailsResponse;
import com.school.teacherparent.models.ClassGroupList;
import com.school.teacherparent.models.ClassGroupListResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.StudentList;
import com.school.teacherparent.models.StudentListResponse;
import com.school.teacherparent.models.StudentListbyclassParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddAttendanceFragment extends BaseFragment {

    private Spinner class_spinner;
    private RecyclerView studentListrecyclerView;
    private TextView presentTextview,addattendanceTextview;
    private AddAttenAdapter studentListAdapter;
    FloatingActionButton addAttendance;
    ClassGroupListResponse classListResponse;
    ArrayList<ClassGroupList> classlist = new ArrayList<ClassGroupList>();
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    SpinAdapter classspinnerArray;
    int classID,sectionID,groupID;
    String formattedDate;
    StudentListResponse studentListResponse;
    public ArrayList<StudentListResponse.classwiseStudentsList> classwiseStudentsLists;
    LinearLayoutManager linearLayoutManager;
    List<StudentList> studentLists=new ArrayList<>();
    String studentID;
    String ODStudentID;
    String LeaveStudentID;
    RadioButton forenoonradiobutton,afternoonradiobutton;
    String sessionTYPE="1";
    RadioGroup radioGroup;
    boolean isSelectedGroup;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_attendance, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        studentListrecyclerView = (RecyclerView) view.findViewById(R.id.add_attendance_recycle);
        addAttendance = view.findViewById(R.id.add_attendance);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_attendance));
        forenoonradiobutton=(RadioButton)view.findViewById(R.id.forenoonradiobutton);
        afternoonradiobutton=(RadioButton)view.findViewById(R.id.afternoonradiobutton);
        afternoonradiobutton.setVisibility(View.GONE);
        forenoonradiobutton.setVisibility(View.GONE);
        radioGroup=(RadioGroup)view.findViewById(R.id.toggle);
        radioGroup.setVisibility(View.GONE);
        forenoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    forenoonradiobutton.setChecked(true);
                    afternoonradiobutton.setChecked(false);
                    sessionTYPE="1";

                }
            }
        });
        afternoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    forenoonradiobutton.setChecked(false);
                    afternoonradiobutton.setChecked(true);
                    sessionTYPE="0";
                }
            }
        });
        getClassList();
        addAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        //  studentListrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        classwiseStudentsLists=new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        studentListrecyclerView.setItemAnimator(new DefaultItemAnimator());
        studentListrecyclerView.setLayoutManager(linearLayoutManager);
        studentListAdapter = new AddAttenAdapter(studentLists,getContext());
        studentListrecyclerView.setAdapter(studentListAdapter);
        presentTextview = (TextView) view.findViewById(R.id.presentTextview);

        presentTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        if (studentLists.size()>0) {
                            Toast.makeText(getContext(),getString(R.string.markallprestnt),Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < studentLists.size(); i++) {
                                studentLists.get(i).setSelectedID(studentLists.get(i).getStudID());
                            }
                        }
                        studentListAdapter.notifyDataSetChanged();
            }
        });
        addattendanceTextview=(TextView)view.findViewById(R.id.add_attendanceTextview);
        addattendanceTextview.setVisibility(View.GONE);
        presentTextview.setVisibility(View.GONE);
        addattendanceTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentID="";
                ODStudentID="";
                LeaveStudentID="";
                if (studentLists.size()>0) {
                    for (int i = 0; i < studentLists.size(); i++) {

                        if (studentLists.get(i).getSelectedID()==studentLists.get(i).getStudID())
                        {
                            Log.d("vignesh","data"+studentLists.get(i).getFname());
                            if (studentLists.get(i).getAttendanceDetails() != null) {
                                if (studentLists.get(i).getAttendanceDetails().size() > 0) {
                                    if (studentLists.get(i).getAttendanceDetails().get(0).getAttendance_type().equals("OD")) {
                                        ODStudentID = ODStudentID.concat(String.valueOf(studentLists.get(i).getStudID())).concat(",");
                                    } else if (studentLists.get(i).getAttendanceDetails().get(0).getAttendance_type().equals("Leave")) {
                                        System.out.println("leave ==> "+studentLists.get(i).getStudID());
                                        LeaveStudentID = LeaveStudentID.concat(String.valueOf(studentLists.get(i).getStudID())).concat(",");
                                    } else {
                                        studentID = studentID.concat(String.valueOf(studentLists.get(i).getStudID())).concat(",");
                                    }
                                } else {
                                    studentID = studentID.concat(String.valueOf(studentLists.get(i).getStudID())).concat(",");
                                }
                            }
                            else
                            {
                                studentID = studentID.concat(String.valueOf(studentLists.get(i).getStudID())).concat(",");
                            }
                        }

                    }


                    if (studentID.length()>0) {
                        studentID = studentID.substring(0, studentID.length() - 1);
                        if (ODStudentID.length() > 0) {
                            ODStudentID = ODStudentID.substring(0, ODStudentID.length() - 1);
                        }
                        if (LeaveStudentID.length() > 0) {
                            LeaveStudentID = LeaveStudentID.substring(0, LeaveStudentID.length() - 1);
                        }
                        System.out.println("studentID ==> " + studentID);
                        System.out.println("ODStudentID ==> " + ODStudentID);
                        System.out.println("LeaveStudentID ==> " + LeaveStudentID);
                        if (!isSelectedGroup) {
                            addAttedancebyClasswise();
                        } else {
                            addGroupAttedance();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(), getString(R.string.selectatlestonestudent), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        classspinnerArray = new SpinAdapter(getActivity(), R.layout.spinner_item, classlist);
        //getClassList();
        classspinnerArray.setDropDownViewResource(R.layout.spinner_item_dialog);

        class_spinner.setAdapter(classspinnerArray);


        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    if (classlist.get(position).getIsGroup() == 0) {
                        classID = classlist.get(position).getClassID();
                        sectionID = classlist.get(position).getSectionID();
                        studentListrecyclerView.setAdapter(null);
                        studentListAdapter.notifyDataSetChanged();
                        presentTextview.setVisibility(View.GONE);
                        addattendanceTextview.setVisibility(View.GONE);
                        studentLists.clear();
                        afternoonradiobutton.setVisibility(View.GONE);
                        forenoonradiobutton.setVisibility(View.GONE);
                        radioGroup.setVisibility(View.GONE);
                        isSelectedGroup = false;
                        getStudentlist();
                    } else if (classlist.get(position).getIsGroup() == 1) {
                        studentLists.clear();
                        studentListAdapter.notifyDataSetChanged();
                        groupID = classlist.get(position).getGroupID();
                        getGroupStudentlist();
                        isSelectedGroup = true;
                    }

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        return view;
    }
    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add(new ClassGroupList("Select Class/Group",0,0,0,0));
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getTeachersClasses(getclassListParams).enqueue(new Callback<ClassGroupListResponse>() {
                @Override
                public void onResponse(Call<ClassGroupListResponse> call, Response<ClassGroupListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(new ClassGroupList(classListResponse.getTeachersClassesList().get(i).getClassName()+"-"+ classListResponse.getTeachersClassesList().get(i).getSection(),
                                            classListResponse.getTeachersClassesList().get(i).getIsgroup(),
                                            classListResponse.getTeachersClassesList().get(i).getClassID(),
                                            classListResponse.getTeachersClassesList().get(i).getSectionID(), 0));

                                }
                                for (int i = 0; i < classListResponse.getTeachersGroupList().size(); i++) {
                                    classlist.add(new ClassGroupList(classListResponse.getTeachersGroupList().get(i).getGroup_name(),
                                            classListResponse.getTeachersGroupList().get(i).getIsgroup(), 0, 0,
                                            classListResponse.getTeachersGroupList().get(i).getId()));
                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();

                                classspinnerArray.notifyDataSetChanged();
                                class_spinner.performClick();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassGroupListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    public void getStudentlist()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            StudentListbyclassParam addAttendanceParams=new StudentListbyclassParam();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addAttendanceParams.setClassID(String.valueOf(classID));
            addAttendanceParams.setSectionID(String.valueOf(sectionID));

            vidyauraAPI.getClasswiseStudentList(addAttendanceParams).enqueue(new Callback<StudentListResponse>() {
                @Override
                public void onResponse(Call<StudentListResponse> call, Response<StudentListResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        studentListResponse = response.body();
                        if (studentListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                            classwiseStudentsLists = response.body().getClasswiseStudentsList();
                            if (studentListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                presentTextview.setVisibility(View.VISIBLE);
                                addattendanceTextview.setVisibility(View.VISIBLE);
                                afternoonradiobutton.setVisibility(View.VISIBLE);
                                forenoonradiobutton.setVisibility(View.VISIBLE);
                                radioGroup.setVisibility(View.GONE);

                                if (studentListResponse.getClasswiseStudentsList().size()!=0) {

                                     //attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());

                                    //studentListAdapter.notifyDataSetChanged();
                                    for (int y=0;y<studentListResponse.getClasswiseStudentsList().size();y++)
                                    {
                                        studentLists.add(new StudentList(studentListResponse.getClasswiseStudentsList().get(y).getStudID(),
                                                studentListResponse.getClasswiseStudentsList().get(y).getFname(),
                                                studentListResponse.getClasswiseStudentsList().get(y).getLname(),
                                                studentListResponse.getClasswiseStudentsList().get(y).getStudent_photo(),
                                                studentListResponse.getClasswiseStudentsList().get(y).getStudID(),
                                                studentListResponse.getClasswiseStudentsList().get(y).getAttendanceDetails()));
                                    }

                                    studentListAdapter = new AddAttenAdapter(studentLists,getContext());
                                    studentListrecyclerView.setAdapter(studentListAdapter);

                                }

                            } else {
                                Toast.makeText(getContext(), studentListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<StudentListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getGroupStudentlist() {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            StudentListbyclassParam addAttendanceParams=new StudentListbyclassParam();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addAttendanceParams.setGroupID(String.valueOf(groupID));

            vidyauraAPI.getGroupwiseStudentList(addAttendanceParams).enqueue(new Callback<StudentListResponse>() {
                @Override
                public void onResponse(Call<StudentListResponse> call, Response<StudentListResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        studentListResponse = response.body();
                        if (studentListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                            if (studentListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                presentTextview.setVisibility(View.VISIBLE);
                                addattendanceTextview.setVisibility(View.VISIBLE);
                                afternoonradiobutton.setVisibility(View.VISIBLE);
                                forenoonradiobutton.setVisibility(View.VISIBLE);
                                radioGroup.setVisibility(View.GONE);

                                if (studentListResponse.getGroupwiseStudentsList().size()!=0) {

                                    //attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());

                                    //studentListAdapter.notifyDataSetChanged();
                                    for (int y=0;y<studentListResponse.getGroupwiseStudentsList().size();y++)
                                    {
                                        studentLists.add(new StudentList(studentListResponse.getGroupwiseStudentsList().get(y).getStudID(),
                                                studentListResponse.getGroupwiseStudentsList().get(y).getFname(),
                                                studentListResponse.getGroupwiseStudentsList().get(y).getLname(),
                                                studentListResponse.getGroupwiseStudentsList().get(y).getStudent_photo(),
                                                studentListResponse.getGroupwiseStudentsList().get(y).getStudID(),
                                                studentListResponse.getGroupwiseStudentsList().get(y).getAttendanceDetails()));
                                    }

                                    studentListAdapter = new AddAttenAdapter(studentLists,getContext());
                                    studentListrecyclerView.setAdapter(studentListAdapter);

                                }

                            } else {
                                Toast.makeText(getContext(), studentListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<StudentListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void addAttedancebyClasswise()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AddAttendanceParams addAttendanceParams=new AddAttendanceParams();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addAttendanceParams.setClassID(String.valueOf(classID));
            addAttendanceParams.setSectionID(String.valueOf(sectionID));
            addAttendanceParams.setStudentID(studentID);
            addAttendanceParams.setODStudentID(ODStudentID);
            addAttendanceParams.setLeaveStudentID(LeaveStudentID);
            addAttendanceParams.setAttendanceDate(formattedDate);
            addAttendanceParams.setSessionType(sessionTYPE);

            vidyauraAPI.addAttendance(addAttendanceParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        AddFeedResponse feedResponse = response.body();
                        if (feedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void addGroupAttedance()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AddAttendanceParams addAttendanceParams=new AddAttendanceParams();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addAttendanceParams.setGroupID(String.valueOf(groupID));
            addAttendanceParams.setStudentID(studentID);
            addAttendanceParams.setODStudentID(ODStudentID);
            addAttendanceParams.setLeaveStudentID(LeaveStudentID);
            addAttendanceParams.setAttendanceDate(formattedDate);
            addAttendanceParams.setSessionType(sessionTYPE);

            vidyauraAPI.addGroupAttendance(addAttendanceParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        AddFeedResponse feedResponse = response.body();
                        if (feedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

}
