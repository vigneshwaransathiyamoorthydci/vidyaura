package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ClassAdapter;
import com.school.teacherparent.adapter.HomeWorkDetailsAdapter;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.models.HomeDetailsResponse;
import com.school.teacherparent.models.HomeworkdetailsParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.HomeWorkFragment.selectedSchool;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeworkDetailsFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private RecyclerView recyclerView;
    private HomeWorkDetailsAdapter homeWorkDetailsAdapter;

    FloatingActionButton addClasswork;
    ImageView noti, back;


    SwipeRefreshLayout classworkswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    HomeDetailsResponse homeDetailsResponse;
    ArrayList<HomeDetailsResponse.homeworkDetailsList> homeworkDetailsListArrayList = new ArrayList<HomeDetailsResponse.homeworkDetailsList>();
    BaseActivity baseActivity;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView selectedDateTextview;
    String formattedDate;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        currentPage = 0;
        lastEnd = false;

            if (isLoading) {
                getHomeWorkDetails();
            }



    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    String className,homeWorkID;
    int classID,sectionID,subjectID,selectedSchoolID;
    int studID = 0;


    int limit=5;
    int offset=0;

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            isStarted = true;
            currentPage = 0;
            if (isLoading) {
                getHomeWorkDetails();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_homework_details, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        baseActivity = (BaseActivity) getActivity();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            className = bundle.getString("className");
            homeWorkID=bundle.getString("homeWorkID");
            classID=bundle.getInt("classID");
            sectionID=bundle.getInt("sectionID");
            subjectID=bundle.getInt("subjectID");
            studID=bundle.getInt("studID");
            selectedSchoolID=bundle.getInt("selectedSchoolID");

            System.out.println("homework ==> "+homeWorkID);
            System.out.println("homework ==> "+classID);
            System.out.println("homework ==> "+sectionID);
            System.out.println("homework ==> "+subjectID);
            System.out.println("homework ==> "+studID);

        }
        SidemenuDetailActivity.title.setText(className.replace("Class Class","Class"));
        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_fragment_class);
        addClasswork = view.findViewById(R.id.add_classwork);

        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
            }
        });
        //homeworkDetailsListArrayList=new ArrayList<>();
        homeWorkDetailsAdapter = new HomeWorkDetailsAdapter(homeworkDetailsListArrayList,getContext(),baseActivity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(homeWorkDetailsAdapter);
        classworkswipelayout=view.findViewById(R.id.swipe_classwork);

        classworkswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                classworkswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                            getHomeWorkDetails();
                            classworkswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });
        selectedDateTextview=(TextView)view.findViewById(R.id.selectedDateTextview);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(date);
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        selectedDateTextview.setText(getString(R.string.today));
        SidemenuDetailActivity.calendarImageview.setVisibility(View.VISIBLE);
        SidemenuDetailActivity.calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                String today = formt.format(c);
                                if (formattedDate.equals(today)) {
                                    selectedDateTextview.setText("Today");
                                } else {
                                    selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                }
                                //homeworkDetailsListArrayList.clear();

                                homeWorkDetailsAdapter.notifyDataSetChanged();
                               getHomeWorkDetails();



                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
       // getHomeWorkDetails();
        return view;

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }


    public void getHomeWorkDetails() {
        if (Util.isNetworkAvailable()) {
            if (homeworkDetailsListArrayList!=null) {
                homeworkDetailsListArrayList.clear();
            }
            showProgress();
            HomeworkdetailsParams homeworkdetailsParams = new HomeworkdetailsParams();
            homeworkdetailsParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            homeworkdetailsParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                homeworkdetailsParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                homeworkdetailsParams.setSchoolID(String.valueOf(selectedSchoolID));
            }
            homeworkdetailsParams.setHomeworkID(homeWorkID);
            homeworkdetailsParams.setClassID(String.valueOf(classID));
            homeworkdetailsParams.setSectionID(String.valueOf(sectionID));
            homeworkdetailsParams.setSubjectID(String.valueOf(subjectID));
            homeworkdetailsParams.setAssignedON(formattedDate);
            homeworkdetailsParams.setClassImage(Constants.CLASSIMAGE);
            homeworkdetailsParams.setStudID(studID);
            isLoading=false;
            vidyauraAPI.getHomeworkDetailsList(homeworkdetailsParams).enqueue(new Callback<HomeDetailsResponse>() {

                @Override
                public void onResponse(Call<HomeDetailsResponse> call, Response<HomeDetailsResponse> response) {
                    hideProgress();
                    isLoading=true;
                    homeDetailsResponse = response.body();
                    if (homeDetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                        //homeworkDetailsListArrayList.clear();
                        homeworkDetailsListArrayList = response.body().getHomeworkDetailsList();
                        if (homeDetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                            if (homeworkDetailsListArrayList.size()!=0) {
//                                feeds_recycle.setVisibility(View.VISIBLE);
//                                nofeedListTextview.setVisibility(View.GONE);
                                homeWorkDetailsAdapter = new HomeWorkDetailsAdapter(homeworkDetailsListArrayList,getContext(),baseActivity);
                                recyclerView.setAdapter(homeWorkDetailsAdapter);
                                homeWorkDetailsAdapter.notifyDataSetChanged();

                            }
//                            else if (feedList.size()!=0 && feedListResponsearray.size()!=0)
//                            {
//                                nofeedListTextview.setVisibility(View.VISIBLE);
//                                feeds_recycle.setVisibility(View.GONE);
//                                loading=false;
//                            }
                        } else {
                            Toast.makeText(getContext(), homeDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        getActivity().finishAffinity();

                    }
                }

                @Override
                public void onFailure(Call<HomeDetailsResponse> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

}
