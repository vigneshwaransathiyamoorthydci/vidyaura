package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.ParentCircularActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.adapter.NewsImageSliderAdapter;
import com.school.teacherparent.adapter.SchoolTestimonalAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.SchoolDetailsModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.AutoScrollViewPager;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 10/12/2018.
 */

public class SchoolFragment extends BaseFragment  implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {
    private static ViewPager mPager;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    SliderLayout sliderLayout;
    CardView testimonial_card;
    List<String> slideImages = new ArrayList<>();
    private TextView top_textView, content_textView, tesimonialContent, testmonial_name, testmonial_postion,priTextview;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    private Spannable.Factory spannableFactory;
    private TextView detail_text, detail,tv_name,tv_post;
    RecyclerView schoolRecyclerview;
    SchoolTestimonalAdapter schoolTestimonalAdapter;
    LinearLayoutManager linearLayoutManager;
    CircularImageView img;
    DiscreteScrollView dsvStudentList;
    CardView cv_student;
    ProgressDialog mDialog;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList = new ArrayList<>();
    int classID, studentID, sectionID,studentSchoolID;
    ArrayList<SchoolDetailsModel.testimonials> circularListResponseArrayList = new ArrayList<>();
    private int currentPage = 0;
    private int count = 0;
    AutoScrollViewPager viewPager;

    Timer timer;
    final long DELAY_MS = 3000;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000; // time in milliseconds between successive task executions.

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }


        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "Less", false);
                    } else {
                        makeTextViewResizable(tv, 3, "More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    ArrayList<SchoolDetailsModel.SchoolDetail> schoolDetails=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_school, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.school));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();


        detail_text = view.findViewById(R.id.detail_text);
        img = view.findViewById(R.id.img);
        tv_name = view.findViewById(R.id.tv_name);
        tv_post = view.findViewById(R.id.tv_post);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        schoolRecyclerview=view.findViewById(R.id.schoolRecyclerview);
        schoolRecyclerview.setNestedScrollingEnabled(false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        schoolRecyclerview.setItemAnimator(new DefaultItemAnimator());
        schoolRecyclerview.setLayoutManager(linearLayoutManager);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        cv_student = view.findViewById(R.id.cv_student);
        viewPager = view.findViewById(R.id.viewPager);

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        priTextview=(TextView)view.findViewById(R.id.priTextview);

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
            cv_student.setVisibility(View.GONE);
            getSchooldetails();
        } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
            cv_student.setVisibility(View.VISIBLE);
            getStudentList();
        }
        return view;
    }

    /*
        private void init() {
            for(int i=0;i<IMAGES.length;i++)
                ImagesArray.add(IMAGES[i]);



            mPager.setAdapter(new SlidingImageAdapter(getActivity(),ImagesArray));

            final float density = getResources().getDisplayMetrics().density;


            NUM_PAGES =IMAGES.length;

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000, 3000);

        }
    */
    public void getSchooldetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            FeedListParams feedListParams = new FeedListParams();
            //  feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            //  feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            //feedListParams.setLimit(Constants.DATALIMIT);
            //feedListParams.setOffset(offset);
            Gson gson = new Gson();
            schoolDetails.clear();
            circularListResponseArrayList.clear();
            detail_text.setText("");
            priTextview.setText("");
           /* slideImages.clear();*/
            String input = gson.toJson(feedListParams);
            vidyauraAPI.getSchoolDetail(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)),String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0))).
                    enqueue(new Callback<SchoolDetailsModel>() {
                        @Override
                        public void onResponse(Call<SchoolDetailsModel> call, Response<SchoolDetailsModel> response) {
                            hideProgress();

                            if (response.body() != null) {
                                SchoolDetailsModel data = response.body();
                                if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                                    Gson gson = new Gson();
                                    String res = gson.toJson(response.body());
                                    Log.d("resdata", "onResponse: " + res);


                                    if (data.getStatus() == Util.STATUS_SUCCESS) {
                                        //  detail.setText(data.getSchoolDetails().get(0).getTestimonial());

//                                        if (data.getSchoolDetails().get(0).getTestimonial() != null &&
//                                                !data.getSchoolDetails().get(0).getTestimonial().equals("")) {
//                                            //testimonial_card.setVisibility(View.VISIBLE);
//                                            //tesimonialContent.setText(data.getSchoolDetails().get(0).getTestimonial());
//                                            makeTextViewResizable(tesimonialContent, 3, "more", true);
//                                        }

                                        //testmonial_name.setText(data.getSchoolDetails().get(0).getTestimonial_name());
                                        detail_text.setText(data.getSchoolDetails().get(0).getDescription());
                                        priTextview.setText(data.getSchoolDetails().get(0).getPrincipal_message());
                                        if (data.getSchoolDetails().get(0).getPrincipal_name() != null) {
                                            tv_name.setText(data.getSchoolDetails().get(0).getPrincipal_name());
                                        } else {
                                            tv_name.setText(data.getSchoolDetails().get(0).getSchool_name());
                                        }

                                        String school = getString(R.string.s3_baseurl) + getString(R.string.s3_schools);
                                        String prins = getString(R.string.s3_baseurl) + getString(R.string.s3_employee);
                                        if (data.getSchoolDetails().get(0).getPrincipal_image() != null) {
                                            Glide.with(getActivity()).load(school+data.getSchoolDetails().get(0).getPrincipal_image()).into(img);
                                        } else {
                                            Glide.with(getActivity()).load(school+data.getSchoolDetails().get(0).getLogo()).into(img);
                                        }

                                        makeTextViewResizable(priTextview, 3, "more", true);



                                        //detail.setText(data.getSchoolDetails().get(0).getPrincipal_message());
                                        //testmonial_postion.setText(data.getSchoolDetails().get(0).getTestimonial_position());
                                        if (data.getSchoolDetails().get(0).getAttachmentArr().size()!=0)
                                        {
                                            slideImages.clear();
                                            for (int y=0;y<data.getSchoolDetails().get(0).getAttachmentArr().size();y++)
                                            {
                                                slideImages.add(data.getSchoolDetails().get(0).getAttachmentArr().get(y));
                                            }
                                        }


                                        setSliderViews(slideImages);
                                        //makeTextViewResizable(detail, 3, "more", true);
                                        makeTextViewResizable(detail_text, 3, "more", true);
                                        schoolDetails.addAll(data.getSchoolDetails());
                                        schoolTestimonalAdapter = new SchoolTestimonalAdapter(schoolDetails.get(0).getTestimonials(),getContext());
                                        schoolRecyclerview.setAdapter(schoolTestimonalAdapter);

                                    } else {

                                        Toast.makeText(getContext(), data.getStatusText(), Toast.LENGTH_SHORT).show();


                                    }
                                } else {
                                    hideProgress();
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, data.getToken()).commit();
                                    getSchooldetails();
                                }


                            } else {
                                Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<SchoolDetailsModel> call, Throwable t) {
                            hideProgress();
                            Log.d("failll", "onFailure: " + "fail");

                            Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                        }
                    });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    public void getSchooldetailsByStudent() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            FeedListParams feedListParams = new FeedListParams();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(studentSchoolID));
            //feedListParams.setLimit(Constants.DATALIMIT);
            //feedListParams.setOffset(offset);
            Gson gson = new Gson();
            schoolDetails.clear();
            circularListResponseArrayList.clear();
            setSliderViews(null);

            //setSliderViews(null);
            String input = gson.toJson(feedListParams);
            vidyauraAPI.getSchoolDetail(String.valueOf(studentSchoolID),
                    String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0))).
                    enqueue(new Callback<SchoolDetailsModel>() {
                        @Override
                        public void onResponse(Call<SchoolDetailsModel> call, Response<SchoolDetailsModel> response) {
                            mDialog.dismiss();

                            if (response.body() != null) {
                                SchoolDetailsModel data = response.body();
                                if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                                    Gson gson = new Gson();
                                    String res = gson.toJson(response.body());
                                    Log.d("resdata", "onResponse: " + res);


                                    if (data.getStatus() == Util.STATUS_SUCCESS) {
                                        //  detail.setText(data.getSchoolDetails().get(0).getTestimonial());

//                                        if (data.getSchoolDetails().get(0).getTestimonial() != null &&
//                                                !data.getSchoolDetails().get(0).getTestimonial().equals("")) {
//                                            //testimonial_card.setVisibility(View.VISIBLE);
//                                            //tesimonialContent.setText(data.getSchoolDetails().get(0).getTestimonial());
//                                            makeTextViewResizable(tesimonialContent, 3, "more", true);
//                                        }

                                        //testmonial_name.setText(data.getSchoolDetails().get(0).getTestimonial_name());
                                        detail_text.setText(data.getSchoolDetails().get(0).getDescription());
                                        priTextview.setText(data.getSchoolDetails().get(0).getPrincipal_message());
                                        if (data.getSchoolDetails().get(0).getPrincipal_name() != null) {
                                            tv_name.setText(data.getSchoolDetails().get(0).getPrincipal_name());
                                        } else {
                                            tv_name.setText(data.getSchoolDetails().get(0).getSchool_name());
                                        }

                                        String school = getString(R.string.s3_baseurl) + getString(R.string.s3_schools);
                                        String prins = getString(R.string.s3_baseurl) + getString(R.string.s3_employee);
                                        if (data.getSchoolDetails().get(0).getPrincipal_image() != null) {
                                            Glide.with(getActivity()).load(school+data.getSchoolDetails().get(0).getPrincipal_image()).into(img);
                                        } else {
                                            Glide.with(getActivity()).load(school+data.getSchoolDetails().get(0).getLogo()).into(img);
                                        }

                                        makeTextViewResizable(priTextview, 3, "more", true);



                                        //detail.setText(data.getSchoolDetails().get(0).getPrincipal_message());
                                        //testmonial_postion.setText(data.getSchoolDetails().get(0).getTestimonial_position());
                                        slideImages.clear();
                                        if (data.getSchoolDetails().get(0).getAttachmentArr().size()!=0)
                                        {
                                            for (int y=0;y<data.getSchoolDetails().get(0).getAttachmentArr().size();y++)
                                            {
                                                slideImages.add(data.getSchoolDetails().get(0).getAttachmentArr().get(y));
                                                System.out.println("sliderrrr ==> "+data.getSchoolDetails().get(0).getAttachmentArr().get(y));
                                            }
                                        }

                                        if (slideImages.size() > 0) {
                                            setSliderViews(slideImages);
                                        }
                                        //makeTextViewResizable(detail, 3, "more", true);
                                        makeTextViewResizable(detail_text, 3, "more", true);
                                        schoolDetails.addAll(data.getSchoolDetails());
                                        circularListResponseArrayList = schoolDetails.get(0).getTestimonials();
                                        schoolTestimonalAdapter = new SchoolTestimonalAdapter(circularListResponseArrayList,getContext());
                                        schoolRecyclerview.setAdapter(schoolTestimonalAdapter);
                                        schoolTestimonalAdapter.notifyDataSetChanged();
                                        mDialog.dismiss();

                                    } else {

                                        Toast.makeText(getContext(), data.getStatusText(), Toast.LENGTH_SHORT).show();


                                    }
                                } else {
                                    mDialog.dismiss();
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, data.getToken()).commit();
                                    getSchooldetailsByStudent();
                                }


                            } else {
                                Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onFailure(Call<SchoolDetailsModel> call, Throwable t) {
                            mDialog.dismiss();
                            Log.d("failll", "onFailure: " + "fail");

                            Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                        }
                    });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                                if (childListResponseResponse.getParentsStudList().size() > 0) {
                                    parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                    setDiscreteScrollViewData();
                                    getSchooldetailsByStudent();
                                } else {
                                    Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }} else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            detail_text.setText("");
            priTextview.setText("");
            getSchooldetailsByStudent();
        }
    }

    private void setSliderViews(List<String> imagelist) {

        if (imagelist != null) {
        if (imagelist.size() > 0) {

            viewPager.startAutoScroll();
            viewPager.setInterval(3000);
            viewPager.setCycle(true);
            viewPager.setStopScrollWhenTouch(true);

            PagerAdapter adapter = new NewsImageSliderAdapter(getActivity(), imagelist);
            viewPager.setAdapter(adapter);

            /*viewPager.setVisibility(View.VISIBLE);
            NewsImageSliderAdapter newsImageSliderAdapter = new NewsImageSliderAdapter(getActivity(), imagelist);
            viewPager.setAdapter(newsImageSliderAdapter);
            newsImageSliderAdapter.notifyDataSetChanged();

            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == imagelist.size()-1) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };

            timer = new Timer(); // This will create a new Thread
            timer.schedule(new TimerTask() { // task to be scheduled
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, DELAY_MS, PERIOD_MS);*/

            // Auto start of viewpager
            /*count = imagelist.size();
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == count) {
                        currentPage = 0;
                    }
                    viewPager.setCurrentItem(currentPage++, true);
                }
            };

          Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 3000,5000);*/
        } else {
            viewPager.setVisibility(View.GONE);
        }
        }




        /*sliderLayout = getView().findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(2); //set scroll delay in seconds :

        for (int i = 0; i < imagelist.size(); i++) {

            SliderView sliderView = new SliderView(getActivity());
            sliderView.setImageUrl(getString(R.string.s3_baseurl)+getString(R.string.s3_schools)+imagelist.get(i));
            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);

            //sliderView.setDescription("" + (i + 1));
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    //Toast.makeText(getActivity(), "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);*/
    }
}
