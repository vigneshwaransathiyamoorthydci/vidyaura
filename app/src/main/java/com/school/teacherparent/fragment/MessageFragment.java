package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.MessageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChatMessageParams;
import com.school.teacherparent.R;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import junit.framework.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by harini on 8/29/2018.
 */

public class MessageFragment extends Fragment {
    RecyclerView message_recycle;
    private String senderUserName;
    private String senderUserID;
    MessageAdapter messageAdapter;
    List<ChatMessageParams> message;
    FloatingActionButton newMessage;
    private DatabaseReference mFirebaseDatabaseReferenceInbox;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private DatabaseReference messagesRef;
    public String RECENT_LIST_CHILD;
    TextView text_no_network;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ProgressDialog dialog;
    //  private ArrayList<ChatMessageParams> membersReceiverList;

    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;
    ArrayList<MessageList> messageListArrayList = new ArrayList<>();
    ArrayList<MessageList> recentMessageListArrayList = new ArrayList<>();
    ArrayList<MessageList> userListArrayList = new ArrayList<>();
    ArrayList<String> stringArrayList = new ArrayList<>();
    ArrayList<String> groupArrayList = new ArrayList<>();
    ArrayList<String> count1 = new ArrayList<>();
    ArrayList<String> count2 = new ArrayList<>();
    String receiverName;
    String receiverImage;
    String messages;
    Long readStatus, state, timeStamp;
    String groupID, groupName, groupImage,receiverFcmKey,senderName;
    ArrayList<String> receiverFcmKeyy;
    ArrayList<String> receiverUserIDD;
    String sentTime;
    boolean thisGroup;
    String receiverUserID;
    ProgressDialog mDialog;
    SwipeRefreshLayout swipeRefresh;
    String id;
    Menu menu;
    NotificationReceiver notificationReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_message, container, false);
        TestDrawerActivity.toolbar.setTitle(getString(R.string.message));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        text_no_network = view.findViewById(R.id.text_no_network);
        dialog = new ProgressDialog(getActivity());

        message_recycle = view.findViewById(R.id.message_recycle);
        newMessage = view.findViewById(R.id.new_msg);
        swipeRefresh = view.findViewById(R.id.swipe_refresh);

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        message = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        message_recycle.setLayoutManager(mLayoutManager);
        message_recycle.setItemAnimator(new DefaultItemAnimator());
        messageAdapter = new MessageAdapter(recentMessageListArrayList, getActivity());
        message_recycle.setAdapter(messageAdapter);
        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        setHasOptionsMenu(true);
        getUserData();
        getchatMessageList();
        //getUserList();
        //getDataFromFirebaseDataBase();
        //swipeRefresh.performClick();
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        TestDrawerActivity.toolbar.setTitle(getString(R.string.message));
                        getchatMessageList();
                        swipeRefresh.setRefreshing(false);
                    }
                }, 000);
            }
        });

        newMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userType == 1) {
                    Fragment fragment = new ChatWithParentListFragment();
                    ((TestDrawerActivity) getActivity()).replaceFragment(fragment);
                } else if (userType == 2) {
                    Fragment fragment = new ChatWithTeacherListFragment();
                    ((TestDrawerActivity) getActivity()).replaceFragment(fragment);
                }
            }
        });

        TestDrawerActivity.edittextleaveSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(TestDrawerActivity.edittextleaveSearch);
            }
        });
        TestDrawerActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (  TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(  TestDrawerActivity.edittextleaveSearch.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        getSearchChatMessageList(TestDrawerActivity.edittextleaveSearch.getText().toString().trim().toLowerCase());
                    }

                    return true;
                }
                else
                {
                    Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                if (TestDrawerActivity.edittextleaveSearch.getText().toString().length() <= 0) {
                    TestDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    TestDrawerActivity.toolbar.setTitle("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                    TestDrawerActivity.edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    TestDrawerActivity.edittextleaveSearch.setEnabled(true);
                    TestDrawerActivity.edittextleaveSearch.requestFocus();
                    showKeyboard(TestDrawerActivity.edittextleaveSearch);
                    TestDrawerActivity.edittextleaveSearch.setText("");
                } else {
                    getSearchChatMessageList(TestDrawerActivity.edittextleaveSearch.getText().toString().trim().toLowerCase());
                }
                editor.putInt(Constants.STATE,1);
                editor.commit();
                return true;

            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;
            default:
                break;
        }

        return false;
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getchatMessageList() {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref_message = database.getReference("message");
            ref_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();
                    System.out.println("chat list single ====> "+dataSnapshot.getValue());
                    stringArrayList.clear();
                    recentMessageListArrayList.clear();
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            id = (String) child.getKey();
                            String chatID[] = id.split("to");
                            count1.clear();
                            count2.clear();
                            for (DataSnapshot child1 : child.getChildren()) {
                                String key = (String) child1.getKey();
                                thisGroup = (boolean) dataSnapshot.child(id).child(key).child("thisGroup").getValue();
                                messages = (String) dataSnapshot.child(id).child(key).child("message").getValue();
                                String readBy = (String) dataSnapshot.child(id).child(key).child("readBy").getValue();
                                readStatus = (Long) dataSnapshot.child(id).child(key).child("readStatus").getValue();
                                receiverFcmKey = (String) dataSnapshot.child(id).child(key).child("receiverFcmKey").getValue();
                                receiverFcmKeyy = (ArrayList<String>) dataSnapshot.child(id).child(key).child("receiverFcmKeyy").getValue();
                                receiverImage = (String) dataSnapshot.child(id).child(key).child("receiverImage").getValue();
                                receiverName = (String) dataSnapshot.child(id).child(key).child("receiverName").getValue();
                                receiverUserID = (String) dataSnapshot.child(id).child(key).child("receiverUserID").getValue();
                                receiverUserIDD = (ArrayList<String>) dataSnapshot.child(id).child(key).child("receiverUserIDD").getValue();
                                String senderImage = (String) dataSnapshot.child(id).child(key).child("senderImage").getValue();
                                senderName = (String) dataSnapshot.child(id).child(key).child("senderName").getValue();
                                String senderUserID = (String) dataSnapshot.child(id).child(key).child("senderUserID").getValue();
                                String senderFcmKey = (String) dataSnapshot.child(id).child(key).child("senderFcmKey").getValue();
                                sentTime = (String) dataSnapshot.child(id).child(key).child("sentTime").getValue();
                                Long state = (Long) dataSnapshot.child(id).child(key).child("state").getValue();
                                timeStamp = (Long) dataSnapshot.child(id).child(key).child("timeStamp").getValue();
                                Long unreadCount = (Long) dataSnapshot.child(id).child(key).child("unreadCount").getValue();

                                groupID = (String) dataSnapshot.child(id).child(key).child("groupID").getValue();
                                groupImage = (String) dataSnapshot.child(id).child(key).child("groupImage").getValue();
                                groupName = (String) dataSnapshot.child(id).child(key).child("groupName").getValue();

                                if (thisGroup) {
                                    if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                        count1.add(receiverUserID);
                                    }
                                } else {
                                    if (chatID[0].equals(receiverUserID) && readStatus == 0) {
                                        count2.add(receiverUserID);
                                    }
                                }
                            }

                            //here
                            if (thisGroup) {
                                if (!stringArrayList.contains(id) && receiverUserIDD.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                    stringArrayList.add(id);
                                    recentMessageListArrayList.add(new MessageList(true, groupID, groupName, groupImage, receiverName, receiverImage, receiverUserID, receiverUserIDD,
                                            receiverFcmKeyy, senderName+": "+messages, Integer.parseInt(String.valueOf(readStatus)), sentTime, count1.size(), timeStamp));
                                    messageAdapter.notifyDataSetChanged();
                                }
                            } else {
                                if (chatID[0].equals(sharedPreferences.getString(Constants.USERID, null)) && !stringArrayList.contains(id)) {
                                    stringArrayList.add(id);
                                    recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                            "", "",chatID[1], null, null, messages,
                                            Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                    messageAdapter.notifyDataSetChanged();
                                }
                            }

                        }

                            //hide//
                            /*if (thisGroup) {
                                if (Objects.requireNonNull(receiverUserID).contains(Objects.requireNonNull(sharedPreferences.getString(Constants.USERID, null)))) {
                                    count1.clear();
                                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                    final DatabaseReference ref_message = database.getReference("group_message").child(id);
                                    ref_message.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                String messageeee = null;
                                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                    String key = (String) child.getKey();
                                                    String readBy = (String) dataSnapshot.child(key).child("readBy").getValue();
                                                    String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                                                    //messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                                                    if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                                        count1.add(receiverUserID);
                                                    }
                                                }

                                                System.out.println("count1 ==> "+count1.size());
                                                if (!stringArrayList.contains(groupID)) {
                                                    stringArrayList.add(groupID);
                                                    recentMessageListArrayList.add(new MessageList(true, groupID, groupName, groupImage, receiverName, receiverImage, receiverUserID,
                                                            receiverFcmKey, messages, Integer.parseInt(String.valueOf(readStatus)), sentTime, count1.size(), timeStamp));
                                                    messageAdapter.notifyDataSetChanged();
                                                }
                                                if (recentMessageListArrayList.size() > 0) {
                                                    message_recycle.setVisibility(View.VISIBLE);
                                                    text_no_network.setVisibility(View.GONE);
                                                    messageAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            System.out.println("The read failed: " + databaseError.getCode());
                                        }
                                    });
                                }
                            } else {
                                String chatID[] = id.split("to");
                               if (chatID[0].equals(sharedPreferences.getString(Constants.USERID, null))) {
                                   count2.clear();
                                   final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                   final DatabaseReference ref_message = database.getReference("message").child(id);
                                   ref_message.addValueEventListener(new ValueEventListener() {
                                       @Override
                                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                           if (dataSnapshot.exists()) {
                                               String messageeee = null;
                                               for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                   String key = (String) child.getKey();
                                                   Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                                                   String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                                                   messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                                                   if (chatID[0].equals(receiverUserID) && readStatus == 0) {
                                                       count2.add(receiverUserID);
                                                   }
                                               }

                                               if (!stringArrayList.contains(id)) {
                                                   stringArrayList.add(id);
                                                   recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                                           "", "", chatID[1], "", messageeee,
                                                           Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                                   messageAdapter.notifyDataSetChanged();
                                               }

                                               *//*if (userListArrayList.size() > 0) {
                                                   for (int i = 0; i < userListArrayList.size(); i++) {
                                                       if (userListArrayList.get(i).getReceiverUserID().equals(chatID[1]) && !stringArrayList.contains(userListArrayList.get(i).getReceiverUserID())) {
                                                           stringArrayList.add(userListArrayList.get(i).getReceiverUserID());
                                                           recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                                                   userListArrayList.get(i).getReceiverName(), userListArrayList.get(i).getReceiverImage(),
                                                                   userListArrayList.get(i).getReceiverUserID(), userListArrayList.get(i).getReceiverFcmKey(), messages,
                                                                   Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                                       }
                                                   }
                                                   messageAdapter.notifyDataSetChanged();
                                               }*//*
                                               if (recentMessageListArrayList.size() > 0) {
                                                   message_recycle.setVisibility(View.VISIBLE);
                                                   text_no_network.setVisibility(View.GONE);
                                                   messageAdapter.notifyDataSetChanged();
                                               }
                                           }
                                       }

                                       @Override
                                       public void onCancelled(DatabaseError databaseError) {
                                           System.out.println("The read failed: " + databaseError.getCode());
                                       }
                                   });
                               }
                            }*/

                        if (recentMessageListArrayList.size() > 0) {

                            Collections.sort(recentMessageListArrayList, new Comparator<MessageList>() {
                                DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                @Override
                                public int compare(MessageList o1, MessageList o2) {
                                   /* try {
                                        DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);

                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                        String dateString1 = formatter.format(new Date(Long.parseLong(String.valueOf(o1.getTimeStamp()))));
                                        String dateString2 = formatter.format(new Date(Long.parseLong(String.valueOf(o2.getTimeStamp()))));

                                        return Long.valueOf(format.parse(dateString1).getTime()).compareTo(format.parse(dateString2).getTime());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        return 0;
                                    }*/

                                    try {
                                        return f.parse(o1.getSentTime()).compareTo(f.parse(o2.getSentTime()));
                                    } catch (ParseException e) {
                                        throw new IllegalArgumentException(e);
                                    }

                                }
                            });

                            Collections.reverse(recentMessageListArrayList);
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.VISIBLE);
                            text_no_network.setVisibility(View.GONE);
                            messageAdapter.notifyDataSetChanged();
                        } else {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.GONE);
                            text_no_network.setVisibility(View.VISIBLE);
                        }

                    } else {
                        mDialog.dismiss();
                        message_recycle.setVisibility(View.GONE);
                        text_no_network.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getSearchChatMessageList(String search) {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref_message = database.getReference("message");
            ref_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();
                    System.out.println("chat list single ====> "+gson.toJson(dataSnapshot.getValue()));
                    stringArrayList.clear();
                    recentMessageListArrayList.clear();
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            id = (String) child.getKey();
                            String chatID[] = id.split("to");
                            count1.clear();
                            count2.clear();
                            for (DataSnapshot child1 : child.getChildren()) {
                                String key = (String) child1.getKey();
                                thisGroup = (boolean) dataSnapshot.child(id).child(key).child("thisGroup").getValue();
                                messages = (String) dataSnapshot.child(id).child(key).child("message").getValue();
                                String readBy = (String) dataSnapshot.child(id).child(key).child("readBy").getValue();
                                readStatus = (Long) dataSnapshot.child(id).child(key).child("readStatus").getValue();
                                receiverFcmKey = (String) dataSnapshot.child(id).child(key).child("receiverFcmKey").getValue();
                                receiverFcmKeyy = (ArrayList<String>) dataSnapshot.child(id).child(key).child("receiverFcmKeyy").getValue();
                                receiverImage = (String) dataSnapshot.child(id).child(key).child("receiverImage").getValue();
                                receiverName = (String) dataSnapshot.child(id).child(key).child("receiverName").getValue();
                                receiverUserID = (String) dataSnapshot.child(id).child(key).child("receiverUserID").getValue();
                                receiverUserIDD = (ArrayList<String>) dataSnapshot.child(id).child(key).child("receiverUserIDD").getValue();
                                String senderImage = (String) dataSnapshot.child(id).child(key).child("senderImage").getValue();
                                senderName = (String) dataSnapshot.child(id).child(key).child("senderName").getValue();
                                String senderUserID = (String) dataSnapshot.child(id).child(key).child("senderUserID").getValue();
                                String senderFcmKey = (String) dataSnapshot.child(id).child(key).child("senderFcmKey").getValue();
                                sentTime = (String) dataSnapshot.child(id).child(key).child("sentTime").getValue();
                                Long state = (Long) dataSnapshot.child(id).child(key).child("state").getValue();
                                timeStamp = (Long) dataSnapshot.child(id).child(key).child("timeStamp").getValue();
                                Long unreadCount = (Long) dataSnapshot.child(id).child(key).child("unreadCount").getValue();

                                groupID = (String) dataSnapshot.child(id).child(key).child("groupID").getValue();
                                groupImage = (String) dataSnapshot.child(id).child(key).child("groupImage").getValue();
                                groupName = (String) dataSnapshot.child(id).child(key).child("groupName").getValue();

                                if (thisGroup) {
                                    if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                        count1.add(receiverUserID);
                                    }
                                } else {
                                    if (chatID[0].equals(receiverUserID) && readStatus == 0) {
                                        count2.add(receiverUserID);
                                    }
                                }

                            }

                            //here
                            if (thisGroup) {
                                if (!stringArrayList.contains(id) && groupName.toLowerCase().contains(search) && receiverUserIDD.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                    stringArrayList.add(id);
                                    String group = groupName.replaceAll(search, "<font color='red'>"+search+"</font>");
                                    recentMessageListArrayList.add(new MessageList(true, groupID, group, groupImage, receiverName, receiverImage, receiverUserID,receiverUserIDD,
                                            receiverFcmKeyy, senderName+": "+messages, Integer.parseInt(String.valueOf(readStatus)), sentTime, count1.size(), timeStamp));
                                    messageAdapter.notifyDataSetChanged();
                                }
                            } else {
                                if (chatID[0].equals(sharedPreferences.getString(Constants.USERID, null)) && !stringArrayList.contains(id) && receiverName.toLowerCase().contains(search)) {
                                    stringArrayList.add(id);
                                    recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                            "", "",chatID[1], null, null, messages,
                                            Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                    messageAdapter.notifyDataSetChanged();
                                }
                            }

                        }

                        //hide//
                            /*if (thisGroup) {
                                if (Objects.requireNonNull(receiverUserID).contains(Objects.requireNonNull(sharedPreferences.getString(Constants.USERID, null)))) {
                                    count1.clear();
                                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                    final DatabaseReference ref_message = database.getReference("group_message").child(id);
                                    ref_message.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                String messageeee = null;
                                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                    String key = (String) child.getKey();
                                                    String readBy = (String) dataSnapshot.child(key).child("readBy").getValue();
                                                    String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                                                    //messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                                                    if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                                        count1.add(receiverUserID);
                                                    }
                                                }

                                                System.out.println("count1 ==> "+count1.size());
                                                if (!stringArrayList.contains(groupID)) {
                                                    stringArrayList.add(groupID);
                                                    recentMessageListArrayList.add(new MessageList(true, groupID, groupName, groupImage, receiverName, receiverImage, receiverUserID,
                                                            receiverFcmKey, messages, Integer.parseInt(String.valueOf(readStatus)), sentTime, count1.size(), timeStamp));
                                                    messageAdapter.notifyDataSetChanged();
                                                }
                                                if (recentMessageListArrayList.size() > 0) {
                                                    message_recycle.setVisibility(View.VISIBLE);
                                                    text_no_network.setVisibility(View.GONE);
                                                    messageAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            System.out.println("The read failed: " + databaseError.getCode());
                                        }
                                    });
                                }
                            } else {
                                String chatID[] = id.split("to");
                               if (chatID[0].equals(sharedPreferences.getString(Constants.USERID, null))) {
                                   count2.clear();
                                   final FirebaseDatabase database = FirebaseDatabase.getInstance();
                                   final DatabaseReference ref_message = database.getReference("message").child(id);
                                   ref_message.addValueEventListener(new ValueEventListener() {
                                       @Override
                                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                           if (dataSnapshot.exists()) {
                                               String messageeee = null;
                                               for (DataSnapshot child : dataSnapshot.getChildren()) {
                                                   String key = (String) child.getKey();
                                                   Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                                                   String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                                                   messageeee = (String) dataSnapshot.child(key).child("message").getValue();
                                                   if (chatID[0].equals(receiverUserID) && readStatus == 0) {
                                                       count2.add(receiverUserID);
                                                   }
                                               }

                                               if (!stringArrayList.contains(id)) {
                                                   stringArrayList.add(id);
                                                   recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                                           "", "", chatID[1], "", messageeee,
                                                           Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                                   messageAdapter.notifyDataSetChanged();
                                               }

                                               *//*if (userListArrayList.size() > 0) {
                                                   for (int i = 0; i < userListArrayList.size(); i++) {
                                                       if (userListArrayList.get(i).getReceiverUserID().equals(chatID[1]) && !stringArrayList.contains(userListArrayList.get(i).getReceiverUserID())) {
                                                           stringArrayList.add(userListArrayList.get(i).getReceiverUserID());
                                                           recentMessageListArrayList.add(new MessageList(false, "", "", "",
                                                                   userListArrayList.get(i).getReceiverName(), userListArrayList.get(i).getReceiverImage(),
                                                                   userListArrayList.get(i).getReceiverUserID(), userListArrayList.get(i).getReceiverFcmKey(), messages,
                                                                   Integer.parseInt(String.valueOf(readStatus)), sentTime, count2.size(), timeStamp));
                                                       }
                                                   }
                                                   messageAdapter.notifyDataSetChanged();
                                               }*//*
                                               if (recentMessageListArrayList.size() > 0) {
                                                   message_recycle.setVisibility(View.VISIBLE);
                                                   text_no_network.setVisibility(View.GONE);
                                                   messageAdapter.notifyDataSetChanged();
                                               }
                                           }
                                       }

                                       @Override
                                       public void onCancelled(DatabaseError databaseError) {
                                           System.out.println("The read failed: " + databaseError.getCode());
                                       }
                                   });
                               }
                            }*/

                        if (recentMessageListArrayList.size() > 0) {

                            Collections.sort(recentMessageListArrayList, new Comparator<MessageList>() {
                                @Override
                                public int compare(MessageList o1, MessageList o2) {
                                    try {
                                        DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);

                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                        String dateString1 = formatter.format(new Date(Long.parseLong(String.valueOf(o1.getTimeStamp()))));
                                        String dateString2 = formatter.format(new Date(Long.parseLong(String.valueOf(o2.getTimeStamp()))));

                                        return Long.valueOf(format.parse(dateString1).getTime()).compareTo(format.parse(dateString2).getTime());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        return 0;
                                    }
                                }
                            });

                            Collections.reverse(recentMessageListArrayList);
                            message_recycle.setVisibility(View.VISIBLE);
                            text_no_network.setVisibility(View.GONE);
                            messageAdapter.notifyDataSetChanged();
                            mDialog.dismiss();
                        } else {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.GONE);
                            text_no_network.setVisibility(View.VISIBLE);
                        }

                    } else {
                        mDialog.dismiss();
                        message_recycle.setVisibility(View.GONE);
                        text_no_network.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    /*public void getMessageList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            messageListArrayList.clear();
            stringArrayList.clear();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref_message = database.getReference("message");
            ref_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    System.out.println("chat list single ====> "+dataSnapshot.getValue());
                    mDialog.dismiss();
                    if (dataSnapshot.exists()) {
                        mDialog.dismiss();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String id = (String) child.getKey();
                            String chatID[] = id.split("to");
                            if (chatID[0].equals(sharedPreferences.getString(Constants.USERID, null))) {
                                DataSnapshot dataSnapshot1 = dataSnapshot.child(id);
                                count.clear();
                                for (DataSnapshot child1 : dataSnapshot1.getChildren()) {
                                    String key = (String) child1.getKey();
                                    receiverName = (String) dataSnapshot1.child(key).child("receiverName").getValue();
                                    receiverImage = (String) dataSnapshot1.child(key).child("receiverImage").getValue();
                                    messages = (String) dataSnapshot1.child(key).child("message").getValue();
                                    readStatus = (Long) dataSnapshot1.child(key).child("readStatus").getValue();
                                    sentTime = (String) dataSnapshot1.child(key).child("sentTime").getValue();
                                    state = (Long) dataSnapshot1.child(key).child("state").getValue();
                                    receiverUserID = (String) dataSnapshot1.child(key).child("receiverUserID").getValue();
                                    timeStamp = (Long) dataSnapshot1.child(key).child("timeStamp").getValue();
                                    //isThisGroup = (boolean) dataSnapshot1.child(key).child("isThisGroup").getValue();
                                    if (chatID[0].equals(receiverUserID) && readStatus==0) {
                                        count.add(receiverUserID);
                                    }
                                }
                                if (userListArrayList.size() > 0) {
                                    for (int i = 0; i < userListArrayList.size(); i++) {
                                        if (userListArrayList.get(i).getReceiverUserID().equals(chatID[1]) && !stringArrayList.contains(userListArrayList.get(i).getReceiverUserID())) {
                                            stringArrayList.add(userListArrayList.get(i).getReceiverUserID());
                                            messageListArrayList.add(new MessageList(false, "","","", userListArrayList.get(i).getReceiverName(), userListArrayList.get(i).getReceiverImage(),
                                                    userListArrayList.get(i).getReceiverUserID(), userListArrayList.get(i).getReceiverFcmKey(), messages,
                                                    Integer.parseInt(String.valueOf(readStatus)), sentTime, count.size(), timeStamp));
                                        }
                                    }
                                }

                                if (messageListArrayList.size() > 0) {
                                    mDialog.dismiss();
                                    message_recycle.setVisibility(View.VISIBLE);
                                    text_no_network.setVisibility(View.GONE);
                                    message_recycle.invalidate();
                                    *//*Collections.sort(messageListArrayList, new Comparator() {
                                        @Override
                                        public int compare(Object o1, Object o2) {
                                            MessageList p1 = (MessageList) o1;
                                            MessageList p2 = (MessageList) o2;

                                            try {
                                                return new SimpleDateFormat("hh:mm a").parse(p1.getSentTime()).compareTo
                                                        (new SimpleDateFormat("hh:mm a").parse(p2.getSentTime()));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            return 0;
                                        }

                                    });*//*
                                    messageAdapter.notifyDataSetChanged();
                                } else {
                                    mDialog.dismiss();
                                    message_recycle.setVisibility(View.GONE);
                                    text_no_network.setVisibility(View.VISIBLE);
                                }
                            }  else {
                                mDialog.dismiss();
                            }
                        }

                    } else {
                        mDialog.dismiss();
                        if (messageListArrayList.size() > 0) {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.VISIBLE);
                            text_no_network.setVisibility(View.GONE);
                            message_recycle.invalidate();
                            messageAdapter.notifyDataSetChanged();
                        } else {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.GONE);
                            text_no_network.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

            final DatabaseReference ref_group_message = database.getReference("group_message");
            ref_group_message.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    System.out.println("chat list group ====> "+dataSnapshot.getValue());
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String group_id = (String) child.getKey();
                                DataSnapshot dataSnapshot1 = dataSnapshot.child(group_id);
                                count.clear();
                                for (DataSnapshot child1 : dataSnapshot1.getChildren()) {
                                    String key = (String) child1.getKey();
                                    receiverName = (String) dataSnapshot1.child(key).child("receiverName").getValue();
                                    receiverImage = (String) dataSnapshot1.child(key).child("receiverImage").getValue();
                                    messages = (String) dataSnapshot1.child(key).child("message").getValue();
                                    readStatus = (Long) dataSnapshot1.child(key).child("readStatus").getValue();
                                    sentTime = (String) dataSnapshot1.child(key).child("sentTime").getValue();
                                    state = (Long) dataSnapshot1.child(key).child("state").getValue();
                                    receiverUserID = (String) dataSnapshot1.child(key).child("receiverUserID").getValue();
                                    timeStamp = (Long) dataSnapshot1.child(key).child("timeStamp").getValue();
                                    //isThisGroup = (boolean) dataSnapshot1.child(key).child("isThisGroup").getValue();
                                    groupID = (String) dataSnapshot1.child(key).child("groupID").getValue();
                                    groupName = (String) dataSnapshot1.child(key).child("groupName").getValue();
                                    groupImage = (String) dataSnapshot1.child(key).child("groupImage").getValue();
                                    receiverFcmKey = (String) dataSnapshot1.child(key).child("receiverFcmKey").getValue();
                                    String readBy = (String) dataSnapshot1.child(key).child("readBy").getValue();
                                    if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                                        count.add(receiverUserID);
                                    }
                                }

                            messageListArrayList.add(new MessageList(true, group_id,groupName,groupImage, receiverName,
                                    "",
                                    receiverUserID, receiverFcmKey, messages,
                                    Integer.parseInt(String.valueOf(readStatus)), sentTime, count.size(), timeStamp));

                                if (messageListArrayList.size() > 0) {
                                    mDialog.dismiss();
                                    message_recycle.setVisibility(View.VISIBLE);
                                    text_no_network.setVisibility(View.GONE);
                                    message_recycle.invalidate();
                                    *//*Collections.sort(messageListArrayList, new Comparator() {
                                        @Override
                                        public int compare(Object o1, Object o2) {
                                            MessageList p1 = (MessageList) o1;
                                            MessageList p2 = (MessageList) o2;

                                            try {
                                                return new SimpleDateFormat("hh:mm a").parse(p1.getSentTime()).compareTo
                                                        (new SimpleDateFormat("hh:mm a").parse(p2.getSentTime()));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            return 0;
                                        }

                                    });*//*
                                    messageAdapter.notifyDataSetChanged();
                                } else {
                                    mDialog.dismiss();
                                    message_recycle.setVisibility(View.GONE);
                                    text_no_network.setVisibility(View.VISIBLE);
                                }
                        }

                    } else {
                        mDialog.dismiss();
                        if (messageListArrayList.size() > 0) {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.VISIBLE);
                            text_no_network.setVisibility(View.GONE);
                            message_recycle.invalidate();
                            messageAdapter.notifyDataSetChanged();
                        } else {
                            mDialog.dismiss();
                            message_recycle.setVisibility(View.GONE);
                            text_no_network.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        } else {
            mDialog.dismiss();
        }
    }*/

    private void getUserData() {
        showProgressDialog();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String id = (String) child.getKey();
                        String receiverName = (String) dataSnapshot.child(id).child("receiverName").getValue();
                        String receiverImage = (String) dataSnapshot.child(id).child("receiverImage").getValue();
                        String receiverUserID = (String) dataSnapshot.child(id).child("receiverUserID").getValue();
                        String receiverFcmKey = (String) dataSnapshot.child(id).child("receiverFcmKey").getValue();
                        String receiverRole = (String) dataSnapshot.child(id).child("receiverRole").getValue();
                        String receiverClass = (String) dataSnapshot.child(id).child("receiverClass").getValue();
                        userListArrayList.add(new MessageList(receiverName,receiverImage,receiverUserID,receiverFcmKey,receiverRole,receiverClass));
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();

            }
        });

    }

    /*public void getDataFromFirebaseDataBase(){
        dialog.setMessage("Please wait..");
        dialog.setCancelable(false);
        dialog.show();
        senderUserName = "dinesh";
        senderUserID = sharedPreferences.getString(Constants.USERID,"");
        RECENT_LIST_CHILD = "" + senderUserID;

        mFirebaseDatabaseReferenceInbox = FirebaseDatabase.getInstance().getReference();
        messagesRef = mFirebaseDatabaseReferenceInbox.child(RECENT_CHAT).child("Inbox")
                .child(RECENT_LIST_CHILD);
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                dialog.dismiss();
                Log.i("snapshot", snapshot.toString());
                if (snapshot.getChildrenCount() > 0) {
                    Log.i("snapshot", "" + snapshot.getChildrenCount());
//                    list_user_list.setVisibility(View.VISIBLE);
//                    relative_info.setVisibility(View.INVISIBLE);
                    message.clear();
                    try {
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Log.i("snapshot1",""+postSnapshot.getValue());
                            ChatMessageParams chatMessageParams = postSnapshot.getValue(ChatMessageParams.class);
                            Log.i("chatmessage","to :"+chatMessageParams.getToName()+"name :"+chatMessageParams.getName().toString());
                            message.add(chatMessageParams);
                            // here you can access to name property like university.name

                        }
                        if (message==null||message.size()==0){

                            text_no_network.setText("No chats, Tap the plus icon to chat");
                            message_recycle.setVisibility(View.GONE);
                            text_no_network.setVisibility(View.VISIBLE);
                        }
                        else {
                            message_recycle.setVisibility(View.VISIBLE);
                            text_no_network.setVisibility(View.GONE);
                            messageAdapter = new MessageAdapter( message,getActivity());
                            message_recycle.setAdapter(messageAdapter);
                        }

                    }
                    catch (Exception e){
//                        text_no_network.setVisibility(View.VISIBLE);
//                        message_recycle.setVisibility(View.GONE);
                        e.printStackTrace();
//                        text_no_network.setText("No chats, Tap the plus icon to chat");
                    }
                } else {
                    message_recycle.setVisibility(View.GONE);
                    text_no_network.setVisibility(View.VISIBLE);
                    //  relative_info.setVisibility(View.VISIBLE);
                    text_no_network.setText("No chat, Tap the plus icon to chat");
                    //image_no_network.setImageResource(R.mipmap.icon_no_network);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        MenuItem item = menu.findItem(R.id.action_calender);
        item.setVisible(false);
        this.menu = menu;

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void showKeyboard(final EditText ettext){
        newMessage.setVisibility(View.VISIBLE);
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    private void hideSoftKeyboard(EditText ettext){
        newMessage.setVisibility(View.VISIBLE);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            /*if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }*/
        }

    }

}

