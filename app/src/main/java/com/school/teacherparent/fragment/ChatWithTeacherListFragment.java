package com.school.teacherparent.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.GroupMessageActivity;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.adapter.StudentsAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.GroupChatResponse;
import com.school.teacherparent.models.GroupListforChatResponse;
import com.school.teacherparent.models.StudentsListforChat;
import com.school.teacherparent.models.TeachersListforChatResponse;
import com.school.teacherparent.models.UserData;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatWithTeacherListFragment extends Fragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    DiscreteScrollView dsvStudentList;
    RecyclerView rvTeachersList;
    TextView tvNoTeachers;
    ProgressDialog mDialog;
    ChildListResponseResponse childListResponseResponse;
    TeachersListforChatResponse teachersListforChatResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    List<TeachersListforChatResponse.chatTeachersList> chatTeachersList = new ArrayList<>();
    int classID, studentID, sectionID,studentSchoolID;
    StudentsAdapter studentsAdapter;
    String selectChatOption;
    SwipeRefreshLayout swipeRefresh;

    GroupListforChatResponse groupListforChatResponse;
    List<GroupChatResponse.groupList> groupList = new ArrayList<>();

    ArrayList<String> TeacherID1 = new ArrayList<>();
    ArrayList<String> TeacherName1 = new ArrayList<>();
    ArrayList<String> TeacherImage1 = new ArrayList<>();
    ArrayList<String> TeacherFcmKey1 = new ArrayList<>();
    ArrayList<String> TeacherRole1 = new ArrayList<>();
    ArrayList<String> TeacherClass1 = new ArrayList<>();
    ArrayList<String> UserList1 = new ArrayList<>();

    ArrayList<UserData> TeacherID = new ArrayList<>();
    ArrayList<UserData> TeacherName = new ArrayList<>();
    ArrayList<UserData> TeacherImage = new ArrayList<>();
    ArrayList<UserData> TeacherFcmKey = new ArrayList<>();
    ArrayList<UserData> TeacherRole = new ArrayList<>();
    ArrayList<UserData> TeacherClass = new ArrayList<>();
    ArrayList<UserData> UserList = new ArrayList<>();

    Menu menu;
    NotificationReceiver notificationReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_with_teacher_list, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        MenuItem items = menu.findItem(R.id.action_notification);
        items.setVisible(true);
        MenuItem items1 = menu.findItem(R.id.action_calender);
        items1.setVisible(false);
    }

    private void initViews(View view) {
        TestDrawerActivity.toolbar.setTitle(getString(R.string.contact_list));
        setHasOptionsMenu(true);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        rvTeachersList = view.findViewById(R.id.rv_teacherslist);
        tvNoTeachers = view.findViewById(R.id.tv_no_teachers);
        swipeRefresh = view.findViewById(R.id.swipe_refresh);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rvTeachersList.setLayoutManager(mLayoutManager);
        rvTeachersList.setItemAnimator(new DefaultItemAnimator());
        rvTeachersList.setHasFixedSize(true);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        selectChatOption();
    }

    private void selectChatOption() {

        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_chat_option);

        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        Button btnParents = dialog.findViewById(R.id.btn_parents);
        Button btnTeachers = dialog.findViewById(R.id.btn_teachers);
        Button btnGroups = dialog.findViewById(R.id.btn_groups);
        btnParents.setVisibility(View.GONE);

        dialog.show();

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((TestDrawerActivity) getActivity()).onBackPressed();
            }
        });
        btnTeachers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentsAdapter = new StudentsAdapter(getContext(), chatTeachersList, 4);
                rvTeachersList.setAdapter(studentsAdapter);
                selectChatOption = "Teachers";
                performAction();
                getStudentList();
                dialog.dismiss();
            }
        });
        btnGroups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentsAdapter = new StudentsAdapter(getContext(), 5, groupList, ChatWithTeacherListFragment.this);
                rvTeachersList.setAdapter(studentsAdapter);
                selectChatOption = "Groups";
                performAction();
                getStudentList();
                dialog.dismiss();
            }
        });

    }

    private void performAction() {
        TestDrawerActivity.edittextleaveSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(TestDrawerActivity.edittextleaveSearch);
            }
        });
        TestDrawerActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (  TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(  TestDrawerActivity.edittextleaveSearch.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        switch (selectChatOption) {
                            case "Teachers":
                                chatTeachersList.clear();
                                getTeachersList(TestDrawerActivity.edittextleaveSearch.getText().toString().trim().toLowerCase());
                                break;
                            case "Groups":
                                groupList.clear();
                                getSearchGroupList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                                break;
                        }
                    }

                    return true;
                }
                else
                {
                    Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        TestDrawerActivity.toolbar.setTitle(getString(R.string.contact_list));
                        switch (selectChatOption) {
                            case "Teachers":
                                chatTeachersList.clear();
                                getTeachersList();
                                break;
                            case "Groups":
                                groupList.clear();
                                getGroupList();
                                break;
                        }
                        swipeRefresh.setRefreshing(false);
                    }
                }, 000);
            }
        });

        studentsAdapter.OnsetClickListen(new StudentsAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                if (selectChatOption.equals("Teachers")) {
                    Intent intent = new Intent(getContext(), MessageActivity.class);
                    intent.putExtra("ReceiverID", chatTeachersList.get(position).getTeacherEncryptedID());
                    intent.putExtra("ReceiverName", chatTeachersList.get(position).getFname()+" "+chatTeachersList.get(position).getLname());
                    intent.putExtra("ReceiverImage", chatTeachersList.get(position).getEmp_photo());
                    intent.putExtra("ReceiverFcmKey", chatTeachersList.get(position).getFcm_key());
                    intent.putExtra("ReceiverRole", chatTeachersList.get(position).getTeacherRole());
                    intent.putExtra("ReceiverClass", chatTeachersList.get(position).getTeacherClass());
                    startActivity(intent);

                } else if (selectChatOption.equals("Groups")) {
                    TeacherID1.clear();
                    TeacherName1.clear();
                    TeacherImage1.clear();
                    TeacherFcmKey1.clear();
                    TeacherRole1.clear();
                    TeacherClass1.clear();
                    UserList1.clear();

                    int pos = groupList.get(position).getDataPosition();
                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_group_path) + "/" ;

                    for (int i = 0; i < groupList.get(position).getTeacherID().size(); i++) {
                        if (pos == groupList.get(position).getTeacherID().get(i).getPosition()) {
                            if (!TeacherID1.contains(groupList.get(position).getTeacherID().get(i).getData())) {
                                TeacherID1.add(groupList.get(position).getTeacherID().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherName().size(); i++) {
                        if (pos == groupList.get(position).getTeacherName().get(i).getPosition()) {
                            if (!TeacherName1.contains(groupList.get(position).getTeacherName().get(i).getData())) {
                                TeacherName1.add(groupList.get(position).getTeacherName().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherImage().size(); i++) {
                        if (pos == groupList.get(position).getTeacherImage().get(i).getPosition()) {
                            if (!TeacherImage1.contains(groupList.get(position).getTeacherImage().get(i).getData())) {
                                TeacherImage1.add(groupList.get(position).getTeacherImage().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherFcmKey().size(); i++) {
                        if (pos == groupList.get(position).getTeacherFcmKey().get(i).getPosition()) {
                            if (!TeacherFcmKey1.contains(groupList.get(position).getTeacherFcmKey().get(i).getData())) {
                                TeacherFcmKey1.add(groupList.get(position).getTeacherFcmKey().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherRole().size(); i++) {
                        if (pos == groupList.get(position).getTeacherRole().get(i).getPosition()) {
                            if (!TeacherRole1.contains(groupList.get(position).getTeacherRole().get(i).getData())) {
                                TeacherRole1.add(groupList.get(position).getTeacherRole().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherClass().size(); i++) {
                        if (pos == groupList.get(position).getTeacherClass().get(i).getPosition()) {
                            if (!TeacherClass1.contains(groupList.get(position).getTeacherClass().get(i).getData())) {
                                TeacherClass1.add(groupList.get(position).getTeacherClass().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getUserList().size(); i++) {
                        if (pos == groupList.get(position).getUserList().get(i).getPosition()) {
                            if (!UserList1.contains(groupList.get(position).getUserList().get(i).getData())) {
                                UserList1.add(groupList.get(position).getUserList().get(i).getData());
                            }
                        }
                    }

                    System.out.println("UserList1 ==> "+Arrays.toString(new ArrayList[]{UserList1}).replaceAll("\\[|\\]", ""));

                    Intent intent = new Intent(getContext(), GroupMessageActivity.class);
                    intent.putExtra("GroupName", groupList.get(position).getGroupName());
                    intent.putExtra("GroupImage", url+groupList.get(position).getGroupImage());
                    intent.putExtra("GroupID", groupList.get(position).getGroupID());
                    intent.putStringArrayListExtra("ArrayReceiverID",TeacherID1);
                    intent.putExtra("ArrayReceiverName", Arrays.toString(new ArrayList[]{TeacherName1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayReceiverImage", Arrays.toString(new ArrayList[]{TeacherImage1}).replaceAll("\\[|\\]", ""));
                    intent.putStringArrayListExtra("ArrayReceiverFcmKey", TeacherFcmKey1);
                    intent.putExtra("ArrayReceiverRole", Arrays.toString(new ArrayList[]{TeacherRole1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayReceiverClass", Arrays.toString(new ArrayList[]{TeacherClass1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayUserList", Arrays.toString(new ArrayList[]{UserList1}).replaceAll("\\[|\\]", ""));
                    startActivity(intent);
                }
            }

        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                if ( TestDrawerActivity.edittextleaveSearch.getText().toString().length()<=0) {
                    TestDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    TestDrawerActivity.toolbar.setTitle("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                    TestDrawerActivity.edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    TestDrawerActivity.edittextleaveSearch.setEnabled(true);
                    TestDrawerActivity.edittextleaveSearch.requestFocus();
                    showKeyboard(TestDrawerActivity.edittextleaveSearch);
                    TestDrawerActivity.edittextleaveSearch.setText("");
                }
                else
                {
                    switch (selectChatOption) {
                        case "Teachers":
                            chatTeachersList.clear();
                            getTeachersList(TestDrawerActivity.edittextleaveSearch.getText().toString().trim().toLowerCase());
                            break;
                        case "Groups":
                            groupList.clear();
                            getSearchGroupList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                            break;
                    }

                }

                return true;

            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            default:
                break;
        }

        return false;
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                switch (selectChatOption) {
                                    case "Teachers":
                                        chatTeachersList.clear();
                                        getTeachersList();
                                        break;
                                    case "Groups":
                                        groupList.clear();
                                        getGroupList();
                                        break;
                                }
                            } else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getTeachersList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(studentSchoolID));
            studentsListforChat.setStudID(String.valueOf(studentID));
            vidyauraAPI.getTeachersListforChat(studentsListforChat).enqueue(new Callback<TeachersListforChatResponse>() {
                @Override
                public void onResponse(Call<TeachersListforChatResponse> call, Response<TeachersListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response teachers list ==> " + gson.toJson(response.body()));
                        teachersListforChatResponse = response.body();
                        chatTeachersList.clear();
                        if(teachersListforChatResponse.getChatTeachersList() != null) {
                        for (int i = 0; i < teachersListforChatResponse.getChatTeachersList().size(); i++) {
                            chatTeachersList.add(new TeachersListforChatResponse.chatTeachersList(
                                    teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getFname(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getLname(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getEmp_photo(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getFcm_key(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getTeacherID(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getSubjectName(),
                                    (teachersListforChatResponse.getChatTeachersList().get(i).getAltName() + teachersListforChatResponse.getChatTeachersList().get(i).getSectionName())));
                        }
                        }

                        if (chatTeachersList.size() > 0) {
                            rvTeachersList.setVisibility(View.VISIBLE);
                            tvNoTeachers.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvTeachersList.setVisibility(View.GONE);
                            tvNoTeachers.setVisibility(View.VISIBLE);
                            tvNoTeachers.setText("No Teachers Found");
                        }


                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TeachersListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getTeachersList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            studentsListforChat.setStudID(String.valueOf(studentID));
            vidyauraAPI.getTeachersListforChat(studentsListforChat).enqueue(new Callback<TeachersListforChatResponse>() {
                @Override
                public void onResponse(Call<TeachersListforChatResponse> call, Response<TeachersListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response teachers list ==> " + gson.toJson(response.body()));
                        teachersListforChatResponse = response.body();
                        chatTeachersList.clear();
                        for (int i=0;i<teachersListforChatResponse.getChatTeachersList().size();i++) {
                            String name = teachersListforChatResponse.getChatTeachersList().get(i).getFname() + " " + teachersListforChatResponse.getChatTeachersList().get(i).getLname();
                            if (name.toLowerCase().contains(search)) {
                                chatTeachersList.add(new TeachersListforChatResponse.chatTeachersList(
                                        teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getFname(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getLname(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getEmp_photo(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getFcm_key(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getTeacherID(),
                                        teachersListforChatResponse.getChatTeachersList().get(i).getSubjectName(),
                                        (teachersListforChatResponse.getChatTeachersList().get(i).getAltName() + teachersListforChatResponse.getChatTeachersList().get(i).getSectionName())));
                            }
                        }

                        if (chatTeachersList.size() > 0) {
                            rvTeachersList.setVisibility(View.VISIBLE);
                            tvNoTeachers.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvTeachersList.setVisibility(View.GONE);
                            tvNoTeachers.setVisibility(View.VISIBLE);
                            tvNoTeachers.setText("No Teachers Found");
                        }


                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TeachersListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getGroupList() {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(studentSchoolID));
            studentsListforChat.setStudID(String.valueOf(studentID));
            groupList.clear();
            TeacherID.clear();
            TeacherName.clear();
            TeacherImage.clear();
            TeacherFcmKey.clear();
            TeacherRole.clear();
            TeacherClass.clear();
            UserList.clear();
            vidyauraAPI.getGroupsListforChat(studentsListforChat).enqueue(new Callback<GroupListforChatResponse>() {
                @Override
                public void onResponse(Call<GroupListforChatResponse> call, Response<GroupListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        groupListforChatResponse = response.body();
                        groupList.clear();
                        Gson gson = new Gson();
                        System.out.println("response group list ==> " + gson.toJson(response.body()));

                        if (groupListforChatResponse.getStatus() == 200)
                        if (groupListforChatResponse.getChatGroupList() != null)
                        if (groupListforChatResponse.getChatGroupList().size()>0)
                        for (int i=0;i<groupListforChatResponse.getChatGroupList().size();i++) {

                            for (int j=0;j<groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().size();j++) {
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getLname()));
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID())) {*/
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID());
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID()));
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                    UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                    UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFcm_key()));
                                    UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                }
                                /*}*/

                            }

                            for (int k=0;k<groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().size();k++) {
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID());
                                TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getEmp_photo()));
                                TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFcm_key()));
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                /*}*/
                            }


                            //TeacherID.add("WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09");
                            //TeacherName.add("Manoj");
                            //TeacherImage.add("");
                            //TeacherFcmKey.add("c_FvOxhgVwg:APA91bFUbC9yyEpMb5A5EZ64yLayQYms6YWwsn8IGNbnYP00zb_RyYcpR_yLhHDXlSRdA--ldLs-1jzKrvOsMp6YxQXxcEq3P0IaIz6_mWDXRKpvvSHtMH3BuKuTJmgZ7odhwy10A-cT");
                            TeacherRole.add(new UserData(i,"TeacherRole"));
                            TeacherClass.add(new UserData(i,"TeacherClass"));


                            groupList.add(new GroupChatResponse.groupList(i,groupListforChatResponse.getChatGroupList().get(i).getGroup_name(),
                                    groupListforChatResponse.getChatGroupList().get(i).getGroupicon(),
                                    groupListforChatResponse.getChatGroupList().get(i).getEncryptedGroupID(),
                                    TeacherID,
                                    TeacherName,
                                    TeacherImage,
                                    TeacherFcmKey,
                                    TeacherRole,
                                    TeacherClass,
                                    UserList));

                        }

                        if (groupList.size() > 0) {
                            for (int i=0;i<groupList.size();i++) {
                                System.out.println("response list ==> " + gson.toJson(groupList.get(i).getTeacherID()));
                            }
                            rvTeachersList.setVisibility(View.VISIBLE);
                            tvNoTeachers.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvTeachersList.setVisibility(View.GONE);
                            tvNoTeachers.setVisibility(View.VISIBLE);
                            tvNoTeachers.setText("No Groups Found");
                        }

                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<GroupListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    System.out.println("Error ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getSearchGroupList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            groupList.clear();
            TeacherID.clear();
            TeacherName.clear();
            TeacherImage.clear();
            TeacherFcmKey.clear();
            TeacherRole.clear();
            TeacherClass.clear();
            UserList.clear();
            vidyauraAPI.getGroupsListforChat(studentsListforChat).enqueue(new Callback<GroupListforChatResponse>() {
                @Override
                public void onResponse(Call<GroupListforChatResponse> call, Response<GroupListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        groupListforChatResponse = response.body();
                        groupList.clear();
                        Gson gson = new Gson();
                        System.out.println("response group list ==> " + gson.toJson(response.body()));

                        for (int i=0;i<groupListforChatResponse.getChatGroupList().size();i++) {

                            for (int j=0;j<groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().size();j++) {
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getLname()));
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID())) {*/
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID());
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID()));
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFcm_key()));
                                }
                                /*}*/

                            }

                            for (int k=0;k<groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().size();k++) {
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID());
                                TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getEmp_photo()));
                                TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFcm_key()));
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                /*}*/
                            }


                            //TeacherID.add("WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09");
                            //TeacherName.add("Manoj");
                            //TeacherImage.add("");
                            //TeacherFcmKey.add("c_FvOxhgVwg:APA91bFUbC9yyEpMb5A5EZ64yLayQYms6YWwsn8IGNbnYP00zb_RyYcpR_yLhHDXlSRdA--ldLs-1jzKrvOsMp6YxQXxcEq3P0IaIz6_mWDXRKpvvSHtMH3BuKuTJmgZ7odhwy10A-cT");
                            TeacherRole.add(new UserData(i,"TeacherRole"));
                            TeacherClass.add(new UserData(i,"TeacherClass"));

                            if (groupListforChatResponse.getChatGroupList().get(i).getGroup_name().toLowerCase().contains(search)) {

                                groupList.add(new GroupChatResponse.groupList(i,groupListforChatResponse.getChatGroupList().get(i).getGroup_name(),
                                        groupListforChatResponse.getChatGroupList().get(i).getGroupicon(),
                                        groupListforChatResponse.getChatGroupList().get(i).getEncryptedGroupID(),
                                        TeacherID,
                                        TeacherName,
                                        TeacherImage,
                                        TeacherFcmKey,
                                        TeacherRole,
                                        TeacherClass,
                                        UserList));
                            }

                        }

                        if (groupList.size() > 0) {
                            for (int i=0;i<groupList.size();i++) {
                                System.out.println("response list ==> " + gson.toJson(groupList.get(i).getTeacherID()));
                            }
                            rvTeachersList.setVisibility(View.VISIBLE);
                            tvNoTeachers.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvTeachersList.setVisibility(View.GONE);
                            tvNoTeachers.setVisibility(View.VISIBLE);
                            tvNoTeachers.setText("No Groups Found");
                        }

                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<GroupListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    System.out.println("Error ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void teacherInfo(int position) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_teacher_info);

        ImageView ivClose = dialog.findViewById(R.id.iv_close);
        ImageView iv_group_image = dialog.findViewById(R.id.iv_group_image);
        TextView tv_group_name = dialog.findViewById(R.id.tv_group_name);
        TextView tv_stafflist = dialog.findViewById(R.id.tv_stafflist);
        TextView tv_studentlist = dialog.findViewById(R.id.tv_studentlist);

        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        String url = getString(R.string.s3_baseurl) + getString(R.string.s3_group_path) + "/" + groupListforChatResponse.getChatGroupList().get(position).getGroupicon();
        Glide.with(getContext()).load(url).apply(error).into(iv_group_image);
        tv_group_name.setText(groupListforChatResponse.getChatGroupList().get(position).getGroup_name());

        StringBuilder staffname = new StringBuilder();
        StringBuilder studentname = new StringBuilder();
        for (int j = 0; j < groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().size(); j++) {
            staffname.append(groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().get(j).getLname()+"\n");
        }
        for (int j = 0; j < groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().size(); j++) {
            studentname.append(groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().get(j).getLname()+"\n");
        }
        tv_stafflist.setText(staffname.toString());
        tv_studentlist.setText(studentname.toString());

        dialog.show();
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID = parentsStudListArrayList.get(adapterPosition).getSchool_id();
            switch (selectChatOption) {
                case "Teachers":
                    chatTeachersList.clear();
                    getTeachersList();
                    break;
                case "Groups":
                    groupList.clear();
                    getGroupList();
                    break;
            }
        }
    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    private void hideSoftKeyboard(EditText ettext){
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

}
