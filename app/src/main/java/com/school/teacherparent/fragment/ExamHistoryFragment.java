package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ExamHistoryAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ExamHistoryList;
import com.school.teacherparent.models.ExamHistoryResponse;
import com.school.teacherparent.models.UpcomingExamListParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamHistoryFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    private RecyclerView examHistoryrecyclerView;
    private ExamHistoryAdapter examHistoryAdapter;

    public ArrayList<ExamHistoryResponse.examsHistoryList> examHistoryListResponsearray;
    SwipeRefreshLayout examhistroyswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    private boolean loading = true;
    int limit=5;
    int offset=0;
    ArrayList<ExamHistoryList> examHistoryListArrayList=new ArrayList<>();
    TextView noexamhistoryListTextview;
    LinearLayoutManager linearLayoutManager;
    boolean service=true;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {
            examHistoryListArrayList.clear();
            offset = 0;
            if (service) {
                getExamHistory();
                isLoading = false;
            }
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            examHistoryListArrayList.clear();
            offset = 0;
            if (service) {
                getExamHistory();
            }

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_exam_history, container, false);

        examHistoryrecyclerView=(RecyclerView)view.findViewById(R.id.history_recycle_fragment);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        ExamsActivity.title.setText("Exam");
        noexamhistoryListTextview=(TextView)view.findViewById(R.id.noexamhistoryListTextview);
        examHistoryListResponsearray=new ArrayList<ExamHistoryResponse.examsHistoryList>();
        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        examHistoryAdapter = new ExamHistoryAdapter(examHistoryListArrayList,getContext(),this,userType);
        //  examHistoryrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        examHistoryrecyclerView.setLayoutManager(linearLayoutManager);
        examHistoryrecyclerView.setItemAnimator(new DefaultItemAnimator());
        examHistoryrecyclerView.setAdapter(examHistoryAdapter);

      //  noexamhistoryListTextview.setVisibility(View.GONE);
       // examHistoryrecyclerView.setVisibility(View.GONE);
//        examHistoryAdapter.setOnClickListen(new ExamHistoryAdapter.AddTouchListen() {
//            @Override
//            public void OnTouchClick(int position) {
//                Fragment fragment=new ExamDetailListFragment();
//                replaceFragment(fragment);
//            }
//
//
//        });

        examhistroyswipelayout=view.findViewById(R.id.swipe_historyexam);
        examhistroyswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                examhistroyswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                            examHistoryListArrayList.clear();
                             limit=5;
                             offset=0;
                            getExamHistory();

                            examhistroyswipelayout.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });

        ExamsActivity.noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),NotificationActivity.class));
            }
        });
        examHistoryrecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView examHistoryrecyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    System.out.println("visibleItemCount ==> "+visibleItemCount+" totalItemCount ==> "+totalItemCount+" pastVisiblesItems ==> "+pastVisiblesItems);
                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            examHistoryAdapter.notifyDataSetChanged();
                            getExamHistory();
                            examHistoryAdapter.notifyDataSetChanged();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        return view;



    }

    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }




    }

    private void getExamHistory()
    {

        if (Util.isNetworkAvailable())
        {
            System.out.println("limit ==> "+limit+" "+offset);
            service=false;
            showProgress();
            UpcomingExamListParams upcomingExamListParams=new UpcomingExamListParams();
            upcomingExamListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            upcomingExamListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            upcomingExamListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            upcomingExamListParams.setLimit(limit);
            upcomingExamListParams.setOffset(offset);
            vidyauraAPI.getExamsHistoryList(upcomingExamListParams).enqueue(new Callback<ExamHistoryResponse>() {
                @Override
                public void onResponse(Call<ExamHistoryResponse> call, Response<ExamHistoryResponse> response) {
                    hideProgress();
                    service=true;
                    if (response.body() != null) {
                        ExamHistoryResponse examHistoryResponse = response.body();
                        if (examHistoryResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examHistoryListResponsearray != null) {
                                examHistoryListResponsearray.clear();
                            }
                            examHistoryListResponsearray = examHistoryResponse.getExamsHistoryList();
                            if (examHistoryResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examHistoryListResponsearray.size()>0) {
                                    //examHistoryrecyclerView.setVisibility(View.VISIBLE);
                                   // noexamhistoryListTextview.setVisibility(View.GONE);
                                    for (int y = 0; y < examHistoryListResponsearray.size(); y++) {
                                        if(examHistoryListResponsearray.get(y).getExamwiseClassList().size()>0){
                                            examHistoryListArrayList.add(new ExamHistoryList(examHistoryResponse.getExamsHistoryList().get(y).getTerm_id(), examHistoryResponse.getExamsHistoryList().get(y).getExam_title(), examHistoryResponse.getExamsHistoryList().get(y).getExam_duration(),
                                                    examHistoryResponse.getExamsHistoryList().get(y).getStart_date(), examHistoryResponse.getExamsHistoryList().get(y).getEnd_date(), examHistoryResponse.getExamsHistoryList().get(y).getSchool_name(),
                                                    examHistoryResponse.getExamsHistoryList().get(y).getLogo(), examHistoryResponse.getExamsHistoryList().get(y).getExamwiseClassList()));

                                            loading = true;
                                        }
                                    }
                                    System.out.println("examHistoryListArrayList ==> "+examHistoryListArrayList.size());
                                    examHistoryAdapter.notifyDataSetChanged();
                                }
                                else if (examHistoryListResponsearray.size()!=0 && examHistoryListResponsearray.size()!=0)
                                {
                                    //noexamhistoryListTextview.setVisibility(View.VISIBLE);
                                    //examHistoryrecyclerView.setVisibility(View.GONE);
                                    Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    loading=false;
                                }
                                else
                                {
                                    Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamHistoryResponse> call, Throwable t) {
                    hideProgress();
                    service=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    }









