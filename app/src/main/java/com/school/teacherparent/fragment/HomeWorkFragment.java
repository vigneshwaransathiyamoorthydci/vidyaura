package com.school.teacherparent.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ParentAssignmentHomeFragment.assignmentArrayListadd;
import static com.school.teacherparent.fragment.ParentSubHomeFragment.homeworkArrayListadd;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeWorkFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {


    private ViewPager viewPager;
    private TabLayout tabLayout;
    ImageView back, noti;
    LinearLayout ltDsvStudentList;
    static ImageView calendarImageview;
    FloatingActionButton addHomework;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;

    public HomeWorkFragment() {
        // Required empty public constructor
    }
    HomeWorkActivity homeWorkActivity;
    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialog;

    @Inject
    public VidyAPI vidyauraAPI;

    int limit=5;
    int offset=0;
    int count = 0;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    public int classID;
    public static int studentID,studentSchoolID;
    public int sectionID;
    int position = 0;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_work, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        homeWorkActivity = (HomeWorkActivity) getActivity();
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        addHomework = view.findViewById(R.id.add_homework);
        calendarImageview=view.findViewById(R.id.calendarImageview);
        calendarImageview.setVisibility(View.GONE);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        ltDsvStudentList = view.findViewById(R.id.lt_dsv_student_list);
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        addHomework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if ( tabLayout.getSelectedTabPosition()==0){
//                    SubHomeFragment.isLoading=true;
//                    AssignmentHomeFragment.isLoading=false;
//
//                }
//                else {
//                    SubHomeFragment.isLoading=false;
//                    AssignmentHomeFragment.isLoading=true;
//
//                }

                startActivity(new Intent(getActivity(),
                        SidemenuDetailActivity.class).putExtra("type", "homework").
                        putExtra("mode","create"));
//                AddHomeWorkTabFragment addHomeWorkTabFragment=new AddHomeWorkTabFragment();
//                homeWorkActivity.replaceFragment(addHomeWorkTabFragment);


            }
        });

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));

        setupViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
//                if ( position==0){
//                    SubHomeFragment.isLoading=true;
//                    AssignmentHomeFragment.isLoading=false;
//
//                }
//                else {
//                    SubHomeFragment.isLoading=false;
//                    AssignmentHomeFragment.isLoading=true;
//
//                }
                // do your work
            }
        });

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {

        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());

        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        if (userType == 1) {
            ltDsvStudentList.setVisibility(View.GONE);
            dsvStudentList.setVisibility(View.GONE);
            addHomework.setVisibility(View.VISIBLE);
            adapter.addFragment(new SubHomeFragment(), "Home");
            adapter.addFragment(new AssignmentHomeFragment(), "Assignment");
        } else if (userType == 2) {
            getStudentList();
            ltDsvStudentList.setVisibility(View.VISIBLE);
            dsvStudentList.setVisibility(View.VISIBLE);
            addHomework.setVisibility(View.GONE);
            adapter.addFragment(new ParentSubHomeFragment(), "Home");
            adapter.addFragment(new ParentAssignmentHomeFragment(), "Assignment");
        } else {

        }
        viewPager.setAdapter(adapter);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }
        });

    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);

        /*if (sharedPreferences.getInt("ParentHomeWork",0) == 1) {
            dsvStudentList.scrollToPosition(sharedPreferences.getInt("ParentHomeWorkID",0));
        } else {*/
            if (parentsStudListArrayList.size() > 2) {
                dsvStudentList.scrollToPosition(1);
                classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
                sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
                studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
                studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
            } else {
                dsvStudentList.scrollToPosition(0);
                classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
                sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
                studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
                studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
            }
        /*}*/
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                //setupViewPager(viewPager);
                            } else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            //return POSITION_NONE;ParentSubHomeFragment
            //ParentAssignmentHomeFragment
            if (object instanceof ParentSubHomeFragment) {
                ((ParentSubHomeFragment) object).getHomeworkList();
            } else if (object instanceof ParentAssignmentHomeFragment) {
                ((ParentAssignmentHomeFragment) object).getAssignmentList();
            }
            return super.getItemPosition(object);
        }
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();

            homeworkArrayListadd.clear();
            assignmentArrayListadd.clear();

            if (position == 0) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentSubHomeFragment()).attach(new ParentSubHomeFragment()).commit();
            } else if (position == 1) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentAssignmentHomeFragment()).attach(new ParentAssignmentHomeFragment()).commit();
            }
        }
    }

    public static int selected() {
        return studentID;
    }
    public static int selectedSchool() {
        return studentSchoolID;
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}



