package com.school.teacherparent.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.CommentReplyAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.CommentReplyAddedResponse;
import com.school.teacherparent.models.CommentReplyList;
import com.school.teacherparent.models.FeedCommentReplyComposeParam;
import com.school.teacherparent.models.FeedCommentreplyListResponse;
import com.school.teacherparent.models.GetCommentReplyParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 10/19/2018.
 */

public class FeedCommentReplyFragment extends BaseFragment {

    private RecyclerView recyclerView;

    private CommentReplyAdapter commentReplyAdapter;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public FeedCommentReplyFragment() {
        // Required empty public constructor
    }

    int feedId;
    int commID;
    FeedCommentreplyListResponse commentReplyResponse;
    FeedCommentreplyListResponse.feedCommentsList commentReplyList;
    List<CommentReplyList> commentReplyLists;
    TextView noreplyTextview;
    ImageView image_comment_send;
    EditText replyEdittext;

    private boolean loading = true;
    int page_size=10;
    int page_number=1;
    LinearLayoutManager mLayoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_feed_comment_reply, container, false);
        recyclerView=view.findViewById(R.id.reply_recycle);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        commentReplyList= new FeedCommentreplyListResponse.feedCommentsList();
        commentReplyLists = new ArrayList<CommentReplyList>();
        noreplyTextview=view.findViewById(R.id.noreplyTextview);
        image_comment_send=view.findViewById(R.id.image_comment_send);
        replyEdittext=view.findViewById(R.id.replyEdittext);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            feedId = bundle.getInt("feedId", 0);
            commID = bundle.getInt("feedcommentId", 0);

        }
        noreplyTextview.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.reply));
        commentReplyAdapter = new CommentReplyAdapter(commentReplyLists, getContext(), feedId, sharedPreferences.getInt(Constants.USERTYPE,0));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));

         mLayoutManager = new LinearLayoutManager(getActivity());
         recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(commentReplyAdapter);
        getfeedcommentReply();

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        image_comment_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (replyEdittext.getText().toString().trim().length()>0)
                {
                    addFeedCommentReply(replyEdittext.getText().toString().trim());
                }
                else
                {
                    replyEdittext.setError(getString(R.string.replyempty));
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            page_number=page_number+1;
                            getfeedcommentReply();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        return view;
    }


    private void addFeedCommentReply(final String commentDescription)
    {
        if (Util.isNetworkAvailable()) {
            showProgress();

            final FeedCommentReplyComposeParam feedCommentcomposeParam=new FeedCommentReplyComposeParam();
            feedCommentcomposeParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedCommentcomposeParam.setFeedID(String.valueOf(feedId));
            feedCommentcomposeParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedCommentcomposeParam.setCommentDescription(commentDescription);
            feedCommentcomposeParam.setCommentID(String.valueOf(commID));
            feedCommentcomposeParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));

            vidyauraAPI.composeReplyToComment(feedCommentcomposeParam).enqueue(new Callback<CommentReplyAddedResponse>() {
                @Override
                public void onResponse(Call<CommentReplyAddedResponse> call, Response<CommentReplyAddedResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        CommentReplyAddedResponse feedCommentcomposeResponse=response.body();
                        if (feedCommentcomposeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedCommentcomposeResponse.getStatus() == Util.STATUS_SUCCESS) {
                                recyclerView.setVisibility(View.VISIBLE);
                                noreplyTextview.setVisibility(View.GONE);
                                int replyID= Integer.parseInt(feedCommentcomposeResponse.getReplyID());
                                String type = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                                    type = "teachers";
                                } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                                    type = "parents";
                                }
                                commentReplyLists.add(new CommentReplyList(replyID,
                                      feedId,
                                        commentDescription,
                                        sharedPreferences.getString(Constants.FNAME,""),
                                        sharedPreferences.getString(Constants.LNAME,""),
                                        sharedPreferences.getString(Constants.PROFILE_PHOTO,""),
                                        feedCommentcomposeResponse.getReplyCommentTime(),type));
                                commentReplyAdapter.notifyDataSetChanged();
                                Toast.makeText(getContext(),feedCommentcomposeResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                replyEdittext.setText(null);
                            }




                            } else {
                                Toast.makeText(getContext(), feedCommentcomposeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }



                @Override
                public void onFailure(Call<CommentReplyAddedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getfeedcommentReply()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            GetCommentReplyParam feedclapsParam=new GetCommentReplyParam();
            feedclapsParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedclapsParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedclapsParam.setFeedID(String.valueOf(feedId));
            feedclapsParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedclapsParam.setCommentID(String.valueOf(commID));




            vidyauraAPI.getcommentReplyListByfield(sharedPreferences.getString(Constants.USERID,"")
                    , String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    String.valueOf(page_size), String.valueOf(page_number)
                    ,String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)),
                    String.valueOf(feedId),String.valueOf(commID)).enqueue(new Callback<FeedCommentreplyListResponse>() {
                @Override
                public void onResponse(Call<FeedCommentreplyListResponse> call, Response<FeedCommentreplyListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        commentReplyResponse=response.body();
                        if (commentReplyResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (commentReplyResponse.getStatus() == Util.STATUS_SUCCESS) {
                                ArrayList<FeedCommentreplyListResponse.feedCommentsList> data=new ArrayList<>();
                                commentReplyList=commentReplyResponse.getFeedCommentsList();
                                if (commentReplyList.getData().size()>0) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    noreplyTextview.setVisibility(View.GONE);

                                    for (int y = 0; y < commentReplyList.getData().size(); y++) {



                                        commentReplyLists.add(new CommentReplyList(commentReplyList.getData().get(y).getId(),
                                                commentReplyList.getData().get(y).getFeed_id(),
                                                commentReplyList.getData().get(y).getReplyComment(),
                                                commentReplyList.getData().get(y).getFeedReplyedByUserDetails().get(0).getFname(),
                                                commentReplyList.getData().get(y).getFeedReplyedByUserDetails().get(0).getLname(),
                                                commentReplyList.getData().get(y).getFeedReplyedByUserDetails().get(0).getPhoto(),
                                                commentReplyList.getData().get(y).getCreated_at(),
                                                commentReplyList.getData().get(y).getReply_user_type()));
                                    }
                                    loading = true;
                                    commentReplyAdapter.notifyDataSetChanged();

                                }
                                else
                                {
                                    recyclerView.setVisibility(View.GONE);
                                    if (commentReplyLists.size()<=0) {
                                        noreplyTextview.setVisibility(View.VISIBLE);
                                    }
                                    loading = false;
                                }

                            } else {
                               // Toast.makeText(getContext(), commentReplyResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                if (commentReplyLists.size()<=0) {
                                    noreplyTextview.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                                loading = false;
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        loading = false;

                    }
                }

                @Override
                public void onFailure(Call<FeedCommentreplyListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading = false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}
