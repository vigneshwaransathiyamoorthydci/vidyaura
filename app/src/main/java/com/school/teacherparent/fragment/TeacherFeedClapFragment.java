package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.ClapAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FeedClap;
import com.school.teacherparent.models.FeedClapListResponse;
import com.school.teacherparent.models.FeedclapsParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by keerthana on 10/19/2018.
 */

public class TeacherFeedClapFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private EditText comment_edit_text;
    private ImageView post;
    private SwipeRefreshLayout swiper;
    private ClapAdapter feedClapAdapter;
    SwipeRefreshLayout swipe;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public TeacherFeedClapFragment() {
        // Required empty public constructor
    }
    FeedClapListResponse feedClapListResponse;
    int feedId;
    //ArrayList<FeedClapListResponse.feedClapsList.data.feedLikedByUserDetails> feedclapListResponsearray = new ArrayList<FeedClapListResponse.feedClapsList.data.feedLikedByUserDetails>();
    ArrayList<FeedClap> feedClapArrayList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_feed_clap, container, false);
        recyclerView=view.findViewById(R.id.claps_recycle);
        swipe=view.findViewById(R.id.swipe);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            feedId = bundle.getInt("feedId", 0);

        }
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.clap)+"s");
        feedClapAdapter = new ClapAdapter(feedClapArrayList,getContext(),feedId);
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(feedClapAdapter);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SidemenuDetailActivity.title.setText(getResources().getString(R.string.clap)+"s");
                        SidemenuDetailActivity.edittextleaveSearch.setText("");
                        SidemenuDetailActivity.title.setVisibility(View.VISIBLE);
                        SidemenuDetailActivity.edittextleaveSearch.setVisibility(View.GONE);
                        feedClapArrayList.clear();
                        getfeedclapList();
                        swipe.setRefreshing(false);
                    }
                }, 000);
            }
        });

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        SidemenuDetailActivity.ivSearch.setVisibility(View.VISIBLE);
        SidemenuDetailActivity.ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SidemenuDetailActivity.title.setVisibility(View.GONE);
                SidemenuDetailActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(SidemenuDetailActivity.edittextleaveSearch,0);
            }
        });

        SidemenuDetailActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (  SidemenuDetailActivity.edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(  SidemenuDetailActivity.edittextleaveSearch.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        getfeedclapList(SidemenuDetailActivity.edittextleaveSearch.getText().toString().toLowerCase());
                    }

                    return true;
                }
                else
                {
                    Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        getfeedclapList();
        return view;
    }
    private void getfeedclapList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            FeedclapsParam feedclapsParam=new FeedclapsParam();
            feedclapsParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedclapsParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedclapsParam.setFeedID(String.valueOf(feedId));
            feedclapsParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedClapArrayList.clear();



            vidyauraAPI.getfeedClapsList(feedclapsParam).enqueue(new Callback<FeedClapListResponse>() {
                @Override
                public void onResponse(Call<FeedClapListResponse> call, Response<FeedClapListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        feedClapListResponse=response.body();
                        if (feedClapListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedClapListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                //feedclapListResponsearray = feedClapListResponse.getFeedClapsList().getData();
                                if (feedClapListResponse.getFeedClapsList().getData().size() > 0) {
                                    for (int i=0;i<feedClapListResponse.getFeedClapsList().getData().size();i++) {
                                        //feedclapListResponsearray.add(new FeedClapListResponse.feedClapsList.data.feedLikedByUserDetails("","","",""));
                                        feedClapArrayList.add(new FeedClap(feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getFname()+" "+
                                                feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getLname() ,
                                                feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getEmp_photo(),
                                                feedClapListResponse.getFeedClapsList().getData().get(i).getLike_user_type()));
                                    }
                                }
                                /*feedClapAdapter = new ClapAdapter(feedClapArrayList, getContext(),feedId);
                                //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                                recyclerView.setAdapter(feedClapAdapter);*/
                                if (feedClapArrayList.size() > 0) {
                                    feedClapAdapter.notifyDataSetChanged();
                                }
                                /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setHasFixedSize(false);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());

                                recyclerView.setAdapter(feedClapAdapter);*/

                            } else {
                                Toast.makeText(getContext(), feedClapListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<FeedClapListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getfeedclapList(String search)
    {
        if (Util.isNetworkAvailable())
        {
            feedClapArrayList.clear();
            showProgress();
            FeedclapsParam feedclapsParam=new FeedclapsParam();
            feedclapsParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedclapsParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedclapsParam.setFeedID(String.valueOf(feedId));
            feedclapsParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));

            vidyauraAPI.getfeedClapsList(feedclapsParam).enqueue(new Callback<FeedClapListResponse>() {
                @Override
                public void onResponse(Call<FeedClapListResponse> call, Response<FeedClapListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        feedClapListResponse=response.body();
                        if (feedClapListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedClapListResponse.getStatus() == Util.STATUS_SUCCESS) {



                                if (feedClapListResponse.getFeedClapsList().getData().size() > 0) {
                                    for (int i=0;i<feedClapListResponse.getFeedClapsList().getData().size();i++) {
                                        //feedclapListResponsearray.add(new FeedClapListResponse.feedClapsList.data.feedLikedByUserDetails("","","",""));
                                        String name = feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getFname()+" "+
                                                feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getLname();
                                        if (name.toLowerCase().contains(search)) {
                                            feedClapArrayList.add(new FeedClap(feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getFname() + " " +
                                                    feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getLname(),
                                                    feedClapListResponse.getFeedClapsList().getData().get(i).getFeedLikedByUserDetails().get(0).getEmp_photo(),
                                                    feedClapListResponse.getFeedClapsList().getData().get(i).getLike_user_type()));
                                        }
                                    }
                                }
                                /*feedClapAdapter = new ClapAdapter(feedClapArrayList, getContext(),feedId);
                                //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                                recyclerView.setAdapter(feedClapAdapter);
                                feedClapAdapter.notifyDataSetChanged();*/
                                if (feedClapArrayList.size() > 0) {
                                    feedClapAdapter.notifyDataSetChanged();
                                }
                                //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

                                /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setHasFixedSize(false);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());

                                recyclerView.setAdapter(feedClapAdapter);*/

                            } else {
                                Toast.makeText(getContext(), feedClapListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<FeedClapListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    private void hideSoftKeyboard(EditText ettext){
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

}
