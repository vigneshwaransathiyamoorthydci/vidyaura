package com.school.teacherparent.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ResultListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.ResultList;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ResultListParams;
import com.school.teacherparent.models.ResultListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends BaseFragment {


    private Spinner result_spinner;
    private RecyclerView resultListrecyclerView;
    private ResultListAdapter resultListAdapter;
    FloatingActionButton addResult;
    ImageView noti,back;

    public ResultFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ExamTermResponse examTermResponse;
    ResultListResponse resultListResponse;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    int examTermID;
    List<ResultList> resultLists=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    public TextView titleTextview;
    int examterm;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        Bundle b = getArguments();
        examterm= b.getInt("examterm");
        Log.d("VIKISS","examterm-3"+examterm);
        editor = sharedPreferences.edit();
        result_spinner = (Spinner) view.findViewById(R.id.result_spinner);
        resultListrecyclerView = (RecyclerView) view.findViewById(R.id.recycle_result_home);
        addResult =  view.findViewById(R.id.add_result);
        titleTextview=(TextView)view.findViewById(R.id.titleTextview);

        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item_dialog);

        result_spinner.setAdapter(spinnerexamTermAdapter);
        addResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type","result"));
            }
        });
        getExamtermList();
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        resultListrecyclerView.setItemAnimator(new DefaultItemAnimator());
        resultListrecyclerView.setLayoutManager(linearLayoutManager);
        resultListAdapter = new ResultListAdapter(resultLists, getActivity());
        resultListrecyclerView.setAdapter(resultListAdapter);

      
      

        resultListAdapter.setOnClickListen(new ResultListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {

                Fragment fragment = new ResultClassListFragment();
                Bundle bundleau = new Bundle();
                bundleau.putInt("classID", resultLists.get(position).getClass_id());
                bundleau.putInt("sectionID", resultLists.get(position).getSection_id());
                bundleau.putString("altname", resultLists.get(position).getAlt_name());
                bundleau.putString("Section", resultLists.get(position).getSection());
                fragment.setArguments(bundleau);
                replaceFragment(fragment);

                result_spinner.setSelection(0);
                resultLists.clear();
                resultListAdapter.notifyDataSetChanged();
            }


        });



        result_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    examTermID = 0;
                } else {
                    examTermID = examTermResponse.getExamTermsList().get(position - 1).getId();
                    resultLists.clear();
                    resultListAdapter.notifyDataSetChanged();
                    getresultList();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        return view;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    private void getresultList()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();

            ResultListParams resultListParams = new ResultListParams();
            resultListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            resultListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            resultListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            resultListParams.setExamTermID(String.valueOf(examTermID));
            vidyauraAPI.getResultsList(resultListParams).enqueue(new Callback<ResultListResponse>() {
                @Override
                public void onResponse(Call<ResultListResponse> call, Response<ResultListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        resultListResponse = response.body();

                        if (resultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (resultListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (resultListResponse.getResultList().size() > 0) {

                                    for (int i=0;i<resultListResponse.getResultList().size();i++) {
                                        if(resultListResponse.getResultList().get(i).getClassAverage().equals("0"))
                                        {
                                            resultLists.add(new ResultList(resultListResponse.getResultList().get(i).getClass_id(),
                                                    resultListResponse.getResultList().get(i).getSection_id(), resultListResponse.getResultList().get(i).getTotalStudentsCount(),
                                                    resultListResponse.getResultList().get(i).getClassAverage(), 0,
                                                    0,"", "", "",
                                                    resultListResponse.getResultList().get(i).getAlt_name(), resultListResponse.getResultList().get(i).getClassName(),
                                                    resultListResponse.getResultList().get(i).getSection(),
                                                    resultListResponse.getResultList().get(i).getClass_image(),0,"","",""));
                                        }
                                        else
                                        {
                                            if (resultListResponse.getResultList().get(i).getTopperInClass().size()>0) {
                                                resultLists.add(new ResultList(resultListResponse.getResultList().get(i).getClass_id(),
                                                        resultListResponse.getResultList().get(i).getSection_id(), resultListResponse.getResultList().get(i).getTotalStudentsCount(),
                                                        resultListResponse.getResultList().get(i).getClassAverage(), resultListResponse.getResultList().get(i).getTopperInClass().get(0).getTotal_marks(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getStudent_id(), resultListResponse.getResultList().get(i).getTopperInClass().get(0).getFname(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getLname(), resultListResponse.getResultList().get(i).getTopperInClass().get(0).getStudent_photo(),
                                                        resultListResponse.getResultList().get(i).getAlt_name(), resultListResponse.getResultList().get(i).getClassName(), resultListResponse.getResultList().get(i).getSection(),
                                                        resultListResponse.getResultList().get(i).getClass_image(),0,"","",""));

                                            }
                                        }

                                    }

                                    resultListAdapter.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(getContext(), resultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), resultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResultListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            examTermSpinner.clear();
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();
                                    result_spinner.performClick();
                                    if (examterm>0)
                                    {
                                        Log.d("VIKISS","selected-1");
                                        for (int y=0;y<examTermResponse.getExamTermsList().size();y++)
                                        {
                                            if (examterm==examTermResponse.getExamTermsList().get(y).getId())
                                            {
                                                result_spinner.setSelection(y+1);
                                                Log.d("VIKISS","selected"+y+1);
                                                hideSpinnerDropDown(result_spinner);
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
