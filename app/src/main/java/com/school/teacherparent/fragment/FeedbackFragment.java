package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.FaqAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FaqResponse;
import com.school.teacherparent.models.FeedbackDetailsResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/25/2018.
 */

public class FeedbackFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    TextView emailTextview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.feedback));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        emailTextview=(TextView)view.findViewById(R.id.emailTextview);
        getfeedback();
        return view;
    }
    public void getfeedback()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            vidyauraAPI.getFeedback(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0))).enqueue(new Callback<FeedbackDetailsResponse>() {
                @Override
                public void onResponse(Call<FeedbackDetailsResponse> call, Response<FeedbackDetailsResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        FeedbackDetailsResponse feedListResponse = response.body();

                        if (feedListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                FeedbackDetailsResponse.getFeedbackDetails getFAQDetails = feedListResponse.getGetFeedbackDetails();

                                if (getFAQDetails!= null) {

                                emailTextview.setText(getFAQDetails.getEmail());
                                } else {
                                    Toast.makeText(getContext(), feedListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                hideProgress();
                                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedListResponse.getToken()).commit();
                                getfeedback();

                            }

                        } else {
                            Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                        }
                    }
                }




                @Override
                public void onFailure(Call<FeedbackDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}
