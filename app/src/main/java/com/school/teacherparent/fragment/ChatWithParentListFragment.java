package com.school.teacherparent.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.GroupMessageActivity;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.ChatWithAdapter;
import com.school.teacherparent.adapter.StudentsAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.GroupChatResponse;
import com.school.teacherparent.models.GroupListforChatResponse;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.StudentsListforChat;
import com.school.teacherparent.models.StudentsListforChatResponse;
import com.school.teacherparent.models.StudentsParentsListResponse;
import com.school.teacherparent.models.TeachersListforChatResponse;
import com.school.teacherparent.models.UserData;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatWithParentListFragment extends Fragment {

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    SwipeRefreshLayout swipeRefresh;
    RecyclerView rvStudentsList, rvChatWithList/*, rvParentsList*/;
    TextView tvNoStudents, tvStudentName;
    ImageView ivClose;
    ArrayList<MessageList> newUserListArrayList = new ArrayList<>();
    ChatWithAdapter chatWithAdapter;

    ClassListResponse classListResponse;
    ArrayList<String> classlist = new ArrayList<String>();
    @Inject
    public VidyAPI vidyauraAPI;
    ArrayAdapter<String> classspinnerArray;
    private Spinner class_spinner;
    int classID, sectionID;
    StudentsListforChatResponse studentsListforChatResponse;
    StudentsParentsListResponse studentsParentsListResponse;
    TeachersListforChatResponse teachersListforChatResponse;
    GroupListforChatResponse groupListforChatResponse;
    public ArrayList<StudentsListforChatResponse.chatStudentsList> chatStudentsLists = new ArrayList<>();
    List<StudentsListforChatResponse.chatStudentsList> studentLists = new ArrayList<>();
    List<TeachersListforChatResponse.chatTeachersList> chatTeachersList = new ArrayList<>();
    List<GroupChatResponse.groupList> groupList = new ArrayList<>();
    StudentsAdapter studentsAdapter;
    ProgressDialog mDialog;
    int limit;
    int offset = 0;
    boolean loading = false;
    LinearLayoutManager layoutManager;
    public ArrayList<StudentsParentsListResponse.chatStudentsParentsList> chatStudentsParentsListArrayList = new ArrayList<>();
    public ArrayList<TeachersListforChatResponse.chatTeachersList> chatTeachersListArrayList = new ArrayList<>();

    LinearLayout llFather, llMother, llGuardian;
    ImageView ivFatherImage, ivMotherImage, ivGuardianImage;
    TextView tvFatherName, tvFatherRoll, tvMotherName, tvMotherRoll, tvGuardianName, tvGuardianRoll;
    ProgressBar progress;
    String selectChatOption;

    ArrayList<String> TeacherID1 = new ArrayList<>();
    ArrayList<String> TeacherName1 = new ArrayList<>();
    ArrayList<String> TeacherImage1 = new ArrayList<>();
    ArrayList<String> TeacherFcmKey1 = new ArrayList<>();
    ArrayList<String> TeacherRole1 = new ArrayList<>();
    ArrayList<String> TeacherClass1 = new ArrayList<>();
    ArrayList<String> UserList1 = new ArrayList<>();

    ArrayList<UserData> TeacherID = new ArrayList<>();
    ArrayList<UserData> TeacherName = new ArrayList<>();
    ArrayList<UserData> TeacherImage = new ArrayList<>();
    ArrayList<UserData> TeacherFcmKey = new ArrayList<>();
    ArrayList<UserData> TeacherRole = new ArrayList<>();
    ArrayList<UserData> TeacherClass = new ArrayList<>();
    ArrayList<UserData> UserList = new ArrayList<>();

    Menu menu;
    NotificationReceiver notificationReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_with_parent_list, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(true);
        MenuItem items = menu.findItem(R.id.action_notification);
        items.setVisible(true);
        MenuItem items1 = menu.findItem(R.id.action_calender);
        items1.setVisible(false);
    }

    private void initViews(View view) {
        TestDrawerActivity.toolbar.setTitle(getString(R.string.contact_list));
        setHasOptionsMenu(true);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        swipeRefresh = view.findViewById(R.id.swipe_refresh);
        rvStudentsList = view.findViewById(R.id.rv_studentslist);
        tvNoStudents = view.findViewById(R.id.tv_no_students);
        layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvStudentsList.setLayoutManager(mLayoutManager);
        rvStudentsList.setItemAnimator(new DefaultItemAnimator());
        rvStudentsList.setHasFixedSize(true);
        //chatWithAdapter = new ChatWithAdapter(getContext(),newUserListArrayList);
        //rvChatWithList.setAdapter(chatWithAdapter);
        selectChatOption();
        //getClassList();

    }

    private void selectChatOption() {

        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_chat_option);

        ImageView iv_close = dialog.findViewById(R.id.iv_close);
        Button btnParents = dialog.findViewById(R.id.btn_parents);
        Button btnTeachers = dialog.findViewById(R.id.btn_teachers);
        Button btnGroups = dialog.findViewById(R.id.btn_groups);

        dialog.show();

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((TestDrawerActivity) getActivity()).onBackPressed();
            }
        });
        btnParents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentsAdapter = new StudentsAdapter(getContext(), studentLists, 1, ChatWithParentListFragment.this);
                rvStudentsList.setAdapter(studentsAdapter);
                selectChatOption = "Parents";
                dialog.dismiss();
                class_spinner.setVisibility(View.VISIBLE);
                performAction();
                getClassList();
            }
        });
        btnTeachers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentsAdapter = new StudentsAdapter(chatTeachersList, getContext(),2, ChatWithParentListFragment.this);
                rvStudentsList.setAdapter(studentsAdapter);
                selectChatOption = "Teachers";
                performAction();
                getTeacherList();
                dialog.dismiss();
            }
        });
        btnGroups.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentsAdapter = new StudentsAdapter(getContext(),3,groupList, ChatWithParentListFragment.this);
                rvStudentsList.setAdapter(studentsAdapter);
                selectChatOption = "Groups";
                performAction();
                getGroupList();
                dialog.dismiss();
            }
        });

    }

    private void performAction() {
        classspinnerArray = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, classlist);
        classspinnerArray.setDropDownViewResource(R.layout.spinner_item_dialog);
        class_spinner.setAdapter(classspinnerArray);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    classID = classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionID = classListResponse.getTeachersClassesList().get(position - 1).getSectionID();
                    studentsAdapter.notifyDataSetChanged();
                    studentLists.clear();
                    getStudentlist();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        TestDrawerActivity.toolbar.setTitle(getString(R.string.contact_list));
                        switch (selectChatOption) {
                            case "Parents":
                                studentLists.clear();
                                getStudentlist();
                                break;
                            case "Teachers":
                                chatTeachersList.clear();
                                getTeacherList();
                                break;
                            case "Groups":
                                groupList.clear();
                                getGroupList();
                                break;
                        }
                        swipeRefresh.setRefreshing(false);
                    }
                }, 000);
            }
        });

        rvStudentsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            offset = offset + limit;
                            getStudentlist();
                        }
                    }
                }
            }
        });

        studentsAdapter.OnsetClickListen(new StudentsAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                if (selectChatOption.equals("Parents")) {
                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                    getParentsList(studentLists.get(position).getStudID(),
                            studentLists.get(position).getFname() + " " + studentLists.get(position).getLname(),
                            url+studentLists.get(position).getStudent_photo() );
                    //parentDialog(position);
                } else if (selectChatOption.equals("Teachers")) {
                    Intent intent = new Intent(getContext(), MessageActivity.class);
                    intent.putExtra("ReceiverID", chatTeachersList.get(position).getTeacherEncryptedID());
                    intent.putExtra("ReceiverName", chatTeachersList.get(position).getFname()+" "+chatTeachersList.get(position).getLname());
                    intent.putExtra("ReceiverImage", chatTeachersList.get(position).getEmp_photo());
                    intent.putExtra("ReceiverFcmKey", chatTeachersList.get(position).getFcm_key());
                    intent.putExtra("ReceiverRole", chatTeachersList.get(position).getTeacherRole());
                    intent.putExtra("ReceiverClass", chatTeachersList.get(position).getTeacherClass());
                    startActivity(intent);

                } else if (selectChatOption.equals("Groups")) {

                    TeacherID1.clear();
                    TeacherName1.clear();
                    TeacherImage1.clear();
                    TeacherFcmKey1.clear();
                    TeacherRole1.clear();
                    TeacherClass1.clear();
                    UserList1.clear();

                    int pos = groupList.get(position).getDataPosition();
                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_group_path) + "/" ;

                    for (int i = 0; i < groupList.get(position).getTeacherID().size(); i++) {
                        if (pos == groupList.get(position).getTeacherID().get(i).getPosition()) {
                            if (!TeacherID1.contains(groupList.get(position).getTeacherID().get(i).getData())) {
                                TeacherID1.add(groupList.get(position).getTeacherID().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherName().size(); i++) {
                        if (pos == groupList.get(position).getTeacherName().get(i).getPosition()) {
                            if (!TeacherName1.contains(groupList.get(position).getTeacherName().get(i).getData())) {
                                TeacherName1.add(groupList.get(position).getTeacherName().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherImage().size(); i++) {
                        if (pos == groupList.get(position).getTeacherImage().get(i).getPosition()) {
                            if (!TeacherImage1.contains(groupList.get(position).getTeacherImage().get(i).getData())) {
                                TeacherImage1.add(groupList.get(position).getTeacherImage().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherFcmKey().size(); i++) {
                        if (pos == groupList.get(position).getTeacherFcmKey().get(i).getPosition()) {
                            if (!TeacherFcmKey1.contains(groupList.get(position).getTeacherFcmKey().get(i).getData())) {
                                TeacherFcmKey1.add(groupList.get(position).getTeacherFcmKey().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherRole().size(); i++) {
                        if (pos == groupList.get(position).getTeacherRole().get(i).getPosition()) {
                            if (!TeacherRole1.contains(groupList.get(position).getTeacherRole().get(i).getData())) {
                                TeacherRole1.add(groupList.get(position).getTeacherRole().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getTeacherClass().size(); i++) {
                        if (pos == groupList.get(position).getTeacherClass().get(i).getPosition()) {
                            if (!TeacherClass1.contains(groupList.get(position).getTeacherClass().get(i).getData())) {
                                TeacherClass1.add(groupList.get(position).getTeacherClass().get(i).getData());
                            }
                        }
                    }

                    for (int i = 0; i < groupList.get(position).getUserList().size(); i++) {
                        if (pos == groupList.get(position).getUserList().get(i).getPosition()) {
                            if (!UserList1.contains(groupList.get(position).getUserList().get(i).getData())) {
                                UserList1.add(groupList.get(position).getUserList().get(i).getData());
                            }
                        }
                    }

                    Intent intent = new Intent(getContext(), GroupMessageActivity.class);
                    intent.putExtra("GroupName", groupList.get(position).getGroupName());
                    intent.putExtra("GroupImage", url+groupList.get(position).getGroupImage());
                    intent.putExtra("GroupID", groupList.get(position).getGroupID());
                    intent.putStringArrayListExtra("ArrayReceiverID",TeacherID1);
                    intent.putExtra("ArrayReceiverName", Arrays.toString(new ArrayList[]{TeacherName1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayReceiverImage", Arrays.toString(new ArrayList[]{TeacherImage1}).replaceAll("\\[|\\]", ""));
                    intent.putStringArrayListExtra("ArrayReceiverFcmKey", TeacherFcmKey1);
                    intent.putExtra("ArrayReceiverRole", Arrays.toString(new ArrayList[]{TeacherRole1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayReceiverClass", Arrays.toString(new ArrayList[]{TeacherClass1}).replaceAll("\\[|\\]", ""));
                    intent.putExtra("ArrayUserList", Arrays.toString(new ArrayList[]{UserList1}).replaceAll("\\[|\\]", ""));
                    startActivity(intent);
                }
            }

        });

        TestDrawerActivity.edittextleaveSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(TestDrawerActivity.edittextleaveSearch);
            }
        });
        TestDrawerActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase().length() > 0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(TestDrawerActivity.edittextleaveSearch.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        switch (selectChatOption) {
                            case "Parents":
                                studentLists.clear();
                                getSearchStudentlist(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                                break;
                            case "Teachers":
                                chatTeachersList.clear();
                                getSearchTeacherList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                                break;
                            case "Groups":
                                groupList.clear();
                                getSearchGroupList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                                break;
                        }
                    }

                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                if (TestDrawerActivity.edittextleaveSearch.getText().toString().length() <= 0) {
                    TestDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    TestDrawerActivity.toolbar.setTitle("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                    TestDrawerActivity.edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    TestDrawerActivity.edittextleaveSearch.setEnabled(true);
                    TestDrawerActivity.edittextleaveSearch.requestFocus();
                    showKeyboard(TestDrawerActivity.edittextleaveSearch);
                    TestDrawerActivity.edittextleaveSearch.setText("");
                } else {
                    switch (selectChatOption) {
                        case "Parents":
                            studentLists.clear();
                            getSearchStudentlist(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                            break;
                        case "Teachers":
                            chatTeachersList.clear();
                            getSearchTeacherList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                            break;
                        case "Groups":
                            groupList.clear();
                            getSearchGroupList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                            break;
                    }
                }
                editor.putInt(Constants.STATE,2);
                editor.commit();
                return true;

            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            default:
                break;
        }

        return false;
    }

    private void parentDialog(int position) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_parent_list);

        //rvParentsList = dialog.findViewById(R.id.rv_parentslist);
        tvStudentName = dialog.findViewById(R.id.tv_student_name);
        ivClose = dialog.findViewById(R.id.iv_close);
        progress = dialog.findViewById(R.id.progress);

        llFather = dialog.findViewById(R.id.ll_father);
        ivFatherImage = dialog.findViewById(R.id.iv_father_image);
        tvFatherName = dialog.findViewById(R.id.tv_fathername);
        tvFatherRoll = dialog.findViewById(R.id.tv_father_roll);

        llMother = dialog.findViewById(R.id.ll_mother);
        ivMotherImage = dialog.findViewById(R.id.iv_mother_image);
        tvMotherName = dialog.findViewById(R.id.tv_mothername);
        tvMotherRoll = dialog.findViewById(R.id.tv_mother_roll);

        llGuardian = dialog.findViewById(R.id.ll_guardian);
        ivGuardianImage = dialog.findViewById(R.id.iv_guardian_image);
        tvGuardianName = dialog.findViewById(R.id.tv_guardianname);
        tvGuardianRoll = dialog.findViewById(R.id.tv_guardian_roll);

                /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                rvParentsList.setLayoutManager(mLayoutManager);
                rvParentsList.setItemAnimator(new DefaultItemAnimator());
                rvParentsList.setHasFixedSize(true);
                chatWithAdapter = new ChatWithAdapter(getContext(),newUserListArrayList);
                rvParentsList.setAdapter(chatWithAdapter);
                chatWithAdapter.notifyDataSetChanged();*/
        //getParentsList(studentLists.get(position).getStudID(), studentLists.get(position).getFname() + " " + studentLists.get(position).getLname());
        dialog.show();
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        llFather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                Intent intent = new Intent(getContext(), MessageActivity.class);
                intent.putExtra("ReceiverID", studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherEncryptedID());
                intent.putExtra("ReceiverName", studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname());
                intent.putExtra("ReceiverImage", url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getPhoto());
                intent.putExtra("ReceiverFcmKey", studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFcm_key());
                intent.putExtra("ReceiverRole", studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getType());
                intent.putExtra("ReceiverClass", "");
                startActivity(intent);
                dialog.dismiss();
            }
        });
        llMother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                Intent intent = new Intent(getContext(), MessageActivity.class);
                intent.putExtra("ReceiverID", studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherEncryptedID());
                intent.putExtra("ReceiverName", studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname());
                intent.putExtra("ReceiverImage", url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getPhoto());
                intent.putExtra("ReceiverFcmKey", studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFcm_key());
                intent.putExtra("ReceiverRole", studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getType());
                intent.putExtra("ReceiverClass", "");
                startActivity(intent);
                dialog.dismiss();
            }
        });
        llGuardian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                Intent intent = new Intent(getContext(), MessageActivity.class);
                intent.putExtra("ReceiverID", studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianEncryptedID());
                intent.putExtra("ReceiverName", studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                intent.putExtra("ReceiverImage", url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto());
                intent.putExtra("ReceiverFcmKey", studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFcm_key());
                intent.putExtra("ReceiverRole", studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getType());
                intent.putExtra("ReceiverClass", "");
                startActivity(intent);
                dialog.dismiss();
            }
        });
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    //hideProgress();
                    mDialog.dismiss();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
                                    classlist.add(classListResponse.getTeachersClassesList().get(i).getClassName() + "-" + classListResponse.getTeachersClassesList().get(i).getSection());
                                }

                                classspinnerArray.notifyDataSetChanged();
                                class_spinner.performClick();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getStudentlist() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            studentsListforChat.setClassID(String.valueOf(classID));
            studentsListforChat.setSectionID(String.valueOf(sectionID));
            studentsListforChat.setLimit(20);
            studentsListforChat.setOffset(offset);

            vidyauraAPI.getStudentsListforChat(studentsListforChat).enqueue(new Callback<StudentsListforChatResponse>() {
                @Override
                public void onResponse(Call<StudentsListforChatResponse> call, Response<StudentsListforChatResponse> response) {
                    //hideProgress();
                    mDialog.dismiss();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response stud list ==> " + gson.toJson(response.body()));
                        studentsListforChatResponse = response.body();
                        if (studentsListforChatResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            chatStudentsLists = response.body().getChatStudentsList();
                            if (studentsListforChatResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (studentsListforChatResponse.getChatStudentsList().size() != 0) {

                                    for (int y = 0; y < studentsListforChatResponse.getChatStudentsList().size(); y++) {
                                        studentLists.add(new StudentsListforChatResponse.chatStudentsList(studentsListforChatResponse.getChatStudentsList().get(y).getStudID(),
                                                studentsListforChatResponse.getChatStudentsList().get(y).getFname(),
                                                studentsListforChatResponse.getChatStudentsList().get(y).getLname(),
                                                studentsListforChatResponse.getChatStudentsList().get(y).getStudent_photo()));
                                    }

                                    /*studentLists = studentsListforChatResponse.getChatStudentsList();*/
                                    if (studentLists.size() > 0) {
                                        rvStudentsList.setVisibility(View.VISIBLE);
                                        tvNoStudents.setVisibility(View.GONE);
                                        studentsAdapter.notifyDataSetChanged();
                                        loading = true;
                                    } else {
                                        loading = false;
                                        rvStudentsList.setVisibility(View.GONE);
                                        tvNoStudents.setVisibility(View.VISIBLE);
                                        tvNoStudents.setText("No Student Found");
                                    }
                                }

                            } else {
                                loading = false;
                                rvStudentsList.setVisibility(View.GONE);
                                tvNoStudents.setVisibility(View.VISIBLE);
                                tvNoStudents.setText("No Student Found");
                                Toast.makeText(getContext(), studentsListforChatResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StudentsListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getSearchStudentlist(String search) {
        if (Util.isNetworkAvailable()) {
            //showProgress();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            studentsListforChat.setClassID(String.valueOf(classID));
            studentsListforChat.setSectionID(String.valueOf(sectionID));
            studentsListforChat.setLimit(20);
            studentsListforChat.setOffset(offset);

            vidyauraAPI.getStudentsListforChat(studentsListforChat).enqueue(new Callback<StudentsListforChatResponse>() {
                @Override
                public void onResponse(Call<StudentsListforChatResponse> call, Response<StudentsListforChatResponse> response) {
                    //hideProgress();

                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response stud list ==> " + gson.toJson(response.body()));
                        studentsListforChatResponse = response.body();
                        if (studentsListforChatResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            chatStudentsLists = response.body().getChatStudentsList();
                            if (studentsListforChatResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (studentsListforChatResponse.getChatStudentsList().size() != 0) {

                                    for (int y = 0; y < studentsListforChatResponse.getChatStudentsList().size(); y++) {
                                        if (studentsListforChatResponse.getChatStudentsList().get(y).getFname().toLowerCase().contains(search.toLowerCase()) || studentsListforChatResponse.getChatStudentsList().get(y).getLname().toLowerCase().contains(search.toLowerCase())) {
                                            studentLists.add(new StudentsListforChatResponse.chatStudentsList(studentsListforChatResponse.getChatStudentsList().get(y).getStudID(),
                                                    studentsListforChatResponse.getChatStudentsList().get(y).getFname(),
                                                    studentsListforChatResponse.getChatStudentsList().get(y).getLname(),
                                                    studentsListforChatResponse.getChatStudentsList().get(y).getStudent_photo()));
                                        }
                                    }

                                    /*studentLists = studentsListforChatResponse.getChatStudentsList();*/
                                    if (studentLists.size() > 0) {
                                        rvStudentsList.setVisibility(View.VISIBLE);
                                        tvNoStudents.setVisibility(View.GONE);
                                        studentsAdapter.notifyDataSetChanged();
                                        loading = true;
                                    } else {
                                        loading = false;
                                        rvStudentsList.setVisibility(View.GONE);
                                        tvNoStudents.setVisibility(View.VISIBLE);
                                        tvNoStudents.setText("No Student Found");
                                    }
                                }

                            } else {
                                loading = false;
                                rvStudentsList.setVisibility(View.GONE);
                                tvNoStudents.setVisibility(View.VISIBLE);
                                tvNoStudents.setText("No Student Found");
                                Toast.makeText(getContext(), studentsListforChatResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StudentsListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getParentsList(String studID, String studName, String studImage) {

        TeacherID1.clear();
        TeacherName1.clear();
        TeacherImage1.clear();
        TeacherFcmKey1.clear();
        TeacherRole1.clear();
        TeacherClass1.clear();
        UserList1.clear();

        if (Util.isNetworkAvailable()) {
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            studentsListforChat.setStudID(studID);

            vidyauraAPI.getStudentsParentsList(studentsListforChat).enqueue(new Callback<StudentsParentsListResponse>() {
                @Override
                public void onResponse(Call<StudentsParentsListResponse> call, Response<StudentsParentsListResponse> response) {
                    //hideProgress();

                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response stud list ==> " + gson.toJson(response.body()));
                        studentsParentsListResponse = response.body();
                        if (studentsParentsListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            chatStudentsParentsListArrayList = response.body().getChatStudentsParentsList();
                            if (studentsParentsListResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (studentsParentsListResponse.getChatStudentsParentsList().size() != 0) {

                                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                                    String teacherUrl = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/";

                                    TeacherID1.add(sharedPreferences.getString(Constants.USERID,""));
                                    TeacherName1.add(sharedPreferences.getString(Constants.FNAME,"")+" "+sharedPreferences.getString(Constants.LNAME,""));
                                    TeacherImage1.add(teacherUrl+sharedPreferences.getString(Constants.PROFILE_PHOTO,""));
                                    TeacherFcmKey1.add(FirebaseInstanceId.getInstance().getToken());
                                    TeacherRole1.add("Teacher");
                                    TeacherClass1.add("Class");
                                    UserList1.add(studName);
                                    UserList1.add(sharedPreferences.getString(Constants.FNAME,"")+" "+sharedPreferences.getString(Constants.LNAME,""));

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails() != null) {
                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().size() > 0) {
                                        TeacherID1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherEncryptedID());
                                        TeacherName1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname());
                                        TeacherImage1.add(url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getPhoto());
                                        TeacherFcmKey1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFcm_key());
                                        TeacherRole1.add("Father");
                                        TeacherClass1.add("class");
                                        UserList1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname());
                                    }
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails() != null) {
                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().size() > 0) {
                                        TeacherID1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherEncryptedID());
                                        TeacherName1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname());
                                        TeacherImage1.add(url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getPhoto());
                                        TeacherFcmKey1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFcm_key());
                                        TeacherRole1.add("Father");
                                        TeacherClass1.add("class");
                                        UserList1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname());
                                    }
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails() != null) {
                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().size() > 0) {
                                        if (!TeacherID1.contains(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianEncryptedID())) {
                                            TeacherID1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianEncryptedID());
                                            TeacherName1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                            TeacherImage1.add(url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto());
                                            TeacherFcmKey1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFcm_key());
                                            TeacherRole1.add("Father");
                                            TeacherClass1.add("class");
                                            UserList1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                        }
                                        }
                                        /*if (!studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname()
                                                .equals(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname())) {
                                            if (!studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname()
                                                    .equals(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname())) {
                                                TeacherID1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianEncryptedID());
                                                TeacherName1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                                TeacherImage1.add(url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto());
                                                TeacherFcmKey1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFcm_key());
                                                TeacherRole1.add("Father");
                                                TeacherClass1.add("class");
                                                UserList1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                            } else {
                                                TeacherID1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianEncryptedID());
                                                TeacherName1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                                TeacherImage1.add(url+studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto());
                                                TeacherFcmKey1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFcm_key());
                                                TeacherRole1.add("Father");
                                                TeacherClass1.add("class");
                                                UserList1.add(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                            }
                                        }*/
                                    }

                                    Intent intent = new Intent(getContext(), GroupMessageActivity.class);
                                    intent.putExtra("GroupName", studName);
                                    intent.putExtra("GroupImage", studImage);
                                    intent.putExtra("GroupID", studID);
                                    intent.putStringArrayListExtra("ArrayReceiverID",TeacherID1);
                                    intent.putExtra("ArrayReceiverName", Arrays.toString(new ArrayList[]{TeacherName1}).replaceAll("\\[|\\]", ""));
                                    intent.putExtra("ArrayReceiverImage", Arrays.toString(new ArrayList[]{TeacherImage1}).replaceAll("\\[|\\]", ""));
                                    intent.putStringArrayListExtra("ArrayReceiverFcmKey", TeacherFcmKey1);
                                    intent.putExtra("ArrayReceiverRole", Arrays.toString(new ArrayList[]{TeacherRole1}).replaceAll("\\[|\\]", ""));
                                    intent.putExtra("ArrayReceiverClass", Arrays.toString(new ArrayList[]{TeacherClass1}).replaceAll("\\[|\\]", ""));
                                    intent.putExtra("ArrayUserList", Arrays.toString(new ArrayList[]{UserList1}).replaceAll("\\[|\\]", ""));
                                    startActivity(intent);




                                    /*tvStudentName.setText("Parents of " + studentsParentsListResponse.getChatStudentsParentsList().get(0).getFname() + " " + studentsParentsListResponse.getChatStudentsParentsList().get(0).getLname());
                                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                                    RequestOptions error = new RequestOptions()
                                            .placeholder(R.drawable.ic_user)
                                            .error(R.drawable.ic_user)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .priority(Priority.HIGH);
                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().size() > 0) {
                                        llFather.setVisibility(View.VISIBLE);
                                        Glide.with(Objects.requireNonNull(getActivity())).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getPhoto()).apply(error).into(ivFatherImage);
                                        tvFatherName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname());
                                        tvFatherRoll.setText("Father");
                                    } else {
                                        llFather.setVisibility(View.GONE);
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().size() > 0) {
                                        llMother.setVisibility(View.VISIBLE);
                                        Glide.with(getActivity()).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getPhoto()).apply(error).into(ivMotherImage);
                                        tvMotherName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname());
                                        tvMotherRoll.setText("Mother");
                                    } else {
                                        llMother.setVisibility(View.GONE);
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().size() > 0) {
                                        llGuardian.setVisibility(View.VISIBLE);
                                        Glide.with(getActivity()).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto()).apply(error).into(ivGuardianImage);
                                        tvGuardianName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                        tvGuardianRoll.setText("Guardian");
                                    } else {
                                        llGuardian.setVisibility(View.GONE);
                                    }*/

                                } else {
                                    Toast.makeText(getContext(), studentsParentsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getContext(), studentsParentsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StudentsParentsListResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    System.out.println("errorrrr ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            progress.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    /*private void getUserList(String studID, String studName) {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            studentsListforChat.setStudID(studID);

            vidyauraAPI.getStudentsParentsList(studentsListforChat).enqueue(new Callback<StudentsParentsListResponse>() {
                @Override
                public void onResponse(Call<StudentsParentsListResponse> call, Response<StudentsParentsListResponse> response) {
                    //hideProgress();

                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response stud list ==> " + gson.toJson(response.body()));
                        studentsParentsListResponse = response.body();
                        if (studentsParentsListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            chatStudentsParentsListArrayList = response.body().getChatStudentsParentsList();
                            if (studentsParentsListResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (studentsParentsListResponse.getChatStudentsParentsList().size() != 0) {
                                    tvStudentName.setText("Parents of " + studentsParentsListResponse.getChatStudentsParentsList().get(0).getFname() + " " + studentsParentsListResponse.getChatStudentsParentsList().get(0).getLname());
                                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                                    RequestOptions error = new RequestOptions()
                                            .placeholder(R.drawable.ic_user)
                                            .error(R.drawable.ic_user)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .priority(Priority.HIGH);
                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().size() > 0) {
                                        llFather.setVisibility(View.VISIBLE);
                                        Glide.with(Objects.requireNonNull(getActivity())).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getPhoto()).apply(error).into(ivFatherImage);
                                        tvFatherName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getFatherDetails().get(0).getFname());
                                        tvFatherRoll.setText("Father");
                                    } else {
                                        llFather.setVisibility(View.GONE);
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().size() > 0) {
                                        llMother.setVisibility(View.VISIBLE);
                                        Glide.with(getActivity()).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getPhoto()).apply(error).into(ivMotherImage);
                                        tvMotherName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getMotherDetails().get(0).getFname());
                                        tvMotherRoll.setText("Mother");
                                    } else {
                                        llMother.setVisibility(View.GONE);
                                    }

                                    if (studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().size() > 0) {
                                        llGuardian.setVisibility(View.VISIBLE);
                                        Glide.with(getActivity()).load(url + studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getPhoto()).apply(error).into(ivGuardianImage);
                                        tvGuardianName.setText(studentsParentsListResponse.getChatStudentsParentsList().get(0).getGuardianDetails().get(0).getFname());
                                        tvGuardianRoll.setText("Guardian");
                                    } else {
                                        llGuardian.setVisibility(View.GONE);
                                    }

                                    mDialog.dismiss();
                                    progress.setVisibility(View.GONE);

                                } else {
                                    progress.setVisibility(View.GONE);
                                    tvNoStudents.setVisibility(View.VISIBLE);
                                }

                            } else {
                                loading = false;
                                *//*rvParentsList.setVisibility(View.GONE);*//*
                                //tvNoStudents.setVisibility(View.VISIBLE);
                                tvNoStudents.setText("No Parents Found");
                                mDialog.dismiss();
                                progress.setVisibility(View.GONE);
                                Toast.makeText(getContext(), studentsParentsListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        mDialog.dismiss();
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<StudentsParentsListResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    progress.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            progress.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

        *//*if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
            newUserListArrayList.add(new MessageList("Sathyamoorthy", "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/uploads/employee/",
                    "dWY5a2xPaXE1Mm44VUpUVWlBRGdNUT09",
                    "flQTaidhgBE:APA91bHM6dp9gq9CjxK4JMg_qmZvKiMC24I2hM2voHKkFqKP0rVW7hcJSnIj4tbp2s_smH2oPG-WUH5kbjVdCAvKyKeBcUB7_WOc5vQloMJUwL3ox3NRt8Msj_b2DNLB48H5cuDgIEaw",
                    "Parent", "Class 6B"));
        } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
            newUserListArrayList.add(new MessageList("Vignesh TS", "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/uploads/employee/a190a964-ce05-47ce-8b41-290874a1c863.png",
                    "WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09",
                    "fHnVFLw_sOw:APA91bFvGqKXRYpzSjvWuTyTerbjLGlZzM_bbfqwcci70OMbIc2SOvQgGAOe5OvcdTxjfqfKdE3fEvrSVbfbykOgotzV2vzTT_lY086eKHb83FOcj--iQlda_30SktnMFG27GChSNBxo",
                    "Maths Teacher", "Class 5A"));
        }

        tvStudentName.setText("Parents of\n"+studName);
        if (newUserListArrayList.size() > 0) {
            chatWithAdapter.notifyDataSetChanged();
        }*//*

    }*/

    private void getTeacherList() {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getTeachersListforChat(studentsListforChat).enqueue(new Callback<TeachersListforChatResponse>() {
                @Override
                public void onResponse(Call<TeachersListforChatResponse> call, Response<TeachersListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response teachers list ==> " + gson.toJson(response.body()));
                        teachersListforChatResponse = response.body();
                        for (int i=0;i<teachersListforChatResponse.getChatTeachersList().size();i++) {
                            if (!teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID().equals(sharedPreferences.getString(Constants.USERID,null)))
                            chatTeachersList.add(new TeachersListforChatResponse.chatTeachersList(
                                    teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getFname(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getLname(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getEmp_photo(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getFcm_key(),
                                    teachersListforChatResponse.getChatTeachersList().get(i).getTeacherID(),
                                    "",""));
                        }

                        if (chatTeachersList.size() > 0) {
                            rvStudentsList.setVisibility(View.VISIBLE);
                            tvNoStudents.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvStudentsList.setVisibility(View.GONE);
                            tvNoStudents.setVisibility(View.VISIBLE);
                            tvNoStudents.setText("No Teachers Found");
                        }


                    } else {
                        mDialog.dismiss();
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TeachersListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    progress.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            progress.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getSearchTeacherList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getTeachersListforChat(studentsListforChat).enqueue(new Callback<TeachersListforChatResponse>() {
                @Override
                public void onResponse(Call<TeachersListforChatResponse> call, Response<TeachersListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("response teachers list ==> " + gson.toJson(response.body()));
                        teachersListforChatResponse = response.body();
                        for (int i = 0; i < teachersListforChatResponse.getChatTeachersList().size(); i++) {
                            if (!teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID().equals(sharedPreferences.getString(Constants.USERID, null))) {
                                String name = teachersListforChatResponse.getChatTeachersList().get(i).getFname() + " " + teachersListforChatResponse.getChatTeachersList().get(i).getLname();
                                if (name.toLowerCase().contains(search))
                                    chatTeachersList.add(new TeachersListforChatResponse.chatTeachersList(
                                            teachersListforChatResponse.getChatTeachersList().get(i).getTeacherEncryptedID(),
                                            teachersListforChatResponse.getChatTeachersList().get(i).getFname(),
                                            teachersListforChatResponse.getChatTeachersList().get(i).getLname(),
                                            teachersListforChatResponse.getChatTeachersList().get(i).getEmp_photo(),
                                            teachersListforChatResponse.getChatTeachersList().get(i).getFcm_key(),
                                            teachersListforChatResponse.getChatTeachersList().get(i).getTeacherID(),
                                            "", ""));
                            }
                        }

                        if (chatTeachersList.size() > 0) {
                            rvStudentsList.setVisibility(View.VISIBLE);
                            tvNoStudents.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvStudentsList.setVisibility(View.GONE);
                            tvNoStudents.setVisibility(View.VISIBLE);
                            tvNoStudents.setText("No Teachers Found");
                        }


                    } else {
                        mDialog.dismiss();
                        progress.setVisibility(View.GONE);
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TeachersListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    progress.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            progress.setVisibility(View.GONE);
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getGroupList() {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            groupList.clear();
            TeacherID.clear();
            TeacherName.clear();
            TeacherImage.clear();
            TeacherFcmKey.clear();
            TeacherRole.clear();
            TeacherClass.clear();
            UserList.clear();
            vidyauraAPI.getGroupsListforChat(studentsListforChat).enqueue(new Callback<GroupListforChatResponse>() {
                @Override
                public void onResponse(Call<GroupListforChatResponse> call, Response<GroupListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        groupListforChatResponse = response.body();
                        Gson gson = new Gson();
                        System.out.println("response group list ==> " + gson.toJson(response.body()));
                        if (groupListforChatResponse.getChatGroupList() != null)
                        if (groupListforChatResponse.getChatGroupList().size()>0)
                        for (int i=0;i<groupListforChatResponse.getChatGroupList().size();i++) {

                            for (int j=0;j<groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().size();j++) {
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getLname()));
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID())) {*/
                                    //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID());
                                    TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID()));
                                    if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails() != null) {
                                        TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                        UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                        TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getPhoto()));
                                        TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFcm_key()));
                                    }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID())) {*/
                                    TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID()));
                                    //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID());
                                    if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails() != null) {
                                        TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                        UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                        TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getPhoto()));
                                        TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFcm_key()));
                                    }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID())) {*/
                                    TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID()));
                                    //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID());
                                    if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails() != null) {
                                        TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                        UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                        TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getPhoto()));
                                        TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFcm_key()));
                                    }
                                /*}*/

                            }

                            for (int k=0;k<groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().size();k++) {
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID())) {*/
                                    TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID()));
                                    //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID());
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getEmp_photo()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFcm_key()));
                                    UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                /*}*/
                            }


                            //TeacherID.add("WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09");
                            //TeacherName.add("Manoj");
                            //TeacherImage.add("");
                            //TeacherFcmKey.add("c_FvOxhgVwg:APA91bFUbC9yyEpMb5A5EZ64yLayQYms6YWwsn8IGNbnYP00zb_RyYcpR_yLhHDXlSRdA--ldLs-1jzKrvOsMp6YxQXxcEq3P0IaIz6_mWDXRKpvvSHtMH3BuKuTJmgZ7odhwy10A-cT");
                            TeacherRole.add(new UserData(i,"TeacherRole"));
                            TeacherClass.add(new UserData(i,"TeacherClass"));


                            groupList.add(new GroupChatResponse.groupList(i,groupListforChatResponse.getChatGroupList().get(i).getGroup_name(),
                                    groupListforChatResponse.getChatGroupList().get(i).getGroupicon(),
                                    groupListforChatResponse.getChatGroupList().get(i).getEncryptedGroupID(),
                                    TeacherID,
                                    TeacherName,
                                    TeacherImage,
                                    TeacherFcmKey,
                                    TeacherRole,
                                    TeacherClass,
                                    UserList));

                        }

                        if (groupList.size() > 0) {
                            for (int i=0;i<groupList.size();i++) {
                                System.out.println("response list ==> " + gson.toJson(groupList.get(i).getTeacherID()));
                            }
                            rvStudentsList.setVisibility(View.VISIBLE);
                            tvNoStudents.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvStudentsList.setVisibility(View.GONE);
                            tvNoStudents.setVisibility(View.VISIBLE);
                            tvNoStudents.setText("No Groups Found");
                        }

                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<GroupListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    System.out.println("Error ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

        /*ArrayList<String> TeacherID = new ArrayList<>();
        ArrayList<String> TeacherName = new ArrayList<>();
        ArrayList<String> TeacherImage = new ArrayList<>();
        ArrayList<String> TeacherFcmKey = new ArrayList<>();
        ArrayList<String> TeacherRole = new ArrayList<>();
        ArrayList<String> TeacherClass = new ArrayList<>();

        TeacherID.add("WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09");
        TeacherName.add("Manoj");
        TeacherImage.add("");
        TeacherFcmKey.add("c_FvOxhgVwg:APA91bFUbC9yyEpMb5A5EZ64yLayQYms6YWwsn8IGNbnYP00zb_RyYcpR_yLhHDXlSRdA--ldLs-1jzKrvOsMp6YxQXxcEq3P0IaIz6_mWDXRKpvvSHtMH3BuKuTJmgZ7odhwy10A-cT");
        TeacherRole.add("English Teacher");
        TeacherClass.add("Grade 1A");

        TeacherID.add("NExIdm1hUFh3T0pvaHZzandNN0dLZz09");
        TeacherName.add("Daniel");
        TeacherImage.add("https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/uploads/employee/3f48fd36-6348-4fab-ae85-6e0ede5a673b.png");
        TeacherFcmKey.add(FirebaseInstanceId.getInstance().getToken());
        TeacherRole.add("Maths Teacher");
        TeacherClass.add("Grade 1C");

        TeacherID.add("dWY5a2xPaXE1Mm44VUpUVWlBRGdNUT09");
        TeacherName.add("Vignesh");
        TeacherImage.add("");
        TeacherFcmKey.add("egPRmtJSg4o:APA91bG3VTHY3nzxlBJa23Ila64g16mki9jNjRz_dmNgnnj0iVkD33tWxGqEtwA93jH-2g2mPpS660mknSFs3Pv_KHQU4PvlWsFneLkJ8LbgTEs4rTKEiFpqPi3968CxnA-c_unw1DzK");
        TeacherRole.add("Parent");
        TeacherClass.add("Grade 1B");

        groupList.add(new GroupChatResponse.groupList("Parents Meeting", "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/uploads/employee/3f48fd36-6348-4fab-ae85-6e0ede5a673b.png",
                "VUpUVWlBRGdNUT09dWY5a2xPaXE1Mm44",TeacherID, TeacherName, TeacherImage, TeacherFcmKey, TeacherRole, TeacherClass));

        if (groupList.size() > 0) {
            rvStudentsList.setVisibility(View.VISIBLE);
            tvNoStudents.setVisibility(View.GONE);
            studentsAdapter.notifyDataSetChanged();
        } else {
            rvStudentsList.setVisibility(View.GONE);
            tvNoStudents.setVisibility(View.VISIBLE);
            tvNoStudents.setText("No Groups Found");
        }*/
    }

    private void getSearchGroupList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            StudentsListforChat studentsListforChat = new StudentsListforChat();
            studentsListforChat.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            studentsListforChat.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            studentsListforChat.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            groupList.clear();
            TeacherID.clear();
            TeacherName.clear();
            TeacherImage.clear();
            TeacherFcmKey.clear();
            TeacherRole.clear();
            TeacherClass.clear();
            UserList.clear();
            vidyauraAPI.getGroupsListforChat(studentsListforChat).enqueue(new Callback<GroupListforChatResponse>() {
                @Override
                public void onResponse(Call<GroupListforChatResponse> call, Response<GroupListforChatResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        groupListforChatResponse = response.body();
                        Gson gson = new Gson();
                        System.out.println("response group list ==> " + gson.toJson(response.body()));
                        if (groupListforChatResponse.getChatGroupList().size()>0)
                        for (int i=0;i<groupListforChatResponse.getChatGroupList().size();i++) {

                            for (int j=0;j<groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().size();j++) {
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getLname()));
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID())) {*/
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID());
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherEncryptedID()));
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getFatherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getMotherDetails().getFcm_key()));
                                }
                                /*}*/
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianEncryptedID());
                                if (groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails() != null) {
                                    TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFname()));
                                    TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getPhoto()));
                                    TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStudentDetails().get(j).getGuardianDetails().getFcm_key()));
                                }
                                /*}*/

                            }

                            for (int k=0;k<groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().size();k++) {
                                /*if (!TeacherID1.contains(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID())) {*/
                                TeacherID.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID()));
                                //TeacherID1.add(groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getTeacherEncryptedID());
                                TeacherName.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                TeacherImage.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getEmp_photo()));
                                TeacherFcmKey.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFcm_key()));
                                UserList.add(new UserData(i,groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getFname()+" "+groupListforChatResponse.getChatGroupList().get(i).getStaffDetails().get(k).getLname()));
                                /*}*/
                            }


                            //TeacherID.add("WlNRa0JTSVRBZjY2R1dYWGl0eVYrUT09");
                            //TeacherName.add("Manoj");
                            //TeacherImage.add("");
                            //TeacherFcmKey.add("c_FvOxhgVwg:APA91bFUbC9yyEpMb5A5EZ64yLayQYms6YWwsn8IGNbnYP00zb_RyYcpR_yLhHDXlSRdA--ldLs-1jzKrvOsMp6YxQXxcEq3P0IaIz6_mWDXRKpvvSHtMH3BuKuTJmgZ7odhwy10A-cT");
                            TeacherRole.add(new UserData(i,"TeacherRole"));
                            TeacherClass.add(new UserData(i,"TeacherClass"));

                            if (groupListforChatResponse.getChatGroupList().get(i).getGroup_name().toLowerCase().contains(search)) {

                                groupList.add(new GroupChatResponse.groupList(i,groupListforChatResponse.getChatGroupList().get(i).getGroup_name(),
                                        groupListforChatResponse.getChatGroupList().get(i).getGroupicon(),
                                        groupListforChatResponse.getChatGroupList().get(i).getEncryptedGroupID(),
                                        TeacherID,
                                        TeacherName,
                                        TeacherImage,
                                        TeacherFcmKey,
                                        TeacherRole,
                                        TeacherClass,
                                        UserList));
                            }

                        }

                        if (groupList.size() > 0) {
                            for (int i=0;i<groupList.size();i++) {
                                System.out.println("response list ==> " + gson.toJson(groupList.get(i).getTeacherID()));
                            }
                            rvStudentsList.setVisibility(View.VISIBLE);
                            tvNoStudents.setVisibility(View.GONE);
                            studentsAdapter.notifyDataSetChanged();
                        } else {
                            rvStudentsList.setVisibility(View.GONE);
                            tvNoStudents.setVisibility(View.VISIBLE);
                            tvNoStudents.setText("No Groups Found");
                        }

                    } else {
                        mDialog.dismiss();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<GroupListforChatResponse> call, Throwable t) {
                    //hideProgress();
                    mDialog.dismiss();
                    System.out.println("Error ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void teacherInfo(int position) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_teacher_info);

        ImageView ivClose = dialog.findViewById(R.id.iv_close);
        ImageView iv_group_image = dialog.findViewById(R.id.iv_group_image);
        TextView tv_group_name = dialog.findViewById(R.id.tv_group_name);
        TextView tv_stafflist = dialog.findViewById(R.id.tv_stafflist);
        TextView tv_studentlist = dialog.findViewById(R.id.tv_studentlist);

        RequestOptions error = new RequestOptions()
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        String url = getString(R.string.s3_baseurl) + getString(R.string.s3_group_path) + "/" + groupListforChatResponse.getChatGroupList().get(position).getGroupicon();
        Glide.with(getContext()).load(url).apply(error).into(iv_group_image);
        tv_group_name.setText(groupListforChatResponse.getChatGroupList().get(position).getGroup_name());

        StringBuilder staffname = new StringBuilder();
        StringBuilder studentname = new StringBuilder();
        for (int j = 0; j < groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().size(); j++) {
            staffname.append(groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(position).getStaffDetails().get(j).getLname()+"\n");
        }
        for (int j = 0; j < groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().size(); j++) {
            studentname.append(groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().get(j).getFname()+" "+groupListforChatResponse.getChatGroupList().get(position).getStudentDetails().get(j).getLname()+"\n");
        }
        tv_stafflist.setText(staffname.toString());
        tv_studentlist.setText(studentname.toString());

        dialog.show();
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showKeyboard(final EditText ettext) {
        ettext.requestFocus();
        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 200);
    }

    private void hideSoftKeyboard(EditText ettext) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

    protected String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
