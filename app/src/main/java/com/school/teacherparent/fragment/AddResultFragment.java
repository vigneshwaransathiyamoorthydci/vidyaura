package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddResultAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedParams;
import com.school.teacherparent.models.AddFeedResponse;

import com.school.teacherparent.R;
import com.school.teacherparent.models.AddResultParams;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.ClassListforResultResponse;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.GetClassForResultparam;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.GetSubjectListforResultParam;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ResultSubjectListResponse;
import com.school.teacherparent.models.StudentListParams;
import com.school.teacherparent.models.StudentListforResultResponse;
import com.school.teacherparent.models.StudentResultList;
import com.school.teacherparent.models.SubjectListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddResultFragment extends BaseFragment {


    private ImageView back;
    private Spinner class_spinner, sub_spinner, exam_spinner;
    private RecyclerView studentListrecyclerview;
    private Button result_btn;
    TextView add_result;

    private AddResultAdapter studentListAdapter;

    public AddResultFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    ExamTermResponse examTermResponse;
       int examTermID=0,subjectId=0,classId=0,sectionId=0;
    ArrayList<String> classlist = new ArrayList<String>();
    ClassListforResultResponse classListResponse;
    ArrayAdapter<String> classspinnerArray;
    ArrayList<String> subjectlist = new ArrayList<String>();
    ArrayAdapter<String> subjectspinnerArray;
    ResultSubjectListResponse subjectListResponse;
    LinearLayoutManager linearLayoutManager;
    StudentListforResultResponse studentListforResultResponse;
    ArrayList<StudentResultList> studentResultLists=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_result, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        back = (ImageView) view.findViewById(R.id.back);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_result));
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        exam_spinner = (Spinner) view.findViewById(R.id.exam_spinner);
        studentListrecyclerview = (RecyclerView) view.findViewById(R.id.recycle_add_result);
        add_result = view.findViewById(R.id.add_result);
        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(spinnerexamTermAdapter);
        studentListAdapter = new AddResultAdapter(studentResultLists, getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        studentListrecyclerview.setItemAnimator(new DefaultItemAnimator());
        studentListrecyclerview.setLayoutManager(linearLayoutManager);
        studentListrecyclerview.setAdapter(studentListAdapter);

        getExamtermList();
        add_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(), "Added Successfully", Toast.LENGTH_SHORT).show();
                //getActivity().finish();
                //  onBackPressed();
                /*Intent intent=new Intent(getApplicationContext(),ClassworkActivity.class);
                startActivity(intent);*/
                boolean isAdd=false;
                if (studentResultLists.size() > 0) {

                    for (int i = 0; i < studentResultLists.size(); i++) {
                        if (studentResultLists.get(i).getMark() != 0) {
                            isAdd=true;
                            break;
                        }
                    }
                    if (isAdd) {
                        JSONObject obj = null;
                        JSONArray jsonArray = new JSONArray();
                        for (int i = 0; i < studentResultLists.size(); i++) {
                            obj = new JSONObject();
                            try {
                                obj.put("studMark", studentResultLists.get(i).getMark());
                                obj.put("studID", studentResultLists.get(i).getStudID());


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            jsonArray.put(obj);
                        }

                        JSONObject finalobject = new JSONObject();
                        try {
                            finalobject.put("studMarksList", jsonArray);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        addstudentResult(jsonArray.toString());
                    }
                    else

                    {
                        Toast.makeText(getContext(),"Please Enter atleast one student Mark",Toast.LENGTH_SHORT).show();
                    }
                }

            }
                });


        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        classlist.add("Select Class");
        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);
        //getClassList();
        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);
        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    classId = classListResponse.getResultClassesList().get(position - 1).getClassID();
                    sectionId = classListResponse.getResultClassesList().get(position - 1).getSectionID();
                    if (classId>0 && sectionId >0 && examTermID>0) {
                        studentResultLists.clear();
                        studentListAdapter.notifyDataSetChanged();
                        subjectlist.clear();
                        subjectspinnerArray.notifyDataSetChanged();
                        getSubjecyList();
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });



        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        subjectlist.add("Select Subject");
        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subjectspinnerArray);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    subjectId = subjectListResponse.getResultSubjectsList().get(position - 1).getSubject_id();
                    studentResultLists.clear();
                    getStudentList();

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    examTermID = 0;
                } else {
                    examTermID = examTermResponse.getExamTermsList().get(position - 1).getId();
                    studentResultLists.clear();
                    studentListAdapter.notifyDataSetChanged();
                    subjectlist.clear();
                    subjectspinnerArray.notifyDataSetChanged();
                    classlist.clear();
                    classspinnerArray.notifyDataSetChanged();
                    getClassesForResults();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        return view;
    }

    private void addstudentResult(String studentList) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddResultParams addResultParams = new AddResultParams();
            addResultParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addResultParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addResultParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addResultParams.setClassID(String.valueOf(classId));
            addResultParams.setSectionID(String.valueOf(sectionId));
            addResultParams.setSubjectID(String.valueOf(subjectId));
            addResultParams.setExamTermID(String.valueOf(examTermID));
            addResultParams.setStudMarksList(studentList.trim());
            vidyauraAPI.addStudentResultfield(classId,String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)),
                    String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    sectionId,sharedPreferences.getString(Constants.USERID,""),examTermID,
                    studentList,subjectId).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            examTermSpinner.clear();
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();


                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getClassesForResults() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add("Select Class");
            GetClassForResultparam getClassForResultparam = new GetClassForResultparam();
            getClassForResultparam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getClassForResultparam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getClassForResultparam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getClassForResultparam.setExamTermID(String.valueOf(examTermID));
            vidyauraAPI.getClassesForResults(getClassForResultparam).enqueue(new Callback<ClassListforResultResponse>() {
                @Override
                public void onResponse(Call<ClassListforResultResponse> call, Response<ClassListforResultResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getResultClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(classListResponse.getResultClassesList().get(i).getClassName()+"-"+
                                            classListResponse.getResultClassesList().get(i).getSection());

                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();

                                classspinnerArray.notifyDataSetChanged();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListforResultResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }




    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            subjectlist.clear();
            subjectlist.add("Select Subject");
            GetSubjectListforResultParam getSubjectListParam = new GetSubjectListforResultParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            getSubjectListParam.setExamTermID(String.valueOf(examTermID));
            vidyauraAPI.getSubjectsForResults(getSubjectListParam).enqueue(new Callback<ResultSubjectListResponse>() {
                @Override
                public void onResponse(Call<ResultSubjectListResponse> call, Response<ResultSubjectListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        subjectListResponse = response.body();
                        if (subjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (subjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < subjectListResponse.getResultSubjectsList().size(); i++) {

                                    subjectlist.add(subjectListResponse.getResultSubjectsList().get(i).getName());

                                }
//                                subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                //                              classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                //chapter_Id=examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();
                                subjectspinnerArray.notifyDataSetChanged();
                                // getExamchapterList();




                            } else {
                                Toast.makeText(getContext(), classListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResultSubjectListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            StudentListParams getSubjectListParam = new StudentListParams();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            vidyauraAPI.getStudentsListForResults(getSubjectListParam).enqueue(new Callback<StudentListforResultResponse>() {
                @Override
                public void onResponse(Call<StudentListforResultResponse> call, Response<StudentListforResultResponse> response) {
                    hideProgress();
                    if (response.body() != null) {

                        studentListforResultResponse = response.body();
                        if (studentListforResultResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (studentListforResultResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < studentListforResultResponse.getResultStudentsList().size(); i++) {

                                    studentResultLists.add(new StudentResultList(studentListforResultResponse.getResultStudentsList().get(i).getStudID(),
                                            studentListforResultResponse.getResultStudentsList().get(i).getFname(),studentListforResultResponse.getResultStudentsList().get(i).getLname(),
                                            studentListforResultResponse.getResultStudentsList().get(i).getStudent_photo(),0));

                                }

                                studentListAdapter.notifyDataSetChanged();



                            } else {
                                Toast.makeText(getContext(), studentListforResultResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }




                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    }


                @Override
                public void onFailure(Call<StudentListforResultResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}
