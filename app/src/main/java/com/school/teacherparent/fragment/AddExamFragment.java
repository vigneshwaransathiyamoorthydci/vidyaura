package com.school.teacherparent.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;

import com.school.teacherparent.adapter.ExamComposechapterMultiSelectionAdpater;
import com.school.teacherparent.adapter.ExamComposetopicAdpater;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.R;
import com.school.teacherparent.models.AddExambyClassParam;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ChapterListResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.ExamDetailByIdResponse;
import com.school.teacherparent.models.ExamDetailsParams;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.ExamTypeResponse;
import com.school.teacherparent.models.GetChapterListParam;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.GetTopicListParam;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofChapterforDialog;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.ListofTopicforDialog;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.models.SubjectListResponse;
import com.school.teacherparent.models.TopicListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddExamFragment extends BaseFragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {
    private SimpleDateFormat simpleDateFormat,dateFormatwithseconds;
    private static int selectedDate = 1;
    private Calendar currentD;
    private Calendar calendardate;
    String formattedDate;
    private RecyclerView recyclerView;

    private Spinner exam_term_spinner, class_spinner, subject_spinner;
    private RelativeLayout relative_class_spinner, relative_sub_spinner;
    private TextView add_exm_btn;

    ExamTermResponse examTermResponse;
    ChapterListResponse chapterListResponse;
    TopicListResponse topicListResponse;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    ArrayList<String> classlist = new ArrayList<String>();
    ArrayAdapter<String> classspinnerArray;
    int examTermId = 0, classId = 0, sectionId = 0, subject_Id = 0, chapter_Id = 0, topic_Id = 0;
    ClassListResponse classListResponse;
    SubjectListResponse subjectListResponse;
    ExamTypeResponse examTypeResponse;
    ArrayAdapter<String> subjectspinnerArray;
    ArrayList<String> subjectlist = new ArrayList<String>();
    ArrayAdapter<String> chapterspinnerArray;
    ArrayList<String> chapterlist = new ArrayList<String>();

    ArrayAdapter<String> topicspinnerArray;
    ArrayList<String> topiclist = new ArrayList<String>();
    LinearLayout linear_examdate, linear_starttime, linear_endtime;
    TextView examdate_textview, examstarttime_textview, examendtime_textview;
    private String eventStartDate, eventStartTime, eventStartDateFormat, eventEndDateFormat;
    private String eventEndDate, eventEndTime;
    private Calendar calendarStartTime, calendarEndTime;
    private Calendar currentT;
    private SimpleDateFormat simpleDateFormat2;
    String examDuration;
    private MultiSpinner chaptermultiSpinner,topicmultiSpinner;
    Spinner examSpinner;
    TextView minTextview,maxTextview;
    ArrayList<Integer> selectedChapter=new ArrayList<>();


    ArrayList<Integer> selectedTopic=new ArrayList<>();
    List<ListofTopicforDialog> topicList = new ArrayList<ListofTopicforDialog>();
    String lastchapterID="",lasttopicId="";
    int visibility=1;
    ArrayAdapter<String> examtypespinnerArray;
    ArrayList<String> examtypelist = new ArrayList<String>();
    int examID;
    ExamDetailByIdResponse examDetailByIdResponse;
    boolean[] selectedchapterItemsupdate;
    boolean[] selectedtopicItemsupdate;
    List<ListofClass> chapterListUpdate = new ArrayList<ListofClass>();

    String topicID = "";
    Dialog chapterDialog,topicDialog;
    ListView topiclistView,chapterlistView;
    ExamComposetopicAdpater examComposetopicAdpater;
    ExamComposechapterMultiSelectionAdpater examComposechapterAdpater;
    SearchView dialogSearchView,chapterdialogSearchView;
    TextView topicnameTextview,searchnameTextview,chapternameTextview,chaptersearchnameTextview,topicsearchnameTextview;
    Button dialogTopicButton,dialogChapterButton;
    static List<ListofChapterforDialog> chapterList = new ArrayList<ListofChapterforDialog>();
    List<ListofChapterforDialog> searchchapterList = new ArrayList<ListofChapterforDialog>();
    String chapterID="";
    List<ListofTopicforDialog> searchtopicList = new ArrayList<ListofTopicforDialog>();
    String examStartdate,examEnddate;
    String selectedstartTime,selectedendTime;
    CheckBox selectAllCheckbox;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_exam_new, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_exam));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        exam_term_spinner = (Spinner) view.findViewById(R.id.exam_type_spinner);
        subject_spinner = (Spinner) view.findViewById(R.id.subject_spinner);
        examSpinner = (Spinner) view.findViewById(R.id.aceadmic_spinner);
        chaptermultiSpinner=(MultiSpinner)view.findViewById(R.id.chapter_spinner);
        topicmultiSpinner=(MultiSpinner)view.findViewById(R.id.topic_spinner);
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        add_exm_btn = (TextView) view.findViewById(R.id.add_exm_btn);
        minTextview=(TextView)view.findViewById(R.id.minTextview);
        minTextview.setTypeface(ProximaNovaFont.getInstance(getContext()).getLightTypeFace());
        maxTextview=(TextView)view.findViewById(R.id.maxTextview);
        maxTextview.setTypeface(ProximaNovaFont.getInstance(getContext()).getLightTypeFace());
        linear_examdate = (LinearLayout) view.findViewById(R.id.linear_examdate);
        linear_examdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (examTermId!=0)
                {
                    datePickerDialog();
                }
                else
                {
                    Toast.makeText(getContext(),getString(R.string.selectexamterm),Toast.LENGTH_SHORT).show();
                }

            }
        });


        linear_starttime = (LinearLayout) view.findViewById(R.id.linear_starttime);


        linear_starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = 1;
                timePickerDialog();

            }
        });
        linear_endtime = (LinearLayout) view.findViewById(R.id.linear_endtime);
        linear_endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = 2;
                timePickerDialog();
            }
        });
        examdate_textview = (TextView) view.findViewById(R.id.examdate_textview);
        examstarttime_textview = (TextView) view.findViewById(R.id.examstarttime_textview);
        examendtime_textview = (TextView) view.findViewById(R.id.examendtime_textview);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = currentDATE.format(c);
        //examdate_textview.setText(getDateFormat(currentDate));
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:a");
        String strDate = sdf.format(c.getTime());
        dateFormatwithseconds=new SimpleDateFormat("HH:mm:ss");
        selectedstartTime=dateFormatwithseconds.format(c.getTime());
        //examstarttime_textview.setText(strDate);

        formattedDate = currentDATE.format(c);
        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        add_exm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAddExam();
            }
        });

        chapterDialog=new Dialog(getContext());
        chapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        chapterDialog.setCancelable(false);
        chapterDialog.setContentView(R.layout.add_exam_chapter_custom_dialog);
        chapterdialogSearchView=(SearchView)chapterDialog.findViewById(R.id.dialogSearchView);
        chaptersearchnameTextview=(TextView)chapterDialog.findViewById(R.id.searchnameTextview);
        chapternameTextview=(TextView)view.findViewById(R.id.chapternameTextview);
        chapterlistView = (ListView) chapterDialog.findViewById(R.id.chapterlistView);
        dialogChapterButton=(Button)chapterDialog.findViewById(R.id.btn_dialog) ;
        selectAllCheckbox=(CheckBox)chapterDialog.findViewById(R.id.selectAllCheckbox);
        topicDialog = new Dialog(getContext());
        topicDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        topicDialog.setCancelable(false);
        topicDialog.setContentView(R.layout.custom_dialog);
        topiclistView = (ListView) topicDialog.findViewById(R.id.listView1);
        dialogTopicButton = (Button) topicDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)topicDialog.findViewById(R.id.dialogSearchView);
        topicnameTextview=(TextView)view.findViewById(R.id.topicnameTextview);
        searchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);
        topicsearchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);
        examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList,selectAllCheckbox);
        examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            examID = bundle.getInt("examID", 0);

        }

        if (examID>0)
        {
            getExamDetails();
        }
        else
        {
            getExamtermList();
        }
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        chapterdialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chapterdialogSearchView.setIconified(false);
            }
        });

        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchtopicList.clear();
                    for(int y=0;y<topicList.size();y++)
                    {
                        if (topicList.get(y).getTopic_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchtopicList.add(topicList.get(y));

                        }

                    }


                    if (searchtopicList.size()>0) {

                        topiclistView.setVisibility(View.VISIBLE);
                        topicsearchnameTextview.setVisibility(View.GONE);
                        examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                        topiclistView.setAdapter(examComposetopicAdpater);
                    }
                    else
                    {
                        topiclistView.setVisibility(View.GONE);
                        topicsearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    topiclistView.setVisibility(View.VISIBLE);
                    topicsearchnameTextview.setVisibility(View.GONE);
                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                    topiclistView.setAdapter(examComposetopicAdpater);

                }
                return false;
            }
        });
        chapterdialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchchapterList.clear();
                    for(int y=0;y<chapterList.size();y++)
                    {
                        if (chapterList.get(y).getChapter_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchchapterList.add(chapterList.get(y));

                        }

                    }


                    if (searchchapterList.size()>0) {

                        chapterlistView.setVisibility(View.VISIBLE);
                        chaptersearchnameTextview.setVisibility(View.GONE);
                        examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList, selectAllCheckbox);
                        chapterlistView.setAdapter(examComposechapterAdpater);

                    }
                    else
                    {
                        chapterlistView.setVisibility(View.GONE);
                        chaptersearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    chapterlistView.setVisibility(View.VISIBLE);
                    chaptersearchnameTextview.setVisibility(View.GONE);
                    examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(chapterList,getContext(),chapterDialog,dialogChapterButton, chapterList, selectAllCheckbox);
                    chapterlistView.setAdapter(examComposechapterAdpater);

                }
                return false;
            }
        });

        dialogChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for (int y = 0; y < chapterList.size(); y++) {
                    if (chapterList.get(y).getSelectedID()==0)
                    {
                        selectAllCheckbox.setChecked(false);
                        Log.d("TTTTTTTTT","kkkkk");
                        break;
                    }
                }
                String chapterName="";
                chapterID="";
                for (int y=0;y<chapterList.size();y++)
                {


                    if (chapterList.get(y).getSelectedID()!=0) {
                        chapterName = chapterName.concat(String.valueOf(chapterList.get(y).getChapter_name()).concat(","));
                        chapterID=chapterID.concat(String.valueOf(chapterList.get(y).getId())).concat(",");
                    }

                }
                if (chapterName.length()>0 && chapterID.length()>0) {
                    chapterName = chapterName.substring(0, chapterName.length() - 1);
                    chapterID = chapterID.substring(0, chapterID.length() - 1);
                    chapternameTextview.setText(chapterName);
                    chapternameTextview.setError(null);
                    topicList.clear();
                    topicID="";
                    topicnameTextview.setText("");
                    topicnameTextview.setHint(getString(R.string.selecttopic));
                    topicnameTextview.setError(null);
                    getTopicList(chapterID);
                }
                else
                {
                    chapternameTextview.setText("");
                }
                examComposechapterAdpater.notifyDataSetChanged();
                chapterDialog.hide();
            }
        });

        topicnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topicList.size()>0) {
                    topicDialog.show();
                }

            }
        });


        chapternameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chapterList.size()>0) {
                    chapterDialog.show();
                }

            }
        });


        dialogTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String topicName="";
                topicID="";
                for (int y=0;y<topicList.size();y++)
                {

                    if (topicList.get(y).getSelectedID()!=0) {
                        topicName = topicName.concat(String.valueOf(topicList.get(y).getTopic_name()).concat(","));
                        topicID=topicID.concat(String.valueOf(topicList.get(y).getId())).concat(",");
                    }

                }
                if (topicName.length()>0 && topicID.length()>0) {
                    topicName = topicName.substring(0, topicName.length() - 1);
                    topicID = topicID.substring(0, topicID.length() - 1);
                    Log.d("BBAA", "ID" + topicID + "NAME" + topicName);
                    topicnameTextview.setText(topicName);
                    topicnameTextview.setError(null);
                }
                else
                {
                    topicnameTextview.setText("");
                }
                topicDialog.hide();
            }
        });
        topicspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, topiclist);

        topicspinnerArray.setDropDownViewResource(R.layout.spinner_item);


        subjectlist.add("Select Subject");
        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        subject_spinner.setAdapter(subjectspinnerArray);

        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_term_spinner.setAdapter(spinnerexamTermAdapter);


        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);

        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);


        chapterspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, chapterlist);

        chapterspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        //chaptermultiSpinner.setAdapter(chapterspinnerArray, false, chapteronSelectedListener);


        exam_term_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    examTermId = 0;
                    setExamdateToCalender(0);
                } else {
                    examTermId = examTermResponse.getExamTermsList().get(position - 1).getId();
                    examStartdate=examTermResponse.getExamTermsList().get(position-1).getStart_date();
                    examEnddate=examTermResponse.getExamTermsList().get(position-1).getEnd_date();
                    setExamdateToCalender(1);
                    if (classId > 0 && sectionId > 0) {
                        if (examID<=0) {
                            subjectspinnerArray.clear();
                            subjectspinnerArray.notifyDataSetChanged();
                            subject_Id = 0;
                            chapterID = "";
                            subjectspinnerArray.clear();
                            subjectspinnerArray.notifyDataSetChanged();
                            chapternameTextview.setText("");
                            topicnameTextview.setText("");
                            chapternameTextview.setHint(getString(R.string.selectchapter));
                            topicnameTextview.setHint(getString(R.string.selecttopic));
                            chapternameTextview.setError(null);
                            topicnameTextview.setError(null);
                            chapterList.clear();
                            examComposechapterAdpater.notifyDataSetChanged();
                            topicList.clear();
                            examComposetopicAdpater.notifyDataSetChanged();
                            topicID = "";
                            getSubjecyList();
                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                examTermId = 0;

            }
        });


        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    classId = 0;
                    sectionId = 0;
                } else {
                    classId = classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionId = classListResponse.getTeachersClassesList().get(position - 1).getSectionID();
                    if (examTermId > 0) {

                        if (examID<=0) {
                            subjectspinnerArray.clear();
                            subjectspinnerArray.notifyDataSetChanged();
                            subject_Id = 0;
                            chapterspinnerArray.clear();
                            chapterspinnerArray.notifyDataSetChanged();
                            chapterID = "";
                            lastchapterID = "";
                            lasttopicId = "";
                            topicspinnerArray.clear();
                            topicspinnerArray.notifyDataSetChanged();
                            subjectspinnerArray.clear();
                            subjectspinnerArray.notifyDataSetChanged();
                            chapternameTextview.setText("");
                            topicnameTextview.setText("");
                            chapternameTextview.setHint(getString(R.string.selectchapter));
                            topicnameTextview.setHint(getString(R.string.selecttopic));
                            topicID = "";
                            chapternameTextview.setError(null);
                            topicnameTextview.setError(null);
                            chapterList.clear();
                            examComposechapterAdpater.notifyDataSetChanged();
                            topicList.clear();
                            examComposetopicAdpater.notifyDataSetChanged();
                            getSubjecyList();
                        }
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        subject_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    subject_Id = 0;

                } else {

                    if (examID<=0) {
                        subject_Id = subjectListResponse.getTeachersSubjectsList().get(position - 1).getSubject_id();

                        topicspinnerArray.clear();
                        topicspinnerArray.notifyDataSetChanged();
                        chapterList.clear();
                        chapterID="";
                        chapternameTextview.setText("");
                        chapternameTextview.setHint(getString(R.string.selectchapter));
                        topicList.clear();
                        topicID="";
                        topicnameTextview.setText("");
                        topicnameTextview.setHint(getString(R.string.selecttopic));
                        chapternameTextview.setError(null);
                        topicnameTextview.setError(null);
                        chapterList.clear();
                        examComposechapterAdpater.notifyDataSetChanged();
                        topicList.clear();
                        examComposetopicAdpater.notifyDataSetChanged();
                        getExamchapterList();
                    }
                    //getSubjecyList();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });




        recyclerView = (RecyclerView) view.findViewById(R.id.add_exam_recycle);




        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // recyclerView.setAdapter(mAdapter);

        examtypespinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_size, examtypelist);

        examtypespinnerArray.setDropDownViewResource(R.layout.spinner_item);

        examSpinner.setAdapter(examtypespinnerArray);

        examSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                visibility=examTypeResponse.getExamTypeList().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }


    private void getExamDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            ExamDetailsParams examDetailsParams=new ExamDetailsParams();
            examDetailsParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            examDetailsParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            examDetailsParams.setExamScheduleID(String.valueOf(examID));
            examDetailsParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getExamDetailsById(examDetailsParams).enqueue(new Callback<ExamDetailByIdResponse>() {
                @Override
                public void onResponse(Call<ExamDetailByIdResponse> call, Response<ExamDetailByIdResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examDetailByIdResponse=response.body();
                        if (examDetailByIdResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examDetailByIdResponse.getStatus() == Util.STATUS_SUCCESS) {
                                //Toast.makeText(getContext(), examDetailByIdResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClass_id();
                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
                                getExamtermList();
                                //setspinnerData();
                            } else {
                                Toast.makeText(getContext(), examDetailByIdResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {

                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        getActivity().finish();


                    }
                }

                @Override
                public void onFailure(Call<ExamDetailByIdResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void setspinnerData()
    {


        minTextview.setText(""+examDetailByIdResponse.getExamScheduleList().get(0).getMin_mark());
        maxTextview.setText(""+examDetailByIdResponse.getExamScheduleList().get(0).getMarks());
        examdate_textview.setText(getDateFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time()));
        examstarttime_textview.setText(getTimeFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time()));
        examendtime_textview.setText(getTimeFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getEnd_time()));
        if(examDetailByIdResponse.getExamScheduleList().get(0).getExam_type()==1)
        {
            examSpinner.setSelection(0);
        }
        else
        {
            examSpinner.setSelection(1);
        }

        for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
            if (examTermResponse.getExamTermsList().get(i).getId() == examDetailByIdResponse.getExamScheduleList().get(0).getTerm_id()) {
                exam_term_spinner.setSelection(i+1);
                break;
            }
        }
        for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
            if (classListResponse.getTeachersClassesList().get(i).getSectionID() == examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id()) {
                class_spinner.setSelection(i+1);
                break;
            }
        }
        for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {
            if (subjectListResponse.getTeachersSubjectsList().get(i).getSubject_id() == examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id()) {
                subject_spinner.setSelection(i+1);
                break;
            }
        }

        String[] chapterstrArray = examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id().split(",");
        selectedchapterItemsupdate = new boolean[chapterspinnerArray.getCount()];
        for (int i=0;i<chapterList.size();i++)
        {
            for (int u=0;u<chapterstrArray.length;u++)
            {
                int selectedclassID= Integer.parseInt(chapterstrArray[u]);
                if (chapterList.get(i).getId()==selectedclassID)
                {
                    selectedchapterItemsupdate[i]=true;

                }
            }

        }
//        ArrayList<Integer> selectedChapter=new ArrayList<>();
//        for(int i=0; i<chaptermultiSpinner.getSelected().length; i++) {
//            if(chaptermultiSpinner.getSelected()[i]) {
//
//                selectedChapter.add(i);
//
//            }
//
//        }

        chaptermultiSpinner.setSelected(selectedchapterItemsupdate);
        chapterspinnerArray.notifyDataSetChanged();




        String[] topicstrArray = examDetailByIdResponse.getExamScheduleList().get(0).getTopic_id().split(",");
        selectedtopicItemsupdate = new boolean[topicspinnerArray.getCount()];
        for (int i=0;i<topicList.size();i++)
        {
            for (int u=0;u<topicstrArray.length;u++)
            {
                int selectedtopicID= Integer.parseInt(topicstrArray[u]);
                if (topicList.get(i).getId()==selectedtopicID)
                {
                    selectedtopicItemsupdate[i]=true;

                }
            }

        }
//        ArrayList<Integer> selectedChapter=new ArrayList<>();
//        for(int i=0; i<chaptermultiSpinner.getSelected().length; i++) {
//            if(chaptermultiSpinner.getSelected()[i]) {
//
//                selectedChapter.add(i);
//
//            }
//
//        }
        chaptermultiSpinner.setSelected(selectedchapterItemsupdate);
        chapterspinnerArray.notifyDataSetChanged();
        topicmultiSpinner.setSelected(selectedtopicItemsupdate);
        topicspinnerArray.notifyDataSetChanged();

    }

//    private MultiSpinner.MultiSpinnerListener chapteronSelectedListener = new MultiSpinner.MultiSpinnerListener() {
//        public void onItemsSelected(boolean[] selected) {
//
//            selectedChapter.clear();
//            for(int i=0; i<selected.length; i++) {
//                if(selected[i]) {
//                    Log.i("TAG", "mes"+i );
//                    selectedChapter.add(i);
//
//                }
//
//            }
//
//            if (selectedChapter.size()>0) {
//                String chapterID = "";
//                String chapterName="";
//                lastchapterID="";
//                for (int y = 0; y < selectedChapter.size(); y++) {
//                    chapterID = chapterID.concat(String.valueOf(chapterList.get(selectedChapter.get(y)).getId()).concat(","));
//                    chapterName=chapterName.concat(String.valueOf(chapterList.get(selectedChapter.get(y)).getChapter_name()).concat(","));
//                }
//                chapterID = chapterID.substring(0, chapterID.length() - 1);
//                chapterName = chapterName.substring(0, chapterName.length() - 1);
//                chapternameTextview.setText(chapterName);
//                chaptermultiSpinner.setText("");
//                lastchapterID=chapterID;
//                getTopicList(chapterID);
//
//            }
//
//            // Do something here with the selected items
//        }
//    };

    private void isAddExam()
    {
        if (examTermId!=0)
        {
            if (classId!=0)
            {
                if (subject_Id!=0)
                {
                    if (formattedDate.length()>0) {
                        if (examstarttime_textview.getText().toString().trim().length() > 0) {
                            if (examendtime_textview.getText().toString().trim().length() > 0) {
                                if (chapterID.length()!=0)
                                {
                                    if (topicID.length()!=0)
                                    {
                                        if (visibility!=0)
                                        {
                                            if (minTextview.getText().toString().length()>0)
                                            {
                                                if (maxTextview.getText().toString().length()>0)
                                                {
                                                    int minmark= Integer.parseInt(minTextview.getText().toString().trim());
                                                    int maxmark= Integer.parseInt(maxTextview.getText().toString().trim());
                                                    if (maxmark>=minmark)
                                                    {
                                                        if (examDuration!=null)
                                                        {
                                                            System.out.println("Start------->"+formattedDate+" "+selectedstartTime);
                                                            System.out.println("End--------->"+formattedDate+" "+selectedendTime);
                                                            Date date1= null,date2 = null;
                                                            try {
                                                                date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formattedDate+" "+selectedstartTime);
                                                                date2= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formattedDate+" "+selectedendTime);
                                                                if(date1.getTime() < date2.getTime()){
                                                                    System.out.println("Hiii "+date1.getTime()+"--"+date2.getTime());

                                                                    /*String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                                                    Date strDate = sdf.parse(formattedDate);
                                                                    if (new Date().after(strDate)) {
                                                                        System.out.println("datetttetetette ==> ");
                                                                    }*/

                                                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                                                    Date sDate = sdf.parse(examStartdate);
                                                                    Date eDate = sdf.parse(examEnddate);
                                                                    String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(sDate);
                                                                    String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(eDate);
                                                                    List<Date> dates = getDates(startDate, endDate);
                                                                    List<String> date111 = new ArrayList<>();
                                                                    for(Date date:dates) {
                                                                        date111.add(new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date));
                                                                        System.out.println("datetttetetette ==> "+new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date));
                                                                    }

                                                                    String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                                                                    if (date111.contains(date)) {
                                                                        Date cDate = sdf.parse(formattedDate);
                                                                        System.out.println("current ==> 1 "+cDate);
                                                                        System.out.println("current ==> 2 "+new Date());
                                                                        if (cDate.after(new Date()) || formattedDate.equals(date)) {
                                                                            System.out.println("current ==> "+cDate);
                                                                            addExam();
                                                                        } else {
                                                                            Toast.makeText(getContext(), "Exam not created for past date.", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    } else {
                                                                        System.out.println("noooooo ==> ");
                                                                        addExam();
                                                                    }

                                                                }else{
                                                                    Toast.makeText(getContext(), "Please Select Valid exam start and end time", Toast.LENGTH_SHORT).show();
                                                                }
                                                            } catch (ParseException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Toast.makeText(getContext(), "Choose exam start and end time ", Toast.LENGTH_SHORT).show();
                                                        }

                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(getContext(),"Invalid Min and Max Mark ",Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                                else
                                                {
                                                    //Toast.makeText(getContext(),"Enter_maxmark",Toast.LENGTH_SHORT).show();
                                                    maxTextview.setError(getString(R.string.Enter_maxmark));
                                                }
                                            }
                                            else
                                            {
                                                // Toast.makeText(getContext(),"Enter Min mark",Toast.LENGTH_SHORT).show();
                                                minTextview.setError(getString(R.string.Enter_minmark));
                                            }
                                        }
                                        else
                                        {
                                            Toast.makeText(getContext(),"Select examtype",Toast.LENGTH_SHORT).show();

                                        }

                                        //Toast.makeText(getContext(),"Exam Ready to add",Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        //Toast.makeText(getContext(),"Choose Topic ",Toast.LENGTH_SHORT).show();
                                        topicnameTextview.setError(getString(R.string.selecttopic));
                                    }
                                }
                                else
                                {
                                    //Toast.makeText(getContext(),"Choose Chapter ",Toast.LENGTH_SHORT).show();
                                    chapternameTextview.setError(getString(R.string.selectchapter));

                                }
                            } else {
                                Toast.makeText(getContext(), "Choose exam end time ", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Choose exam start time ", Toast.LENGTH_SHORT).show();
                        }
                    }

                    else
                    {
                        Toast.makeText(getContext(),"Choose date",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getContext(),"Choose Subject",Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(getContext(),"Choose Class",Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getContext(),"Choose Exam term",Toast.LENGTH_SHORT).show();
        }


    }

    private static List<Date> getDates(String dateString1, String dateString2)
    {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1 .parse(dateString1);
            date2 = df1 .parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void getExamtype()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            vidyauraAPI.getExamType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0))).enqueue(new Callback<ExamTypeResponse>() {
                @Override
                public void onResponse(Call<ExamTypeResponse> call, Response<ExamTypeResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examTypeResponse = response.body();
                        if (examTypeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examTypeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (examTypeResponse.getExamTypeList().size() > 0) {
                                    for (int i = 0; i < examTypeResponse.getExamTypeList().size(); i++) {
                                        examtypelist.add(examTypeResponse.getExamTypeList().get(i).getName());
                                    }
                                    examtypespinnerArray.notifyDataSetChanged();

                                    //setspinnerData();

                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getContext(), examTypeResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamTypeResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void addExam() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddExambyClassParam addExambyClassParam=new AddExambyClassParam();
            addExambyClassParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addExambyClassParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addExambyClassParam.setExamDuration(examDuration);
            addExambyClassParam.setExamStartTime(formattedDate+" "+selectedstartTime);
            addExambyClassParam.setExamEndTime(formattedDate+" "+selectedendTime);
            addExambyClassParam.setClassID(String.valueOf(classId));
            addExambyClassParam.setSectionID(String.valueOf(sectionId));
            addExambyClassParam.setSubjectID(String.valueOf(subject_Id));
            addExambyClassParam.setTermID(String.valueOf(examTermId));
            addExambyClassParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addExambyClassParam.setMaxMark(maxTextview.getText().toString().trim());
            addExambyClassParam.setMinMark(minTextview.getText().toString().trim());
            addExambyClassParam.setExamType(String.valueOf(visibility));
            addExambyClassParam.setChapterID(chapterID);
            addExambyClassParam.setTopicID(topicID);
            vidyauraAPI.addExamByClass(addExambyClassParam).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    chapterDialog.dismiss();
                    topicDialog.dismiss();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                                sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                                sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                                sendNotification.setType("exam_schedules");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            getActivity().finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            subjectlist.clear();
            subjectlist.add("Select Subject");
            GetSubjectListParam getSubjectListParam = new GetSubjectListParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            vidyauraAPI.getTeachersSubjects(getSubjectListParam).enqueue(new Callback<SubjectListResponse>() {
                @Override
                public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        subjectListResponse = response.body();
                        if (subjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (subjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {

                                    subjectlist.add(subjectListResponse.getTeachersSubjectsList().get(i).getName());

                                }
//                                subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                //                              classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                //chapter_Id=examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();
                                subjectspinnerArray.notifyDataSetChanged();
                                // getExamchapterList();




                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(classListResponse.getTeachersClassesList().get(i).getClassName()+"-"+
                                            classListResponse.getTeachersClassesList().get(i).getSection());

                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
//                                getSubjecyList();
                                classspinnerArray.notifyDataSetChanged();
                                //class_spinner.performClick();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            examTermSpinner.clear();
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();

                                    getClassList();
                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getExamchapterList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            chapterList.clear();
            GetChapterListParam getChapterListParam = new GetChapterListParam();
            getChapterListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getChapterListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getChapterListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getChapterListParam.setClassID(String.valueOf(classId));
            getChapterListParam.setSubjectID(String.valueOf(subject_Id));
            vidyauraAPI.getExamChaptersList(getChapterListParam).enqueue(new Callback<ChapterListResponse>() {
                @Override
                public void onResponse(Call<ChapterListResponse> call, Response<ChapterListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        chapterListResponse = response.body();
                        if (chapterListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (chapterListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (chapterListResponse.getChaptersList().size() > 0) {
                                    for (int i = 0; i < chapterListResponse.getChaptersList().size(); i++) {
                                        chapterList.add(new ListofChapterforDialog(chapterListResponse.getChaptersList().get(i).getId()
                                                ,chapterListResponse.getChaptersList().get(i).getChapter_name(),0));
                                        // chapterspinnerArray.add(chapterListResponse.getChaptersList().get(i).getChapter_name());
                                    }
                                    chapterspinnerArray.notifyDataSetChanged();
                                    examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(chapterList,getContext(), chapterDialog, dialogChapterButton, chapterList, selectAllCheckbox);
                                    chapterlistView.setAdapter(examComposechapterAdpater);
                                    //chapterDialog.show();

                                } else {
                                    Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ChapterListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getTopicList(String chapterID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            topicspinnerArray.clear();
            topicList.clear();
            GetTopicListParam getTopicListParam = new GetTopicListParam();
            getTopicListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getTopicListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getTopicListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getTopicListParam.setClassID(String.valueOf(classId));
            getTopicListParam.setSubjectID(String.valueOf(subject_Id));
            getTopicListParam.setChapterID(chapterID);
            vidyauraAPI.getTopicsList(getTopicListParam).enqueue(new Callback<TopicListResponse>() {
                @Override
                public void onResponse(Call<TopicListResponse> call, Response<TopicListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        topicListResponse = response.body();
                        if (topicListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (topicListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (topicListResponse.getTopicsList().size() > 0) {
                                    for (int i = 0; i < topicListResponse.getTopicsList().size(); i++) {

                                        topicList.add(new ListofTopicforDialog(topicListResponse.getTopicsList().get(i).getId()
                                                ,topicListResponse.getTopicsList().get(i).getTopic_name(),0));
                                    }
                                    topicspinnerArray.notifyDataSetChanged();

                                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                                    topiclistView.setAdapter(examComposetopicAdpater);
                                    //topicDialog.show();
                                    if (examtypelist.size()<=0) {
                                        getExamtype();
                                    }


                                } else {
                                    Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TopicListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance((com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                currentD.get(Calendar.DAY_OF_MONTH));
        Calendar examStartcal = Calendar.getInstance();
        Calendar examEndcal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            examStartcal.setTime(sdf.parse(examStartdate));
            examEndcal.setTime(sdf.parse(examEnddate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datePickerDialog.setMinDate(examStartcal);
        datePickerDialog.setMaxDate(examEndcal);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }
    public void setExamdateToCalender(int mode)
    {
        if (mode==0)
        {
            examdate_textview.setText("");
            formattedDate = "";
        }
        else {
            examdate_textview.setText(getDateFormat(examStartdate));
            formattedDate = examStartdate;
        }


    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String donateDate = simpleDateFormat.format(calendardate.getTime());
        Date dob = null;
        try {
            dob = simpleDateFormat.parse(donateDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate = simpleDateFormat.format(dob);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = currentDATE.format(c);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(currentDate);
            date2 = sdf.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println("date1 : " + sdf.format(date1));
        System.out.println("date2 : " + sdf.format(date2));

        if (date1.compareTo(date2) > 0) {
            System.out.println("Date1 is after Date2");
            //Toast.makeText(getContext(), "Invalid date selection", Toast.LENGTH_SHORT).show();
            examdate_textview.setText(getDateFormat(formattedDate));
        } else if (date1.compareTo(date2) < 0) {
            System.out.println("Date1 is before Date2");
            examdate_textview.setText(getDateFormat(formattedDate));
        } else if (date1.compareTo(date2) == 0) {
            System.out.println("Date1 is equal to Date2");
            examdate_textview.setText(getDateFormat(formattedDate));
        } else {
            System.out.println("How to get here?");
        }


    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String getDateFormatwithTMS(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getTimeFormatwithTMS(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        simpleDateFormat = new SimpleDateFormat("hh:mm aa");
        simpleDateFormat2 = new SimpleDateFormat("HH:mm aa");
        dateFormatwithseconds=new SimpleDateFormat("HH:mm:ss");

        if (selectedDate == 1) {
//            start date
            calendarStartTime = Calendar.getInstance();
            calendarStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarStartTime.set(Calendar.MINUTE, minute);
            eventStartTime = simpleDateFormat.format(calendarStartTime.getTime());
            examstarttime_textview.setText(eventStartTime);
            selectedstartTime=dateFormatwithseconds.format(calendarStartTime.getTime());
            if (eventStartTime !=null && eventEndTime!=null) {
                boolean timecheck = checktimings(eventStartTime, eventEndTime);
                if (!timecheck)
                {

                    Toast.makeText(getContext(), "Invalid time selection", Toast.LENGTH_SHORT).show();
                    examstarttime_textview.setText("");

                }
                else
                {
                    try {
                        DurationBetweenTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("vikii", "ss" + timecheck);
            }

        } else if (selectedDate == 2) {
            //end date
            calendarEndTime = Calendar.getInstance();
            calendarEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarEndTime.set(Calendar.MINUTE, minute);
            eventEndTime = simpleDateFormat.format(calendarEndTime.getTime());
            examendtime_textview.setText(eventEndTime);
            selectedendTime=dateFormatwithseconds.format(calendarEndTime.getTime());
            if (eventStartTime !=null && eventEndTime!=null) {
                boolean timecheck = checktimings(eventStartTime, eventEndTime);
                if (!timecheck)
                {
                    examendtime_textview.setText("");
                    Toast.makeText(getContext(), "Invalid time selection", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    try {
                        DurationBetweenTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("vikii", "ss" + timecheck);
            }


        }

    }

    public void DurationBetweenTime() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = simpleDateFormat.parse(eventStartTime);
        Date endDate = simpleDateFormat.parse(eventEndTime);

        long difference = endDate.getTime() - startDate.getTime();
        if(difference<0)
        {
            Date dateMax = simpleDateFormat.parse("12:00");
            Date dateMin = simpleDateFormat.parse("00:00");
            difference=(dateMax.getTime() -startDate.getTime() )+(endDate.getTime()-dateMin.getTime());
        }
        int days = (int) (difference / (1000*60*60*24));
        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

        examDuration=hours+":"+min;



    }
    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            }
            else if (date1.after(date2))
            {
                return true;
            }
            else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }

    private void timePickerDialog() {
        if (selectedDate == 2) {
            // end date
            currentT = Calendar.getInstance();
        } else if (selectedDate == 1) {
//            start date
            currentT = Calendar.getInstance();
        }
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.blue));
        if (selectedDate == 2) {
            //end date
            Date ec = Calendar.getInstance().getTime();
            SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");
            String ecurrentDate = edf.format(ec);
            if (ecurrentDate.equalsIgnoreCase(eventStartDateFormat)) {
                timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
            } else if (selectedDate == 1) {
                //start date
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = df.format(c);
                if (currentDate.equalsIgnoreCase(eventStartDateFormat)) {
                    timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                            currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
                }
            }


        }
        else
        {
            Date ec = Calendar.getInstance().getTime();
            SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");
            String ecurrentDate = edf.format(ec);
            if (ecurrentDate.equalsIgnoreCase(eventStartDateFormat)) {
                timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
            } else if (selectedDate == 1) {
                //start date
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = df.format(c);
                if (currentDate.equalsIgnoreCase(eventStartDateFormat)) {
                    timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                            currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
                }
            }
        }
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }
}
