package com.school.teacherparent.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ResultStudentAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.GetStudentResultListParams;
import com.school.teacherparent.R;
import com.school.teacherparent.models.StudentResultDetailsListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultStudentFragment extends BaseFragment {


    private RecyclerView recyclerView;
    private ResultStudentAdapter mAdapter;
    CircularImageView ivStudentProfile;
    private LinearLayout lin;
    private TextView studentNameTextiview,classnameTextview;
    private String[] myString;
    ImageView noti, back;

    List<String> srudentMark2=new ArrayList<>();
    public ResultStudentFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int classID,sectionID,studID;
    String studName,classname,section,studentProfile;
    StudentResultDetailsListResponse studentResultDetailsListResponse;
    ArrayList<StudentResultDetailsListResponse.studentResultsDetailsList>studentResultsDetailsListArrayList=new ArrayList<>();
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_student, container, false);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            classID = bundle.getInt("classID");
            sectionID = bundle.getInt("sectionID");
            studID=bundle.getInt("studID");
            studName=bundle.getString("studName","");
            classname=bundle.getString("classname","");
            section=bundle.getString("section","");
            studentProfile=bundle.getString("studentProfile","");
        }
        studentNameTextiview=(TextView)view.findViewById(R.id.nameTextiview);
        studentNameTextiview.setText(studName);
        classnameTextview=(TextView)view.findViewById(R.id.classnameTextview);
        classnameTextview.setText(classname+" "+section);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_result_stuname);
        lin = (LinearLayout) view.findViewById(R.id.lin);
        ivStudentProfile = view.findViewById(R.id.iv_studentProfile);
        recyclerView.setNestedScrollingEnabled(false);
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        final int userType = sharedPreferences.getInt(Constants.USERTYPE,0);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userType == 1) {
                    Fragment fragment = new ResultClassListFragment();
                    replaceFragment(fragment);
                } else if (userType == 2) {
                    Fragment fragment = new ParentResultFragment();
                    replaceFragment(fragment);
                }
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        String url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path)+"/"+studentProfile;
        Picasso.get().load(url).placeholder(R.drawable.ic_user).into(ivStudentProfile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                ivStudentProfile.setImageResource(R.drawable.ic_user);
            }
        });

        //mAdapter = new ResultStudentAdapter(studentResultsDetailsListArrayList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getStudentResultList();
//        recyclerView.setAdapter(mAdapter);
//        RecyclerSectionItemDecoration sectionItemDecoration =
//                new RecyclerSectionItemDecoration(25,
//                        true,
//                        getSectionCallback(getData()));
//        recyclerView.addItemDecoration(sectionItemDecoration);
//        RecyclerSectionItemDecoration sectionItemDecoration =
//                new RecyclerSectionItemDecoration(25,
//                        true, // true for sticky, false for not
//                        new RecyclerSectionItemDecoration.SectionCallback() {
//                            @Override
//                            public boolean isSection(int position) {
//                                return position == 0;
//                            }
//
//                            @Override
//                            public CharSequence getSectionHeader(int position) {
//                                return "Test";
//                            }
//                        });
//        recyclerView.addItemDecoration(sectionItemDecoration);


       /* mAdapter.setOnClickListen(new ResultStudentAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "result"));
            }
        });
*/



        return view;
    }
//    public ArrayList<String> getData() {
//        String[] strings = getResources().getStringArray(R.array.sticy_header);
//        ArrayList<String> list = new ArrayList<String>(Arrays.asList(strings));
//        return list;
//    }
//    private RecyclerSectionItemDecoration.SectionCallback getSectionCallback(final List<String> people) {
//        return new RecyclerSectionItemDecoration.SectionCallback() {
//            @Override
//            public boolean isSection(int position) {
//                return position == 0
//                        || people.get(position)
//                        .charAt(0) != people.get(position - 1)
//                        .charAt(0);
//            }
//
//            @Override
//            public CharSequence getSectionHeader(int position) {
//                return people.get(position)
//                        .subSequence(0,
//                                1);
//            }
//        };
//    }
    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void getStudentResultList()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();

            GetStudentResultListParams getStudentResultListParams = new GetStudentResultListParams();
            getStudentResultListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentResultListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentResultListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getStudentResultListParams.setClassID(String.valueOf(classID));
            getStudentResultListParams.setSectionID(String.valueOf(sectionID));
            getStudentResultListParams.setStudID(String.valueOf(studID));
            vidyauraAPI.getStudentResultDetails(getStudentResultListParams).enqueue(new Callback<StudentResultDetailsListResponse>() {
                @Override
                public void onResponse(Call<StudentResultDetailsListResponse> call, Response<StudentResultDetailsListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        studentResultDetailsListResponse = response.body();
                        if (studentResultDetailsListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (studentResultDetailsListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (studentResultDetailsListResponse.getStudentResultsDetailsList().size()>0)
                                {
                                    studentResultsDetailsListArrayList=studentResultDetailsListResponse.getStudentResultsDetailsList();
                                    mAdapter = new ResultStudentAdapter(studentResultsDetailsListArrayList, getActivity());
                                    recyclerView.setAdapter(mAdapter);
                                }







                            } else {
                                Toast.makeText(getContext(), studentResultDetailsListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<StudentResultDetailsListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
