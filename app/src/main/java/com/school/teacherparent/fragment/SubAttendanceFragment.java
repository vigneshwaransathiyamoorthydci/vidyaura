package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SubAttendanceAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AttendancedetailsParams;
import com.school.teacherparent.models.AttendancedetailsResponse;
import com.school.teacherparent.R;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubAttendanceFragment extends BaseFragment {


    private RecyclerView attendanceSubrecyclerView;
    private SubAttendanceAdapter subAttendanceAdapter;
    
    FloatingActionButton add_attendance;
    ImageView back;
   int classID,sectionID,AttendanceSessionCount,groupID;
   boolean isGroup;
   String attendanceDATE,className,classname,ClassImage,groupName;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    TextView classnameTitle,dateTextview,classNametextveiw,stu_counts,present_count,absent_count;
    ImageView calendarImageview;
    CircularImageView classImage;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate,total,present,leave;
    int sessionTYPE;
    AttendancedetailsResponse attendancedetailsResponse;
    public ArrayList<AttendancedetailsResponse.attendanceDetailsList> attendanceDetailsListArrayList;
    LinearLayoutManager linearLayoutManager;
    ImageView editImageview,ivNoti;
    NotificationReceiver notificationReceiver;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sub_attendance, container, false);

        attendanceSubrecyclerView=(RecyclerView)view.findViewById(R.id.sub_attendnce_recycle_fragment);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        classnameTitle=(TextView)view.findViewById(R.id.classnameTitle);
        dateTextview=(TextView)view.findViewById(R.id.dateTextview);
        calendarImageview=(ImageView)view.findViewById(R.id.calendarImageview);
        classImage=(CircularImageView)view.findViewById(R.id.classImage);
        classNametextveiw=(TextView)view.findViewById(R.id.className);
        stu_counts=(TextView)view.findViewById(R.id.stu_counts);
        present_count=(TextView)view.findViewById(R.id.present_count);
        absent_count=(TextView)view.findViewById(R.id.absent_count);
        editImageview=(ImageView)view.findViewById(R.id.editImageview);
        ivNoti=(ImageView)view.findViewById(R.id.noti);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            classID = bundle.getInt("classID", 0);
            sectionID = bundle.getInt("sectionID", 0);
            attendanceDATE=bundle.getString("attendanceDATE","");
            className=bundle.getString("className","");
            sessionTYPE=bundle.getInt("sessionTYPE");
            total=bundle.getString("total","");
            present=bundle.getString("present","");
            leave=bundle.getString("leave","");
            classname=bundle.getString("classnameTitle","");
            ClassImage=bundle.getString("classImage","");
            AttendanceSessionCount=bundle.getInt("attendanceSessionCount",5);
            groupID = bundle.getInt("groupID", 0);
            isGroup = bundle.getBoolean("isGroup", false);
            groupName = bundle.getString("groupName", "");
            if (!isGroup) {
                classnameTitle.setText(classname);
                getattendancedetails();
            } else {
                classnameTitle.setText(groupName);
                getGroupAttendancedetails();
            }
        }

        dateTextview.setText(getDateFormatforatten(attendanceDATE));
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(date);
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        //classNametextveiw.setText(className);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        String url = getString(R.string.s3_baseurl) + getString(R.string.s3_classworksection_path)+"/"+ClassImage;
        Picasso.get().load(url).placeholder(R.drawable.nopreview).into(classImage, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                classImage.setImageResource(R.drawable.nopreview);
            }
        });

        //classImage.setImageResource(R.mipmap.pink_icon);
        editImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendanceupdate")
                .putExtra("updatedate",attendanceDATE).putExtra("classID",classID).putExtra("sectionID",sectionID)
                        .putExtra("attendanceSessionCount",AttendanceSessionCount)
                        .putExtra("groupID",groupID)
                        .putExtra("isGroup",isGroup));
            }
        });
        calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");
                                Date c = Calendar.getInstance().getTime();
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                dateTextview.setText(getDateFormatforatten(formattedDate));
                                attendanceDATE=formattedDate;
                                attendanceDetailsListArrayList=new ArrayList<>();
                                attendanceSubrecyclerView.setAdapter(null);
                                stu_counts.setVisibility(View.INVISIBLE);
                                present_count.setVisibility(View.INVISIBLE);
                                absent_count.setVisibility(View.INVISIBLE);
                                if (!isGroup) {
                                    getattendancedetails();
                                } else {
                                    getGroupAttendancedetails();
                                }


                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());



            }
        });

        attendanceDetailsListArrayList=new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        attendanceSubrecyclerView.setItemAnimator(new DefaultItemAnimator());
        attendanceSubrecyclerView.setLayoutManager(linearLayoutManager);
        subAttendanceAdapter = new SubAttendanceAdapter(attendanceDetailsListArrayList,getContext(), sessionTYPE);
        attendanceSubrecyclerView.setAdapter(subAttendanceAdapter);

        add_attendance = view.findViewById(R.id.add_attendance);
        add_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
            }
        });
        attendanceSubrecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    add_attendance.setVisibility(View.VISIBLE);
                }
                else
                {
                    add_attendance.setVisibility(View.GONE);
                }
            }
        });
       //getattendancedetails();
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AttendanceFragment();
                replaceFragment(fragment);
            }
        });

        return view;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_attendance, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }


    public void getattendancedetails()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AttendancedetailsParams attendanceListParams=new AttendancedetailsParams();
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            attendanceListParams.setSessionType(2);
            attendanceListParams.setAttendanceDate(attendanceDATE);
            attendanceListParams.setClassID(String.valueOf(classID));
            attendanceListParams.setSectionID(String.valueOf(sectionID));

            vidyauraAPI.getAttendanceDetailsList(attendanceListParams).enqueue(new Callback<AttendancedetailsResponse>() {
                @Override
                public void onResponse(Call<AttendancedetailsResponse> call, Response<AttendancedetailsResponse> response) {
                    hideProgress();
                    System.out.println("inside ==> 1");
                    if (response.body() != null) {
                        System.out.println("inside ==> 2");
                        attendancedetailsResponse = response.body();
                        if (attendancedetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//                            attendanceDetailsListArrayList.clear();
                            attendanceDetailsListArrayList = response.body().getAttendanceDetailsList();
                            if (attendancedetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (attendanceDetailsListArrayList.size()!=0) {
                                    stu_counts.setVisibility(View.VISIBLE);
                                    present_count.setVisibility(View.VISIBLE);
                                    absent_count.setVisibility(View.VISIBLE);
                                   // attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());
                                    stu_counts.setText(attendanceDetailsListArrayList.get(0).getTotalStudentsCount()+" "+getString(R.string.students));
                                    present_count.setText(attendanceDetailsListArrayList.get(0).getStudentsPresentCount());
                                    absent_count.setText(attendanceDetailsListArrayList.get(0).getStudentsAbsentCount());
                                    subAttendanceAdapter = new SubAttendanceAdapter(attendanceDetailsListArrayList,getContext(),sessionTYPE);
                                    attendanceSubrecyclerView.setAdapter(subAttendanceAdapter);
                                   
                                }
                               
                            } else {
                                Toast.makeText(getContext(), attendancedetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        System.out.println("inside ==> 3");
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AttendancedetailsResponse> call, Throwable t) {
                    hideProgress();
                    System.out.println("inside ==> 4 "+t.getMessage());
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getGroupAttendancedetails()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AttendancedetailsParams attendanceListParams=new AttendancedetailsParams();
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            attendanceListParams.setSessionType(2);
            attendanceListParams.setAttendanceDate(attendanceDATE);
            attendanceListParams.setGroupID(String.valueOf(groupID));

            vidyauraAPI.getGroupAttendanceDetailsList(attendanceListParams).enqueue(new Callback<AttendancedetailsResponse>() {
                @Override
                public void onResponse(Call<AttendancedetailsResponse> call, Response<AttendancedetailsResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        attendancedetailsResponse = response.body();
                        if (attendancedetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//                            attendanceDetailsListArrayList.clear();
                            attendanceDetailsListArrayList = response.body().getAttendanceDetailsList();
                            if (attendancedetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (attendanceDetailsListArrayList.size()!=0) {
                                    stu_counts.setVisibility(View.VISIBLE);
                                    present_count.setVisibility(View.VISIBLE);
                                    absent_count.setVisibility(View.VISIBLE);
                                    // attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());
                                    stu_counts.setText(attendanceDetailsListArrayList.get(0).getTotalStudentsCount()+" "+getString(R.string.students));
                                    present_count.setText(attendanceDetailsListArrayList.get(0).getStudentsPresentCount());
                                    absent_count.setText(attendanceDetailsListArrayList.get(0).getStudentsAbsentCount());
                                    subAttendanceAdapter = new SubAttendanceAdapter(attendanceDetailsListArrayList,getContext(),sessionTYPE);
                                    attendanceSubrecyclerView.setAdapter(subAttendanceAdapter);

                                }

                            } else {
                                Toast.makeText(getContext(), attendancedetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AttendancedetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}



