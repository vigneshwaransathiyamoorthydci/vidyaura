package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SyllabusListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.R;
import com.school.teacherparent.models.SyllabusListModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SyllabusFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    int limit = 5;
    int offset = 0;
    LinearLayoutManager linearLayoutManager;

    FloatingActionButton addClasswork;
    SwipeRefreshLayout syllabusworkswipelayout;
    private RecyclerView recyclerView;
    private SyllabusListAdapter mAdapter;
    private List<SyllabusListModel.SyllabusCompletedPercentage> syllabusList = new ArrayList<>();
    private boolean loading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public SyllabusFragment() {
        // Required empty public constructor
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){
            syllabusList.clear();
            mAdapter.notifyDataSetChanged();
            offset=0;
            getsyllabusList();
        }


    }
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
//        if (isVisible){
//            syllabusList.clear();
//            offset=0;
//            getsyllabusList();
//        }


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    public void getsyllabusList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            FeedListParams feedListParams = new FeedListParams();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            feedListParams.setLimit(limit);
            feedListParams.setOffset(offset);
            feedListParams.setClassImage(Constants.CLASSIMAGE);
            Gson gson = new Gson();
            String input = gson.toJson(feedListParams);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getSyllabusList(feedListParams).enqueue(new Callback<SyllabusListModel>() {
                @Override
                public void onResponse(Call<SyllabusListModel> call, Response<SyllabusListModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        SyllabusListModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getSyllabusCompletedPercentage() != null &&
                                        response.body().getSyllabusCompletedPercentage().size() != 0) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    syllabusList.addAll(data.getSyllabusCompletedPercentage());
                                    mAdapter.notifyDataSetChanged();
                                    loading = true;
                                } else {
                                    if (!loading){
                                      //  Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    }

                                    loading = false;
                                }
                            } else {
                               // Toast.makeText(getContext(), data.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<SyllabusListModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_syllabus, container, false);
        addClasswork = view.findViewById(R.id.add_classwork);
        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
            }
        });
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_fragment_syllabus);

        mAdapter = new SyllabusListAdapter(getContext(),syllabusList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        syllabusworkswipelayout = view.findViewById(R.id.swipe_syllabus);
        syllabusworkswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                syllabusworkswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                          syllabusList.clear();
                    mAdapter.notifyDataSetChanged();
                    offset=0;
                getsyllabusList();
                            syllabusworkswipelayout.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });
        mAdapter.setOnClickListen(new SyllabusListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                syllabusList.get(position);
                Fragment fragment = new SubSyllabusFragment();
                Bundle bundle=new Bundle();
                bundle.putString("progvalue",syllabusList.get(position).getSyllabusList());
                bundle.putString("clsid",String.valueOf(syllabusList.get(position).getClassID()));
                bundle.putString("subid",String.valueOf(syllabusList.get(position).getSubjectID()));
                bundle.putString("secid",String.valueOf(syllabusList.get(position).getSectionID()));
                fragment.setArguments(bundle);
                replaceFragment(fragment);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset = offset + limit;
                            getsyllabusList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });


        // Analdata();
        return view;

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
//    private void Analdata() {
//        SyllabusListDTO s=new SyllabusListDTO();
//        s.setTitle("Class 5A");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("62%");
//        syllabusList.add(s);
//
//
//        s=new SyllabusListDTO();
//        s.setTitle("Class 6C");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("72%");
//        syllabusList.add(s);
//
//        s=new SyllabusListDTO();
//        s.setTitle("Class 7C");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("50%");
//        syllabusList.add(s);
//
//        s=new SyllabusListDTO();
//        s.setTitle("Class 8C");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("52%");
//        syllabusList.add(s);
//
//        s=new SyllabusListDTO();
//        s.setTitle("Class 9C");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("62%");
//        syllabusList.add(s);
//
//        s=new SyllabusListDTO();
//        s.setTitle("Class 10C");
//        s.setContent("English");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setCounts("65%");
//        syllabusList.add(s);
//    }


}
