package com.school.teacherparent.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.AssignmentMainAdapter;
import com.school.teacherparent.adapter.HomeworkAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AssignmentListResponse;

import com.school.teacherparent.models.HomeworkList;
import com.school.teacherparent.models.HomeworkListParms;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 10/22/2018.
 */

public class AssignmentHomeFragment extends BaseFragment
{ private RecyclerView homeworkRecyclerview;

    
    private AssignmentMainAdapter assignmentMainAdapter;
    SwipeRefreshLayout homeworkswipelayout;
    public  boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public static boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int limit=10;
    int offset=0;
    LinearLayoutManager linearLayoutManager;
    BaseActivity baseActivity;
    AssignmentListResponse homeworkListResponse;
    ArrayList<HomeworkList> homeworkLists;
    public ArrayList<AssignmentListResponse.assignmentsList> homeworkArrayList;
    public ArrayList<AssignmentListResponse.assignmentsList> homeworkArrayListadd;


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        currentPage = 0;
        lastEnd = false;
        if (isVisible)
        {
            homeworkArrayListadd.clear();
            assignmentMainAdapter.notifyDataSetChanged();
            getAssignmentList();
        }
        Log.d("VIKIS","AssignmentHomeFragment-START");

    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        isVisible = visible;
        if (isVisible && isStarted) {
            isStarted = true;
            currentPage = 0;
            if (isLoading)
            {
                homeworkArrayListadd.clear();
                assignmentMainAdapter.notifyDataSetChanged();
                getAssignmentList();
            }
            Log.d("VIKIS","AssignmentHomeFragment-VISIBLE");
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        isVisible = isVisibleToUser;
//        if (isVisible && isStarted) {
//            isStarted = true;
//            currentPage = 0;
//            if (isLoading)
//            {
//                getAssignmentList();
//            }
//            Log.d("VIKIS","AssignmentHomeFragment-VISIBLE");
//        }
//    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_home, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        homeworkRecyclerview = (RecyclerView) view.findViewById(R.id.recycle_sub_home);
        homeworkArrayList=new ArrayList<AssignmentListResponse.assignmentsList>();
        homeworkArrayListadd=new ArrayList<AssignmentListResponse.assignmentsList>();
        homeworkLists=new ArrayList<>();
        baseActivity = (BaseActivity) getActivity();
        assignmentMainAdapter = new AssignmentMainAdapter(homeworkArrayList,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        homeworkRecyclerview.setItemAnimator(new DefaultItemAnimator());
        homeworkRecyclerview.setLayoutManager(linearLayoutManager);
        homeworkRecyclerview.setAdapter(assignmentMainAdapter);

        homeworkswipelayout=view.findViewById(R.id.swipe_homework);

        homeworkswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homeworkswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                            homeworkArrayListadd.clear();
                            assignmentMainAdapter.notifyDataSetChanged();
                            getAssignmentList();
                            homeworkswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });


        return view;
    }


    private void getAssignmentList()
    {
        Log.d("Testworkcalling", "getAssignmentList: ");
        if (Util.isNetworkAvailable())
        {
            showProgress();
            HomeworkListParms homeworkListParms=new HomeworkListParms();
            homeworkListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            homeworkListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            homeworkListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            homeworkListParms.setLimit(limit);
            homeworkListParms.setOffset(offset);
            homeworkListParms.setClassImage(Constants.CLASSIMAGE);
            isLoading = false;
            vidyauraAPI.getAssignmentList(homeworkListParms).enqueue(new Callback<AssignmentListResponse>() {
                @Override
                public void onResponse(Call<AssignmentListResponse> call, Response<AssignmentListResponse> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                        homeworkListResponse = response.body();
                        if (homeworkListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            homeworkArrayList = response.body().getAssignmentsList();
                            HashMap Showtimingmap = null;
                            if (homeworkListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (homeworkArrayList.size() != 0) {
                                    homeworkArrayListadd.addAll(homeworkArrayList);
                                    assignmentMainAdapter = new AssignmentMainAdapter(homeworkArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));
                                    homeworkRecyclerview.setAdapter(assignmentMainAdapter);
                                  //  isLoading=false;
//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
//                                    for (int y = 0; y < homeworkArrayList.size(); y++)
//                                    {
//
//                                    }
                                    //homeworkAdapter.notifyDataSetChanged();


//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
                                }
//                                else if (eventLists.size() != 0 && eventsListArrayList.size() != 0) {
//                                    noeventListTextview.setVisibility(View.VISIBLE);
//                                    eventrecycle.setVisibility(View.GONE);
//                                    loading = false;
//                                }
                            } else {
                                Toast.makeText(getContext(), homeworkListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    }
                }
                @Override
                public void onFailure(Call<AssignmentListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else

        {
            isLoading = true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //getAssignmentList();
    }
}
