package com.school.teacherparent.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.CustomPagerAdapter;
import com.school.teacherparent.adapter.FeedscommentviewAdapter;
import com.school.teacherparent.adapter.MainSliderAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.CommentList;
import com.school.teacherparent.models.DeleteFeedParms;
import com.school.teacherparent.models.FeedCommentListParam;
import com.school.teacherparent.models.FeedCommentListResponse;
import com.school.teacherparent.models.FeedCommentcomposeParam;
import com.school.teacherparent.models.FeedCommentcomposeResponse;
import com.school.teacherparent.models.FeedLikeParmas;
import com.school.teacherparent.models.FeedLikeResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.PicassoImageLoadingService;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ss.com.bannerslider.Slider;


/**
 * Created by keerthana on 10/19/2018.
 */

public class FeedCommentviewFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private ImageView post;
    private SwipeRefreshLayout swiper;
    private FeedscommentviewAdapter feedscommentviewAdapter;


    public FeedCommentviewFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int feedId;
    FeedCommentListResponse feedCommentListResponse;
    ArrayList<FeedCommentListResponse.feedCommentsList.data> feedcommentListResponsearray;
    public TextView feedtitle, feedDescription, status, percentage, claps, clapsCount, commentsCount,
            shareCount,uname,schoolname,feeddate;
    public ImageView feedmenuticon, settings,clapImageview,image_comment_send;
    LinearLayout share_lay,commentLinear,clapLinearlayout;
    AppBarLayout appBarLayout;
    //ScrollView nestedScroollview;
    EditText commentEdittext;
    TextView nocemmentTextview;
    int feedCount=0;
    int IsUserLiked;
    List<CommentList> commentList;
    BaseActivity baseActivity;
    LinearLayoutManager linearLayoutManager;
    private boolean loading = true;
    int page_size=5;
    int page_number=1;
    NestedScrollView nestedScrollView;
    ImageView feedimageview,profile_image;
    ViewPager viewPager;
    Slider banner_slider1;
    ImageView ivLeftArrow,ivRightArrow;

    public static boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    int ispostedBy=0;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;


            isLoading=false;
        commentList.clear();
            getCommentList();

    }

    @Override
    public void onResume() {
        super.onResume();
        /*isStarted = true;


        isLoading=false;
        commentList.clear();
        getCommentList();*/

    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        isVisible = visible;

            isStarted = true;

            if (isLoading)
            {
                isLoading=false;
                commentList.clear();
                getCommentList();
            }




    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_feed_commentviewnew, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        recyclerView=view.findViewById(R.id.claps_recycle);
        baseActivity = (BaseActivity) getActivity();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.comments));
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            feedId = bundle.getInt("feedId", 0);

        }
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        commentList = new ArrayList<CommentList>();
        appBarLayout=view.findViewById(R.id.appbarFeedcommentview);
        appBarLayout.setVisibility(View.INVISIBLE);
        //nestedScroollview=view.findViewById(R.id.nestedScroollview);
        recyclerView.setVisibility(View.INVISIBLE);
        nocemmentTextview=view.findViewById(R.id.nocommentTextview);
        nocemmentTextview.setVisibility(View.INVISIBLE);
        image_comment_send=view.findViewById(R.id.image_comment_send);
        image_comment_send.setVisibility(View.INVISIBLE);
        //nestedScroollview.setVisibility(View.INVISIBLE);
        commentEdittext=view.findViewById(R.id.commentEdittext);
        nestedScrollView=view.findViewById(R.id.nestedScroollview);
        commentEdittext.setVisibility(View.INVISIBLE);
        share_lay = view.findViewById(R.id.share_lay);
        clapsCount = view.findViewById(R.id.claps_count);
        commentsCount = view.findViewById(R.id.comments_count);
        shareCount = view.findViewById(R.id.share_count);
        feedtitle = (TextView) view.findViewById(R.id.feedtitleTextview);
        feedDescription = (TextView) view.findViewById(R.id.feedDescriptionTextview);
        feedimageview=(ImageView)view.findViewById(R.id.feedimageview);
        viewPager=view.findViewById(R.id.viewpager);
        Slider.init(new PicassoImageLoadingService(getContext()));
        banner_slider1 = view.findViewById(R.id.banner_slider1);
        ivLeftArrow = view.findViewById(R.id.iv_left_arrow);
        ivRightArrow = view.findViewById(R.id.iv_right_arrow);

        profile_image=(ImageView)view.findViewById(R.id.profile_image);
           /* filmname = (TextView) view.findViewById(R.id.text_label_now_showing_film_name);
            category = (TextView) view.findViewById(R.id.text_label_film_category);
            status = (TextView) view.findViewById(R.id.text_label_status);
            percentage = (TextView) view.findViewById(R.id.text_label_now_showing_percentage);
            views = (TextView) view.findViewById(R.id.text_label_now_showing_views);
            image_now_showing = view.findViewById(R.id.image_now_showing);
*/
        uname=view.findViewById(R.id.uname);
        schoolname=view.findViewById(R.id.schoolname);
        feeddate=view.findViewById(R.id.feeddate);
        commentLinear=view.findViewById(R.id.commentLinear);
        feedmenuticon=view.findViewById(R.id.feedmenuticon);
        claps=view.findViewById(R.id.claps);
        clapImageview=view.findViewById(R.id.clapsImageview);
        clapLinearlayout=view.findViewById(R.id.clapLinearlayout);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        feedcommentListResponsearray=new  ArrayList<FeedCommentListResponse.feedCommentsList.data>();
        feedscommentviewAdapter = new FeedscommentviewAdapter(commentList,getContext(),baseActivity,feedId);

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        feedmenuticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_custom_edit_delete,null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);
                final TextView report = alertLayout.findViewById(R.id.report);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme));
                alert.setView(alertLayout);
                final AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                if (ispostedBy==1)
                {
                    edit.setVisibility(View.VISIBLE);
                    delete.setVisibility(View.VISIBLE);
                    report.setVisibility(View.VISIBLE);
                }
                else
                {
                    edit.setVisibility(View.GONE);
                    delete.setVisibility(View.GONE);
                    report.setVisibility(View.VISIBLE);
                }
                report.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View alertLayout = inflater.inflate(R.layout.layout_feed_report_comment, null);
                        final EditText edit = alertLayout.findViewById(R.id.edittext_report);
                        final Button reportButton = alertLayout.findViewById(R.id.report_button);
                        AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.actionSheetTheme));
                        alert.setView(alertLayout);
                        final AlertDialog dialog = alert.create();
                        dialog.show();
                        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
                        reportButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edit.getText().toString().length()>0)
                                {
                                    DeleteOrReportFeed(1,feedId,edit.getText().toString().trim());
                                    dialog.dismiss();
                                }
                                else
                                {
                                    edit.setError(getString(R.string.enter_report));
                                }
                            }
                        });
                    }
                });
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", feedId));

                        }
                        else
                        {
                            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Util.isNetworkAvailable()) {
                            showProgress();
                            DeleteFeedParms feedLikeParmas=new DeleteFeedParms();
                            feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
                            feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                            feedLikeParmas.setFeedID(String.valueOf(feedId));
                            feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
                            feedLikeParmas.setIsReportorDelete(0);

                            vidyauraAPI.deleteFeed(feedLikeParmas).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    hideProgress();
                                    if (response.body()!=null)
                                    {
                                        AddFeedResponse feedLikeResponse=response.body();
                                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                                Toast.makeText(getActivity(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                getActivity().finish();


                                            } else {
                                                Toast.makeText(getActivity(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        else
                                        {
                                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(i);
                                            getActivity().finishAffinity();
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    hideProgress();
                                    Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else {
                            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();

                    }

                }

                });
            }
        });
        nestedScrollView.setSmoothScrollingEnabled(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(feedscommentviewAdapter);

        //getCommentList();
        clapsCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    if (feedCount>0) {
                        startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "claps").putExtra("feedID", feedId));
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });
        clapLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isNetworkAvailable()) {
                    showProgress();
                    FeedLikeParmas feedLikeParmas=new FeedLikeParmas();
                    feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
                    feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                    feedLikeParmas.setFeedID(String.valueOf(feedId));
                    feedLikeParmas.setCommentID("0");
                    feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
                    if (IsUserLiked==0)
                    {
                        feedLikeParmas.setClapStatus("1");
                    }
                    else
                    {
                        feedLikeParmas.setClapStatus("0");
                    }

                    vidyauraAPI.updateFeedClaps(feedLikeParmas).enqueue(new Callback<FeedLikeResponse>() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onResponse(Call<FeedLikeResponse> call, Response<FeedLikeResponse> response) {
                            hideProgress();
                            if (response.body()!=null)
                            {
                                FeedLikeResponse feedLikeResponse=response.body();
                                if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                                    if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                        Toast.makeText(getActivity(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        if (feedLikeResponse.getClapStatus() == 1) {
                                            claps.setTextColor(getContext().getColor(R.color.colorPrimary));
                                            IsUserLiked = 1;
                                            feedCount = feedLikeResponse.getFeedLikeCount();
                                            clapsCount.setText("" + feedLikeResponse.getFeedLikeCount() + " " + getContext().getString(R.string.claps));
                                            claps.setText(getContext().getString(R.string.claps));

                                            if (feedLikeResponse.getFeedLikeCount()==0 ||feedLikeResponse.getFeedLikeCount()==1)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.clap));
                                                claps.setText(getString(R.string.clap));
                                            }
                                            else if (feedLikeResponse.getFeedLikeCount()>=2 && feedLikeResponse.getFeedLikeCount()<=98)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));

                                            }
                                            else if (feedLikeResponse.getFeedLikeCount()>=99 && feedLikeResponse.getFeedLikeCount()<=999)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));
                                            }
                                            else
                                            {
                                                clapsCount.setText("999+"+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));
                                            }



                                            clapImageview.setImageResource(R.mipmap.clap);
                                        } else {
                                            claps.setTextColor(getContext().getColor(R.color.text_color_light));
                                            IsUserLiked = 0;
                                            feedCount = feedLikeResponse.getFeedLikeCount();
                                            clapImageview.setImageResource(R.mipmap.unclap_icon);
                                            if (feedLikeResponse.getFeedLikeCount()==0 ||feedLikeResponse.getFeedLikeCount()==1)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.clap));
                                                claps.setText(getString(R.string.clap));
                                            }
                                            else if (feedLikeResponse.getFeedLikeCount()>=2 && feedLikeResponse.getFeedLikeCount()<=98)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));

                                            }
                                            else if (feedLikeResponse.getFeedLikeCount()>=99 && feedLikeResponse.getFeedLikeCount()<=999)
                                            {
                                                clapsCount.setText(""+feedLikeResponse.getFeedLikeCount()+" "+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));
                                            }
                                            else
                                            {
                                                clapsCount.setText("999+"+getString(R.string.claps));
                                                claps.setText(getString(R.string.claps));
                                            }

                                        }
                                    } else {
                                        Toast.makeText(getActivity(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else
                                {
                                    Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    getActivity().finishAffinity();
                                }
                            }
                            else
                            {
                                Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<FeedLikeResponse> call, Throwable t) {
                            hideProgress();
                            Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                else
                {
                    Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });
        image_comment_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentEdittext.getText().toString().trim().length()>0)
                {
                    addFeedComment(commentEdittext.getText().toString().trim());
                }
                else
                {
                    commentEdittext.setError(getString(R.string.entersomethintopost));
                }
            }
        });
       /* if (nestedScrollView != null) {*/

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
            {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy)
                {

                    int pastVisiblesItems, visibleItemCount, totalItemCount;
                    if(dy > 0) //check for scroll down
                    {
                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                        if (loading)
                        {
                            if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                            {
                                loading = false;
                                Log.d("...", "Last Item Wow !");
                                page_number=page_number+1;
                                getCommentList();
                                //Do pagination.. i.e. fetch new data
                            }
                        }
                    }
                }
            });

            /*nestedScrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    int pastVisiblesItems, visibleItemCount, totalItemCount;
                    if (v.getChildAt(v.getChildCount() - 1) != null) {

                            visibleItemCount = linearLayoutManager.getChildCount();
                            totalItemCount = linearLayoutManager.getItemCount();
                            pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                            if (loading)
                            {

                                    loading = false;
                                    Log.d("...", "Last Item Wow !");
                                    page_number=page_number+1;
                                    getCommentList();
                                    //Do pagination.. i.e. fetch new data

                            }

                    }
                }

            });*/
        /*}*/

        share_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Vidyaura")
                        .setMessage("Are you sure you want to share?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if (Util.isNetworkAvailable()) {
                                    DeleteFeedParms feedLikeParmas=new DeleteFeedParms();
                                    feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
                                    feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                                    feedLikeParmas.setFeedID(String.valueOf(feedId));

                                    vidyauraAPI.updateFeedShare(feedLikeParmas).enqueue(new Callback<AddFeedResponse>() {
                                        @Override
                                        public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                            if (response.body()!=null)
                                            {
                                                AddFeedResponse feedLikeResponse=response.body();
                                                if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                                                    if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {
                                                        if (feedCommentListResponse.getFeedCommentsList() != null) {
                                                            if (feedCommentListResponse.getFeedCommentsList().getData().size() > 0) {
                                                                Intent share = new Intent(Intent.ACTION_SEND);
                                                                share.setType("text/plain");
                                                                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                                                share.putExtra(Intent.EXTRA_SUBJECT, feedCommentListResponse.getFeedCommentsList().getData().get(0).getTitle());
                                                                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/" +
                                                                        feedCommentListResponse.getFeedCommentsList().getData().get(0).getAttachmentsList().get(0));
                                                                startActivity(Intent.createChooser(share, "Share link!"));
                                                            }
                                                        }

                                                        if (feedCommentListResponse.getFeedDetailsList() != null) {
                                                            if (feedCommentListResponse.getFeedDetailsList().size() > 0) {
                                                                Intent share = new Intent(Intent.ACTION_SEND);
                                                                share.setType("text/plain");
                                                                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                                                                share.putExtra(Intent.EXTRA_SUBJECT, feedCommentListResponse.getFeedDetailsList().get(0).getTitle());
                                                                share.putExtra(Intent.EXTRA_TEXT, getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/" +
                                                                        feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().get(0));
                                                                startActivity(Intent.createChooser(share, "Share link!"));
                                                            }
                                                        }
                                                    } else {
                                                        Toast.makeText(getActivity(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                                else
                                                {
                                                    Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(i);
                                                    getActivity().finishAffinity();
                                                }
                                            }
                                            else
                                            {
                                                Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                            hideProgress();
                                            Toast.makeText(getActivity(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                                else {
                                    Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();

                                }
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
//        {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
//            {
//
//                int pastVisiblesItems, visibleItemCount, totalItemCount;
//                if(dy > 0) //check for scroll down
//                {
//                    visibleItemCount = linearLayoutManager.getChildCount();
//                    totalItemCount = linearLayoutManager.getItemCount();
//                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
//
//                    if (loading)
//                    {
//                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
//                        {
//                            loading = false;
//                            Log.d("...", "Last Item Wow !");
//                            page_number=page_number+1;
//                            getCommentList();
//                            //Do pagination.. i.e. fetch new data
//                        }
//                    }
//                }
//            }
//        });
        return view;
    }

    private void addFeedComment(final String commentDescription)
    {
        if (Util.isNetworkAvailable()) {
            showProgress();

            FeedCommentcomposeParam feedCommentcomposeParam=new FeedCommentcomposeParam();
            feedCommentcomposeParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedCommentcomposeParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedCommentcomposeParam.setFeedID(String.valueOf(feedId));
            feedCommentcomposeParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedCommentcomposeParam.setCommentDescription(commentDescription);
            vidyauraAPI.composeFeedComment(feedCommentcomposeParam).enqueue(new Callback<FeedCommentcomposeResponse>() {
                @Override
                public void onResponse(Call<FeedCommentcomposeResponse> call, Response<FeedCommentcomposeResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        FeedCommentcomposeResponse feedCommentcomposeResponse=response.body();
                        if (feedCommentcomposeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedCommentcomposeResponse.getStatus() == Util.STATUS_SUCCESS) {
                                recyclerView.setVisibility(View.VISIBLE);
                                nocemmentTextview.setVisibility(View.INVISIBLE);
                                commentEdittext.setText("");
                                int commentID= Integer.parseInt(feedCommentcomposeResponse.getCommentID());

                                String type = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                                    type = "teachers";
                                } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                                    type = "parents";
                                }

                                commentList.add(new CommentList(0, feedId,
                                        commentDescription, sharedPreferences.getString(Constants.FNAME, ""), sharedPreferences.getString(Constants.LNAME, ""),
                                        sharedPreferences.getString(Constants.PROFILE_PHOTO,""),
                                        feedCommentcomposeResponse.getCommentPostedTime(), "", "",
                                        0, 0, 0,
                                        0, 0, 0,
                                        feedCommentcomposeResponse.getCommentPostedTime(), "",0,"0","0",commentID,type));
                                feedscommentviewAdapter = new FeedscommentviewAdapter(commentList, getContext(),baseActivity,feedId);
                                recyclerView.setAdapter(feedscommentviewAdapter);
                                feedscommentviewAdapter.notifyDataSetChanged();
                                Toast.makeText(getContext(), feedCommentcomposeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                               // commentsCount.setText("" + feedCommentcomposeResponse.getFeedCommentCount() + " " + getString(R.string.comments));
                                if (feedCommentcomposeResponse.getFeedCommentCount()==0 ||feedCommentcomposeResponse.getFeedCommentCount()==1)
                                {
                                    commentsCount.setText(""+feedCommentcomposeResponse.getFeedCommentCount()+" "+getString(R.string.comment));

                                }
                                else if (feedCommentcomposeResponse.getFeedCommentCount()>=2 && feedCommentcomposeResponse.getFeedCommentCount()<=98)
                                {
                                    commentsCount.setText(""+feedCommentcomposeResponse.getFeedCommentCount()+" "+getString(R.string.comments));


                                }
                                else if (feedCommentcomposeResponse.getFeedCommentCount()>=99 && feedCommentcomposeResponse.getFeedCommentCount()<=999)
                                {
                                    commentsCount.setText(""+feedCommentcomposeResponse.getFeedCommentCount()+" "+getString(R.string.comments));

                                }
                                else
                                {
                                    commentsCount.setText("999+"+getString(R.string.comments));

                                }

                            } else {
                                Toast.makeText(getContext(), feedCommentcomposeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<FeedCommentcomposeResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getCommentList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            FeedCommentListParam feedCommentListParam=new FeedCommentListParam();
            feedCommentListParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedCommentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedCommentListParam.setFeedID(String.valueOf(feedId));
            feedCommentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedCommentListParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));




            vidyauraAPI.getcommentListByfield(sharedPreferences.getString(Constants.PHONE,""),String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0))
                    ,sharedPreferences.getString(Constants.USERID,""),
                    String.valueOf(page_size), String.valueOf(page_number),String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)), String.valueOf(feedId)).enqueue(new Callback<FeedCommentListResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @SuppressLint("ResourceAsColor")
                @Override
                public void onResponse(Call<FeedCommentListResponse> call, Response<FeedCommentListResponse> response) {
                    hideProgress();
                    isLoading=true;
                    if (response.body() != null) {
                        feedCommentListResponse=response.body();
                        if (feedCommentListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedCommentListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                appBarLayout.setVisibility(View.VISIBLE);
                                commentEdittext.setVisibility(View.VISIBLE);
                                image_comment_send.setVisibility(View.VISIBLE);
                                if (feedCommentListResponse.getFeedDetailsList().size() > 0) {
                                    Log.d("TEST","checking-1");

                                    feedCount = feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount();
                                    IsUserLiked = feedCommentListResponse.getFeedDetailsList().get(0).getIsUserLiked();
                                    feedtitle.setText(feedCommentListResponse.getFeedDetailsList().get(0).getTitle());
                                    feedDescription.setText(feedCommentListResponse.getFeedDetailsList().get(0).getDescription());
                                    feeddate.setText(getDateFormat(feedCommentListResponse.getFeedDetailsList().get(0).getPublish_date()));
                                    //clapsCount.setText("" + feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount() + " " + getString(R.string.claps));
                                    //commentsCount.setText("" + feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount() + " " + getString(R.string.comments));
                                    shareCount.setText("" + feedCommentListResponse.getFeedDetailsList().get(0).getFeedShareCount() + " " + getString(R.string.shares));
                                    //feeddate.setText(getDateFormat(feedCommentListResponse.getFeedDetailsList().get(0).getCreated_at()));
                                    //claps.setText("" + feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount() + " " + getString(R.string.claps));
                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getAuthor_type().equals("Admin"))
                                    {
                                        uname.setText(feedCommentListResponse.getFeedDetailsList().get(0).getName());
                                    }
                                    else
                                    {

                                        Log.d("TEST","checking-2");
                                            uname.setText(feedCommentListResponse.getFeedDetailsList().get(0).getFeedPostedByUserDetails().getFname() + " " + feedCommentListResponse.getFeedDetailsList().get(0).getFeedPostedByUserDetails().getLname());

//                                        else
//                                        {
//                                            uname.setText(feedCommentListResponse.getFeedCommentsList().getData().get(0).getFeedPostedByUserDetails().getFname() + " "
//                                                    + feedCommentListResponse.getFeedCommentsList().getData().get(0).getFeedPostedByUserDetails().getLname());
//                                        }
                                    }

                                    schoolname.setText(feedCommentListResponse.getFeedDetailsList().get(0).getSchool_name());
                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getIsUserLiked() == 1) {
                                        claps.setTextColor(getContext().getColor(R.color.colorPrimary));
                                        clapImageview.setImageResource(R.mipmap.clap);
                                    } else {
                                        claps.setTextColor(getContext().getColor(R.color.text_color_light));
                                        clapImageview.setImageResource(R.mipmap.unclap_icon);
                                    }


                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()==0 ||feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()==1)
                                    {
                                        clapsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()+" "+getString(R.string.clap));
                                        claps.setText(getString(R.string.clap));
                                    }
                                    else if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()>=2 && feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()<=98)
                                    {
                                        clapsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()+" "+getString(R.string.claps));
                                        claps.setText(getString(R.string.claps));

                                    }
                                    else if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()>=99 && feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()<=999)
                                    {
                                        clapsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedLikeCount()+" "+getString(R.string.claps));
                                        claps.setText(getString(R.string.claps));
                                    }
                                    else
                                    {
                                        clapsCount.setText("999+"+getString(R.string.claps));
                                        claps.setText(getString(R.string.claps));
                                    }


                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()==0 ||feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()==1)
                                    {
                                        commentsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()+" "+getString(R.string.comment));

                                    }
                                    else if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()>=2 && feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()<=98)
                                    {
                                        commentsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()+" "+getString(R.string.comments));


                                    }
                                    else if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()>=99 && feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()<=999)
                                    {
                                        commentsCount.setText(""+feedCommentListResponse.getFeedDetailsList().get(0).getFeedCommentCount()+" "+getString(R.string.comments));

                                    }
                                    else
                                    {
                                        commentsCount.setText("999+"+getString(R.string.comments));

                                    }

                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getIsFeedPostedBy() == 1) {
                                        feedmenuticon.setVisibility(View.VISIBLE);
                                        ispostedBy=1;
                                    } else {
                                        feedmenuticon.setVisibility(View.VISIBLE);
                                        ispostedBy=0;
                                    }

                                    if (!feedCommentListResponse.getFeedDetailsList().get(0).getAuthor_type().equals("Admin")) {
                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedPostedByUserDetails()!=null) {
                                        if (feedCommentListResponse.getFeedDetailsList().get(0).getFeedPostedByUserDetails().getEmp_photo() != null) {
                                            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" + feedCommentListResponse.getFeedDetailsList().get(0).getFeedPostedByUserDetails().getEmp_photo();

                                            Picasso.get().load(url).


                                                    into(profile_image, new com.squareup.picasso.Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {


                                                                }
                                                            }, 1000);


                                                        }

                                                        @Override
                                                        public void onError(Exception e) {
                                                            profile_image.setImageResource(R.drawable.ic_user);
                                                        }
                                                    });

                                        }
                                    } }
                                    else
                                    {
                                            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_schools) + feedCommentListResponse.getFeedDetailsList().get(0).getLogo();
                                            Picasso.get().load(url).


                                                    into(profile_image, new com.squareup.picasso.Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {


                                                                }
                                                            }, 1000);


                                                        }

                                                        @Override
                                                        public void onError(Exception e) {
                                                            profile_image.setImageResource(R.drawable.ic_user);
                                                        }
                                                    });

                                    }
                                    if (feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().size()>0) {

                                        /*banner_slider1.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                banner_slider1.setAdapter(new MainSliderAdapter(getContext(),feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList()));
                                                banner_slider1.setSelectedSlide(0);
                                            }
                                        }, 100);*/

                                        if (feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().size()>0) {

                                        if (feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().size()>1) {
                                            ivLeftArrow.setVisibility(View.VISIBLE);
                                            ivRightArrow.setVisibility(View.VISIBLE);
                                        }

                                            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                                @Override
                                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                                }

                                                @Override
                                                public void onPageSelected(int position) {

                                                }

                                                @Override
                                                public void onPageScrollStateChanged(int state) {
                                                    JzvdStd.releaseAllVideos();
                                                }
                                            });

                                            ivLeftArrow.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    JzvdStd.releaseAllVideos();
                                                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
                                                }
                                            });
                                            ivRightArrow.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    JzvdStd.releaseAllVideos();
                                                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
                                                }
                                            });
                                        } else {
                                            ivLeftArrow.setVisibility(View.GONE);
                                            ivRightArrow.setVisibility(View.GONE);
                                        }
                                        viewPager.setAdapter(new CustomPagerAdapter(getContext(), 7,  feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList()));
                                        String url=getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().get(0);
                                        Log.d("image", "onBindViewHolder: "+getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+feedCommentListResponse.getFeedDetailsList().get(0).getAttachmentsList().get(0));
                                        Picasso.get().load(url).


                                                into(feedimageview, new com.squareup.picasso.Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        new Handler().postDelayed(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                try {
                                                                    BitmapDrawable drawable = (BitmapDrawable) feedimageview.getDrawable();
                                                                    Bitmap bitmap = drawable.getBitmap();
                                                                    int bitmapwidth=0;
                                                                    int bitmpheight=0;
                                                                    bitmapwidth=bitmap.getWidth();
                                                                    bitmpheight=bitmap.getHeight();
                                                                    LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)feedimageview.getLayoutParams();
                                                                    if (bitmapwidth==0||bitmpheight==0){
                                                                        return;
                                                                    }
                                                                    int newwidth=feedimageview.getWidth();
                                                                    int newheight=newwidth*bitmpheight/bitmapwidth;
                                                                    params.height=newheight;
                                                                    feedimageview.setLayoutParams(params);

                                                                }
                                                                catch (Exception e){

                                                                }

                                                            }
                                                        },1000);


                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                       feedimageview.setImageResource(R.drawable.client);
                                                    }
                                                });
                                    }

                                }
                                else {


                                    feedcommentListResponsearray = feedCommentListResponse.getFeedCommentsList().getData();
                                    for (int i = 0; i < feedcommentListResponsearray.size(); i++) {
                                        commentList.add(new CommentList(feedcommentListResponsearray.get(i).getId(), feedcommentListResponsearray.get(i).getFeed_id(),
                                                feedcommentListResponsearray.get(i).getComment(),
                                                feedcommentListResponsearray.get(i).getFeedCommentedByUserDetails().get(0).getFname(),
                                                feedcommentListResponsearray.get(i).getFeedCommentedByUserDetails().get(0).getLname(),
                                                feedcommentListResponsearray.get(i).getFeedCommentedByUserDetails().get(0).getEmp_photo(),
                                                feedcommentListResponsearray.get(i).getCommentedDateTime(), feedcommentListResponsearray.get(i).getTitle(), feedcommentListResponsearray.get(i).getDescription(),
                                                feedcommentListResponsearray.get(i).getIsFeedPostedBy(), feedcommentListResponsearray.get(i).getFeedLikeCount(), feedcommentListResponsearray.get(i).getFeedCommentCount(),
                                                feedcommentListResponsearray.get(i).getFeedShareCount(), feedcommentListResponsearray.get(i).getIsUserLiked(), feedcommentListResponsearray.get(i).getIsUserCommented(),
                                                feedcommentListResponsearray.get(i).getPublish_date(), feedcommentListResponsearray.get(i).getSchool_name(),feedcommentListResponsearray.get(i).getFeedReplyCommentCount(),
                                                feedcommentListResponsearray.get(i).getFeedCommentClapsCount(), feedcommentListResponsearray.get(i).getIsUserLikedComment(),
                                                feedcommentListResponsearray.get(i).getComment_ID(),feedcommentListResponsearray.get(i).getComment_user_type()));

                                }
                                    loading = true;
                                    Gson gson = new Gson();
                                    System.out.println("reply list ==> "+gson.toJson(response.body()));
                                    feedscommentviewAdapter = new FeedscommentviewAdapter(commentList, getContext(),baseActivity,feedId);
                                    //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                                    if (feedcommentListResponsearray.size() > 0) {
                                        recyclerView.setVisibility(View.VISIBLE);
                                        feedCount = feedcommentListResponsearray.get(0).getFeedLikeCount();
                                        IsUserLiked = feedcommentListResponsearray.get(0).getIsUserLiked();
                                        feedtitle.setText(feedcommentListResponsearray.get(0).getTitle());
                                        feedDescription.setText(feedcommentListResponsearray.get(0).getDescription());
                                        feeddate.setText(getDateFormat(feedcommentListResponsearray.get(0).getPublish_date()));
                                        //clapsCount.setText("" + feedcommentListResponsearray.get(0).getFeedLikeCount() + " " + getString(R.string.claps));
                                        //commentsCount.setText("" + feedcommentListResponsearray.get(0).getFeedCommentCount() + " " + getString(R.string.comments));
                                        //shareCount.setText("" + feedcommentListResponsearray.get(0).getFeedCommentCount() + " " + getString(R.string.shares));
                                        //feeddate.setText(getDateFormat(feedcommentListResponsearray.get(0).getCreated_at()));
                                        //claps.setText("" + feedcommentListResponsearray.get(0).getFeedLikeCount() + " " + getString(R.string.claps));
                                       // uname.setText(feedcommentListResponsearray.get(0).getFname() + " " + feedcommentListResponsearray.get(0).getLname());
                                        uname.setText(feedCommentListResponse.getFeedCommentsList().getData().get(0).getFeedPostedByUserDetails().getFname() + " "
                                                + feedCommentListResponse.getFeedCommentsList().getData().get(0).getFeedPostedByUserDetails().getLname());
                                        schoolname.setText(feedcommentListResponsearray.get(0).getSchool_name());
//                                        if (feedcommentListResponsearray.get(0).getIsUserLiked() == 1) {
//                                            claps.setTextColor(getContext().getColor(R.color.colorPrimary));
//
//
//                                            //holder.clapImageview.setImageResource(R.mipmap.clap);
//                                        } else {
//                                            claps.setTextColor(getContext().getColor(R.color.text_color_light));
//                                        }

                                        if (feedcommentListResponsearray.get(0).getFeedLikeCount()==0 ||feedcommentListResponsearray.get(0).getFeedLikeCount()==1)
                                        {
                                            clapsCount.setText(""+feedcommentListResponsearray.get(0).getFeedLikeCount()+" "+getString(R.string.clap));
                                            claps.setText(getString(R.string.clap));
                                        }
                                        else if (feedcommentListResponsearray.get(0).getFeedLikeCount()>=2 && feedcommentListResponsearray.get(0).getFeedLikeCount()<=98)
                                        {
                                            clapsCount.setText(""+feedcommentListResponsearray.get(0).getFeedLikeCount()+" "+getString(R.string.claps));
                                            claps.setText(getString(R.string.claps));

                                        }
                                        else if (feedcommentListResponsearray.get(0).getFeedLikeCount()>=99 && feedcommentListResponsearray.get(0).getFeedLikeCount()<=999)
                                        {
                                            clapsCount.setText(""+feedcommentListResponsearray.get(0).getFeedLikeCount()+" "+getString(R.string.claps));
                                            claps.setText(getString(R.string.claps));
                                        }
                                        else
                                        {
                                            clapsCount.setText("999+"+getString(R.string.claps));
                                            claps.setText(getString(R.string.claps));
                                        }


                                        if (feedcommentListResponsearray.get(0).getFeedCommentCount()==0 ||feedcommentListResponsearray.get(0).getFeedCommentCount()==1)
                                        {
                                            commentsCount.setText(""+feedcommentListResponsearray.get(0).getFeedCommentCount()+" "+getString(R.string.comment));

                                        }
                                        else if (feedcommentListResponsearray.get(0).getFeedCommentCount()>=2 && feedcommentListResponsearray.get(0).getFeedCommentCount()<=98)
                                        {
                                            commentsCount.setText(""+feedcommentListResponsearray.get(0).getFeedCommentCount()+" "+getString(R.string.comments));


                                        }
                                        else if (feedcommentListResponsearray.get(0).getFeedCommentCount()>=99 && feedcommentListResponsearray.get(0).getFeedCommentCount()<=999)
                                        {
                                            commentsCount.setText(""+feedcommentListResponsearray.get(0).getFeedCommentCount()+" "+getString(R.string.comments));

                                        }
                                        else
                                        {
                                            commentsCount.setText("999+"+getString(R.string.comments));

                                        }

                                        if (feedcommentListResponsearray.get(0).getIsUserLiked() == 1) {
                                            claps.setTextColor(getContext().getColor(R.color.colorPrimary));
                                            clapImageview.setImageResource(R.mipmap.clap);
                                        } else {
                                            claps.setTextColor(getContext().getColor(R.color.text_color_light));
                                            clapImageview.setImageResource(R.mipmap.unclap_icon);
                                        }


                                        if (feedcommentListResponsearray.get(0).getIsFeedPostedBy() == 1) {
                                            feedmenuticon.setVisibility(View.VISIBLE);
                                            ispostedBy=1;
                                        } else {
                                            feedmenuticon.setVisibility(View.VISIBLE);
                                            ispostedBy=0;
                                        }

                                        if (!feedcommentListResponsearray.get(0).getAuthor_type().equals("Admin")) {
                                        if (feedcommentListResponsearray.get(0).getFeedPostedByUserDetails().getEmp_photo()!=null)
                                        {
                                            String url=getString(R.string.s3_baseurl)+getString(R.string.s3_employee)+"/"+feedcommentListResponsearray.get(0).getFeedPostedByUserDetails().getEmp_photo();

                                            Picasso.get().load(url).


                                                    into(profile_image, new com.squareup.picasso.Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {


                                                                }
                                                            },1000);


                                                        }

                                                        @Override
                                                        public void onError(Exception e) {
                                                            profile_image.setImageResource(R.mipmap.profile);
                                                        }
                                                    });

                                        } } else {
                                                String url = getString(R.string.s3_baseurl) + getString(R.string.s3_schools) + feedcommentListResponsearray.get(0).getLogo();
                                                Picasso.get().load(url).


                                                        into(profile_image, new com.squareup.picasso.Callback() {
                                                            @Override
                                                            public void onSuccess() {
                                                                new Handler().postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {


                                                                    }
                                                                }, 1000);


                                                            }

                                                            @Override
                                                            public void onError(Exception e) {
                                                                profile_image.setImageResource(R.drawable.ic_user);
                                                            }
                                                        });

                                        }
                                        if (feedcommentListResponsearray.get(0).getAttachmentsList().size()>0) {

                                            /*banner_slider1.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    banner_slider1.setAdapter(new MainSliderAdapter(getContext(),feedcommentListResponsearray.get(0).getAttachmentsList()));
                                                    banner_slider1.setSelectedSlide(0);
                                                }
                                            }, 100);*/
                                            if (feedcommentListResponsearray.get(0).getAttachmentsList().size()>1) {
                                                ivLeftArrow.setVisibility(View.VISIBLE);
                                                ivRightArrow.setVisibility(View.VISIBLE);
                                            }
                                            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                                @Override
                                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                                }

                                                @Override
                                                public void onPageSelected(int position) {

                                                }

                                                @Override
                                                public void onPageScrollStateChanged(int state) {
                                                    JzvdStd.releaseAllVideos();
                                                }
                                            });

                                            ivLeftArrow.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    JzvdStd.releaseAllVideos();
                                                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
                                                }
                                            });
                                            ivRightArrow.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    JzvdStd.releaseAllVideos();
                                                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
                                                }
                                            });

                                            viewPager.setAdapter(new CustomPagerAdapter(getContext(), 7,  feedcommentListResponsearray.get(0).getAttachmentsList()));
                                            String url=getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+feedcommentListResponsearray.get(0).getAttachmentsList().get(0);
                                            Log.d("image", "onBindViewHolder: "+getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+feedcommentListResponsearray.get(0).getAttachmentsList().get(0));
                                            Picasso.get().load(url).


                                                    into(feedimageview, new com.squareup.picasso.Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            new Handler().postDelayed(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        BitmapDrawable drawable = (BitmapDrawable) feedimageview.getDrawable();
                                                                        Bitmap bitmap = drawable.getBitmap();
                                                                        int bitmapwidth=0;
                                                                        int bitmpheight=0;
                                                                        bitmapwidth=bitmap.getWidth();
                                                                        bitmpheight=bitmap.getHeight();
                                                                        LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)feedimageview.getLayoutParams();
                                                                        if (bitmapwidth==0||bitmpheight==0){
                                                                            return;
                                                                        }
                                                                        int newwidth=feedimageview.getWidth();
                                                                        int newheight=newwidth*bitmpheight/bitmapwidth;
                                                                        params.height=newheight;
                                                                        feedimageview.setLayoutParams(params);

                                                                    }
                                                                    catch (Exception e){

                                                                    }

                                                                }
                                                            },1000);


                                                        }

                                                        @Override
                                                        public void onError(Exception e) {
                                                            feedimageview.setImageResource(R.drawable.client);
                                                        }
                                                    });
                                        }
                                    }
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setNestedScrollingEnabled(false);
                                    recyclerView.setHasFixedSize(false);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(feedscommentviewAdapter);

                                    if (commentList.size()<=0) {
                                        nocemmentTextview.setVisibility(View.VISIBLE);
                                    }else{
                                        nocemmentTextview.setVisibility(View.INVISIBLE);
                                    }

                                }


                            } else {
                                loading = false;
                                isLoading=true;
                                Toast.makeText(getContext(), feedCommentListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        else
                        {
                            isLoading=true;
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }

                    }
                    else
                    {
                        isLoading=true;
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        getActivity().finish();


                    }
                }

                @Override
                public void onFailure(Call<FeedCommentListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    loading = false;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    getActivity().finish();

                }
            });

        }
        else

        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public void DeleteOrReportFeed(final int isDeleteorReport, final int feedId, final String reportReason)
    {
        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            DeleteFeedParms feedLikeParmas=new DeleteFeedParms();
            feedLikeParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedLikeParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedLikeParmas.setFeedID(String.valueOf(feedId));
            feedLikeParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            feedLikeParmas.setIsReportorDelete(isDeleteorReport);
            feedLikeParmas.setReportReason(reportReason);


            vidyauraAPI.deleteFeed(feedLikeParmas).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    baseActivity.hideProgress();
                    if (response.body()!=null)
                    {
                        AddFeedResponse feedLikeResponse=response.body();
                        if (feedLikeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                            if (feedLikeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                Toast.makeText(getContext(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.FROMADDFEED, "yes").commit();
                                getActivity().onBackPressed();



                            } else {
                                Toast.makeText(getContext(), feedLikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            baseActivity.hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedLikeResponse.getToken()).commit();
                            DeleteOrReportFeed(isDeleteorReport,feedId,reportReason);
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}
