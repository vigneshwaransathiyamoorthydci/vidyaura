package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ExamComposechapterAdpater;
import com.school.teacherparent.adapter.ExamComposetopicAdpater;
import com.school.teacherparent.adapter.SelectedImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AcademicTermResponse;
import com.school.teacherparent.models.AddAssignmentParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AddhomeworkParams;
import com.school.teacherparent.models.ChapterListResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.GetChapterListParam;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.GetTopicListParam;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.models.ListofChapter;
import com.school.teacherparent.models.ListofChapterforDialog;
import com.school.teacherparent.models.ListofTopic;
import com.school.teacherparent.models.ListofTopicforDialog;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.models.SubjectListResponse;
import com.school.teacherparent.models.TopicListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by harini on 10/23/2018.
 */

public class AddHomeAssignmentFragment extends BaseFragment  implements OnImageRemoved {

    AddHomeWorkTabFragment addHomeWorkTabFragment;
    private Spinner class_spinner, sub_spinner, chap_spinner, topic_spinner,term_spinner;
    public static EditText edittextDesc;
    private LinearLayout date_picker;

    private View btnDatePicker;
    private int mYear, mMonth, mDay, mHour, mMinute;

    private TextView dateTextview;
    private LinearLayout lin_date;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY=1;

    private static final String IMAGE_DIRECTORY = "/demonuts";
    private ImageView img_upload;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ClassListResponse classListResponse;
    ArrayList<String> classlist = new ArrayList<String>();
    ArrayAdapter<String> classspinnerArray;
    ArrayList<String> subjectlist = new ArrayList<String>();
    SubjectListResponse subjectListResponse;
    public  static int examTermId = 0, classId = 0, sectionId = 0, subject_Id = 0, chapter_Id = 0, topic_Id = 0,term_Id=0;
    ArrayAdapter<String> subjectspinnerArray;
    static List<ListofChapterforDialog> chapterList = new ArrayList<ListofChapterforDialog>();
    ChapterListResponse chapterListResponse;
    TopicListResponse topicListResponse;
    ArrayAdapter<String> chapterspinnerArray;
    ArrayList<String> chapterlist = new ArrayList<String>();
    SelectedImageAdapter selectedImageAdapter;

    ArrayAdapter<String> topicspinnerArray;

    List<ListofTopicforDialog> topicList = new ArrayList<ListofTopicforDialog>();
    ArrayList<String> topiclist = new ArrayList<String>();
    private com.thomashaertel.widget.MultiSpinner topicmultiSpinner;
    ArrayList<Integer> selectedTopic=new ArrayList<>();

    static  String dueDate="";
    static String topicID = "";
    SidemenuDetailActivity sidemenuDetailActivity;

    static String SETUSERTYPE,USERID,SCHOOLID;
    BaseActivity baseActivity;
    private boolean loading = true;
    int limit=5;
    int offset=0;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    static  EditText edittextmarks;
    ArrayAdapter<String> spinnerexamTermAdapter;
    AcademicTermResponse examTermResponse;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    static  TextView topicnameTextview,chapternameTextview;
    static  Dialog chapterDialog,topicDialog;
    ListView topiclistView,chapterlistView;
    ExamComposetopicAdpater examComposetopicAdpater;
    ExamComposechapterAdpater examComposechapterAdpater;
    SearchView dialogSearchView,chapterdialogSearchView;
    TextView searchnameTextview,chaptersearchnameTextview,topicsearchnameTextview;
    Button dialogTopicButton,dialogChapterButton;
    List<ListofChapterforDialog> searchchapterList = new ArrayList<ListofChapterforDialog>();
    static List<SelectedImageList> imageList = new ArrayList<>();
    static  String imgname = "";
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            getExamtermList();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;





    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    static String chapterID="";
    List<ListofTopicforDialog> searchtopicList = new ArrayList<ListofTopicforDialog>();
    private LinearLayout upload_liner;
    RecyclerView select_image_recyclerview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_addhomework_assignment, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        Log.d("VIKIS","AddHomeAssignmentFragment");
        editor = sharedPreferences.edit();
        sidemenuDetailActivity=(SidemenuDetailActivity)getActivity();
        class_spinner = (Spinner)view.findViewById(R.id.class_spinner);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);
        topic_spinner = (Spinner)view. findViewById(R.id.topic_spinner);
        edittextDesc = (EditText) view.findViewById(R.id.edit_desc);
        edittextDesc.setTypeface(ProximaNovaFont.getInstance(getContext()).getSemiBoldTypeFace());
        date_picker = (LinearLayout) view.findViewById(R.id.date_picker);
        lin_date = (LinearLayout)view. findViewById(R.id.lin_date);
        addHomeWorkTabFragment=new AddHomeWorkTabFragment();
        dateTextview = (TextView) view.findViewById(R.id.dateTextview);
        upload_liner = (LinearLayout) view.findViewById(R.id.upload_linear);
        edittextmarks=(EditText)view.findViewById(R.id.edittextmarks);
        edittextmarks.setTypeface(ProximaNovaFont.getInstance(getContext()).getLightTypeFace());
        term_spinner=(Spinner)view.findViewById(R.id.term_spinner);
        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        term_spinner.setAdapter(spinnerexamTermAdapter);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);
        dateTextview.setText(formattedDate);
        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
        dueDate = currentdueDATE.format(c);
        //SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
        chapternameTextview=(TextView)view.findViewById(R.id.chapternameTextview);
        topicnameTextview=(TextView)view.findViewById(R.id.topicnameTextview);
        chapterDialog=new Dialog(getContext());
        chapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        chapterDialog.setCancelable(false);
        chapterDialog.setContentView(R.layout.chapter_custom_dialog);
        chapterdialogSearchView=(SearchView)chapterDialog.findViewById(R.id.dialogSearchView);
        chaptersearchnameTextview=(TextView)chapterDialog.findViewById(R.id.searchnameTextview);
        chapternameTextview=(TextView)view.findViewById(R.id.chapternameTextview);
        chapterlistView = (ListView) chapterDialog.findViewById(R.id.chapterlistView);
        dialogChapterButton=(Button)chapterDialog.findViewById(R.id.btn_dialog) ;

        topicDialog = new Dialog(getContext());
        topicDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        topicDialog.setCancelable(false);
        topicDialog.setContentView(R.layout.custom_dialog);
        topiclistView = (ListView) topicDialog.findViewById(R.id.listView1);
        dialogTopicButton = (Button) topicDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)topicDialog.findViewById(R.id.dialogSearchView);
        topicnameTextview=(TextView)view.findViewById(R.id.topicnameTextview);
        searchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);
        topicsearchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);
        examComposechapterAdpater= new ExamComposechapterAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList);
        examComposetopicAdpater=new ExamComposetopicAdpater(searchtopicList,getContext());

        select_image_recyclerview = view.findViewById(R.id.select_image_recycle);
        selectedImageAdapter = new SelectedImageAdapter(imageList, getActivity(), this);
        //LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),5);
        select_image_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        select_image_recyclerview.setAdapter(selectedImageAdapter);
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        chapterdialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chapterdialogSearchView.setIconified(false);
            }
        });
        chapternameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chapterList.size()>0)
                {
                    chapterDialog.show();
                }
            }
        });

        topicnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topicList.size()>0)
                {
                    topicDialog.show();
                }
            }
        });

        chapterdialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchchapterList.clear();
                    for(int y=0;y<chapterList.size();y++)
                    {
                        if (chapterList.get(y).getChapter_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchchapterList.add(chapterList.get(y));

                        }

                    }


                    if (searchchapterList.size()>0) {

                        chapterlistView.setVisibility(View.VISIBLE);
                        chaptersearchnameTextview.setVisibility(View.GONE);
                        examComposechapterAdpater= new ExamComposechapterAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList);
                        chapterlistView.setAdapter(examComposechapterAdpater);

                    }
                    else
                    {
                        chapterlistView.setVisibility(View.GONE);
                        chaptersearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    chapterlistView.setVisibility(View.VISIBLE);
                    chaptersearchnameTextview.setVisibility(View.GONE);
                    examComposechapterAdpater= new ExamComposechapterAdpater(chapterList,getContext(),chapterDialog,dialogChapterButton, chapterList);
                    chapterlistView.setAdapter(examComposechapterAdpater);

                }
                return false;
            }
        });
        dialogChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chapterName="";
                chapterID="";
                for (int y=0;y<chapterList.size();y++)
                {


                    if (chapterList.get(y).getSelectedID()!=0) {
                        chapterName = chapterName.concat(String.valueOf(chapterList.get(y).getChapter_name()).concat(","));
                        chapterID=chapterID.concat(String.valueOf(chapterList.get(y).getId())).concat(",");
                    }

                }
                if (chapterName.length()>0 && chapterID.length()>0) {
                    chapterName = chapterName.substring(0, chapterName.length() - 1);
                    chapterID = chapterID.substring(0, chapterID.length() - 1);
                    chapternameTextview.setText(chapterName);
                    chapternameTextview.setError(null);
                    topicList.clear();
                    topicID="";
                    topicnameTextview.setText("");
                    topicnameTextview.setHint(getString(R.string.selecttopic));
                    topicnameTextview.setError(null);
                    getTopicList(chapterID);
                }
                else
                {
                    chapternameTextview.setText("");
                }
                examComposechapterAdpater.notifyDataSetChanged();
                chapterDialog.hide();
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchtopicList.clear();
                    for(int y=0;y<topicList.size();y++)
                    {
                        if (topicList.get(y).getTopic_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchtopicList.add(topicList.get(y));

                        }

                    }


                    if (searchtopicList.size()>0) {

                        topiclistView.setVisibility(View.VISIBLE);
                        topicsearchnameTextview.setVisibility(View.GONE);
                        examComposetopicAdpater=new ExamComposetopicAdpater(searchtopicList,getContext());
                        topiclistView.setAdapter(examComposetopicAdpater);
                    }
                    else
                    {
                        topiclistView.setVisibility(View.GONE);
                        topicsearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    topiclistView.setVisibility(View.VISIBLE);
                    topicsearchnameTextview.setVisibility(View.GONE);
                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                    topiclistView.setAdapter(examComposetopicAdpater);

                }
                return false;
            }
        });
        dialogTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String topicName="";
                topicID="";
                for (int y=0;y<topicList.size();y++)
                {

                    if (topicList.get(y).getSelectedID()!=0) {
                        topicName = topicName.concat(String.valueOf(topicList.get(y).getTopic_name()).concat(","));
                        topicID=topicID.concat(String.valueOf(topicList.get(y).getId())).concat(",");
                    }

                }
                if (topicName.length()>0 && topicID.length()>0) {
                    topicName = topicName.substring(0, topicName.length() - 1);
                    topicID = topicID.substring(0, topicID.length() - 1);
                    Log.d("BBAA", "ID" + topicID + "NAME" + topicName);
                    topicnameTextview.setText(topicName);
                    topicnameTextview.setError(null);
                }
                else
                {
                    topicnameTextview.setText("");
                }
                topicDialog.hide();
            }
        });


        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);
        //getClassList();
        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);
        topicmultiSpinner=(com.thomashaertel.widget.MultiSpinner)view.findViewById(R.id.topic_multispinner);
        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");



                                    Date c = Calendar.getInstance().getTime();


                                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                                    String formattedDate = df.format(c);

                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                                    Date date1 = null;
                                    try {
                                        date1 = sdf.parse(formt.format(dateObj));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = sdf.parse(formattedDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");
                                        dateTextview.setText(formt.format(dateObj));
                                        dateTextview.setError(null);
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        dueDate = currentdueDATE.format(dateObj);
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
                                        dateTextview.setError(getString(R.string.validDate));
                                        dueDate="";

                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                        dateTextview.setText(formt.format(dateObj));
                                        dateTextview.setError(null);
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        dueDate = currentdueDATE.format(dateObj);
                                    } else {
                                        System.out.println("How to get here?");
                                        dateTextview.setError(getString(R.string.validDate));
                                        dueDate="";

                                    }
                                }

                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();



                    String dateStr = "04/05/2010";
                    SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                    Date dateObj = null;
                    try {
                        dateObj = curFormater.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

                    String newDateStr = postFormater.format(dateObj);


                    //upload image



                }
            }
        });








        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    topicnameTextview.setError(null);
                    chapternameTextview.setError(null);
                    classId = classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionId=classListResponse.getTeachersClassesList().get(position - 1).getSectionID();
                    subjectspinnerArray.clear();
                    subjectspinnerArray.notifyDataSetChanged();
                    subject_Id = 0;
                    subjectspinnerArray.clear();
                    subjectspinnerArray.notifyDataSetChanged();
                    chapternameTextview.setText("");
                    topicnameTextview.setText("");
                    chapterList.clear();
                    examComposechapterAdpater.notifyDataSetChanged();
                    topicList.clear();
                    examComposetopicAdpater.notifyDataSetChanged();
                    chapternameTextview.setHint(getString(R.string.selectchapter));
                    topicnameTextview.setHint(getString(R.string.selecttopic));
                    topicID = "";
                    chapterID="";
                    getSubjecyList();
                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        subjectlist.add("Select Subject");
        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subjectspinnerArray);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    subject_Id = 0;
                } else {
                    topicnameTextview.setError(null);
                    chapternameTextview.setError(null);
                    chapterID="";
                    topicID="";
                    topicnameTextview.setText("");
                    chapternameTextview.setText("");
                    subject_Id=subjectListResponse.getTeachersSubjectsList().get(position-1).getSubject_id();
                    //classId = subjectListResponse.getTeachersSubjectsList().get(position - 1).getClassID();

                    chapterList.clear();
                    examComposechapterAdpater.notifyDataSetChanged();
                    topicList.clear();
                    examComposetopicAdpater.notifyDataSetChanged();
                    getExamchapterList();




                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);

        chapterspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, chapterlist);

        chapterspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        chap_spinner.setAdapter(chapterspinnerArray);


        chap_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    topicID="";
                    topicnameTextview.setText("");
                    //classId = subjectListResponse.getTeachersSubjectsList().get(position - 1).getClassID();
                    chapter_Id=chapterListResponse.getChaptersList().get(position-1).getId();
                    getTopicList(String.valueOf(chapter_Id));



                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        topicspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, topiclist);

        //topicspinnerArray.setDropDownViewResource(R.layout.spinner_item);






        term_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    term_Id=0;
                } else {
                    term_Id = examTermResponse.getAcademicTermsList().get(position - 1).getId();
                    topicnameTextview.setError(null);
                    chapternameTextview.setError(null);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        upload_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (imageList.size() > 4) {
                    Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });
        return view;
    }
//    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {
        for (int i=0;i<path.size();i++)
        {
            imageList.add(new SelectedImageList(path.get(i),true));
        }
        if (imageList.size() > 4) {
            Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }


    }

    public void compareDate(String dateone,String datetwo) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date date1 = sdf.parse(dateone);
        Date date2 = sdf.parse(datetwo);
        if (date1.compareTo(date2) > 0) {
            System.out.println("Date1 is after Date2");
        } else if (date1.compareTo(date2) < 0) {
            System.out.println("Date1 is before Date2");
        } else if (date1.compareTo(date2) == 0) {
            System.out.println("Date1 is equal to Date2");
        } else {
            System.out.println("How to get here?");
        }
    }
    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            examTermSpinner.clear();
            examTermSpinner.add("Select Term");
            SCHOOLID= String.valueOf((sharedPreferences.getInt(Constants.SCHOOLID,0)));
            USERID= sharedPreferences.getString(Constants.USERID,"");
            SETUSERTYPE= String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0));
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getAcademicTerms(getExamTermParam).enqueue(new Callback<AcademicTermResponse>() {
                @Override
                public void onResponse(Call<AcademicTermResponse> call, Response<AcademicTermResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getAcademicTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getAcademicTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getAcademicTermsList().get(i).getName());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();

                                    getClassList();
                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AcademicTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public void  isValidate(final SidemenuDetailActivity sidemenuDetailActivity)
    {

        if (term_Id!=0) {
            if (classId != 0) {
                if (subject_Id != 0) {
                    if (chapterID.length() != 0) {
                        if (topicID.length() != 0) {
                            if (dueDate.length() != 0) {
                                if (edittextmarks.getText().toString().length() > 0) {

                                    ((SidemenuDetailActivity) sidemenuDetailActivity).uploadFile(imageList, sidemenuDetailActivity.getString(R.string.s3_assignment_path),
                                            imgname, new OnAmazonFileuploaded() {
                                                @Override
                                                public void FileStatus(int status, String filename) {
                                                    if (status == 1) {
                                                        //addClasswork();
                                                        //Toast.makeText(sidemenuDetailActivity,sidemenuDetailActivity.getString(R.string.school_name), Toast.LENGTH_SHORT).show();
                                                        addAssignment(sidemenuDetailActivity);

                                                    } else {
                                                        //Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                        Toast.makeText(sidemenuDetailActivity,sidemenuDetailActivity.getString(R.string.uploadfail), Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            });

                                } else {
                                    //Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.validMarks), Toast.LENGTH_SHORT).show();
                                    edittextmarks.setError( sidemenuDetailActivity.getString(R.string.validMarks));
                                }

                            } else {
                                Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.validDate), Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            //Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.selecttopic), Toast.LENGTH_SHORT).show();
                            topicnameTextview.setError( sidemenuDetailActivity.getString(R.string.selecttopic));
                        }
                    } else {
                        //Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.selectchapter), Toast.LENGTH_SHORT).show();
                        chapternameTextview.setError( sidemenuDetailActivity.getString(R.string.selectchapter));
                    }
                } else {
                    Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.selectsubject), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.selectclass), Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.term), Toast.LENGTH_SHORT).show();
        }
    }


    private void addAssignment(final SidemenuDetailActivity sidemenuDetailActivity) {
        Handler mHandler = new Handler();
        ProgressDialog mDialog;
        if (Util.isNetworkAvailable()) {

            mDialog = new ProgressDialog(sidemenuDetailActivity);
            mDialog = new ProgressDialog(sidemenuDetailActivity);
            mDialog.getWindow().setBackgroundDrawable(new
                    ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
            mDialog.show();
            mDialog.setContentView(R.layout.custom_progress_view);
            AddAssignmentParams addAssignmentParams=new AddAssignmentParams();
            addAssignmentParams.setSchoolID(String.valueOf(SCHOOLID));
            addAssignmentParams.setUserID(String.valueOf(USERID));
            addAssignmentParams.setUserType(String.valueOf(SETUSERTYPE));
            addAssignmentParams.setClassID(String.valueOf(classId));
            addAssignmentParams.setSubjectID(String.valueOf(subject_Id));
            addAssignmentParams.setChapterID(chapterID);
            addAssignmentParams.setTopicID(topicID);
            addAssignmentParams.setDocument(getImagelist());
            if (edittextDesc.getText().toString().length()<=0)
            {
                addAssignmentParams.setAssignmentDesc("No description");
            }
            else {
                addAssignmentParams.setAssignmentDesc(edittextDesc.getText().toString().trim());
            }
            addAssignmentParams.setSectionID(String.valueOf(sectionId));
            addAssignmentParams.setDueDate(dueDate);
            addAssignmentParams.setAssignmentMark(edittextmarks.getText().toString().trim());
            addAssignmentParams.setTermID(String.valueOf(term_Id));
            addAssignmentParams.setTitle("assignment");
            final ProgressDialog finalMDialog = mDialog;
            sidemenuDetailActivity.vidyauraAPI.addAssignment(addAssignmentParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    finalMDialog.dismiss();
                    topicDialog.dismiss();
                    chapterDialog.dismiss();
                    if (response.body() != null) {

                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(String.valueOf(USERID));
                                sendNotification.setSchoolID(String.valueOf(SCHOOLID));
                                sendNotification.setUserType(String.valueOf(SETUSERTYPE));
                                sendNotification.setType("assignments");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                sidemenuDetailActivity.vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            if (getActivity() != null) {
                                                Toast.makeText(getContext(), "Assignment Added Successfully", Toast.LENGTH_SHORT).show();
                                            }
                                            sidemenuDetailActivity.finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } else {
                                Toast.makeText(sidemenuDetailActivity, addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    finalMDialog.dismiss();
                    Toast.makeText(sidemenuDetailActivity, sidemenuDetailActivity.getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(classListResponse.getTeachersClassesList().get(i).getClassName()+"-"+
                                            classListResponse.getTeachersClassesList().get(i).getSection());

                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();

                                classspinnerArray.notifyDataSetChanged();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            subjectlist.clear();
            subjectlist.add("Select Subject");
            GetSubjectListParam getSubjectListParam = new GetSubjectListParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            vidyauraAPI.getTeachersSubjects(getSubjectListParam).enqueue(new Callback<SubjectListResponse>() {
                @Override
                public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        subjectListResponse = response.body();
                        if (subjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (subjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {

                                    subjectlist.add(subjectListResponse.getTeachersSubjectsList().get(i).getName());

                                }
//                                subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                //chapter_Id=examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();
                                subjectspinnerArray.notifyDataSetChanged();
//                                getExamchapterList();




                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getExamchapterList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            chapterList.clear();
            chapterspinnerArray.clear();
            chapterspinnerArray.add("Select chapter");
            GetChapterListParam getChapterListParam = new GetChapterListParam();
            getChapterListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getChapterListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getChapterListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getChapterListParam.setClassID(String.valueOf(classId));
            getChapterListParam.setSubjectID(String.valueOf(subject_Id));
            vidyauraAPI.getExamChaptersList(getChapterListParam).enqueue(new Callback<ChapterListResponse>() {
                @Override
                public void onResponse(Call<ChapterListResponse> call, Response<ChapterListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        chapterListResponse = response.body();
                        if (chapterListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (chapterListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (chapterListResponse.getChaptersList().size() > 0) {
                                    for (int i = 0; i < chapterListResponse.getChaptersList().size(); i++) {
                                        chapterList.add(new ListofChapterforDialog(chapterListResponse.getChaptersList().get(i).getId()
                                                ,chapterListResponse.getChaptersList().get(i).getChapter_name(),0));
                                    }
                                    examComposechapterAdpater= new ExamComposechapterAdpater(chapterList,getContext(), chapterDialog, dialogChapterButton, chapterList);
                                    chapterlistView.setAdapter(examComposechapterAdpater);


                                    //getTopicList(String.valueOf(examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id()));

                                } else {
                                    Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ChapterListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getTopicList(String chapterID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            topicspinnerArray.clear();
            topicList.clear();
            GetTopicListParam getTopicListParam = new GetTopicListParam();
            getTopicListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getTopicListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getTopicListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getTopicListParam.setClassID(String.valueOf(classId));
            getTopicListParam.setSubjectID(String.valueOf(subject_Id));
            getTopicListParam.setChapterID(chapterID);
            vidyauraAPI.getTopicsList(getTopicListParam).enqueue(new Callback<TopicListResponse>() {
                @Override
                public void onResponse(Call<TopicListResponse> call, Response<TopicListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        topicListResponse = response.body();
                        if (topicListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (topicListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (topicListResponse.getTopicsList().size() > 0) {
                                    for (int i = 0; i < topicListResponse.getTopicsList().size(); i++) {

                                        topicList.add(new ListofTopicforDialog(topicListResponse.getTopicsList().get(i).getId()
                                                ,topicListResponse.getTopicsList().get(i).getTopic_name(),0));
                                    }
                                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                                    topiclistView.setAdapter(examComposetopicAdpater);
                                    topicspinnerArray.notifyDataSetChanged();



                                } else {
                                    Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TopicListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onimageremoved(int pos, View view) {
        if (view.getId() == R.id.close) {
            imageList.remove(pos);
            selectedImageAdapter.notifyItemRemoved(pos);
        }
    }

    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("attachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("extension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }
    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
}

