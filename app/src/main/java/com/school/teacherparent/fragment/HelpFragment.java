package com.school.teacherparent.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

/**
 * Created by harini on 9/25/2018.
 */

public class HelpFragment extends BaseFragment {


    LinearLayout faqLinearlayout,feedbackLinearlayout,privacypolicyLinearlayout,aboutLinearlayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.help));
        faqLinearlayout=(LinearLayout)view.findViewById(R.id.faqLinearlayout);
        feedbackLinearlayout=(LinearLayout)view.findViewById(R.id.feedbackLinearlayout);
        privacypolicyLinearlayout=(LinearLayout)view.findViewById(R.id.privacypolicyLinearlayout);
        aboutLinearlayout=(LinearLayout)view.findViewById(R.id.aboutLinearlayout);

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        faqLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "faq"));
            }
        });
        feedbackLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feedback"));
            }
        });
        privacypolicyLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "privacy"));
            }
        });
        aboutLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "privacy"));
            }
        });
        return view;
    }
}
