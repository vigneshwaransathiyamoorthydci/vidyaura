package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.ParentCircularActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.FeesConditionAdapter;
import com.school.teacherparent.adapter.FeesListAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.FeesListParms;
import com.school.teacherparent.models.FeesListResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeesFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;

    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialog;
    TextView tvFeesdue, tvDueAmount;
    ListView lvCondition;
    ArrayList<String> feesConditionArrayList = new ArrayList<>();
    RecyclerView rvFeeslist;
    FeesListAdapter feesListAdapter;
    FeesListResponse feesListParms;
    ArrayList<FeesListParms> feesListParmsArrayList = new ArrayList<>();

    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID;
    int count = 0;
    int dueAmount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fees, container, false);
        intiVIews(view);
        performAction();
        return view;
    }

    private void intiVIews(View view) {
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.fees));
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        tvFeesdue = view.findViewById(R.id.tv_feesdue);
        tvDueAmount = view.findViewById(R.id.tv_due_amount);
        lvCondition = view.findViewById(R.id.lv_condition);
        lvCondition.setVisibility(View.VISIBLE);
        rvFeeslist = view.findViewById(R.id.rv_feeslist);
        rvFeeslist.setHasFixedSize(true);
        rvFeeslist.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        //feesListAdapter = new FeesListAdapter(getContext(), feesListParmsArrayList);
        rvFeeslist.setAdapter(feesListAdapter);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        getStudentList();
        getConditions();
    }

    private void performAction() {

    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(), parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size() > 2) {
            dsvStudentList.scrollToPosition(1);
        } else {
            dsvStudentList.scrollToPosition(0);
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
                                sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
                                studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
                                getFeesDetails(studentID);
                            } else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getConditions() {
        /*feesConditionArrayList.add("Include Transport Charges");
        feesConditionArrayList.add("Include Exam Fees");
        feesConditionArrayList.add("Include Lab Charges");

        FeesConditionAdapter feesConditionAdapter = new FeesConditionAdapter(getContext(), feesConditionArrayList);
        lvCondition.setAdapter(feesConditionAdapter);*/
    }

    private void getFeesDetails(int selectedStudentID) {

        /*feesListParmsArrayList.clear();
        if (selectedStudentID == 48) {
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 3", "Rs.3850/-", "05/04/2019", "", false, ""));
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 2", "Rs.3850/-", "10/11/2018", "05/11/2018", true, "TXN ID: CHE96AVR25"));
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 1", "Rs.7300/-", "28/08/2018", "02/09/2018", true, "TXN ID: CHE38AVR79"));
        } else {
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 3", "Rs.3850/-", "05/04/2019", "05/11/2018", true, ""));
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 2", "Rs.3850/-", "10/11/2018", "", false, "TXN ID: CHE96AVR25"));
            feesListParmsArrayList.add(new FeesListParms("TERM FEES 1", "Rs.7300/-", "28/08/2018", "02/09/2018", true, "TXN ID: CHE38AVR79"));
        }
        feesListAdapter = new FeesListAdapter(getContext(), feesListParmsArrayList);
        rvFeeslist.setAdapter(feesListAdapter);*/

        if (Util.isNetworkAvailable()) {
            /*showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getStudentListParam.setStudID(String.valueOf(selectedStudentID));

            vidyauraAPI.getStudentFees(getStudentListParam).enqueue(new Callback<FeesListResponse>() {
                @Override
                public void onResponse(Call<FeesListResponse> call, Response<FeesListResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        feesListParms = response.body();
                        feesListParmsArrayList.clear();
                        dueAmount = 0;
                        for (int i=0; i<feesListParms.getFeesList().size();i++) {
                            if (feesListParms.getFeesList().get(i).getFeesPaidDetails().size()>0) {
                                feesListParmsArrayList.add(new FeesListParms(feesListParms.getFeesList().get(i).getTermName(),
                                        String.valueOf(feesListParms.getFeesList().get(i).getAmountToBePaid()),
                                        String.valueOf(feesListParms.getFeesList().get(i).getDue_date()),
                                        feesListParms.getFeesList().get(i).getFeesPaidDetails().get(0).getPaid_date(),
                                        true,
                                        feesListParms.getFeesList().get(i).getFeesPaidDetails().get(0).getTransaction_id()));
                            } else {
                                feesListParmsArrayList.add(new FeesListParms(feesListParms.getFeesList().get(i).getTermName(),
                                        String.valueOf(feesListParms.getFeesList().get(i).getAmountToBePaid()),
                                        String.valueOf(feesListParms.getFeesList().get(i).getDue_date()),
                                        "",
                                        false,
                                        ""));
                                dueAmount += feesListParms.getFeesList().get(i).getAmountToBePaid();
                            }
                        }

                        if (feesListParmsArrayList.size() > 0) {
                            if (dueAmount>0) {
                                tvDueAmount.setVisibility(View.VISIBLE);
                                tvFeesdue.setText("Fees Due");
                                tvDueAmount.setText("₹ " + dueAmount);
                            } else {
                                tvFeesdue.setText("No Fees Due");
                                tvDueAmount.setVisibility(View.GONE);
                            }
                            feesListAdapter.notifyDataSetChanged();
                        }
                    } else {
                        getFeesDetails(studentID);
                    }
                }

                @Override
                public void onFailure(Call<FeesListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });*/

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID = parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID = parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID = parentsStudListArrayList.get(adapterPosition).getStudID();
            getFeesDetails(studentID);
        }
    }
}
