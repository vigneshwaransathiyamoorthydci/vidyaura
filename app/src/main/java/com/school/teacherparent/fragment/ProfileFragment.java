package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddProfileParams;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.UpdateProfileResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.FontTextViewRegular;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment  {


    private ViewPager viewPager;
    private TabLayout tabLayout;
    TextView user_name,birth_date,empid_textview,calendar_date,user_time,points,points_textview;

    public ProfileFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    CircularImageView userImageview;
    String imgname = "";
    List<SelectedImageList> imageList = new ArrayList<>();
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    ImageView imageProfileEdit;
    FontTextViewRegular tvEditProfile;
    Menu menu;
    NotificationReceiver notificationReceiver;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        setHasOptionsMenu(true);
        TestDrawerActivity.toolbar.setTitle(getResources().getString(R.string.profile));
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        user_name=view.findViewById(R.id.user_name);
        birth_date=view.findViewById(R.id.birth_date);
        empid_textview=view.findViewById(R.id.empid_textview);
        calendar_date=view.findViewById(R.id.calendar_date);
        user_time=view.findViewById(R.id.user_time);
        points=view.findViewById(R.id.points);
        points_textview=view.findViewById(R.id.points_textview);
        userImageview=view.findViewById(R.id.userImageview);
        user_name.setText(sharedPreferences.getString(Constants.FNAME,"")+" "+sharedPreferences.getString(Constants.LNAME,""));
        birth_date.setText(getDateFormatbyMonth(sharedPreferences.getString(Constants.DOB,"")));
        empid_textview.setText(sharedPreferences.getString(Constants.EMP_ID,""));
        points.setText(sharedPreferences.getString(Constants.POINTS_NAME,""));
        points_textview.setText(""+sharedPreferences.getInt(Constants.TOTAL_LEVEL_POINTS,0));
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c);
        calendar_date.setText(getDateFormatbyMonthforProfile(sharedPreferences.getString(Constants.JOINING_DATE,"")));
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));
        imageProfileEdit = view.findViewById(R.id.image_profile_edit);
        tvEditProfile = view.findViewById(R.id.tv_editProfile);

        tabLayout.setTabTextColors(
                getResources().getColor(R.color.text_color_light),
                getResources().getColor(R.color.black)
        );
        if (sharedPreferences.getString(Constants.PROFILE_PHOTO,"")!=null) {


            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" +
                    sharedPreferences.getString(Constants.PROFILE_PHOTO, "");


            //picassoImageHolder(holder.feed_image,context.getString(R.string.s3_feeds_path),"/"+fData.getAttachmentsList().get(0));
            Picasso.get().load(url).


                    into(userImageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {


                        }

                        @Override
                        public void onError(Exception e) {
                            userImageview.setImageDrawable(getActivity().getDrawable(R.drawable.ic_user));

                        }
                    });
        }
        else
        {
            userImageview.setImageDrawable(getActivity().getDrawable(R.drawable.ic_user));
        }
        userImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });
        imageProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        MenuItem item = menu.findItem(R.id.action_calender);
        item.setVisible(false);
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MyclassesFragment(), getString(R.string.myclasses));

        if (sharedPreferences.getInt(Constants.HASCHILDREN,0)==1)
        {
            adapter.addFragment(new ParentsOfFragment(), getString(R.string.parentof));
        }

        viewPager.setAdapter(adapter);
    }



    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {


        Bitmap myBitmap = BitmapFactory.decodeFile(path.get(0));
        userImageview.setBackgroundResource(Integer.parseInt(path.get(0)));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (data!=null) {
            final List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            final File imgFile = new File(mPaths.get(0));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ImageView imageView = (ImageView) getView().findViewById(R.id.userImageview);
                imageView.setImageBitmap(myBitmap);
                imageList = new ArrayList<>();
                imageList.add(new SelectedImageList(mPaths.get(0), true));
                ((TestDrawerActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_employee),
                        imgname, new OnAmazonFileuploaded() {
                            @Override
                            public void FileStatus(int status, String filename) {
                                if (status == 1) {
                                    updateProfile(imgFile);
                                } else {
                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
            }
        }

    }
    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("profileAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("profileAttachmentExtension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }
    private void updateProfile(final File imgFile) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddProfileParams addProfileParams = new AddProfileParams();
            addProfileParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            addProfileParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            addProfileParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            addProfileParams.setTeacherAvatar(getImagelist());
            Gson gson=new Gson();
            String input=gson.toJson(addProfileParams);
            Log.d("addParms", "addFeed: "+input);

            vidyauraAPI.changeTeacherAvatar(addProfileParams).enqueue(new Callback<UpdateProfileResponse>() {
                @Override
                public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        UpdateProfileResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.PROFILE_PHOTO, addFeedResponse.getUserDetails().get(0).getEmp_photo());
                                editor.commit();
                                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                                TestDrawerActivity.userImageview.setImageBitmap(myBitmap);

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((sharedPreferences.getString(Constants.FNAME, "")+" "+
                                        sharedPreferences.getString(Constants.LNAME, "")).replace("null","")
                                        ,url+addFeedResponse.getUserDetails().get(0).getEmp_photo(),
                                        sharedPreferences.getString(Constants.USERID, ""),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(sharedPreferences.getString(Constants.USERID, "")).setValue(messageList1);

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, addFeedResponse.getToken()).commit();
                            updateProfile(imgFile);
                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

}
