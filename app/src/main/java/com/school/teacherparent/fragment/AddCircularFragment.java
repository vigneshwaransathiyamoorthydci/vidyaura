package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinner;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.androidbuts.multispinnerfilter.SpinnerListener;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.CircularComposeclassAdpater;
import com.school.teacherparent.adapter.ClassListAdapter;
import com.school.teacherparent.adapter.MultiSpinnerCustomspinnet;
import com.school.teacherparent.adapter.SelectedImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AddcircularParam;
import com.school.teacherparent.models.CircularDetailsParams;
import com.school.teacherparent.models.CircularDetailsResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.FeedDetailsByIDResponse;
import com.school.teacherparent.models.FeedDetailsParams;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.models.UpdatecircularParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class AddCircularFragment extends BaseFragment implements OnImageRemoved {
    BaseActivity baseActivity;
    private EditText edit_title, edit_desc;
    private Spinner spinner;
    MultiSpinner spinner_class;

    private ImageView img_upload;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY = 1;
    ImageView back;
    TextView add_circular;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private CheckBox allclasscheck;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public AddCircularFragment() {
        // Required empty public constructor
    }
    ClassListResponse classListResponse;
    boolean valid = false;
    String sendTo="teachers";
    List<ListofClass> classList = new ArrayList<ListofClass>();
    List<ListofClass> searchclassList = new ArrayList<ListofClass>();

    String classID="";
    ArrayList<Integer> selectedClass=new ArrayList<>();
    int circularID;

    ListView classlistView;
    CircularComposeclassAdpater circularComposeclassAdpater;
    Button dialogDistrictButton;
    boolean[] selectedtopicItemsupdate;
    SearchView dialogSearchView;
    List<String> categories = new ArrayList<String>();
    List<String> ClassNameandsectionName = new ArrayList<String>();
    TextView classnameTextview,searchnameTextview;
    Dialog classDialog;
     List<SelectedImageList> imageList = new ArrayList<>();
    SelectedImageAdapter selectedImageAdapter;
    RecyclerView select_image_recyclerview;
    private LinearLayout upload_liner;
    String imgname = "";
    LinearLayout classselectionLinearlayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_circular, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        edit_title=view.findViewById(R.id.edit_title);
        edit_desc=view.findViewById(R.id.edit_desc);
        allclasscheck = view.findViewById(R.id.allclasscheck);
        img_upload = view.findViewById(R.id.img_upload);
        add_circular = view.findViewById(R.id.add_circular);
        classnameTextview=view.findViewById(R.id.classnameTextview);
        baseActivity = (BaseActivity) getActivity();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_new_circular));
        classselectionLinearlayout=view.findViewById(R.id.classselectionLinearlayout);
        getClassList();
        select_image_recyclerview = view.findViewById(R.id.select_image_recycle);
        selectedImageAdapter = new SelectedImageAdapter(imageList, getActivity(), this);
        //LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),5);
        select_image_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        select_image_recyclerview.setAdapter(selectedImageAdapter);
        upload_liner = (LinearLayout) view.findViewById(R.id.upload_linear);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            circularID = bundle.getInt("circuId", 0);




        }

        spinner_class = (MultiSpinner) view.findViewById(R.id.spinner_class);
        //classListAdapter=new ClassListAdapter(classList,getContext());

        classDialog = new Dialog(getContext());
        classDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        classDialog.setCancelable(false);
        classDialog.setContentView(R.layout.custom_dialog);
        searchnameTextview=(TextView)classDialog.findViewById(R.id.searchnameTextview);
        classlistView = (ListView) classDialog.findViewById(R.id.listView1);
        dialogDistrictButton = (Button) classDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)classDialog.findViewById(R.id.dialogSearchView);
        dialogSearchView.setQueryHint(getString(R.string.enter_class));
        dialogSearchView.setIconifiedByDefault(false);
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });


        upload_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (imageList.size() > 4) {
                    Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });

        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1)
                {
//                    Collections.sort(classList, new Comparator<ListofClass>() {
//
//                        @Override
//                        public int compare(ListofClass lhs, ListofClass rhs) {
//                            //here getTitle() method return app name...
//                            return newText.compareTo(lhs.getClassname());
//
//                        }
//
//
//                    });
                    searchclassList.clear();
                    for(int y=0;y<classList.size();y++)
                    {
                        if (classList.get(y).getClassname().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchclassList.add(classList.get(y));
                            Log.d("SEARCH","classLIST"+classList.get(y).getClassname());
                        }
                    }
                    if (searchclassList.size()>0) {
                        circularComposeclassAdpater = new CircularComposeclassAdpater(searchclassList, getContext());
                        classlistView.setAdapter(circularComposeclassAdpater);
                        circularComposeclassAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        classlistView.setVisibility(View.GONE);
                        searchnameTextview.setVisibility(View.VISIBLE);
                    }

                }
                else if (newText.toString().length()==0)
                {
                    classlistView.setVisibility(View.VISIBLE);
                    searchnameTextview.setVisibility(View.GONE);
                    circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                    classlistView.setAdapter(circularComposeclassAdpater);
                    circularComposeclassAdpater.notifyDataSetChanged();
                }
                return false;
            }
        });


        if (circularID!=0)
        {
            add_circular.setText(getString(R.string.update));
            SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_circular));
        }

        dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String topicName="";
                classID="";
                for (int y=0;y<classList.size();y++)
                {

                    if (classList.get(y).getSelectedID()!=0) {
                        topicName = topicName.concat(String.valueOf(classList.get(y).getClassname()+classList.get(y).getClasssection()).concat(","));
                        classID=classID.concat(String.valueOf(classList.get(y).getClasssectionid())).concat(",");
                    }

                }
                if (topicName.length()>0 && classID.length()>0) {
                    topicName = topicName.substring(0, topicName.length() - 1);
                    classID = classID.substring(0, classID.length() - 1);
                    classnameTextview.setText(topicName);
                    allclasscheck.setChecked(false);
                }
                else
                {
                    classnameTextview.setText("");
                }
                classDialog.hide();
            }
        });
        classnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classDialog.show();
            }
        });

        add_circular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().finish();
                if (circularID==0)
                {
                    if (validate()) {
                        if (spinner.getSelectedItemPosition()!=4) {
                            if (allclasscheck.isChecked()) {
                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    addCircular();
                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                            } else if (classID.length() > 0) {
                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    addCircular();
                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });
                            } else {
                                Toast.makeText(getActivity(), "Select the Class", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {

                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    addCircular();
                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                        }
                    }
                }
                else {
                    if (validate()) {
//                        if (allclasscheck.isChecked()) {
//                            ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
//                                    imgname, new OnAmazonFileuploaded() {
//                                        @Override
//                                        public void FileStatus(int status, String filename) {
//                                            if (status == 1) {
//                                                updateCircular();
//                                            } else {
//                                                Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
//                                            }
//
//                                        }
//                                    });
//                        } else if (classID.length() > 0) {
//                            ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
//                                    imgname, new OnAmazonFileuploaded() {
//                                        @Override
//                                        public void FileStatus(int status, String filename) {
//                                            if (status == 1) {
//                                                updateCircular();
//                                            } else {
//                                                Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
//                                            }
//
//                                        }
//                                    });
//                        } else {
//                            Toast.makeText(getActivity(), "Select the Class", Toast.LENGTH_SHORT).show();
//                        }
                        if (spinner.getSelectedItemPosition()!=4) {
                            if (allclasscheck.isChecked()) {
                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    updateCircular();
                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                            } else if (classID.length() > 0) {
                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    updateCircular();
                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });
                            } else {
                                Toast.makeText(getActivity(), "Select the Class", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {

                            ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_circular_path),
                                    imgname, new OnAmazonFileuploaded() {
                                        @Override
                                        public void FileStatus(int status, String filename) {
                                            if (status == 1) {
                                                updateCircular();
                                            } else {
                                                Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    });

                        }


                    }
                }

            }
        });


        allclasscheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    classID="";
                    classnameTextview.setText("");

                    for (int i=0;i<classList.size();i++)
                    {
                        classList.get(i).setSelectedID(0);
                        classID=classID.concat(String.valueOf(classList.get(i).getClasssectionid())).concat(",");
                    }
                    circularComposeclassAdpater.notifyDataSetChanged();
                }

            }
        });
        //spinner class for send circular

        categories.add("Teachers");
        categories.add("Parents");
        categories.add("Teachers and Staffs");
        categories.add("Parents and Teachers");
        categories.add("All");
        spinner = view.findViewById(R.id.spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, categories);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position==0)
                {
                    classselectionLinearlayout.setVisibility(View.VISIBLE);
                    sendTo="teacher";
                }
                else if (position==1)
                {
                    classselectionLinearlayout.setVisibility(View.VISIBLE);
                    sendTo="parent";
                }
                else if (position==2)
                {
                    classselectionLinearlayout.setVisibility(View.VISIBLE);
                    sendTo="teacher and staff";
                }
                else if (position==3)
                {
                    sendTo="parent and teacher";
                    classselectionLinearlayout.setVisibility(View.VISIBLE);
                }
                else if (position==4)
                {
                    sendTo="all";
                    classID="";
                    searchnameTextview.setText("");
                    classselectionLinearlayout.setVisibility(View.GONE);
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });





        return view;
    }


    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public boolean validate() {
        String feeddescription = edit_desc.getText().toString().trim();
        String title = edit_title.getText().toString().trim();

        if (title.length() == 0 || title.length() <= 0) {
            edit_title.setError(getString(R.string.Enterfeedtitle));
            valid = false;

        }
        else if (feeddescription.length() == 0 || feeddescription.length() <=0) {
            edit_desc.setError(getString(R.string.Enterdescription));
            valid = false;
        }
        else {
            edit_desc.setError(null);
            edit_title.setError(null);
            valid = true;
        }
        return valid;
    }
    private void getCircularDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            CircularDetailsParams circularDetailsParams=new CircularDetailsParams();
            circularDetailsParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            circularDetailsParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            circularDetailsParams.setCircularsID(String.valueOf(circularID));
            circularDetailsParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getCircularsDetailsById(circularDetailsParams).enqueue(new Callback<CircularDetailsResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onResponse(Call<CircularDetailsResponse> call, Response<CircularDetailsResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        CircularDetailsResponse circularDetailsIDResponse=response.body();
                        if (circularDetailsIDResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (circularDetailsIDResponse.getStatus() == Util.STATUS_SUCCESS) {
                                //Toast.makeText(getContext(), circularDetailsIDResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                edit_title.setText(circularDetailsIDResponse.getCircularsResult().get(0).getTitle());
                                edit_desc.setText(circularDetailsIDResponse.getCircularsResult().get(0).getDescription());
                                if (circularDetailsIDResponse.getCircularsResult().get(0).getAttachmentsList()!=null&&
                                        circularDetailsIDResponse.getCircularsResult().get(0).getAttachmentsList().size()!=0){
                                    for (String data:circularDetailsIDResponse.getCircularsResult().get(0).getAttachmentsList()){
                                        //imageList.add(getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+data);

                                        imageList.add(new SelectedImageList(getString(R.string.s3_baseurl)+getString(R.string.s3_circular_path)+"/"+data,false));

                                    }



                                }
                                selectedImageAdapter.notifyDataSetChanged();
                                if (circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("all"))
                                {
                                    spinner.setSelection(4);
                                }
                                else if (circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("teacher"))
                                {
                                    spinner.setSelection(0);
                                }
                                else if (circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("parent"))
                                {
                                    spinner.setSelection(1);
                                }
                                else if (circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("teacher and staff"))
                                {

                                    spinner.setSelection(2);
                                }

                                else if (circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("parent and teacher"))
                                {
                                    spinner.setSelection(3);
                                }
                                if (!circularDetailsIDResponse.getCircularsResult().get(0).getSend_to().toLowerCase().equals("all")) {


                                    if (circularDetailsIDResponse.getCircularsResult().get(0).getClassroom_id().toLowerCase().equals("all")) {
                                        allclasscheck.setChecked(true);
                                    } else {
                                        String[] chapterstrArray = circularDetailsIDResponse.getCircularsResult().get(0).getClassroom_id().split(",");
                                        String className = "";
                                        for (int i = 0; i < classList.size(); i++) {
                                            for (int u = 0; u < chapterstrArray.length; u++) {
                                                int selectedclassID = Integer.parseInt(chapterstrArray[u]);
                                                if (classList.get(i).getClasssectionid() == selectedclassID) {

                                                    className = className.concat(classList.get(i).getClassname() + classList.get(i).getClasssection()).concat(",");
                                                    classID = classID.concat(String.valueOf(classList.get(u).getClasssectionid())).concat(",");
                                                    classList.get(i).setSelectedID(classList.get(i).getClasssectionid());


                                                }
                                            }

                                        }
                                        if (className.length() > 0 && classID.length() > 0) {
                                            className = className.substring(0, className.length() - 1);
                                            classID = classID.substring(0, classID.length() - 1);
                                            classnameTextview.setText(className);
                                        }


                                    }

                                }
                            } else {
                                Toast.makeText(getContext(), circularDetailsIDResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {

                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        getActivity().finish();


                    }
                }

                @Override
                public void onFailure(Call<CircularDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean useList(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }
    private void getClassList()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();
            GetclassListParams getclassListParams=new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                         classListResponse=response.body();
                        if (classListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                               for (int i=0;i<classListResponse.getTeachersClassesList().size();i++)
                               {
                                   classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
                                           classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
                                           classListResponse.getTeachersClassesList().get(i).getSectionID(),false,0));
                                   ClassNameandsectionName.add(categories.get(i));
                               }
                                //classListAdapter.notifyDataSetChanged();
                                circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                                classlistView.setAdapter(circularComposeclassAdpater);

                                //classDialog.show();


//
                                if (circularID>0)
                                {
                                    getCircularDetails();
                                }

                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });


        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCircular() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UpdatecircularParam updatecircularParam=new UpdatecircularParam();
            updatecircularParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            updatecircularParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            updatecircularParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            updatecircularParam.setDocumentName(getImagelist());
            updatecircularParam.setTitle(edit_title.getText().toString().trim());
            updatecircularParam.setCircularDesc(edit_desc.getText().toString().trim());
            updatecircularParam.setSendTo(sendTo);
            updatecircularParam.setCircularsID(circularID);
            classDialog.dismiss();
            if (allclasscheck.isChecked())
            {
                updatecircularParam.setClassID("all");
            }
            else
            {
                updatecircularParam.setClassID(classID);

            }
            vidyauraAPI.updateCirculars(updatecircularParam).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("attachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("extension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }

    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
    private void addCircular() {
        if (Util.isNetworkAvailable()) {
                showProgress();
            AddcircularParam addcircularParam=new AddcircularParam();
            addcircularParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addcircularParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addcircularParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addcircularParam.setDocumentName(getImagelist());
            addcircularParam.setTitle(edit_title.getText().toString().trim());
            addcircularParam.setCircularDesc(edit_desc.getText().toString().trim());
            addcircularParam.setSendTo(sendTo);
            addcircularParam.setClassID(classID);
                classDialog.dismiss();
            System.out.println("classs ==> 1 "+classID);


            /*if (allclasscheck.isChecked())
            {
                addcircularParam.setClassID("all");
            }
            else
            {

//                String classID = "";
//                for (int y=0;y<selectedClass.size();y++)
//                {
//                    classID=classID.concat(String.valueOf(classList.get(selectedClass.get(y)).getClasssectionid()).concat(","));
//                }
//                Log.d("vignesh","classID"+classID);
//                classID= classID.substring(0, classID.length() - 1);
//                Log.d("vignesh","remove"+classID);
                addcircularParam.setClassID(classID);

            }*/

            vidyauraAPI.composeCircular(addcircularParam).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {

                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                                sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                                sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                                sendNotification.setType("circulars");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            getActivity().finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });


                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    //   Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    @Override
    public void onimageremoved(int pos, View view) {
        if (view.getId() == R.id.close) {
            imageList.remove(pos);
            selectedImageAdapter.notifyItemRemoved(pos);
        }

    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {
        for (int i=0;i<path.size();i++)
        {
            imageList.add(new SelectedImageList(path.get(i),true));
        }
        if (imageList.size() > 4) {
            Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }


    }
}


























