package com.school.teacherparent.fragment;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vijayaganesh on 10/17/2017.
 */

public class BaseFragment extends Fragment  {

    private static final float PICTURE_SIZE = 640;
    String blockCharacterSet = "~#^|$%&*!)(?/-+@:,.;'=_1234567890#{}[]";
    SwipeRefreshLayout swip_lay;
    RecyclerView recyclerView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }
//    @Override
//    public void onClick(View v) {
//        initFragment(v);
//
//    }
//    protected void initFragment(@NonNull View view) {
//       /* recyclerView=view.findViewById(R.id.recycle);*/
//        swip_lay=view.findViewById(R.id.swip_lay);
//        swip_lay.setOnRefreshListener(this);
//      //  progressDialog=new ProgressDialog(getActivity());
//
//    }
    public void hiderefresh() {
        if (swip_lay != null && swip_lay.isRefreshing()) {
            swip_lay.setRefreshing(false);
            swip_lay.destroyDrawingCache();
            swip_lay.clearAnimation();
        }
    }
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }


    public void hideProgress() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
        }
    }

    public String getDateFormatbyMonth(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getDateFormatbyMonthforProfile(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //to show TODAY and Yesterday for date
    public String parseOrderDate(long date) {
//        convert requried date format
        SimpleDateFormat mSdf = new SimpleDateFormat("yyyy-MM-dd");
//        getCurrentDate
        Calendar currentDate = Calendar.getInstance();
//        getDayBeforDate
        Calendar dayBeforeDate = Calendar.getInstance();
        dayBeforeDate.roll(Calendar.DATE, -1);
//        order date
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
//        setOrderDate in Calendar
        Calendar orderPlacedCalendar = Calendar.getInstance();
        Date orderPlacedDate = null;
        orderPlacedDate = calendar.getTime();
        orderPlacedCalendar.setTime(orderPlacedDate);
//        compare dates
        if (isSameDay(currentDate, orderPlacedCalendar)) {
            return "Today, " + mSdf.format(orderPlacedDate);
        } else if (isSameDay(dayBeforeDate, orderPlacedCalendar)) {
            return "Yesterday, " + mSdf.format(orderPlacedDate);
        } else {
            return "" + mSdf.format(orderPlacedDate);
        }
    }

    // to check isSameDay
    private boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }


    // to enable and disable layout view
    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    // to change bitmap to base64
    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }




    // to download using AsynTask
    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    //to restrict smileys in editext
    public InputFilter editTextRestrictSmileys() {

        InputFilter restrictSmileys = new InputFilter() {

            public CharSequence filter(CharSequence src, int start,
                                       int end, Spanned dst, int dstart, int dend) {
                if (src.equals("")) { // for backspace
                    return src;
                }
                if (src.toString().matches("[\\x00-\\x7F]+")) {
                    return src;
                }
                return "";
            }
        };
        return restrictSmileys;
    }

    //to restrict length in editext
    public InputFilter editTextRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(15);
        return setLength;
    }

    //to restrict length in Emaileditext
    public InputFilter editTextEmailRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }

    //to Check Vaild Email Address
    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    //to restrict special in Emaileditext
    public InputFilter editTextRestrictSpecialCharacters() {
        InputFilter restrictSpecialCharacter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
        return restrictSpecialCharacter;
    }

    //animation for textView
    public void startCountAnimation(int count, final TextView textView) {
        ValueAnimator animator = ValueAnimator.ofInt(0, count);
        animator.setDuration(100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    //to getAge from date
    public String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);

        String ageS = ageInt.toString();

        return ageS;
    }
    // to download using Picasso
    //    public void picasoImageLoader(ImageView imageView, String url, Context context) {
//        Picasso.with(context)
//                .load(getString(R.string.falcon_image_url) + url)
//                .placeholder(R.mipmap.img_sample_pro_pic)   // optional
////                                    .error(R.drawable.ic_error_fallback)      // optional
////                                    .resize(250, 200)                        // optional
////                                    .rotate(90)                             // optional
//                .into(imageView);
//    }
//    to download using Universal image loader
//    public void universalImageLoader(ImageView imageView, String image) {
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisk(true).resetViewBeforeLoading(true)
//                .showImageForEmptyUri(R.mipmap.dmk_fifteen)
//                .showImageOnFail(R.mipmap.dmk_fifteen)
//                .showImageOnLoading(R.mipmap.dmk_fifteen).build();
//        imageLoader.displayImage("http://memberportal.dci.in/uploads/Timelinemedia/" + image, imageView, options);
//    }

    //to compress Single Image
    public static void compressInputImage(Intent data, Context context, ImageView newIV) {
        Bitmap bitmap;
        Uri inputImageData = data.getData();
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), inputImageData);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
                newIV.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //to compress Multiple Images
    public static Bitmap compressInputImage(Uri uri, Context context) {
        Bitmap bitmap = null;
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    // to get path from URI
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
    public String getDateFormatforatten(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMMM dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
    //to change Date Format
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getDateFormatforhomework(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getyearfromdate(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
    public String getDateFormatforhomeworkupdate(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String geteventupdateDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }



    public String geteventDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String geteventDateFormatforload(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }



    public String geteventtimeFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("HH:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String geteventtimeFormatselectedtime(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("HH:mm:ss");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


}
