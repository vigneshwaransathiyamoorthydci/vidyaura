package com.school.teacherparent.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 10/23/2018.
 */

public class HomeWorkClassWiseTabFragment extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    ImageView back, noti;
    FloatingActionButton addHomework;

    public HomeWorkClassWiseTabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_homework_classwise_tab, container, false);

        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        addHomework = view.findViewById(R.id.add_homework);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        addHomework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "homework"));
            }
        });


        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));
        setupViewPager(viewPager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {

        PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        adapter.addFragment(new HomeWorkClassWiseSubFragment(), "Home");
        adapter.addFragment(new HomeWorkClassWiseSubAssignmentFragment(), "Assignment");
        viewPager.setAdapter(adapter);
    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}

