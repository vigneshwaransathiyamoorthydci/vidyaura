package com.school.teacherparent.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.ClassAdapter;
import com.school.teacherparent.adapter.PollsListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.PollsListParms;
import com.school.teacherparent.models.PollsResultListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PollsFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    RecyclerView rvPollsList;
    TextView tvNoPolls;
    PollsListAdapter pollsListAdapter;
    SwipeRefreshLayout pollswipelayout;
    ArrayList<PollsResultListResponse> pollsListParmsArrayList = new ArrayList<>();
    PollsResultListResponse pollsResultListResponse;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_polls, container, false);
        initViews(view);
        performAction();
        return view;

    }

    private void initViews(View view) {
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.polls));
        rvPollsList = view.findViewById(R.id.rv_polls_list);
        tvNoPolls = view.findViewById(R.id.tv_nopolls);
        pollswipelayout=view.findViewById(R.id.swipe_polls);

        rvPollsList.setHasFixedSize(true);
        rvPollsList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        pollsListAdapter = new PollsListAdapter(getContext(),pollsListParmsArrayList);
        //pollsListAdapter = new PollsListAdapter(getContext(),pollsListParmsArrayList);
        rvPollsList.setAdapter(pollsListAdapter);
        pollsListAdapter.notifyDataSetChanged();
        getPollsList();

        pollswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pollswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pollsListParmsArrayList.clear();
                        pollsListAdapter.notifyDataSetChanged();
                        getPollsList();
                        pollswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });
    }

    private void performAction() {

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        pollsListAdapter.setOnClickListen(new PollsListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "pollsview")
                        .putExtra("SelectedPollsCategoryID",pollsListParmsArrayList.get(position).getPollsCategoryID()));
            }
        });

    }

    private void getPollsList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            PollsListParms pollsListParms = new PollsListParms();
            pollsListParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            pollsListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            pollsListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getPollsList(pollsListParms).enqueue(new Callback<PollsResultListResponse>() {
                @Override
                public void onResponse(Call<PollsResultListResponse> call, Response<PollsResultListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        pollsResultListResponse = response.body();
                        System.out.println("polls response ==> "+gson.toJson(response.body()));
                        if (pollsResultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (pollsResultListResponse.getPolls.size() > 0) {
                                for (int i=0;i<pollsResultListResponse.getPolls.size();i++) {
                                    pollsListParmsArrayList.add(new PollsResultListResponse(pollsResultListResponse.getPolls.get(i).getCategoryName(),
                                            pollsResultListResponse.getPolls.get(i).getCategoryPercentage(),
                                            pollsResultListResponse.getPolls.get(i).getCategoryNoOfVotes(),
                                            pollsResultListResponse.getPolls.get(i).getPoll_category_id()));
                                }
                                if (pollsListParmsArrayList.size() > 0) {
                                    rvPollsList.setVisibility(View.VISIBLE);
                                    tvNoPolls.setVisibility(View.GONE);
                                    pollsListAdapter.notifyDataSetChanged();
                                } else {
                                    rvPollsList.setVisibility(View.GONE);
                                    tvNoPolls.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Toast.makeText(getContext(), pollsResultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            hideProgress();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<PollsResultListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            hideProgress();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

}
