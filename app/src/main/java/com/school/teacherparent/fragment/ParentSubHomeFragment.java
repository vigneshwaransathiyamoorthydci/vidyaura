package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.HomeworkAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.HomeworkList;
import com.school.teacherparent.models.HomeworkListParms;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.HomeWorkFragment.selected;
import static com.school.teacherparent.fragment.HomeWorkFragment.selectedSchool;
import static com.school.teacherparent.fragment.HomeWorkFragment.studentSchoolID;

public class ParentSubHomeFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    TextView selectedDateTextview,tvNoresult;
    SwipeRefreshLayout swipeHomework;
    RecyclerView rvHomework;
    ChildListResponseResponse childListResponseResponse;
    LinearLayoutManager linearLayoutManager;
    private HomeworkAdapter homeworkAdapter;
    BaseActivity baseActivity;
    HomeworkListResponse homeworkListResponse;
    ArrayList<HomeworkList> homeworkLists;
    public ArrayList<HomeworkListResponse.homeworksList> homeworkArrayList;
    public static ArrayList<HomeworkListResponse.homeworksList> homeworkArrayListadd = new ArrayList<>();
    int count = 0;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID;
    ProgressDialog mDialog;
    int limit=5;
    int offset=0;
    public static boolean isLoading = true;
    private Boolean isVisible = false;
    private Boolean isStarted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_sub_home, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {

        selectedDateTextview = view.findViewById(R.id.selectedDateTextview);
        swipeHomework = view.findViewById(R.id.swipe_homework);
        rvHomework = view.findViewById(R.id.rv_homework);
        tvNoresult = view.findViewById(R.id.tv_noresult);

        homeworkArrayList=new ArrayList<HomeworkListResponse.homeworksList>();
        homeworkArrayListadd=new ArrayList<>();
        homeworkLists=new ArrayList<>();
        baseActivity = (BaseActivity) getActivity();

        linearLayoutManager=new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvHomework.setLayoutManager(mLayoutManager);
        rvHomework.setItemAnimator(new DefaultItemAnimator());
        rvHomework.setHasFixedSize(true);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        homeworkAdapter = new HomeworkAdapter(homeworkArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        selectedDateTextview.setText("Today");

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void performAction() {
        swipeHomework.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeHomework.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /*if (isVisible) {*/
                        homeworkArrayListadd.clear();
                        homeworkAdapter.notifyDataSetChanged();
                        getHomeworkList();
                        swipeHomework.setRefreshing(false);
                        /*}*/
                    }
                }, 000);
            }
        });
    }

    public void getHomeworkList() {

        if (Util.isNetworkAvailable())
        {
            homeworkArrayListadd.clear();
            showProgressDialog();
            HomeworkListParms homeworkListParms=new HomeworkListParms();
            homeworkListParms.setSchoolID(String.valueOf(selectedSchool()));
            homeworkListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            homeworkListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            homeworkListParms.setLimit(100);
            homeworkListParms.setOffset(offset);
            homeworkListParms.setClassImage(Constants.CLASSIMAGE);
            homeworkListParms.setStudID(selected());
            isLoading=false;
            vidyauraAPI.getHomeworkList(homeworkListParms).enqueue(new Callback<HomeworkListResponse>() {
                @Override
                public void onResponse(Call<HomeworkListResponse> call, Response<HomeworkListResponse> response) {
                    mDialog.dismiss();
                    isLoading=true;
                    if (response.body() != null) {
                        homeworkListResponse = response.body();
                        if (homeworkListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            homeworkArrayList = response.body().getHomeworksList();
                            HashMap Showtimingmap = null;
                            if (homeworkListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (homeworkArrayList.size() != 0) {
                                    //  isLoading=false;
                                    homeworkArrayListadd.addAll(homeworkArrayList);
                                    swipeHomework.setVisibility(View.VISIBLE);
                                    tvNoresult.setVisibility(View.GONE);
                                    homeworkAdapter = new HomeworkAdapter(homeworkArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));
                                    rvHomework.setAdapter(homeworkAdapter);
                                    homeworkAdapter.notifyDataSetChanged();
//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
//                                    for (int y = 0; y < homeworkArrayList.size(); y++)
//                                    {
//
//                                    }
                                    //homeworkAdapter.notifyDataSetChanged();


//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
                                }
//                                else if (eventLists.size() != 0 && eventsListArrayList.size() != 0) {
//                                    noeventListTextview.setVisibility(View.VISIBLE);
//                                    eventrecycle.setVisibility(View.GONE);
//                                    loading = false;
//                                }
                            } else {

                                swipeHomework.setVisibility(View.GONE);
                                tvNoresult.setVisibility(View.VISIBLE);
                                tvNoresult.setText(homeworkListResponse.getMessage());
                                //Toast.makeText(getContext(), homeworkListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    }
                }
                @Override
                public void onFailure(Call<HomeworkListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible){

            if (isLoading) {
                getHomeworkList();
                isLoading=false;
            }

        }



    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){



            if (isLoading) {
                offset=0;
                getHomeworkList();
                isLoading=false;
            }
        }


    }


    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        isLoading=false;
    }

}