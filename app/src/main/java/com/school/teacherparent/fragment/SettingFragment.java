package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SplashScreenActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AppsettingParmas;
import com.school.teacherparent.models.EmptySecurityPinResponse;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener {


    private Switch chat_switch,notification_switch,security_switch;
    private Button update;
    RelativeLayout security_pin_layout;
    LinearLayout logoutLinear;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    private SharedPreferences fcmSharedPrefrences;
    SharedPreferences secureTokenSharedPreferences;
    int notificationStatus,messageStatus,securityStatus;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        chat_switch = (Switch) view.findViewById(R.id.chat_switch);
        notification_switch = (Switch) view.findViewById(R.id.noti_switch);
        security_switch = (Switch) view.findViewById(R.id.enable_switch);

        update = (Button) view.findViewById(R.id.update);
        security_pin_layout = view.findViewById(R.id.security_pin_layout);
        logoutLinear=view.findViewById(R.id.logoutLinear);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getContext().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, Context.MODE_PRIVATE);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.settings));

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        logoutLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Vidyaura")
                        .setMessage("Are you sure you want to exit from app?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                sharedPreferences.edit().clear().commit();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                getActivity().finishAffinity();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(), "updated successfully", Toast.LENGTH_SHORT).show();
                updateAppsetting();
            }
        });

        security_pin_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Vidyaura")
                        .setMessage("Are you sure you want to change your security pin?. If yes you will get logout")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ForgotSecuritypin();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        notificationStatus=sharedPreferences.getInt(Constants.NOTIFICATIONSTATUS,0);

        messageStatus=sharedPreferences.getInt(Constants.MESSAGESTATUS,0);

        securityStatus=sharedPreferences.getInt(Constants.SECURITYPINSTATUS,0);

        if (notificationStatus>0)
        {
            notification_switch.setChecked(true);

        }
        notification_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    notificationStatus=1;
                }
                else
                {
                    notificationStatus=0;
                }
            }
        });

        if (messageStatus>0)
        {
            chat_switch.setChecked(true);

        }
        chat_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    messageStatus=1;
                }
                else
                {
                    messageStatus=0;
                }
            }
        });

        if (securityStatus>0)
        {
            security_switch.setChecked(true);

        }
        security_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    securityStatus=1;
                }
                else
                {
                    securityStatus=0;
                }
            }
        });
        return view;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    private void ForgotSecuritypin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            vidyauraAPI.forgetSecutiyPin(sharedPreferences.getString(Constants.PHONE, "")
                    , String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0))
                    , String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0))).enqueue(new Callback<EmptySecurityPinResponse>() {
                @Override
                public void onResponse(Call<EmptySecurityPinResponse> call, Response<EmptySecurityPinResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        EmptySecurityPinResponse emptySecurityPinResponse = response.body();
                        if (emptySecurityPinResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            if (emptySecurityPinResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                sharedPreferences.edit().clear().apply();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                getActivity().finishAffinity();
                            } else {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, emptySecurityPinResponse.getToken()).commit();
                            ForgotSecuritypin();

                        }
                    }

                }

                @Override
                public void onFailure(Call<EmptySecurityPinResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void updateAppsetting() {
        if (Util.isNetworkAvailable()) {
            showProgress();


            AppsettingParmas appsettingParmas=new AppsettingParmas();
            appsettingParmas.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));
            appsettingParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            appsettingParmas.setNotificationStatus(notificationStatus);
            appsettingParmas.setMessageStatus(messageStatus);
            appsettingParmas.setSecurityPinStatus(securityStatus);


            vidyauraAPI.updateAppSettings(appsettingParmas).enqueue(new Callback<EmptySecurityPinResponse>() {
                @Override
                public void onResponse(Call<EmptySecurityPinResponse> call, Response<EmptySecurityPinResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        EmptySecurityPinResponse emptySecurityPinResponse = response.body();
                        if (emptySecurityPinResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            if (emptySecurityPinResponse.getStatus() == Util.STATUS_SUCCESS) {

                                editor.putInt(Constants.NOTIFICATIONSTATUS, notificationStatus);
                                editor.putInt(Constants.MESSAGESTATUS, messageStatus);
                                editor.putInt(Constants.SECURITYPINSTATUS, securityStatus);
                                editor.commit();
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //newpin.setError(getString(R.string.PleaseEnterValidPin));

                            }
                        } else {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, emptySecurityPinResponse.getToken()).commit();
                            updateAppsetting();

                        }
                    }

                }

                @Override
                public void onFailure(Call<EmptySecurityPinResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}
