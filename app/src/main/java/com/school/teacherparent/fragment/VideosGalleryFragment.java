package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.CustomPagerAdapter;
import com.school.teacherparent.adapter.PhotosandVideoGalleryAdapter;
import com.school.teacherparent.adapter.VideoAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GalleryParams;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.Photoslist;
import com.school.teacherparent.models.VideoDTO;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JZMediaManager;
import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.PhotosGalleryFragment.CAMERA_REQUEST;
import static com.school.teacherparent.fragment.PhotosGalleryFragment.GALLERY_PICTURE;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideosGalleryFragment extends BaseFragment {


    private FullscreenVideoLayout videoLayout;
    private RecyclerView recyclerview;
    private VideoAdapter videomAdapter;
    protected static final int GALLERY_PICTURE = 1;
    protected static final int CAMERA_REQUEST = 0;
    Bitmap bitmap;

    String selectedImagePath;
    private int RESULT_OK=-1;
    private List<VideoDTO> gridList;
    private FloatingActionButton add_result;
    AlertDialog dialog;


    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    int limit=50;
    int offset=0;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public boolean isLoading = true;
    List<Photoslist> videolists=new ArrayList<>();
    GridLayoutManager gridLayoutManager;

    private boolean loading = true;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_videos_gallery, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        recyclerview=(RecyclerView)view.findViewById(R.id.recycler);
        add_result=(FloatingActionButton)view.findViewById(R.id.add_result);

        add_result.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                intiatepopupwindow();
            }

            private void intiatepopupwindow() {
                {
                    LayoutInflater inflater = getLayoutInflater();



                    View alertLayout = inflater.inflate(R.layout.popupwindow, null);
                    final ImageView camera=alertLayout.findViewById(R.id.camera);
                    final ImageView gallery=alertLayout.findViewById(R.id.gallery);
                    LinearLayout camera_layout=alertLayout.findViewById(R.id.camera_layout);
                    LinearLayout gallery_layout=alertLayout.findViewById(R.id.gallery_layout);
                    AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme));
                    alert.setView(alertLayout);
                   /* AlertDialog dialog = alert.create();
                    dialog.show();*/
                    BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                    dialog.setContentView(R.layout.popupwindow);
                    dialog.show();


                    camera_layout.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            Toast.makeText(getActivity(),"hello",Toast.LENGTH_SHORT).show();

                            return false;
                        }
                    });
                    gallery_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Toast.makeText(getActivity(),"hello",Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }

        });

        /*videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
        videoLayout.setActivity(getActivity());

        Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        try {
            videoLayout.setVideoURI(videoUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        gridList=new ArrayList<>();

        videomAdapter=new VideoAdapter(videolists,getActivity());
         gridLayoutManager = new GridLayoutManager(getActivity(),2);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(videomAdapter);


        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getgalleryList();

                        }
                    }
                }
            }
        });

        videomAdapter.setOnClickListen(new VideoAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                initiatepopupwindow(videolists.get(position).getImage(),"mp4", position);

            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        bitmap = null;
        selectedImagePath = null;

        if (resultCode == RESULT_OK && requestCode == CAMERA_REQUEST) {

            File f = new File(Environment.getExternalStorageDirectory()
                    .toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }

            if (!f.exists()) {

                Toast.makeText(getActivity(),

                        "Error while capturing image", Toast.LENGTH_LONG)

                        .show();

                return;

            }

            try {

                bitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, true);

                int rotate = 0;
                try {
                    ExifInterface exif = new ExifInterface(f.getAbsolutePath());
                    int orientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION,
                            ExifInterface.ORIENTATION_NORMAL);

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            rotate = 270;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            rotate = 180;
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            rotate = 90;
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Matrix matrix = new Matrix();
                matrix.postRotate(rotate);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                        bitmap.getHeight(), matrix, true);




                gridList.add(new VideoDTO());
                //    img_logo.setImageBitmap(bitmap);
                //storeImageTosdCard(bitmap);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (resultCode == RESULT_OK && requestCode == GALLERY_PICTURE) {
            if (data != null) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Cursor c = getActivity().getContentResolver().query(selectedImage, filePath,
                        null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                selectedImagePath = c.getString(columnIndex);
                c.close();

                if (selectedImagePath != null) {
                    gridList.add(new VideoDTO());
                }
                VideoDTO placeWorkModel = new VideoDTO();
                gridList.add(placeWorkModel);

           //     mAdapter.updateList(gridList);

                videomAdapter.notifyDataSetChanged();
              /*  bitmap = BitmapFactory.decodeFile(selectedImagePath); // load
                // preview image
                bitmap = Bitmap.createScaledBitmap(bitmap, 400, 400, false);



                gridList.add(new PhotosGalleryDTO());*/

            } else {
                Toast.makeText(getActivity(), "Cancelled",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void getgalleryList() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            GalleryParams galleryParams = new GalleryParams();
            galleryParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            galleryParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            galleryParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            galleryParams.setMediaType(3);
            galleryParams.setLimit(limit);
            galleryParams.setOffset(offset);


            vidyauraAPI.getGalleryList(galleryParams).enqueue(new Callback<GalleryListResponse>() {
                @Override
                public void onResponse(Call<GalleryListResponse> call, Response<GalleryListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        GalleryListResponse galleryListResponse = response.body();
                        if (galleryListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (galleryListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < galleryListResponse.getGalleryListResult().size(); i++) {
                                    if (galleryListResponse.getGalleryListResult().get(i).getAlbumList() != null) {
                                        for (int y = 0; y < galleryListResponse.getGalleryListResult().get(i).getAlbumList().size(); y++) {
                                            videolists.add(new Photoslist(galleryListResponse.getGalleryListResult().get(i).getId(),
                                                    galleryListResponse.getGalleryListResult().get(i).getTitle(), galleryListResponse.getGalleryListResult().get(i).getDescription(),
                                                    galleryListResponse.getGalleryListResult().get(i).getAlbumList().get(y).getMedia_path()));
                                        }
                                    }
                                }
                                loading=false;
                                videomAdapter.notifyDataSetChanged();

                            } else {
                                loading=false;
                                Toast.makeText(getContext(), galleryListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            loading=true;
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        loading=true;

                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<GalleryListResponse> call, Throwable t) {
                    hideProgress();
                    loading=true;

                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            loading=true;

            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        JzvdStd.releaseAllVideos();

        if (isVisible){
            //videolists.clear();
            videomAdapter.notifyDataSetChanged();
            offset = 0;
            if (isLoading) {
                getgalleryList();
                isLoading=false;
            }

        }



    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        JzvdStd.releaseAllVideos();
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){
            videolists.clear();
            videomAdapter.notifyDataSetChanged();
                offset = 0;
                getgalleryList();
                isLoading=false;

        }


    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        JzvdStd.releaseAllVideos();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        JzvdStd.releaseAllVideos();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        JzvdStd.releaseAllVideos();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void initiatepopupwindow(String mediapath, String media_type, int position) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd video_selected=alertLayout.findViewById(R.id.video_selected);;

        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(getActivity(),videolists,1));
        viewPager.setCurrentItem(position);

        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        dialog = alert.create();
        dialog.show();

        if (videolists.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                JzvdStd.releaseAllVideos();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                dialog.dismiss();
            }
        });
        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                viewPager.setCurrentItem(viewPager.getCurrentItem()-1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true); //getItem(-1) for previous
            }
        });

        /*if (media_type.equals("png")) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/" + mediapath;
            Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
//                            Bitmap bitmap = drawable.getBitmap();
//                            int bitmapwidth=0;
//                            int bitmpheight=0;
//                            bitmapwidth=bitmap.getWidth();
//                            bitmpheight=bitmap.getHeight();
//                            LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)imageView.getLayoutParams();
//                            if (bitmapwidth==0||bitmpheight==0){
//                                return;
//                            }
//                            int newwidth=imageView.getWidth();
//                            int newheight=newwidth*bitmpheight/bitmapwidth;
//                            params.height=newheight;
//                            imageView.setLayoutParams(params);
//
//                        }
//                        catch (Exception e){
//
//                        }
//
//                    }
//                },1000);
                }

                @Override
                public void onError(Exception e) {
                    imageView.setImageResource(R.mipmap.school);
                }
            });
        }
        else
        {
            video_selected.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);

            video_selected.setUp((getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath)
                    , Jzvd.SCREEN_WINDOW_NORMAL, "");
            video_selected.setSystemTimeAndBattery();

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
            requestOptions.error(R.mipmap.icon_video_thumbnail);
            Glide.with(getContext())
                    .load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath)
                    .apply(requestOptions)
                    .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath))
                    .into(video_selected.thumbImageView);
        }*/
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }

}
