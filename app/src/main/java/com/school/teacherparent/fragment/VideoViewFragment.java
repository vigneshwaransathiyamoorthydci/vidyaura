package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.VideoDetailActivity;
import com.school.teacherparent.adapter.PhotosGalleryAdapter;
import com.school.teacherparent.adapter.VideoViewAdapter;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.VideoViewDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoViewFragment extends Fragment {


    private RecyclerView recyclerview;
    private LinearLayout lin;
    private List<VideoViewDTO> videoviewList=new ArrayList<>();
    private VideoViewAdapter mAdapter;
    private FloatingActionButton addResult;

    public VideoViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_video_view, container, false);


        recyclerview = (RecyclerView) view.findViewById(R.id.videoview_recyclerview);
        //   lin = (LinearLayout) view.findViewById(R.id.lin);
        //   edit=(EditText)view.findViewById(R.id.edit);
        mAdapter = new VideoViewAdapter(videoviewList, getActivity());

        addResult = view.findViewById(R.id.add_result);


        AnalData();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(mAdapter);


        mAdapter.setOnClickListen(new VideoViewAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                Intent intent=new Intent(getActivity(), VideoDetailActivity.class);
                startActivity(intent);
            }
        });

        addResult = view.findViewById(R.id.add_result);

        addResult.setOnClickListener(new View.OnClickListener() {

            public int REQUEST_CAPTURE_IMAGE=-1;
            public PopupWindow pwindow;
            public Context context;
            public Bitmap mBitmap;
            public AlertDialog.Builder myAlertDialog;
            public int position;

            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();


                View alertLayout = inflater.inflate(R.layout.popupwindow, null);
                final ImageView camera = alertLayout.findViewById(R.id.camera);
                final ImageView gallery = alertLayout.findViewById(R.id.gallery);
                LinearLayout camera_layout = alertLayout.findViewById(R.id.camera_layout);
                LinearLayout gallery_layout = alertLayout.findViewById(R.id.gallery_layout);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme));
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                BottomSheetDialog dialog1 = new BottomSheetDialog(getActivity());
                dialog1.setContentView(R.layout.popupwindow);
                dialog1.show();

            }});
        return view;
    }

    private void AnalData() {
        VideoViewDTO s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));

        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.party));

        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.test));

        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.dance));


        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));

        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.party));


        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.test));


        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.dance));


        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        videoviewList.add(s);

        s=new VideoViewDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        videoviewList.add(s);
    }

}


