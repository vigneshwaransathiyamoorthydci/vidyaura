package com.school.teacherparent.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ClassWorkforParentAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentClassWorkFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    DiscreteScrollView dsvStudentList;
    ImageView ivNoti,ivCalendar,ivBack;
    TextView dateTextview;
    ChildListResponseResponse childListResponseResponse;
    SwipeRefreshLayout swipeClasswork;
    private RecyclerView rvClassworkList;
    private ClassWorkforParentAdapter classworkAdapter;
    private List<ClassworkLists.ClassworkList> classList = new ArrayList<>();
    int count = 0;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID,studentSchoolID;
    String formattedDate;
    ProgressDialog mDialog;
    int offset=0;
    int limit = 5;
    boolean loading=false;
    LinearLayoutManager layoutManager;
    boolean isLoading=true;
    NotificationReceiver notificationReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_class_work, container, false);
        initViews(view);
        performAction();


        return view;
    }

    private void initViews(View view) {
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        classworkAdapter = new ClassWorkforParentAdapter(classList,getActivity(),ParentClassWorkFragment.this, studentID,studentSchoolID);
        ivNoti = view.findViewById(R.id.noti);
        ivCalendar = view.findViewById(R.id.calendar);
        ivBack = view.findViewById(R.id.back);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        swipeClasswork =view.findViewById(R.id.swipe_classwork);
        dateTextview = view.findViewById(R.id.dateTextview);
        rvClassworkList = view.findViewById(R.id.rv_classwork_list);

        layoutManager=new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rvClassworkList.setLayoutManager(mLayoutManager);
        rvClassworkList.setItemAnimator(new DefaultItemAnimator());
        rvClassworkList.setHasFixedSize(true);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getActivity().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date c = Calendar.getInstance().getTime();
        formattedDate = curFormater.format(c);
        dateTextview.setText("Today");

        getStudentList();


    }

    private void performAction() {


        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        ivCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                SimpleDateFormat selectedDate = new SimpleDateFormat("dd MMMM");
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    Date dateObj = curFormater.parse(a);
                                    formattedDate = curFormater.format(dateObj);
                                    Date c = Calendar.getInstance().getTime();
                                    String todayDate = curFormater.format(c);;
                                    if (formattedDate.equals(todayDate)) {
                                        dateTextview.setText("Today");
                                    } else {
                                        dateTextview.setText(selectedDate.format(dateObj));
                                    }
                                    getStudentClassworkList(studentID);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, mYear, mMonth, mDay);
                //datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        rvClassworkList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset = offset + limit;
                            getStudentClassworkList(studentID);
                        }
                    }
                }
            }
        });

        swipeClasswork.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeClasswork.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                 classList.clear();
                 classworkAdapter.notifyDataSetChanged();
                 offset=0;
                 getStudentClassworkList(studentID);
                 swipeClasswork.setRefreshing(false);
                    }
                }, 000);
            }
        });



    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                if (isLoading) {
                                    getStudentClassworkList(studentID);
                                }
                            } else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);

        /*if (sharedPreferences.getInt("ParentClassWork",0) == 1) {
            dsvStudentList.scrollToPosition(sharedPreferences.getInt("ParentClassWorkID",0));
        } else {*/
            if (parentsStudListArrayList.size() > 2) {
                dsvStudentList.scrollToPosition(1);
                classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
                sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
                studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
                studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
            } else {
                dsvStudentList.scrollToPosition(0);
                classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
                sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
                studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
                studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
            }
        /*}*/
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentClassworkList(final int selectedStudentID) {


        if (Util.isNetworkAvailable()) {
            isLoading=false;
            classList.clear();
            showProgressDialog();
            //isLoading=false;
            FeedListParams feedListParams = new FeedListParams();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(studentSchoolID));
            feedListParams.setLimit(Constants.DATALIMIT);
            feedListParams.setOffset(offset);
            feedListParams.setClassImage(Constants.CLASSIMAGE);
            feedListParams.setStudID(studentID);
            Gson gson = new Gson();
            String input = gson.toJson(feedListParams);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getClassworkList(feedListParams).enqueue(new Callback<ClassworkLists>() {
                @Override
                public void onResponse(Call<ClassworkLists> call, Response<ClassworkLists> response) {
                    mDialog.dismiss();
                    //isLoading=true;
                    if (response.body() != null) {
                        isLoading=true;
                        ClassworkLists data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse parent class : " + res);

                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getClassworkList() != null &&
                                        response.body().getClassworkList().size() != 0) {
                                    rvClassworkList.setVisibility(View.VISIBLE);
                                    classList.addAll(data.getClassworkList());
                                    classworkAdapter = new ClassWorkforParentAdapter(classList,getActivity(), ParentClassWorkFragment.this,studentID,studentSchoolID);
                                    rvClassworkList.setAdapter(classworkAdapter);
                                    classworkAdapter.notifyDataSetChanged();

                                    loading = true;
                                } else {
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    loading = false;
                                }
                            } else {
                                if (!loading){
                                    Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        isLoading=true;
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ClassworkLists> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading=true;
                    //isLoading=true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {

            mDialog.dismiss();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            classList.clear();
            if (isLoading) {
                getStudentClassworkList(studentID);
            }

        }
    }



    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
