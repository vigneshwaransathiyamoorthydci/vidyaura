package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ExamHistoryAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamHistoryList;
import com.school.teacherparent.models.ExamHistoryResponse;
import com.school.teacherparent.models.UpcomingExamListParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ExamFragment.selected;
import static com.school.teacherparent.fragment.ExamFragment.selectedSchID;

public class ParentExamHistoryFragment extends BaseFragment {

    SwipeRefreshLayout swipeExamHistory;
    RecyclerView rvExamHistory;
    TextView tvNoExamHistory;
    LinearLayoutManager linearLayoutManager;
    private ExamHistoryAdapter examHistoryAdapter;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public ArrayList<ExamHistoryResponse.examsHistoryList> examHistoryListResponsearray=new ArrayList<>();
    static ArrayList<ExamHistoryList> examHistoryListArrayList=new ArrayList<>();

    private boolean loading = true;
    int limit=5;
    public static int offset=0;
    private Boolean isVisible = false;
    boolean service=true;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    ProgressDialog mDialog;

    @Override
    public void onStart() {
        super.onStart();
        /*isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {
            offset = 0;
            examHistoryListArrayList.clear();
            if (service) {
                getExamList();
            }
        }*/


    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            offset = 0;
            examHistoryListArrayList.clear();
            if (service) {
                getExamList();
            }

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_exam_history, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {
        swipeExamHistory = view.findViewById(R.id.swipe_historyexam);
        rvExamHistory = view.findViewById(R.id.rv_exam_history);
        tvNoExamHistory = view.findViewById(R.id.tv_noexam_history);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        ExamsActivity.title.setText("Exam");
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvExamHistory.setLayoutManager(linearLayoutManager);
        rvExamHistory.setItemAnimator(new DefaultItemAnimator());
        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        examHistoryAdapter = new ExamHistoryAdapter(examHistoryListArrayList,getContext(),this,userType);
        rvExamHistory.setAdapter(examHistoryAdapter);

    }

    private void performAction() {
        swipeExamHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeExamHistory.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                            limit=5;
                            offset=0;
                            examHistoryListArrayList.clear();
                            getExamList();
                            swipeExamHistory.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });

        ExamsActivity.noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),NotificationActivity.class));
            }
        });
        rvExamHistory.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView examHistoryrecyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            examHistoryAdapter.notifyDataSetChanged();
                            getExamList();
                            examHistoryAdapter.notifyDataSetChanged();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }

    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getExamList() {

        if (Util.isNetworkAvailable())
        {
            System.out.println("examHistoryListArrayList ==> "+examHistoryListArrayList.size()+limit+offset);
            service=false;
            showProgressDialog();
            UpcomingExamListParams upcomingExamListParams=new UpcomingExamListParams();
            upcomingExamListParams.setSchoolID(String.valueOf(selectedSchID()));
            upcomingExamListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            upcomingExamListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            upcomingExamListParams.setLimit(limit);
            upcomingExamListParams.setOffset(offset);
            upcomingExamListParams.setStudID(selected());
            vidyauraAPI.getExamsHistoryList(upcomingExamListParams).enqueue(new Callback<ExamHistoryResponse>() {
                @Override
                public void onResponse(Call<ExamHistoryResponse> call, Response<ExamHistoryResponse> response) {
                    mDialog.dismiss();
                    service=true;
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("reponse for his ==> "+gson.toJson(response.body()));
                        ExamHistoryResponse examHistoryResponse = response.body();
                        if (examHistoryResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examHistoryListResponsearray != null) {
                                examHistoryListResponsearray.clear();
                            }
                            examHistoryListResponsearray = examHistoryResponse.getExamsHistoryList();
                            if (examHistoryResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examHistoryListResponsearray.size()>0) {
                                    //examHistoryrecyclerView.setVisibility(View.VISIBLE);
                                    // noexamhistoryListTextview.setVisibility(View.GONE);
                                    for (int y = 0; y < examHistoryListResponsearray.size(); y++) {
                                        examHistoryListArrayList.add(new ExamHistoryList(examHistoryResponse.getExamsHistoryList().get(y).getTerm_id(), examHistoryResponse.getExamsHistoryList().get(y).getExam_title(), examHistoryResponse.getExamsHistoryList().get(y).getExam_duration(),
                                                examHistoryResponse.getExamsHistoryList().get(y).getStart_date(), examHistoryResponse.getExamsHistoryList().get(y).getEnd_date(), examHistoryResponse.getExamsHistoryList().get(y).getSchool_name(),
                                                examHistoryResponse.getExamsHistoryList().get(y).getLogo(), examHistoryResponse.getExamsHistoryList().get(y).getClassroom_id(),
                                                examHistoryResponse.getExamsHistoryList().get(y).getClassName(),examHistoryResponse.getExamsHistoryList().get(y).getSection(),examHistoryResponse.getExamsHistoryList().get(y).getClasssection_id()));

                                        //examHistoryAdapter.notifyDataSetChanged();
                                        loading = true;
                                    }
                                    if (examHistoryListArrayList.size() > 0) {
                                        examHistoryAdapter.notifyDataSetChanged();
                                    }
                                }
                                /*else if (examHistoryListResponsearray.size()!=0 && examHistoryListResponsearray.size()!=0)
                                {
                                    //noexamhistoryListTextview.setVisibility(View.VISIBLE);
                                    //examHistoryrecyclerView.setVisibility(View.GONE);
                                   // Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    loading=false;
                                }*/
                                /*else
                                {
                                    //Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }*/
                            } else {
                                examHistoryAdapter.notifyDataSetChanged();
                                //Toast.makeText(getContext(), examHistoryResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamHistoryResponse> call, Throwable t) {
                    mDialog.dismiss();
                    service=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

}