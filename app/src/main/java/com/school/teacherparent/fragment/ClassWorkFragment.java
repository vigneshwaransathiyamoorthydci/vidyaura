package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ClassAdapter;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.SyllabusListModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassWorkFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private RecyclerView classworkrecyclerView;
    private ClassAdapter classworkAdapter;
    private List<ClassworkLists.ClassworkList> classList = new ArrayList<>();
    FloatingActionButton addClasswork;
    ImageView noti, back;
    int offset=0;
    boolean loading=false;
    LinearLayoutManager layoutManager;
    int limit = 5;


    SwipeRefreshLayout classworkswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
//        if (isVisible && isStarted){
//            classList.clear();
//            offset=0;
//            getClassworkList();
//        }
        if (isVisible){
            classList.clear();
            offset=0;
            if (isLoading) {
                classList.clear();
                classworkAdapter.notifyDataSetChanged();
                getClassworkList();
                isLoading=false;
            }

        }



    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){
            classList.clear();
            offset=0;

            if (isLoading) {
                classList.clear();
                classworkAdapter.notifyDataSetChanged();
                getClassworkList();
                isLoading=false;
            }
        }


    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        classList.clear();
//        offset=0;
//        getClassworkList();
//    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_class, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        classworkrecyclerView = (RecyclerView) view.findViewById(R.id.recycle_fragment_class);
        addClasswork = view.findViewById(R.id.add_classwork);


      /*  noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });*/

        classworkAdapter = new ClassAdapter(classList,getActivity());
        layoutManager=new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        classworkrecyclerView.setLayoutManager(mLayoutManager);
        classworkrecyclerView.setItemAnimator(new DefaultItemAnimator());
        classworkrecyclerView.setAdapter(classworkAdapter);

        classworkrecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addClasswork.setVisibility(View.VISIBLE);
                }
                else
                {
                    addClasswork.setVisibility(View.GONE);
                }
            }
        });
        classworkrecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset = offset + limit;
                            getClassworkList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        //Analdata();
        classworkswipelayout=view.findViewById(R.id.swipe_classwork);
        classworkAdapter.setOnClickListen(new ClassAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment = new SubClassFragment();
                Bundle bundle=new Bundle();
                bundle.putString("classname",classList.get(position).getClassName()+classList.get(position).getSection());
                bundle.putString("classwrkid",String.valueOf(classList.get(position).getClassworkID()));
                bundle.putString("classid",String.valueOf(classList.get(position).getClassID()));
                bundle.putString("sectionid",String.valueOf(classList.get(position).getSectionID()));
                bundle.putString("subjectid",String.valueOf(classList.get(position).getSubjectID()));
                fragment.setArguments(bundle);
               // bundle.putString("");
                replaceFragment(fragment);
            }
        });
        classworkswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                classworkswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
//                            isLoading = false;
//                            currentPage = 0;
//                            lastEnd = false;
//                            timeLineList.clear();
//                            timeLineAdapter.notifyDataSetChanged();
//                            getTimeLineList();

                            classList.clear();
                            classworkAdapter.notifyDataSetChanged();
                            offset=0;
                            getClassworkList();
                            isLoading=false;
                            classworkswipelayout.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });

        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classworkrecyclerView.setAdapter(null);
                classList.clear();
                classworkAdapter.notifyDataSetChanged();
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
            }
        });

        return view;

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;
        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
    public void getClassworkList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading=false;
            FeedListParams feedListParams = new FeedListParams();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            feedListParams.setLimit(Constants.DATALIMIT);
            feedListParams.setOffset(offset);
            feedListParams.setClassImage(Constants.CLASSIMAGE);
            Gson gson = new Gson();
            String input = gson.toJson(feedListParams);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getClassworkList(feedListParams).enqueue(new Callback<ClassworkLists>() {
                @Override
                public void onResponse(Call<ClassworkLists> call, Response<ClassworkLists> response) {
                    hideProgress();
                    isLoading=true;
                    if (response.body() != null) {
                        ClassworkLists data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);

                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getClassworkList() != null &&
                                        response.body().getClassworkList().size() != 0) {
                                    classworkrecyclerView.setVisibility(View.VISIBLE);
                                    System.out.println("getClassImage ==> "+data.getClassworkList().get(0).getClass_image());
                                    classList.addAll(data.getClassworkList());
                                    classworkrecyclerView.setAdapter(classworkAdapter);
                                    classworkAdapter.notifyDataSetChanged();
                                    loading = true;
                                } else {
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    loading = false;
                                }
                            } else {
                                if (!loading){
                                    Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ClassworkLists> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }



}
