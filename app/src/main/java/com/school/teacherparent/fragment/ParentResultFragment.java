package com.school.teacherparent.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ParentResultListAdapter;
import com.school.teacherparent.adapter.ResultListAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.ResultList;
import com.school.teacherparent.models.ResultListParams;
import com.school.teacherparent.models.ResultListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParentResultFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {


    private Spinner result_spinner;
    private RecyclerView resultListrecyclerView;
    private ParentResultListAdapter parentResultListAdapter;
    ImageView noti,back;

    public ParentResultFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    ExamTermResponse examTermResponse;
    ResultListResponse resultListResponse;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    int examTermID;
    List<ResultList> resultLists=new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    public TextView titleTextview;
    int examterm;
    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialog;

    int limit=5;
    int offset=0;
    int count = 0;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    public int classID;
    public static int studentID,studentSchoolID;
    public int sectionID;
    String altname,sectionname;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parent_result, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        Bundle b = getArguments();
        examterm= b.getInt("examterm");
        editor = sharedPreferences.edit();
        result_spinner = (Spinner) view.findViewById(R.id.result_spinner);
        resultListrecyclerView = (RecyclerView) view.findViewById(R.id.recycle_result_home);
        titleTextview=(TextView)view.findViewById(R.id.titleTextview);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item_dialog);

        result_spinner.setAdapter(spinnerexamTermAdapter);
        getStudentList();
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        resultListrecyclerView.setItemAnimator(new DefaultItemAnimator());
        resultListrecyclerView.setLayoutManager(linearLayoutManager);

        parentResultListAdapter = new ParentResultListAdapter(resultLists, getActivity(),ParentResultFragment.this);
        resultListrecyclerView.setAdapter(parentResultListAdapter);


        result_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // TODO Auto-generated method stub
                if (position == 0) {
                    examTermID = 0;
                } else {
                    examTermID = examTermResponse.getExamTermsList().get(position - 1).getId();
                    resultLists.clear();
                    parentResultListAdapter.notifyDataSetChanged();
                    getresultList();
                    /*parentsStudListArrayList.clear();
                    examTermSpinner.clear();
                    resultLists.clear();*/
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        return view;
    }


    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            resultLists.clear();
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                resultLists.clear();
                                parentResultListAdapter.notifyDataSetChanged();
                                getExamtermList();
                            } else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getresultList()
    {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            resultLists.clear();
            ResultListParams resultListParams = new ResultListParams();
            resultListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            resultListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            resultListParams.setSchoolID(String.valueOf(studentSchoolID));
            resultListParams.setExamTermID(String.valueOf(examTermID));
            resultListParams.setStudID(studentID);
            vidyauraAPI.getResultsList(resultListParams).enqueue(new Callback<ResultListResponse>() {
                @Override
                public void onResponse(Call<ResultListResponse> call, Response<ResultListResponse> response) {
                    mDialog.dismiss();
                    Gson gson = new Gson();
                    System.out.println("response result ==> "+gson.toJson(response.body()));
                    if (response.body() != null) {
                        resultListResponse = response.body();
                        if (resultListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (resultListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (resultListResponse.getResultList().size() > 0) {
                                    resultLists.clear();
                                    for (int i=0;i<resultListResponse.getResultList().size();i++) {
                                        if(resultListResponse.getResultList().get(i).getClassAverage().equals("0"))
                                        {
                                            resultLists.add(new ResultList(classID,
                                                    sectionID, resultListResponse.getResultList().get(i).getSchoolName(),
                                                    resultListResponse.getResultList().get(i).getClassAverage(), 0,
                                                    0,"", "", "",
                                                    altname, resultListResponse.getResultList().get(i).getClassName(),
                                                    sectionname,
                                                    resultListResponse.getResultList().get(i).getStudent_photo(),resultListResponse.getResultList().get(i).getStudentDetails().getStudID(),
                                                    resultListResponse.getResultList().get(i).getStudentDetails().getFname(),resultListResponse.getResultList().get(i).getStudentDetails().getLname(),
                                                    resultListResponse.getResultList().get(i).getStudentDetails().getStudent_photo()));
                                        }
                                        else
                                        {
                                            if (resultListResponse.getResultList().get(i).getTopperInClass().size()>0) {
                                                resultLists.add(new ResultList(classID,
                                                        sectionID, resultListResponse.getResultList().get(i).getSchoolName(),
                                                        resultListResponse.getResultList().get(i).getClassAverage(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getTotal_marks(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getStudent_id(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getFname(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getLname(),
                                                        resultListResponse.getResultList().get(i).getTopperInClass().get(0).getStudent_photo(),
                                                        altname,
                                                        resultListResponse.getResultList().get(i).getClassName(),
                                                        sectionname,
                                                        resultListResponse.getResultList().get(i).getStudent_photo(),resultListResponse.getResultList().get(i).getStudentDetails().getStudID(),
                                                        resultListResponse.getResultList().get(i).getStudentDetails().getFname(),resultListResponse.getResultList().get(i).getStudentDetails().getLname(),
                                                        resultListResponse.getResultList().get(i).getStudentDetails().getStudent_photo()));

                                            }
                                        }
                                        parentResultListAdapter.notifyDataSetChanged();
                                    }

                                    parentResultListAdapter.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(getContext(), resultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), resultListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResultListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            altname=parentsStudListArrayList.get(adapterPosition).getClassName();
            sectionname=parentsStudListArrayList.get(adapterPosition).getSection();
            getExamtermList();
            if (examTermID > 0 && studentID > 0) {
                getresultList();
            }
        }
    }

    public static void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            resultLists.clear();
            examTermSpinner.clear();
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getExamTermParam.setStudID(studentID);

            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    mDialog.dismiss();

                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                resultLists.clear();
                                examTermSpinner.clear();
                                examTermSpinner.add("Select Exam");
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();
                                    result_spinner.performClick();
                                    if (examterm>0)
                                    {

                                        for (int y=0;y<examTermResponse.getExamTermsList().size();y++)
                                        {
                                            if (examterm==examTermResponse.getExamTermsList().get(y).getId())
                                            {
                                                result_spinner.setSelection(y+1);
                                                hideSpinnerDropDown(result_spinner);
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
