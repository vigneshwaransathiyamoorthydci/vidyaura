package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.adapter.TimeTableAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddProfileParams;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.models.TimetableParms;
import com.school.teacherparent.models.UpdateProfileResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.FontTextViewRegular;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileforStudentsFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder>   {



    TextView user_name,birth_date,empid_textview,calendar_date,user_time,points,points_textview;

    public ProfileforStudentsFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    List<SelectedImageList> imageList = new ArrayList<>();
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    DiscreteScrollView dsvStudentList;
    ChildListResponseResponse childListResponseResponse;
    int count=0;
    ArrayList<ChildListResponseResponse.parentsStudList>  parentsStudListArrayList;
    RecyclerView recyclerView;
    private List<TimeTableModel.TimetableList> timeList = new ArrayList<>();
    private TimeTableAdapter mAdapter;
    int studentID,studentSchoolID;
    Menu menu;
    NotificationReceiver notificationReceiver;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_profile_student, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        setHasOptionsMenu(true);
        TestDrawerActivity.toolbar.setTitle(getResources().getString(R.string.profile));
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        user_name=view.findViewById(R.id.user_name);
        birth_date=view.findViewById(R.id.birth_date);
        empid_textview=view.findViewById(R.id.empid_textview);
        calendar_date=view.findViewById(R.id.calendar_date);
        user_time=view.findViewById(R.id.user_time);
        points=view.findViewById(R.id.points);
        points_textview=view.findViewById(R.id.points_textview);
        points.setText(getString(R.string.rank));
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        parentsStudListArrayList=new ArrayList<>();

        recyclerView = view.findViewById(R.id.timetable_recycle);
        mAdapter = new TimeTableAdapter(timeList,getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        getStudentList();
        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        MenuItem item = menu.findItem(R.id.action_calender);
        item.setVisible(false);
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void setupViewPager() {

        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
        } else {
            dsvStudentList.scrollToPosition(0);
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());


    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            //Toast.makeText(getContext(),parentsStudListArrayList.get(adapterPosition).getFname(),Toast.LENGTH_SHORT).show();
            birth_date.setText(getDateFormatbyMonth(parentsStudListArrayList.get(adapterPosition).getDob()));
            calendar_date.setText(getDateFormatbyMonthforProfile(parentsStudListArrayList.get(adapterPosition).getAdmission_date()));
            empid_textview.setText(parentsStudListArrayList.get(adapterPosition).getAdmission_no());
            if(parentsStudListArrayList.get(adapterPosition).getStudRank()>0) {
                points_textview.setText("" + parentsStudListArrayList.get(adapterPosition).getStudRank());
            }
            else
            {
                points_textview.setText(getString(R.string.nill));
            }
            Calendar calendar;
            studentID=parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID=parentsStudListArrayList.get(adapterPosition).getSchool_id();
            calendar = Calendar.getInstance();
            Date date = calendar.getTime();
           String daynam=(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
            selecteddayname(daynam);

        }
    }

    public void selecteddayname(String dayname) {
        switch (dayname) {
            case "Monday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.MON);

                break;
            case "Tuesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.TUES);

                break;
            case "Wednesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.WED);

                break;
            case "Thursday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.THURS);

                break;
            case "Friday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.FRI);

                break;
            case "Saturday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SAT);

                break;
            case "Sunday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SUN);

                break;
        }
    }
    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.action_search);
        item.setVisible(false);
    }


    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));

            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                 hideProgress();
                    if (response.body()!=null)
                    {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                                if (childListResponseResponse.getParentsStudList().size() > 0) {
                                    count = response.body().getParentsStudList().size();
                                    parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                    setupViewPager();


                                } else {
                                    Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                         hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();

                            getStudentList();
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
              hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    public void getTimetableList(String dayname) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            TimetableParms timetableParms = new TimetableParms();
            timetableParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            timetableParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            timetableParms.setSchoolID(String.valueOf(studentSchoolID));
            timetableParms.setTimetableDay(dayname);
            timetableParms.setStudID(studentID);
            Gson gson = new Gson();
            String input = gson.toJson(timetableParms);

            vidyauraAPI.getTimetableList(timetableParms).enqueue(new Callback<TimeTableModel>() {
                @Override
                public void onResponse(Call<TimeTableModel> call, Response<TimeTableModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        TimeTableModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getTimetableList() != null &&
                                        response.body().getTimetableList().size() != 0) {
                                    recyclerView.setVisibility(View.VISIBLE);


                                    // timeList.addAll(data.getTimetableList());


                                    List<String> l = new ArrayList<String>();
                                    for (int i=0;i<data.getTimetableList().size();i++)
                                    {
                                        //l.add(data.getTimetableList().get(i).getBreakTiming().get(0).getStart_time());
                                        if (data.getTimetableList().get(i).getBreakTiming() != null) {
                                            if (data.getTimetableList().get(i).getBreakTiming().size() > 0) {
                                                timeList.add(data.getTimetableList().get(i));
                                            }
                                        }
                                    }



                                    Collections.sort(timeList, new Comparator() {
                                        @Override
                                        public int compare(Object o1, Object o2) {
                                            TimeTableModel.TimetableList p1 = (TimeTableModel.TimetableList) o1;
                                            TimeTableModel.TimetableList p2 = (TimeTableModel.TimetableList) o2;

                                            try {
                                                return new SimpleDateFormat("hh:mm a").parse(p1.getBreakTiming().get(0).getStart_time()).compareTo
                                                        (new SimpleDateFormat("hh:mm a").parse(p2.getBreakTiming().get(0).getStart_time()));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            return 0;
                                            // return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));

                                        }

                                    });
                                    mAdapter.notifyDataSetChanged();

//                                    Collections.sort(l, new Comparator<String>() {
//
//                                        @Override
//                                        public int compare(String o1, String o2) {
//                                            try {
//                                                return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
//                                            } catch (ParseException e) {
//                                                return 0;
//                                            }
//                                        }
//                                    });
//                                    System.out.println(l);


                                    // loading = true;
                                } else {
                                    timeList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    // loading = false;
                                }
                            } else {
                                timeList.clear();
                                mAdapter.notifyDataSetChanged();
                                //  if (!loading) {
                                Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                // }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        timeList.clear();
                        mAdapter.notifyDataSetChanged();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TimeTableModel> call, Throwable t) {
                    timeList.clear();
                    mAdapter.notifyDataSetChanged();
                    hideProgress();

                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

}
