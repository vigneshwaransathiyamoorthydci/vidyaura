package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.BuildConfig;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.IsUserExists;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 10/23/2018.
 */

public class LoginNumberFragment extends BaseFragment {
    TextView skip;
    EditText mPhone_number;
    Button submit;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public LoginNumberFragment() {
        // Required empty public constructor
    }
    IsUserExists isUserExists;
    UserLoginResponse userLoginResponse;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    private SharedPreferences fcmSharedPrefrences;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_login, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        submit = view.findViewById(R.id.submit);
        skip = view.findViewById(R.id.skip);
        mPhone_number = view.findViewById(R.id.mobile_number);
        mPhone_number.setInputType(InputType.TYPE_CLASS_PHONE);
        fcmSharedPrefrences = getContext().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, Context.MODE_PRIVATE);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, "U2VjcmV0OnBhbmR0YXV0aGN8UGFzc3dvcmQ6IyRlZnJIeWhhNjQ3").commit();
        secureTokenSharedPreferenceseditor.putString(Constants.HEADER_KEY, "WSH").commit();
        secureTokenSharedPreferenceseditor.putString(Constants.BEARER, "").commit();

        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(Constants.APPID, getContext().getPackageName()).commit();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).commit();
        editor.putInt(Constants.OS_VERSION, Build.VERSION.SDK_INT).commit();
        fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPhone_number.getText().toString().isEmpty()) {
                    mPhone_number.setError(getString(R.string.enter_valid_phonenumber));
                } else if (mPhone_number.getText().toString().length() < 10) {
                    mPhone_number.setError(getString(R.string.enter_valid_phonenumber));
                } else {
                    mPhone_number.setError(null);
                    checkIfUserExists(mPhone_number.getText().toString());

                }
            }
        });
        mPhone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhone_number.setInputType(InputType.TYPE_CLASS_PHONE);
            }
        });
//        skip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Fragment fragment = new LoginUserFragment();
//                replaceFragment(fragment);
//            }
//        });
        return view;
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }

    private void checkIfUserExists(String mobile_number)
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            vidyauraAPI.checkuserExists(mobile_number,1).enqueue(new Callback<IsUserExists>() {
                @Override
                public void onResponse(Call<IsUserExists> call, Response<IsUserExists> response) {
                    hideProgress();
                    if (response.body()!=null) {
                        isUserExists = response.body();
                        if (isUserExists.status == Util.STATUS_SUCCESS) {

                            editor.putString("CheckMobileNumber",mobile_number);

                            if (isUserExists.getUserExist()!=3) {
                                editor.putInt(Constants.USERTYPE, isUserExists.getUserExist());
                                editor.putInt(Constants.USERTYPE_REAL, isUserExists.getUserExist());
                                editor.putString(Constants.PHONE, isUserExists.getPhoneNumber());
                                editor.putString(Constants.SECURITYPIN, isUserExists.getSecurityPin());
                                if (isUserExists.getUserExist()==1)
                                {
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN_TEACHER, isUserExists.getTokenTeacher()).commit();
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, isUserExists.getTokenTeacher()).commit();
                                }
                                else if (isUserExists.getUserExist()==2)
                                {
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN_TEACHER, isUserExists.getTokenParent()).commit();
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, isUserExists.getTokenParent()).commit();
                                }


                                secureTokenSharedPreferenceseditor.putString(Constants.HEADER_KEY, "authorization").commit();
                                secureTokenSharedPreferenceseditor.putString(Constants.BEARER, "Bearer ").commit();
                                if (isUserExists.getAppSettings() != null) {
                                if (isUserExists.getAppSettings().size() > 0) {
                                    editor.putInt(Constants.NOTIFICATIONSTATUS, isUserExists.getAppSettings().get(0).getNotificationStatus());
                                    editor.putInt(Constants.MESSAGESTATUS, isUserExists.getAppSettings().get(0).getMessageStatus());
                                    editor.putInt(Constants.SECURITYPINSTATUS, isUserExists.getAppSettings().get(0).getSecurityPinStatus());
                                }
                                }

                                editor.commit();
                                if (isUserExists.getSecurityPin().length() > 3) {

                                    if (isUserExists.getAppSettings() != null) {
                                        if (isUserExists.getAppSettings().size() > 0) {
                                            if (isUserExists.getAppSettings().get(0).getSecurityPinStatus() == 0) {
                                                userLogin();
                                            } else {
                                                startActivity(new Intent(getActivity(), SecurityPinActivity.class));
                                                getActivity().finish();
                                            }
                                        }
                                    } else {
                                        startActivity(new Intent(getActivity(), SecurityPinActivity.class));
                                        getActivity().finish();
                                    }
                                } else {
                                    startActivity(new Intent(getActivity(), OtpverificationActivity.class)
                                            .putExtra("phn_number", mPhone_number.getText().toString()));
                                    getActivity().finish();
                                }

                            }
                            else
                            {
                                editor.putInt(Constants.USERTYPE, isUserExists.getUserExist());
                                editor.putString(Constants.PHONE, isUserExists.getPhoneNumber());
                                editor.putString(Constants.SECURITYPIN, isUserExists.getSecurityPin());
                                editor.putInt(Constants.USERTYPE_REAL, isUserExists.getUserExist());
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN_TEACHER, isUserExists.getTokenTeacher()).commit();
                                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN_PAREENTS, isUserExists.getTokenParent()).commit();
                                    secureTokenSharedPreferenceseditor.putString(Constants.HEADER_KEY, "authorization").commit();
                                    secureTokenSharedPreferenceseditor.putString(Constants.BEARER, "Bearer ").commit();
                                if (isUserExists.getAppSettings() != null) {
                                if (isUserExists.getAppSettings().size() > 0) {
                                    editor.putInt(Constants.NOTIFICATIONSTATUS, isUserExists.getAppSettings().get(0).getNotificationStatus());
                                    editor.putInt(Constants.MESSAGESTATUS, isUserExists.getAppSettings().get(0).getMessageStatus());
                                    editor.putInt(Constants.SECURITYPINSTATUS, isUserExists.getAppSettings().get(0).getSecurityPinStatus());
                                }
                                }

                                editor.commit();
                                if (isUserExists.getSecurityPin().length() > 3) {
                                    if (isUserExists.getAppSettings() != null) {
                                        if (isUserExists.getAppSettings()
                                                .size() > 0) {
                                            if (isUserExists.getAppSettings().get(0).getSecurityPinStatus() == 0) {
                                                //userLogin();
                                                startActivity(new Intent(getActivity(), SecurityPinActivity.class));
                                                getActivity().finish();
                                            } else {
                                                startActivity(new Intent(getActivity(), SecurityPinActivity.class));
                                                getActivity().finish();
                                        /*startActivity(new Intent(getActivity(), OtpverificationActivity.class)
                                                .putExtra("phn_number", mPhone_number.getText().toString()));
                                        getActivity().finish();*/
                                            }
                                        }
                                    }
                                } else {
                                    startActivity(new Intent(getActivity(), OtpverificationActivity.class)
                                            .putExtra("phn_number", mPhone_number.getText().toString()));
                                    getActivity().finish();
                                }

                            }
                        } else {
                            Toast.makeText(getContext(), isUserExists.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<IsUserExists> call, Throwable t) {
                hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    Gson gson = new Gson();
                    System.out.println("response ==> "+gson.toJson(response.body()));
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus()!= 401) {
                        if (userLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, userLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, userLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, userLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, userLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, userLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, userLoginResponse.getData().get(0).getEmail());
                                editor.putString(Constants.GENDER, userLoginResponse.getData().get(0).getGender());
                                editor.putString(Constants.EMP_ID, userLoginResponse.getData().get(0).getEmp_id());
                                editor.putInt(Constants.ID, userLoginResponse.getData().get(0).getId());
                                editor.putString(Constants.DOB, userLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                editor.putString(Constants.JOINING_DATE, userLoginResponse.getData().get(0).getJoining_date());
                                if (userLoginResponse.getAppSettings() != null) {
                                    if (userLoginResponse.getAppSettings().size() > 0) {
                                        editor.putInt(Constants.NOTIFICATIONSTATUS, userLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                        editor.putInt(Constants.MESSAGESTATUS, userLoginResponse.getAppSettings().get(0).getMessageStatus());
                                        editor.putInt(Constants.SECURITYPINSTATUS, userLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                    }
                                }
                                editor.putInt(Constants.HASCHILDREN, userLoginResponse.getHasChildren());
                                editor.putString(Constants.SCHOOL_LOGO, userLoginResponse.getSchoolLogo());
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    editor.putInt(Constants.TOTAL_LEVEL_POINTS, userLoginResponse.getTotalLevelPoints());
                                    editor.putString(Constants.POINTS_NAME, userLoginResponse.getTeacherLevel().getName());
                                    editor.putString(Constants.BADGE_IMAGE, userLoginResponse.getTeacherLevel().getBadge_image());
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, userLoginResponse.getData().get(0).getType());
                                }
                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,userLoginResponse.getData().getPhone());
                                editor.commit();

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname()+" "+userLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+userLoginResponse.getData().get(0).getEmp_photo(),
                                        userLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                startActivity(new Intent(getActivity(), TestDrawerActivity.class));
                                getActivity().finish();
                                Log.d("device", "onResponse: " + userLoginResponse.getSecuredToken());
                            } else {
                                Toast.makeText(getContext(), userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                            userLogin();
                        }
                        } else
                        {
                            hideProgress();
                            startActivity(new Intent(getContext(), LoginActivity.class));
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}

