package com.school.teacherparent.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harini on 10/23/2018.
 */

public class HomeWorkClassWiseSubFragment extends Fragment {
    private RecyclerView recyclerView;


    public HomeWorkClassWiseSubFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_category_home, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_sub_category_homefragment);

//        mAdapter = new SubHomeClassAdapter(subhomeList);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mAdapter);


       /* mAdapter.setOnClickListen(new SubHomeClassAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddHomeWorkActivity.class);
                startActivity(intent);
            }
        });*/


        return view;

    }


}


