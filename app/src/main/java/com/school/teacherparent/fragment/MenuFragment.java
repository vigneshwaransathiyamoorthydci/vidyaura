package com.school.teacherparent.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.AttendanceActivity;
import com.school.teacherparent.activity.CircularActivity;
import com.school.teacherparent.activity.ClassworkActivity;
import com.school.teacherparent.activity.EventListActivity;
import com.school.teacherparent.activity.EventParentListActivity;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.ParentCircularActivity;
import com.school.teacherparent.activity.ParentLeaveApplyActivity;
import com.school.teacherparent.activity.ResultActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;

import javax.inject.Inject;

/**
 * Created by harini on 8/29/2018.
 */

public class MenuFragment extends BaseFragment implements View.OnClickListener {

    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    public VidyAPI vidyauraAPI;
    public Integer[] mThumbIds = {
            R.mipmap.attendance_icon, R.mipmap.leave_icon, R.mipmap.classwork_icon, R.mipmap.homework_icon,
            R.mipmap.exam_icon, R.mipmap.results_icon,
            R.mipmap.events_icon, R.mipmap.circular_icon,

    };
    int usetType;
    Menu menu;
    NotificationReceiver notificationReceiver;

    CardView cvAttendance, cvLeave, cvClasswork, cvHomework, cvExams, cvResults, cvEvents, cvCircular;
    ImageView attendanceIV,leaveIV,classworkIV,homeworkIV,examsIV,resultIV,eventIV,circularIV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        TestDrawerActivity.toolbar.setTitle("Menu");
        setHasOptionsMenu(true);
        usetType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        attendanceIV=(ImageView)view.findViewById(R.id.grid_image1);
        leaveIV=(ImageView)view.findViewById(R.id.grid_image2);
        classworkIV=(ImageView)view.findViewById(R.id.grid_image3);
        homeworkIV=(ImageView)view.findViewById(R.id.grid_image4);
        examsIV=(ImageView)view.findViewById(R.id.grid_image5);
        resultIV=(ImageView)view.findViewById(R.id.grid_image6);
        eventIV=(ImageView)view.findViewById(R.id.grid_image7);
        circularIV=(ImageView)view.findViewById(R.id.grid_image8);

        editor.putInt("ParentClassWork",0);
        editor.putInt("ParentHomeWork",0);
        editor.apply();

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (usetType==1)
        {
            attendanceIV.setImageResource(R.mipmap.attendance_icon);
            leaveIV.setImageResource(R.mipmap.leave_icon);
            classworkIV.setImageResource(R.mipmap.classwork_icon);
            homeworkIV.setImageResource(R.mipmap.homework_icon);
            examsIV.setImageResource(R.mipmap.exam_icon);
            resultIV.setImageResource(R.mipmap.results_icon);
            eventIV.setImageResource(R.mipmap.events_icon);
            circularIV.setImageResource(R.mipmap.circular_icon);
        }
        else if (usetType==2)
        {
            attendanceIV.setImageResource(R.mipmap.attendance_icon_p);
            leaveIV.setImageResource(R.mipmap.leave_icon_p);
            classworkIV.setImageResource(R.mipmap.class_icon_p);
            homeworkIV.setImageResource(R.mipmap.homework_icon_p);
            examsIV.setImageResource(R.mipmap.exam_icon_p);
            resultIV.setImageResource(R.mipmap.results_icon_p);
            eventIV.setImageResource(R.mipmap.event_icon_p);
            circularIV.setImageResource(R.mipmap.circular_icon_p);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cvAttendance = view.findViewById(R.id.cv_attendance);
        cvLeave = view.findViewById(R.id.cv_leave);
        cvClasswork = view.findViewById(R.id.cv_classwork);
        cvHomework = view.findViewById(R.id.cv_homework);
        cvExams = view.findViewById(R.id.cv_exams);
        cvResults = view.findViewById(R.id.cv_results);
        cvEvents = view.findViewById(R.id.cv_events);
        cvCircular = view.findViewById(R.id.cv_circular);

        cvAttendance.setOnClickListener(this);
        cvLeave.setOnClickListener(this);
        cvClasswork.setOnClickListener(this);
        cvHomework.setOnClickListener(this);
        cvExams.setOnClickListener(this);
        cvResults.setOnClickListener(this);
        cvEvents.setOnClickListener(this);
        cvCircular.setOnClickListener(this);

        /*GridView gridView = (GridView) view.findViewById(R.id.grid_view);
        final String[] web = getActivity().getResources().getStringArray(R.array.menu_name);
        gridView.setAdapter(new MenuAdapter(getActivity(), web, mThumbIds));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(getActivity(), AttendanceActivity.class));
                        break;
                    case 1:
                        if (usetType==1) {
                            startActivity(new Intent(getActivity(), LeaveListActivity.class));
                        }
                        else if (usetType==2)
                        {
                            startActivity(new Intent(getActivity(), ParentLeaveApplyActivity.class));
                        }
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), ClassworkActivity.class));

                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), HomeWorkActivity.class));

                        break;
                    case 4:
                          startActivity(new Intent(getActivity(), ExamsActivity.class));
                        break;
                    case 5:
                         startActivity(new Intent(getActivity(), ResultActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(getActivity(), EventListActivity.class));
                        break;
                    case 7:
                        startActivity(new Intent(getActivity(), CircularActivity.class));
                        break;
                }


            }
        });*/

        // Instance of ImageAdapter Class
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        MenuItem items = menu.findItem(R.id.action_calender);
        items.setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;
            default:
                break;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_attendance:
                startActivity(new Intent(getActivity(), AttendanceActivity.class));
                break;
            case R.id.cv_leave:
                if (usetType == 1) {
                    startActivity(new Intent(getActivity(), LeaveListActivity.class));
                } else if (usetType == 2) {
                    startActivity(new Intent(getActivity(), ParentLeaveApplyActivity.class));
                }
                break;
            case R.id.cv_classwork:
                startActivity(new Intent(getActivity(), ClassworkActivity.class));

                break;
            case R.id.cv_homework:
                startActivity(new Intent(getActivity(), HomeWorkActivity.class));

                break;
            case R.id.cv_exams:
                startActivity(new Intent(getActivity(), ExamsActivity.class));
                break;
            case R.id.cv_results:
                startActivity(new Intent(getActivity(), ResultActivity.class));
                break;
            case R.id.cv_events:
                if (usetType == 1) {
                    startActivity(new Intent(getActivity(), EventListActivity.class));
                } else if (usetType == 2) {
                    startActivity(new Intent(getActivity(), EventParentListActivity.class));
                }
                break;
            case R.id.cv_circular:
                if (usetType == 1) {
                    startActivity(new Intent(getActivity(), CircularActivity.class));
                } else if (usetType == 2) {
                    startActivity(new Intent(getActivity(), ParentCircularActivity.class));
                }
                break;
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(context.getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(context.getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
