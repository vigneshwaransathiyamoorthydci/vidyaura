package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentSyllabusFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    DiscreteScrollView dsvStudentList;
    ChildListResponseResponse childListResponseResponse;
    int count = 0;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID;
    ProgressDialog mDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_syllabus, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {

        dsvStudentList = view.findViewById(R.id.dsv_student_list);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        getStudentList();

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            Log.d("tag", "userLogin: " + sharedPreferences.getString(Constants.PHONE, "") +
                    sharedPreferences.getString(Constants.APPVERSION, "") + sharedPreferences.getString(Constants.DEVICEID, "") +
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)) + String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
                                sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
                                studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
                                getStudentClassworkList(studentID);
                            } else {
                                Toast.makeText(getActivity(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
        } else {
            dsvStudentList.scrollToPosition(0);
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentClassworkList(int selectedStudentID) {
        //Toast.makeText(getContext(),"Student ID is "+selectedStudentID,Toast.LENGTH_SHORT).show();
    }

    private void performAction() {

    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            getStudentClassworkList(parentsStudListArrayList.get(adapterPosition).getStudID());
        }
    }

}