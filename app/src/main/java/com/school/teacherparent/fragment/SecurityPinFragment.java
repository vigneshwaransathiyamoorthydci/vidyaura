package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.BuildConfig;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.EmptySecurityPinResponse;
import com.school.teacherparent.models.FeedCommentreplyListResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.IsUserExists;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.ParentsUserLoginResponse;
import com.school.teacherparent.models.SecurityPinDTO;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.lang.reflect.Field;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 10/23/2018.
 */

public class SecurityPinFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    TextView security_pin;
    EditText newpin, cnfrmpin;
    Button update;
    boolean valid = false;
    UserLoginResponse userLoginResponse;
    ParentsUserLoginResponse parentsUserLoginResponse;
    SecurityPinDTO securityPinDTO;
    SharedPreferences secureTokenSharedPreferences;
    TextView forgot_securitypin;
    private SharedPreferences fcmSharedPrefrences;
    public SecurityPinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_changepin, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getContext().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, Context.MODE_PRIVATE);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        security_pin = view.findViewById(R.id.security_pin);
        newpin = view.findViewById(R.id.new_pin);
        cnfrmpin = view.findViewById(R.id.confrm_pin);
        forgot_securitypin = view.findViewById(R.id.forgot_securitypin);
        forgot_securitypin.setVisibility(View.GONE);
        update = view.findViewById(R.id.update);

        newpin.setInputType(InputType.TYPE_CLASS_PHONE);
        cnfrmpin.setInputType(InputType.TYPE_CLASS_PHONE);


        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(Constants.APPID, getContext().getPackageName()).commit();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).commit();
        editor.putInt(Constants.OS_VERSION, Build.VERSION.SDK_INT).commit();
        fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        if (sharedPreferences.getString(Constants.SECURITYPIN, "").length() > 0) {
            security_pin.setText(getString(R.string.Entersecuritypin));
            update.setText(getString(R.string.ok));
            newpin.setHint(getString(R.string.securitypin));
            cnfrmpin.setVisibility(View.GONE);
            forgot_securitypin.setVisibility(View.VISIBLE);
        }


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getString(Constants.SECURITYPIN, "").length() > 0) {
                    String securityPin = newpin.getText().toString();
                    if (securityPin.length() == 0 || securityPin.length() < 3) {
                        newpin.setError(getString(R.string.PleaseEnterValidPin));
                    } else {
//                        if (securityPin.equals(sharedPreferences.getString(Constants.SECURITYPIN, ""))) {
//                            userLogin();
//                        } else {
//                            newpin.setError(getString(R.string.PleaseEnterValidPin));
//                            forgot_securitypin.setVisibility(View.VISIBLE);
//                        }
                        CheckSecuritypin(securityPin);
                    }
                } else if (validate()) {
                    createSecurityPin(newpin.getText().toString());
                }

//                if (sharedPreferences.getString(Constants.SECURITYPIN,null)!=null)
//                {
//                    String newpn = newpin.getText().toString();
//                    if (newpn.equals(sharedPreferences.getString(Constants.SECURITYPIN,null))) {
//                        startActivity(new Intent(getActivity(), TestDrawerActivity.class));
//                    }
//                    else
//                    {
//                        newpin.setError(getString(R.string.PleaseEnterValidPin));
//                    }
//
//                }
//                else {
//                    if (validate()) {
//                        createSecurityPin(newpin.getText().toString());
//                        //startActivity(new Intent(getActivity(), TestDrawerActivity.class));
//                        //startActivity(new Intent(getActivity(), TestDrawerActivity.class));
//
//
//                    }
//                }
//                startActivity(new Intent(getActivity(), TestDrawerActivity.class));
            }
        });

        forgot_securitypin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotSecuritypin();
            }
        });
        return view;
    }

    public boolean validate() {
        String newpn = newpin.getText().toString();
        String cnfrmpn = cnfrmpin.getText().toString();

        if (newpn.length() == 0 || newpn.length() < 4) {
            newpin.setError(getString(R.string.PleaseEnterValidPin));
            valid = false;
        } else if (cnfrmpn.length() == 0 || cnfrmpn.length() < 4) {
            newpin.setError(getString(R.string.PleaseEnterValidPin));
            valid = false;
        } else if (!newpn.equals(cnfrmpn)) {
            cnfrmpin.setError(getString(R.string.Pindoesntmatch));
            valid = false;
        } else {
            newpin.setError(null);
            cnfrmpin.setError(null);
            valid = true;
        }
        return valid;
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }

    private void createSecurityPin(String securityPin) {
        if (Util.isNetworkAvailable()) {
//            Log.d("VIKIS","PHONE"+sharedPreferences.getString(Constants.PHONE,""));
//            Log.d("VIKIS","USERTYPE"+sharedPreferences.getInt(Constants.USERTYPE,0));
//            Log.d("VIKIS","securityPin"+securityPin);
            showProgress();
            vidyauraAPI.createSecutiryPin(String.valueOf(sharedPreferences.getString(Constants.PHONE, "")),
                    String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)), String.valueOf(securityPin),
                    sharedPreferences.getInt(Constants.USERTYPE_REAL,0)).enqueue(new Callback<SecurityPinDTO>() {
                @Override
                public void onResponse(Call<SecurityPinDTO> call, Response<SecurityPinDTO> response) {
                    hideProgress();
                    securityPinDTO = response.body();
                    if (securityPinDTO.getStatus() == Util.STATUS_SUCCESS) {
                        Toast.makeText(getContext(), securityPinDTO.getMessage(), Toast.LENGTH_SHORT).show();
                        editor.putString(Constants.SECURITYPIN, securityPinDTO.getSecurityPin()).commit();

                        int usetype=sharedPreferences.getInt(Constants.USERTYPE, 0);
                        if (usetype==1)
                        {
                            userLogin();
                        }
                        else if (usetype==2)
                        {
                            parentsuserLogin();
                        }


                    } else {
                        Toast.makeText(getContext(), securityPinDTO.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<SecurityPinDTO> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void ForgotSecuritypin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            vidyauraAPI.forgetSecutiyPin(sharedPreferences.getString(Constants.PHONE, "")
                    , String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0))
                    , String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0))).enqueue(new Callback<EmptySecurityPinResponse>() {
                @Override
                public void onResponse(Call<EmptySecurityPinResponse> call, Response<EmptySecurityPinResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        EmptySecurityPinResponse emptySecurityPinResponse = response.body();
                        if (emptySecurityPinResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            if (emptySecurityPinResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                sharedPreferences.edit().clear().commit();
                                Intent i = new Intent(getActivity(), LoginActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                getActivity().finishAffinity();
                            } else {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    }

                }

                @Override
                public void onFailure(Call<EmptySecurityPinResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void CheckSecuritypin(String securityPin) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            //forgot_securitypin.setVisibility(View.GONE);
            vidyauraAPI.checkSecurityPin(sharedPreferences.getString(Constants.PHONE, "")
                    , String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0))
                    , securityPin).enqueue(new Callback<EmptySecurityPinResponse>() {
                @Override
                public void onResponse(Call<EmptySecurityPinResponse> call, Response<EmptySecurityPinResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        EmptySecurityPinResponse emptySecurityPinResponse = response.body();
                        if (emptySecurityPinResponse.getStatus()!= 404) {
                        if (emptySecurityPinResponse.getStatus()!= 401) {
                        if (emptySecurityPinResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            if (emptySecurityPinResponse.getStatus() == Util.STATUS_SUCCESS) {
                               // Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                int usetype=sharedPreferences.getInt(Constants.USERTYPE, 0);
                                if (usetype==1)
                                {
                                    userLogin();
                                }
                                else if (usetype==2)
                                {
                                    parentsuserLogin();
                                }

                            } else {
                                Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //newpin.setError(getString(R.string.PleaseEnterValidPin));
                                forgot_securitypin.setVisibility(View.VISIBLE);
                            }
                        } else {
                            //Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            //startActivity(i);
                            //getActivity().finishAffinity();
                            hideProgress();
                            startActivity(new Intent(getContext(),LoginActivity.class));

                        }
                        } else
                        {
                            hideProgress();
                            startActivity(new Intent(getContext(),LoginActivity.class));
                        }
                        } else {
                            Toast.makeText(getContext(), emptySecurityPinResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            sharedPreferences.edit().clear().commit();
                            Intent i = new Intent(getContext(), LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }

                }

                @Override
                public void onFailure(Call<EmptySecurityPinResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });
        } else

        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    if (userLoginResponse != null) {
                        Gson gson = new Gson();
                        System.out.println("response ==> "+gson.toJson(response.body()));
                        if (userLoginResponse.getStatus()!= 401) {
                            if (userLoginResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                                if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                    // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                    editor.putString(Constants.SECURITYPIN, userLoginResponse.getData().get(0).getSecurityPin());
                                    editor.putString(Constants.USERID, userLoginResponse.getEncyptedUserID());
                                    editor.putInt(Constants.SCHOOLID, userLoginResponse.getData().get(0).getSchool_id());
                                    editor.putString(Constants.FNAME, userLoginResponse.getData().get(0).getFname());
                                    editor.putString(Constants.LNAME, userLoginResponse.getData().get(0).getLname());
                                    editor.putString(Constants.EMAIL, userLoginResponse.getData().get(0).getEmail());
                                    editor.putString(Constants.GENDER, userLoginResponse.getData().get(0).getGender());
                                    editor.putString(Constants.EMP_ID, userLoginResponse.getData().get(0).getEmp_id());
                                    editor.putInt(Constants.ID, userLoginResponse.getData().get(0).getId());
                                    editor.putString(Constants.DOB, userLoginResponse.getData().get(0).getDob());
                                    editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                    editor.putString(Constants.JOINING_DATE, userLoginResponse.getData().get(0).getJoining_date());
                                    if (userLoginResponse.getAppSettings() != null) {
                                        if (userLoginResponse.getAppSettings().size() > 0) {
                                            editor.putInt(Constants.NOTIFICATIONSTATUS, userLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                            editor.putInt(Constants.MESSAGESTATUS, userLoginResponse.getAppSettings().get(0).getMessageStatus());
                                            editor.putInt(Constants.SECURITYPINSTATUS, userLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                        }
                                    }
                                    editor.putInt(Constants.HASCHILDREN, userLoginResponse.getHasChildren());
                                    editor.putString(Constants.SCHOOL_LOGO, userLoginResponse.getSchoolLogo());
                                    if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                        editor.putInt(Constants.TOTAL_LEVEL_POINTS, userLoginResponse.getTotalLevelPoints());
                                        editor.putString(Constants.POINTS_NAME, userLoginResponse.getTeacherLevel().getName());
                                        editor.putString(Constants.BADGE_IMAGE, userLoginResponse.getTeacherLevel().getBadge_image());
                                    } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                        editor.putString(Constants.TYPE, userLoginResponse.getData().get(0).getType());
                                    }
                                    //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getSecuredToken()).commit();
                                    //editor.putString(Constants.PHONE,userLoginResponse.getData().getPhone());
                                    editor.commit();

                                    String url = null;
                                    if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                        url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/";
                                    } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                        url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                                        ;
                                    }

                                    final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                    MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname() + " " + userLoginResponse.getData().get(0).getLname()).replace("null", "")
                                            , url + userLoginResponse.getData().get(0).getEmp_photo(),
                                            userLoginResponse.getEncyptedUserID(),
                                            FirebaseInstanceId.getInstance().getToken(),
                                            "senderRole",
                                            "senderClass");
                                    mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                    startActivity(new Intent(getActivity(), TestDrawerActivity.class));
                                    getActivity().finish();
                                    Log.d("device", "onResponse: " + userLoginResponse.getSecuredToken());
                                } else {
                                    Toast.makeText(getContext(), userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                hideProgress();
                                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                                userLogin();
                            }

                        } else
                            {
                                hideProgress();
                                startActivity(new Intent(getContext(),LoginActivity.class));
                            }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }





    private void parentsuserLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.parentsuserLogin(userLoginParam).enqueue(new Callback<ParentsUserLoginResponse>() {
                @Override
                public void onResponse(Call<ParentsUserLoginResponse> call, Response<ParentsUserLoginResponse> response) {
                    hideProgress();
                    parentsUserLoginResponse = response.body();
                    if (parentsUserLoginResponse != null) {
                        if (parentsUserLoginResponse.getStatus()!= 401) {
                        if (parentsUserLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (parentsUserLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, parentsUserLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, parentsUserLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, parentsUserLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, parentsUserLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, parentsUserLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, parentsUserLoginResponse.getData().get(0).getEmail());
                                //editor.putString(Constants.SCHOOL_LOGO, parentsUserLoginResponse.getSchoolLogo());
                                //editor.putString(Constants.GENDER, parentsUserLoginResponse.getData().get(0).getGender());
                                //editor.putString(Constants.EMP_ID, parentsUserLoginResponse.getData().get(0).getEmp_id());
                                //editor.putString(Constants.DOB, parentsUserLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.PROFILE_PHOTO, parentsUserLoginResponse.getData().get(0).getPhoto());
                                if (parentsUserLoginResponse.getAppSettings() != null) {
                                    if (parentsUserLoginResponse.getAppSettings().size() > 0) {
                                        editor.putInt(Constants.NOTIFICATIONSTATUS, parentsUserLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                        editor.putInt(Constants.MESSAGESTATUS, parentsUserLoginResponse.getAppSettings().get(0).getMessageStatus());
                                        editor.putInt(Constants.SECURITYPINSTATUS, parentsUserLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                    }
                                }

                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, parentsUserLoginResponse.getData().get(0).getType());
                                }

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((parentsUserLoginResponse.getData().get(0).getFname()+" "+parentsUserLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+parentsUserLoginResponse.getData().get(0).getPhoto(),
                                        parentsUserLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(parentsUserLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, parentsUserLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,parentsUserLoginResponse.getData().getPhone());
                                editor.commit();
                                startActivity(new Intent(getActivity(), TestDrawerActivity.class));
                                getActivity().finish();
                                Log.d("device", "onResponse: " + parentsUserLoginResponse.getSecuredToken());
                            } else {
                                Toast.makeText(getContext(), parentsUserLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, parentsUserLoginResponse.getToken()).commit();
                            parentsuserLogin();
                        }} else
                        {
                            hideProgress();
                            startActivity(new Intent(getContext(),LoginActivity.class));
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ParentsUserLoginResponse> call, Throwable t) {
                    hideProgress();
                    System.out.println("error ==> "+t.getMessage());
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}

