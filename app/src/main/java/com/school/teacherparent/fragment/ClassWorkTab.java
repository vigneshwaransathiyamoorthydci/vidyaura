package com.school.teacherparent.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClassWorkTab extends Fragment {


    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView noti;
    private ImageView back;
    private boolean doubleBackToExitPressedOnce = false;
    @Inject
    SharedPreferences sharedPreferences;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_class_work_tab, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getActivity().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setCurrentItem(1);

        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());

        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        if (userType == 1) {
            adapter.addFragment(new ClassWorkFragment(), "Classwork");
            adapter.addFragment(new SyllabusFragment(), "Syllabus");
        } else if (userType == 2) {
            //adapter.addFragment(new ParentClassWorkFragment(), "Classwork");
            //adapter.addFragment(new ParentSyllabusFragment(), "Syllabus");
        } else {

        }
        //   adapter.addFragment(new SubClassFragment(),"English");
        viewPager.setAdapter(adapter);


    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
