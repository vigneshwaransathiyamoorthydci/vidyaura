package com.school.teacherparent.fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AllGalleryAdapter;
import com.school.teacherparent.adapter.CustomPagerAdapter;
import com.school.teacherparent.adapter.PhotosGalleryAdapter;
import com.school.teacherparent.adapter.PhotosandVideoGalleryAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamUpcomingResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GalleryParams;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.Photoslist;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPhotosandVideoGalleryFragment extends BaseFragment {
    private GridView gridView;
    private RecyclerView recyclerview;
    private PhotosandVideoGalleryAdapter photosandVideoGalleryAdapter;
    private EditText edit;
    private List<PhotosGalleryDTO> gridList=new ArrayList<>();
    private FloatingActionButton addResult;
    LinearLayout lin;
    protected static final int GALLERY_PICTURE = 1;
    protected static final int CAMERA_REQUEST = 0;
    private Intent pictureActionIntent = null;
    private static int RESULT_LOAD_IMG = 1;
    private int GALLERY=1;
    private static final String IMAGE_DIRECTORY = "/demonuts";

    int limit=5;
    int offset=0;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public boolean isLoading = true;

    ArrayList<GalleryListResponse.albumList> gallerylists=new ArrayList<>();
    String title;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_allphotosandgallery_gallery, container, false);
        recyclerview = (RecyclerView) view.findViewById(R.id.recycler);

        Bundle b = getArguments();
        gallerylists= (  ArrayList<GalleryListResponse.albumList> ) b.getSerializable("albumlist");

        SidemenuDetailActivity.title.setText(getResources().getString(R.string.gallery));
        //VidyauraApplication.getContext().getComponent().inject(this);
        photosandVideoGalleryAdapter=new PhotosandVideoGalleryAdapter(gallerylists,getContext());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),4);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(photosandVideoGalleryAdapter);

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        photosandVideoGalleryAdapter.setOnClickListen(new PhotosandVideoGalleryAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                initiatepopupwindow(gallerylists.get(position).getMedia_path(),gallerylists.get(position).getMedia_type(),position);

            }
        });
        return view;
    }

    private void initiatepopupwindow(String mediapath, String media_type, int position) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd    video_selected=alertLayout.findViewById(R.id.video_selected);;

        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(getActivity(),gallerylists,2));
        viewPager.setCurrentItem(position);

        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);

        final AlertDialog dialog = alert.create();
        dialog.show();

        if (gallerylists.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                JzvdStd.releaseAllVideos();
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                dialog.dismiss();
            }
        });
        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                viewPager.setCurrentItem(viewPager.getCurrentItem()-1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JzvdStd.releaseAllVideos();
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true); //getItem(-1) for previous
            }
        });

        /*if (media_type.equals("png")) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/" + mediapath;
            Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
//                            Bitmap bitmap = drawable.getBitmap();
//                            int bitmapwidth=0;
//                            int bitmpheight=0;
//                            bitmapwidth=bitmap.getWidth();
//                            bitmpheight=bitmap.getHeight();
//                            LinearLayout.LayoutParams params=(LinearLayout.LayoutParams)imageView.getLayoutParams();
//                            if (bitmapwidth==0||bitmpheight==0){
//                                return;
//                            }
//                            int newwidth=imageView.getWidth();
//                            int newheight=newwidth*bitmpheight/bitmapwidth;
//                            params.height=newheight;
//                            imageView.setLayoutParams(params);
//
//                        }
//                        catch (Exception e){
//
//                        }
//
//                    }
//                },1000);
                }

                @Override
                public void onError(Exception e) {
                    imageView.setImageResource(R.mipmap.school);
                }
            });
        }
        else
        {
            video_selected.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);

            video_selected.setUp((getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                           mediapath)
                    , Jzvd.SCREEN_WINDOW_NORMAL, "");
            video_selected.setSystemTimeAndBattery();

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
            requestOptions.error(R.mipmap.icon_video_thumbnail);
            Glide.with(getContext())
                    .load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                           mediapath)
                    .apply(requestOptions)
                    .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                           mediapath))
                    .into(video_selected.thumbImageView);
        }*/

        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }





}

