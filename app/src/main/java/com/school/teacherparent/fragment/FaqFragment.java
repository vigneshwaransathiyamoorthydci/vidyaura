package com.school.teacherparent.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.FaqAdapter;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FaqResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/25/2018.
 */

public class FaqFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    RecyclerView  faqRecyclerView;
    LinearLayoutManager linearLayoutManager;
    FaqAdapter faqAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.faq));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        faqRecyclerView=(RecyclerView)view.findViewById(R.id.faqRecyclerView);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        faqRecyclerView.setItemAnimator(new DefaultItemAnimator());
        faqRecyclerView.setLayoutManager(linearLayoutManager);

        getfaq();
        return view;
    }
    public void getfaq()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            vidyauraAPI.getFAQDetails(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0))).enqueue(new Callback<FaqResponse>() {
                @Override
                public void onResponse(Call<FaqResponse> call, Response<FaqResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        FaqResponse feedListResponse = response.body();

                        if (feedListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                ArrayList<FaqResponse.getFAQDetails> getFAQDetails = feedListResponse.getGetFAQDetails();

                                if (getFAQDetails.size() != 0) {
                                    faqAdapter = new FaqAdapter(getFAQDetails,getContext());
                                    faqRecyclerView.setAdapter(faqAdapter);

                                } else {
                                    Toast.makeText(getContext(), feedListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                hideProgress();
                                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedListResponse.getToken()).commit();
                                getfaq();

                            }

                        } else {
                            Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                        }
                    }
                }



                @Override
                public void onFailure(Call<FaqResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
        }
}
