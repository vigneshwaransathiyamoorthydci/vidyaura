package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.MyClassAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.MyclassesResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyclassesFragment extends BaseFragment {


    private RecyclerView myclassesrecyclerview;
    private MyClassAdapter myclassesAdapter;


    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    LinearLayoutManager linearLayoutManager;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    private List<MyclassesResponse.classesProfileDetails> classList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_student, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        myclassesrecyclerview=(RecyclerView)view.findViewById(R.id.studentfragment_recycle);

        myclassesAdapter = new MyClassAdapter(classList,getContext());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        myclassesrecyclerview.setItemAnimator(new DefaultItemAnimator());
        myclassesrecyclerview.setLayoutManager(linearLayoutManager);
        myclassesrecyclerview.setAdapter(myclassesAdapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible){
            classList.clear();

            if (isLoading) {
                getMyclasses();
                isLoading=false;
            }

        }



    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){
            classList.clear();
            if (isLoading) {
                getMyclasses();
                isLoading=false;
            }
        }


    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


    public void getMyclasses() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading=false;
            FeedListParams feedListParams = new FeedListParams();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            Gson gson = new Gson();
            String input = gson.toJson(feedListParams);
            vidyauraAPI.getTeachersClassesProfile(feedListParams).enqueue(new Callback<MyclassesResponse>() {
                @Override
                public void onResponse(Call<MyclassesResponse> call, Response<MyclassesResponse> response) {
                    hideProgress();
                    isLoading=true;
                    if (response.body() != null) {
                        MyclassesResponse data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getClassesProfileDetails() != null &&
                                        response.body().getClassesProfileDetails().size() != 0) {
                                    myclassesrecyclerview.setVisibility(View.VISIBLE);
                                    classList.addAll(data.getClassesProfileDetails());
                                    myclassesAdapter.notifyDataSetChanged();
                                    isLoading = true;
                                } else {
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    isLoading = false;
                                }
                            } else {
                                if (!isLoading){
                                    Toast.makeText(getContext(), data.getStatusText(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<MyclassesResponse> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

}
