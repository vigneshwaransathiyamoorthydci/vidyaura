package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.HomeworkAdapter;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.R;
import com.school.teacherparent.models.HomeworkList;
import com.school.teacherparent.models.HomeworkListParms;
import com.school.teacherparent.models.HomeworkListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubHomeFragment extends BaseFragment {


    private RecyclerView homeworkRecyclerview;

    
    private HomeworkAdapter homeworkAdapter;



    SwipeRefreshLayout homeworkswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public static boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int limit=5;
    int offset=0;
    LinearLayoutManager linearLayoutManager;
    BaseActivity baseActivity;
    HomeworkListResponse homeworkListResponse;
    ArrayList<HomeworkList> homeworkLists;
   public ArrayList<HomeworkListResponse.homeworksList> homeworkArrayList;
    public ArrayList<HomeworkListResponse.homeworksList> homeworkArrayListadd;
    HomeWorkFragment homeWorkFragment;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView selectedDateTextview;
    String formattedDate;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        currentPage = 0;
        lastEnd = false;
        if (isVisible)
        {
            homeworkArrayListadd.clear();
            homeworkAdapter.notifyDataSetChanged();
            gethomeworkList();
        }
            //gethomeworkList();
            Log.d("VIKIS","SubHomeFragment-START");



    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        isVisible = isVisibleToUser;
//        if (isVisible && isStarted) {
//            isStarted = true;
//            currentPage = 0;
//            if (isLoading)
//            {
//                gethomeworkList();
//            }
//                Log.d("VIKIS","SubHomeFragment-VISIBLE");
//                //gethomeworkList();
//
//
//        }
//    }
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        isVisible = visible;
        if (isVisible && isStarted) {
            isStarted = true;
            currentPage = 0;
            if (isLoading)
            {
                homeworkArrayListadd.clear();
                homeworkAdapter.notifyDataSetChanged();
                gethomeworkList();
            }
            Log.d("VIKIS","SubHomeFragment-VISIBLE");
            //gethomeworkList();


        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_home, container, false);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        homeworkRecyclerview = (RecyclerView) view.findViewById(R.id.recycle_sub_home);
        homeworkArrayList=new ArrayList<HomeworkListResponse.homeworksList>();
        homeworkArrayListadd=new ArrayList<>();
        homeworkLists=new ArrayList<>();
        //gethomeworkList();
        baseActivity = (BaseActivity) getActivity();

        homeworkAdapter = new HomeworkAdapter(homeworkArrayList,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        homeworkRecyclerview.setItemAnimator(new DefaultItemAnimator());
        homeworkRecyclerview.setLayoutManager(linearLayoutManager);
        homeworkRecyclerview.setAdapter(homeworkAdapter);

        homeworkswipelayout=view.findViewById(R.id.swipe_homework);

        homeworkswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                homeworkswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                            homeworkArrayListadd.clear();
                            homeworkAdapter.notifyDataSetChanged();
                            gethomeworkList();
                            homeworkswipelayout.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });

//        homeworkAdapter.setOnClickListen(new HomeworkAdapter.AddTouchListen() {
//            @Override
//            public void OnTouchClick(int position) {
//                String homeWorkID = "";
//
//                for (int j = 0; j < homeworkArrayList.get(position).getClasswiseHomework().size(); j++) {
//                    homeWorkID = homeWorkID.concat(String.valueOf((homeworkArrayList.get(position).getClasswiseHomework().get(j).getHomeworkID())).concat(","));
//                }
//                homeWorkID = homeWorkID.substring(0, homeWorkID.length() - 1);
//                for (int i = 0; i < homeworkArrayList.get(position).getClasswiseHomework().size(); i++) {
////
////                    context.startActivity(new Intent(context, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
////                            .putExtra("classname", context.getString(R.string.class_name) + " " + homeworkList.getClasswiseHomework().get(i).getClassName()
////                                    + homeworkList.getClasswiseHomework().get(i).getSection()).putExtra("homeWorkID", homeWorkID)
////                            .putExtra("classID", homeworkList.getClassID()).putExtra("sectionID", homeworkList.getSectionID()
////                            ).putExtra("subjectID", homeworkList.getSubjectID()));
//
//                   HomeworkDetailsFragment fragment = new HomeworkDetailsFragment();
//                    Bundle bundles3 = new Bundle();
//                    bundles3.putString("className", getString(R.string.class_name) + " " + homeworkArrayList.get(position)
//                            .getClasswiseHomework().get(i).getClassName()
//                            + homeworkArrayList.get(position).getClasswiseHomework().get(i).getSection());
//                    bundles3.putString("homeWorkID",homeWorkID);
//                    bundles3.putInt("classID", homeworkArrayList.get(position).getClassID());
//                    bundles3.putInt("sectionID",homeworkArrayList.get(position).getSectionID());
//                    bundles3.putInt("subjectID",homeworkArrayList.get(position).getSubjectID());
//                    fragment.setArguments(bundles3);
//                    replaceFragment(fragment);
//
//
//                }
//            }
//        });
        //gethomeworkList();


        return view;

    }

    private void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_homework, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void gethomeworkList()
    {

        Log.d("Testworkcalling", "gethomeworkList: ");
        if (Util.isNetworkAvailable())
        {
            showProgress();
            HomeworkListParms homeworkListParms=new HomeworkListParms();
            homeworkListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            homeworkListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            homeworkListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            homeworkListParms.setLimit(limit);
            homeworkListParms.setOffset(offset);
            homeworkListParms.setClassImage(Constants.CLASSIMAGE);
            isLoading=false;
            vidyauraAPI.getHomeworkList(homeworkListParms).enqueue(new Callback<HomeworkListResponse>() {
                @Override
                public void onResponse(Call<HomeworkListResponse> call, Response<HomeworkListResponse> response) {
                    hideProgress();
                    isLoading=true;
                    if (response.body() != null) {
                        homeworkListResponse = response.body();
                        if (homeworkListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            homeworkArrayList = response.body().getHomeworksList();
                            HashMap Showtimingmap = null;
                            if (homeworkListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (homeworkArrayList.size() != 0) {
                                  //  isLoading=false;
                                    homeworkArrayListadd.addAll(homeworkArrayList);
                                    homeworkAdapter = new HomeworkAdapter(homeworkArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));
                                    homeworkRecyclerview.setAdapter(homeworkAdapter);
//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
//                                    for (int y = 0; y < homeworkArrayList.size(); y++)
//                                    {
//
//                                    }
                                    //homeworkAdapter.notifyDataSetChanged();


//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
                                }
//                                else if (eventLists.size() != 0 && eventsListArrayList.size() != 0) {
//                                    noeventListTextview.setVisibility(View.VISIBLE);
//                                    eventrecycle.setVisibility(View.GONE);
//                                    loading = false;
//                                }
                            } else {

                                Toast.makeText(getContext(), homeworkListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    }
                }
                @Override
                public void onFailure(Call<HomeworkListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


}
