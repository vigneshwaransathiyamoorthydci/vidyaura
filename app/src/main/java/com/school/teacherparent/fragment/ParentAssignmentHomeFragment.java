package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.AssignmentMainAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AssignmentListResponse;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.HomeworkList;
import com.school.teacherparent.models.HomeworkListParms;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.HomeWorkFragment.selected;
import static com.school.teacherparent.fragment.HomeWorkFragment.selectedSchool;

public class ParentAssignmentHomeFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    TextView selectedDateTextview,tvNoresult;
    SwipeRefreshLayout swipeHomeAssignment;
    RecyclerView rvHomeAssignment;
    ChildListResponseResponse childListResponseResponse;
    int count = 0;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID;
    ProgressDialog mDialog;
    int limit=10;
    int offset=0;
    LinearLayoutManager linearLayoutManager;
    BaseActivity baseActivity;
    AssignmentListResponse homeworkListResponse;
    ArrayList<HomeworkList> homeworkLists;
    public ArrayList<AssignmentListResponse.assignmentsList> homeworkArrayList;
    public static ArrayList<AssignmentListResponse.assignmentsList> assignmentArrayListadd = new ArrayList<>();
    private AssignmentMainAdapter assignmentMainAdapter;
    public static boolean isLoading = true;
    private Boolean isVisible = false;
    private Boolean isStarted = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_assignment_home, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {

        selectedDateTextview = view.findViewById(R.id.selectedDateTextview);
        swipeHomeAssignment = view.findViewById(R.id.swipe_home_assignment);
        rvHomeAssignment = view.findViewById(R.id.rv_home_assignment);
        tvNoresult = view.findViewById(R.id.tv_noresult);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        homeworkArrayList=new ArrayList<AssignmentListResponse.assignmentsList>();
        assignmentArrayListadd =new ArrayList<AssignmentListResponse.assignmentsList>();
        homeworkLists=new ArrayList<>();
        baseActivity = (BaseActivity) getActivity();
        assignmentMainAdapter = new AssignmentMainAdapter(assignmentArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvHomeAssignment.setItemAnimator(new DefaultItemAnimator());
        rvHomeAssignment.setLayoutManager(linearLayoutManager);
        rvHomeAssignment.setAdapter(assignmentMainAdapter);

    }

    private void performAction() {
        swipeHomeAssignment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeHomeAssignment.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        assignmentArrayListadd.clear();
                        assignmentMainAdapter.notifyDataSetChanged();
                        getAssignmentList();
                        swipeHomeAssignment.setRefreshing(false);

                    }
                }, 000);
            }
        });
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getAssignmentList() {

        if (Util.isNetworkAvailable())
        {
            assignmentArrayListadd.clear();
            showProgressDialog();
            HomeworkListParms homeworkListParms=new HomeworkListParms();
            homeworkListParms.setSchoolID(String.valueOf(selectedSchool()));
            homeworkListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            homeworkListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            homeworkListParms.setLimit(100);
            homeworkListParms.setOffset(offset);
            homeworkListParms.setClassImage(Constants.CLASSIMAGE);
            homeworkListParms.setStudID(selected());
            Gson gson = new Gson();
            System.out.println("reponse for asm sent ==> "+gson.toJson(homeworkListParms));
            isLoading = false;
            vidyauraAPI.getAssignmentList(homeworkListParms).enqueue(new Callback<AssignmentListResponse>() {
                @Override
                public void onResponse(Call<AssignmentListResponse> call, Response<AssignmentListResponse> response) {
                    mDialog.dismiss();
                    isLoading = true;
                    if (response.body() != null) {
                        homeworkListResponse = response.body();
                        if (homeworkListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            homeworkArrayList = response.body().getAssignmentsList();
                            HashMap Showtimingmap = null;
                            if (homeworkListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (homeworkArrayList.size() != 0) {
                                    assignmentArrayListadd.addAll(homeworkArrayList);
                                    Gson gson = new Gson();
                                    System.out.println("reponse for asm ==> "+gson.toJson(assignmentArrayListadd));
                                    swipeHomeAssignment.setVisibility(View.VISIBLE);
                                    tvNoresult.setVisibility(View.GONE);
                                    assignmentMainAdapter = new AssignmentMainAdapter(assignmentArrayListadd,getContext(),baseActivity,sharedPreferences.getInt(Constants.USERTYPE,0));
                                    rvHomeAssignment.setAdapter(assignmentMainAdapter);
                                    //  isLoading=false;
//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
//                                    for (int y = 0; y < homeworkArrayList.size(); y++)
//                                    {
//
//                                    }
                                    //homeworkAdapter.notifyDataSetChanged();


//                                    eventrecycle.setVisibility(View.VISIBLE);
//                                    noeventListTextview.setVisibility(View.GONE);
                                }
//                                else if (eventLists.size() != 0 && eventsListArrayList.size() != 0) {
//                                    noeventListTextview.setVisibility(View.VISIBLE);
//                                    eventrecycle.setVisibility(View.GONE);
//                                    loading = false;
//                                }
                            } else {
                                swipeHomeAssignment.setVisibility(View.GONE);
                                tvNoresult.setVisibility(View.VISIBLE);
                                tvNoresult.setText(homeworkListResponse.getMessage());
                                //Toast.makeText(getContext(), homeworkListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    }
                }
                @Override
                public void onFailure(Call<AssignmentListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading = true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else

        {
            isLoading = true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible){

            if (isLoading) {
                getAssignmentList();
                isLoading=false;
            }

        }



    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){



            if (isLoading) {
                offset=0;
                getAssignmentList();
                isLoading=false;
            }
        }


    }


    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        isLoading=false;
    }

}