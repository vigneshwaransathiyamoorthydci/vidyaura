package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AttendanceListParams;
import com.school.teacherparent.models.AttendanceListResponseforParents;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.MyPagerAdapter;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceforParentFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    ChildListResponseResponse childListResponseResponse;
    int count=0;
    ArrayList<ChildListResponseResponse.parentsStudList>  parentsStudListArrayList;

    //private ViewPager viewpagerTop, viewPagerBackground;
    public static final int ADAPTER_TYPE_TOP = 1;
    public static final int ADAPTER_TYPE_BOTTOM = 2;
    int classID,studentID,sectionID,studentSchoolID;
    AttendanceListResponseforParents attendanceListResponse;
    String formattedDate;
    MaterialCalendarView materialCalendarView;
    TextView totalpercentageTextview,acedmicyearTextview;
    ImageView backImageview;
    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialog;
    ImageView noti;
    NotificationReceiver notificationReceiver;
    ArrayList arrayListholoday = new ArrayList();
    ArrayList absentArralist = new ArrayList();
    ArrayList leaveArralist = new ArrayList();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_attendancelistforparents, container, false);

        TestDrawerActivity.toolbar.setTitle(getString(R.string.attendance));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        totalpercentageTextview=view.findViewById(R.id.totalpercentageTextview);
        parentsStudListArrayList=new ArrayList<>();
        materialCalendarView = view.findViewById(R.id.material_calender);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        materialCalendarView.setCurrentDate(Calendar.getInstance());
        materialCalendarView.getCalendarMode();
        new ArrayList().add(new CalendarDay());

        noti=(ImageView) view.findViewById(R.id.noti);
        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });

      //  this.materialCalendarView.addDecorator(new DayDecorator());
        acedmicyearTextview=view.findViewById(R.id.labeltextview);
        this.materialCalendarView.addDecorator(new AbsentDecorators());
        materialCalendarView.addDecorator(new EventDecorator(getActivity().getResources().getColor(R.color.colorPrimary), CalendarDay.today()));
        materialCalendarView.clearSelection();
        materialCalendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_NONE);
        materialCalendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {


                formattedDate = getdateConvert(String.valueOf(calendarDay.getDate()));
                if (studentID>0) {
                    acedmicyearTextview.setText(getString(R.string.attendancefotheyear)+" "+getyearfromdate(formattedDate));
                    totalpercentageTextview.setText("");
                    materialCalendarView.removeDecorators();
                    getattendanceList();
                }

            }
        });

        backImageview = view.findViewById(R.id.backImageview);
        backImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        getStudentList();

        return view;
    }



    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));

            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body()!=null)
                    {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size()>0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                //leaveheaderConstraintlayout.setVisibility(View.VISIBLE);
                                init();
                                setupViewPager();

                                Date c = Calendar.getInstance().getTime();
                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                getattendanceList();
                            }
                            else
                            {
                                Toast.makeText(getActivity(),childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }}
                            else
                            {
                                Toast.makeText(getActivity(),childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }


                        }
                        else
                        {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();

                            getStudentList();
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }



    private void init() {
        /*viewpagerTop = (ViewPager)getView().findViewById(R.id.viewpagerTop);
        viewPagerBackground = (ViewPager)getView(). findViewById(R.id.viewPagerbackground);

        viewpagerTop.setClipChildren(false);
        viewpagerTop.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.pager_margin));
        viewpagerTop.setOffscreenPageLimit(3);
        viewpagerTop.setPageTransformer(false, new CarouselEffectTransformer(getActivity())); // Set transformer*/
    }

    /**
     * Setup viewpager and it's events
     */
    private void setupViewPager() {

        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID=childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID=childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID= childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID= childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID=childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID=childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID= childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID= childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());

        // Set Top ViewPager Adapter
        //MyPagerAdapter adapter = new MyPagerAdapter(getActivity(), count, ADAPTER_TYPE_TOP,parentsStudListArrayList);
        //viewpagerTop.setAdapter(adapter);

        // Set Background ViewPager Adapter
        //MyPagerAdapter adapterBackground = new MyPagerAdapter(getActivity(), count, ADAPTER_TYPE_BOTTOM,parentsStudListArrayList);
        //viewPagerBackground.setAdapter(adapterBackground);

        /*viewpagerTop.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int index = 0;

            @Override
            public void onPageSelected(int position) {
                index = position;
                System.out.println("onPageSelected ==> "+parentsStudListArrayList.get(index).getStudID());
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int width = viewPagerBackground.getWidth();
                viewPagerBackground.scrollTo((int) (width * position + width * positionOffset), 0);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPagerBackground.setCurrentItem(index);
                    Log.d("VIGNESH","waran");
                    classID=parentsStudListArrayList.get(index).getClass_id();
                    sectionID= parentsStudListArrayList.get(index).getSection_id();
                    studentID= parentsStudListArrayList.get(index).getStudID();
                    totalpercentageTextview.setText("");
                    materialCalendarView.removeDecorators();
                    getattendanceList();
                    //getLeaveHistory(parentsStudListArrayList.get(index).getStudID());

                }

            }
        });*/
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            totalpercentageTextview.setText("");
            materialCalendarView.removeDecorators();
            getattendanceList();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getattendanceList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgressDialog();
            AttendanceListParams attendanceListParams=new AttendanceListParams();
            attendanceListParams.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(studentSchoolID));
            attendanceListParams.setSessionType(2);
            attendanceListParams.setAttendanceDate(formattedDate);
            attendanceListParams.setStudID(studentID);

            arrayListholoday.clear();
            absentArralist.clear();
            leaveArralist.clear();

            //attendancerecyclerView.setVisibility(View.VISIBLE);
            //attendanceListArrayList.clear();
            vidyauraAPI.getAttendanceListforParents(attendanceListParams).enqueue(new Callback<AttendanceListResponseforParents>() {
                @Override
                public void onResponse(Call<AttendanceListResponseforParents> call, Response<AttendanceListResponseforParents> response) {
                    mDialog.dismiss();
                    //hideProgress();
                    // isLoading=true;
                    if (response.body() != null) {
                        attendanceListResponse = response.body();
                        if (attendanceListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            // attendanceListArrayList = response.body().getAttendanceList();
                            if (attendanceListResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (attendanceListResponse.getAttendanceList().size() != 0) {
                                    acedmicyearTextview.setText(getString(R.string.attendancefotheyear)+" "+getyearfromdate(formattedDate));
                                    CalendarDay calendarDay = null;
                                    for (int y=0;y<attendanceListResponse.getAttendanceList().size();y++) {
                                        try {
                                            calendarDay = new CalendarDay(new SimpleDateFormat("yyyy-MM-dd").parse(attendanceListResponse.getAttendanceList().get(y).getCheckLeaveDate()));
                                            if(attendanceListResponse.getAttendanceList().get(y).getIsHoliday().equals("0"))
                                            {
                                                    if (attendanceListResponse.getAttendanceList().get(y).getIsLeaveOrAbsent().equals("0"))
                                                    {
                                                        absentArralist.add(calendarDay);
                                                    }
                                                    else
                                                    {
                                                        leaveArralist.add(calendarDay);
                                                    }
                                            }
                                            else
                                            {
                                                arrayListholoday.add(calendarDay);
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    materialCalendarView.addDecorator(new HolidayDecorator(arrayListholoday));
                                    materialCalendarView.addDecorator(new AbsentDecorator(absentArralist));
                                    materialCalendarView.addDecorator(new LeaveDecorator(leaveArralist));
                                    materialCalendarView.addDecorator(new EventDecorator(getActivity().getResources().getColor(R.color.colorPrimary), CalendarDay.today()));
                                    totalpercentageTextview.setText(attendanceListResponse.getAttendancePercentage()+" "+"%");

                                } else {
                                    //hideProgress();
                                    //attendancerecyclerView.setVisibility(View.GONE);
                                    mDialog.dismiss();
                                    Toast.makeText(getContext(), attendanceListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                //hideProgress();
                                mDialog.dismiss();
                                Toast.makeText(getContext(), attendanceListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            //hideProgress();
                            mDialog.dismiss();
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();


                        }
                    }
                }
                @Override
                public void onFailure(Call<AttendanceListResponseforParents> call, Throwable t) {
                    //hideProgress();
                   // isLoading=true;
                    mDialog.dismiss();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
          //  isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class AbsentDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public AbsentDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.mipmap.absent_bg_icon));
            view.addSpan(new ForegroundColorSpan(-1));
        }

    }

    public class AbsentDecorators implements DayViewDecorator {
        public boolean shouldDecorate(CalendarDay calendarDay) {
            return Boolean.parseBoolean(null);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.pdlg_color_yellow)));
        }
    }

    public class PresentDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public PresentDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new DotSpan(15.0f, getActivity().getResources().getColor(R.color.blue_grey_500)));
        }
    }

    public class HolidayDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public HolidayDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.mipmap.holiday_bg_icon));
            view.addSpan(new ForegroundColorSpan(-1));
        }



    }

    public class LeaveDecorator implements DayViewDecorator {
        private HashSet<CalendarDay> dates;

        public LeaveDecorator(ArrayList<CalendarDay> arrayList) {
            this.dates = new HashSet(arrayList);
        }

        public boolean shouldDecorate(CalendarDay calendarDay) {
            return this.dates.contains(calendarDay);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.mipmap.leave_bg_icon));
            view.addSpan(new ForegroundColorSpan(-1));
        }
    }


    public class DayDecorator implements DayViewDecorator {
        private final Calendar calendar = Calendar.getInstance();

        public boolean shouldDecorate(CalendarDay calendarDay) {
            calendarDay.copyTo(this.calendar);
            return this.calendar.get(Calendar.DAY_OF_WEEK) == 1;
        }

        public void decorate(DayViewFacade dayViewFacade) {
            dayViewFacade.addSpan(new ForegroundColorSpan(getActivity().getResources().getColor(R.color.gray)));
        }
    }

    public class EventDecorator implements DayViewDecorator {

        private final int color;
        private final CalendarDay today;

        public EventDecorator(int color, CalendarDay today) {
            this.color = color;
            this.today = today;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return day.equals(today);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.setBackgroundDrawable(getActivity().getResources().getDrawable(R.mipmap.today_bg_icon));
            view.addSpan(new ForegroundColorSpan(-1));
        }
    }


    public void clickEvent(View view) {
        switch (view.getId()) {
            case R.id.linMain:
                if (view.getTag() != null) {
                    int poisition = Integer.parseInt(view.getTag().toString());
                    //Toast.makeText(getApplicationContext(), "Poistion: " + poisition, Toast.LENGTH_LONG).show();
                    //viewpagerTop.setCurrentItem(poisition);

//                    Intent intent=new Intent(this,FullScreenActivity.class);
//                    intent.putExtra(EXTRA_IMAGE,listItems[poisition]);
//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view.findViewById(R.id.imageCover), EXTRA_TRANSITION_IMAGE);
//                    ActivityCompat.startActivity(this, intent, options.toBundle());
                }
                break;
        }
    }

    public String getdateConvert(String currentda) {
        DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = (Date) formatter.parse(currentda);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String formatedDate = outputFormat.format(date);
        System.out.println("formatedDate : " + formatedDate);
        return formatedDate;
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
