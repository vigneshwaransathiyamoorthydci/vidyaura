package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SubClassAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ClassWorkDetailsParms;
import com.school.teacherparent.models.ClassworkDetailsModel;
import com.school.teacherparent.models.ClassworkLists;
import com.school.teacherparent.models.FeedListParams;

import com.school.teacherparent.R;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.FontTextViewBold;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubClassFragment extends BaseFragment {


    private RecyclerView recyclerView;
    private SubClassAdapter mAdapter;
    private List<ClassworkDetailsModel.ClassworkDetail> subclassList = new ArrayList<>();
    FloatingActionButton addClasswork;
    ImageView noti, back,calendarImageview;
    Fragment fragment;
    String classWrkid,clasid,sectionid,subjectid;
    FontTextViewBold classname;
    String formattedDate;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public SubClassFragment() {
        // Required empty public constructor
    }
    BaseActivity baseActivity;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView selectedDateTextview;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_class, container, false);
        noti = view.findViewById(R.id.noti);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        baseActivity = (BaseActivity) getActivity();
        Bundle bundle = this.getArguments();
        back = view.findViewById(R.id.back);
        classname= view.findViewById(R.id.classname);
        classWrkid=getArguments().getString("classwrkid");
        clasid=getArguments().getString("classid");
        sectionid=getArguments().getString("sectionid");
        subjectid=getArguments().getString("subjectid");
String classs = "Class "+getArguments().getString("classname").replace("Class Class","Class");
        classname.setText(classs.replace("Class Class","Class"));
        calendarImageview=(ImageView)view.findViewById(R.id.calendarImageview);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getActivity().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new ClassWorkTab();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_sub_class);
        addClasswork = view.findViewById(R.id.add_classwork);

        addClasswork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
            }
        });
        selectedDateTextview=(TextView)view.findViewById(R.id.selectedDateTextview);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(date);
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        selectedDateTextview.setText(getString(R.string.today));

        calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                String today = formt.format(c);
                                if (formattedDate.equals(today)) {
                                    selectedDateTextview.setText("Today");
                                } else {
                                    selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                }

                                    subclassList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    getClassworkList();



                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();



            }
        });
        mAdapter = new SubClassAdapter(subclassList,getContext(),baseActivity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
      //  Analdata();
        //getClassworkList();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;




        if (isLoading)
        {
            subclassList.clear();
            isLoading = false;
            getClassworkList();
        }







    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            isStarted = true;
            if (isLoading)
            {
                subclassList.clear();
                isLoading = false;
                getClassworkList();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    public void getClassworkList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ClassWorkDetailsParms feedListParams = new ClassWorkDetailsParms();
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            feedListParams.setClassworkID(classWrkid);
            feedListParams.setClassID(clasid);
            feedListParams.setSectionID(sectionid);
            feedListParams.setSubjectID(subjectid);
            feedListParams.setCompletedDate(formattedDate);
            feedListParams.setClassImage(Constants.CLASSIMAGE);

            Gson gson = new Gson();
            String input = gson.toJson(feedListParams);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getClassworkDetails(feedListParams).enqueue(new Callback<ClassworkDetailsModel>() {
                @Override
                public void onResponse(Call<ClassworkDetailsModel> call, Response<ClassworkDetailsModel> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                        ClassworkDetailsModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getClassworkDetails() != null &&
                                        response.body().getClassworkDetails().size() != 0) {

                                    recyclerView.setVisibility(View.VISIBLE);
                                    subclassList.addAll(data.getClassworkDetails());
                                    mAdapter.notifyDataSetChanged();
                                    //loading = true;
                                } else {
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                   // loading = false;
                                }
                            } else {
                                Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        isLoading = true;
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ClassworkDetailsModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

//    private void Analdata() {
//
//        SubClassDTO s = new SubClassDTO();
//        s.setTitle("English");
//        s.setSubtitle("My mother at sixty two");
//        s.setContent("Topic:2.4");
//        s.setStatus1("Lorium ipsum dolar sit amet,constructor eclipsing elid, sed do elusmud");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
//        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
//        subclassList.add(s);
//
//
//        s = new SubClassDTO();
//        s.setTitle("Science");
//        s.setSubtitle("Anatomy of Human Beings");
//        s.setContent("Topic:6.4");
//        s.setStatus1("Human beings skeletal system and collections of Red Blood cells and White Blood cells");
//        s.setImg(String.valueOf(R.mipmap.maths_icon));
//        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
//        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
//        subclassList.add(s);
//
//        s = new SubClassDTO();
//        s.setTitle("Maths");
//        s.setSubtitle("Trignomentry Formulae");
//        s.setStatus1("Trignometry Formulae of sin,cos,tan theta and describe and solve pythagoras theorem");
//        s.setContent("Topic:4");
//        s.setImg(String.valueOf(R.mipmap.english_icon));
//        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
//        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
//        subclassList.add(s);
//
//
//        s = new SubClassDTO();
//        s.setTitle("Social");
//        s.setSubtitle("Indian Constitutional");
//        s.setStatus1("Indian constitution of economy and welfare of Indian");
//        s.setContent("Topic:8");
//        s.setImg(String.valueOf(R.mipmap.maths_icon));
//        s.setSubimg1(String.valueOf(R.drawable.bg_overlay));
//        s.setSubimg2(String.valueOf(R.drawable.bg_overlay));
//        subclassList.add(s);
//
//
//    }
    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
