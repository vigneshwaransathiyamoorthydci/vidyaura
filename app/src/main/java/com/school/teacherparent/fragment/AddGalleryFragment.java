package com.school.teacherparent.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.CircularComposeclassAdpater;
import com.school.teacherparent.adapter.PhotosandVideoGalleryAdapter;
import com.school.teacherparent.adapter.SelectedImageAdapter;
import com.school.teacherparent.adapter.SelectedImageandVideoAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AddGalleryParams;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.Photoslist;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SelectedImageandVideoList;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddGalleryFragment extends BaseFragment implements OnImageRemoved {

    RecyclerView galleryRecyclerview;
    TextView add_Gallery,feedtitleTextview;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    String albumName;
    /////////////////////////////////
    LinearLayout privatevisiblityLinearlayout;
    String classID = "";
    TextView classnameTextview,searchnameTextview;
    List<ListofClass> classList = new ArrayList<ListofClass>();
    List<ListofClass> searchclassList = new ArrayList<ListofClass>();
    private ArrayAdapter<String> multiSpinneradapter;
    boolean[] selectedclassItems;
    CircularComposeclassAdpater circularComposeclassAdpater;
    ListView classlistView;
    Dialog classDialog;
    Button dialogDistrictButton;
    SearchView dialogSearchView;
    /////////////////////////////////

    List<SelectedImageandVideoList> imageList = new ArrayList<>();
    SelectedImageandVideoAdapter selectedImageAdapter;
    String imgname = "";
    LinearLayout cardview_upload,uploadvideo_linear;
    int visibility = 0;
    Spinner feedvisiblityspinner;
    List<SelectedImageList> videoList = new ArrayList<>();
    JzvdStd videoSelected;
    ImageView image_videodelete;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_gallery, container, false);
        galleryRecyclerview = (RecyclerView) view.findViewById(R.id.recycle_grid);
        add_Gallery=(TextView)view.findViewById(R.id.add_gallery);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        Bundle b = getArguments();
        albumName= b.getString("albumName");
        SidemenuDetailActivity.title.setText(albumName);
        feedtitleTextview=(TextView)view.findViewById(R.id.feedtitleTextview);
        selectedImageAdapter = new SelectedImageandVideoAdapter(imageList, getActivity(), this);
        //LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),5);
        galleryRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        galleryRecyclerview.setAdapter(selectedImageAdapter);
        cardview_upload = view.findViewById(R.id.upload_linear);
        uploadvideo_linear = view.findViewById(R.id.uploadvideo_linear);
        feedtitleTextview.setText(albumName);
        ///////////////////////////////////////////
        privatevisiblityLinearlayout=view.findViewById(R.id.privatevisiblityLinearlayout);
        privatevisiblityLinearlayout.setVisibility(View.GONE);
        classnameTextview=view.findViewById(R.id.classnameTextview);
        multiSpinneradapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item);
        selectedclassItems = new boolean[multiSpinneradapter.getCount()];

        classDialog = new Dialog(getContext());
        classDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        classDialog.setCancelable(false);
        classDialog.setContentView(R.layout.custom_dialog);
        classlistView = (ListView) classDialog.findViewById(R.id.listView1);
        searchnameTextview=(TextView)classDialog.findViewById(R.id.searchnameTextview);
        classlistView = (ListView) classDialog.findViewById(R.id.listView1);
        dialogDistrictButton = (Button) classDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)classDialog.findViewById(R.id.dialogSearchView);
        classnameTextview=view.findViewById(R.id.classnameTextview);
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        dialogSearchView.setQueryHint(getString(R.string.enter_class));
        classnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classDialog.show();
            }
        });
        dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String className="";
                classID="";
                for (int y=0;y<classList.size();y++)
                {

                    if (classList.get(y).getSelectedID()!=0) {
                        className = className.concat(String.valueOf(classList.get(y).getClassname()+classList.get(y).getClasssection()).concat(","));
                        classID=classID.concat(String.valueOf(classList.get(y).getClasssectionid())).concat(",");
                    }

                }
                if (className.length()>0 && classID.length()>0) {
                    className = className.substring(0, className.length() - 1);
                    classID = classID.substring(0, classID.length() - 1);
                    classnameTextview.setText(className);
                    classnameTextview.setError(null);

                }
                else
                {
                    classnameTextview.setText("");
                }
                classDialog.hide();
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1)
                {
//                    Collections.sort(classList, new Comparator<ListofClass>() {
//
//                        @Override
//                        public int compare(ListofClass lhs, ListofClass rhs) {
//                            //here getTitle() method return app name...
//                            return newText.compareTo(lhs.getClassname());
//
//                        }
//
//
//                    });
                    searchclassList.clear();
                    for(int y=0;y<classList.size();y++)
                    {
                        if (classList.get(y).getClassname().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchclassList.add(classList.get(y));
                            Log.d("SEARCH","classLIST"+classList.get(y).getClassname());
                        }
                    }
                    if (searchclassList.size()>0) {
                        circularComposeclassAdpater = new CircularComposeclassAdpater(searchclassList, getContext());
                        classlistView.setAdapter(circularComposeclassAdpater);
                        circularComposeclassAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        classlistView.setVisibility(View.GONE);
                        searchnameTextview.setVisibility(View.VISIBLE);
                    }

                }
                else if (newText.toString().length()==0)
                {
                    classlistView.setVisibility(View.VISIBLE);
                    searchnameTextview.setVisibility(View.GONE);
                    circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                    classlistView.setAdapter(circularComposeclassAdpater);
                    circularComposeclassAdpater.notifyDataSetChanged();
                }
                return false;
            }
        });
        ////////////////////////////////////////////
        videoSelected=(JzvdStd)view.findViewById(R.id.video_selected);
        image_videodelete=(ImageView)view.findViewById(R.id.image_videodelete);
        image_videodelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoList.clear();
                videoSelected.setVisibility(View.INVISIBLE);
                image_videodelete.setVisibility(View.INVISIBLE);
            }
        });
        add_Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageList.size()<=0){
                    Toast.makeText(getActivity(), R.string.attachmedia, Toast.LENGTH_SHORT).show();

                }
                else
                {
                    ((BaseActivity) getActivity()).uploadImageandVideo(imageList, getString(R.string.s3_feeds_path),
                            imgname, new OnAmazonFileuploaded() {
                                @Override
                                public void FileStatus(int status, String filename) {
                                    if (status == 1) {
                                                addGallery();
                                    } else {
                                        Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                }
            }
        });
        cardview_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageList.size()>5){
                    Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .scale(500,500)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });
        uploadvideo_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageList.size()>5){
                    Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                videoChooser();
                /*new VideoPicker.Builder(getActivity())
                        .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                        .directory(VideoPicker.Directory.DEFAULT)
                        .extension(VideoPicker.Extension.MP4)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Name", "Public");
        map.put("Icon", R.mipmap.visiblity_icon);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("Name", "Private");
        map.put("Icon", R.mipmap.privacy_icon);
        list.add(map);
        feedvisiblityspinner = view.findViewById(R.id.feedvisiblityspinner);

        myAdapter adapter = new myAdapter(getActivity(), list,
                R.layout.spinner_dropdown_item_image, new String[]{"Name", "Icon"},
                new int[]{R.id.name, R.id.icon});

        feedvisiblityspinner.setAdapter(adapter);
        feedvisiblityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                visibility = position;
                if (visibility==1) {
                    getClassList();
                }
                else if (visibility==0)
                {
                    classID="";
                    classnameTextview.setText("");
                    privatevisiblityLinearlayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void getClassList(){
        if (Util.isNetworkAvailable()) {
            showProgress();
            classList.clear();
            GetclassListParams getclassListParams=new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        ClassListResponse classListResponse=response.body();
                        if (classListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i=0;i<classListResponse.getTeachersClassesList().size();i++)
                                {
                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
                                            classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
                                            classListResponse.getTeachersClassesList().get(i).getSectionID(),false,0));
                                    multiSpinneradapter.add(  classListResponse.getTeachersClassesList().get(i).getClassName()+" "+
                                            classListResponse.getTeachersClassesList().get(i).getSection());
                                }
                                privatevisiblityLinearlayout.setVisibility(View.VISIBLE);
                                circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                                classlistView.setAdapter(circularComposeclassAdpater);


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });


        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    private void videoChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VideoPicker.Builder(getActivity())
                        .mode(VideoPicker.Mode.CAMERA)
                        .directory(VideoPicker.Directory.DEFAULT)
                        .extension(VideoPicker.Extension.MP4)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VideoPicker.Builder(getActivity())
                        .mode(VideoPicker.Mode.GALLERY)
                        .directory(VideoPicker.Directory.DEFAULT)
                        .extension(VideoPicker.Extension.MP4)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {

        for (int i=0;i<path.size();i++)
        {
            imageList.add(new SelectedImageandVideoList(path.get(i),true,"image"));
        }
        if (imageList.size() > 5) {
            Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }


    }

    public void setPickedVideoDetails(Bitmap bitmap, List<String> path) {

       // videoList.clear();
        for (int i=0;i<path.size();i++)
        {
            File file = new File(path.get(i));
            int file_size = Integer.parseInt(String.valueOf(file.length()/1024));

            Log.d("BBBBBBBBBBB","TEsss"+file_size);
            imageList.add(new SelectedImageandVideoList(path.get(i),true,"video"));
        }

        if (imageList.size() > 5) {
            Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }

//        videoSelected.setVisibility(View.VISIBLE);
//        image_videodelete.setVisibility(View.VISIBLE);
//        //VideoPath =videoList.get(0);
//        videoSelected.setUp(String.valueOf(Uri.fromFile(new File(videoList.get(0).getImage())))
//                , Jzvd.SCREEN_WINDOW_NORMAL, "");
////        Glide.with(this)
////                .load(Uri.fromFile(new File(String.valueOf(Uri.fromFile(new File(videoList.get(0).getImage()))))))
////                .into(videoSelected.thumbImageView);

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else return null;
    }
    @Override
    public void onimageremoved(int pos, View view) {
        if (view.getId() == R.id.close) {
            imageList.remove(pos);
            selectedImageAdapter.notifyItemRemoved(pos);
        }
    }

    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        String video = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("albumAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("albumAttachmentExtension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }

        for (int y=0;y<videoList.size();y++)
        {
            video = videoList.get(y).getImage();

            if (imageList.size()<=0)
            {
                obj = new JSONObject();
            }

            try {
                obj.put("albumAttachment",video.substring(video.lastIndexOf("/")+1,video.length()));
                String extension = getExt(video);
                obj.put("albumAttachmentExtension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }

//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }

    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
    private void addGallery() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddGalleryParams addFeedParams = new AddGalleryParams();
            addFeedParams.setAlbumName(feedtitleTextview.getText().toString().trim());
            addFeedParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            addFeedParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            addFeedParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            addFeedParams.setVisibility(String.valueOf(visibility));
            addFeedParams.setAlbumAttachment(getImagelist());
            addFeedParams.setSectionIDs(classID);
            Gson gson=new Gson();
            String input=gson.toJson(addFeedParams);


            vidyauraAPI.addGallery(addFeedParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {

                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                                sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                                sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                                sendNotification.setType("galleries");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            getActivity().finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private class myAdapter extends SimpleAdapter {

        public myAdapter(Context context, List<? extends Map<String, ?>> data,
                         int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.spinner_dropdown_item_image,
                        null);
            }

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            ((TextView) convertView.findViewById(R.id.name))
                    .setText((String) data.get("Name"));
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageResource((Integer) data.get("Icon"));

            return convertView;
        }

    }




}

