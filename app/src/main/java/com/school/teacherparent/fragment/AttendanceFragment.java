package com.school.teacherparent.fragment;


import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AttendanceAdapter;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.R;
import com.school.teacherparent.models.AttendanceList;
import com.school.teacherparent.models.AttendanceListParams;
import com.school.teacherparent.models.AttendanceListResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends BaseFragment {


    private RecyclerView attendancerecyclerView;
    private AttendanceAdapter attendanceAdapter;
    FloatingActionButton add_attendance;
    ImageView back;

    SwipeRefreshLayout attendanceswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    AttendanceListResponse attendanceListResponse;
    //public ArrayList<AttendanceListResponse.attendanceList> attendanceListArrayList=new ArrayList<>();
    public ArrayList<AttendanceList> attendanceListArrayList=new ArrayList<>();
    ImageView calendarImageview;
    TextView dateTextview;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate;
    LinearLayoutManager linearLayoutManager;
    ImageView noti;
    NotificationReceiver notificationReceiver;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        currentPage = 0;
        lastEnd = false;
        if (isVisible) {
//            getTimeLineList();
//            if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1) {
//                getDistrictList();
//            }
//            getNotificationCount();
            if (isLoading)
            {
                isLoading=false;
                attendanceListArrayList.clear();
                attendanceAdapter.notifyDataSetChanged();
                getattendanceList();
            }
        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            currentPage = 0;
            if (isLoading)
            {
                isLoading=false;
                attendanceListArrayList.clear();
                attendanceAdapter.notifyDataSetChanged();
                getattendanceList();
            }
//            getTimeLineList();
//            if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1) {
//                getDistrictList();
//            }
//            getNotificationCount();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        attendancerecyclerView = (RecyclerView) view.findViewById(R.id.attendance_fragment_recycle);
        calendarImageview=(ImageView)view.findViewById(R.id.calendarImageview);
        dateTextview=(TextView)view.findViewById(R.id.dateTextview);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
         formattedDate = df.format(c);
        dateTextview.setText(getString(R.string.today));
        noti=(ImageView) view.findViewById(R.id.noti);
        final Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(),NotificationActivity.class));
            }
        });
        calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                /*final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);*/

                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                               // dateTextview.setText(formattedDate);

                                String today = formt.format(c);
                                if (formattedDate.equals(today)) {
                                    dateTextview.setText("Today");
                                } else {
                                    dateTextview.setText(getDateFormatforatten(formattedDate));
                                }

                                getattendanceList();


                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            }
        });

        add_attendance = view.findViewById(R.id.add_attendance);
        add_attendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
            }
        });
        attendanceswipelayout=view.findViewById(R.id.swipe_attendance);
        back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });


        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        attendancerecyclerView.setItemAnimator(new DefaultItemAnimator());
        attendancerecyclerView.setLayoutManager(linearLayoutManager);
        attendanceAdapter = new AttendanceAdapter(attendanceListArrayList,getContext());
        attendancerecyclerView.setAdapter(attendanceAdapter);


        attendancerecyclerView.setItemAnimator(new DefaultItemAnimator());

        attendanceAdapter.OnsetClickListen(new AttendanceAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                Fragment fragment = new SubAttendanceFragment();
                Bundle attendanceDetails=new Bundle();
                attendanceDetails.putInt("classID",attendanceListArrayList.get(position).getClassID());
                attendanceDetails.putInt("sectionID",attendanceListArrayList.get(position).getSectionID());
                attendanceDetails.putString("attendanceDATE",formattedDate);
                attendanceDetails.putString("className",attendanceListArrayList.get(position).getIconName()+" "+attendanceListArrayList.get(position).getSection());
                attendanceDetails.putInt("sessionTYPE",1);
                attendanceDetails.putString("total",attendanceListArrayList.get(position).getTotalStudentsCount());
                attendanceDetails.putString("present",attendanceListArrayList.get(position).getStudentsPresentCount());
                attendanceDetails.putString("leave",attendanceListArrayList.get(position).getStudentsAbsentCount());
                attendanceDetails.putString("classnameTitle",attendanceListArrayList.get(position).getClassName()+" "+attendanceListArrayList.get(position).getSection());
                attendanceDetails.putString("classImage",attendanceListArrayList.get(position).getClass_image());
                attendanceDetails.putInt("attendanceSessionCount",attendanceListArrayList.get(position).getAttendanceSessionCount());
                attendanceDetails.putInt("groupID",attendanceListArrayList.get(position).getGroup_id());
                attendanceDetails.putBoolean("isGroup",attendanceListArrayList.get(position).isGroupAttendance());
                attendanceDetails.putString("groupName",attendanceListArrayList.get(position).getIconName());
                fragment.setArguments(attendanceDetails);
                replaceFragment(fragment);
            }

        });

       // getattendanceList();
        attendanceswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                attendanceswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Date c = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        formattedDate = df.format(c);
                        dateTextview.setText(getString(R.string.today));
                            getattendanceList();
                            attendanceswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isLoading)
        {
            isLoading=false;
            attendanceListArrayList.clear();
            attendanceAdapter.notifyDataSetChanged();
            getattendanceList();
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_attendance, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }


    public void getattendanceList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AttendanceListParams attendanceListParams=new AttendanceListParams();
            attendanceListParams.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            attendanceListParams.setSessionType(2);
            attendanceListParams.setAttendanceDate(formattedDate);
            attendanceListParams.setClassImage(Constants.CLASSIMAGE);
            attendancerecyclerView.setVisibility(View.VISIBLE);
            attendanceListArrayList.clear();
            vidyauraAPI.getAttendanceList(attendanceListParams).enqueue(new Callback<AttendanceListResponse>() {
                @Override
                public void onResponse(Call<AttendanceListResponse> call, Response<AttendanceListResponse> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                        attendanceListResponse = response.body();
                        if (attendanceListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            // attendanceListArrayList = response.body().getAttendanceList();
                            if (attendanceListResponse.getStatus() == Util.STATUS_SUCCESS) {

                                /*if (attendanceListResponse.getAttendanceList().size() != 0) {*/
//                                    feeds_recycle.setVisibility(View.VISIBLE);
//                                    nofeedListTextview.setVisibility(View.GONE);

                                    //attendanceListArrayList.addAll(attendanceListResponse.getAttendanceList());

                                    if (attendanceListResponse.getAttendanceList() != null) {
                                        if (attendanceListResponse.getAttendanceList().size() > 0) {
                                            for (int i = 0; i < attendanceListResponse.getAttendanceList().size(); i++) {
                                                attendanceListArrayList.add(new AttendanceList(
                                                        false,
                                                        attendanceListResponse.getAttendanceList().get(i).getClassID(),
                                                        attendanceListResponse.getAttendanceList().get(i).getClassName(),
                                                        attendanceListResponse.getAttendanceList().get(i).getSectionID(),
                                                        attendanceListResponse.getAttendanceList().get(i).getSection(),
                                                        attendanceListResponse.getAttendanceList().get(i).getTotalStudentsCount(),
                                                        attendanceListResponse.getAttendanceList().get(i).getStudentsPresentCount(),
                                                        attendanceListResponse.getAttendanceList().get(i).getStudentsAbsentCount(),
                                                        attendanceListResponse.getAttendanceList().get(i).getIconName(),
                                                        attendanceListResponse.getAttendanceList().get(i).getClass_image(),
                                                        attendanceListResponse.getAttendanceList().get(i).getAttendanceSessionCount(),
                                                        attendanceListResponse.getAttendanceList().get(i).getTodaysAbsenteesList(),
                                                        0,
                                                        ""
                                                ));
                                            }
                                        }
                                    }

                                    if (attendanceListResponse.getGroupattendanceList() != null) {
                                        if (attendanceListResponse.getGroupattendanceList().size() > 0) {
                                            for (int j = 0; j < attendanceListResponse.getGroupattendanceList().size(); j++) {
                                                attendanceListArrayList.add(new AttendanceList(
                                                        true,
                                                        0,
                                                        "",
                                                        0,
                                                        "",
                                                        attendanceListResponse.getGroupattendanceList().get(j).getTotalStudentsCount(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getStudentsPresentCount(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getStudentsAbsentCount(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getIconName(),
                                                        "",
                                                        attendanceListResponse.getGroupattendanceList().get(j).getAttendanceSessionCount(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getTodaysAbsenteesList(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getGroup_id(),
                                                        attendanceListResponse.getGroupattendanceList().get(j).getGroupimage()
                                                ));
                                            }
                                        }
                                    }

                                    if (attendanceListArrayList.size() > 0) {
                                        attendanceAdapter.notifyDataSetChanged();
                                    } else {
                                        Toast.makeText(getContext(), attendanceListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                                    /*}*/
//                                else if (feedList.size()!=0 && feedListResponsearray.size()!=0)
//                                {
//                                    nofeedListTextview.setVisibility(View.VISIBLE);
//                                    feeds_recycle.setVisibility(View.GONE);
//                                    loading=false;
//                                }
                                /*} else {
                                    hideProgress();
                                    attendancerecyclerView.setVisibility(View.GONE);
                                    Toast.makeText(getContext(), attendanceListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                }*/
                            } else {
                                hideProgress();
                                Toast.makeText(getContext(), attendanceListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                                //i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                //startActivity(i);
                                //getActivity().finishAffinity();

                            }

                        } else {
                            hideProgress();
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                            //Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                        }
                    }
                }

                @Override
                public void onFailure(Call<AttendanceListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
