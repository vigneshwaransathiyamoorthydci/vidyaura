package com.school.teacherparent.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection;
import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.DiaryAdapter;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.adapter.LeaveAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.calender.DayDateMonthYearModel;
import com.school.teacherparent.calender.HorizontalCalendarListener;
import com.school.teacherparent.calender.HorizontalCalendarView;
import com.school.teacherparent.models.DiaryListResponse;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.models.TimetableParms;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.inject.Inject;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.prolificinteractive.materialcalendarview.MaterialCalendarView.SELECTION_MODE_RANGE;
import static com.school.teacherparent.calender.HorizontalCalendarView.tv_month;

/**
 * Created by harini on 8/29/2018.
 */

public class DiaryFragment extends BaseFragment implements HorizontalCalendarListener {
    RecyclerView diary_recycle;
    DiaryAdapter diaryAdapter;
    MaterialCalendarView materialCalendarView;
    List<String> list;
    FabSpeedDial fabSpeedDial;

    SwipeRefreshLayout diaryswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    int Calendarmode=1;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private List<DiaryListResponse.diaryDetailsList> diaryDetailsLists = new ArrayList<>();
    private List<DiaryListResponse.diaryDetailsList> diaryDetailsListssearch = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    String formattedDate;
    Calendar calendar;
    HorizontalCalendarView hcvCalender;
    String selectedCalenderDate;
    TextView calendardayTextview;
    int userType;
    Menu menu;
    NotificationReceiver notificationReceiver;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {

        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            currentPage = 0;

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    Date c;
    DiaryListResponse data;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        MenuItem item = menu.findItem(R.id.action_calender);
        item.setVisible(false);
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_diary, container, false);
        TestDrawerActivity.toolbar.setTitle(getString(R.string.diary));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        setHasOptionsMenu(true);
        /*FlexibleCalendar viewCalendar = (FlexibleCalendar) view.findViewById(R.id.calendar);
        Calendar cal = Calendar.getInstance();
        CalendarAdapter adapter = new CalendarAdapter(getActivity(), cal);
        viewCalendar.setAdapter(adapter);*/
        diary_recycle = view.findViewById(R.id.diary_recycle);
       // diaryswipelayout=view.findViewById(R.id.swipe_diary);
        hcvCalender = view.findViewById(R.id.hcv_calender);
        materialCalendarView = view.findViewById(R.id.material_calender);
        calendardayTextview=view.findViewById(R.id.calendardayTextview);
        /*materialCalendarView.state().edit().setFirstDayOfWeek(1).
                setMinimumDate(CalendarDay.from(2018, 4, 1)).
                setMaximumDate(CalendarDay.from(2050, 12, 12)).
                setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        materialCalendarView.setCurrentDate(Calendar.getInstance());
        materialCalendarView.getCalendarMode();*/
         c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        diary_recycle.setItemAnimator(new DefaultItemAnimator());
        diary_recycle.setLayoutManager(linearLayoutManager);
        diaryAdapter = new DiaryAdapter(diaryDetailsLists, getActivity());
        diary_recycle.setAdapter(diaryAdapter);
        TestDrawerActivity.edittextleaveSearch.setInputType(0);
        TestDrawerActivity.edittextleaveSearch.setEnabled(false);
        /*materialCalendarView.setSelectionMode(1);
        materialCalendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2018, 1, 1))
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();*/
        calendar = Calendar.getInstance();
        hcvCalender.setContext(this);
        //materialCalendarView.setSelectedDate(CalendarDay.today());
        //materialCalendarView.addDecorator(new DayDecorator());
        final Calendar calendar = Calendar.getInstance();
        materialCalendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2018, 1, 1))
                .setMaximumDate(CalendarDay.from(2050, 1, 1))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();
        materialCalendarView.setSelectedDate(calendar.getTime());
        calendardayTextview.setText("Today");
        //materialCalendarView.setOnTouchListener(null);
        //materialCalendarView.setBackgroundColor(Color.alpha(Color.WHITE));

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hcvCalender.setVisibility(View.GONE);
                materialCalendarView.setVisibility(View.VISIBLE);
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
                try {
                    Date convertedDate = dateFormat.parse(selectedCalenderDate);
                    materialCalendarView.setSelectedDate(convertedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        materialCalendarView.setOnTitleClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hcvCalender.setVisibility(View.VISIBLE);
                materialCalendarView.setVisibility(View.GONE);
            }
        });



        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @SuppressLint("ResourceType")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView,
                                       @NonNull CalendarDay calendarDay, boolean b) {
                Log.d("ondateselect", "onDateSelected: " +
                        android.text.format.DateFormat.format("yyyy-MM-dd", calendarDay.getDate()));
                String dayname = (String) android.text.format.DateFormat.format("EEEE", calendarDay.getDate());
//                TestDrawerActivity.edittextleaveSearch.setText("");
                TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                TestDrawerActivity.toolbar.setTitle(getString(R.string.diary));
                formattedDate=  (String)android.text.format.DateFormat.format("yyyy-MM-dd", calendarDay.getDate());
                selecteddayname(dayname);
                String selected = (String) android.text.format.DateFormat.format("MMMM dd", calendarDay.getDate());
                String today = (String) android.text.format.DateFormat.format("MMMM dd", calendar.getTime());
                if (selected.equals(today)) {
                    calendardayTextview.setText("Today");
                } else {
                    calendardayTextview.setText(selected);
                }
            }
        });


        userType = sharedPreferences.getInt(Constants.USERTYPE, 0);


        fabSpeedDial = view.findViewById(R.id.fab_menu);

        if (userType == 1) {
            fabSpeedDial.setVisibility(View.VISIBLE);
        }

        diary_recycle.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    if (userType == 1)
                        fabSpeedDial.setVisibility(View.VISIBLE);

                }
                else
                {
                    fabSpeedDial.setVisibility(View.GONE);
                }
            }
        });
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                if (fabSpeedDial.isSelected()) {
                    Log.e("open", ">>");
                } else {
                    Log.e("close", ">>" + navigationMenu.toString().trim());
                }
                // TODO: Do something with yout menu items, or return false if you don't want to show them
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                String menuitem = menuItem.toString();
                if (menuitem.equalsIgnoreCase("Add Feed")) {
                    //  getActivity().getResources().getString(R.string.add_feed))) {
                    Log.e("open", ">>" + menuItem);
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feed"));
                }
                else if (menuitem.equalsIgnoreCase("Attendance"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
                }
                else if (menuitem.equalsIgnoreCase("Homework"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "homework").putExtra("mode","create"));
                }
                else if (menuitem.equalsIgnoreCase("Classwork"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
                }
                else if (menuitem.equalsIgnoreCase("Events"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "events"));
                }
                else if (menuitem.equalsIgnoreCase("Exams"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "exam"));
                }
                return super.onMenuItemSelected(menuItem);
            }

        });




        Date date = calendar.getTime();
        String daynam=(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        selecteddayname(daynam);

        TestDrawerActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (  TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(  TestDrawerActivity.edittextleaveSearch.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        if (diaryDetailsLists.size()>0) {
                            searchLeaveList(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                        }
                        else
                        {
                            Toast.makeText(getContext(), R.string.noData, Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                    else
                    {
                        Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        //TestDrawerActivity.edittextleaveSearch.addTextChangedListener(new MyTextWatcher( TestDrawerActivity.edittextleaveSearch));
        TestDrawerActivity.edittextleaveSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(TestDrawerActivity.edittextleaveSearch);
            }
        });
        return view;
    }


    public void searchLeaveList(String searchvalue)
    {
        hideSoftKeyboard(TestDrawerActivity.edittextleaveSearch);
        if (data!=null && data.getDiaryDetailsList().size()>0)
        {
            diaryDetailsListssearch.clear();
            for(int y=0;y<data.getDiaryDetailsList().size();y++)
            {
                if (data.getDiaryDetailsList().get(y).getName().toLowerCase().contains(searchvalue.toString().toLowerCase()))
                {

                    diaryDetailsListssearch.add(data.getDiaryDetailsList().get(y));

                }
            }

            if (diaryDetailsListssearch.size()>0)
            {
                diaryAdapter = new DiaryAdapter(diaryDetailsListssearch, getActivity());
                diary_recycle.setAdapter(diaryAdapter);
                diaryAdapter.notifyDataSetChanged();

            }
            else
            {

                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                Toast.makeText(getContext(),"No data found",Toast.LENGTH_SHORT).show();
            }


        }

        else
        {
            Toast.makeText(getContext(), R.string.noData, Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void updateMonthOnScroll(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
    }

    @Override
    public void newDateSelected(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
        String str_date=selectedDate.date+"-"+selectedDate.monthNumeric+"-"+selectedDate.year;
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = (Date)formatter.parse(str_date);
        formattedDate=  (String)android.text.format.DateFormat.format("yyyy-MM-dd", date.getTime());
        String dayname = (String) android.text.format.DateFormat.format("EEEE", date.getTime());
        selecteddayname(dayname);
        String selected = (String) android.text.format.DateFormat.format("MMMM dd", date.getTime());
        String today = (String) android.text.format.DateFormat.format("MMMM dd", calendar.getTime());
        if (selected.equals(today)) {
            calendardayTextview.setText("Today");
        } else {
            calendardayTextview.setText(selected);
        }
    }

    public void selecteddayname(String dayname) {
        switch (dayname) {
            case "Monday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.MON);

                break;
            case "Tuesday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();

                getDiaryList(Constants.TUES);

                break;
            case "Wednesday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.WED);

                break;
            case "Thursday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.THURS);

                break;
            case "Friday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.FRI);

                break;
            case "Saturday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.SAT);

                break;
            case "Sunday":
                diaryDetailsLists.clear();
                diary_recycle.setAdapter(null);
                diaryAdapter.notifyDataSetChanged();
                getDiaryList(Constants.SUN);
                break;
        }
    }


//    @Override
//    public void onPrepareOptionsMenu(Menu menu) {
//        getActivity().getMenuInflater().inflate(R.menu.main, menu);
//        menu.getItem(0).setVisible(true);
//        super.onPrepareOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                //Toast.makeText(getContext(),"tttt",Toast.LENGTH_SHORT).show();
                if ( TestDrawerActivity.edittextleaveSearch.getText().toString().length()<=0) {
                    TestDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    TestDrawerActivity.toolbar.setTitle("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                    TestDrawerActivity.edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    TestDrawerActivity.edittextleaveSearch.setEnabled(true);
                    TestDrawerActivity.edittextleaveSearch.requestFocus();
                    showKeyboard(TestDrawerActivity.edittextleaveSearch);
                    TestDrawerActivity.edittextleaveSearch.setText("");
                }
                else
                {
                    searchLeaveList( TestDrawerActivity.edittextleaveSearch.getText().toString().trim());

                }
                return true;

            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showKeyboard(final EditText ettext){
        fabSpeedDial.setVisibility(View.GONE);
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }
    public void getDiaryList(String dayname) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = false;
            TimetableParms timetableParms = new TimetableParms();
            timetableParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            timetableParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            timetableParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            timetableParms.setDiaryDate(formattedDate);
            timetableParms.setTimetableDay(dayname);

            Gson gson = new Gson();
            String input = gson.toJson(timetableParms);

            vidyauraAPI.getDiaryDetailsList(timetableParms).enqueue(new Callback<DiaryListResponse>() {
                @Override
                public void onResponse(Call<DiaryListResponse> call, Response<DiaryListResponse> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                         data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            if (data.getStatus() == Util.STATUS_SUCCESS) {
                                if (response.body().getDiaryDetailsList() != null &&
                                        response.body().getDiaryDetailsList().size() != 0) {
                                    diary_recycle.setVisibility(View.VISIBLE);
                                    diaryDetailsLists.addAll(data.getDiaryDetailsList());
                                    diary_recycle.setAdapter(diaryAdapter);
                                    diaryAdapter.notifyDataSetChanged();

                                    // loading = true;
                                } else {
                                    diaryDetailsLists.clear();
                                   diaryAdapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    // loading = false;
                                }
                            } else {
                                diaryDetailsLists.clear();
                                diaryAdapter.notifyDataSetChanged();
                                //  if (!loading) {
                                Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                // }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        //timeList.clear();
                       // diaryAdapter.notifyDataSetChanged();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<DiaryListResponse> call, Throwable t) {
                    //timeList.clear();
                   // diaryAdapter.notifyDataSetChanged();
                    hideProgress();
                    isLoading = true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TestDrawerActivity.edittextleaveSearch.setText("");
        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
        Log.d("readas","adsfadf");
    }

    private void hideSoftKeyboard(EditText ettext){
        fabSpeedDial.setVisibility(View.VISIBLE);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    public class OneDayDecorator implements DayViewDecorator {

        private CalendarDay date;

        public OneDayDecorator() {
            date = CalendarDay.today();
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return date != null && day.equals(date);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(Color.RED));
        }

        /**
         * We're changing the internals, so make sure to call {@linkplain MaterialCalendarView#invalidateDecorators()}
         */
        public void setDate(Date date) {
            this.date = CalendarDay.from(date);
        }
    }
    public class MyTextWatcher implements TextWatcher {
        private EditText et;

        // Pass the EditText instance to TextWatcher by constructor
        public MyTextWatcher(EditText et) {
            this.et = et;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Unregister self before update
            String enteredValue  = s.toString();
            if (enteredValue.length()>2)
            {
                searchLeaveList(  TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
            }

        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

}
