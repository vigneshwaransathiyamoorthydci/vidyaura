package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ExamUpcomingAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ExamUpcomingList;
import com.school.teacherparent.models.ExamUpcomingResponse;
import com.school.teacherparent.models.UpcomingExamListParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamUpcomingFragment extends BaseFragment {


    private RecyclerView upcomingexmrecyclerView;
    private ExamUpcomingAdapter upcomingexmmAdapter;
    FloatingActionButton addExam;
    public ArrayList<ExamUpcomingResponse.examsHistoryList> upcomingexamResponsearray;
    ArrayList<ExamUpcomingList> examHistoryListArrayList=new ArrayList<>();
    public ExamUpcomingFragment() {
        // Required empty public constructor
    }

    SwipeRefreshLayout upcomingswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private boolean loading = true;
    int limit=5;
    int offset=0;
    TextView noupcomingexamListTextview;
    boolean service=true;
    LinearLayoutManager linearLayoutManager;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {
            examHistoryListArrayList.clear();
            limit=5;
            offset=0;
            if (service) {
                getUpcomingExam();
            }

        }


    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {

            examHistoryListArrayList.clear();
             limit=5;
             offset=0;
            if (service) {
                getUpcomingExam();
            }

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_exam, container, false);

        upcomingexmrecyclerView = (RecyclerView) view.findViewById(R.id.exam_recycle_fragment);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        ExamsActivity.title.setText("Exam");
        upcomingexamResponsearray=new ArrayList<ExamUpcomingResponse.examsHistoryList>();
        addExam = view.findViewById(R.id.add_exam);
        noupcomingexamListTextview=(TextView)view.findViewById(R.id.noupcomingexamListTextview);
        addExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type","exam").putExtra("examID", 0));
            }
        });

        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        upcomingexmmAdapter = new ExamUpcomingAdapter(examHistoryListArrayList,getContext(),this,userType);
        //  upcomingexmrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        upcomingexmrecyclerView.setLayoutManager(linearLayoutManager);
        upcomingexmrecyclerView.setItemAnimator(new DefaultItemAnimator());
        upcomingexmrecyclerView.setAdapter(upcomingexmmAdapter);


        noupcomingexamListTextview.setVisibility(View.GONE);
        //upcomingexmrecyclerView.setVisibility(View.GONE);
        upcomingswipelayout=view.findViewById(R.id.swipe_upcomingexam);
        upcomingswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                upcomingswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                             limit=5;
                             offset=0;
                            examHistoryListArrayList.clear();
                            getUpcomingExam();

                            upcomingswipelayout.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });

        ExamsActivity.noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),NotificationActivity.class));
            }
        });
        upcomingexmrecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addExam.setVisibility(View.VISIBLE);
                }
                else
                {
                    addExam.setVisibility(View.GONE);
                }
            }
        });

        upcomingexmrecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView examHistoryrecyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            upcomingexmmAdapter.notifyDataSetChanged();
                            getUpcomingExam();
                            upcomingexmmAdapter.notifyDataSetChanged();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

//        upcomingexmmAdapter.setOnClickListen(new ExamUpcomingAdapter.AddTouchListen() {
//            @Override
//            public void OnTouchClick(int position) {
//                Fragment fragment = new ExamDetailListFragment();
//                replaceFragment(fragment);
//            }
//
//
//        });
        return view;


    }

    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }

    private void getUpcomingExam()
    {

        if (Util.isNetworkAvailable())
        {
            service=false;
            showProgress();
            UpcomingExamListParams upcomingExamListParams=new UpcomingExamListParams();
            upcomingExamListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            upcomingExamListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            upcomingExamListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            upcomingExamListParams.setLimit(limit);
            upcomingExamListParams.setOffset(offset);
            vidyauraAPI.getUpcomingExamsList(upcomingExamListParams).enqueue(new Callback<ExamUpcomingResponse>() {
                @Override
                public void onResponse(Call<ExamUpcomingResponse> call, Response<ExamUpcomingResponse> response) {
                    hideProgress();
                    service=true;
                    if (response.body() != null) {
                        ExamUpcomingResponse upcomingexamResponse = response.body();
                        if (upcomingexamResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//                            upcomingexamResponsearray.clear();
                            upcomingexamResponsearray = upcomingexamResponse.getUpcomingExamsList();
                            if (upcomingexamResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (upcomingexamResponsearray.size()>0) {
                                    //noupcomingexamListTextview.setVisibility(View.GONE);
                                   // upcomingexmrecyclerView.setVisibility(View.VISIBLE);
                                    for (int y = 0; y < upcomingexamResponsearray.size(); y++) {
                                        examHistoryListArrayList.add(new ExamUpcomingList(upcomingexamResponse.getUpcomingExamsList().get(y).getTerm_id(), upcomingexamResponse.getUpcomingExamsList().get(y).getExam_title(), upcomingexamResponse.getUpcomingExamsList().get(y).getExam_duration(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getStart_date(), upcomingexamResponse.getUpcomingExamsList().get(y).getEnd_date(), upcomingexamResponse.getUpcomingExamsList().get(y).getSchool_name(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getLogo(), upcomingexamResponse.getUpcomingExamsList().get(y).getExamwiseClassList()));
                                        upcomingexmmAdapter.notifyDataSetChanged();
                                        loading = true;
                                    }
                                }
                                else if (examHistoryListArrayList.size()!=0 && upcomingexamResponsearray.size()!=0)
                                {
                                    //noupcomingexamListTextview.setVisibility(View.VISIBLE);
                                    //upcomingexmrecyclerView.setVisibility(View.GONE);
                                    loading=false;
                                }
                                else {
                                    Toast.makeText(getContext(), upcomingexamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            else {
                                Toast.makeText(getContext(), upcomingexamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamUpcomingResponse> call, Throwable t) {
                    service=true;
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }



}
