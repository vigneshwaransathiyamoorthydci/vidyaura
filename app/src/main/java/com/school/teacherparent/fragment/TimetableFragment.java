package com.school.teacherparent.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.TimeTableAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.R;
import com.school.teacherparent.calender.DayDateMonthYearModel;
import com.school.teacherparent.calender.HorizontalCalendarListener;
import com.school.teacherparent.calender.HorizontalCalendarView;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.models.TimetableParms;
import com.school.teacherparent.models.TimetablewithBreak;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.CurrentDateDecorator;
import com.school.teacherparent.utils.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.calender.HorizontalCalendarView.tv_month;

/**
 * Created by harini on 9/26/2018.
 */

public class TimetableFragment extends BaseFragment implements HorizontalCalendarListener {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = false;
    RecyclerView recyclerView;

    int offset = 0;
    SwipeRefreshLayout timelineswipelayout;
    CalendarView calender_view;
    private TimeTableAdapter mAdapter;
    private List<TimeTableModel.TimetableList> timeList = new ArrayList<>();
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    TextView calendardayTextview;
    MaterialCalendarView materialCalendarView;
    HorizontalCalendarView hcvCalender;
    Calendar calendar;
    String formattedDate;
    Date c;
    String selectedCalenderDate;
    ArrayList<TimetablewithBreak>  timetablewithBreakArrayList;
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {

        }


    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            isLoading = false;
            currentPage = 0;

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_timetable, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.timetabel));
        hcvCalender = view.findViewById(R.id.hcv_calender);
        recyclerView = view.findViewById(R.id.timetable_recycle);
        calendardayTextview=view.findViewById(R.id.calendardayTextview);
        mAdapter = new TimeTableAdapter(timeList,getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        materialCalendarView = view.findViewById(R.id.material_calender);
        timetablewithBreakArrayList=new ArrayList<>();
        c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        final DateFormat dateFormat = new SimpleDateFormat("MMMM-EEE-yyyy-MM-dd");
        final String currentDate=dateFormat.format(new Date());

        calendar = Calendar.getInstance();
        hcvCalender.setContext(this);

        materialCalendarView.state().edit()
                .setFirstDayOfWeek(Calendar.MONDAY)
                .setMinimumDate(CalendarDay.from(2018, 1, 1))
                .setMaximumDate(CalendarDay.from(2050, 1, 1))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();

        materialCalendarView.setSelectedDate(calendar.getTime());
        calendardayTextview.setText("Today");

        tv_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hcvCalender.setVisibility(View.GONE);
                materialCalendarView.setVisibility(View.VISIBLE);
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
                try {
                    Date convertedDate = dateFormat.parse(selectedCalenderDate);
                    materialCalendarView.setSelectedDate(convertedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        materialCalendarView.setOnTitleClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hcvCalender.setVisibility(View.VISIBLE);
                materialCalendarView.setVisibility(View.GONE);
                //hcvCalender.setContext(this,currentDate);
            }
        });

        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @SuppressLint("ResourceType")
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView,
                                       @NonNull CalendarDay calendarDay, boolean b) {
                Log.d("ondateselect", "onDateSelected: " +
                        android.text.format.DateFormat.format("yyyy-MM-dd", calendarDay.getDate()));
                String dayname = (String) android.text.format.DateFormat.format("EEEE", calendarDay.getDate());
//                TestDrawerActivity.edittextleaveSearch.setText("");
                TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                TestDrawerActivity.toolbar.setTitle(getString(R.string.diary));
                formattedDate=  (String)android.text.format.DateFormat.format("yyyy-MM-dd", calendarDay.getDate());
                selecteddayname(dayname);
                String selected = (String) android.text.format.DateFormat.format("MMMM dd", calendarDay.getDate());
                String today = (String) android.text.format.DateFormat.format("MMMM dd", calendar.getTime());
                if (selected.equals(today)) {
                    calendardayTextview.setText("Today");
                } else {
                    calendardayTextview.setText(selected);
                }
            }
        });

        Date date = calendar.getTime();
        String daynam=(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        selecteddayname(daynam);
        return view;
    }

    @Override
    public void updateMonthOnScroll(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
    }

    @Override
    public void newDateSelected(DayDateMonthYearModel selectedDate) throws ParseException {
        tv_month.setText(selectedDate.month + " " + selectedDate.year);
        selectedCalenderDate = selectedDate.monthNumeric+"/"+selectedDate.date+"/"+selectedDate.year+" 00:00:00 AM";
        String str_date=selectedDate.date+"-"+selectedDate.monthNumeric+"-"+selectedDate.year;
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = (Date)formatter.parse(str_date);
        String dayname = (String) android.text.format.DateFormat.format("EEEE", date.getTime());
        selecteddayname(dayname);
        String selected = (String) android.text.format.DateFormat.format("MMMM dd", date.getTime());
        String today = (String) android.text.format.DateFormat.format("MMMM dd", calendar.getTime());
        if (selected.equals(today)) {
            calendardayTextview.setText("Today");
        } else {
            calendardayTextview.setText(selected);
        }
    }

    public void selecteddayname(String dayname) {
        switch (dayname) {
            case "Monday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.MON);

                break;
            case "Tuesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.TUES);

                break;
            case "Wednesday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.WED);

                break;
            case "Thursday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.THURS);

                break;
            case "Friday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.FRI);

                break;
            case "Saturday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SAT);

                break;
            case "Sunday":
                timeList.clear();
                mAdapter.notifyDataSetChanged();
                getTimetableList(Constants.SUN);

                break;
        }
    }


    public void getTimetableList(String dayname) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = false;
            TimetableParms timetableParms = new TimetableParms();
            timetableParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            timetableParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            timetableParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            timetableParms.setTimetableDay(dayname);
            timetablewithBreakArrayList=new ArrayList<>();
            Gson gson = new Gson();
            String input = gson.toJson(timetableParms);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getTimetableList(timetableParms).enqueue(new Callback<TimeTableModel>() {
                @Override
                public void onResponse(Call<TimeTableModel> call, Response<TimeTableModel> response) {
                    hideProgress();
                    isLoading = true;
                    if (response.body() != null) {
                        TimeTableModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getTimetableList() != null &&
                                        response.body().getTimetableList().size() != 0) {
                                    recyclerView.setVisibility(View.VISIBLE);


                                   // timeList.addAll(data.getTimetableList());


                                    List<String> l = new ArrayList<String>();
                                    for (int i=0;i<data.getTimetableList().size();i++)
                                    {
                                        //l.add(data.getTimetableList().get(i).getBreakTiming().get(0).getStart_time());
                                        if (data.getTimetableList().get(i).getBreakTiming() != null) {
                                            if (data.getTimetableList().get(i).getBreakTiming().size() > 0) {
                                                timeList.add(data.getTimetableList().get(i));
                                            }
                                        }
                                    }



                                    Collections.sort(timeList, new Comparator() {
                                        @Override
                                        public int compare(Object o1, Object o2) {
                                            TimeTableModel.TimetableList p1 = (TimeTableModel.TimetableList) o1;
                                            TimeTableModel.TimetableList p2 = (TimeTableModel.TimetableList) o2;

                                            try {
                                                return new SimpleDateFormat("hh:mm a").parse(p1.getBreakTiming().get(0).getStart_time()).compareTo
                                                        (new SimpleDateFormat("hh:mm a").parse(p2.getBreakTiming().get(0).getStart_time()));
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                                return 0;
                                            // return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));

                                        }

                                    });
                                    mAdapter.notifyDataSetChanged();

//                                    Collections.sort(l, new Comparator<String>() {
//
//                                        @Override
//                                        public int compare(String o1, String o2) {
//                                            try {
//                                                return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
//                                            } catch (ParseException e) {
//                                                return 0;
//                                            }
//                                        }
//                                    });
//                                    System.out.println(l);


                                    // loading = true;
                                } else {
                                    timeList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();
                                    // loading = false;
                                }
                            } else {
                                timeList.clear();
                                mAdapter.notifyDataSetChanged();
                                //  if (!loading) {
                                Toast.makeText(getContext(), data.getMessage(), Toast.LENGTH_SHORT).show();
                                // }

                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        timeList.clear();
                        mAdapter.notifyDataSetChanged();
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<TimeTableModel> call, Throwable t) {
                    timeList.clear();
                    mAdapter.notifyDataSetChanged();
                    hideProgress();
                    isLoading = true;
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }




}
