package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;

import java.util.ArrayList;
import java.util.List;


public class AddHomeWorkTabFragment extends Fragment {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public TextView add_homework;
    private ImageView back;
    public  String test="TEST";
    public AddHomeWorkTabFragment() {
        // Required empty public constructor
    }
    SidemenuDetailActivity sidemenuDetailActivity;
    AddHomeWorkFragment addHomeWorkFragment;
    AddHomeAssignmentFragment addHomeAssignmentFragment;
    String mode;
    int homeworkIDforedit;
    int position;
    List<String>imagelist=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_home_work, container, false);

        sidemenuDetailActivity=(SidemenuDetailActivity)getActivity();
        add_homework = view.findViewById(R.id.add_homework);
        viewPager = view.findViewById(R.id.viewpager);
        addHomeWorkFragment=new AddHomeWorkFragment();
        addHomeAssignmentFragment=new AddHomeAssignmentFragment();
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mode = bundle.getString("mode", "");
            homeworkIDforedit=bundle.getInt("homeworkIDforedit");
            position=bundle.getInt("position");
            if (mode.equals("create"))
            {
                SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_homework));
                setupViewPager(viewPager);
//                TabLayout.Tab tab = tabLayout.getTabAt(position);
//                tab.select();
//                viewPager.setCurrentItem(position);

            }
            else
            {
                add_homework.setText(getString(R.string.update_homework));
                SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_homework));
                setupViewPagerforUpdate(viewPager);


            }

        }
        /*back = view.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });*/

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });



        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                if (mode.equals("create")) {
                    if (tabLayout.getSelectedTabPosition() == 0) {
                        add_homework.setText(getString(R.string.add_homework));
                        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_homework));
                    } else if (tabLayout.getSelectedTabPosition() == 1) {
                        add_homework.setText(getString(R.string.add_assigment));
                        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_assigment));
                    }
                }
                else
                {
                    if (tabLayout.getSelectedTabPosition() == 0) {
                        add_homework.setText(getString(R.string.update_homework));
                        SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_homework));
                    } else if (tabLayout.getSelectedTabPosition() == 1) {
                        add_homework.setText(getString(R.string.update_assigment));
                        SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_assigment));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
//
        add_homework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tabLayout.getSelectedTabPosition()==0) {
//                    if (addHomeWorkFragment.isalldataEntered(sidemenuDetailActivity)){
//                        final ProgressDialog dialog=new ProgressDialog(getActivity());
//                        dialog.setMessage("Uploading");
//                        dialog.show();
////                        ((BaseActivity)getActivity()).uploadFile(imagelist, "", "", new OnAmazonFileuploaded() {
////                            @Override
////                            public void FileStatus(int status, String filename) {
////                               dialog.dismiss();
////                               addHomeWorkFragment.isValidate(sidemenuDetailActivity);
////                            }
////                        });
//                    }
                    addHomeWorkFragment.isValidate(sidemenuDetailActivity);


                }
                else if (tabLayout.getSelectedTabPosition()==1)
                {
                    addHomeAssignmentFragment.isValidate(sidemenuDetailActivity);
                }



            }
        });
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        adapter.addFragment(new AddHomeWorkFragment(), getString(R.string.home));
        adapter.addFragment(new AddHomeAssignmentFragment(), getString(R.string.assignment));


        viewPager.setAdapter(adapter);


    }
    public void getCurrentFragment(List<String>images){
        imagelist=images;
        Log.d("picked", "getCurrentFragment: "+"tABCALLED");
        Fragment page = getActivity().getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
        if(mode.equals("create")){
            if (viewPager.getCurrentItem() == 0&& page != null) {
                ((AddHomeWorkFragment)page).setPickedImageDetails(null,images);
            }
            else if (viewPager.getCurrentItem() == 1 && page != null){
                ((AddHomeAssignmentFragment)page).setPickedImageDetails(null,images);
            }
        }
        else {
            if (viewPager.getCurrentItem() == 0&& page != null) {
              //  ((UpdateHomeWorkFragment)page).setPickedImageDetails(null,images);
            }
            else if (viewPager.getCurrentItem() == 1 && page != null){
                //((UpdateHomeAssignmentFragment)page).setPickedImageDetails(null,images);
            }
        }


    }

    private void setupViewPagerforUpdate(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        UpdateHomeWorkFragment updateHomeWorkFragment=new UpdateHomeWorkFragment();
        Bundle bundle11 = new Bundle();
        bundle11.putInt("homeworkIDforedit",homeworkIDforedit);
        updateHomeWorkFragment.setArguments(bundle11);
        adapter.addFragment(updateHomeWorkFragment, getString(R.string.home));
        adapter.addFragment(new UpdateHomeAssignmentFragment(), getString(R.string.assignment));
        viewPager.setAdapter(adapter);


    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
