package com.school.teacherparent.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ExamComposechapterMultiSelectionAdpater;
import com.school.teacherparent.adapter.ExamComposetopicAdpater;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ChapterListResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.ExamDetailByIdResponse;
import com.school.teacherparent.models.ExamDetailsParams;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.ExamTypeResponse;
import com.school.teacherparent.models.GetChapterListParam;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.GetTopicListParam;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofChapterforDialog;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.ListofTopicforDialog;
import com.school.teacherparent.models.SubjectListResponse;
import com.school.teacherparent.models.TopicListResponse;
import com.school.teacherparent.models.UpdateExambyClassParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateExamFragment extends BaseFragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {
    private SimpleDateFormat simpleDateFormat,dateFormatwithseconds;
    private static int selectedDate = 1;
    private Calendar currentD;
    private Calendar calendardate;
    String formattedDate;
    private RecyclerView recyclerView;

    private Spinner exam_term_spinner, class_spinner, subject_spinner;
    private RelativeLayout relative_class_spinner, relative_sub_spinner;
    private TextView add_exm_btn;

    ExamTermResponse examTermResponse;
    ChapterListResponse chapterListResponse;
    TopicListResponse topicListResponse;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    ArrayList<String> classlist = new ArrayList<String>();
    ArrayAdapter<String> classspinnerArray;
    int examTermId = 0, classId = 0, sectionId = 0, subject_Id = 0, topic_Id = 0;
    String chapter_Id="";
    ClassListResponse classListResponse;
    SubjectListResponse subjectListResponse;
    ExamTypeResponse examTypeResponse;
    ArrayAdapter<String> subjectspinnerArray;
    ArrayList<String> subjectlist = new ArrayList<String>();
    ArrayAdapter<String> chapterspinnerArray;
    ArrayList<String> chapterlist = new ArrayList<String>();

    ArrayAdapter<String> topicspinnerArray;
    ArrayList<String> topiclist = new ArrayList<String>();
    LinearLayout linear_examdate, linear_starttime, linear_endtime;
    TextView examdate_textview, examstarttime_textview, examendtime_textview;
    private String eventStartDate, eventStartTime, eventStartDateFormat, eventEndDateFormat;
    private String eventEndDate, eventEndTime;
    private Calendar calendarStartTime, calendarEndTime;
    private Calendar currentT;
    private SimpleDateFormat simpleDateFormat2;
    String examDuration;
     private MultiSpinner chaptermultiSpinner,topicmultiSpinner;
     Spinner examSpinner;
    TextView minTextview,maxTextview;
    ArrayList<Integer> selectedChapter=new ArrayList<>();
    static List<ListofChapterforDialog> chapterList = new ArrayList<ListofChapterforDialog>();
    List<ListofChapterforDialog> searchchapterList = new ArrayList<ListofChapterforDialog>();

    ArrayList<Integer> selectedTopic=new ArrayList<>();
    List<ListofTopicforDialog> topicList = new ArrayList<ListofTopicforDialog>();
    List<ListofTopicforDialog> searchtopicList = new ArrayList<ListofTopicforDialog>();
    String lastchapterID="",lasttopicId="";
    int visibility=1;
    ArrayAdapter<String> examtypespinnerArray;
    ArrayList<String> examtypelist = new ArrayList<String>();
    int examID;
    ExamDetailByIdResponse examDetailByIdResponse;
    boolean[] selectedchapterItemsupdate;
    boolean[] selectedtopicItemsupdate;
    List<ListofClass> chapterListUpdate = new ArrayList<ListofClass>();
    TextView chapterTextview,topicTextview;
    String topicID = "";
    String chapterID = "";
    boolean isforEdit=false;
    Dialog chapterDialog,topicDialog;
    ListView topiclistView,chapterlistView;
    ExamComposetopicAdpater examComposetopicAdpater;
    ExamComposechapterMultiSelectionAdpater examComposechapterAdpater;
    SearchView dialogSearchView,chapterdialogSearchView;
    TextView topicsearchnameTextview,chaptersearchnameTextview;
    Button dialogTopicButton,dialogChapterButton;
    String examStartdate,examEnddate;
    String selectedstartTime,selectedendTime;
    CheckBox selectAllCheckbox;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_exam, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_exam));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();


        exam_term_spinner = (Spinner) view.findViewById(R.id.exam_type_spinner);
        subject_spinner = (Spinner) view.findViewById(R.id.subject_spinner);
        examSpinner = (Spinner) view.findViewById(R.id.aceadmic_spinner);
        chaptermultiSpinner=(MultiSpinner)view.findViewById(R.id.chapter_spinner);
        topicmultiSpinner=(MultiSpinner)view.findViewById(R.id.topic_spinner);
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        add_exm_btn = (TextView) view.findViewById(R.id.add_exm_btn);
        add_exm_btn.setText(getString(R.string.update_exam));
        minTextview=(TextView)view.findViewById(R.id.minTextview);
        maxTextview=(TextView)view.findViewById(R.id.maxTextview);
        linear_examdate = (LinearLayout) view.findViewById(R.id.linear_examdate);
        linear_examdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog();
            }
        });
        linear_starttime = (LinearLayout) view.findViewById(R.id.linear_starttime);

        linear_starttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = 1;
                timePickerDialog();

            }
        });
        linear_endtime = (LinearLayout) view.findViewById(R.id.linear_endtime);
        linear_endtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedDate = 2;
                timePickerDialog();
            }
        });
        examdate_textview = (TextView) view.findViewById(R.id.examdate_textview);
        examstarttime_textview = (TextView) view.findViewById(R.id.examstarttime_textview);
        examendtime_textview = (TextView) view.findViewById(R.id.examendtime_textview);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = currentDATE.format(c);
        examdate_textview.setText(getDateFormat(currentDate));

        dateFormatwithseconds=new SimpleDateFormat("HH:mm:ss");
        selectedstartTime=dateFormatwithseconds.format(c.getTime());
        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        add_exm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAddExam();
            }
        });

        chapterDialog=new Dialog(getContext());
        chapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        chapterDialog.setCancelable(false);
        chapterDialog.setContentView(R.layout.add_exam_chapter_custom_dialog);
        chapterdialogSearchView=(SearchView)chapterDialog.findViewById(R.id.dialogSearchView);
        chaptersearchnameTextview=(TextView)chapterDialog.findViewById(R.id.searchnameTextview);
        //chapternameTextview=(TextView)view.findViewById(R.id.chapternameTextview);
        chapterlistView = (ListView) chapterDialog.findViewById(R.id.chapterlistView);
        dialogChapterButton=(Button)chapterDialog.findViewById(R.id.btn_dialog) ;
        selectAllCheckbox=(CheckBox)chapterDialog.findViewById(R.id.selectAllCheckbox);
        topicDialog = new Dialog(getContext());
        topicDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        topicDialog.setCancelable(false);
        topicDialog.setContentView(R.layout.custom_dialog);
        topiclistView = (ListView) topicDialog.findViewById(R.id.listView1);
        dialogTopicButton = (Button) topicDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)topicDialog.findViewById(R.id.dialogSearchView);
        topicsearchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);
        chapterTextview=(TextView)view.findViewById(R.id.chaptername);
        topicTextview=(TextView)view.findViewById(R.id.topicname);
        topicTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topicList.size()>0)
                {
                    topicDialog.show();
                }
            }
        });

        chapterTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chapterList.size()>0)
                {
                    chapterDialog.show();
                }
            }
        });
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        chapterdialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chapterdialogSearchView.setIconified(false);
            }
        });



        chapterdialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchchapterList.clear();
                    for(int y=0;y<chapterList.size();y++)
                    {
                        if (chapterList.get(y).getChapter_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchchapterList.add(chapterList.get(y));

                        }

                    }


                    if (searchchapterList.size()>0) {

                        chapterlistView.setVisibility(View.VISIBLE);
                        chaptersearchnameTextview.setVisibility(View.GONE);
                        examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList, selectAllCheckbox);
                        chapterlistView.setAdapter(examComposechapterAdpater);

                    }
                    else
                    {
                        chapterlistView.setVisibility(View.GONE);
                        chaptersearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    chapterlistView.setVisibility(View.VISIBLE);
                    chaptersearchnameTextview.setVisibility(View.GONE);
                    examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(chapterList,getContext(),chapterDialog,dialogChapterButton, chapterList, selectAllCheckbox);
                    chapterlistView.setAdapter(examComposechapterAdpater);

                }
                return false;
            }
        });

        dialogChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                for (int y = 0; y < chapterList.size(); y++) {
                    if (chapterList.get(y).getSelectedID()==0)
                    {
                        selectAllCheckbox.setChecked(false);
                        Log.d("TTTTTTTTT","kkkkk");
                        break;
                    }
                }
                String chapterName="";
                chapterID="";
                for (int y=0;y<chapterList.size();y++)
                {


                    if (chapterList.get(y).getSelectedID()!=0) {
                        chapterName = chapterName.concat(String.valueOf(chapterList.get(y).getChapter_name()).concat(","));
                        chapterID=chapterID.concat(String.valueOf(chapterList.get(y).getId())).concat(",");
                    }

                }
                if (chapterName.length()>0 && chapterID.length()>0) {
                    chapterName = chapterName.substring(0, chapterName.length() - 1);
                    chapterID = chapterID.substring(0, chapterID.length() - 1);
                    chapterTextview.setText(chapterName);
                    chapterTextview.setError(null);
                    topicList.clear();
                    topicID="";
                    topicTextview.setText("");
                    topicTextview.setHint(getString(R.string.selecttopic));
                    topicTextview.setError(null);
                    getTopicList(chapterID);
                }
                else
                {
                    chapterTextview.setText("");
                }
                examComposechapterAdpater.notifyDataSetChanged();
                chapterDialog.hide();
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchtopicList.clear();
                    for(int y=0;y<topicList.size();y++)
                    {
                        if (topicList.get(y).getTopic_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchtopicList.add(topicList.get(y));

                        }

                    }


                    if (searchtopicList.size()>0) {

                        topiclistView.setVisibility(View.VISIBLE);
                        topicsearchnameTextview.setVisibility(View.GONE);
                        examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                        topiclistView.setAdapter(examComposetopicAdpater);
                    }
                    else
                    {
                        topiclistView.setVisibility(View.GONE);
                        topicsearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    topiclistView.setVisibility(View.VISIBLE);
                    topicsearchnameTextview.setVisibility(View.GONE);
                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                    topiclistView.setAdapter(examComposetopicAdpater);

                }
                return false;
            }
        });
        dialogTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String topicName="";
                topicID="";
                for (int y=0;y<topicList.size();y++)
                {

                    if (topicList.get(y).getSelectedID()!=0) {
                        topicName = topicName.concat(String.valueOf(topicList.get(y).getTopic_name()).concat(","));
                        topicID=topicID.concat(String.valueOf(topicList.get(y).getId())).concat(",");
                    }

                }
                if (topicName.length()>0 && topicID.length()>0) {
                    topicName = topicName.substring(0, topicName.length() - 1);
                    topicID = topicID.substring(0, topicID.length() - 1);
                    Log.d("BBAA", "ID" + topicID + "NAME" + topicName);
                    topicTextview.setText(topicName);
                    topicTextview.setError(null);
                }
                else
                {
                    topicTextview.setText("");
                }
                topicDialog.hide();
            }
        });
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            examID = bundle.getInt("examID", 0);

        }

            if (examID>0)
            {
                getExamDetails();
            }
            else
            {
                getExamtermList();
            }


        topicspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, topiclist);

        topicspinnerArray.setDropDownViewResource(R.layout.spinner_item);



        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        subject_spinner.setAdapter(subjectspinnerArray);

        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_term_spinner.setAdapter(spinnerexamTermAdapter);


        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);

        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);


        chapterspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, chapterlist);

        chapterspinnerArray.setDropDownViewResource(R.layout.spinner_item);




        exam_term_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    examTermId = 0;
                    setExamdateToCalender(0);
                } else {
                    examTermId = examTermResponse.getExamTermsList().get(position - 1).getId();
                    if (isforEdit) {
                        setExamdateToCalender(1);
                        if (classId > 0 && sectionId > 0) {
                            if (examID <= 0) {
                                subjectspinnerArray.clear();
                                subjectspinnerArray.notifyDataSetChanged();
                                subject_Id = 0;
                                chapterID = "";
                                subjectspinnerArray.clear();
                                subjectspinnerArray.notifyDataSetChanged();
                                chapterTextview.setText("");
                                topicTextview.setText("");
                                chapterTextview.setHint(getString(R.string.selectchapter));
                                topicTextview.setHint(getString(R.string.selecttopic));
                                chapterTextview.setError(null);
                                topicTextview.setError(null);
                                chapterList.clear();
                                examComposechapterAdpater.notifyDataSetChanged();
                                topicList.clear();
                                examComposetopicAdpater.notifyDataSetChanged();
                                topicID = "";
                                getSubjecyList();
                            }
                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                examTermId = 0;

            }
        });


        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    classId = 0;
                    sectionId = 0;
                } else {
                    classId = classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionId = classListResponse.getTeachersClassesList().get(position - 1).getSectionID();
                    if (isforEdit) {



                        subjectspinnerArray.clear();
                        subjectspinnerArray.notifyDataSetChanged();
                        subject_Id = 0;
                        chapterspinnerArray.clear();
                        chapterspinnerArray.notifyDataSetChanged();
                        chapterID = "";
                        lastchapterID = "";
                        lasttopicId = "";
                        topicspinnerArray.clear();
                        topicspinnerArray.notifyDataSetChanged();
                        subjectspinnerArray.clear();
                        subjectspinnerArray.notifyDataSetChanged();
                        chapterTextview.setText("");
                        topicTextview.setText("");
                        chapterTextview.setHint(getString(R.string.selectchapter));
                        topicTextview.setHint(getString(R.string.selecttopic));
                        topicID = "";
                        chapterTextview.setError(null);
                        topicTextview.setError(null);
                        chapterList.clear();
                        examComposechapterAdpater.notifyDataSetChanged();
                        topicList.clear();
                        examComposetopicAdpater.notifyDataSetChanged();
                        getSubjecyList();
                            }
                        }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        subject_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    //subject_Id = 0;

                } else {
                    if (isforEdit) {
                        subject_Id = subjectListResponse.getTeachersSubjectsList().get(position-1).getSubject_id();
                        topicspinnerArray.clear();
                        topicspinnerArray.notifyDataSetChanged();
                        chapterList.clear();
                        chapterID="";
                        chapterTextview.setText("");
                        chapterTextview.setHint(getString(R.string.selectchapter));
                        topicList.clear();
                        topicID="";
                        topicTextview.setText("");
                        topicTextview.setHint(getString(R.string.selecttopic));
                        chapterTextview.setError(null);
                        topicTextview.setError(null);
                        chapterList.clear();
                        examComposechapterAdpater.notifyDataSetChanged();
                        topicList.clear();
                        examComposetopicAdpater.notifyDataSetChanged();
                        getExamchapterList();
                        }

                    //getSubjecyList();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });




        recyclerView = (RecyclerView) view.findViewById(R.id.add_exam_recycle);





        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        examtypespinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examtypelist);

        examtypespinnerArray.setDropDownViewResource(R.layout.spinner_item);

        examSpinner.setAdapter(examtypespinnerArray);

        examSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                visibility=examTypeResponse.getExamTypeList().get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }


    private void getExamDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            ExamDetailsParams examDetailsParams=new ExamDetailsParams();
            examDetailsParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            examDetailsParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            examDetailsParams.setExamScheduleID(String.valueOf(examID));
            examDetailsParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getExamDetailsById(examDetailsParams).enqueue(new Callback<ExamDetailByIdResponse>() {
                @Override
                public void onResponse(Call<ExamDetailByIdResponse> call, Response<ExamDetailByIdResponse> response) {

                    if (response.body() != null) {
                         examDetailByIdResponse=response.body();
                        if (examDetailByIdResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examDetailByIdResponse.getStatus() == Util.STATUS_SUCCESS) {
                                //Toast.makeText(getContext(), examDetailByIdResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClass_id();
                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
                                getExamtermList();
                                //setspinnerData();
                            } else {
                                Toast.makeText(getContext(), examDetailByIdResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {

                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                        getActivity().finish();


                    }
                }

                @Override
                public void onFailure(Call<ExamDetailByIdResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void setspinnerData()
    {

        examTermId=examDetailByIdResponse.getExamScheduleList().get(0).getTerm_id();

        minTextview.setText(""+examDetailByIdResponse.getExamScheduleList().get(0).getMin_mark());
        maxTextview.setText(""+examDetailByIdResponse.getExamScheduleList().get(0).getMarks());
        examdate_textview.setText(getDateFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time()));
        examstarttime_textview.setText(getTimeFormatwithAMPM(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time()));
        examendtime_textview.setText(getTimeFormatwithAMPM(examDetailByIdResponse.getExamScheduleList().get(0).getEnd_time()));
        eventStartTime=getTimeFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time());
        eventEndTime=getTimeFormatwithTMS(examDetailByIdResponse.getExamScheduleList().get(0).getEnd_time());
        formattedDate=geteventupdateDateFormat(examDetailByIdResponse.getExamScheduleList().get(0).getStart_time());
        try {
            DurationBetweenTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(examDetailByIdResponse.getExamScheduleList().get(0).getExam_type()==1)
        {
            examSpinner.setSelection(0);
        }
        else
        {
            examSpinner.setSelection(1);
        }


        for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
            if (examTermResponse.getExamTermsList().get(i).getId() == examDetailByIdResponse.getExamScheduleList().get(0).getTerm_id()) {
                exam_term_spinner.setSelection(i+1);
                examStartdate=examTermResponse.getExamTermsList().get(i).getStart_date();
                examEnddate=examTermResponse.getExamTermsList().get(i).getEnd_date();
                break;
            }
        }
        for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
            if (classListResponse.getTeachersClassesList().get(i).getSectionID() == examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id()) {
                class_spinner.setSelection(i+1);
                break;
            }
        }
        for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {
            if (subjectListResponse.getTeachersSubjectsList().get(i).getSubject_id() == examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id()) {
                subject_spinner.setSelection(i+1);
                break;
            }
        }

        String[] chapterstrArray = examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id().split(",");
        String chapterName="";
        for (int i=0;i<chapterList.size();i++)
        {
            for (int u=0;u<chapterstrArray.length;u++)
            {
                int selectedclassID= Integer.parseInt(chapterstrArray[u]);
                if (chapterList.get(i).getId()==selectedclassID)
                {

                    chapterName=chapterName.concat(chapterList.get(i).getChapter_name()).concat(",");
                    chapterID=chapterID.concat(String.valueOf(chapterList.get(u).getId())).concat(",");
                    chapterList.get(i).setSelectedID(chapterList.get(i).getId());


                }
            }

        }
        if (chapterName.length()>0 && chapterID.length()>0) {
            chapterName = chapterName.substring(0, chapterName.length() - 1);
            chapterID = chapterID.substring(0, chapterID.length() - 1);
            chapterTextview.setText(chapterName);
            examComposechapterAdpater.notifyDataSetChanged();

        }





        for (int u=0;u<chapterList.size();u++)
        {
            if (chapterList.get(u).getSelectedID()==chapterList.get(u).getId())
            {
                selectAllCheckbox.setChecked(true);
                continue;
            }
            else
            {
                selectAllCheckbox.setChecked(false);
                break;
            }
        }



        String[] topicstrArray = examDetailByIdResponse.getExamScheduleList().get(0).getTopic_id().split(",");
        String topicName="";
        for (int i=0;i<topicList.size();i++)
        {
            for (int u=0;u<topicstrArray.length;u++)
            {
                int selectedtopicID= Integer.parseInt(topicstrArray[u]);
                if (topicList.get(i).getId()==selectedtopicID)
                {
                    topicName=topicName.concat(topicList.get(i).getTopic_name()).concat(",");
                    topicID=topicID.concat(String.valueOf(topicList.get(u).getId())).concat(",");
                    topicList.get(i).setSelectedID(topicList.get(i).getId());

                }
            }

        }

        if (topicName.length()>0 && topicID.length()>0) {
            topicName = topicName.substring(0, topicName.length() - 1);
            topicID = topicID.substring(0, topicID.length() - 1);
            topicTextview.setText(topicName);
            examComposetopicAdpater.notifyDataSetChanged();

        }







        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                    isforEdit=true;
                    hideProgress();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }


    private void isAddExam()
    {
        if (examTermId!=0)
        {
                if (classId!=0)
                {
                   if (subject_Id!=0)
                   {
                        if (examdate_textview.getText().toString().trim().length()>0) {
                            if (examstarttime_textview.getText().toString().trim().length() > 0) {
                                if (examendtime_textview.getText().toString().trim().length() > 0) {
                                    if (chapterID.length()!=0)
                                    {
                                        if (topicID.length()!=0)
                                        {
                                            if (visibility!=0)
                                            {
                                                if (minTextview.getText().toString().length()>0)
                                                {
                                                    if (maxTextview.getText().toString().length()>0)
                                                    {
                                                        updateExam();
                                                    }
                                                    else
                                                    {
                                                       // Toast.makeText(getContext(),"Enter Max mark",Toast.LENGTH_SHORT).show();
                                                        maxTextview.setError(getString(R.string.Enter_maxmark));
                                                    }
                                                }
                                                else
                                                {
                                                    //Toast.makeText(getContext(),"Enter Min mark",Toast.LENGTH_SHORT).show();
                                                    minTextview.setError(getString(R.string.Enter_minmark));
                                                }
                                            }
                                            else
                                            {
                                                Toast.makeText(getContext(),"Select examtype",Toast.LENGTH_SHORT).show();
                                            }

                                            //Toast.makeText(getContext(),"Exam Ready to add",Toast.LENGTH_SHORT).show();
                                        }
                                        else
                                        {
                                           // Toast.makeText(getContext(),"Choose Topic ",Toast.LENGTH_SHORT).show();
                                            topicTextview.setError(getString(R.string.selecttopic));
                                        }
                                    }
                                    else
                                    {
                                        //Toast.makeText(getContext(),"Choose Chapter ",Toast.LENGTH_SHORT).show();
                                        chapterTextview.setError(getString(R.string.selectchapter));
                                    }
                                } else {
                                    Toast.makeText(getContext(), "Choose exam end time ", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Choose exam start time ", Toast.LENGTH_SHORT).show();
                            }
                        }

                        else
                        {
                            Toast.makeText(getContext(),"Choose date",Toast.LENGTH_SHORT).show();
                        }
                   }
                   else
                   {
                       Toast.makeText(getContext(),"Choose Subject",Toast.LENGTH_SHORT).show();
                   }
                }
                else
                {
                    Toast.makeText(getContext(),"Choose Class",Toast.LENGTH_SHORT).show();
                }
        }
        else
        {
            Toast.makeText(getContext(),"Choose Exam term",Toast.LENGTH_SHORT).show();
        }


    }
    private void getExamtype()
    {
        if (Util.isNetworkAvailable())
        {
            if (isforEdit) {
                showProgress();
            }
            vidyauraAPI.getExamType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0))).enqueue(new Callback<ExamTypeResponse>() {
                @Override
                public void onResponse(Call<ExamTypeResponse> call, Response<ExamTypeResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {

                        examTypeResponse = response.body();
                        if (examTypeResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (examTypeResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (examTypeResponse.getExamTypeList().size() > 0) {
                                    for (int i = 0; i < examTypeResponse.getExamTypeList().size(); i++) {
                                        examtypelist.add(examTypeResponse.getExamTypeList().get(i).getName());
                                    }
                                    examtypespinnerArray.notifyDataSetChanged();
                                    if (!isforEdit) {
                                        setspinnerData();
                                    }

                                } else {
                                    Toast.makeText(getContext(), examTypeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getContext(), examTypeResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamTypeResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void updateExam() {
        if (Util.isNetworkAvailable()) {
                showProgress();
                UpdateExambyClassParam updateExambyClassParam=new UpdateExambyClassParam();
                updateExambyClassParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
                updateExambyClassParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
                updateExambyClassParam.setExamDuration(examDuration);
                updateExambyClassParam.setExamStartTime(formattedDate+" "+eventStartTime);
                updateExambyClassParam.setExamEndTime(formattedDate+" "+eventEndTime);
                updateExambyClassParam.setClassID(String.valueOf(classId));
                updateExambyClassParam.setSectionID(String.valueOf(sectionId));
                updateExambyClassParam.setSubjectID(String.valueOf(subject_Id));
                updateExambyClassParam.setTermID(String.valueOf(examTermId));
                updateExambyClassParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
                updateExambyClassParam.setMaxMark(maxTextview.getText().toString().trim());
                updateExambyClassParam.setMinMark(minTextview.getText().toString().trim());
                updateExambyClassParam.setExamType(String.valueOf(1));
                updateExambyClassParam.setChapterID(chapterID);
                updateExambyClassParam.setTopicID(topicID);
                updateExambyClassParam.setExamScheduleID(String.valueOf(examID));
            System.out.println("exam data time ==> "+formattedDate+" "+eventStartTime);
            System.out.println("exam data time ==> "+formattedDate+" "+eventEndTime);
                vidyauraAPI.updateExamByClass(updateExambyClassParam).enqueue(new Callback<AddFeedResponse>() {
                    @Override
                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                        hideProgress();
                        if (response.body() != null) {
                            AddFeedResponse addFeedResponse = response.body();
                            if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                                if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                    Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    getActivity().finish();

                                } else {
                                    Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                getActivity().finishAffinity();

                            }

                        }
                        else
                        {
                            Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            if (isforEdit) {
                showProgress();
            }
            subjectlist.clear();
            subjectlist.add("Select Subject");
            GetSubjectListParam getSubjectListParam = new GetSubjectListParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            vidyauraAPI.getTeachersSubjects(getSubjectListParam).enqueue(new Callback<SubjectListResponse>() {
                @Override
                public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {
                        subjectListResponse = response.body();
                        if (subjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (subjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {

                                    subjectlist.add(subjectListResponse.getTeachersSubjectsList().get(i).getName());

                                }
                                subjectspinnerArray.notifyDataSetChanged();
                                if (!isforEdit) {
                                    subject_Id = examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                    classId = examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                    chapter_Id = examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();

                                    getExamchapterList();

                                }


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            if (isforEdit) {
                showProgress();
            }
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(classListResponse.getTeachersClassesList().get(i).getClassName()+"-"+
                                            classListResponse.getTeachersClassesList().get(i).getSection());

                                }
                                if (!isforEdit) {
                                    classId = examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                    sectionId = examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
                                    getSubjecyList();
                                }
                                classspinnerArray.notifyDataSetChanged();


                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            if (isforEdit) {
                showProgress();
            }
            examTermSpinner.clear();
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();

                                    getClassList();
                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getExamchapterList() {
        if (Util.isNetworkAvailable()) {
            if (isforEdit) {
                showProgress();
            }
            chapterList.clear();
            GetChapterListParam getChapterListParam = new GetChapterListParam();
            getChapterListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getChapterListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getChapterListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getChapterListParam.setClassID(String.valueOf(classId));
            getChapterListParam.setSubjectID(String.valueOf(subject_Id));
            vidyauraAPI.getExamChaptersList(getChapterListParam).enqueue(new Callback<ChapterListResponse>() {
                @Override
                public void onResponse(Call<ChapterListResponse> call, Response<ChapterListResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {
                        chapterListResponse = response.body();
                        if (chapterListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (chapterListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (chapterListResponse.getChaptersList().size() > 0) {
                                    for (int i = 0; i < chapterListResponse.getChaptersList().size(); i++) {
                                        chapterList.add(new ListofChapterforDialog(chapterListResponse.getChaptersList().get(i).getId()
                                                ,chapterListResponse.getChaptersList().get(i).getChapter_name(),0));
                                        // chapterspinnerArray.add(chapterListResponse.getChaptersList().get(i).getChapter_name());
                                    }

                                    chapterspinnerArray.notifyDataSetChanged();
                                    examComposechapterAdpater=new ExamComposechapterMultiSelectionAdpater(chapterList,getContext(), chapterDialog, dialogChapterButton, chapterList, selectAllCheckbox);
                                    chapterlistView.setAdapter(examComposechapterAdpater);
                                    //chapterspinnerArray.notifyDataSetChanged();

                                    if (!isforEdit) {
                                        subject_Id = examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                        getTopicList(String.valueOf(examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id()));
                                    }

                                } else {
                                    Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ChapterListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getTopicList(String chapterID) {
        if (Util.isNetworkAvailable()) {
            if (isforEdit) {
                showProgress();
            }
            topicspinnerArray.clear();
            topicList.clear();
            GetTopicListParam getTopicListParam = new GetTopicListParam();
            getTopicListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getTopicListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getTopicListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getTopicListParam.setClassID(String.valueOf(classId));
            getTopicListParam.setSubjectID(String.valueOf(subject_Id));
            getTopicListParam.setChapterID(chapterID);
            vidyauraAPI.getTopicsList(getTopicListParam).enqueue(new Callback<TopicListResponse>() {
                @Override
                public void onResponse(Call<TopicListResponse> call, Response<TopicListResponse> response) {
                    if (isforEdit) {
                        hideProgress();
                    }
                    if (response.body() != null) {
                        topicListResponse = response.body();
                        if (topicListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (topicListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (topicListResponse.getTopicsList().size() > 0) {
                                    for (int i = 0; i < topicListResponse.getTopicsList().size(); i++) {

                                        topicList.add(new ListofTopicforDialog(topicListResponse.getTopicsList().get(i).getId()
                                                ,topicListResponse.getTopicsList().get(i).getTopic_name(),0));
                                    }
                                    //topicspinnerArray.notifyDataSetChanged();
                                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                                    topiclistView.setAdapter(examComposetopicAdpater);
                                    if (examtypelist.size()<=0) {
                                        getExamtype();
                                    }


                                } else {
                                    Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TopicListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void datePickerDialog() {
        if (selectedDate == 1) currentD = Calendar.getInstance();
        else currentD = calendardate;

        String[] selectDateTime = examDetailByIdResponse.getExamScheduleList().get(0).getStart_time().split(" ");
        String[] select = selectDateTime[0].split("-");

    /*com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance((com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) this, currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
            currentD.get(Calendar.DAY_OF_MONTH));*/

        com.wdullaer.materialdatetimepicker.date.DatePickerDialog datePickerDialog = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance((com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) this,Integer.parseInt(select[0]),
                Integer.parseInt(select[1])-1, Integer.parseInt(select[2]));
        Calendar examStartcal = Calendar.getInstance();
        Calendar examEndcal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            examStartcal.setTime(sdf.parse(examStartdate));
            examEndcal.setTime(sdf.parse(examEnddate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datePickerDialog.setMinDate(examStartcal);
        datePickerDialog.setMaxDate(examEndcal);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String donateDate = simpleDateFormat.format(calendardate.getTime());
        Date dob = null;
        try {
            dob = simpleDateFormat.parse(donateDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formattedDate = simpleDateFormat.format(dob);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = currentDATE.format(c);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null, date2 = null;
        try {
            date1 = sdf.parse(currentDate);
            date2 = sdf.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        System.out.println("date1 : " + sdf.format(date1));
        System.out.println("date2 : " + sdf.format(date2));

        if (date1.compareTo(date2) > 0) {
            System.out.println("Date1 is after Date2");
            examdate_textview.setText(getDateFormat(formattedDate));
            //Toast.makeText(getContext(), "Invalid date selection", Toast.LENGTH_SHORT).show();
        } else if (date1.compareTo(date2) < 0) {
            System.out.println("Date1 is before Date2");
            examdate_textview.setText(getDateFormat(formattedDate));
        } else if (date1.compareTo(date2) == 0) {
            System.out.println("Date1 is equal to Date2");
            examdate_textview.setText(getDateFormat(formattedDate));
        } else {
            System.out.println("How to get here?");
        }


    }

    public void setExamdateToCalender(int mode)
    {
        if (mode==0)
        {
            examdate_textview.setText("");
            formattedDate = "";
        }
        else {
            examdate_textview.setText(getDateFormat(examStartdate));
            formattedDate = examStartdate;
        }


    }
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }


    public String getDateFormatwithTMS(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMM dd,yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getTimeFormatwithTMS(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getTimeFormatwithAMPM(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("hh:mm aa");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        simpleDateFormat2 = new SimpleDateFormat("hh:mm aa");
        if (selectedDate == 1) {
//            start date
            calendarStartTime = Calendar.getInstance();
            calendarStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarStartTime.set(Calendar.MINUTE, minute);
            eventStartTime = simpleDateFormat.format(calendarStartTime.getTime());
            examstarttime_textview.setText(simpleDateFormat2.format(calendarStartTime.getTime()));

            if (eventStartTime !=null && eventEndTime!=null) {
                boolean timecheck = checktimings(eventStartTime, eventEndTime);
                if (!timecheck)
                {

                    Toast.makeText(getContext(), "Invalid time selection", Toast.LENGTH_SHORT).show();
                    examstarttime_textview.setText("");

                }
                else
                {
                    try {
                        DurationBetweenTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("vikii", "ss" + timecheck);
            }

        } else if (selectedDate == 2) {
            //end date
            calendarEndTime = Calendar.getInstance();
            calendarEndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarEndTime.set(Calendar.MINUTE, minute);
            eventEndTime = simpleDateFormat.format(calendarEndTime.getTime());
            examendtime_textview.setText(simpleDateFormat2.format(calendarEndTime.getTime()));
            if (eventStartTime !=null && eventEndTime!=null) {
                boolean timecheck = checktimings(eventStartTime, eventEndTime);
                if (!timecheck)
                {
                    examendtime_textview.setText("");
                        Toast.makeText(getContext(), "Invalid time selection", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    try {
                        DurationBetweenTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("vikii", "ss" + timecheck);
            }


        }

    }

    public void DurationBetweenTime() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = simpleDateFormat.parse(eventStartTime);
        Date endDate = simpleDateFormat.parse(eventEndTime);

        long difference = endDate.getTime() - startDate.getTime();
        if(difference<0)
        {
            Date dateMax = simpleDateFormat.parse("24:00");
            Date dateMin = simpleDateFormat.parse("00:00");
            difference=(dateMax.getTime() -startDate.getTime() )+(endDate.getTime()-dateMin.getTime());
        }
        int days = (int) (difference / (1000*60*60*24));
        int hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);

        examDuration=hours+":"+min;



    }
    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            if(date1.before(date2)) {
                return true;
            }
            else if (date1.after(date2))
            {
                return true;
            }
            else {

                return false;
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return false;
    }

    private void timePickerDialog() {
        if (selectedDate == 2) {
            // end date
            currentT = Calendar.getInstance();
        } else if (selectedDate == 1) {
//            start date
            currentT = Calendar.getInstance();
        }
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this, currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.blue));
        if (selectedDate == 2) {
            //end date
            Date ec = Calendar.getInstance().getTime();
            SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");
            String ecurrentDate = edf.format(ec);
            if (ecurrentDate.equalsIgnoreCase(eventStartDateFormat)) {
                timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
            } else if (selectedDate == 1) {
                //start date
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = df.format(c);
                if (currentDate.equalsIgnoreCase(eventStartDateFormat)) {
                    timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                            currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
                }
            }


        }
        else
        {
            Date ec = Calendar.getInstance().getTime();
            SimpleDateFormat edf = new SimpleDateFormat("yyyy-MM-dd");
            String ecurrentDate = edf.format(ec);
            if (ecurrentDate.equalsIgnoreCase(eventStartDateFormat)) {
                timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                        currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
            } else if (selectedDate == 1) {
                //start date
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = df.format(c);
                if (currentDate.equalsIgnoreCase(eventStartDateFormat)) {
                    timePickerDialog.setMinTime(currentT.get(Calendar.HOUR_OF_DAY),
                            currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND));
                }
            }
        }
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }
}
