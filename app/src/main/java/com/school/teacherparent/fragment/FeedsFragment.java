package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.internal.NavigationMenu;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.ParentLeaveApplyActivity;
import com.school.teacherparent.activity.PaymentActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.FeedListParamsforSearch;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.activity.TestDrawerActivity.userImageview;

/**
 * Created by harini on 8/27/2018.
 */

public class FeedsFragment extends BaseFragment {
    RecyclerView feeds_recycle;
    FeedsAdapter feedsAdapter;
    List<String> theatersAdapter;
    Toolbar toolbar;
    FabSpeedDial fabSpeedDial,fab_menu_parent;
    SwipeRefreshLayout feedswipelayout;
    public boolean isLastPage = false;
    public int currentPage = 0;
    public boolean lastEnd;
    public boolean isLoading = true;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public ArrayList<FeedListResponse.feedResult> feedListResponsearray;
    ArrayList<FeedList> feedList = new ArrayList<>();
    ArrayList<String> stringFeedList = new ArrayList<>();
    /*ArrayList<FeedList> feedList;*/
    FeedListResponse feedListResponse;
    BaseActivity baseActivity;
    private boolean loading = true;
    private boolean isloading = true;
    int limit=5;
    int offset=0;
    Gson gson;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    int usetType;
    MenuItem item;
    ProgressDialog mDialog;
    Menu menu;
    NotificationReceiver notificationReceiver;
    UserLoginResponse userLoginResponse;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        currentPage = 0;
        lastEnd = false;


        if (sharedPreferences.getString(Constants.FROMADDFEED,"no").equals("yes"))
        {
            feedList.clear();
//            feedListResponsearray.clear();
            //feedList.clear();
            offset=0;
            editor.putString(Constants.FROMADDFEED,"no").commit();
            if (isloading) {
                stringFeedList.clear();
                getfeedList();
                isLoading=false;
            }

        }




    }

    @Override
    public void onResume() {
        super.onResume();
        feedList.clear();
        stringFeedList.clear();
        getfeedList();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible && isStarted) {
            isStarted = true;
            //isLoading = false;
            currentPage = 0;

            //getfeedList();
//            getTimeLineList();
//            if (sharedPreferences.getInt(DmkConstants.DESIGNATIONID, 0) == 1) {
//                getDistrictList();
//            }
//            getNotificationCount();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    TextView nofeedListTextview;
    LinearLayoutManager linearLayoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_feeds_list, container, false);
        baseActivity = (BaseActivity) getActivity();
        TestDrawerActivity.toolbar.setTitle(getString(R.string.feed));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        feeds_recycle = view.findViewById(R.id.feeds_recycle);
        feedswipelayout=view.findViewById(R.id.swipe_feed_line);
        feedListResponsearray=new ArrayList<FeedListResponse.feedResult>();
        feedList =new ArrayList<>();
        feedList=new ArrayList<>();
       // feedListResponsearray.clear();
        feedList.clear();
        gson=new Gson();
        setHasOptionsMenu(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        feeds_recycle.setItemAnimator(new DefaultItemAnimator());
        feeds_recycle.setLayoutManager(linearLayoutManager);
        feedsAdapter = new FeedsAdapter(feedList, getActivity(),baseActivity,FeedsFragment.this);
        feeds_recycle.setAdapter(feedsAdapter);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        nofeedListTextview=view.findViewById(R.id.nofeedListTextview);
        nofeedListTextview.setVisibility(View.GONE);
        feeds_recycle.setVisibility(View.GONE);
        usetType=sharedPreferences.getInt(Constants.USERTYPE,0);
        if (isLoading) {
            getfeedList();
        }
        //TestDrawerActivity.edittextleaveSearch.addTextChangedListener(new MyTextWatcher( TestDrawerActivity.edittextleaveSearch));
        feedswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                feedswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                            isLoading = false;
//                            currentPage = 0;
//                            lastEnd = false;
//                            timeLineList.clear();
//                            timeLineAdapter.notifyDataSetChanged();
//                            getTimeLineList();
//                        feedListResponsearray.clear();
                        TestDrawerActivity.toolbar.setTitle("Feed");
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        feedList.clear();
                        feedList.clear();
                        stringFeedList.clear();
                        feedsAdapter.notifyDataSetChanged();
                        offset=0;
                        getfeedList();
                        isLoading=false;
                        feedswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });


        feeds_recycle.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getfeedList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        fabSpeedDial = view.findViewById(R.id.fab_menu);
      fab_menu_parent=view.findViewById(R.id.fab_menu_parent);

        if (usetType==1) {
            fabSpeedDial.setVisibility(View.VISIBLE);
            fab_menu_parent.setVisibility(View.GONE);
        }
        else if (usetType==2)
        {
            fab_menu_parent.setVisibility(View.VISIBLE);
            fabSpeedDial.setVisibility(View.GONE);
        }

        feeds_recycle.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    //fabSpeedDial.setVisibility(View.VISIBLE);
                }
                else
                {
                   // fabSpeedDial.setVisibility(View.GONE);
                }
            }
        });
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                if (fabSpeedDial.isSelected()) {
                    Log.e("open", ">>");
                } else {
                    Log.e("close", ">>" + navigationMenu.toString().trim());
                }
                // TODO: Do something with yout menu items, or return false if you don't want to show them
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                String menuitem = menuItem.toString();
                if (menuitem.equalsIgnoreCase("Add New Feed")) {
                    //  getActivity().getResources().getString(R.string.add_feed))) {
                    Log.e("open", ">>" + menuItem);
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", 0));


                }
                else if (menuitem.equalsIgnoreCase("Attendance"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
                }
                else if (menuitem.equalsIgnoreCase("Homework"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "homework").putExtra("mode","create"));
                }
                else if (menuitem.equalsIgnoreCase("Classwork"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "classwork"));
                }
                else if (menuitem.equalsIgnoreCase("Events"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "events"));
                }
                else if (menuitem.equalsIgnoreCase("Exams"))
                {
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "exam"));
                }
                return super.onMenuItemSelected(menuItem);
            }

        });


        fab_menu_parent.setMenuListener(new SimpleMenuListenerAdapter() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {

                if (fab_menu_parent.isSelected()) {
                    Log.e("open", ">>");
                } else {
                    Log.e("close", ">>" + navigationMenu.toString().trim());
                }
                // TODO: Do something with yout menu items, or return false if you don't want to show them
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                String menuitem = menuItem.toString();
                if (menuitem.equalsIgnoreCase("Add New Feed")) {
                    //  getActivity().getResources().getString(R.string.add_feed))) {
                    Log.e("open", ">>" + menuItem);
                    startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "feed").putExtra("feedID", 0));


                }
                else if (menuitem.equalsIgnoreCase("Write Message"))
                {
                    MessageFragment fragment = new MessageFragment();
                    ((TestDrawerActivity) getActivity()).replaceFragment(fragment);
                    TestDrawerActivity.bottomNavigationView.setCurrentItem(2);
                    //startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "attendance"));
                }
                else if (menuitem.equalsIgnoreCase("Pay Fees"))
                {
                    startActivity(new Intent(getContext(), PaymentActivity.class));
                    //startActivity(new Intent(getContext(),SidemenuDetailActivity.class).putExtra("type","fees"));
                    //startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "homework").putExtra("mode","create"));
                }
                else if (menuitem.equalsIgnoreCase("Apply Leave"))
                {
                    startActivity(new Intent(getActivity(), ParentLeaveApplyActivity.class).putExtra("type", "classwork"));
                }

                return super.onMenuItemSelected(menuItem);
            }

        });
        TestDrawerActivity.edittextleaveSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(TestDrawerActivity.edittextleaveSearch);
            }
        });
        TestDrawerActivity.edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (  TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(  TestDrawerActivity.edittextleaveSearch.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);

                            getfeedsearch(TestDrawerActivity.edittextleaveSearch.getText().toString().toLowerCase());
                        }

                        return true;
                    }
                    else
                    {
                        Toast.makeText(getContext(), R.string.entersomething, Toast.LENGTH_SHORT).show();
                    }

                return false;
            }
        });

        userLogin();

        return view;
    }

    /*@Override
    public void onDetach() {
        super.onDetach();
        getfeedList();
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Do something that differs the Activity's menu here
        MenuItem item = menu.findItem(R.id.action_calender);
        item.setVisible(false);
        this.menu = menu;
        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            menu.getItem(3).setIcon(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                if ( TestDrawerActivity.edittextleaveSearch.getText().toString().length()<=0) {
                    TestDrawerActivity.toolbar.setVisibility(View.VISIBLE);
                    TestDrawerActivity.toolbar.setTitle("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.VISIBLE);
                    TestDrawerActivity.edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    TestDrawerActivity.edittextleaveSearch.setEnabled(true);
                    TestDrawerActivity.edittextleaveSearch.requestFocus();
                    showKeyboard(TestDrawerActivity.edittextleaveSearch);
                    TestDrawerActivity.edittextleaveSearch.setText("");
                }
                else
                {
                    //searchLeaveList( TestDrawerActivity.edittextleaveSearch.getText().toString().trim());

                    getfeedsearch(TestDrawerActivity.edittextleaveSearch.getText().toString().trim());

                }

                return true;

            case R.id.action_notification:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            case R.id.action_notifi:
                startActivity(new Intent(getContext(), NotificationActivity.class));
                return true;

            default:
                break;
        }

        return false;
    }





    public void searchLeaveList(String searchvalue)
    {
        hideSoftKeyboard(TestDrawerActivity.edittextleaveSearch);
        if (feedListResponse!=null && feedListResponse.getFeedResult().size()>0)
        {
            feedList.clear();
            for(int y=0;y<feedListResponse.getFeedResult().size();y++)
            {
                if (feedListResponse.getFeedResult().get(y).getTitle().toLowerCase().contains(searchvalue.toString().toLowerCase()))
                {

                    if (feedListResponsearray.get(y).getAuthor_type().equals("Admin"))
                    {
                        feedList.add(new FeedList(feedListResponsearray.get(y).getId(),feedListResponsearray.get(y).getTitle(),feedListResponsearray.get(y).getDescription(),feedListResponsearray.get(y).getAuthor_id(),feedListResponsearray.get(y).getAuthor_type(),
                                feedListResponsearray.get(y).getVisibility(),feedListResponsearray.get(y).getCreated_at(),feedListResponsearray.get(y).getSalutation(),"","",feedListResponsearray.get(y).getEmp_photo(),
                                feedListResponsearray.get(y).getFeed_id(),feedListResponsearray.get(y).getFeedLikeCount(),feedListResponsearray.get(y).getFeedCommentCount(),feedListResponsearray.get(y).getFeedShareCount(),feedListResponsearray.get(y).getIsUserLiked(),feedListResponsearray.get(y).getIsUserCommented(),feedListResponsearray.get(y).getSchoolName(),feedListResponsearray.get(y).getSchool_code(),feedListResponsearray.get(y).getLogo(),
                                feedListResponsearray.get(y).getName(),feedListResponsearray.get(y).getIsFeedPostedBy(),feedListResponsearray.get(y).getPublish_date(),feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                    }
                    else
                    {
                        feedList.add(new FeedList(feedListResponsearray.get(y).getId(),feedListResponsearray.get(y).getTitle(),feedListResponsearray.get(y).getDescription(),feedListResponsearray.get(y).getAuthor_id(),feedListResponsearray.get(y).getAuthor_type(),
                                feedListResponsearray.get(y).getVisibility(),feedListResponsearray.get(y).getCreated_at(),feedListResponsearray.get(y).getSalutation(),feedListResponsearray.get(y).getUserDetails().get(0).getFname(),feedListResponsearray.get(y).getUserDetails().get(0).getLname(),feedListResponsearray.get(y).getUserDetails().get(0).getEmp_photo(),
                                feedListResponsearray.get(y).getFeed_id(),feedListResponsearray.get(y).getFeedLikeCount(),feedListResponsearray.get(y).getFeedCommentCount(),feedListResponsearray.get(y).getFeedShareCount(),feedListResponsearray.get(y).getIsUserLiked(),feedListResponsearray.get(y).getIsUserCommented(),feedListResponsearray.get(y).getSchoolName(),feedListResponsearray.get(y).getSchool_code(),feedListResponsearray.get(y).getLogo(),
                                feedListResponsearray.get(y).getName(),feedListResponsearray.get(y).getIsFeedPostedBy(),feedListResponsearray.get(y).getPublish_date(),feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                    }

                }
            }

            if (feedList.size()>0)
            {
                /*feedsAdapter = new FeedsAdapter(feedList, getActivity(),baseActivity,FeedsFragment.this);
                feeds_recycle.setAdapter(feedsAdapter);*/
                feedsAdapter.notifyDataSetChanged();

            }
            else
            {

                //feeds_recycle.setAdapter(null);
                feedsAdapter.notifyDataSetChanged();
                Toast.makeText(getContext(),"No data found",Toast.LENGTH_SHORT).show();
            }


        }

        else
        {
            Toast.makeText(getContext(), R.string.noData, Toast.LENGTH_SHORT).show();
        }


    }

    private void showProgressDialog() {
        if (mDialog == null) {
            mDialog = new ProgressDialog(getContext());
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
        }
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void dismissProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public void getfeedList()
    {
        if (Util.isNetworkAvailable())
        {
            showProgressDialog();
            FeedListParams feedListParams=new FeedListParams();
            feedListParams.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedListParams.setLimit(limit);
            feedListParams.setOffset(offset);
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            Log.d("getfeedListparms", "getfeedList: "+gson.toJson(feedListParams));
            vidyauraAPI.getFeedList(feedListParams).enqueue(new Callback<FeedListResponse>() {
                @Override
                public void onResponse(Call<FeedListResponse> call, Response<FeedListResponse> response) {
                    /*if (mDialog != null && mDialog.isShowing()) {
                        mDialog.dismiss();
                    }*/
                    isLoading=true;
                    if (response.body() != null) {
                        feedListResponse = response.body();

                        if (feedListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            String json=gson.toJson(feedListResponse);
                            Log.d("response", "onResponse: "+json);
                            //feedListResponsearray.clear();
                            feedListResponsearray = response.body().getFeedResult();
                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (feedListResponsearray.size()!=0) {
                                    feeds_recycle.setVisibility(View.VISIBLE);
                                    nofeedListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<feedListResponsearray.size();y++)
                                    {
                                        if (feedListResponsearray.get(y).getAuthor_type().equals("Admin"))
                                        {
                                            if (!stringFeedList.contains(String.valueOf(feedListResponsearray.get(y).getFeed_id()))) {
                                                stringFeedList.add(String.valueOf(feedListResponsearray.get(y).getFeed_id()));
                                                feedList.add(new FeedList(feedListResponsearray.get(y).getId(), feedListResponsearray.get(y).getTitle(), feedListResponsearray.get(y).getDescription(), feedListResponsearray.get(y).getAuthor_id(), feedListResponsearray.get(y).getAuthor_type(),
                                                        feedListResponsearray.get(y).getVisibility(), feedListResponsearray.get(y).getCreated_at(), feedListResponsearray.get(y).getSalutation(), "", "", feedListResponsearray.get(y).getEmp_photo(),
                                                        feedListResponsearray.get(y).getFeed_id(), feedListResponsearray.get(y).getFeedLikeCount(), feedListResponsearray.get(y).getFeedCommentCount(), feedListResponsearray.get(y).getFeedShareCount(), feedListResponsearray.get(y).getIsUserLiked(), feedListResponsearray.get(y).getIsUserCommented(), feedListResponsearray.get(y).getSchoolName(), feedListResponsearray.get(y).getSchool_code(), feedListResponsearray.get(y).getLogo(),
                                                        feedListResponsearray.get(y).getName(), feedListResponsearray.get(y).getIsFeedPostedBy(), feedListResponsearray.get(y).getPublish_date(), feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                                            }
                                        }
                                        else
                                        {
                                            if (feedListResponsearray.get(y).getUserDetails().size() > 0)
                                                if (!stringFeedList.contains(String.valueOf(feedListResponsearray.get(y).getFeed_id()))) {
                                                    stringFeedList.add(String.valueOf(feedListResponsearray.get(y).getFeed_id()));
                                                    feedList.add(new FeedList(feedListResponsearray.get(y).getId(), feedListResponsearray.get(y).getTitle(), feedListResponsearray.get(y).getDescription(), feedListResponsearray.get(y).getAuthor_id(), feedListResponsearray.get(y).getAuthor_type(),
                                                            feedListResponsearray.get(y).getVisibility(), feedListResponsearray.get(y).getCreated_at(), feedListResponsearray.get(y).getSalutation(),
                                                            feedListResponsearray.get(y).getUserDetails().get(0).getFname(),
                                                            feedListResponsearray.get(y).getUserDetails().get(0).getLname(),
                                                            feedListResponsearray.get(y).getUserDetails().get(0).getEmp_photo(),
                                                            feedListResponsearray.get(y).getFeed_id(), feedListResponsearray.get(y).getFeedLikeCount(), feedListResponsearray.get(y).getFeedCommentCount(), feedListResponsearray.get(y).getFeedShareCount(), feedListResponsearray.get(y).getIsUserLiked(), feedListResponsearray.get(y).getIsUserCommented(), feedListResponsearray.get(y).getSchoolName(), feedListResponsearray.get(y).getSchool_code(), feedListResponsearray.get(y).getLogo(),
                                                            feedListResponsearray.get(y).getName(), feedListResponsearray.get(y).getIsFeedPostedBy(), feedListResponsearray.get(y).getPublish_date(), feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                                                }
                                        }

                                    }

                                    /*feedsAdapter = new FeedsAdapter(feedList, getActivity(),baseActivity,FeedsFragment.this);
                                    feeds_recycle.setAdapter(feedsAdapter);*/
                                    if (feedList.size() > 0) {
                                        System.out.println("feedList ==> "+ feedList.size());
                                        feeds_recycle.setVisibility(View.VISIBLE);
                                        feedsAdapter.notifyDataSetChanged();
                                        loading = true;
                                        dismissProgressDialog();
                                    }
                                }
                                else
                                {
                                    if (feedList.size() == 0) {
                                        nofeedListTextview.setVisibility(View.VISIBLE);
                                        feeds_recycle.setVisibility(View.GONE);
                                        loading = false;
                                    }
                                    dismissProgressDialog();
                                }
                            } else {
                                dismissProgressDialog();
                                Toast.makeText(getContext(), feedListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                 if (feedList==null && feedListResponsearray.size()==0)
                                {
                                    loading=false;
                                    Toast.makeText(getContext(), feedListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                        else
                        {
//                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            getActivity().finishAffinity();
                            dismissProgressDialog();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedListResponse.getToken()).commit();
                            getfeedList();

                        }

                    }
                    else
                    {
                        dismissProgressDialog();
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<FeedListResponse> call, Throwable t) {
                    dismissProgressDialog();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    public void getfeedsearch(final String searchstring)
    {
        if (Util.isNetworkAvailable())
        {
            hideSoftKeyboard(TestDrawerActivity.edittextleaveSearch);
            //feeds_recycle.setAdapter(null);
            feedsAdapter.notifyDataSetChanged();
            feedList.clear();
            showProgressDialog();
            FeedListParamsforSearch feedListParams=new FeedListParamsforSearch();
            feedListParams.setPhoneNumber(sharedPreferences.getString(Constants.PHONE,""));
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            feedListParams.setLimit(50);
            feedListParams.setOffset(offset);
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            feedListParams.setSearchString(searchstring);
            Log.d("getfeedListparms", "getfeedList: "+gson.toJson(feedListParams));
            vidyauraAPI.getFeedSearchList(feedListParams).enqueue(new Callback<FeedListResponse>() {
                @Override
                public void onResponse(Call<FeedListResponse> call, Response<FeedListResponse> response) {
                    mDialog.dismiss();
                    isLoading=true;
                    if (response.body() != null) {
                        feedListResponse = response.body();

                        if (feedListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            String json=gson.toJson(feedListResponse);
                            Log.d("response", "onResponse: "+json);
                            //feedListResponsearray.clear();
                            feedListResponsearray = response.body().getFeedResult();
                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (feedListResponsearray.size()!=0) {
                                    feeds_recycle.setVisibility(View.VISIBLE);
                                    nofeedListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<feedListResponsearray.size();y++)
                                    {
                                        if (feedListResponsearray.get(y).getAuthor_type().equals("Admin"))
                                        {
                                            if (!stringFeedList.contains(String.valueOf(feedListResponsearray.get(y).getFeed_id()))) {
                                                stringFeedList.add(String.valueOf(feedListResponsearray.get(y).getFeed_id()));
                                                feedList.add(new FeedList(feedListResponsearray.get(y).getId(), feedListResponsearray.get(y).getTitle(), feedListResponsearray.get(y).getDescription(), feedListResponsearray.get(y).getAuthor_id(), feedListResponsearray.get(y).getAuthor_type(),
                                                        feedListResponsearray.get(y).getVisibility(),
                                                        feedListResponsearray.get(y).getCreated_at(),
                                                        feedListResponsearray.get(y).getSalutation(), "", "", feedListResponsearray.get(y).getEmp_photo(),
                                                        feedListResponsearray.get(y).getFeed_id(), feedListResponsearray.get(y).getFeedLikeCount(), feedListResponsearray.get(y).getFeedCommentCount(), feedListResponsearray.get(y).getFeedShareCount(), feedListResponsearray.get(y).getIsUserLiked(), feedListResponsearray.get(y).getIsUserCommented(), feedListResponsearray.get(y).getSchoolName(), feedListResponsearray.get(y).getSchool_code(), feedListResponsearray.get(y).getLogo(),
                                                        feedListResponsearray.get(y).getName(), feedListResponsearray.get(y).getIsFeedPostedBy(), feedListResponsearray.get(y).getPublish_date(), feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                                            }
                                        }
                                        else
                                        {
                                            if (feedListResponsearray.get(y).getUserDetails() != null)
                                            if (feedListResponsearray.get(y).getUserDetails().size() > 0)
                                            if (!stringFeedList.contains(String.valueOf(feedListResponsearray.get(y).getFeed_id()))) {
                                                stringFeedList.add(String.valueOf(feedListResponsearray.get(y).getFeed_id()));
                                                feedList.add(new FeedList(feedListResponsearray.get(y).getId(), feedListResponsearray.get(y).getTitle(), feedListResponsearray.get(y).getDescription(), feedListResponsearray.get(y).getAuthor_id(), feedListResponsearray.get(y).getAuthor_type(),
                                                        feedListResponsearray.get(y).getVisibility(),
                                                        feedListResponsearray.get(y).getCreated_at(),
                                                        feedListResponsearray.get(y).getSalutation(),
                                                        feedListResponsearray.get(y).getUserDetails().get(0).getFname(), feedListResponsearray.get(y).getUserDetails().get(0).getLname(), feedListResponsearray.get(y).getUserDetails().get(0).getEmp_photo(),
                                                        feedListResponsearray.get(y).getFeed_id()
                                                        , feedListResponsearray.get(y).getFeedLikeCount(), feedListResponsearray.get(y).getFeedCommentCount(), feedListResponsearray.get(y).getFeedShareCount(), feedListResponsearray.get(y).getIsUserLiked(), feedListResponsearray.get(y).getIsUserCommented(), feedListResponsearray.get(y).getSchoolName(), feedListResponsearray.get(y).getSchool_code(), feedListResponsearray.get(y).getLogo(),
                                                        feedListResponsearray.get(y).getName()
                                                        , feedListResponsearray.get(y).getIsFeedPostedBy(), feedListResponsearray.get(y).getPublish_date(), feedListResponsearray.get(y).getAttachmentsList(),feedListResponsearray.get(y).getSchoolLogo()));
                                            }
                                        }

                                    }
                                    feeds_recycle.setVisibility(View.VISIBLE);
                                    /*feedsAdapter = new FeedsAdapter(feedList, getActivity(),baseActivity,FeedsFragment.this);
                                    feeds_recycle.setAdapter(feedsAdapter);*/
                                    feedsAdapter.notifyDataSetChanged();
                                    //feedsAdapter.notifyDataSetChanged();

                                    loading=true;
                                }
                                else if (feedList.size()==0 && feedListResponsearray.size()==0)
                                {
                                    nofeedListTextview.setVisibility(View.VISIBLE);
                                    feeds_recycle.setVisibility(View.GONE);
                                    loading=false;
                                    Toast.makeText(getContext(), getString(R.string.noData), Toast.LENGTH_SHORT).show();
                                }
                            } else {
//                                if (feedList.size()==0 && feedListResponsearray.size()==0)
//                                {
                                    //loading=false;
                                nofeedListTextview.setVisibility(View.VISIBLE);
                                feeds_recycle.setVisibility(View.GONE);
                                loading=false;
                                    Toast.makeText(getContext(), feedListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //}

                            }
                        }
                        else
                        {
//                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            getActivity().finishAffinity();
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedListResponse.getToken()).commit();
                            getfeedsearch(searchstring);

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<FeedListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading=true;
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            isLoading=true;
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void showKeyboard(final EditText ettext){
        fabSpeedDial.setVisibility(View.GONE);
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    private void hideSoftKeyboard(EditText ettext){
        fabSpeedDial.setVisibility(View.VISIBLE);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TestDrawerActivity.edittextleaveSearch.setText("");
        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
        dismissProgressDialog();
       // TestDrawerActivity.edittextleaveSearch.addTextChangedListener(null);
        Log.d("readas","adsfadf");
    }
    public class MyTextWatcher implements TextWatcher {
        private EditText et;

        // Pass the EditText instance to TextWatcher by constructor
        public MyTextWatcher(EditText et) {
            this.et = et;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Unregister self before update
            String enteredValue  = s.toString();
            if (enteredValue.length()>2)
            {
                getfeedsearch(enteredValue);
            }

        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            editor.putBoolean(Constants.NOTIFICATION_STATE,state);
            editor.apply();
            if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_orange_icon));
            } else {
                menu.getItem(3).setIcon(ContextCompat.getDrawable(context, R.mipmap.noti_white_icon));
            }
        }

    }

    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            Log.d("tag", "priya user Login: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    userLoginResponse = response.body();
                    Gson gson = new Gson();
                    System.out.println("priya response ==> "+gson.toJson(response.body()));
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus()!= 401) {
                            if (userLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                                if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                    editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                    editor.commit();

                                    String url = null;
                                    if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                        //url = Objects.requireNonNull(getContext()).getResources().getString(R.string.s3_baseurl) + getContext().getResources().getString(R.string.s3_employee) + "/" ;
                                        url = "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/" + "uploads/employee" + "/" ;
                                    } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                        //url = Objects.requireNonNull(getContext()).getResources().getString(R.string.s3_baseurl) + getContext().getResources().getString(R.string.s3_student_path) + "/" ;;
                                        url = "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/" + "uploads/student" + "/" ;
                                    }

                                    final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                    MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname()+" "+userLoginResponse.getData().get(0).getLname()).replace("null","")
                                            ,url+userLoginResponse.getData().get(0).getEmp_photo(),
                                            userLoginResponse.getEncyptedUserID(),
                                            FirebaseInstanceId.getInstance().getToken(),
                                            "senderRole",
                                            "senderClass");
                                    mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                    if (sharedPreferences.getString(Constants.PROFILE_PHOTO,"")!=null) {


                                        String url1 = null;
                                        if (usetType==1) {
                                            url1 = "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/" + "uploads/employee" + "/"  +
                                                    sharedPreferences.getString(Constants.PROFILE_PHOTO, "");
                                        }
                                        else if (usetType==2)
                                        {
                                            url1 = "https://s3.amazonaws.com/vidyaura-userfiles-mobilehub-1887134683/" + "uploads/student" + "/" +
                                                    sharedPreferences.getString(Constants.PROFILE_PHOTO, "");
                                        }

                                        Picasso.get().load(url1).


                                                into(userImageview, new com.squareup.picasso.Callback() {
                                                    @Override
                                                    public void onSuccess() {


                                                    }

                                                    @Override
                                                    public void onError(Exception e) {
                                                        if (getActivity() != null) {
                                                            userImageview.setImageDrawable(Objects.requireNonNull(getActivity()).getResources().getDrawable(R.drawable.ic_user));
                                                        }
                                                    }
                                                });
                                    }
                                    else
                                    {
                                        userImageview.setImageDrawable(Objects.requireNonNull(getContext()).getResources().getDrawable(R.drawable.ic_user));
                                    }
                                    Log.d("device", "onResponse: " + userLoginResponse.getSecuredToken());
                                } else {
                                    Toast.makeText(getContext(), userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                                userLogin();
                            }
                        } else
                        {

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}

