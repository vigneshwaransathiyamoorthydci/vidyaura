package com.school.teacherparent.fragment;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.LoginActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginUserTypeFragment extends BaseFragment {


    private Button submit;
    private int usertype = 0;
    private ImageView checkTeacher, checkParent;
    LinearLayout teacherLinearlayout,parentLinearlayout;
    public LoginUserTypeFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    UserLoginResponse userLoginResponse;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_usertype, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        checkTeacher = view.findViewById(R.id.check_parent);
        checkParent = view.findViewById(R.id.check_teacher);
        submit = view.findViewById(R.id.submit);
        teacherLinearlayout=view.findViewById(R.id.teacherLinearlayout);
        parentLinearlayout=view.findViewById(R.id.parentLinearlayout);
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (usertype!=0) {
                    secureTokenSharedPreferenceseditor.commit();
                    editor.commit();
                    if (sharedPreferences.getString(Constants.SECURITYPIN, "").length()>0)
                    {
                        if (sharedPreferences.getInt(Constants.SECURITYPINSTATUS, 0) == 0) {
                            userLogin();
                        } else {
                            Fragment fragment = new SecurityPinFragment();
                            replaceFragment(fragment);
                        }
                    }
                    else
                    {
                        Fragment fragment = new SecurityPinFragment();
                        replaceFragment(fragment);
                    }

                    /*if (sharedPreferences.getInt(Constants.SECURITYPINSTATUS, 0) == 0)
                    {
                        userLogin();
                    }
                    else
                    {
                        Fragment fragment = new SecurityPinFragment();
                        replaceFragment(fragment);
                    }*/

                }
                else
                {
                    Toast.makeText(getActivity(),"Please select Parent or Teacher",Toast.LENGTH_SHORT).show();
                }
            }
        });


        teacherLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkParent.setImageResource(R.mipmap.tick_icon);//chek
                checkTeacher.setImageResource(R.mipmap.untick_icon);//uncheck
                usertype=1;
                editor.putInt(Constants.USERTYPE, usertype);
                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, secureTokenSharedPreferences.getString(Constants.SECURE_TOKEN_TEACHER,""));

            }
        });

        parentLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkParent.setImageResource(R.mipmap.untick_icon);//chek
                checkTeacher.setImageResource(R.mipmap.tick_icon);//uncheck
                usertype=2;
                editor.putInt(Constants.USERTYPE, usertype);
                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, secureTokenSharedPreferences.getString(Constants.SECURE_TOKEN_PAREENTS,""));


            }
        });


        return view;
    }
    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }


    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    Gson gson = new Gson();
                    System.out.println("response ==> "+gson.toJson(response.body()));
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus()!= 401) {
                        if (userLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, userLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, userLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, userLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, userLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, userLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, userLoginResponse.getData().get(0).getEmail());
                                editor.putString(Constants.GENDER, userLoginResponse.getData().get(0).getGender());
                                editor.putString(Constants.EMP_ID, userLoginResponse.getData().get(0).getEmp_id());
                                editor.putInt(Constants.ID, userLoginResponse.getData().get(0).getId());
                                editor.putString(Constants.DOB, userLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                editor.putString(Constants.JOINING_DATE, userLoginResponse.getData().get(0).getJoining_date());
                                if (userLoginResponse.getAppSettings() != null) {
                                    if (userLoginResponse.getAppSettings().size() > 0) {
                                        editor.putInt(Constants.NOTIFICATIONSTATUS, userLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                        editor.putInt(Constants.MESSAGESTATUS, userLoginResponse.getAppSettings().get(0).getMessageStatus());
                                        editor.putInt(Constants.SECURITYPINSTATUS, userLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                    }
                                }
                                editor.putInt(Constants.HASCHILDREN, userLoginResponse.getHasChildren());
                                editor.putString(Constants.SCHOOL_LOGO, userLoginResponse.getSchoolLogo());
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    editor.putInt(Constants.TOTAL_LEVEL_POINTS, userLoginResponse.getTotalLevelPoints());
                                    editor.putString(Constants.POINTS_NAME, userLoginResponse.getTeacherLevel().getName());
                                    editor.putString(Constants.BADGE_IMAGE, userLoginResponse.getTeacherLevel().getBadge_image());
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, userLoginResponse.getData().get(0).getType());
                                }
                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,userLoginResponse.getData().getPhone());
                                editor.commit();

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname()+" "+userLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+userLoginResponse.getData().get(0).getEmp_photo(),
                                        userLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                startActivity(new Intent(getActivity(), TestDrawerActivity.class));
                                getActivity().finish();
                                Log.d("device", "onResponse: " + userLoginResponse.getSecuredToken());
                            } else {
                                Toast.makeText(getActivity(), userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                            userLogin();
                        }
                    } else
                    {
                        hideProgress();
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getActivity(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

}
