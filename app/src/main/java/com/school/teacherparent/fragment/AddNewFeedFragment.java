package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.CircularComposeclassAdpater;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.adapter.SelectedImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.FeedCommentListResponse;
import com.school.teacherparent.models.FeedDetailsByIDResponse;
import com.school.teacherparent.models.FeedDetailsParams;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.models.UpdateFeedParams;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by harini on 9/10/2018.
 */

public class AddNewFeedFragment extends BaseFragment implements OnImageRemoved {


    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    TextView add_feed;
    ImageView back;
    boolean valid = false;
    EditText feedtitleTextview;
    EditText feeddescriptionTextview;
    LinearLayout cardview_upload;
    Spinner feedvisiblityspinner;
    CheckBox feedaddToGallery;
    TextView addfeed;
    int addTogallery = 0;
    int visibility = 0;
    int feedId;
    ImageView imagetemp;
    Uri uri;
    String imgname = "";
    List<String> paths;
    RecyclerView image_recycle;
    SelectedImageAdapter selectedImageAdapter;
    List<SelectedImageList> imageList = new ArrayList<>();
    List<AddFeedParams.feedAttachment>feedAttachments=new ArrayList<>();
    @RequiresApi(api = Build.VERSION_CODES.O)
    public AddNewFeedFragment() {
        // Required empty public constructor
    }
    List<ListofClass> classList = new ArrayList<ListofClass>();
    private ArrayAdapter<String> multiSpinneradapter;
    boolean[] selectedclassItems;
    LinearLayout privatevisiblityLinearlayout;
    TextView classnameTextview,searchnameTextview;
    ListView classlistView;
    Button dialogDistrictButton;
    Dialog classDialog;
    SearchView dialogSearchView;
    String classID = "";
    List<ListofClass> searchclassList = new ArrayList<ListofClass>();
    CircularComposeclassAdpater circularComposeclassAdpater;
    FeedDetailsByIDResponse feedDetailsByIDResponse;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_addnewfeed, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_new_feed));
        secureTokenSharedPreferences = getContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        editor = sharedPreferences.edit();
        cardview_upload = view.findViewById(R.id.upload_linear);
        imagetemp = view.findViewById(R.id.imagetemp);
        init(view);
        addfeed = view.findViewById(R.id.add_feed);
        image_recycle = view.findViewById(R.id.image_recycle);
        selectedImageAdapter = new SelectedImageAdapter(imageList, getActivity(), this);
        multiSpinneradapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item);
        selectedclassItems = new boolean[multiSpinneradapter.getCount()];
        //LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),5);
        image_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        image_recycle.setAdapter(selectedImageAdapter);
        privatevisiblityLinearlayout=view.findViewById(R.id.privatevisiblityLinearlayout);
        privatevisiblityLinearlayout.setVisibility(View.GONE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            feedId = bundle.getInt("feedId", 0);
            if (feedId > 0) {
                addfeed.setText(getString(R.string.update_feed));
                SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_feed));
                getFeedDetails();
            }

        }

        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Name", "Public");
        map.put("Icon", R.mipmap.visiblity_icon);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("Name", "Private");
        map.put("Icon", R.mipmap.privacy_icon);
        list.add(map);
        feedvisiblityspinner = view.findViewById(R.id.feedvisiblityspinner);

        myAdapter adapter = new myAdapter(getActivity(), list,
                R.layout.spinner_dropdown_item_image, new String[]{"Name", "Icon"},
                new int[]{R.id.name, R.id.icon});

        feedvisiblityspinner.setAdapter(adapter);
        feedvisiblityspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                visibility = position;
                if (visibility==1) {
                    getClassList();
                }
                else if (visibility==0)
                {
                    classID="";
                    classnameTextview.setText("");
                    privatevisiblityLinearlayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        classDialog = new Dialog(getContext());
        classDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        classDialog.setCancelable(false);
        classDialog.setContentView(R.layout.custom_dialog);
        searchnameTextview=(TextView)classDialog.findViewById(R.id.searchnameTextview);
        classlistView = (ListView) classDialog.findViewById(R.id.listView1);
        dialogDistrictButton = (Button) classDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)classDialog.findViewById(R.id.dialogSearchView);
        classnameTextview=view.findViewById(R.id.classnameTextview);
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        dialogSearchView.setQueryHint(getString(R.string.enter_class));
        classnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classDialog.show();
            }
        });
        dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String className="";
                classID="";
                for (int y=0;y<classList.size();y++)
                {

                    if (classList.get(y).getSelectedID()!=0) {
                        className = className.concat(String.valueOf(classList.get(y).getClassname()+classList.get(y).getClasssection()).concat(","));
                        classID=classID.concat(String.valueOf(classList.get(y).getClasssectionid())).concat(",");
                    }

                }
                if (className.length()>0 && classID.length()>0) {
                    className = className.substring(0, className.length() - 1);
                    classID = classID.substring(0, classID.length() - 1);
                    classnameTextview.setText(className);
                    classnameTextview.setError(null);

                }
                else
                {
                    classnameTextview.setText("");
                }
                classDialog.hide();
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1)
                {
//                    Collections.sort(classList, new Comparator<ListofClass>() {
//
//                        @Override
//                        public int compare(ListofClass lhs, ListofClass rhs) {
//                            //here getTitle() method return app name...
//                            return newText.compareTo(lhs.getClassname());
//
//                        }
//
//
//                    });
                    searchclassList.clear();
                    for(int y=0;y<classList.size();y++)
                    {
                        if (classList.get(y).getClassname().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchclassList.add(classList.get(y));
                            Log.d("SEARCH","classLIST"+classList.get(y).getClassname());
                        }
                    }
                    if (searchclassList.size()>0) {
                        circularComposeclassAdpater = new CircularComposeclassAdpater(searchclassList, getContext());
                        classlistView.setAdapter(circularComposeclassAdpater);
                        circularComposeclassAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        classlistView.setVisibility(View.GONE);
                        searchnameTextview.setVisibility(View.VISIBLE);
                    }

                }
                else if (newText.toString().length()==0)
                {
                    classlistView.setVisibility(View.VISIBLE);
                    searchnameTextview.setVisibility(View.GONE);
                    circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                    classlistView.setAdapter(circularComposeclassAdpater);
                    circularComposeclassAdpater.notifyDataSetChanged();
                }
                return false;
            }
        });


        return view;

    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {

        for (int i=0;i<path.size();i++)
        {
            imageList.add(new SelectedImageList(path.get(i),true));
        }
        if (imageList.size() > 10) {
            Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }


    }


    private void init(View view) {

        feedtitleTextview = view.findViewById(R.id.feedtitleTextview);
        feeddescriptionTextview = view.findViewById(R.id.feeddescriptionTextview);

        feedaddToGallery = view.findViewById(R.id.feedaddToGallery);
        addfeed = view.findViewById(R.id.add_feed);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "proximanova_semibold.otf");
        feedaddToGallery.setTypeface(font);

        addfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedaddToGallery.isChecked()) {
                    addTogallery = 1;
                } else {
                    addTogallery = 0;
                }
                if (validate()) {

                        ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_feeds_path),
                                imgname, new OnAmazonFileuploaded() {
                                    @Override
                                    public void FileStatus(int status, String filename) {
                                        if (status == 1) {
                                            if (feedId == 0) {
                                                addFeed();

                                            }
                                            else {

                                                UpdateFeed();

                                            }
                                        } else {
                                            Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });





                }

            }
        });
        cardview_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageList.size()>=5){
                    Toast.makeText(getActivity(), R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .scale(500,500)
                        .enableDebuggingMode(true)
                        .build();*/

            }
        });


    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public boolean validate() {
        String feeddescription = feeddescriptionTextview.getText().toString().trim();
        String title = feedtitleTextview.getText().toString().trim();

//        if (title.length() == 0 || title.length() <= 0) {
//            feedtitleTextview.setError(getString(R.string.Enterfeedtitle));
//            valid = false;
//
//        } else if (feeddescription.length() == 0 || feeddescription.length() <= 0) {
//            feeddescriptionTextview.setError(getString(R.string.Enterdescription));
//            valid = false;
//        }
        if (visibility==1)
        {
            if (classID.length()>0)
            {
                if(imageList.size()<=0)
                {
                    Toast.makeText(getActivity(), R.string.attachmedia, Toast.LENGTH_SHORT).show();
                    valid = false;
                }



                else {
                    feeddescriptionTextview.setError(null);
                    feedtitleTextview.setError(null);
                    valid = true;
                }

            }
            else
            {
                valid = false;
                classnameTextview.setError(getString(R.string.selectclass));
            }
        }
        else
        {
            if(imageList.size()<=0)
            {
                Toast.makeText(getActivity(), R.string.attachmedia, Toast.LENGTH_SHORT).show();
                valid = false;
            }



            else {
                feeddescriptionTextview.setError(null);
                feedtitleTextview.setError(null);
                valid = true;
            }

        }

        return valid;
    }

    private void getFeedDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();

            FeedDetailsParams feedListParams = new FeedDetailsParams();
            feedListParams.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            feedListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            feedListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            feedListParams.setFeedID(String.valueOf(feedId));
            feedListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getFeedDetails(feedListParams).enqueue(new Callback<FeedDetailsByIDResponse>() {
                @Override
                public void onResponse(Call<FeedDetailsByIDResponse> call, Response<FeedDetailsByIDResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        feedDetailsByIDResponse = response.body();
                        if (feedDetailsByIDResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (feedDetailsByIDResponse.getStatus() == Util.STATUS_SUCCESS) {
                                //Toast.makeText(getContext(), feedDetailsByIDResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                feedtitleTextview.setText(feedDetailsByIDResponse.getFeedResult().get(0).getTitle());
                                feeddescriptionTextview.setText(feedDetailsByIDResponse.getFeedResult().get(0).getDescription());
                                if (feedDetailsByIDResponse.getFeedResult().get(0).getVisibility().equals("public")) {
                                    feedvisiblityspinner.setSelection(0);
                                } else {
                                    feedvisiblityspinner.setSelection(1);

                                }
                                if (feedDetailsByIDResponse.getFeedResult().get(0).getAdd_gallery() == 1) {
                                    addTogallery = 1;
                                    feedaddToGallery.setChecked(true);

                                } else {
                                    addTogallery = 0;
                                    feedaddToGallery.setChecked(false);
                                }
                                if (feedDetailsByIDResponse.getFeedResult().get(0).getAttachmentsList()!=null&&
                                        feedDetailsByIDResponse.getFeedResult().get(0).getAttachmentsList().size()!=0){
                                    for (String data:feedDetailsByIDResponse.getFeedResult().get(0).getAttachmentsList()){
                                        //imageList.add(getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+data);

                                        imageList.add(new SelectedImageList(getString(R.string.s3_baseurl)+getString(R.string.s3_feeds_path)+"/"+data,false));

                                    }



                                }

                                selectedImageAdapter.notifyDataSetChanged();

                            } else {
                                Toast.makeText(getContext(), feedDetailsByIDResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {

                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                        getActivity().finish();


                    }
                }

                @Override
                public void onFailure(Call<FeedDetailsByIDResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            });
        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("feedAttachmentExtension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }
    private void addFeed() {
        if (Util.isNetworkAvailable()) {
            showProgress();

//            JSONObject finalobject = new JSONObject();
//            try {
//                finalobject.put("feedMultipleAttachment", jsonArray);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            AddFeedParams addFeedParams = new AddFeedParams();
            addFeedParams.setTitle(feedtitleTextview.getText().toString().trim());
            addFeedParams.setFeedDesc(feeddescriptionTextview.getText().toString().trim());
            addFeedParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            addFeedParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
          //  addFeedParams.setFeedAttachment("testing");
            addFeedParams.setAddGallery(String.valueOf(addTogallery));
            addFeedParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            addFeedParams.setVisibility(String.valueOf(visibility));
            addFeedParams.setFeedMultipleAttachment(getImagelist());
            addFeedParams.setSectionIDs(classID);
            Gson gson=new Gson();
            String input=gson.toJson(addFeedParams);
            Log.d("addParms", "addFeed: "+input);

            vidyauraAPI.addFeed(addFeedParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    classDialog.dismiss();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {

                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                                sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                                sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                                sendNotification.setType("feeds");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        System.out.println("addFeedResponse1 ==> "+gson.toJson(response.body()));
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            Toast.makeText(getContext(), "Feed Added Successfully", Toast.LENGTH_SHORT).show();
                                            editor.putString(Constants.FROMADDFEED, "yes").commit();
                                            getActivity().finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });


                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
//                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            getActivity().finishAffinity();

                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, addFeedResponse.getToken()).commit();
                            addFeed();
                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void UpdateFeed() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UpdateFeedParams updateFeedParams = new UpdateFeedParams();
            updateFeedParams.setTitle(feedtitleTextview.getText().toString().trim());
            updateFeedParams.setFeedDesc(feeddescriptionTextview.getText().toString().trim());
            updateFeedParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            updateFeedParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
           // updateFeedParams.setFeedAttachment("testing");
            updateFeedParams.setAddGallery(String.valueOf(addTogallery));
            updateFeedParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            updateFeedParams.setVisibility(String.valueOf(visibility));
            updateFeedParams.setFeedID(String.valueOf(feedId));
            updateFeedParams.setFeedMultipleAttachment(getImagelist());
            updateFeedParams.setSectionIDs(classID);
            vidyauraAPI.updateFeed(updateFeedParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.FROMADDFEED, "yes").commit();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, addFeedResponse.getToken()).commit();
                            UpdateFeed();
                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onimageremoved(int pos, View view) {
        if (view.getId() == R.id.close) {
            imageList.remove(pos);
            selectedImageAdapter.notifyItemRemoved(pos);
        }
        //Toast.makeText(getActivity(), "calles", Toast.LENGTH_SHORT).show();

    }

    private class myAdapter extends SimpleAdapter {

        public myAdapter(Context context, List<? extends Map<String, ?>> data,
                         int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.spinner_dropdown_item_image,
                        null);
            }

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            ((TextView) convertView.findViewById(R.id.name))
                    .setText((String) data.get("Name"));
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageResource((Integer) data.get("Icon"));

            return convertView;
        }

    }

    private void getClassList()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classList.clear();
            GetclassListParams getclassListParams=new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        ClassListResponse classListResponse=response.body();
                        if (classListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i=0;i<classListResponse.getTeachersClassesList().size();i++)
                                {
                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
                                            classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
                                            classListResponse.getTeachersClassesList().get(i).getSectionID(),false,0));
                                    multiSpinneradapter.add(  classListResponse.getTeachersClassesList().get(i).getClassName()+" "+
                                            classListResponse.getTeachersClassesList().get(i).getSection());
                                }
                                privatevisiblityLinearlayout.setVisibility(View.VISIBLE);
                                circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                                classlistView.setAdapter(circularComposeclassAdpater);
                                //circularComposeclassAdpater=new CircularComposeclassAdpater(classList,getContext());
                                //classlistView.setAdapter(circularComposeclassAdpater);


                                if (feedId > 0) {
                                    if (feedDetailsByIDResponse.getFeedResult().get(0).getVisibility().equals("private")) {
                                        String[] chapterstrArray = feedDetailsByIDResponse.getFeedResult().get(0).getSection_id().split(",");
                                        String className = "";
                                        for (int i = 0; i < classList.size(); i++) {
                                            for (int u = 0; u < chapterstrArray.length; u++) {
                                                int selectedclassID = Integer.parseInt(chapterstrArray[u]);
                                                if (classList.get(i).getClasssectionid() == selectedclassID) {

                                                    className = className.concat(classList.get(i).getClassname() + classList.get(i).getClasssection()).concat(",");
                                                    classID = classID.concat(String.valueOf(classList.get(u).getClasssectionid())).concat(",");
                                                    classList.get(i).setSelectedID(classList.get(i).getClasssectionid());


                                                }
                                            }

                                        }

                                        if (className.length() > 0 && classID.length() > 0) {
                                            className = className.substring(0, className.length() - 1);
                                            classID = classID.substring(0, classID.length() - 1);
                                            classnameTextview.setText(className);
                                        }

                                    }

                                }




                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });


        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}
