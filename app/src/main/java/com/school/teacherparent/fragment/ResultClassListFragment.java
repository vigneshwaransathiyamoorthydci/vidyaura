package com.school.teacherparent.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.ResultClassListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamTermResponse;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetResultDetailsParams;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.ResultClassListDTO;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ResultDetailsResponse;
import com.school.teacherparent.models.SubjectListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultClassListFragment extends BaseFragment {


    private Spinner exam_spinner, sub_spinner;
    private RecyclerView resultDetailsrecyclerView;
    private ResultClassListAdapter resultDetailsAdapter;
    private List<ResultClassListDTO> resclassList = new ArrayList<>();
    FloatingActionButton addResult;
    ImageView noti, back;

    public ResultClassListFragment() {
        // Required empty public constructor
    }
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ExamTermResponse examTermResponse;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    int classID,sectionID;
    String altname,Section;
    public TextView titleTextview;
    SubjectListResponse subjectListResponse;
    ResultDetailsResponse resultDetailsResponse;
    ArrayList<String> subjectlist = new ArrayList<String>();
    ArrayAdapter<String> subjectspinnerArray;
    int examTermID=0;
    int subjectId=0;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ResultDetailsResponse.resultDetailsList> resultDetailsLists=new ArrayList<>();
    ImageView result_filter;
    boolean filter=false;
    ProgressDialog mDialog;
    NotificationReceiver notificationReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_class, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        titleTextview=(TextView)view.findViewById(R.id.titleTextview);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        result_filter=(ImageView)view.findViewById(R.id.result_filter);
        result_filter.setImageResource(R.mipmap.sort_dec);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            classID = bundle.getInt("classID");
            sectionID = bundle.getInt("sectionID");
            Section = bundle.getString("Section");
            altname = bundle.getString("altname");




        }
        result_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resultDetailsLists.size()>0) {
                    if (filter) {
                        Collections.sort(resultDetailsLists, new Comparator<ResultDetailsResponse.resultDetailsList>() {
                            @Override
                            public int compare(ResultDetailsResponse.resultDetailsList lhs, ResultDetailsResponse.resultDetailsList rhs) {

                                return Integer.valueOf(lhs.getMarks()).compareTo(rhs.getMarks());
                            }
                        });
                        filter = false;

                        result_filter.setImageResource(R.mipmap.sort_asc);

                    } else {
                        Collections.sort(resultDetailsLists, new Comparator<ResultDetailsResponse.resultDetailsList>() {
                            @Override
                            public int compare(ResultDetailsResponse.resultDetailsList lhs, ResultDetailsResponse.resultDetailsList rhs) {

                                return Integer.valueOf(rhs.getMarks()).compareTo(lhs.getMarks());
                            }
                        });
                        filter = true;
                        result_filter.setImageResource(R.mipmap.sort_dec);
                    }

                    resultDetailsAdapter.notifyDataSetChanged();
                }


            }
        });
        titleTextview.setText(altname+" "+Section);
        exam_spinner = (Spinner) view.findViewById(R.id.exam_spinner);

        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(spinnerexamTermAdapter);

        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            getContext().registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(getContext(), R.mipmap.noti_white_icon));
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ResultFragment();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });



        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub

                if (position == 0) {
                    examTermID = 0;
                } else {
                    examTermID = examTermResponse.getExamTermsList().get(position - 1).getId();
                    if (subjectId!=0) {
                        getResultdetailsList();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subjectspinnerArray);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    subjectId = 0;
                } else {
                    subjectId = subjectListResponse.getTeachersSubjectsList().get(position - 1).getSubject_id();
                    if (examTermID!=0) {
                        getResultdetailsList();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        resultDetailsrecyclerView = (RecyclerView) view.findViewById(R.id.recycle_resultclass_members);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        resultDetailsrecyclerView.setItemAnimator(new DefaultItemAnimator());
        resultDetailsrecyclerView.setLayoutManager(linearLayoutManager);

        resultDetailsAdapter = new ResultClassListAdapter(resultDetailsLists, getActivity(),ResultClassListFragment.this,
                classID,sectionID,altname,Section);
        resultDetailsrecyclerView.setAdapter(resultDetailsAdapter);

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        getExamtermList();

        addResult = view.findViewById(R.id.add_result);

        addResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).putExtra("type", "result"));
            }
        });
       

        resultDetailsAdapter.setOnClickListen(new ResultClassListAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment = new ResultStudentFragment();
                replaceFragment(fragment);
            }

        });

       /* resultDetailsAdapter.setOnClickListen(new ResultHomeAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment=new ResultClassFragment();
                replaceFragment(fragment);
            }


        });

*/
        return view;

    }


    public void resetSpinner()
    {
        exam_spinner.setSelection(exam_spinner.getSelectedItemPosition());
        sub_spinner.setSelection(sub_spinner.getSelectedItemPosition());
        System.out.println("spinner ==> "+exam_spinner.getSelectedItemPosition());
        System.out.println("spinner ==> "+sub_spinner.getSelectedItemPosition());
    }
    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }



    private void getExamtermList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            //showProgress();
            //examTermSpinner.clear();
            if (!examTermSpinner.contains("Select Exam"))
            examTermSpinner.add("Select Exam");
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
                @Override
                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        examTermResponse = response.body();
                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (examTermResponse.getExamTermsList().size() > 0) {
                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
                                        if (!examTermSpinner.contains(examTermResponse.getExamTermsList().get(i).getExam_title()))
                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();
                                    getSubjecyList();

                                } else {
                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            //subjectlist.clear();
            if (!subjectlist.contains("Select Subject"))
            subjectlist.add("Select Subject");
            GetSubjectListParam getSubjectListParam = new GetSubjectListParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classID));
            getSubjectListParam.setSectionID(String.valueOf(sectionID));
            vidyauraAPI.getTeachersSubjects(getSubjectListParam).enqueue(new Callback<SubjectListResponse>() {
                @Override
                public void onResponse(Call<SubjectListResponse> call, Response<SubjectListResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        subjectListResponse = response.body();
                        if (subjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (subjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < subjectListResponse.getTeachersSubjectsList().size(); i++) {
                                    if (!subjectlist.contains(subjectListResponse.getTeachersSubjectsList().get(i).getName()))
                                    subjectlist.add(subjectListResponse.getTeachersSubjectsList().get(i).getName());
                                }
//                                subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                //                              classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                //chapter_Id=examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();
                                subjectspinnerArray.notifyDataSetChanged();
                                // getExamchapterList();

                            } else {
                                Toast.makeText(getContext(), subjectListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SubjectListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getResultdetailsList() {
        if (Util.isNetworkAvailable()) {
            resultDetailsLists.clear();
            showProgressDialog();
            //showProgress();
            filter = true;
            result_filter.setImageResource(R.mipmap.sort_dec);
            GetResultDetailsParams getResultDetailsParams = new GetResultDetailsParams();
            getResultDetailsParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getResultDetailsParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getResultDetailsParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getResultDetailsParams.setClassID(String.valueOf(classID));
            getResultDetailsParams.setSectionID(String.valueOf(sectionID));
            getResultDetailsParams.setExamTermID(String.valueOf(examTermID));
            getResultDetailsParams.setSubjectID(String.valueOf(subjectId));
            vidyauraAPI.getResultDetailsList(getResultDetailsParams).enqueue(new Callback<ResultDetailsResponse>() {
                @Override
                public void onResponse(Call<ResultDetailsResponse> call, Response<ResultDetailsResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        resultDetailsResponse = response.body();

                        Gson gson = new Gson();
                        String respon = gson.toJson(response.body());
                        System.out.println("response ==> "+respon);

                        if (resultDetailsResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (resultDetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (resultDetailsResponse.getResultDetailsList().size()>0)
                                {
                                    resultDetailsLists=resultDetailsResponse.getResultDetailsList();
                                    resultDetailsAdapter = new ResultClassListAdapter(resultDetailsLists, getActivity(), ResultClassListFragment.this, classID, sectionID, altname, Section);
                                    resultDetailsrecyclerView.setAdapter(resultDetailsAdapter);
                                    resultDetailsAdapter.notifyDataSetChanged();
                                }
                            } else {
                                resultDetailsAdapter.notifyDataSetChanged();
                                Toast.makeText(getContext(), resultDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResultDetailsResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
