package com.school.teacherparent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ExamUpcomingAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamUpcomingList;
import com.school.teacherparent.models.ExamUpcomingResponse;
import com.school.teacherparent.models.UpcomingExamListParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ExamFragment.selected;
import static com.school.teacherparent.fragment.ExamFragment.selectedSchID;

public class ParentExamUpcomingFragment extends BaseFragment {

    SwipeRefreshLayout swipeExamUpcoming;
    RecyclerView rvExamUpcoming;
    TextView tvNoExamUpcoming;
    LinearLayoutManager linearLayoutManager;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    public ArrayList<ExamUpcomingResponse.examsHistoryList> upcomingexamResponsearray = new ArrayList<>();
    static ArrayList<ExamUpcomingList> examHistoryListArrayList=new ArrayList<>();
    private ExamUpcomingAdapter upcomingexmmAdapter;

    private Boolean isVisible = false;
    int limit=5;
    static int offset=0;
    boolean service=true;
    private boolean loading = true;
    public boolean lastEnd;
    public boolean isLoading = false;
    private Boolean isStarted = false;
    public int currentPage = 0;
    ProgressDialog mDialog;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        isLoading = false;
        currentPage = 0;
        lastEnd = false;
        if (isVisible) {
            examHistoryListArrayList.clear();
            limit=5;
            offset=0;
            if (service) {
                getExamList();
            }

        }


    }
    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {

            examHistoryListArrayList.clear();
            limit=5;
            offset=0;
            if (service) {
                getExamList();
            }

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_exam_upcoming, container, false);
        initViews(view);
        performAction();
        return view;
    }

    private void initViews(View view) {
        swipeExamUpcoming = view.findViewById(R.id.swipe_upcomingexam);
        rvExamUpcoming = view.findViewById(R.id.rv_exam_upcoming);
        tvNoExamUpcoming = view.findViewById(R.id.tv_noexam_upcoming);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        ExamsActivity.title.setText("Exam");
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvExamUpcoming.setLayoutManager(linearLayoutManager);
        rvExamUpcoming.setItemAnimator(new DefaultItemAnimator());
        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        upcomingexmmAdapter = new ExamUpcomingAdapter(examHistoryListArrayList,getContext(), this, userType);
        rvExamUpcoming.setAdapter(upcomingexmmAdapter);
    }

    private void performAction() {
        swipeExamUpcoming.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeExamUpcoming.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /*if (isVisible) {*/
                            limit=5;
                            offset=0;
                            examHistoryListArrayList.clear();
                            upcomingexmmAdapter.notifyDataSetChanged();
                            getExamList();
                            swipeExamUpcoming.setRefreshing(false);
                        }
                    /*}*/
                }, 000);
            }
        });

        ExamsActivity.noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),NotificationActivity.class));
            }
        });

        rvExamUpcoming.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView examHistoryrecyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getExamList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
    }

    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getExamList() {
        if (Util.isNetworkAvailable())
        {
            service=false;
            showProgressDialog();
            UpcomingExamListParams upcomingExamListParams=new UpcomingExamListParams();
            upcomingExamListParams.setSchoolID(String.valueOf(selectedSchID()));
            upcomingExamListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            upcomingExamListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            upcomingExamListParams.setLimit(limit);
            upcomingExamListParams.setOffset(offset);
            upcomingExamListParams.setStudID(selected());
            vidyauraAPI.getUpcomingExamsList(upcomingExamListParams).enqueue(new Callback<ExamUpcomingResponse>() {
                @Override
                public void onResponse(Call<ExamUpcomingResponse> call, Response<ExamUpcomingResponse> response) {
                    mDialog.dismiss();
                    service=true;
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        System.out.println("reponse for up ==> "+gson.toJson(response.body()));
                        ExamUpcomingResponse upcomingexamResponse = response.body();
                        if (upcomingexamResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            upcomingexamResponsearray = upcomingexamResponse.getUpcomingExamsList();
                            if (upcomingexamResponse.getStatus() == Util.STATUS_SUCCESS) {

                                if (upcomingexamResponsearray.size()>0) {
                                    //noupcomingexamListTextview.setVisibility(View.GONE);
                                    // upcomingexmrecyclerView.setVisibility(View.VISIBLE);
                                    for (int y = 0; y < upcomingexamResponsearray.size(); y++) {
                                        examHistoryListArrayList.add(new ExamUpcomingList(upcomingexamResponse.getUpcomingExamsList().get(y).getTerm_id(), upcomingexamResponse.getUpcomingExamsList().get(y).getExam_title(), upcomingexamResponse.getUpcomingExamsList().get(y).getExam_duration(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getStart_date(), upcomingexamResponse.getUpcomingExamsList().get(y).getEnd_date(), upcomingexamResponse.getUpcomingExamsList().get(y).getSchool_name(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getLogo(), upcomingexamResponse.getUpcomingExamsList().get(y).getClassroom_id(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getClassName(),upcomingexamResponse.getUpcomingExamsList().get(y).getSection(),
                                                upcomingexamResponse.getUpcomingExamsList().get(y).getClasssection_id()));
                                        //upcomingexmmAdapter.notifyDataSetChanged();
                                        loading = true;
                                    }
                                    if (examHistoryListArrayList.size() > 0) {
                                        upcomingexmmAdapter.notifyDataSetChanged();
                                    }
                                }
                                /*else if (examHistoryListArrayList.size()!=0 && upcomingexamResponsearray.size()!=0)
                                {
                                    //noupcomingexamListTextview.setVisibility(View.VISIBLE);
                                    //upcomingexmrecyclerView.setVisibility(View.GONE);
                                    loading=false;
                                }
                                else {
                                    //Toast.makeText(getContext(), upcomingexamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }*/
                            }
                            else {
                                upcomingexmmAdapter.notifyDataSetChanged();
                                //Toast.makeText(getContext(), upcomingexamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamUpcomingResponse> call, Throwable t) {
                    service=true;
                    mDialog.dismiss();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

}
