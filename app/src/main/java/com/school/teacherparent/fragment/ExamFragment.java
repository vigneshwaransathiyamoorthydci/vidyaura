package com.school.teacherparent.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamFragment extends Fragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {


    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Boolean isStarted=false;
    private Boolean isVisible=false;
    PagerAdapter adapter;
    DiscreteScrollView dsvStudentList;
    LinearLayout ltDsvStudentList;
    ProgressDialog mDialog;

    @Inject
    public VidyAPI vidyauraAPI;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;

    int limit=5;
    int offset=0;
    int count = 0;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    public int classID;
    public static int studentID,studentSchoolID;
    public int sectionID;
    int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_exam, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        ltDsvStudentList = view.findViewById(R.id.lt_dsv_student_list);
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));
        adapter = new PagerAdapter(getChildFragmentManager());
        VidyauraApplication.getContext().getComponent().inject(this);
        int userType = sharedPreferences.getInt(Constants.USERTYPE, 0);
        if (userType == 1) {
            ltDsvStudentList.setVisibility(View.GONE);
            dsvStudentList.setVisibility(View.GONE);
            adapter.addFragment(new ExamUpcomingFragment(), getString(R.string.upcoming_Exams));
            adapter.addFragment(new ExamHistoryFragment(), getString(R.string.history));
            viewPager.setAdapter(adapter);
        } else if (userType == 2) {
            getStudentList();
            ltDsvStudentList.setVisibility(View.VISIBLE);
            dsvStudentList.setVisibility(View.VISIBLE);
            adapter.addFragment(new ParentExamUpcomingFragment(), getString(R.string.upcoming_Exams));
            adapter.addFragment(new ParentExamHistoryFragment(), getString(R.string.history));
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }
        });

        ExamsActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return view;
    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                viewPager.setAdapter(adapter);
                                //setupViewPager(viewPager);
                            } else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            //return POSITION_NONE;ParentExamUpcomingFragment
            //ParentExamHistoryFragment
            if (object instanceof ParentExamUpcomingFragment) {
                ParentExamUpcomingFragment.offset = 0;
                ((ParentExamUpcomingFragment) object).getExamList();
            } else if (object instanceof ParentExamHistoryFragment) {
                ParentExamHistoryFragment.offset = 0;
                ((ParentExamHistoryFragment) object).getExamList();
            }
            return super.getItemPosition(object);
        }
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getStudID();
            ParentExamUpcomingFragment.examHistoryListArrayList.clear();
            ParentExamHistoryFragment.examHistoryListArrayList.clear();
            if (position == 0) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentExamUpcomingFragment()).attach(new ParentExamUpcomingFragment()).commit();
            } else if (position == 1) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentExamHistoryFragment()).attach(new ParentExamHistoryFragment()).commit();
            }
        }
    }

    public static int selected() {
        return studentID;
    }

    public static int selectedSchID() {
        return studentSchoolID;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new PagerAdapter(getChildFragmentManager()); //here used child fragment manager
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isVisible) {

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;


        }

    }


}
