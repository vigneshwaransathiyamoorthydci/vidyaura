package com.school.teacherparent.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.FeedbackDetailsResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.PrivacyandAboutResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/25/2018.
 */

public class PricavypolicyFragment extends BaseFragment {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    TextView emailTextview,feedtitleTextview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.privacy));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        emailTextview=(TextView)view.findViewById(R.id.emailTextview);
        feedtitleTextview=(TextView)view.findViewById(R.id.feedtitleTextview);
        feedtitleTextview.setText("");
        getprivacypolicy();
        return view;
    }
    public void getprivacypolicy()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            vidyauraAPI.getPrivacyAboutDetails(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0))).enqueue(new Callback<PrivacyandAboutResponse>() {
                @Override
                public void onResponse(Call<PrivacyandAboutResponse> call, Response<PrivacyandAboutResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        PrivacyandAboutResponse feedListResponse = response.body();

                        if (feedListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                ArrayList<PrivacyandAboutResponse.getPrivacyAboutDetails> getFAQDetails = feedListResponse.getGetPrivacyAboutDetails();

                                if (getFAQDetails.size()>0) {

                                    //emailTextview.setText(getFAQDetails.getEmail());
                                    System.out.println("kkkkkkkkkkkkkkkkk"+feedListResponse.getGetPrivacyAboutDetails().get(0).getAbout()+"="+feedListResponse.getGetPrivacyAboutDetails().get(0).getPrivacy_policy());
                                    emailTextview.setText(feedListResponse.getGetPrivacyAboutDetails().get(0).getAbout());
                                    feedtitleTextview.setText(feedListResponse.getGetPrivacyAboutDetails().get(0).getPrivacy_policy());
                                } else {
                                    Toast.makeText(getContext(), feedListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                hideProgress();
                                secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, feedListResponse.getToken()).commit();
                                getprivacypolicy();

                            }

                        } else {
                            Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                        }
                    }


                }

                @Override
                public void onFailure(Call<PrivacyandAboutResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}
