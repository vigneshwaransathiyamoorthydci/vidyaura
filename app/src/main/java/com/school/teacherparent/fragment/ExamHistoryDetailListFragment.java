package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.ExamHistoryDetailAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ExamHistoryDetailsResponse;
import com.school.teacherparent.models.ExamHistoryResponse;
import com.school.teacherparent.models.ExamHistorydetailList;
import com.school.teacherparent.models.GetUpcomingExamDetailsParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ExamFragment.selected;
import static com.school.teacherparent.fragment.ExamFragment.selectedSchID;


/**
 * A simple {@link Fragment} subclass.
 */
public class ExamHistoryDetailListFragment extends BaseFragment {


    private Spinner spinner;
    private RecyclerView recyclerView;
    private ExamHistoryDetailAdapter mAdapter;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    ArrayList<ExamHistoryResponse.examwiseClassList> examwiseClassList;
    ArrayList<ExamHistoryDetailsResponse.ExamHistoryDetailsByClassList> upcomingDetailsByClassListArrayList;
    List<String> classpinner = new ArrayList<String>();
    private boolean loading = true;
    int limit=5;
    int offset=0;
    ArrayList<ExamHistorydetailList> upcomingExamdetailLists;
    int classid,termid,sectionid;
    String exam_title;

    String classNameText;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_exam_detail, container, false);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_exams3fragment);
        upcomingExamdetailLists=new ArrayList<>();


        int userType = sharedPreferences.getInt(Constants.USERTYPE,0);
        if (userType == 1) {
            Bundle b = getArguments();
            examwiseClassList= (ArrayList<ExamHistoryResponse.examwiseClassList>) b.getSerializable("classlist");
            termid=b.getInt("termid");
            exam_title=b.getString("exam_title");
            ExamsActivity.title.setText(exam_title);
            Log.d("VIGNESH","examwisearray"+b.getString("test"));
            spinner.setVisibility(View.VISIBLE);
        } else if (userType == 2) {
            Bundle b = getArguments();
            termid=b.getInt("termid");
            exam_title=b.getString("exam_title");
            ExamsActivity.title.setText(exam_title);
            classid=b.getInt("classid");
            sectionid=b.getInt("sectionid");
            spinner.setVisibility(View.GONE);
            getUpcomingExamdetails();
        }

        upcomingDetailsByClassListArrayList=new ArrayList<>();
        classpinner.add("Please Select class");
        if (examwiseClassList!=null)
        {
            for (int y=0;y<examwiseClassList.size();y++)
            {
                Log.d("VIGNESH","examwisearray"+examwiseClassList.get(y).getClassName());
                classpinner.add(examwiseClassList.get(y).getClassName()+"-"+examwiseClassList.get(y).getSection());

            }
        }
        ExamsActivity.noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArguments().remove("classlist");
                startActivity(new Intent(getContext(),NotificationActivity.class));
            }
        });

        ExamsActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new ExamFragment();
                replaceFragment(fragment);
            }
        });



        mAdapter = new ExamHistoryDetailAdapter(upcomingExamdetailLists,getContext());
        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);








        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_dialog);

        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                String className=classpinner.get(position).toString();
                Log.d("VICKY","className"+className);
                if (!className.equals("Please Select class")) {
                    classid = examwiseClassList.get(position-1).getClassroom_id();
                    sectionid = examwiseClassList.get(position-1).getClasssection_id();
                    upcomingExamdetailLists.clear();
                    classNameText=classpinner.get(position).toString();
                    getUpcomingExamdetails();
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        if (userType == 1) {
            spinner.performClick();
        }

        return view;
    }


    private void getUpcomingExamdetails() 
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            GetUpcomingExamDetailsParam upcomingExamDetailsParam=new GetUpcomingExamDetailsParam();
            upcomingExamDetailsParam.setSchoolID(String.valueOf(selectedSchID()));
            upcomingExamDetailsParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            upcomingExamDetailsParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            upcomingExamDetailsParam.setLimit(limit);
            upcomingExamDetailsParam.setOffset(offset);
            upcomingExamDetailsParam.setClassID(String.valueOf(classid));
            upcomingExamDetailsParam.setTermID(String.valueOf(termid));
            upcomingExamDetailsParam.setSectionID(String.valueOf(sectionid));
            upcomingExamDetailsParam.setClassImage(Constants.CLASSIMAGE);
            upcomingExamDetailsParam.setStudID(selected());
            vidyauraAPI.getExamHistoryDetailsByClass(upcomingExamDetailsParam).enqueue(new Callback<ExamHistoryDetailsResponse>() {
                @Override
                public void onResponse(Call<ExamHistoryDetailsResponse> call, Response<ExamHistoryDetailsResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        ExamHistoryDetailsResponse feedListResponse = response.body();

                        if (feedListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                            if (feedListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                upcomingDetailsByClassListArrayList.clear();
                                upcomingDetailsByClassListArrayList = response.body().getExamHistoryDetailsByClassList();
                                if (upcomingDetailsByClassListArrayList.size()!=0) {
//                                    feeds_recycle.setVisibility(View.VISIBLE);
//                                    nofeedListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<upcomingDetailsByClassListArrayList.size();y++)
                                    {

                                        if ( upcomingDetailsByClassListArrayList.get(y).getChapterName().size()>0 &&  upcomingDetailsByClassListArrayList.get(y).getTopicName().size()>0) {
                                            upcomingExamdetailLists.add(new ExamHistorydetailList(upcomingDetailsByClassListArrayList.get(y).getId(),
                                                    upcomingDetailsByClassListArrayList.get(y).getExam_duration(), upcomingDetailsByClassListArrayList.get(y).getStart_time(),
                                                    upcomingDetailsByClassListArrayList.get(y).getEnd_time(), upcomingDetailsByClassListArrayList.get(y).getSubject_id(),
                                                    upcomingDetailsByClassListArrayList.get(y).getClassroom_id(), upcomingDetailsByClassListArrayList.get(y).getClasssection_id(),
                                                    upcomingDetailsByClassListArrayList.get(y).getTerm_id(), upcomingDetailsByClassListArrayList.get(y).getClass_id(),
                                                    upcomingDetailsByClassListArrayList.get(y).getName(), upcomingDetailsByClassListArrayList.get(y).getIsUserPostedExam(),
                                                    upcomingDetailsByClassListArrayList.get(y).getTopicName(),
                                                    upcomingDetailsByClassListArrayList.get(y).getChapterName(),classNameText,upcomingDetailsByClassListArrayList.get(y).getClassImage()));
                                        }


                                    }
                                    //feedsAdapter = new FeedsAdapter(feedListResponsearray, getActivity(), baseActivity,FeedsFragment.this);
                                    //feeds_recycle.setAdapter(feedsAdapter);
                                    mAdapter.notifyDataSetChanged();
                                    loading=true;
                                }
//                                else if (feedList.size()!=0 && feedListResponsearray.size()!=0)
//                                {
//                                    nofeedListTextview.setVisibility(View.VISIBLE);
//                                    feeds_recycle.setVisibility(View.GONE);
//                                    loading=false;
//                                }
                            } else {
                                Toast.makeText(getContext(), feedListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<ExamHistoryDetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void replaceFragment(Fragment fragment) {

        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }
}



