package com.school.teacherparent.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerListener;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.CircularComposeclassAdpater;
import com.school.teacherparent.adapter.CircularComposeclassAdpaterwithCheckbox;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddEventParams;
import com.school.teacherparent.models.AddFeedParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.ClassListResponseforEvents;
import com.school.teacherparent.models.DeleteEventParmas;
import com.school.teacherparent.models.EventListResponse;
import com.school.teacherparent.models.EventdetailsResponse;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofClass;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.models.UpdateEventParams;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;
import com.thomashaertel.widget.MultiSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddEventFragment extends BaseFragment {

    private LinearLayout lin_date;
    private TextView date, timeTextview, add_event,titleTextview,descTextview,venueTextview;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Spinner class_spinner,visiblity_spinner;
    private Spinner status1;
    private LinearLayout lin_time;
    private int hour, minute;
    ImageView back;
    private MultiSpinner multiSpinner;
    String selectedTime;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private ArrayAdapter<String> multiSpinneradapter;

    ArrayList<Integer> selectedClass=new ArrayList<>();
    String classID = "";
    boolean valid;List<ListofClass> classList = new ArrayList<ListofClass>();
    int visibility=0;
    int rsvp=1;
    CheckBox rsvp_radiobutton;
    String date_n;
    int eventID;
    boolean[] selectedclassItems;
    TextView classnameTextview,searchnameTextview;
    ListView classlistView;
    Button dialogDistrictButton;
    Dialog classDialog;
    SearchView dialogSearchView;
    CircularComposeclassAdpaterwithCheckbox circularComposeclassAdpater;
    List<ListofClass> searchclassList = new ArrayList<ListofClass>();
    CheckBox selectAllCheckbox;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_event, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_new_event));
        multiSpinneradapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item);
        selectedclassItems = new boolean[multiSpinneradapter.getCount()];
        titleTextview=view.findViewById(R.id.titleTextview);
        titleTextview.setTypeface(ProximaNovaFont.getInstance(getContext()).getSemiBoldTypeFace());
        descTextview=view.findViewById(R.id.descTextview);
        descTextview.setTypeface(ProximaNovaFont.getInstance(getContext()).getSemiBoldTypeFace());
        venueTextview=view.findViewById(R.id.venueTextview);
        venueTextview.setTypeface(ProximaNovaFont.getInstance(getContext()).getSemiBoldTypeFace());
        date = view.findViewById(R.id.date);
        lin_date = view.findViewById(R.id.lin_date);
        lin_time = view.findViewById(R.id.lin_time);
        add_event = view.findViewById(R.id.add_event);
        visiblity_spinner = (Spinner) view.findViewById(R.id.visiblitySpinner);
        rsvp_radiobutton=(CheckBox) view.findViewById(R.id.rsvp_radiobutton);
        timeTextview = view.findViewById(R.id.time);
        date_n = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        //date.setText(date_n);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh:mm aa",Locale.UK);

        SimpleDateFormat seletedFormat = new SimpleDateFormat("hh:mm:ss",Locale.UK);

        String strDate = "" + mdformat.format(calendar.getTime());
        selectedTime= seletedFormat.format(calendar.getTime());
        multiSpinner=(MultiSpinner)view.findViewById(R.id.spinnerMulti);
        //rsvp_radiogroup=(RadioGroup)view.findViewById(R.id.radio_group_type);
        rsvp_radiobutton.setChecked(true);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            eventID = bundle.getInt("eventID", 0);
            }
            if (eventID<=0)
            {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
                String currentDate = currentDATE.format(c);
                date.setText(getDateFormat(currentDate));
                timeTextview.setText(strDate);
                getClassList();
            }
            else
            {
                add_event.setText(R.string.update);
                SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_Event));
                getClassList();
            }

        classDialog = new Dialog(getContext());
        classDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        classDialog.setCancelable(false);
        classDialog.setContentView(R.layout.add_exam_chapter_custom_dialog);
        searchnameTextview=(TextView)classDialog.findViewById(R.id.searchnameTextview);
        classlistView = (ListView) classDialog.findViewById(R.id.chapterlistView);
        dialogDistrictButton = (Button) classDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)classDialog.findViewById(R.id.dialogSearchView);
        selectAllCheckbox=(CheckBox)classDialog.findViewById(R.id.selectAllCheckbox);
        classnameTextview=view.findViewById(R.id.classnameTextview);
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });
        dialogSearchView.setQueryHint(getString(R.string.enter_class));
        classnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classDialog.show();
            }
        });
        dialogDistrictButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int y = 0; y < classList.size(); y++) {
                    if (classList.get(y).getSelectedID()==0)
                    {
                        selectAllCheckbox.setChecked(false);
                        Log.d("TTTTTTTTT","kkkkk");
                        break;
                    }
                }
                String className="";
                classID="";
                for (int y=0;y<classList.size();y++)
                {

                    if (classList.get(y).getSelectedID()!=0) {
                        className = className.concat(String.valueOf(classList.get(y).getClassname()+classList.get(y).getClasssection()).concat(","));
                        classID=classID.concat(String.valueOf(classList.get(y).getClasssectionid())).concat(",");
                    }

                }
                if (className.length()>0 && classID.length()>0) {
                    className = className.substring(0, className.length() - 1);
                    classID = classID.substring(0, classID.length() - 1);
                    classnameTextview.setText(className);
                    classnameTextview.setError(null);

                }
                else
                {
                    classnameTextview.setText("");
                }
                classDialog.hide();
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1)
                {
//                    Collections.sort(classList, new Comparator<ListofClass>() {
//
//                        @Override
//                        public int compare(ListofClass lhs, ListofClass rhs) {
//                            //here getTitle() method return app name...
//                            return newText.compareTo(lhs.getClassname());
//
//                        }
//
//
//                    });
                    searchclassList.clear();
                    for(int y=0;y<classList.size();y++)
                    {
                        if (classList.get(y).getClassname().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchclassList.add(classList.get(y));

                        }
                    }
                    if (searchclassList.size()>0) {
                        circularComposeclassAdpater = new CircularComposeclassAdpaterwithCheckbox(searchclassList, getContext(),selectAllCheckbox);
                        classlistView.setAdapter(circularComposeclassAdpater);
                        circularComposeclassAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        classlistView.setVisibility(View.GONE);
                        searchnameTextview.setVisibility(View.VISIBLE);
                    }

                }
                else if (newText.toString().length()==0)
                {
                    classlistView.setVisibility(View.VISIBLE);
                    searchnameTextview.setVisibility(View.GONE);
                    circularComposeclassAdpater=new CircularComposeclassAdpaterwithCheckbox(classList,getContext(),selectAllCheckbox);
                    classlistView.setAdapter(circularComposeclassAdpater);
                    circularComposeclassAdpater.notifyDataSetChanged();
                }
                return false;
            }
        });


        add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (eventID==0)
                {
                    if (validate()) {

                        if (eventID == 0)
                        {
                            addEvent();
                        }
                    }

                }
                else
                    {
                        if (eventID != 0) {
                    if (validate()) {


                            updateEvent();
                        }
                    }

                }

//                if (validate()) {
//
//                    if (eventID==0)
//                    {
//
//                        addEvent();
//                    }
//                    else
//                    {
//
//
//                        updateEvent();
//                    }
//
//
//                }

            }
        });

        lin_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_time) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);


                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {

                                public String format;

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    /*if (hourOfDay == 0) {

                                        hourOfDay += 12;

                                        format = "AM";
                                    } else if (hourOfDay == 12) {

                                        format = "PM";

                                    } else if (hourOfDay > 12) {

                                        hourOfDay -= 12;

                                        format = "PM";

                                    } else {

                                        format = "AM";
                                    }
                                    if (hourOfDay<=9)
                                    {
                                        hourOfDay= Integer.valueOf(String.valueOf(0) + String.valueOf(hourOfDay));
                                    }
                                    selectedTime=hourOfDay + ":" + minute+":"+0+0;
                                    System.out.println("selectedTime ==> "+date_n+" "+selectedTime);
                                    timeTextview.setText(hourOfDay + ":" + minute+":"+format);*/

                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                                    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("hh:mm aa");
                                    Calendar calendarStartTime = Calendar.getInstance();
                                    calendarStartTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                    calendarStartTime.set(Calendar.MINUTE, minute);
                                    selectedTime = simpleDateFormat.format(calendarStartTime.getTime());
                                    timeTextview.setText(simpleDateFormat2.format(calendarStartTime.getTime()));
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();

                }
            }
        });


        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
//                                    SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");
//                                    date_n=formt.format(dateObj);
//                                    date.setText(formt.format(dateObj));

                                    SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");
                                    date_n=formt.format(dateObj);
                                    SimpleDateFormat viewformat = new SimpleDateFormat("dd MMM yyyy");

                                    date.setText(viewformat.format(dateObj));
                                }

                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                    datePickerDialog.show();
                }
            }
        });




        List<String> classpinner = new ArrayList<String>();
        classpinner.add("1 Classes");
        classpinner.add("2 Classes");
        classpinner.add("3 Classes");
        classpinner.add("4 Classes");
        classpinner.add("5 Classes");
        class_spinner = (Spinner) view.findViewById(R.id.invi_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        multiSpinner.setAdapter(multiSpinneradapter, false, onSelectedListener);


        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Name", "Public");
        map.put("Icon", R.mipmap.visiblity_icon);
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("Name", "Private");
        map.put("Icon", R.mipmap.privacy_icon);
        list.add(map);


        myAdapter adapter = new myAdapter(getActivity(), list,
                R.layout.spinner_dropdown_item_image_forevent, new String[]{"Name", "Icon"},
                new int[]{R.id.name, R.id.icon});

        visiblity_spinner.setAdapter(adapter);

        visiblity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                visibility=position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;

    }

    private void getEventDetails()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();
            DeleteEventParmas deleteEventParmas=new DeleteEventParmas();
            deleteEventParmas.setUserID(sharedPreferences.getString(Constants.USERID,""));
            deleteEventParmas.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            deleteEventParmas.setEventID(String.valueOf(eventID));
            deleteEventParmas.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getEventDetailsById(deleteEventParmas).enqueue(new Callback<EventdetailsResponse>() {
                @Override
                public void onResponse(Call<EventdetailsResponse> call, Response<EventdetailsResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        EventdetailsResponse eventdetailsResponse=response.body();
                        if (eventdetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {


                            if (eventdetailsResponse.getStatus() == Util.STATUS_SUCCESS) {

                               titleTextview.setText(eventdetailsResponse.getEventDetails().get(0).getTitle());
                               descTextview.setText(eventdetailsResponse.getEventDetails().get(0).getDescription());
                               venueTextview.setText(eventdetailsResponse.getEventDetails().get(0).getVenue());
                               if (eventdetailsResponse.getEventDetails().get(0).getRSVP()==0)
                               {
                                   rsvp_radiobutton.setChecked(false);
                                   rsvp=0;
                               }
                               else
                               {
                                   rsvp_radiobutton.setChecked(true);
                                   rsvp=1;
                               }



                                String[] chapterstrArray = eventdetailsResponse.getEventDetails().get(0).getClass_id().split(",");
                                String className="";
                                for (int i=0;i<classList.size();i++)
                                {
                                    for (int u=0;u<chapterstrArray.length;u++)
                                    {
                                        int selectedclassID= Integer.parseInt(chapterstrArray[u]);
                                        if (classList.get(i).getClasssectionid()==selectedclassID)
                                        {

                                            className=className.concat(classList.get(i).getClassname() +classList.get(i).getClasssection()).concat(",");
                                            classID=classID.concat(String.valueOf(classList.get(i).getClasssectionid())).concat(",");
                                            classList.get(i).setSelectedID(classList.get(i).getClasssectionid());


                                        }
                                    }
                                    Log.d("VIII","classID-1"+classID);

                                }


                                if (className.length()>0 && classID.length()>0) {
                                    className = className.substring(0, className.length() - 1);
                                    classID = classID.substring(0, classID.length() - 1);
                                    classnameTextview.setText(className);
                                    Log.d("VIII","classID-2"+classID);
                                }



                                if (eventdetailsResponse.getEventDetails().get(0).getVisibility().equals("public"))
                                {
                                        visiblity_spinner.setSelection(0);
                                }
                                else
                                {
                                    visiblity_spinner.setSelection(1);
                                }


                                for (int u=0;u<classList.size();u++)
                                {
                                    if (classList.get(u).getSelectedID()==classList.get(u).getClasssectionid())
                                    {
                                        selectAllCheckbox.setChecked(true);
                                        continue;
                                    }
                                    else
                                    {
                                        selectAllCheckbox.setChecked(false);
                                        break;
                                    }
                                }
                                date_n=geteventDateFormatforload(eventdetailsResponse.getEventDetails().get(0).getEvent_date());
                                date.setText(geteventDateFormat(eventdetailsResponse.getEventDetails().get(0).getEvent_date()));
                                //time.setText(geteventtimeFormat(eventdetailsResponse.getEventDetails().get(0).getEvent_date()));
                                final String time = geteventtimeFormat(eventdetailsResponse.getEventDetails().get(0).getEvent_date());

                                try {
                                    final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                                    final Date dateObj = sdf.parse(time);
                                    System.out.println(dateObj);
                                    System.out.println(new SimpleDateFormat("K:mm a").format(dateObj));
                                    selectedTime=geteventtimeFormatselectedtime(eventdetailsResponse.getEventDetails().get(0).getEvent_date());
                                    timeTextview.setText(new SimpleDateFormat("K:mm a").format(dateObj));

                                } catch (final ParseException e) {
                                    e.printStackTrace();
                                }


                            } else {
                                Toast.makeText(getContext(), eventdetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EventdetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });

        }

        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }


    }
private void updateEvent()
{
    if (Util.isNetworkAvailable())
    {
        if (rsvp_radiobutton.isChecked())
        {
            rsvp=1;
        }
        else
        {
            rsvp=0;
        }
        showProgress();
        UpdateEventParams updateEventParams=new UpdateEventParams();
        updateEventParams.setEventTitle(titleTextview.getText().toString().trim());
        updateEventParams.setEventDesc(descTextview.getText().toString().trim());
        updateEventParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
        updateEventParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
        updateEventParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
        updateEventParams.setVisibility(String.valueOf(visibility));
        updateEventParams.setEventRSVP(rsvp);
        updateEventParams.setClassID(classID);
        updateEventParams.setEventVenue(venueTextview.getText().toString().trim());
        updateEventParams.setEventDateTime(date_n+" "+selectedTime);
        updateEventParams.setEventID(eventID);
        vidyauraAPI.updateEvent(updateEventParams).enqueue(new Callback<AddFeedResponse>() {
            @Override
            public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                hideProgress();
                if (response.body() != null) {
                    classDialog.dismiss();
                    AddFeedResponse addFeedResponse = response.body();
                    if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                        if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            getActivity().finish();

                        } else {
                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        getActivity().finishAffinity();

                    }

                }
                else
                {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
            }
        });
    }
    else
    {
        Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
    }
}
    private void addEvent() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            if (rsvp_radiobutton.isChecked())
            {
                rsvp=1;
            }
            else
            {
                rsvp=0;
            }
            AddEventParams addEventParams=new AddEventParams();
            addEventParams.setEventTitle(titleTextview.getText().toString().trim());
            addEventParams.setEventDesc(descTextview.getText().toString().trim());
            addEventParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addEventParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addEventParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addEventParams.setVisibility(String.valueOf(visibility));
            addEventParams.setEventRSVP(rsvp);
            addEventParams.setClassID(classID);
            addEventParams.setEventVenue(venueTextview.getText().toString().trim());
            addEventParams.setEventDateTime(date_n+" "+selectedTime);
            vidyauraAPI.addEvent(addEventParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();
                    classDialog.dismiss();
                    if (response.body() != null) {
                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                SendNotification sendNotification = new SendNotification();
                                sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                                sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
                                sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                                sendNotification.setType("event");
                                sendNotification.setType_id(addFeedResponse.getTypeId());
                                vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                    @Override
                                    public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                        AddFeedResponse addFeedResponse1 = response.body();
                                        if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                            Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                            getActivity().finish();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                        hideProgress();
                                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                    }
                                });

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean validate() {
        String feeddescription = descTextview.getText().toString().trim();
        String title = titleTextview.getText().toString().trim();
        String venue=venueTextview.getText().toString().trim();
        if (title.length() == 0 || title.length() <= 0) {
            titleTextview.setError(getString(R.string.Enterfeedtitle));
            valid = false;

        }
        else if (feeddescription.length() == 0 || feeddescription.length() <=0) {
            descTextview.setError(getString(R.string.Enterdescription));
            valid = false;
        }
        else if (venue.length()==0 || venue.length()<=0)
        {
            venueTextview.setError(getString(R.string.Entervenue));
            valid = false;
        }
        else if (classID.toString().length()<=0)
        {
            classnameTextview.setError(getString(R.string.class_name));
            valid = false;
        }
        else {
            descTextview.setError(null);
            titleTextview.setError(null);
            multiSpinner.setError(null);
            venueTextview.setError(null);
            classnameTextview.setError(null);
            valid = true;
        }
        return valid;
    }
    private MultiSpinner.MultiSpinnerListener onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
        public void onItemsSelected(boolean[] selected) {
            selectedClass.clear();
            for(int i=0; i<selected.length; i++) {
                if(selected[i]) {
                    Log.i("TAG", "mes"+i );
                    selectedClass.add(i);

                }
                multiSpinner.setError(null);
            }

            // Do something here with the selected items
        }
    };


    private void display(String num) {
        TextView time = (TextView) getView().findViewById(R.id.time);
        time.setText(num);
    }

    private class myAdapter extends SimpleAdapter {

        public myAdapter(Context context, List<? extends Map<String, ?>> data,
                         int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.spinner_dropdown_item_image,
                        null);
            }

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            ((TextView) convertView.findViewById(R.id.name))
                    .setText((String) data.get("Name"));
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageResource((Integer) data.get("Icon"));

            return convertView;
        }

    }

    private void getClassList()
    {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classList.clear();
            GetclassListParams getclassListParams=new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            vidyauraAPI.getAllClasses (getclassListParams).enqueue(new Callback<ClassListResponseforEvents>() {
                @Override
                public void onResponse(Call<ClassListResponseforEvents> call, Response<ClassListResponseforEvents> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        ClassListResponseforEvents classListResponse=response.body();
                        if (classListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i=0;i<classListResponse.getAllClassesList().size();i++)
                                {
                                    classList.add(new ListofClass(classListResponse.getAllClassesList().get(i).getClassID(),
                                            classListResponse.getAllClassesList().get(i).getClassName(),classListResponse.getAllClassesList().get(i).getSection(),
                                            classListResponse.getAllClassesList().get(i).getSectionID(),false,0));
                                    multiSpinneradapter.add(  classListResponse.getAllClassesList().get(i).getClassName()+" "+
                                    classListResponse.getAllClassesList().get(i).getSection());
                                }
                                circularComposeclassAdpater=new CircularComposeclassAdpaterwithCheckbox(classList,getContext(),selectAllCheckbox);
                                classlistView.setAdapter(circularComposeclassAdpater);

                                if (eventID>0)
                                {

                                    getEventDetails();
                                }

                                //Util.setListViewHeightBasedOnChildren(classlistView);



                            } else {
                                Toast.makeText(getContext(), classListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponseforEvents> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });


        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }
}
