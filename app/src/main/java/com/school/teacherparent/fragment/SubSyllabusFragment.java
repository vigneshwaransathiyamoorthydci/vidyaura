package com.school.teacherparent.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.SubClassAdapter;
import com.school.teacherparent.adapter.SylabusdetailsAdapter;
import com.school.teacherparent.adapter.SylabusdetailsNewAdapter;
import com.school.teacherparent.app.VidyauraApplication;

import com.school.teacherparent.models.SyllabusDetailParms;
import com.school.teacherparent.models.SyllabusDetailsModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.FontTextViewBold;
import com.school.teacherparent.utils.FontTextViewRegular;
import com.school.teacherparent.utils.FontTextViewSemibold;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubSyllabusFragment extends BaseFragment {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    private SubClassAdapter mAdapter;
    private List<SyllabusDetailsModel.SyllabusDetailsList> subclassList = new ArrayList<>();
    FloatingActionButton addClasswork;
    ImageView noti, back;
    Fragment fragment;
    String classid,subjectid,sectionid;
    RecyclerView recyclesylabusdetailsstuname;
    SeekBar t1;
    TextView counts;

    FontTextViewRegular desc;
    FontTextViewBold title1,title;
    public SubSyllabusFragment() {
        // Required empty public constructor
    }
    SylabusdetailsAdapter sylabusdetailsAdapter;
    SylabusdetailsNewAdapter sylabusdetailsNewAdapter;
    private List<SyllabusDetailsModel.SyllabusDetailsList> resstuList = new ArrayList<>();
    ImageView calendarImageview;
    CircularImageView classnameImageview;
    TextView className;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_syllabus, container, false);
        classid=getArguments().getString("clsid");
        subjectid=getArguments().getString("subid");
        sectionid=getArguments().getString("secid");
        title=view.findViewById(R.id.title);
        t1=view.findViewById(R.id.t1);
        counts=view.findViewById(R.id.counts);
        classnameImageview=view.findViewById(R.id.classNameImgeview);
        String progressValue=getArguments().getString("progvalue");
        if (progressValue!=null){
            int value=Math.round(Float.parseFloat(progressValue));
            t1.setProgress(value);
            counts.setText(progressValue+"%");
        }
        t1.setEnabled(false);
        calendarImageview=view.findViewById(R.id.calendarImageview);

        noti = view.findViewById(R.id.noti);
        back = view.findViewById(R.id.back);
        title1= view.findViewById(R.id.title1);
        desc= view.findViewById(R.id.desc);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new ClassWorkTab();
                replaceFragment(fragment);
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        className=view.findViewById(R.id.className);
        calendarImageview.setVisibility(View.GONE);
        //Analdata();
        recyclesylabusdetailsstuname=view.findViewById(R.id.recycle_sylabusdetails_stuname);
        sylabusdetailsNewAdapter = new SylabusdetailsNewAdapter(resstuList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclesylabusdetailsstuname.setLayoutManager(mLayoutManager);
        recyclesylabusdetailsstuname.setItemAnimator(new DefaultItemAnimator());
        recyclesylabusdetailsstuname.setAdapter(sylabusdetailsNewAdapter);
        getsyllabusDetails();


        return view;
    }
    public void getsyllabusDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            SyllabusDetailParms syllabusDetails = new SyllabusDetailParms();
            syllabusDetails.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            syllabusDetails.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            syllabusDetails.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            syllabusDetails.setClassID(classid);
            syllabusDetails.setSubjectID(subjectid);
            syllabusDetails.setSectionID(sectionid);
            syllabusDetails.setClassImage(Constants.CLASSIMAGE);
            Gson gson = new Gson();
            String input = gson.toJson(syllabusDetails);
            Log.d("input", "getsyllabusList: " + input);
            vidyauraAPI.getSyllabusdetails(syllabusDetails).enqueue(new Callback<SyllabusDetailsModel>() {
                @Override
                public void onResponse(Call<SyllabusDetailsModel> call, Response<SyllabusDetailsModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        SyllabusDetailsModel data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getSyllabusDetailsList() != null &&
                                        response.body().getSyllabusDetailsList().size() != 0) {
                                    title1.setText(data.getSyllabusDetailsList().get(0).getSubjectName());
                                    title.setText("Class "+data.getSyllabusDetailsList().get(0).getClassName()+data.getSyllabusDetailsList().get(0).getSection());

                                    String url = getString(R.string.s3_baseurl) + getString(R.string.s3_classworksection_path)+"/"+data.getSyllabusDetailsList().get(0).getClass_image();
                                    Picasso.get().load(url).placeholder(R.drawable.nopreview).into(classnameImageview, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onError(Exception e) {
                                            classnameImageview.setImageResource(R.drawable.nopreview);
                                        }
                                    });
                                    resstuList.addAll(data.getSyllabusDetailsList());
                                    //classnameImageview.setImageResource(Constants.imgid[2]);
                                    //className.setText(data.getSyllabusDetailsList().get(0).getClassName()+" "+data.getSyllabusDetailsList().get(0).getSection());
                                  /*  for(int i=0;i<data.getSyllabusDetailsList().size();i++){
                                    resstuList.addAll(data.getSyllabusDetailsList().get(i).getSyllabusList());
                                        Log.d("sizelist", "onResponse: "+data.getSyllabusDetailsList().get(i).getSyllabusList().size());}
*/
                                    sylabusdetailsNewAdapter.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(getActivity(), R.string.noData, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(getContext(), data.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<SyllabusDetailsModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            hideProgress();
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}
