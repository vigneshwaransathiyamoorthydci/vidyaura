package com.school.teacherparent.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.ParentCircularActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GalleryParams;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ParentAllGalleryFragment.galleryAdapter;
import static com.school.teacherparent.fragment.ParentPhotosGalleryFragment.photosGalleryAdapter;
import static com.school.teacherparent.fragment.ParentVideosGalleryFragment.videomAdapter;

/**
 * Created by harini on 10/12/2018.
 */

public class ParentGalleryFragment extends BaseFragment implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    DiscreteScrollView dsvStudentList;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    int limit = 5;
    int count = 0;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    public int classID;
    public static int studentID,selectdSchoolID;
    public int sectionID;
    ProgressDialog mDialog;
    int position = 0;
    public boolean isLoading = true;
    List<Gallerylist> gallerylists=new ArrayList<>();
    GalleryListResponse galleryListResponse;
    private boolean loading = true;
    List<Gallerylist> getGallerylists = new ArrayList<>();

    public ParentGalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_parent_gallery,container,false);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.gallery));
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        dsvStudentList = view.findViewById(R.id.dsv_student_list);
        viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        getStudentList();
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));


        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        //   GalleryFragment.PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        adapter.addFragment(new ParentAllGalleryFragment(), "All");
        adapter.addFragment(new ParentPhotosGalleryFragment(), "Photos");
        adapter.addFragment(new ParentVideosGalleryFragment(), "Videos");

        viewPager.setAdapter(adapter);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                position = tab.getPosition();
            }
        });

    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(getContext(),parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
        } else {
            dsvStudentList.scrollToPosition(0);
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
                                sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
                                studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
                                selectdSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
                                editor.putInt("SelectedStudentId",studentID);
                                editor.apply();
                                setupViewPager(viewPager);
                            } else {
                                Toast.makeText(getContext(), childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            //return POSITION_NONE;
            if (object instanceof ParentAllGalleryFragment) {
                ParentAllGalleryFragment.gallerylists.clear();
                ParentAllGalleryFragment.offset = 0;
                ((ParentAllGalleryFragment) object).getgalleryList();
            } else if (object instanceof ParentPhotosGalleryFragment) {
                ParentPhotosGalleryFragment.photoslists.clear();
                ParentPhotosGalleryFragment.offset = 0;
                ((ParentPhotosGalleryFragment) object).getgalleryList();
            } else if (object instanceof ParentVideosGalleryFragment) {
                ParentVideosGalleryFragment.videolists.clear();
                ParentVideosGalleryFragment.offset = 0;
                ((ParentVideosGalleryFragment) object).getgalleryList();
            }
            return super.getItemPosition(object);
        }
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            selectdSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            ParentPhotosGalleryFragment.offset = 0;
            if (position == 0) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentAllGalleryFragment()).attach(new ParentAllGalleryFragment()).commit();
                //galleryAdapter.notifyDataSetChanged();
            } else if (position == 1) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentPhotosGalleryFragment()).attach(new ParentPhotosGalleryFragment()).commit();
                //photosGalleryAdapter.notifyDataSetChanged();
            } else if (position == 2) {
                viewPager.getAdapter().notifyDataSetChanged();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(new ParentVideosGalleryFragment()).attach(new ParentVideosGalleryFragment()).commit();
                //videomAdapter.notifyDataSetChanged();
            }
        }
    }

    public static int selected() {
        return studentID;
    }

    public static int selectedSchool() {
        return selectdSchoolID;
    }

}
