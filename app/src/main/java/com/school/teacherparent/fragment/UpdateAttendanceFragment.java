package com.school.teacherparent.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.SubAttendanceAdapter;
import com.school.teacherparent.adapter.UpdateAttenAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddAttendanceParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AttendanceUpdateStudentList;
import com.school.teacherparent.models.AttendancedetailsParams;
import com.school.teacherparent.models.AttendancedetailsResponse;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.StudentList;
import com.school.teacherparent.models.StudentListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateAttendanceFragment extends BaseFragment {

    private Spinner class_spinner;
    private RecyclerView studentListrecyclerView;
    private TextView presentTextview,addattendanceTextview;
    private UpdateAttenAdapter updateAttenAdapter;
    FloatingActionButton addAttendance;
    ClassListResponse classListResponse;
    ArrayList<String> classlist = new ArrayList<String>();

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ArrayAdapter<String> classspinnerArray;
    int classID,sectionID,attendanceSessionCount,groupID;
    boolean isGroup;
    String formattedDate;
    StudentListResponse studentListResponse;
    public ArrayList<StudentListResponse.classwiseStudentsList> classwiseStudentsLists;
    LinearLayoutManager linearLayoutManager;
    List<AttendanceUpdateStudentList> attendanceUpdateStudentLists=new ArrayList<>();
    String studentID;
    String ODStudentID;
    String LeaveStudentID;
    RadioButton forenoonradiobutton,afternoonradiobutton;
    String sessionTYPE="1";
    RadioGroup radioGroup;
    AttendancedetailsResponse attendancedetailsResponse;
    public ArrayList<AttendancedetailsResponse.attendanceDetailsList> attendanceDetailsListArrayList;
    TextView currentdateTextview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_attendance, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        studentListrecyclerView = (RecyclerView) view.findViewById(R.id.add_attendance_recycle);
        addAttendance = view.findViewById(R.id.add_attendance);
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.update_attendance));
        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });
        forenoonradiobutton=(RadioButton)view.findViewById(R.id.forenoonradiobutton);
        afternoonradiobutton=(RadioButton)view.findViewById(R.id.afternoonradiobutton);
        afternoonradiobutton.setVisibility(View.GONE);
        forenoonradiobutton.setVisibility(View.GONE);
        radioGroup=(RadioGroup)view.findViewById(R.id.toggle);
        radioGroup.setVisibility(View.GONE);
        attendanceDetailsListArrayList=new ArrayList<>();
        addattendanceTextview=(TextView)view.findViewById(R.id.add_attendanceTextview);
        addattendanceTextview.setText(getString(R.string.update_attendance));
        addattendanceTextview.setVisibility(View.GONE);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            formattedDate = bundle.getString("fdate", "");
            classID = bundle.getInt("classID");
            sectionID = bundle.getInt("sectionID");
            attendanceSessionCount = bundle.getInt("attendanceSessionCount");
            groupID = bundle.getInt("groupID");
            isGroup = bundle.getBoolean("isGroup");

            if (attendanceSessionCount == 1) {
                radioGroup.setVisibility(View.GONE);
            } else if (attendanceSessionCount == 0) {
                radioGroup.setVisibility(View.VISIBLE);
            }

            if (!isGroup) {
                getattendancedetails();
            } else {
                getGroupAttendancedetails();
            }

        }
        currentdateTextview=(TextView)view.findViewById(R.id.currentdateTextview);
        currentdateTextview.setText(formattedDate);
        forenoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    afternoonradiobutton.setVisibility(View.GONE);
                    forenoonradiobutton.setVisibility(View.GONE);
                    //radioGroup.setVisibility(View.GONE);
                    addattendanceTextview.setVisibility(View.GONE);
                    forenoonradiobutton.setChecked(true);
                    afternoonradiobutton.setChecked(false);
                    sessionTYPE="1";
                    studentListrecyclerView.setAdapter(null);
                    attendanceDetailsListArrayList=new ArrayList<>();
                    attendanceUpdateStudentLists=new ArrayList<>();
                    if (!isGroup) {
                        getattendancedetails();
                    } else {
                        getGroupAttendancedetails();
                    }

                }
            }
        });
        afternoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    afternoonradiobutton.setVisibility(View.GONE);
                    forenoonradiobutton.setVisibility(View.GONE);
                    //radioGroup.setVisibility(View.GONE);
                    addattendanceTextview.setVisibility(View.GONE);
                    forenoonradiobutton.setChecked(false);
                    afternoonradiobutton.setChecked(true);
                    sessionTYPE="0";
                    studentListrecyclerView.setAdapter(null);
                    attendanceDetailsListArrayList=new ArrayList<>();
                    attendanceUpdateStudentLists=new ArrayList<>();
                    if (!isGroup) {
                        getattendancedetails();
                    } else {
                        getGroupAttendancedetails();
                    }
                }
            }
        });
       // getClassList();
        addAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        //  studentListrecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        classwiseStudentsLists=new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //studentListrecyclerView.setItemAnimator(new DefaultItemAnimator());
        studentListrecyclerView.setHasFixedSize(true);
        studentListrecyclerView.setItemViewCacheSize(20);
        studentListrecyclerView.setDrawingCacheEnabled(true);
        studentListrecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        studentListrecyclerView.setLayoutManager(linearLayoutManager);
        updateAttenAdapter = new UpdateAttenAdapter(attendanceUpdateStudentLists,getContext(), sessionTYPE);
        studentListrecyclerView.setAdapter(updateAttenAdapter);
        presentTextview = (TextView) view.findViewById(R.id.presentTextview);

        presentTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        if (attendanceUpdateStudentLists.size()>0) {
                            for (int i = 0; i < attendanceUpdateStudentLists.size(); i++) {
                                attendanceUpdateStudentLists.get(i).setSelectedID(attendanceUpdateStudentLists.get(i).getStudID());
                            }
                        }
                updateAttenAdapter.notifyDataSetChanged();
            }
        });


        presentTextview.setVisibility(View.GONE);
        addattendanceTextview.setText(getString(R.string.update_attendance));
        addattendanceTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentID="";
                ODStudentID="";
                LeaveStudentID="";
                if (attendanceUpdateStudentLists.size()>0) {
                    for (int i = 0; i < attendanceUpdateStudentLists.size(); i++) {
                        System.out.println("studentID ==> 1 "+attendanceUpdateStudentLists.get(i).getSelectedID());
                        System.out.println("studentID ==> 1 "+attendanceUpdateStudentLists.get(i).getStudID());
                        if (attendanceUpdateStudentLists.get(i).getSelectedID()==attendanceUpdateStudentLists.get(i).getStudID())
                        {
                            if (attendanceUpdateStudentLists.get(i).getStudentsList().size()>0) {
                                if (attendanceUpdateStudentLists.get(i).getStudentsList().get(0).getAttendance_type().equals("OD")) {
                                    ODStudentID = ODStudentID.concat(String.valueOf(attendanceUpdateStudentLists.get(i).getStudID())).concat(",");
                                } else if (attendanceUpdateStudentLists.get(i).getStudentsList().get(0).getAttendance_type().equals("Leave")) {
                                    LeaveStudentID = LeaveStudentID.concat(String.valueOf(attendanceUpdateStudentLists.get(i).getStudID())).concat(",");
                                } else {
                                    studentID = studentID.concat(String.valueOf(attendanceUpdateStudentLists.get(i).getStudID())).concat(",");
                                }
                            }
                            else {

                                studentID = studentID.concat(String.valueOf(attendanceUpdateStudentLists.get(i).getStudID())).concat(",");
                            }
                        }

                    }


                    if (studentID.length()>0) {
                        System.out.println("studentID ==> 2 "+studentID);
                        System.out.println("studentID ==> 2 "+ODStudentID);
                        System.out.println("studentID ==> 2 "+LeaveStudentID);
                        studentID = studentID.substring(0, studentID.length() - 1);
                        if (ODStudentID.length() > 0) {
                            ODStudentID = ODStudentID.substring(0, ODStudentID.length() - 1);
                        }
                        if (LeaveStudentID.length() > 0) {
                            LeaveStudentID = LeaveStudentID.substring(0, LeaveStudentID.length() - 1);
                        }
                        if (!isGroup) {
                            updateAttedancebyClasswise();
                        } else {
                            updateGroupAttedancebyClasswise();
                        }
                    }
                    else
                    {
                        Toast.makeText(getContext(), "No one Present", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //formattedDate = df.format(c);

        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);
        //getClassList();
        //getattendancedetails();
        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);
        class_spinner.setVisibility(View.GONE);
        //getStudentlist();
        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {

                } else {
                    classID=classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionID=classListResponse.getTeachersClassesList().get(position-1).getSectionID();
                    studentListrecyclerView.setAdapter(null);
                    updateAttenAdapter.notifyDataSetChanged();
                    presentTextview.setVisibility(View.GONE);
                    addattendanceTextview.setVisibility(View.GONE);

                    afternoonradiobutton.setVisibility(View.GONE);
                    forenoonradiobutton.setVisibility(View.GONE);
                    radioGroup.setVisibility(View.GONE);
                    //getStudentlist();

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        return view;
    }

//    public void getStudentlist()
//    {
//        if (Util.isNetworkAvailable())
//        {
//            showProgress();
//            StudentListbyclassParam addAttendanceParams=new StudentListbyclassParam();
//            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
//            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
//            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
//            addAttendanceParams.setClassID(String.valueOf(classID));
//            addAttendanceParams.setSectionID(String.valueOf(sectionID));
//
//            vidyauraAPI.getClasswiseStudentList(addAttendanceParams).enqueue(new Callback<StudentListResponse>() {
//                @Override
//                public void onResponse(Call<StudentListResponse> call, Response<StudentListResponse> response) {
//                    hideProgress();
//
//                    if (response.body() != null) {
//                        studentListResponse = response.body();
//                        if (studentListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//
//                            classwiseStudentsLists = response.body().getClasswiseStudentsList();
//                            if (studentListResponse.getStatus() == Util.STATUS_SUCCESS) {
//                                presentTextview.setVisibility(View.VISIBLE);
//                                addattendanceTextview.setVisibility(View.VISIBLE);
//                                afternoonradiobutton.setVisibility(View.VISIBLE);
//                                forenoonradiobutton.setVisibility(View.VISIBLE);
//                                radioGroup.setVisibility(View.VISIBLE);
//
//                                if (studentListResponse.getClasswiseStudentsList().size()!=0) {
//
//                                     //attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());
//
//                                    //studentListAdapter.notifyDataSetChanged();
//                                    for (int y=0;y<studentListResponse.getClasswiseStudentsList().size();y++)
//                                    {
//                                        studentLists.add(new StudentList(studentListResponse.getClasswiseStudentsList().get(y).getStudID(),
//                                                studentListResponse.getClasswiseStudentsList().get(y).getFname(),studentListResponse.getClasswiseStudentsList().get(y).getLname(),studentListResponse.getClasswiseStudentsList().get(y).getStudent_photo(),studentListResponse.getClasswiseStudentsList().get(y).getStudID()));
//                                    }
//
//                                    studentListAdapter = new AddAttenAdapter(studentLists,getContext());
//                                    studentListrecyclerView.setAdapter(studentListAdapter);
//
//                                }
//
//                            } else {
//                                Toast.makeText(getContext(), studentListResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                        else
//                        {
//                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            getActivity().finishAffinity();
//
//                        }
//
//                    }
//                    else
//                    {
//                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
//
//                    }
//                }
//                @Override
//                public void onFailure(Call<StudentListResponse> call, Throwable t) {
//                    hideProgress();
//                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
//
//
//                }
//            });
//
//        }
//        else
//        {
//            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
//        }
//    }

    public void getattendancedetails()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AttendancedetailsParams attendanceListParams=new AttendancedetailsParams();
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            attendanceListParams.setSessionType(Integer.parseInt(sessionTYPE));
            attendanceListParams.setAttendanceDate(formattedDate);
            attendanceListParams.setClassID(String.valueOf(classID));
            attendanceListParams.setSectionID(String.valueOf(sectionID));

            vidyauraAPI.getAttendanceDetailsList(attendanceListParams).enqueue(new Callback<AttendancedetailsResponse>() {
                @Override
                public void onResponse(Call<AttendancedetailsResponse> call, Response<AttendancedetailsResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        //radioGroup.setVisibility(View.VISIBLE);
                        addattendanceTextview.setVisibility(View.VISIBLE);
                        addattendanceTextview.setVisibility(View.VISIBLE);
                        afternoonradiobutton.setVisibility(View.VISIBLE);
                        forenoonradiobutton.setVisibility(View.VISIBLE);
                        attendancedetailsResponse = response.body();

                        if (attendancedetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//                            attendanceDetailsListArrayList.clear();
                            attendanceDetailsListArrayList = response.body().getAttendanceDetailsList();
                            if (attendancedetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (attendanceDetailsListArrayList.size()!=0) {

                                    // attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());
                                    //stu_counts.setText(attendanceDetailsListArrayList.get(0).getTotalStudentsCount()+" "+getString(R.string.students));
                                    //present_count.setText(attendanceDetailsListArrayList.get(0).getStudentsPresentCount());
                                    //absent_count.setText(attendanceDetailsListArrayList.get(0).getStudentsAbsentCount());
                                    //subAttendanceAdapter = new SubAttendanceAdapter(attendanceDetailsListArrayList,getContext());
                                    //studentListrecyclerView.setAdapter(subAttendanceAdapter);
                                    for (int i = 0; i < attendancedetailsResponse.getAttendanceDetailsList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                        if (sessionTYPE.equals("1")) {
                                            if (attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session() == 1) {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session(),attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            } else {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), 0, 0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            }
                                        }
                                        else
                                        {
                                            if (attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session() == 1) {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session(),0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            } else {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),   attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), 0, attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session(), 0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            }
                                        }

                                    }
                                    updateAttenAdapter = new UpdateAttenAdapter(attendanceUpdateStudentLists,getContext(),sessionTYPE);
                                    studentListrecyclerView.setAdapter(updateAttenAdapter);

                                }

                            } else {
                                Toast.makeText(getContext(), attendancedetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AttendancedetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getGroupAttendancedetails()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AttendancedetailsParams attendanceListParams=new AttendancedetailsParams();
            attendanceListParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            attendanceListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            attendanceListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            attendanceListParams.setSessionType(Integer.parseInt(sessionTYPE));
            attendanceListParams.setAttendanceDate(formattedDate);
            attendanceListParams.setGroupID(String.valueOf(groupID));

            vidyauraAPI.getGroupAttendanceDetailsList(attendanceListParams).enqueue(new Callback<AttendancedetailsResponse>() {
                @Override
                public void onResponse(Call<AttendancedetailsResponse> call, Response<AttendancedetailsResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        //radioGroup.setVisibility(View.VISIBLE);
                        addattendanceTextview.setVisibility(View.VISIBLE);
                        afternoonradiobutton.setVisibility(View.VISIBLE);
                        forenoonradiobutton.setVisibility(View.VISIBLE);
                        attendancedetailsResponse = response.body();

                        if (attendancedetailsResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
//                            attendanceDetailsListArrayList.clear();
                            attendanceDetailsListArrayList = response.body().getAttendanceDetailsList();
                            if (attendancedetailsResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (attendanceDetailsListArrayList.size()!=0) {

                                    // attendanceDetailsListArrayList.addAll(attendancedetailsResponse.getAttendanceDetailsList());
                                    //stu_counts.setText(attendanceDetailsListArrayList.get(0).getTotalStudentsCount()+" "+getString(R.string.students));
                                    //present_count.setText(attendanceDetailsListArrayList.get(0).getStudentsPresentCount());
                                    //absent_count.setText(attendanceDetailsListArrayList.get(0).getStudentsAbsentCount());
                                    //subAttendanceAdapter = new SubAttendanceAdapter(attendanceDetailsListArrayList,getContext());
                                    //studentListrecyclerView.setAdapter(subAttendanceAdapter);
                                    for (int i = 0; i < attendancedetailsResponse.getAttendanceDetailsList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                        if (sessionTYPE.equals("1")) {
                                            if (attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session() == 1) {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session(),attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            } else {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), 0, 0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getForenoon_session(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            }
                                        }
                                        else
                                        {
                                            if (attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session() == 1) {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session(),0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            } else {
                                                attendanceUpdateStudentLists.add(new AttendanceUpdateStudentList(attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudID(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getFname(),
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getLname(),   attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudent_photo(), 0, attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList().get(0).getAfternoon_session(), 0,
                                                        attendancedetailsResponse.getAttendanceDetailsList().get(i).getStudentsList()));
                                            }
                                        }

                                    }
                                    updateAttenAdapter = new UpdateAttenAdapter(attendanceUpdateStudentLists,getContext(),sessionTYPE);
                                    studentListrecyclerView.setAdapter(updateAttenAdapter);

                                }

                            } else {
                                Toast.makeText(getContext(), attendancedetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AttendancedetailsResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void updateAttedancebyClasswise()
    {
        if (Util.isNetworkAvailable())
        {
            showProgress();
            AddAttendanceParams addAttendanceParams=new AddAttendanceParams();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addAttendanceParams.setClassID(String.valueOf(classID));
            addAttendanceParams.setSectionID(String.valueOf(sectionID));
            addAttendanceParams.setStudentID(studentID);
            addAttendanceParams.setODStudentID(ODStudentID);
            addAttendanceParams.setLeaveStudentID(LeaveStudentID);
            addAttendanceParams.setAttendanceDate(formattedDate);
            if (attendanceSessionCount == 1) {
                addAttendanceParams.setSessionType("2");
            } else if (attendanceSessionCount == 0) {
                addAttendanceParams.setSessionType(sessionTYPE);
            }

            vidyauraAPI.updateAttendance(addAttendanceParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        AddFeedResponse feedResponse = response.body();
                        if (feedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (feedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    }
                    else
                    {
                        Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }
                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(),getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();


                }
            });

        }
        else
        {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void updateGroupAttedancebyClasswise() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddAttendanceParams addAttendanceParams = new AddAttendanceParams();
            addAttendanceParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            addAttendanceParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            addAttendanceParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            addAttendanceParams.setGroupID(String.valueOf(groupID));
            addAttendanceParams.setStudentID(studentID);
            addAttendanceParams.setODStudentID(ODStudentID);
            addAttendanceParams.setLeaveStudentID(LeaveStudentID);
            addAttendanceParams.setAttendanceDate(formattedDate);
            if (attendanceSessionCount == 1) {
                addAttendanceParams.setSessionType("2");
            } else if (attendanceSessionCount == 0) {
                addAttendanceParams.setSessionType(sessionTYPE);
            }

            vidyauraAPI.updateGroupAttendance(addAttendanceParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        AddFeedResponse feedResponse = response.body();
                        if (feedResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (feedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), feedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

}
