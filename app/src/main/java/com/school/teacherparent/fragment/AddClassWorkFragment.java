package com.school.teacherparent.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.Interface.OnImageRemoved;
import com.school.teacherparent.activity.BaseActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AddResultAdapter;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.ClassAdapter;
import com.school.teacherparent.adapter.ExamComposechapterAdpater;
import com.school.teacherparent.adapter.ExamComposetopicAdpater;
import com.school.teacherparent.adapter.ExamComposetopicAdpaterwithCheckbox;
import com.school.teacherparent.adapter.SelectedImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AcademicTermResponse;
import com.school.teacherparent.models.AddClassworkParams;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.GetChapterListParam;
import com.school.teacherparent.models.GetExamTermParam;
import com.school.teacherparent.models.GetSubjectListParam;
import com.school.teacherparent.models.GetTopicListParam;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.ListofChapterforDialog;
import com.school.teacherparent.models.ListofTopicforDialog;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SyllabusChapterListResponse;
import com.school.teacherparent.models.SyllabusClassListResponse;
import com.school.teacherparent.models.SyllabusSubjectListResponse;
import com.school.teacherparent.models.SyllabusTopicListResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class AddClassWorkFragment extends BaseFragment  implements OnImageRemoved {

    private Spinner class_spinner, sub_spinner, chap_spinner, topic_spinner,term_spinner;
    private EditText edit_desc;
    private LinearLayout date_picker;
    private LinearLayout upload_liner;
    private View btnDatePicker;
    private int mYear, mMonth, mDay, mHour, mMinute;

    private TextView dateTextview;
    private LinearLayout lin_date;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY = 1;

    private static final String IMAGE_DIRECTORY = "/demonuts";
    private ImageView img_upload;
    private TextView add_classwork;
    private ImageView back;
    private AddResultAdapter mAdapter;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    SyllabusTopicListResponse topicListResponse;
    List<ListofTopicforDialog> topicList = new ArrayList<ListofTopicforDialog>();
    Dialog topicDialog;
     Dialog chapterDialog;
    ListView topiclistView,chapterlistView;
    ExamComposetopicAdpater examComposetopicAdpater;
     ExamComposechapterAdpater examComposechapterAdpater;
    Button dialogTopicButton,dialogChapterButton;
    boolean[] selectedtopicItemsupdate;
    SearchView dialogSearchView,chapterdialogSearchView;
    List<ListofTopicforDialog> searchtopicList = new ArrayList<ListofTopicforDialog>();
    TextView topicnameTextview,searchnameTextview,chapternameTextview,chaptersearchnameTextview;
    String topicID="",chapterID="";
    SyllabusClassListResponse syllabusClassListResponse;
    ArrayList<String> classlist = new ArrayList<String>();
    ArrayAdapter<String> classspinnerArray;
    SyllabusSubjectListResponse syllabusSubjectListResponse;
    ArrayList<String> subjectlist = new ArrayList<String>();
    ArrayAdapter<String> subjectspinnerArray;
    int examTermId = 0, classId = 0, sectionId = 0, subject_Id = 0, chapter_Id = 0, topic_Id = 0;
    //ExamTermResponse examTermResponse;
    AcademicTermResponse aceTermResponse;
    ArrayList<String> examTermSpinner = new ArrayList<String>();
    ArrayAdapter<String> spinnerexamTermAdapter;
    SyllabusChapterListResponse chapterListResponse;
    //List<ListofChapter> chapterList = new ArrayList<ListofChapter>();
    static List<ListofChapterforDialog> chapterList = new ArrayList<ListofChapterforDialog>();
    List<ListofChapterforDialog> searchchapterList = new ArrayList<ListofChapterforDialog>();
    static  String dueDate="";
    List<SelectedImageList> imageList = new ArrayList<>();
    SelectedImageAdapter selectedImageAdapter;
    RecyclerView image_recycle;
    String imgname = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_class_work, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        SidemenuDetailActivity.title.setText(getResources().getString(R.string.add_classwork));
        SidemenuDetailActivity.ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), NotificationActivity.class));
            }
        });

         /* back=view.findViewById(R.id.back);
          back.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  getActivity().finish();
              }
          });*/
        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);
        sub_spinner = (Spinner) view.findViewById(R.id.sub_spinner);
        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);
        topic_spinner = (Spinner) view.findViewById(R.id.topic_spinner);
        term_spinner=(Spinner)view.findViewById(R.id.term_spinner);
        edit_desc = (EditText) view.findViewById(R.id.edit_desc);
        edit_desc.setTypeface(ProximaNovaFont.getInstance(getContext()).getSemiBoldTypeFace());
        date_picker = (LinearLayout) view.findViewById(R.id.date_picker);
        lin_date = (LinearLayout) view.findViewById(R.id.lin_date);
        upload_liner = (LinearLayout) view.findViewById(R.id.upload_linear);
        img_upload = (ImageView) view.findViewById(R.id.img_upload);
        dateTextview = (TextView) view.findViewById(R.id.dateTextview);
        add_classwork = view.findViewById(R.id.add_classwork);
        topicDialog = new Dialog(getContext());
        topicDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        topicDialog.setCancelable(false);
        topicDialog.setContentView(R.layout.custom_dialog);
        topiclistView = (ListView) topicDialog.findViewById(R.id.listView1);
        dialogTopicButton = (Button) topicDialog.findViewById(R.id.btn_dialog);
        dialogSearchView=(SearchView)topicDialog.findViewById(R.id.dialogSearchView);
        topicnameTextview=(TextView)view.findViewById(R.id.topicnameTextview);
        searchnameTextview=(TextView)topicDialog.findViewById(R.id.searchnameTextview);

        chapterDialog=new Dialog(getContext());
        chapterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        chapterDialog.setCancelable(false);
        chapterDialog.setContentView(R.layout.chapter_custom_dialog);
        chapterdialogSearchView=(SearchView)chapterDialog.findViewById(R.id.dialogSearchView);
        chaptersearchnameTextview=(TextView)chapterDialog.findViewById(R.id.searchnameTextview);
        chapternameTextview=(TextView)view.findViewById(R.id.chapternameTextview);
        chapterlistView = (ListView) chapterDialog.findViewById(R.id.chapterlistView);
        dialogChapterButton=(Button)chapterDialog.findViewById(R.id.btn_dialog) ;
        selectedImageAdapter = new SelectedImageAdapter(imageList, getActivity(), this);
        //LinearLayoutManager linearLayoutManager=new GridLayoutManager(getActivity(),5);
        image_recycle = view.findViewById(R.id.image_recycle);
        image_recycle.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        image_recycle.setAdapter(selectedImageAdapter);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        final String formattedDate = df.format(c);
        dateTextview.setText(formattedDate);
        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
        dueDate = currentdueDATE.format(c);
        examComposechapterAdpater=new ExamComposechapterAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton, chapterList);
        examComposetopicAdpater=new ExamComposetopicAdpater(searchtopicList,getContext());
        //getTopicList("4");
        //getExamtermList();
        getAcedtermList();
        dialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSearchView.setIconified(false);
            }
        });

        chapterdialogSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chapterdialogSearchView.setIconified(false);
            }
        });
        dialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1)
                {
//                    Collections.sort(classList, new Comparator<ListofClass>() {
//
//                        @Override
//                        public int compare(ListofClass lhs, ListofClass rhs) {
//                            //here getTitle() method return app name...
//                            return newText.compareTo(lhs.getClassname());
//
//                        }
//
//
//                    });
                    searchtopicList.clear();
                    for(int y=0;y<topicList.size();y++)
                    {
                        if (topicList.get(y).getTopic_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchtopicList.add(topicList.get(y));

                        }
                    }
                    if (searchtopicList.size()>0) {
                        examComposetopicAdpater=new ExamComposetopicAdpater(searchtopicList,getContext());
                        topiclistView.setAdapter(examComposetopicAdpater);
                        examComposetopicAdpater.notifyDataSetChanged();
                    }
                    else
                    {
                        topiclistView.setVisibility(View.GONE);
                        searchnameTextview.setVisibility(View.VISIBLE);
                    }



                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    topiclistView.setVisibility(View.VISIBLE);
                    searchnameTextview.setVisibility(View.GONE);
                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                    topiclistView.setAdapter(examComposetopicAdpater);
                    examComposetopicAdpater.notifyDataSetChanged();
                }
                return false;
            }
        });

        chapterdialogSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.toString().length()>=1) {
                    searchchapterList.clear();
                    for(int y=0;y<chapterList.size();y++)
                    {
                        if (chapterList.get(y).getChapter_name().toLowerCase().contains(newText.toString().toLowerCase()))
                        {

                            searchchapterList.add(chapterList.get(y));

                        }

                    }


                    if (searchchapterList.size()>0) {

                        chapterlistView.setVisibility(View.VISIBLE);
                        chaptersearchnameTextview.setVisibility(View.GONE);
                        examComposechapterAdpater=new ExamComposechapterAdpater(searchchapterList,getContext(), chapterDialog, dialogChapterButton,chapterList);
                        chapterlistView.setAdapter(examComposechapterAdpater);

                    }
                    else
                    {
                        chapterlistView.setVisibility(View.GONE);
                        chaptersearchnameTextview.setVisibility(View.VISIBLE);
                    }
                }
                else if (newText.toString().length()==0)
                {
                    //classList.addAll(searchclassList);
                    chapterlistView.setVisibility(View.VISIBLE);
                    chaptersearchnameTextview.setVisibility(View.GONE);
                    examComposechapterAdpater=new ExamComposechapterAdpater(chapterList,getContext(),chapterDialog,dialogChapterButton, chapterList);
                    chapterlistView.setAdapter(examComposechapterAdpater);

                }
                return false;
            }
        });



        dialogTopicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String topicName="";
                topicID="";
                for (int y=0;y<topicList.size();y++)
                {

                    if (topicList.get(y).getSelectedID()!=0) {
                        topicName = topicName.concat(String.valueOf(topicList.get(y).getTopic_name()).concat(","));
                        topicID=topicID.concat(String.valueOf(topicList.get(y).getId())).concat(",");
                    }

                }
                if (topicName.length()>0 && topicID.length()>0) {
                    topicName = topicName.substring(0, topicName.length() - 1);
                    topicID = topicID.substring(0, topicID.length() - 1);
                    topicnameTextview.setText(topicName);
                    topicnameTextview.setError(null);
                }
                else
                {
                    topicnameTextview.setText("");
                }
                topicDialog.hide();
            }
        });

        dialogChapterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String chapterName="";
                chapterID="";
                for (int y=0;y<chapterList.size();y++)
                {


                    if (chapterList.get(y).getSelectedID()!=0) {
                        chapterName = chapterName.concat(String.valueOf(chapterList.get(y).getChapter_name()).concat(","));
                        chapterID=chapterID.concat(String.valueOf(chapterList.get(y).getId())).concat(",");
                    }

                }
                if (chapterName.length()>0 && chapterID.length()>0) {
                    chapterName = chapterName.substring(0, chapterName.length() - 1);
                    chapterID = chapterID.substring(0, chapterID.length() - 1);
                    chapternameTextview.setText(chapterName);
                    getTopicList(chapterID);
                }
                else
                {
                    chapternameTextview.setText("");
                }
                examComposechapterAdpater.notifyDataSetChanged();
                chapterDialog.hide();
            }
        });


        examComposechapterAdpater.setOnClickListen(new ClassAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
              Toast.makeText(getContext(),+position,Toast.LENGTH_LONG).show();
            }
        });
        topicnameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (topicList.size()>0) {
                    topicDialog.show();
                }
            }
        });
        chapternameTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chapterList.size()>0) {
                    chapterDialog.show();
                }

            }
        });
        add_classwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValidate();

            }
        });


        subjectlist.add("Select Subject");
        subjectspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, subjectlist);

        subjectspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subjectspinnerArray);

        spinnerexamTermAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, examTermSpinner);
        spinnerexamTermAdapter.setDropDownViewResource(R.layout.spinner_item);

        term_spinner.setAdapter(spinnerexamTermAdapter);

        //date picker
        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");


                                    Date c = Calendar.getInstance().getTime();


                                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                                    String formattedDate = df.format(c);

                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                                    Date date1 = null;
                                    try {
                                        date1 = sdf.parse(formt.format(dateObj));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = sdf.parse(formattedDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");
                                        //dateTextview.setText(formt.format(dateObj));
                                        //dateTextview.setError(null);
//                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
//                                        dueDate = currentdueDATE.format(dateObj);
                                        dateTextview.setError(getString(R.string.validDate));
                                        dueDate = "";
                                        dateTextview.setText(formt.format(dateObj));
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
//                                        dateTextview.setError(getString(R.string.validDate));
//                                        dueDate = "";
                                        dateTextview.setText(formt.format(dateObj));
                                        dateTextview.setError(null);
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        dueDate = currentdueDATE.format(dateObj);

                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                        dateTextview.setText(formt.format(dateObj));
                                        dateTextview.setError(null);
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        dueDate = currentdueDATE.format(dateObj);
                                    } else {
                                        System.out.println("How to get here?");
                                        dateTextview.setError(getString(R.string.validDate));
                                        dueDate = "";
                                    }
                                }
                            }, mYear, mMonth, mDay);




                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            }
        });

        //current date
        String dateStr = "04/05/2010";
        SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("MMMM dd, yyyy");

        String newDateStr = postFormater.format(dateObj);

        //class spinner

        class_spinner = (Spinner) view.findViewById(R.id.class_spinner);

        classspinnerArray = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, classlist);

        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);

        term_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    examTermId = 0;
                } else {
                    examTermId = aceTermResponse.getAcademicTermsList().get(position - 1).getId();
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    classId = 0;
                    sectionId = 0;
                } else {
                    classId = syllabusClassListResponse.getSyllabusClassesList().get(position - 1).getClassID();
                    sectionId = syllabusClassListResponse.getSyllabusClassesList().get(position - 1).getSectionID();
                    if (examTermId > 0) {
                        subject_Id = 0;
                        subjectspinnerArray.clear();
                        subjectspinnerArray.notifyDataSetChanged();
                        chapterID="";
                        chapternameTextview.setText("");
                        chapterList.clear();
                        examComposechapterAdpater.notifyDataSetChanged();
                        topicID="";
                        topicnameTextview.setText("");
                        topicList.clear();
                        examComposetopicAdpater.notifyDataSetChanged();
                        getSubjecyList();
                        }
                    }







        }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });






        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    subject_Id = 0;

                } else {


                    subject_Id = syllabusSubjectListResponse.getSyllabusSubjectsList().get(position - 1).getSubjectID();
                    chapterID="";
                    chapternameTextview.setText("");
                    chapterList.clear();
                    examComposechapterAdpater.notifyDataSetChanged();
                    topicID="";
                    topicnameTextview.setText("");
                    topicList.clear();
                    examComposetopicAdpater.notifyDataSetChanged();



                        getExamchapterList();

                    }

                }






            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //chapter spinner
        List<String> chapspinner = new ArrayList<String>();
        chapspinner.add("My Mother at Sixty Two");
        chapspinner.add("Shakesphere Poem");
        chapspinner.add("Punctuations");
        chapspinner.add("Articles and Pronouns");
        chapspinner.add("Nouns and Verbs");
        chapspinner.add("Active and Passive Voice");
        chap_spinner = (Spinner) view.findViewById(R.id.chap_spinner);

        ArrayAdapter<String> chapspinnerarrayadapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, chapspinner);

        chapspinnerarrayadapter.setDropDownViewResource(R.layout.spinner_item);

        chap_spinner.setAdapter(chapspinnerarrayadapter);

        chap_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //topic spinner
        List<String> topicspinner = new ArrayList<String>();
        topicspinner.add("Topic:2.4");
        topicspinner.add("Topic:3.2");
        topicspinner.add("Topic:4.4");
        topicspinner.add("Topic:5.9");
        topicspinner.add("Topic:8.6");
        topicspinner.add("Topic:7.7");
        topic_spinner = (Spinner) view.findViewById(R.id.topic_spinner);

        ArrayAdapter<String> topicspinnerarrayadapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, topicspinner);

        topicspinnerarrayadapter.setDropDownViewResource(R.layout.spinner_item);

        topic_spinner.setAdapter(topicspinnerarrayadapter);

        topic_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        //upload image
        upload_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageList.size()>=4){
                    Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
                    return;
                }
                imageChooser();
                /*new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .scale(500,500)
                        .enableDebuggingMode(true)
                        .build();*/
            }
        });
        return view;
    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public void setPickedImageDetails(Bitmap bitmap, List<String> path) {

        for (int i=0;i<path.size();i++)
        {
            imageList.add(new SelectedImageList(path.get(i),true));
        }
        if (imageList.size() > 4) {
            Toast.makeText(getActivity(), R.string.maxm_four_image, Toast.LENGTH_SHORT).show();
        } else {
            selectedImageAdapter.notifyDataSetChanged();
        }


    }

//public static void reloadchapterDialog(int chapter_id)
//    {
////        for (int i=0;i<chapterList.size();i++)
////        {
////            if (chapterList.get(i).getSelectedID()==chapter_id)
////            {
////                chapterList.get(i).setSelectedID(chapter_id);
////            }
////            else
////            {
////                chapterList.get(i).setSelectedID(0);
////            }
////        }
//
//        examComposechapterAdpater.notifyDataSetChanged();
//
//
//    }

//    public void setChapterData()
//    {
//        String chapterName="";
//        chapterID="";
//        for (int y=0;y<chapterList.size();y++)
//        {
//
//
//            if (chapterList.get(y).getSelectedID()!=0) {
//                chapterName = chapterName.concat(String.valueOf(chapterList.get(y).getChapter_name()).concat(","));
//                chapterID=chapterID.concat(String.valueOf(chapterList.get(y).getId())).concat(",");
//            }
//
//        }
//        if (chapterName.length()>0 && chapterID.length()>0) {
//            chapterName = chapterName.substring(0, chapterName.length() - 1);
//            chapterID = chapterID.substring(0, chapterID.length() - 1);
//            chapternameTextview.setText(chapterName);
//            getTopicList(chapterID);
//        }
//        else
//        {
//            chapternameTextview.setText("");
//        }
//        chapterDialog.hide();
//    }
    //after uplading image, save a image
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getclassListParams.setType("1");
            vidyauraAPI.getSyllabusClasses(getclassListParams).enqueue(new Callback<SyllabusClassListResponse>() {
                @Override
                public void onResponse(Call<SyllabusClassListResponse> call, Response<SyllabusClassListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        syllabusClassListResponse = response.body();
                        if (syllabusClassListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (syllabusClassListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < syllabusClassListResponse.getSyllabusClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(syllabusClassListResponse.getSyllabusClassesList().get(i).getClassName()+"-"+
                                            syllabusClassListResponse.getSyllabusClassesList().get(i).getSection());

                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
//                                getSubjecyList();
                                if (classlist.size() > 1) {
                                    classspinnerArray.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(getContext(), "No Class Mapped", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Toast.makeText(getContext(), syllabusClassListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SyllabusClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getSubjecyList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            subjectlist.clear();
            subjectlist.add("Select Subject");
            GetSubjectListParam getSubjectListParam = new GetSubjectListParam();
            getSubjectListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getSubjectListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getSubjectListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getSubjectListParam.setClassID(String.valueOf(classId));
            getSubjectListParam.setSectionID(String.valueOf(sectionId));
            getSubjectListParam.setType("1");
            vidyauraAPI.getSyllabusSubjects(getSubjectListParam).enqueue(new Callback<SyllabusSubjectListResponse>() {
                @Override
                public void onResponse(Call<SyllabusSubjectListResponse> call, Response<SyllabusSubjectListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        syllabusSubjectListResponse = response.body();
                        if (syllabusSubjectListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (syllabusSubjectListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < syllabusSubjectListResponse.getSyllabusSubjectsList().size(); i++) {

                                    subjectlist.add(syllabusSubjectListResponse.getSyllabusSubjectsList().get(i).getName());

                                }
//                                subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
                                //                              classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
                                //chapter_Id=examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id();
                                subjectspinnerArray.notifyDataSetChanged();
                                // getExamchapterList();




                            } else {
                                Toast.makeText(getContext(), syllabusSubjectListResponse.getStatusText(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SyllabusSubjectListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

//    private void getExamtermList() {
//        if (Util.isNetworkAvailable()) {
//            showProgress();
//            examTermSpinner.clear();
//            examTermSpinner.add("Select Term");
//            GetExamTermParam getExamTermParam = new GetExamTermParam();
//            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
//            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
//            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
//            vidyauraAPI.getExamTermsList(getExamTermParam).enqueue(new Callback<ExamTermResponse>() {
//                @Override
//                public void onResponse(Call<ExamTermResponse> call, Response<ExamTermResponse> response) {
//                    hideProgress();
//                    if (response.body() != null) {
//                        examTermResponse = response.body();
//                        if (examTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
//                            if (examTermResponse.getStatus() == Util.STATUS_SUCCESS) {
//                                if (examTermResponse.getExamTermsList().size() > 0) {
//                                    for (int i = 0; i < examTermResponse.getExamTermsList().size(); i++) {
//                                        examTermSpinner.add(examTermResponse.getExamTermsList().get(i).getExam_title());
//                                    }
//                                    spinnerexamTermAdapter.notifyDataSetChanged();
//
//                                    getClassList();
//                                } else {
//                                    Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                Toast.makeText(getContext(), examTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        } else {
//                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            getActivity().finishAffinity();
//
//                        }
//                    } else {
//                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ExamTermResponse> call, Throwable t) {
//                    hideProgress();
//                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
//                }
//            });
//
//        } else {
//            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
//        }
//    }

    private void getAcedtermList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            examTermSpinner.clear();
            examTermSpinner.add("Select Term");
//            SCHOOLID= String.valueOf((sharedPreferences.getInt(Constants.SCHOOLID,0)));
//            USERID= sharedPreferences.getString(Constants.USERID,"");
//            SETUSERTYPE= String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0));
            GetExamTermParam getExamTermParam = new GetExamTermParam();
            getExamTermParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getExamTermParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getExamTermParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getAcademicTerms(getExamTermParam).enqueue(new Callback<AcademicTermResponse>() {
                @Override
                public void onResponse(Call<AcademicTermResponse> call, Response<AcademicTermResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        aceTermResponse = response.body();
                        if (aceTermResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (aceTermResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (aceTermResponse.getAcademicTermsList().size() > 0) {
                                    for (int i = 0; i < aceTermResponse.getAcademicTermsList().size(); i++) {
                                        examTermSpinner.add(aceTermResponse.getAcademicTermsList().get(i).getName());
                                    }
                                    spinnerexamTermAdapter.notifyDataSetChanged();

                                    getClassList();
                                } else {
                                    Toast.makeText(getContext(), aceTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), aceTermResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AcademicTermResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getExamchapterList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            chapterList.clear();
            GetChapterListParam getChapterListParam = new GetChapterListParam();
            getChapterListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getChapterListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getChapterListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getChapterListParam.setClassID(String.valueOf(classId));
            getChapterListParam.setSubjectID(String.valueOf(subject_Id));
            getChapterListParam.setSectionID(String.valueOf(sectionId));
            getChapterListParam.setTermID(String.valueOf(examTermId));
            getChapterListParam.setType("1");
            vidyauraAPI.getSyllabusChapters(getChapterListParam).enqueue(new Callback<SyllabusChapterListResponse>() {
                @Override
                public void onResponse(Call<SyllabusChapterListResponse> call, Response<SyllabusChapterListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        chapterListResponse = response.body();
                        if (chapterListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (chapterListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (chapterListResponse.getSyllabusChaptersList().size() > 0) {
                                    for (int i = 0; i < chapterListResponse.getSyllabusChaptersList().size(); i++) {
                                        chapterList.add(new ListofChapterforDialog(chapterListResponse.getSyllabusChaptersList().get(i).getChapterID()
                                                ,chapterListResponse.getSyllabusChaptersList().get(i).getChapter_name(),0));
                                       // chapterspinnerArray.add(chapterListResponse.getChaptersList().get(i).getChapter_name());
                                    }
                                    //chapterspinnerArray.notifyDataSetChanged();
//                                    subject_Id=examDetailByIdResponse.getExamScheduleList().get(0).getSubject_id();
//                                    getTopicList(String.valueOf(examDetailByIdResponse.getExamScheduleList().get(0).getChapter_id()));
                                    examComposechapterAdpater=new ExamComposechapterAdpater(chapterList,getContext(), chapterDialog, dialogChapterButton, chapterList);
                                    chapterlistView.setAdapter(examComposechapterAdpater);
                                    //chapterDialog.show();



                                } else {
                                    Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), chapterListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SyllabusChapterListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    private void getTopicList(String chapterID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            topicList.clear();
            GetTopicListParam getTopicListParam = new GetTopicListParam();
            getTopicListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getTopicListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getTopicListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            getTopicListParam.setClassID(String.valueOf(classId));
            getTopicListParam.setSubjectID(String.valueOf(subject_Id));
            getTopicListParam.setChapterID(String.valueOf(chapterID));
            getTopicListParam.setType("1");
            getTopicListParam.setSectionID(String.valueOf(sectionId));
            vidyauraAPI.getSyllabusTopics(getTopicListParam).enqueue(new Callback<SyllabusTopicListResponse>() {
                @Override
                public void onResponse(Call<SyllabusTopicListResponse> call, Response<SyllabusTopicListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        topicListResponse = response.body();
                        if (topicListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (topicListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (topicListResponse.getSyllabusTopicsList().size() > 0) {
                                    for (int i = 0; i < topicListResponse.getSyllabusTopicsList().size(); i++) {

                                        topicList.add(new ListofTopicforDialog(topicListResponse.getSyllabusTopicsList().get(i).getTopicID()
                                                ,topicListResponse.getSyllabusTopicsList().get(i).getTopic_name(),0));

                                    }

                                    examComposetopicAdpater=new ExamComposetopicAdpater(topicList,getContext());
                                    topiclistView.setAdapter(examComposetopicAdpater);
                                    //topicDialog.show();

                                } else {
                                    Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), topicListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SyllabusTopicListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
    public void  isValidate()
    {

        if (examTermId!=0) {
            if (classId != 0) {
                if (subject_Id != 0) {
                    if (chapterID.length()!=0 ) {
                        if (topicID.length() != 0) {
                            if (dueDate.length() != 0) {
                             //   if (edit_desc.getText().toString().length() > 0) {
                                    chapterDialog.dismiss();
                                    topicDialog.dismiss();
                                    //addClasswork();
                                ((BaseActivity) getActivity()).uploadFile(imageList, getString(R.string.s3_classwork_path),
                                        imgname, new OnAmazonFileuploaded() {
                                            @Override
                                            public void FileStatus(int status, String filename) {
                                                if (status == 1) {
                                                    addClasswork();

                                                } else {
                                                    Toast.makeText(getActivity(), R.string.uploadfail, Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });
                             //   } else {
                                 //   Toast.makeText(getContext(),getString(R.string.validdescription), Toast.LENGTH_SHORT).show();
                              //  }

                            } else {
                                Toast.makeText(getContext(), getString(R.string.validDate), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), getString(R.string.selecttopic), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.selectchapter), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.selectsubject), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.selectclass), Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getContext(), getString(R.string.term), Toast.LENGTH_SHORT).show();
        }
    }

    public String getImagelist(){
        JSONObject obj = null;
        JSONArray jsonArray = new JSONArray();
        String images = null;
        for (int y=0;y<imageList.size();y++)
        {
            images = imageList.get(y).getImage();
            obj = new JSONObject();
            try {
                obj.put("attachment",images.substring(images.lastIndexOf("/")+1,images.length()));
                String extension = getExt(images);
                obj.put("extension", extension);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(obj);
        }
//        for (String images:imageList){
//            obj = new JSONObject();
//            try {
//                obj.put("feedAttachment",images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                obj.put("feedAttachmentExtension", extension);
//            } catch (JSONException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            jsonArray.put(obj);

//                AddFeedParams.feedAttachment attachment=new AddFeedParams().new feedAttachment();
//                attachment.setFeedAttachment(images.substring(images.lastIndexOf("/")+1,images.length()));
//                String extension = getExt(images);
//                attachment.setFeedAttachmentExtension(extension);
//                feedAttachments.add(attachment);
        //}
        return jsonArray.toString();

    }

    public String getExt(String filePath){
        int strLength = filePath.lastIndexOf(".");
        if(strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }
    private void addClasswork() {
      
        if (Util.isNetworkAvailable()) {

            showProgress();
            AddClassworkParams addClassworkParams=new AddClassworkParams();
            addClassworkParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            addClassworkParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addClassworkParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addClassworkParams.setClassID(String.valueOf(classId));
            addClassworkParams.setSubjectID(String.valueOf(subject_Id));
            addClassworkParams.setChapterID(String.valueOf(chapterID));
            addClassworkParams.setTopicID(topicID);
            if(edit_desc.getText().toString().length()>0) {
                addClassworkParams.setClassworkDesc(edit_desc.getText().toString().trim());
            }
            else
            {
                addClassworkParams.setClassworkDesc("No description");
            }
            addClassworkParams.setSectionID(String.valueOf(sectionId));
            addClassworkParams.setCompletedON(dueDate);
            addClassworkParams.setTermID(String.valueOf(examTermId));
            addClassworkParams.setDocument(getImagelist());


            vidyauraAPI.addClasswork(addClassworkParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                hideProgress();
                    if (response.body() != null) {

                        AddFeedResponse addFeedResponse = response.body();
                        if (addFeedResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (addFeedResponse.getStatus() == Util.STATUS_SUCCESS) {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } else {
                                Toast.makeText(getContext(), addFeedResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                //getActivity().finish();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }

                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                     hideProgress();
                    Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            });


        } else {
            Toast.makeText(getActivity(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onimageremoved(int pos, View view) {
        if (view.getId() == R.id.close) {
            imageList.remove(pos);
            selectedImageAdapter.notifyItemRemoved(pos);
        }
    }
}
