package com.school.teacherparent.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.adapter.CustomPagerAdapter;
import com.school.teacherparent.adapter.PhotosGalleryAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GalleryParams;
import com.school.teacherparent.models.PhotosGalleryDTO;
import com.school.teacherparent.models.Photoslist;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.school.teacherparent.fragment.ParentGalleryFragment.selected;
import static com.school.teacherparent.fragment.ParentGalleryFragment.selectedSchool;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentPhotosGalleryFragment extends BaseFragment {
    private GridView gridView;
    private RecyclerView recyclerview;
    public static PhotosGalleryAdapter photosGalleryAdapter;
    private EditText edit;
    private List<PhotosGalleryDTO> gridList = new ArrayList<>();
    private FloatingActionButton addResult;
    LinearLayout lin;
    protected static final int GALLERY_PICTURE = 1;
    protected static final int CAMERA_REQUEST = 0;
    private Intent pictureActionIntent = null;
    private static int RESULT_LOAD_IMG = 1;
    private int GALLERY = 1;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    int limit = 50;
    static int offset = 0;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public boolean isLoading = true;
    static List<Photoslist> photoslists = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    private boolean loading = true;
    ParentGalleryFragment parentGalleryFragment;
    ProgressDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photos_gallery, container, false);
        recyclerview = (RecyclerView) view.findViewById(R.id.recycle_grid);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        lin = (LinearLayout) view.findViewById(R.id.lin);
        //   edit=(EditText)view.findViewById(R.id.edit);
        photosGalleryAdapter = new PhotosGalleryAdapter(photoslists, getActivity());

        addResult = view.findViewById(R.id.add_result);
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        addResult.setOnClickListener(new View.OnClickListener() {

            public int REQUEST_CAPTURE_IMAGE = -1;
            public PopupWindow pwindow;
            public Context context;
            public Bitmap mBitmap;
            public AlertDialog.Builder myAlertDialog;
            public int position;

            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();


                View alertLayout = inflater.inflate(R.layout.popupwindow, null);
                final ImageView camera = alertLayout.findViewById(R.id.camera);
                final ImageView gallery = alertLayout.findViewById(R.id.gallery);
                LinearLayout camera_layout = alertLayout.findViewById(R.id.camera_layout);
                LinearLayout gallery_layout = alertLayout.findViewById(R.id.gallery_layout);
                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme));
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                BottomSheetDialog dialog1 = new BottomSheetDialog(getActivity());
                dialog1.setContentView(R.layout.popupwindow);
                dialog1.show();

                camera_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                       /* Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        startActivity(intent);*/
                    }
                });

                gallery_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(getActivity());

                        Toast.makeText(getActivity(), "hello", Toast.LENGTH_SHORT).show();

//                        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                        // Start the Intent
//
//                        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//
//                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                        intent.setType("*/*");
//                        startActivityForResult(intent, 7);
                    }
                });
            }

        });


        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(photosGalleryAdapter);
        parentGalleryFragment = new ParentGalleryFragment();

        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset = offset + limit;
                            getgalleryList();

                        }
                    }
                }
            }
        });


        photosGalleryAdapter.setOnClickListen(new PhotosGalleryAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                initiatepopupwindow(photoslists.get(position).getImage(), "png", position);

            }
        });
        return view;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(getActivity(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                    // img_upload.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    //   Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }

        if (resultCode == RESULT_OK) {
            String PathHolder = data.getData().getPath();
            Toast.makeText(getActivity(), PathHolder, Toast.LENGTH_LONG).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getgalleryList() {
        if (Util.isNetworkAvailable()) {

            showProgressDialog();
            VidyauraApplication.getContext().getComponent().inject(this);
            editor = sharedPreferences.edit();
            GalleryParams galleryParams = new GalleryParams();
            galleryParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            galleryParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            galleryParams.setSchoolID(String.valueOf(selectedSchool()));
            galleryParams.setMediaType(2);
            galleryParams.setLimit(limit);
            galleryParams.setOffset(offset);
            galleryParams.setStudID(selected());

            vidyauraAPI.getGalleryList(galleryParams).enqueue(new Callback<GalleryListResponse>() {
                @Override
                public void onResponse(Call<GalleryListResponse> call, Response<GalleryListResponse> response) {
                    mDialog.dismiss();
                    isLoading = false;
                    if (response.body() != null) {
                        Gson gson = new Gson();
                        //System.out.println("response ==> photos ==> "+gson.toJson(response.body()));
                        GalleryListResponse galleryListResponse = response.body();
                        if (galleryListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (galleryListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < galleryListResponse.getGalleryListResult().size(); i++) {
                                    if (galleryListResponse.getGalleryListResult().get(i).getAlbumList() != null) {
                                        if (galleryListResponse.getGalleryListResult().get(i).getAlbumList().size() > 0) {
                                            for (int y = 0; y < galleryListResponse.getGalleryListResult().get(i).getAlbumList().size(); y++) {
                                                photoslists.add(new Photoslist(galleryListResponse.getGalleryListResult().get(i).getId(),
                                                        galleryListResponse.getGalleryListResult().get(i).getTitle(), galleryListResponse.getGalleryListResult().get(i).getDescription(),
                                                        galleryListResponse.getGalleryListResult().get(i).getAlbumList().get(y).getMedia_path()));
                                            }
                                        }
                                    }
                                }

                                photosGalleryAdapter.notifyDataSetChanged();

                            } else {
                                isLoading = true;
                                loading = false;
                                if (photoslists.size() <= 0) {
                                    Toast.makeText(getContext(), galleryListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GalleryListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading = false;
                    Activity activity = getActivity();
                    if (isAdded() && activity != null) {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            isLoading = false;
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;

        if (isVisible) {
            //photoslists.clear();
            offset = 0;
            if (isLoading) {
                getgalleryList();
                isLoading = false;
            }

        }


    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void onResume() {
        super.onResume();
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void onPause() {
        super.onPause();
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            photoslists.clear();
            offset = 0;
            getgalleryList();
            isLoading = false;
        }
    }

    private void initiatepopupwindow(String mediapath, String media_type, final int position) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.show_image, null);
        //final ImageView imageView=alertLayout.findViewById(R.id.imageview);
        //final JzvdStd video_selected=alertLayout.findViewById(R.id.video_selected);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewpager);
        ImageView ivLeftArrow = alertLayout.findViewById(R.id.iv_left_arrow);
        ImageView ivRightArrow = alertLayout.findViewById(R.id.iv_right_arrow);
        ImageView iv_close = alertLayout.findViewById(R.id.iv_close);
        viewPager.setAdapter(new CustomPagerAdapter(getActivity(), photoslists, 1));
        viewPager.setCurrentItem(position);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
        //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
        //   alert.setTitle("Info");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();

        if (photoslists.size() > 1) {
            ivLeftArrow.setVisibility(View.VISIBLE);
            ivRightArrow.setVisibility(View.VISIBLE);
        } else {
            ivLeftArrow.setVisibility(View.GONE);
            ivRightArrow.setVisibility(View.GONE);
        }

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ivLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true); //getItem(-1) for previous
            }
        });
        ivRightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true); //getItem(-1) for previous
            }
        });

        /*if (media_type.equals("png")) {
            video_selected.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/" + mediapath;
            Picasso.get().load(url).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    imageView.setImageResource(R.mipmap.school);
                }
            });
        }
        else
        {
            video_selected.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);

            video_selected.setUp((getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath)
                    , Jzvd.SCREEN_WINDOW_NORMAL, "");
            video_selected.setSystemTimeAndBattery();

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.mipmap.icon_video_thumbnail);
            requestOptions.error(R.mipmap.icon_video_thumbnail);
            Glide.with(getContext())
                    .load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath)
                    .apply(requestOptions)
                    .thumbnail(Glide.with(getContext()).load(getString(R.string.s3_baseurl) +
                            getString(R.string.s3_feeds_path) +"/"+
                            mediapath))
                    .into(video_selected.thumbImageView);
        }*/
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


    }

}

