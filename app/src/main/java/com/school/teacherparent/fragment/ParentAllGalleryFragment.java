package com.school.teacherparent.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.adapter.AllGalleryAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.GalleryListResponse;
import com.school.teacherparent.models.GalleryParams;
import com.school.teacherparent.models.Gallerylist;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.ParentGalleryFragment.selected;
import static com.school.teacherparent.fragment.ParentGalleryFragment.selectedSchool;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParentAllGalleryFragment extends BaseFragment {
    private GridView gridView;
    private AllGalleryAdapter gridAdapter;
    private RecyclerView mediaRecyclerview;
    public static AllGalleryAdapter galleryAdapter;
    private EditText edit;
    private FloatingActionButton addResult;

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    int limit=50;
    static int offset=0;
    private Boolean isStarted = false;
    private Boolean isVisible = false;
    public boolean isLoading = true;
    static List<Gallerylist> gallerylists=new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    GalleryListResponse galleryListResponse;
    LinearLayoutManager linearLayoutManager;
    private boolean loading = true;
    ParentGalleryFragment parentGalleryFragment;
    ProgressDialog mDialog;

    @Override
    public void onDestroy() {
        super.onDestroy();
        isLoading=false;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_gallery, container, false);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        mediaRecyclerview=(RecyclerView)view.findViewById(R.id.recycle_grid);
        //   edit=(EditText)view.findViewById(R.id.edit);
        addResult =  view.findViewById(R.id.add_result);
        addResult.setVisibility(View.GONE);
        mDialog = new ProgressDialog(getContext());
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);
        galleryAdapter = new AllGalleryAdapter(gallerylists,getActivity());
        galleryAdapter.setOnClickListen(new AllGalleryAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {

                Bundle bundle=new Bundle();
                bundle.putSerializable("albumlist",gallerylists.get(position).getAlbumList());
                startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).
                        putExtra("type", "gallerydetails").putExtra("albumlist",bundle));

            }
        });

        /*mediaRecyclerview.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addResult.setVisibility(View.VISIBLE);
                }
                else
                {
                    addResult.setVisibility(View.GONE);
                }
            }
        });
        addResult.setOnClickListener(new View.OnClickListener() {
            public String type;
            public int position;

            @Override
            public void onClick(View v) {
             

          initiatepopupwindow();

            }

            private void initiatepopupwindow() {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.add_alert_gallery, null);
                *//*final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);

*//*
                final EditText albumName=alertLayout.findViewById(R.id.edit);
                final TextView cancel = alertLayout.findViewById(R.id.cancel);
                final TextView create = alertLayout.findViewById(R.id.create);
                final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.actionSheetTheme1));
                //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
                //   alert.setTitle("Info");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(false);

                final AlertDialog dialog = alert.create();
                dialog.show();
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // edit.setText("");
                        if (albumName.getText().toString().length()>0) {
                            startActivity(new Intent(getActivity(), SidemenuDetailActivity.class).
                                    putExtra("type", "addgallery").putExtra("albumName", albumName.getText().toString().trim()));

                            dialog.dismiss();

                        }
                        else
                        {
                            Toast.makeText(getContext(),getString(R.string.album_name),Toast.LENGTH_SHORT).show();
                        }


                    }
                });

                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);


            }
        });*/

        parentGalleryFragment = new ParentGalleryFragment();

//        linearLayoutManager = new LinearLayoutManager(getActivity());
//        linearLayoutManager.setAutoMeasureEnabled(true);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        gridLayoutManager = new GridLayoutManager(getActivity(),2);
        mediaRecyclerview.setLayoutManager(gridLayoutManager);
        mediaRecyclerview.setItemAnimator(new DefaultItemAnimator());
        mediaRecyclerview.setAdapter(galleryAdapter);
        mediaRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getgalleryList();

                        }
                    }
                }
            }
        });
        return view;
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    public void getgalleryList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            VidyauraApplication.getContext().getComponent().inject(this);
            editor = sharedPreferences.edit();
            GalleryParams galleryParams = new GalleryParams();
            galleryParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            galleryParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            galleryParams.setSchoolID(String.valueOf(selectedSchool()));
            galleryParams.setMediaType(1);
            galleryParams.setLimit(limit);
            galleryParams.setOffset(offset);
            galleryParams.setStudID(selected());

            vidyauraAPI.getGalleryList(galleryParams).enqueue(new Callback<GalleryListResponse>() {
                @Override
                public void onResponse(Call<GalleryListResponse> call, Response<GalleryListResponse> response) {
                    isLoading=true;
                    mDialog.dismiss();
                    if (response.body() != null) {
                         galleryListResponse = response.body();
                        Gson gson = new Gson();
                        //System.out.println("response ==> all ==> "+gson.toJson(galleryListResponse.getGalleryListResult()));
                        if (galleryListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (galleryListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < galleryListResponse.getGalleryListResult().size(); i++) {
                                    if (galleryListResponse.getGalleryListResult().get(i).getAlbumList() != null) {
                                        if (galleryListResponse.getGalleryListResult().get(i).getAlbumList().size() > 0) {
                                            gallerylists.add(new Gallerylist(galleryListResponse.getGalleryListResult().get(i).getId(),
                                                    galleryListResponse.getGalleryListResult().get(i).getTitle(), galleryListResponse.getGalleryListResult().get(i).getDescription(),
                                                    galleryListResponse.getGalleryListResult().get(i).getAlbumList()));
                                        }
                                    }
                                }
                                if (gallerylists.size() > 0) {
                                    galleryAdapter.notifyDataSetChanged();
                                }

                            } else {
                                isLoading=true;
                                loading=false;
                                Toast.makeText(getContext(), galleryListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            isLoading=true;
                            Intent i = new Intent(getActivity(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finishAffinity();

                        }


                    } else {
                        isLoading=true;
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<GalleryListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    isLoading=true;
                    Activity activity = getActivity();
                    if (isAdded() && activity != null) {
                        Toast.makeText(getContext(), getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            isLoading=true;
            Toast.makeText(getContext(), getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible){
            //gallerylists.clear();
            offset=0;
            if (isLoading) {
                getgalleryList();
                isLoading=false;
            }

        }



    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void onResume() {
        super.onResume();
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void onPause() {
        super.onPause();
        JzvdStd.releaseAllVideos();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted){
            gallerylists.clear();
            if (isLoading) {
                offset=0;
                getgalleryList();
                isLoading=false;
            }
        }


    }


 /*   @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        isLoading=false;
    }*/

}

