package com.school.teacherparent.calender;

import java.text.ParseException;

public interface HorizontalCalendarListener {
    void updateMonthOnScroll(DayDateMonthYearModel selectedDate) throws ParseException;
    void newDateSelected(DayDateMonthYearModel selectedDate) throws ParseException;
}
