package com.school.teacherparent.calender;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.utils.Constants;

import java.util.ArrayList;

public class CalAdapter extends RecyclerView.Adapter<CalAdapter.MyViewHolder> {

    Context context;

    Object toCallBack;

    DayDateMonthYearModel lastDaySelected;

    int color = R.color.black;

    ArrayList<DayDateMonthYearModel> dayModelList = new ArrayList<>();
    TextView clickedTextView = null;
    ArrayList<TextView> dateArrayList = new ArrayList<>();
    ArrayList<TextView> dayArrayList = new ArrayList<>();
    ArrayList<View> dividerArrayList = new ArrayList<>();
    public static ArrayList<Integer> currentPosition = new ArrayList<>();

    public CalAdapter(Context context  , ArrayList<DayDateMonthYearModel> dayModelList   ){
        this.context = context;
        this.dayModelList  = dayModelList;
    }

    public void setCallback(Object toCallBack){
        this.toCallBack = toCallBack;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView day, date;
        // ImageView haveAppointment;
        //public View divider;
        LinearLayout ll_dat;

        public MyViewHolder(View view) {
            super(view);
            day =  view.findViewById(R.id.day);
            date =  view.findViewById(R.id.date);
            //haveAppointment =  view.findViewById(R.id.have_appointment);
            //divider =  view.findViewById(R.id.divider);
            ll_dat =  view.findViewById(R.id.ll_dat);

        }
    }

    @Override
    public CalAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_day_layout, parent, false);
        return  new CalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        char t = dayModelList.get(position).day.charAt(0);

        holder.day.setText(dayModelList.get(position).day + " ");
        holder.date.setText(dayModelList.get(position).date);
        holder.date.setTag(position);
        dateArrayList.add(holder.date);
        dayArrayList.add(holder.day);
        holder.day.setTextColor(context.getResources().getColor(color));
        holder.date.setTextColor(context.getResources().getColor(color));

        if (Constants.SingleType.contains(dayModelList.get(position).date+"-"+dayModelList.get(position).monthNumeric+"-"+dayModelList.get(position).year)) {
            holder.ll_dat.setBackground(context.getResources().getDrawable(R.drawable.background_selected_day));
            holder.day.setTextColor(context.getResources().getColor(R.color.white));
            holder.date.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.ll_dat.setBackground(context.getResources().getDrawable(R.drawable.background_unselected_day));
            holder.day.setTextColor(context.getResources().getColor(R.color.grayTextColor));
            holder.date.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (dayModelList.get(position).isToday) {
            holder.ll_dat.setBackground(context.getResources().getDrawable(R.drawable.background_selected_day));
            holder.day.setTextColor(context.getResources().getColor(R.color.white));
            holder.date.setTextColor(context.getResources().getColor(R.color.white));
            /*try {
                CallBack cb = new CallBack(toCallBack, "newDateSelected");
                cb.invoke(dayModelList.get(position));
            } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }*/
            dayModelList.get(position).isToday = false;
        }
    }


    @Override
    public int getItemCount() {
        return dayModelList.size();
    }

    public void add(DayDateMonthYearModel DDMYModel) {
        dayModelList.add(DDMYModel);
        notifyItemInserted(dayModelList.size() - 1);
    }

    @Override
    public void onViewAttachedToWindow(MyViewHolder holder) {
        holder.setIsRecyclable(false);
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        holder.setIsRecyclable(false);
        super.onViewDetachedFromWindow(holder);
    }


    public void changeAccent(int color ){
        this.color = color;
        for(int i = 0 ; i < dateArrayList.size()  ; i++){
            dayArrayList.get(i).setTextColor(context.getResources().getColor(color));
            dateArrayList.get(i).setTextColor(context.getResources().getColor(color));
            dividerArrayList.get(i).setBackgroundColor(context.getResources().getColor(color));
        }
    }

}
