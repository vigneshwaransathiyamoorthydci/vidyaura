package com.school.teacherparent.calender;

public class DayDateMonthYearModel {
    public String date;
    public String month;
    public String year;
    public String day;
    public String monthNumeric;
    public Boolean isToday;
}
