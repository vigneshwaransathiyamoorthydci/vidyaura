package com.school.teacherparent.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.adapter.LeaveHistoryAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.AddLeaveParams;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.GetStudentHistoryParam;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.LeaveHistoryListResponse;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.CarouselEffectTransformer;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.MyPagerAdapter;
import com.school.teacherparent.utils.ProximaNovaFont;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentLeaveApplyActivity extends BaseActivity implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    //private ViewPager viewpagerTop, viewPagerBackground;
    public static final int ADAPTER_TYPE_TOP = 1;
    public static final int ADAPTER_TYPE_BOTTOM = 2;



    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int count=0,session=2;
    ChildListResponseResponse childListResponseResponse;
    LeaveHistoryListResponse leaveHistoryListResponse;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    ArrayList<ChildListResponseResponse.parentsStudList>  parentsStudListArrayList;
    ArrayList<LeaveHistoryListResponse.leaveHistory>  leaveHistoryArrayList;
    RecyclerView leavehistory_recycle;
    LinearLayoutManager linearLayoutManager;
    TextView nodataTextview;
    ConstraintLayout startdate_picker,enddate_picker,leaveheaderConstraintlayout;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView startdateTextview,enddateTextview,submitleave;
    String leavestartDate,leaveendDate;
    EditText leavedescriptionTextview,leavereasonTextview;
    int classID,studentID,sectionID,studentSchoolID;
    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialogg;
    ImageView backImageview,noti;
    NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leavetest);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        parentsStudListArrayList=new ArrayList<>();
        leaveHistoryArrayList=new ArrayList<>();
        leavehistory_recycle=(RecyclerView)findViewById(R.id.leavehistory_recycle);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        leavehistory_recycle.setNestedScrollingEnabled(false);
        leavehistory_recycle.setItemAnimator(new DefaultItemAnimator());
        leavehistory_recycle.setLayoutManager(linearLayoutManager);
        nodataTextview=(TextView)findViewById(R.id.nodataTextview);
        startdate_picker=(ConstraintLayout)findViewById(R.id.startdate_picker);
        enddate_picker=(ConstraintLayout)findViewById(R.id.enddate_picker);
        leaveheaderConstraintlayout=(ConstraintLayout)findViewById(R.id.leaveheaderConstraintlayout);
        //leaveheaderConstraintlayout.setVisibility(View.GONE);
        startdateTextview=(TextView)findViewById(R.id.startdateTextview);
        enddateTextview=(TextView)findViewById(R.id.enddateTextview);
        leavedescriptionTextview=(EditText)findViewById(R.id.leavedescriptionTextview);
        leavedescriptionTextview.setTypeface(ProximaNovaFont.getInstance(this).getSemiBoldTypeFace());
        leavereasonTextview=(EditText)findViewById(R.id.leavereasonTextview);
        leavereasonTextview.setTypeface(ProximaNovaFont.getInstance(this).getSemiBoldTypeFace());
        submitleave=(TextView)findViewById(R.id.submitleave);
        dsvStudentList = findViewById(R.id.dsv_student_list);
        backImageview = findViewById(R.id.backImageview);

        mDialogg = new ProgressDialog(this);
        mDialogg.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialogg.setIndeterminate(true);
        mDialogg.setCancelable(false);

        noti=findViewById(R.id.noti);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentLeaveApplyActivity.this,NotificationActivity.class));

            }
        });

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = currentDATE.format(c);
        startdateTextview.setText(getDateFormatforleave(currentDate)+" ");
        enddateTextview.setText(getDateFormatforleave(currentDate)+" ");
        leavestartDate=currentDate;
        leaveendDate=currentDate;
        startdate_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == startdate_picker) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(ParentLeaveApplyActivity.this,
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    Calendar choosenDate = Calendar.getInstance();
                                    choosenDate.set(year, monthOfYear, dayOfMonth);

                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");


                                    Date c = Calendar.getInstance().getTime();


                                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                                    String formattedDate = df.format(c);

                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                                    Date date1 = null;
                                    try {
                                        date1 = sdf.parse(formt.format(dateObj));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = sdf.parse(formattedDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");

                                        leavestartDate = "";
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        leavestartDate = currentdueDATE.format(dateObj);
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            startdateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            startdateTextview.setText(formt.format(dateObj)+" ");
                                            startdateTextview.setError(null);
                                        }
                                        leavestartDate = currentdueDATE.format(dateObj);
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
//                                        dateTextview.setError(getString(R.string.validDate));
//                                        leavestartDate = "";
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            startdateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            startdateTextview.setText(formt.format(dateObj)+" ");
                                            startdateTextview.setError(null);
                                        }
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        leavestartDate = currentdueDATE.format(dateObj);

                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            startdateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            startdateTextview.setText(formt.format(dateObj)+" ");
                                            startdateTextview.setError(null);
                                        }
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        leavestartDate = currentdueDATE.format(dateObj);
                                    } else {
                                        System.out.println("How to get here?");
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            startdateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            startdateTextview.setError(getString(R.string.validDate));
                                        }
                                        leavestartDate = "";
                                    }
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());


                    datePickerDialog.show();
                }
            }
        });
        enddate_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == enddate_picker) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(ParentLeaveApplyActivity.this,
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    Calendar choosenDate = Calendar.getInstance();
                                    choosenDate.set(year, monthOfYear, dayOfMonth);

                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");


                                    Date c = Calendar.getInstance().getTime();


                                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
                                    String formattedDate = df.format(c);

                                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                                    Date date1 = null;
                                    try {
                                        date1 = sdf.parse(formt.format(dateObj));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = sdf.parse(formattedDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            enddateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            enddateTextview.setText(formt.format(dateObj)+" ");
                                            enddateTextview.setError(null);
                                        }
                                        leaveendDate = currentdueDATE.format(dateObj);
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
//                                        dateTextview.setError(getString(R.string.validDate));
//                                        leavestartDate = "";
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            enddateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            enddateTextview.setText(formt.format(dateObj)+" ");
                                            enddateTextview.setError(null);
                                        }
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        leaveendDate = currentdueDATE.format(dateObj);

                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            enddateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            enddateTextview.setText(formt.format(dateObj)+" ");
                                            enddateTextview.setError(null);
                                        }
                                        SimpleDateFormat currentdueDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        leaveendDate = currentdueDATE.format(dateObj);
                                    } else {
                                        System.out.println("How to get here?");
                                        if (choosenDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                                            enddateTextview.setError("This is Sunday");
                                            Toast.makeText(ParentLeaveApplyActivity.this, "This is Sunday", Toast.LENGTH_SHORT).show();
                                        }else{
                                            enddateTextview.setError(getString(R.string.validDate));
                                        }
                                        leaveendDate = "";
                                    }
                                }
                            }, mYear, mMonth, mDay);

                    datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());


                    datePickerDialog.show();
                }
            }
        });
        submitleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leavereasonTextview.getText().toString().trim().length()>3 && leavedescriptionTextview.getText().toString().trim().length()>3) {
                    if(startdateTextview.getError()==null && enddateTextview.getError()==null)
                    {
                        Date date1= null,date2 = null;
                        try {
                            date1 = new SimpleDateFormat("dd MMM yyyy").parse(startdateTextview.getText().toString().trim());
                            date2 = new SimpleDateFormat("dd MMM yyyy").parse(enddateTextview.getText().toString().trim());
                            if(date1.getTime() == date2.getTime()){
                                oneDayDialog(1);
                            }else if(date1.getTime() < date2.getTime()){
                                addLeave();
                            }
                            else{
                                Toast.makeText(ParentLeaveApplyActivity.this, "Please select Validate date", Toast.LENGTH_SHORT).show();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                    }else{
                        Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.validDate), Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    if(leavereasonTextview.getText().toString().trim().length()<=3){
                        leavereasonTextview.setError(getString(R.string.leavereason));
                    }else if(leavedescriptionTextview.getText().toString().trim().length()<=3){
                        leavedescriptionTextview.setError(getString(R.string.leavereasondesc));
                    }

                }
            }
        });
        backImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getStudentList();

    }

    /**
     * Initialize all required variables
     */
    private void init() {
        /*viewpagerTop = (ViewPager) findViewById(R.id.viewpagerTop);
        viewPagerBackground = (ViewPager) findViewById(R.id.viewPagerbackground);

        viewpagerTop.setClipChildren(false);
        viewpagerTop.setPageMargin(getResources().getDimensionPixelOffset(R.dimen.pager_margin));
        viewpagerTop.setOffscreenPageLimit(3);
        viewpagerTop.setPageTransformer(false, new CarouselEffectTransformer(this)); // Set transformer*/
    }

    /**
     * Setup viewpager and it's events
     */
    private void setupViewPager() {

        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(this,parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID=childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID=childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID= childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID= childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID=childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID=childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID= childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID= childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());

        // Set Top ViewPager Adapter
        /*MyPagerAdapter adapter = new MyPagerAdapter(this, count, ADAPTER_TYPE_TOP,parentsStudListArrayList);
        viewpagerTop.setAdapter(adapter);

        // Set Background ViewPager Adapter
        MyPagerAdapter adapterBackground = new MyPagerAdapter(this, count, ADAPTER_TYPE_BOTTOM,parentsStudListArrayList);
        viewPagerBackground.setAdapter(adapterBackground);


        viewpagerTop.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int index = 0;

            @Override
            public void onPageSelected(int position) {
                index = position;

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int width = viewPagerBackground.getWidth();
                viewPagerBackground.scrollTo((int) (width * position + width * positionOffset), 0);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    viewPagerBackground.setCurrentItem(index);
                    Log.d("VIGNESH","waran");
                    classID=parentsStudListArrayList.get(index).getClass_id();
                    sectionID= parentsStudListArrayList.get(index).getSection_id();
                    studentID= parentsStudListArrayList.get(index).getStudID();

                    getLeaveHistory(parentsStudListArrayList.get(index).getStudID());

                }

            }
        });*/
    }

    /**
     * Handle all click event of activity
     */
    public void clickEvent(View view) {
        switch (view.getId()) {
            case R.id.linMain:
                if (view.getTag() != null) {
                    int poisition = Integer.parseInt(view.getTag().toString());
                    //Toast.makeText(getApplicationContext(), "Poistion: " + poisition, Toast.LENGTH_LONG).show();
                    //viewpagerTop.setCurrentItem(poisition);

//                    Intent intent=new Intent(this,FullScreenActivity.class);
//                    intent.putExtra(EXTRA_IMAGE,listItems[poisition]);
//                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view.findViewById(R.id.imageCover), EXTRA_TRANSITION_IMAGE);
//                    ActivityCompat.startActivity(this, intent, options.toBundle());
                }
                break;
        }
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            getLeaveHistory(parentsStudListArrayList.get(adapterPosition).getStudID());
        }
    }

    private void showProgressDialog() {
        mDialogg.show();
        mDialogg.setContentView(R.layout.custom_progress_view);
    }

    private void oneDayDialog(int position) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_leave_approval);
        dialog.show();
        TextView tvHead = dialog.findViewById(R.id.tv_head);
        Button btnOneDay = dialog.findViewById(R.id.btn_approval);
        CheckBox checkboxFn = dialog.findViewById(R.id.checkbox_approve);
        TextView textFn = dialog.findViewById(R.id.text_approve);
        CheckBox checkboxAn = dialog.findViewById(R.id.checkbox_reject);
        TextView textAn = dialog.findViewById(R.id.text_reject);
        ImageView ivClose = dialog.findViewById(R.id.iv_close);

        tvHead.setText("Leave Session");
        textFn.setText("F.N");
        textAn.setText("A.N");
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnOneDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkboxFn.isChecked() || checkboxAn.isChecked()){
                    if(checkboxFn.isChecked() && checkboxAn.isChecked()){
                        session=2;
                        addLeave();
                        dialog.dismiss();
                    }else if(checkboxFn.isChecked()){
                        session=1;
                        addLeave();
                        dialog.dismiss();}
                    else if(checkboxAn.isChecked()){
                        session=0;
                        addLeave();
                        dialog.dismiss();}

                }else{
                    Toast.makeText(ParentLeaveApplyActivity.this, "Please select any one option", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    private void addLeave() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            AddLeaveParams addLeaveParams = new AddLeaveParams();
            addLeaveParams.setUserID(sharedPreferences.getString(Constants.USERID,""));
            addLeaveParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            addLeaveParams.setSchoolID(String.valueOf(studentSchoolID));
            addLeaveParams.setClassID(String.valueOf(classID));
            addLeaveParams.setSectionID(String.valueOf(sectionID));
            addLeaveParams.setStudID(String.valueOf(studentID));
             long count= getDaysBetweenDates(leavestartDate,leaveendDate);
             if (count==0)
             {
                 addLeaveParams.setDaysCount(1);
             }
             else
             {
                 addLeaveParams.setDaysCount(getDaysBetweenDates(leavestartDate,leaveendDate));
             }

            addLeaveParams.setFromDate(leavestartDate);
            addLeaveParams.setToDate(leaveendDate);
            addLeaveParams.setLeaveSession("");
            addLeaveParams.setIsHalfDay(0);
            System.out.println("Session------>"+session);
            addLeaveParams.setIsSessionType(session);

            addLeaveParams.setReason(leavereasonTextview.getText().toString().trim());
            addLeaveParams.setDescription(leavedescriptionTextview.getText().toString().trim());
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.applyLeave(addLeaveParams).enqueue(new Callback<AddFeedResponse>() {
                @Override
                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                    mDialogg.dismiss();
                    if (response.body()!=null) {
                        AddFeedResponse childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {


                            SendNotification sendNotification = new SendNotification();
                            sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
                            sendNotification.setSchoolID(String.valueOf(studentSchoolID));
                            sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
                            sendNotification.setType("leave");
                            sendNotification.setType_id(childListResponseResponse.getTypeId());
                            vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
                                @Override
                                public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                                    AddFeedResponse addFeedResponse1 = response.body();
                                    if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                                        leavereasonTextview.setText("");
                                        leavedescriptionTextview.setText("");
                                        Date c = Calendar.getInstance().getTime();
                                        SimpleDateFormat currentDATE = new SimpleDateFormat("yyyy-MM-dd");
                                        String currentDate = currentDATE.format(c);
                                        startdateTextview.setText(getDateFormatforleave(currentDate)+" ");
                                        enddateTextview.setText(getDateFormatforleave(currentDate)+" ");
                                        leavestartDate=currentDate;
                                        leaveendDate=currentDate;
                                        Toast.makeText(ParentLeaveApplyActivity.this, childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();

                                        getLeaveHistory(studentID);
                                    }
                                }

                                @Override
                                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                                    hideProgress();
                                    Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                                }
                            });



                    }
                        else
                        {
                            mDialogg.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            addLeave();
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                    mDialogg.dismiss();
                    Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }



    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));

            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialogg.dismiss();
                    if (response.body()!=null)
                    {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {

                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size()>0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                //leaveheaderConstraintlayout.setVisibility(View.VISIBLE);
                                init();
                                setupViewPager();
                                getLeaveHistory(childListResponseResponse.getParentsStudList().get(0).getStudID());
                            }
                            else
                            {
                                Toast.makeText(ParentLeaveApplyActivity.this,childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }}else
                            {
                                submitleave.setEnabled(false);
                                Toast.makeText(ParentLeaveApplyActivity.this,childListResponseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }


                        }
                        else
                        {
                            mDialogg.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialogg.dismiss();
                    Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void getLeaveHistory(final int studID) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentHistoryParam getStudentHistoryParam = new GetStudentHistoryParam();
            getStudentHistoryParam.setUserID(sharedPreferences.getString(Constants.USERID,""));
            getStudentHistoryParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            getStudentHistoryParam.setSchoolID(String.valueOf(studentSchoolID));
            getStudentHistoryParam.setStudID(studID);
            leavehistory_recycle.setAdapter(null);
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.studLeaveHistory(getStudentHistoryParam).enqueue(new Callback<LeaveHistoryListResponse>() {
                @Override
                public void onResponse(Call<LeaveHistoryListResponse> call, Response<LeaveHistoryListResponse> response) {
                    mDialogg.dismiss();
                    if (response.body()!=null)
                    {
                        leaveHistoryListResponse = response.body();
                        if (leaveHistoryListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (leaveHistoryListResponse.getStudLeaveHistory().size()>0)
                            {
                                if (leaveHistoryListResponse.getStudLeaveHistory().get(0).getLeaveHistory().size()>0)
                                {
                                    leaveHistoryArrayList=leaveHistoryListResponse.getStudLeaveHistory().get(0).getLeaveHistory();
                                    LeaveHistoryAdapter leaveHistoryAdapter=new LeaveHistoryAdapter(getApplicationContext(),leaveHistoryArrayList);
                                    leavehistory_recycle.setAdapter(leaveHistoryAdapter);
                                    leavehistory_recycle.setVisibility(View.VISIBLE);
                                    nodataTextview.setVisibility(View.GONE);
                                }
                                else
                                {
                                    //Toast.makeText(MainActivityte.this,leaveHistoryListResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                    leavehistory_recycle.setVisibility(View.GONE);
                                    nodataTextview.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                //Toast.makeText(MainActivityte.this,leaveHistoryListResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                leavehistory_recycle.setVisibility(View.GONE);
                                nodataTextview.setVisibility(View.VISIBLE);
                            }}
                            else
                            {
                                //Toast.makeText(MainActivityte.this,leaveHistoryListResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                leavehistory_recycle.setVisibility(View.GONE);
                                nodataTextview.setVisibility(View.VISIBLE);
                            }



                        }
                        else
                        {
                            mDialogg.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, leaveHistoryListResponse.getToken()).commit();
                            getLeaveHistory(studID);
                        }
                    }
                    else
                    {

                    }

                }

                @Override
                public void onFailure(Call<LeaveHistoryListResponse> call, Throwable t) {
                    mDialogg.dismiss();
                    Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(ParentLeaveApplyActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public static final String DATE_FORMAT = "yyyy-mm-dd";  //or use "M/d/yyyy"

    public static long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }
    private static long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
