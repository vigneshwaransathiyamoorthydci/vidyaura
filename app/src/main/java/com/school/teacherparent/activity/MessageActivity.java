package com.school.teacherparent.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.MessageListAdapter;
import com.school.teacherparent.adapter.PreviewImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MessageActivity extends AppCompatActivity {

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    EditText tvReceiverName;
    ImageView ivBack, ivReceiverProfile, ivAttachment, ivSend, ivSearch;
    RecyclerView rvChatList;
    TextView tvNoMessage,tv_name;
    EditText etMessage;
    MessageListAdapter messageListAdapter;
    ArrayList<MessageList> messageListArrayList = new ArrayList<>();
    Uri imageUri;
    FirebaseStorage storage;
    StorageReference storageReference, retrieve;
    ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        initViews();
        performAction();
    }

    private void initViews() {
        ivBack = findViewById(R.id.iv_back);
        tvReceiverName = findViewById(R.id.textView);
        rvChatList = findViewById(R.id.rv_chatList);
        tvNoMessage = findViewById(R.id.tv_momessage);
        ivAttachment = findViewById(R.id.iv_attachment);
        etMessage = findViewById(R.id.et_message);
        tv_name = findViewById(R.id.tv_name);
        ivSend = findViewById(R.id.iv_send);
        ivSearch = findViewById(R.id.search);

        etMessage.requestFocus();
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        mDialog = new ProgressDialog(this);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        retrieve = storage.getReference();

        rvChatList.setHasFixedSize(true);
        rvChatList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        messageListAdapter = new MessageListAdapter(MessageActivity.this, messageListArrayList, sharedPreferences.getString(Constants.USERID, null));
        rvChatList.setAdapter(messageListAdapter);
        getMessageList();
        setMessageReadStatus();
    }

    private void performAction() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();//Fragment fragment = new MessageFragment();
                //testDrawerActivity.replaceFragment(fragment);
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvReceiverName.setVisibility(View.VISIBLE);
                tv_name.setVisibility(View.GONE);
                showKeyboard(tvReceiverName);
            }
        });

        tvReceiverName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (tvReceiverName.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(tvReceiverName.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        getMessageList(tvReceiverName.getText().toString().trim().toLowerCase());
                    }

                    return true;
                }
                else
                {
                    Toast.makeText(MessageActivity.this, R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        /*tvReceiverName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String search = s.toString();
                if (search.length() > 0) {
                    getMessageList(search.toLowerCase());
                }
            }
        });*/

        ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        ivSend.setEnabled(true);
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (imageUri != null) {
                    uploadImage(etMessage.getText().toString().trim(),imageUri);
                } else if (etMessage.getText().toString().trim().length() > 0) {*/
                if (etMessage.getText().toString().trim().length()>0) {
                    sendMessage(etMessage.getText().toString().trim(), null);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);
                }
                /*}*/
            }
        });
    }

    private void uploadImage(final String message, final List<String> mPaths) {
        final List<String> uploadedPaths = new ArrayList<>();
        for (int i=0;i<mPaths.size();i++) {
            final File imgFile = new File(mPaths.get(i));
            Uri mUploadImageUri = Uri.fromFile(imgFile);
            if (mUploadImageUri != null) {
                showProgressDialog();
                final String uid = UUID.randomUUID().toString();
                StorageReference ref = storageReference.child("message_image").child("single").child(uid);
                ref.putFile(mUploadImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        retrieve.child("message_image").child("single").child(uid).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                if (uri.toString() != null) {
                                    String image = uri.toString();
                                    uploadedPaths.add(image);
                                    if (uploadedPaths.size() == mPaths.size()) {
                                        sendMessage(message, uploadedPaths);
                                    }
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                                //progress.dismiss();
                            }
                        });

                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                //calculating progress percentage
                                double progressD = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //progress.setMax(100);
                                //progress.setProgress((int) progressD);
                                //displaying percentage in progress dialog
                                mDialog.setMessage("Uploaded " + ((int) progressD) + "%...");
                            }
                        });

            }
        }
    }


    private void sendMessage(String message,List<String> imageUrl) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

        int readStatus = 0;
        String sentTime = mdformat.format(calendar.getTime());
        Long timeStamp = System.currentTimeMillis();
        int state = sharedPreferences.getInt(Constants.USERTYPE, 0);


        String senderID = sharedPreferences.getString(Constants.USERID, null);
        String senderName = sharedPreferences.getString(Constants.FNAME, "") + " " + sharedPreferences.getString(Constants.LNAME, "");
        String url = null;
        if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
            url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
        } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
            url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
        }
        String senderImage = url+ sharedPreferences.getString(Constants.PROFILE_PHOTO, "");
        String senderFcmKey = FirebaseInstanceId.getInstance().getToken();
        String senderRole = "teacher";
        String senderClass = "class";

        String receiverID = getIntent().getStringExtra("ReceiverID");
        String receiverName = getIntent().getStringExtra("ReceiverName");
        String receiverImage = url+getIntent().getStringExtra("ReceiverImage");
        String receiverFcmKey = getIntent().getStringExtra("ReceiverFcmKey");
        String receiverRole = getIntent().getStringExtra("ReceiverRole");
        String receiverClass = getIntent().getStringExtra("ReceiverClass");

        String senderMessage = senderID + "to" + receiverID;
        String receiverMessage = receiverID + "to" + senderID;

        MessageList messageList = new MessageList(false,senderName, senderImage, senderID, senderFcmKey,receiverName, receiverImage, receiverID, receiverFcmKey,message, imageUrl, readStatus, sentTime, timeStamp, state);
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("message").child(senderMessage).push().setValue(messageList);
        mDatabase.child("message").child(receiverMessage).push().setValue(messageList);

        //mDatabase.child("recent_message").child(senderMessage).setValue(messageList);
        //mDatabase.child("recent_message").child(receiverMessage).setValue(messageList);

        mDatabase.child("notification").child(receiverID).push().setValue(messageList);

        /*if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
            MessageList messageList1 = new MessageList(senderName, senderImage, senderID, senderFcmKey,senderRole,senderClass);
            mDatabase.child("users").child(senderID).setValue(messageList1);
        } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
            MessageList messageList1 = new MessageList(senderName, senderImage, senderID, senderFcmKey,"Parent","");
            mDatabase.child("users").child(senderID).setValue(messageList1);
        }

            MessageList messageList2 = new MessageList(receiverName, receiverImage, receiverID,receiverFcmKey,receiverRole,receiverClass);
            mDatabase.child("users").child(receiverID).setValue(messageList2);*/
        /*} else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
            MessageList messageList1 = new MessageList(senderName, senderImage, sharedPreferences.getString(Constants.USERID, null),senderFcmKey);
            mDatabase.child("users").child(sharedPreferences.getString(Constants.USERID, null)).setValue(messageList1);
            MessageList messageList2 = new MessageList(receiverName, receiverImage, chatWithUserID,receiverFcmKey);
            mDatabase.child("users").child(chatWithUserID).setValue(messageList2);
        }*/

        //MessageList messageList1 = new MessageList(receiverName, receiverImage, message, readStatus, sentTime);
        //mDatabase.child("recent_message").child(senderID).setValue(messageList1);
        //mDatabase.child("recent_message").child(receiverID).setValue(messageList1);

        sendNotification(receiverFcmKey,message,senderName,senderImage);
        mDialog.dismiss();
        etMessage.setText("");
        tvReceiverName.setText("");
        imageUri = null;

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    @Override
    public void onBackPressed() {

        if (tvReceiverName.getVisibility() == View.VISIBLE) {
            tvReceiverName.setVisibility(View.GONE);
            tvReceiverName.setText("");
            tv_name.setVisibility(View.VISIBLE);
            getMessageList();
        } else {
            finish();
            super.onBackPressed();
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);

    }

    private void getMessageList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            String chatWith = getIntent().getStringExtra("ReceiverID");
            tv_name.setText(getIntent().getStringExtra("ReceiverName"));
            String chatID = sharedPreferences.getString(Constants.USERID, null) + "to" + chatWith;
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("message").child(chatID);
            Query query = ref.orderByChild("timeStamp");
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        //mDialog.dismiss();
                        messageListArrayList.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            //boolean isThisGroup = (boolean) dataSnapshot.child(key).child("isThisGroup").getValue();
                            String senderName = (String) dataSnapshot.child(key).child("senderName").getValue();
                            String senderImage = (String) dataSnapshot.child(key).child("senderImage").getValue();
                            String senderUserID = (String) dataSnapshot.child(key).child("senderUserID").getValue();
                            String senderFcmKey = (String) dataSnapshot.child(key).child("senderFcmKey").getValue();
                            String receiverName = (String) dataSnapshot.child(key).child("receiverName").getValue();
                            String receiverImage = (String) dataSnapshot.child(key).child("receiverImage").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            String receiverFcmKey = (String) dataSnapshot.child(key).child("receiverFcmKey").getValue();
                            String message = (String) dataSnapshot.child(key).child("message").getValue();
                            List<String> imageUrl = (List<String>) dataSnapshot.child(key).child("imageUrl").getValue();
                            Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                            String sentTime = (String) dataSnapshot.child(key).child("sentTime").getValue();
                            Long timeStamp = (Long) dataSnapshot.child(key).child("timeStamp").getValue();
                            Long state = (Long) dataSnapshot.child(key).child("state").getValue();
                            System.out.println("readStatus ==> "+readStatus+" "+state+" "+key);
                            messageListArrayList.add(new MessageList(false, senderName, senderImage, senderUserID, senderFcmKey, receiverName, receiverImage, receiverUserID, receiverFcmKey, message, imageUrl, Integer.parseInt(String.valueOf(readStatus)), sentTime, timeStamp, Integer.parseInt(String.valueOf(state))));
                        }
                        if (messageListArrayList.size() > 0) {
                            rvChatList.setVisibility(View.VISIBLE);
                            tvNoMessage.setVisibility(View.GONE);
                            messageListAdapter.notifyDataSetChanged();
                            rvChatList.scrollToPosition(messageListArrayList.size() - 1);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        } else {
                            rvChatList.setVisibility(View.GONE);
                            tvNoMessage.setVisibility(View.VISIBLE);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        }
                    } else {
                        rvChatList.setVisibility(View.GONE);
                        tvNoMessage.setVisibility(View.VISIBLE);
                        if (mDialog != null && mDialog.isShowing()) {
                            mDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        }

    }

    private void getMessageList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            String chatWith = getIntent().getStringExtra("ReceiverID");
            //tvReceiverName.setText(getIntent().getStringExtra("ReceiverName"));
            String chatID = sharedPreferences.getString(Constants.USERID, null) + "to" + chatWith;
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("message").child(chatID);
            Query query = ref.orderByChild("timeStamp");
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        //mDialog.dismiss();
                        messageListArrayList.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            //boolean isThisGroup = (boolean) dataSnapshot.child(key).child("isThisGroup").getValue();
                            String senderName = (String) dataSnapshot.child(key).child("senderName").getValue();
                            String senderImage = (String) dataSnapshot.child(key).child("senderImage").getValue();
                            String senderUserID = (String) dataSnapshot.child(key).child("senderUserID").getValue();
                            String senderFcmKey = (String) dataSnapshot.child(key).child("senderFcmKey").getValue();
                            String receiverName = (String) dataSnapshot.child(key).child("receiverName").getValue();
                            String receiverImage = (String) dataSnapshot.child(key).child("receiverImage").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            String receiverFcmKey = (String) dataSnapshot.child(key).child("receiverFcmKey").getValue();
                            String message = (String) dataSnapshot.child(key).child("message").getValue();
                            List<String> imageUrl = (List<String>) dataSnapshot.child(key).child("imageUrl").getValue();
                            Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                            String sentTime = (String) dataSnapshot.child(key).child("sentTime").getValue();
                            Long timeStamp = (Long) dataSnapshot.child(key).child("timeStamp").getValue();
                            Long state = (Long) dataSnapshot.child(key).child("state").getValue();
                            System.out.println("readStatus ==> "+readStatus+" "+state+" "+key);
                            /*if (message.contains(search)) {*/
                                String mess = message.replaceAll(search, "<font color='red'>"+search+"</font>");
                                messageListArrayList.add(new MessageList(false, senderName, senderImage, senderUserID, senderFcmKey, receiverName, receiverImage, receiverUserID, receiverFcmKey, mess, imageUrl, Integer.parseInt(String.valueOf(readStatus)), sentTime, timeStamp, Integer.parseInt(String.valueOf(state))));
                            /*}*/
                        }
                        if (messageListArrayList.size() > 0) {
                            rvChatList.setVisibility(View.VISIBLE);
                            tvNoMessage.setVisibility(View.GONE);
                            messageListAdapter.notifyDataSetChanged();
                            //rvChatList.scrollToPosition(messageListArrayList.size() - 1);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        } else {
                            rvChatList.setVisibility(View.GONE);
                            tvNoMessage.setVisibility(View.VISIBLE);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        }
                    } else {
                        rvChatList.setVisibility(View.GONE);
                        tvNoMessage.setVisibility(View.VISIBLE);
                        if (mDialog != null && mDialog.isShowing()) {
                            mDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        }

    }

    protected String getSaltString() {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    private void setMessageReadStatus() {

        final String chatWith = getIntent().getStringExtra("ReceiverID");

        final String chatID = sharedPreferences.getString(Constants.USERID, null) + "to" + chatWith;

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("message").child(chatID);
        Query query = ref.orderByChild("timeStamp");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String key = (String) child.getKey();
                        String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                        if (receiverUserID.equals(sharedPreferences.getString(Constants.USERID, null))) {
                            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                            mDatabase.child("message").child(chatID).child(key).child("readStatus").setValue(1);
                            //mDatabase.child("recent_message").child(chatID).child("readStatus").setValue(1);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final String chatWithID = chatWith + "to" + sharedPreferences.getString(Constants.USERID, null);
        final DatabaseReference ref1 = database.getReference("message").child(chatWithID);
        Query query1 = ref1.orderByChild("timeStamp");
        query1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String key = (String) child.getKey();
                        String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                        if (receiverUserID.equals(sharedPreferences.getString(Constants.USERID, null))) {
                            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                            mDatabase.child("message").child(chatWithID).child(key).child("readStatus").setValue(1);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final DatabaseReference ref2 = database.getReference("notification").child(sharedPreferences.getString(Constants.USERID, null));
        Query query2 = ref2.orderByChild("timeStamp");
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String key = (String) child.getKey();
                        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                        mDatabase.child("notification").child(sharedPreferences.getString(Constants.USERID, null)).child(key).child("readStatus").setValue(1);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(MessageActivity.this)
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .allowOnlineImages(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(MessageActivity.this)
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .allowOnlineImages(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data != null) {
            final List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            if (mPaths.size() > 4) {
                Toast.makeText(getApplicationContext(), "Maximum 4 images allowed", Toast.LENGTH_SHORT).show();
                List<String> path = new ArrayList<>();
                for (int i = 0; i < 4; i++) {
                    path.add(mPaths.get(i));
                }
                showPreviewDialog(path);
            } else {
                showPreviewDialog(mPaths);
            }
            /*final File imgFile = new File(mPaths.get(0));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageUri = Uri.fromFile(imgFile);
            }*/
        }

    }

    private void showPreviewDialog(final List<String> mPaths) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.preview_image, null);
        final RecyclerView gvPreview = alertLayout.findViewById(R.id.gv_preview);
        final EditText etMessage = alertLayout.findViewById(R.id.et_message);
        final ImageView ivSend = alertLayout.findViewById(R.id.iv_send);
        gvPreview.setLayoutManager(new GridLayoutManager(this,2));
        gvPreview.setAdapter(new PreviewImageAdapter(this, mPaths,1));

        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        ivSend.setEnabled(true);
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);
                if (mPaths.size() > 0) {
                    /*if (mPaths.size() > 4) {

                        uploadImage(etMessage.getText().toString().trim(),path);
                    } else {*/
                        uploadImage(etMessage.getText().toString().trim(),mPaths);
                    /*}*/

                } else if (etMessage.getText().toString().trim().length() > 0) {
                    sendMessage(etMessage.getText().toString().trim(),null);
                }
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private void sendNotification(String regToken, String message, String senderName, String img) {
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json=new JSONObject();
                    JSONObject dataJson=new JSONObject();
                    dataJson.put("body",message);
                    dataJson.put("title",senderName+" sent you a message");
                    dataJson.put("icon",img);
                    json.put("data",dataJson);
                    json.put("to",regToken);
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization","key="+Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                    System.out.println("finalResponse ==> "+finalResponse);
                }catch (Exception e){
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

}
