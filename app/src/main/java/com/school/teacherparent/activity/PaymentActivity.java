package com.school.teacherparent.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.FeesConditionAdapter;
import com.school.teacherparent.adapter.FeesListAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.FeesCondition;
import com.school.teacherparent.models.FeesListParms;
import com.school.teacherparent.models.FeesListResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.models.PayFees;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keerthana on 8/24/2018.
 */

public class PaymentActivity extends AppCompatActivity implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder>, PaymentResultListener {


    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;

    DiscreteScrollView dsvStudentList;
    ProgressDialog mDialog;
    TextView tvFeesdue, tvDueAmount;
    //ListView lvCondition;
    Button btnPaynow;
    ArrayList<FeesCondition> feesConditionArrayList = new ArrayList<>();
    ArrayList<FeesCondition.feesDetails> feesConditionArrayListtt = new ArrayList<>();
    RecyclerView rvFeeslist;
    FeesListAdapter feesListAdapter;
    FeesConditionAdapter feesConditionAdapter;
    FeesListResponse feesListParms;
    ArrayList<FeesListParms> feesListParmsArrayList = new ArrayList<>();

    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList;
    int classID, studentID, sectionID,selectedSchoolID;
    int dueAmount = 0;
    int totalAmount = 0;
    int discountAmount = 0;

    String ParentNumber, schoolid, versioncode;
    //WS_CallService service_Login;
    String amount, idfees, name, studentid;
    String in;
    ImageView back;
    String SelectedFeesID,SelectedFeesAmount,SelectedFeesDiscount;
    String feesID = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        intiVIews();
        performAction();
    }

    private void intiVIews() {
        back = findViewById(R.id.back);
        dsvStudentList = findViewById(R.id.dsv_student_list);
        tvFeesdue = findViewById(R.id.tv_feesdue);
        tvDueAmount = findViewById(R.id.tv_due_amount);
        btnPaynow = findViewById(R.id.btn_paynow);
        //lvCondition = findViewById(R.id.lv_condition);
        //lvCondition.setVisibility(View.VISIBLE);
        rvFeeslist = findViewById(R.id.rv_feeslist);
        rvFeeslist.setHasFixedSize(true);
        rvFeeslist.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        feesListAdapter = new FeesListAdapter(this, feesListParmsArrayList, PaymentActivity.this);
        rvFeeslist.setAdapter(feesListAdapter);
        //feesConditionAdapter = new FeesConditionAdapter(this, feesConditionArrayList);
        //lvCondition.setAdapter(feesConditionAdapter);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();

        mDialog = new ProgressDialog(this);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        getStudentList();
        //getConditions();
    }

    private void performAction() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //assignmentID = feesID.substring(0, feesID.length() - 1);
                System.out.println("ffffff ==> "+feesID.substring(0, feesID.length() - 1));
                startPayment( totalAmount, discountAmount,
                        feesID.substring(0, feesID.length() - 1),
                        (sharedPreferences.getString(Constants.FNAME,"")+" "+sharedPreferences.getString(Constants.LNAME,"")).replace(" null",""),
                        String.valueOf(dueAmount*100),"","");
            }
        });

        feesListAdapter.OnsetClickListen(new FeesListAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Dialog dialog = new Dialog(PaymentActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_fees_detail);
                int totalFees = 0;
                int discount = 0;

                ImageView iv_close = dialog.findViewById(R.id.iv_close);
                Button btn_download = dialog.findViewById(R.id.btn_download);
                TextView tv_feesName = dialog.findViewById(R.id.tv_feesName);
                TextView tv_total = dialog.findViewById(R.id.tv_total);
                TextView tv_discount = dialog.findViewById(R.id.tv_discount);
                TextView tv_pay = dialog.findViewById(R.id.tv_pay);
                RecyclerView rvCondition = dialog.findViewById(R.id.rv_condition);
                rvCondition.setHasFixedSize(true);
                rvCondition.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));

                dialog.show();

                for (int i = 0; i < feesConditionArrayList.get(position).getFeesDetails().size(); i++) {
                    totalFees += feesConditionArrayList.get(position).getFeesDetails().get(i).getTotal_amount();
                    discount += feesConditionArrayList.get(position).getFeesDetails().get(i).getDiscountAmount();
                    feesConditionArrayListtt.add(new FeesCondition.feesDetails(
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getFeesPaidID(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getClass_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getSection_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getTerm_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getFees_type_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getTotal_amount(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getDiscount_amount(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getFees_status_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getPaid_status(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getStudent_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getPaid_date(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getDiscountAmount(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getFeesTypeName(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getTransaction_unique_id(),
                            feesConditionArrayList.get(position).getFeesDetails().get(i).getPayment_transaction_id()
                    ));
                }

                tv_feesName.setText("Fees Details "+feesConditionArrayList.get(position).getTerm_name());
                tv_total.setText("Total Amount : "+totalFees);
                tv_discount.setText("Disount Amount : "+discount);
                tv_pay.setText("Amount to be Pay : "+(totalFees-discount));

                for (int i=0;i<feesConditionArrayListtt.size();i++) {
                    System.out.println("getFeesTypeName ==> "+feesConditionArrayListtt.get(i).getFeesTypeName());
                }

                if (feesConditionArrayListtt.size() > 0) {
                    feesConditionAdapter = new FeesConditionAdapter(PaymentActivity.this, feesConditionArrayListtt);
                    rvCondition.setAdapter(feesConditionAdapter);
                    feesConditionAdapter.notifyDataSetChanged();
                }

                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feesConditionArrayListtt.clear();
                        dialog.dismiss();
                    }
                });

                btn_download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        feesConditionArrayListtt.clear();
                        dialog.dismiss();
                    }
                });
            }

        });

    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(this, parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size() > 2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            selectedSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            selectedSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        getFeesDetails(studentID,selectedSchoolID);
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                            } else {
                                Toast.makeText(PaymentActivity.this, childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(PaymentActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    /*private void getConditions() {
        feesConditionArrayList.add("Include Transport Charges");
        feesConditionArrayList.add("Include Exam Fees");
        feesConditionArrayList.add("Include Lab Charges");
        feesConditionAdapter.notifyDataSetChanged();
    }*/

    private void getFeesDetails(int selectedStudentID,int selectedSchoolID) {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(selectedSchoolID));
            getStudentListParam.setStudID(String.valueOf(selectedStudentID));

            feesListParmsArrayList.clear();
            feesConditionArrayList.clear();
            feesID = "";

            vidyauraAPI.getStudentFees(getStudentListParam).enqueue(new Callback<FeesListResponse>() {
                @Override
                public void onResponse(Call<FeesListResponse> call, Response<FeesListResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        feesListParms = response.body();
                    if (feesListParms.getStatus() == 200) {
                        feesListParmsArrayList.clear();
                        feesConditionArrayList.clear();
                        dueAmount = 0;
                        totalAmount = 0;
                        discountAmount = 0;
                        feesID = "";
                        if (feesListParms.getFeesList() != null)
                            if (feesListParms.getFeesList().size() > 0)
                                for (int i = 0; i < feesListParms.getFeesList().size(); i++) {
                                    if (feesListParms.getFeesList().get(i).getFeesDetails() != null) {
                                        if (feesListParms.getFeesList().get(i).getFeesDetails().size() > 0) {
                                            feesListParmsArrayList.add(new FeesListParms(
                                                    (sharedPreferences.getString(Constants.FNAME, "") + " " + sharedPreferences.getString(Constants.LNAME, "")).replace(" null", ""),
                                                    feesListParms.getFeesList().get(i).getTerm_name(),
                                                    feesListParms.getFeesList().get(i).getTotal_amount(),
                                                    feesListParms.getFeesList().get(i).getDiscountedAmount(),
                                                    feesListParms.getFeesList().get(i).getDueDate(),
                                                    feesListParms.getFeesList().get(i).getPaidDate(),
                                                    feesListParms.getFeesList().get(i).getFeesDetails().get(0).getPaid_status(),
                                                    feesListParms.getFeesList().get(i).getFeesDetails().get(0).getPayment_transaction_id(),
                                                    feesListParms.getFeesList().get(i).getTerm_id()));

                                            feesConditionArrayList.add(new FeesCondition(feesListParms.getFeesList().get(i).getTotal_amount(),
                                                    feesListParms.getFeesList().get(i).getDiscountedAmount(),
                                                    feesListParms.getFeesList().get(i).getTerm_name(),
                                                    feesListParms.getFeesList().get(i).getTerm_id(),
                                                    feesListParms.getFeesList().get(i).getFeesDetails()));

                                            dueAmount += feesListParms.getFeesList().get(i).getTotal_amount() - feesListParms.getFeesList().get(i).getDiscountedAmount();
                                            totalAmount += feesListParms.getFeesList().get(i).getTotal_amount();
                                            discountAmount += feesListParms.getFeesList().get(i).getDiscountedAmount();
                                            //feesID = String.valueOf(feesListParms.getFeesList().get(i).getTerm_id()).concat(",");
                                            feesID = feesID.concat(String.valueOf((feesListParms.getFeesList().get(i).getTerm_id())).concat(","));
                                            System.out.println("feeeeeeeesssssss ==> "+feesListParms.getFeesList().get(i).getTerm_id());
                                            System.out.println("feeeeeeeesssssss ==> "+feesID);
                                        }
                                    }
                                }

                       /* for (int i = 0; i < feesListParms.getFeesList().size(); i++) {
                            if (feesListParms.getFeesList().get(i).getFeesDetails() != null) {
                                if (feesListParms.getFeesList().get(i).getFeesDetails().size() > 0) {
                                    for (int j = 0; j < feesListParms.getFeesList().get(i).getFeesDetails().size(); j++) {
                                        feesConditionArrayList.add(new FeesCondition(feesListParms.getFeesList().get(i).getFeesDetails().get(j)));
                                        *//*feesConditionArrayList.add(new FeesListResponse.feesList(
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getFeesPaidID(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getClass_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getSection_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getTerm_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getFees_type_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getTotal_amount(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getDiscount_amount(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getFees_status_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getPaid_status(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getStudent_id(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getPaid_date(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getDiscountAmount(),
                                                feesListParms.getFeesList().get(i).getFeesDetails().get(j).getFeesTypeName()
                                        ));*//*
                                    }
                                }
                            }
                        }*/

                        if (feesListParmsArrayList.size() > 0) {
                            if (dueAmount > 0) {
                                btnPaynow.setVisibility(View.VISIBLE);
                                tvDueAmount.setVisibility(View.VISIBLE);
                                tvFeesdue.setText("Fees Due");
                                tvDueAmount.setText("₹ " + dueAmount);
                            } else {
                                tvFeesdue.setText("No Fees Due");
                                btnPaynow.setVisibility(View.GONE);
                                tvDueAmount.setVisibility(View.GONE);
                            }
                            feesListAdapter.notifyDataSetChanged();
                        }
                    } else {
                        feesListParmsArrayList.clear();
                        feesConditionArrayList.clear();
                        tvFeesdue.setText("No Fees Due");
                        dueAmount = 0;
                        totalAmount = 0;
                        discountAmount = 0;
                        btnPaynow.setVisibility(View.GONE);
                        tvDueAmount.setVisibility(View.GONE);
                        feesListAdapter.notifyDataSetChanged();
                    }
                    } else {
                        //getFeesDetails(studentID);
                        Toast.makeText(PaymentActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<FeesListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(PaymentActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID = parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID = parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID = parentsStudListArrayList.get(adapterPosition).getStudID();
            selectedSchoolID = parentsStudListArrayList.get(adapterPosition).getSchool_id();
            dueAmount = 0;
            totalAmount = 0;
            discountAmount = 0;
            getFeesDetails(studentID,selectedSchoolID);
        }
    }

    public void startPayment(int feesAmount,int feesDiscount,String feesType, String name,String amount,String email,String mobile) {

        SelectedFeesID = feesType;
        SelectedFeesAmount = String.valueOf(feesAmount);
        SelectedFeesDiscount = String.valueOf(feesDiscount);

        final Activity activity = PaymentActivity.this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", name);
            options.put("description", "Fees Payment");
            //You can omit the image option to fetch the image from dashboard
            //  options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", amount);

            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", mobile);

            options.put("prefill", preFill);

            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }

    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            callAPIFeePaid(razorpayPaymentID, SelectedFeesID, "success");
            // Toast.makeText(Payment.this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
            Log.e("paymentId", "razorpayPaymentID " + razorpayPaymentID);
        } catch (Exception e) {
            Log.e("payment ", "Exception in onPaymentSuccess", e);
        }
    }


    @Override
    public void onPaymentError(int i, String s) {
        Log.e("error", "payment " + s);
        //callAPIFeePaid("", "",s);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // finish();
    }

    private void callAPIFeePaid(String razorpayPaymentID, String feesid, String status) {
        System.out.println("SelectedFeesID ==> "+feesid);
        SelectedFeesID = "";

        PayFees payFees = new PayFees();
        payFees.setFeesStatusID(feesid);
        payFees.setGatewayTxID(razorpayPaymentID);
        payFees.setTotalAmnt(SelectedFeesAmount);
        payFees.setDiscountAmnt(SelectedFeesDiscount);
        payFees.setClassID(String.valueOf(classID));
        payFees.setSectionID(String.valueOf(sectionID));
        payFees.setStudentID(String.valueOf(studentID));
        payFees.setSchoolID(String.valueOf(selectedSchoolID));
        payFees.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
        payFees.setUserID(sharedPreferences.getString(Constants.USERID, ""));

        Gson gson = new Gson();
        System.out.println("payfees ==> "+gson.toJson(payFees));

        getFeesDetails(studentID,selectedSchoolID);

        SendNotification sendNotification = new SendNotification();
        sendNotification.setUserID(sharedPreferences.getString(Constants.USERID, ""));
        sendNotification.setSchoolID(String.valueOf(selectedSchoolID));
        sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
        sendNotification.setType("fees");
        sendNotification.setType_id(feesid);
        vidyauraAPI.sendNotification(sendNotification).enqueue(new Callback<AddFeedResponse>() {
            @Override
            public void onResponse(Call<AddFeedResponse> call, Response<AddFeedResponse> response) {
                AddFeedResponse addFeedResponse1 = response.body();
                if (addFeedResponse1.getStatus() == Util.STATUS_SUCCESS) {
                    Toast.makeText(PaymentActivity.this, "Payment Successfully", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddFeedResponse> call, Throwable t) {
                Toast.makeText(PaymentActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
            }
        });


        //getFeesDetails(studentID,selectedSchoolID);
       /* try {
            //   progressBar.setVisibility(View.VISIBLE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            // String schoolid = "SC-001-LS";
            // studentid = "1";
            byte[] data;
            String login_str = "UserName:" + schoolid + "|TransactionId:" + razorpayPaymentID + "|FeesId:" + feesid + "|StudentId:" + studentid + "|Function:FeesPaymentSuccess|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:" + Build.VERSION.BASE_OS + "";
            //String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:IsFeesPaid|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.BASE_OS + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_SuccessPayment_WS load_plan_list = new Load_SuccessPayment_WS(Payment.this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /*public class Load_SuccessPayment_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_SuccessPayment_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");

            Log.e("jsonResponse", "Fee list" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equals("Success")) {

                    Intent i = new Intent(PaymentActivity.this, MainActivity.class);
                    i.putExtra("type", "payment");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    Toast.makeText(PaymentActivity.this, jObj.getString("response"), Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }*/

    @Override
    public void onBackPressed() {
        finish();
    }

    /*Intent i = getIntent();
    amount = i.getStringExtra("amount");
    idfees = i.getStringExtra("feesid");
    name = i.getStringExtra("name");
    studentid = i.getStringExtra("studentid");

        if (amount.contains(".")) {
        float fot = Float.parseFloat(amount);
        float ina = fot * 100;
        in = String.valueOf(ina);

        Log.e("float", "" + in);
    } else {
        in = String.valueOf(Integer.parseInt(amount) * 100);
    }
    back = findViewById(R.id.back_pay);
        back.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    });
    ParentNumber = AppPreferences.getParentNumber(Payment.this);
    schoolid = AppPreferences.getSchool_Name(Payment.this);
    versioncode = AppPreferences.getversioncode(Payment.this);
    startPayment();*/

    /**/
}
