package com.school.teacherparent.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.ExamFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.utils.Constants;

import javax.inject.Inject;


public class ExamsActivity extends BaseActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static ImageView back,noti;
    public static TextView title;
    NotificationReceiver notificationReceiver;
    @Inject
    public SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exams);
        back=(ImageView)findViewById(R.id.back);
        noti=(ImageView)findViewById(R.id.noti);
        title=(TextView)findViewById(R.id.title);

        VidyauraApplication.getContext().getComponent().inject(this);
        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        /*Fragment fragment=new SchoolFragment();
        replaceFragment(fragment);
*/
        Fragment fragment=new ExamFragment();
        replaceFragment(fragment);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_exam, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
    @Override
    public void onBackPressed() {
        Log.e("back_arrow pressed", "back_arrow pressed");
        FragmentManager manager = getSupportFragmentManager();
        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());
        if (manager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}

    /*  //  toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  setSupportActionBar(toolbar);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3498db"));


        //tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));

    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new UpcomingExamsFragment(), "Upcoming Exams");
        adapter.addFragment(new PostExamsFragment(), "History");
        adapter.addFragment(new Setting(),"Settings");
        adapter.addFragment(new GalleryFragment(),"Gallery");
        adapter.addFragment(new ProfileFragment(),"profile");
        adapter.addFragment(new TimeTableFragment(),"Timetable");

        viewPager.setAdapter(adapter);


    }
    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
*/