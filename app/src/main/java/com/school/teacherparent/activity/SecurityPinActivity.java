package com.school.teacherparent.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.LoginUserTypeFragment;
import com.school.teacherparent.fragment.SecurityPinFragment;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/24/2018.
 */

public class SecurityPinActivity extends BaseActivity {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    UserLoginResponse userLoginResponse;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_securitypin);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getApplicationContext().getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        if (sharedPreferences.getInt(Constants.USERTYPE,0)==3)
        {
            Fragment fragment = new LoginUserTypeFragment();
            replaceFragment(fragment);

        }
        else
        {
            Fragment fragment = new SecurityPinFragment();
            replaceFragment(fragment);
        }


    }


    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(SecurityPinActivity.this, SplashScreenActivity.class);
        // }
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        startActivity(i);
        finish();
    }


}
