package com.school.teacherparent.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.LeaveAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ClassListResponse;
import com.school.teacherparent.models.GetclassListParams;
import com.school.teacherparent.models.LeaveListModel;
import com.school.teacherparent.models.LeaveParms;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/12/2018.
 */

public class ApplyLeaveActivity extends BaseActivity {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    int offset = 0;
    ImageView back,noti;
    SwipeRefreshLayout leavesummaryswipelayout;
    private RecyclerView recyclerview;
    private LeaveAdapter mAdapter;
    private List<LeaveListModel.LeaveDetailsList> leaveList = new ArrayList<>();
    private List<LeaveListModel.LeaveDetailsList> searchLeaveList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    boolean loading=false;
    ImageView calendarImageview;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate;
    TextView selectedDateTextview;
    private Spinner class_spinner;
    ArrayAdapter<String> classspinnerArray;
    ArrayList<String> classlist = new ArrayList<String>();
    ClassListResponse classListResponse;
    int classId = 0,sectionId=0;
    String sessionTYPE="1";
    RadioGroup radioGroup;
    RadioButton forenoonradiobutton,afternoonradiobutton;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    FloatingActionButton searchFloatingButton;
    EditText edittextleaveSearch;
    LeaveListModel data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        recyclerview = (RecyclerView) findViewById(R.id.recycle);
        calendarImageview=(ImageView)findViewById(R.id.calendarImageview);
        selectedDateTextview=(TextView)findViewById(R.id.selectedDateTextview);
        leaveList = new ArrayList<>();
        forenoonradiobutton=(RadioButton)findViewById(R.id.forenoonradiobutton);
        afternoonradiobutton=(RadioButton)findViewById(R.id.afternoonradiobutton);
        radioGroup=(RadioGroup)findViewById(R.id.toggle);
        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        noti=findViewById(R.id.noti);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ApplyLeaveActivity.this,NotificationActivity.class));

            }
        });
        searchFloatingButton=(FloatingActionButton)findViewById(R.id.searchFloatingButton);
        class_spinner = (Spinner) findViewById(R.id.class_spinner);
        edittextleaveSearch=(EditText)findViewById(R.id.edittextleaveSearch);
        classspinnerArray = new ArrayAdapter<String>(
                this, R.layout.spinner_item, classlist);

        classspinnerArray.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(classspinnerArray);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        selectedDateTextview.setText(getString(R.string.today));

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        edittextleaveSearch.setInputType(0);
        edittextleaveSearch.setEnabled(false);

        searchFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (leaveList.size()>0) {
                    edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
                    edittextleaveSearch.setEnabled(true);
                    edittextleaveSearch.requestFocus();
                    showKeyboard(edittextleaveSearch);
                    edittextleaveSearch.setText("");
                }
                else
                {
                    Toast.makeText(ApplyLeaveActivity.this, R.string.noData, Toast.LENGTH_SHORT).show();
                }

            }
        });

        edittextleaveSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (edittextleaveSearch.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edittextleaveSearch.getWindowToken(),
                                InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        if (leaveList.size()>0) {
                            searchLeaveList(edittextleaveSearch.getText().toString().toLowerCase());
                        }
                        else
                        {
                            Toast.makeText(ApplyLeaveActivity.this, R.string.noData, Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                    else
                    {
                        Toast.makeText(ApplyLeaveActivity.this, R.string.entersomething, Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
        calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ApplyLeaveActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                if (classId>0&&sectionId>0) {
                                    leaveList.clear();
                                    mAdapter.notifyDataSetChanged();
                                    getLeaveList();
                                }


                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();










            }
        });
        leavesummaryswipelayout = findViewById(R.id.swipe_leavesummary);
        leavesummaryswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                leavesummaryswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (classId>0&&sectionId>0) {
                            leaveList.clear();
                            mAdapter.notifyDataSetChanged();
                            getLeaveList();
                        }
                        leavesummaryswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });
        //mAdapter = new LeaveAdapter(leaveList, ApplyLeaveActivity.this);

        linearLayoutManager = new LinearLayoutManager(this);

        recyclerview.setLayoutManager(linearLayoutManager);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);

        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset = offset + Constants.DATALIMIT;

                            if (classId>0&&sectionId>0) {
                                getLeaveList();
                            }
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position == 0) {
                    classId=0;
                    sectionId=0;
                } else {
                    classId = classListResponse.getTeachersClassesList().get(position - 1).getClassID();
                    sectionId = classListResponse.getTeachersClassesList().get(position - 1).getSectionID();
                    leaveList.clear();
                    mAdapter.notifyDataSetChanged();
                    getLeaveList();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        getClassList();

        forenoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    forenoonradiobutton.setChecked(true);
                    afternoonradiobutton.setChecked(false);
                    sessionTYPE="1";
                    if (classId>0&&sectionId>0) {
                        leaveList.clear();
                        mAdapter.notifyDataSetChanged();
                        getLeaveList();
                    }
                }
            }
        });
        afternoonradiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    forenoonradiobutton.setChecked(false);
                    afternoonradiobutton.setChecked(true);
                    sessionTYPE="2";
                    if (classId>0&&sectionId>0) {
                        leaveList.clear();
                        mAdapter.notifyDataSetChanged();
                        getLeaveList();
                    }
                }
            }
        });

    }

    public void searchLeaveList(String searchvalue)
    {
        if (data!=null && data.getLeaveDetailsList().size()>0)
        {
            searchLeaveList.clear();
            for(int y=0;y<data.getLeaveDetailsList().size();y++)
            {
                if (data.getLeaveDetailsList().get(y).getFname().toLowerCase().contains(searchvalue.toString().toLowerCase()))
                {

                    searchLeaveList.add(data.getLeaveDetailsList().get(y));

                }
            }

            if (searchLeaveList.size()>0)
            {
                //mAdapter = new LeaveAdapter(searchLeaveList, ApplyLeaveActivity.this);
                recyclerview.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();

            }


        }

        else
        {
            Toast.makeText(ApplyLeaveActivity.this, R.string.noData, Toast.LENGTH_SHORT).show();
        }


    }


    public void getLeaveList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            LeaveParms leavelistParms = new LeaveParms();
            leavelistParms.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            leavelistParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            leavelistParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            leavelistParms.setClassID(String.valueOf(classId));
            leavelistParms.setSectionID(String.valueOf(sectionId));
            leavelistParms.setLimit(Constants.DATALIMIT);
            leavelistParms.setOffset(offset);
            leavelistParms.setAttendanceDate(formattedDate);
            leavelistParms.setSessionType(sessionTYPE);
            Gson gson = new Gson();
            String input = gson.toJson(leavelistParms);
            /*vidyauraAPI.getLeaveList(leavelistParms).enqueue(new Callback<LeaveListModel>() {
                @Override
                public void onResponse(Call<LeaveListModel> call, Response<LeaveListModel> response) {
                    hideProgress();

                    if (response.body() != null) {
                        data = response.body();
                        if (data.getStatus() != Util.STATUS_TOKENEXPIRE) {

                            Gson gson = new Gson();
                            String res = gson.toJson(response.body());
                            Log.d("resdata", "onResponse: " + res);


                            if (data.getStatus() == Util.STATUS_SUCCESS) {

                                if (response.body().getLeaveDetailsList() != null &&
                                        response.body().getLeaveDetailsList().size() != 0) {

                                    recyclerview.setVisibility(View.VISIBLE);
                                    leaveList.addAll(data.getLeaveDetailsList());
                                    mAdapter.notifyDataSetChanged();
                                    loading = true;
                                } else {
                                     Toast.makeText(ApplyLeaveActivity.this, R.string.noData, Toast.LENGTH_SHORT).show();
                                    loading = false;
                                }
                            } else {
                                Toast.makeText(ApplyLeaveActivity.this, data.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
//                            Intent i = new Intent(LeaveListActivity.this, SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            finishAffinity();
                            leaveList.clear();
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, data.getToken()).commit();
                            getLeaveList();

                        }



                    } else {
                        Toast.makeText(ApplyLeaveActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<LeaveListModel> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ApplyLeaveActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();


                }
            });*/

        } else {
            hideProgress();
            Toast.makeText(ApplyLeaveActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void getClassList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            classlist.clear();
            classlist.add("Select Class");
            GetclassListParams getclassListParams = new GetclassListParams();
            getclassListParams.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getclassListParams.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getclassListParams.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getClassList(getclassListParams).enqueue(new Callback<ClassListResponse>() {
                @Override
                public void onResponse(Call<ClassListResponse> call, Response<ClassListResponse> response) {
                    hideProgress();
                    if (response.body() != null) {
                        classListResponse = response.body();
                        if (classListResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (classListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                for (int i = 0; i < classListResponse.getTeachersClassesList().size(); i++) {
//                                    classList.add(new ListofClass(classListResponse.getTeachersClassesList().get(i).getClassID(),
//                                        classListResponse.getTeachersClassesList().get(i).getClassName(),classListResponse.getTeachersClassesList().get(i).getSection(),
//                                        classListResponse.getTeachersClassesList().get(i).getSectionID()));
                                    classlist.add(classListResponse.getTeachersClassesList().get(i).getClassName()+"-"+
                                            classListResponse.getTeachersClassesList().get(i).getSection());

                                }
//                                classId=examDetailByIdResponse.getExamScheduleList().get(0).getClassroom_id();
//                                sectionId=examDetailByIdResponse.getExamScheduleList().get(0).getClasssection_id();
//                                getSubjecyList();
                                classspinnerArray.notifyDataSetChanged();


                            } else {
                                Toast.makeText(ApplyLeaveActivity.this, classListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
//                            Intent i = new Intent(LeaveListActivity.this, SecurityPinActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            finishAffinity();
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, classListResponse.getToken()).commit();
                            getClassList();


                        }


                    } else {
                        Toast.makeText(ApplyLeaveActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ClassListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(ApplyLeaveActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Toast.makeText(ApplyLeaveActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

    private void hideSoftKeyboard(EditText ettext){
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(ettext.getWindowToken(), 0);
    }
}
