package com.school.teacherparent.activity;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.school.teacherparent.fragment.LoginNumberFragment;
import com.school.teacherparent.fragment.LoginUserTypeFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.utils.CustomTextView;
import com.school.teacherparent.utils.UiUtils;

/**
 * Created by harini on 10/23/2018.
 */

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_type);
        Fragment fragment = new LoginNumberFragment();
        replaceFragment(fragment);
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.login_frame, fragment, "");
            fragmentTransaction.commit();
        }
    }



}
