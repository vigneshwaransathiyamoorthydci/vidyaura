package com.school.teacherparent.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.school.teacherparent.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Testac extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new SampleView(this));
    }

    private static class SampleView extends View {

        // CONSTRUCTOR
        public SampleView(Context context) {
            super(context);
            setFocusable(true);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            Paint paint = new Paint();

            canvas.drawColor(Color.YELLOW);


            // you need to insert a image flower_blue into res/drawable folder
             paint.setFilterBitmap(true);
            String url = "http://tracker.flightview.com/fvXMLDemoMapEstPos/flightviewCGI.exe?qtype=GIF&acid=NH9&arrap=NRT&depap=JFK";
            new AsyncTaskLoadImage(canvas).execute(url);



        }

    }
    public static class AsyncTaskLoadImage  extends AsyncTask<String, String, Bitmap> {
        private final static String TAG = "AsyncTaskLoadImage";
        Canvas canvas;
        public AsyncTaskLoadImage(Canvas canvas) {
            this.canvas=canvas;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                URL url = new URL("http://tracker.flightview.com/fvXMLDemoMapEstPos/flightviewCGI.exe?qtype=GIF&acid=NH9&arrap=NRT&depap=JFK");
                bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch(IOException e) {
                System.out.println(e);
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //imageView.setImageBitmap(bitmap);
            Log.e(TAG,"test");
            Paint paint = new Paint();

            canvas.drawColor(Color.YELLOW);


            // you need to insert a image flower_blue into res/drawable folder
            paint.setFilterBitmap(true);
            Bitmap croppedBmp = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth() / 2, bitmap.getHeight());
            int h = bitmap.getHeight();
            canvas.drawBitmap(bitmap, 10, 10, paint);
            canvas.drawBitmap(croppedBmp, 10, 10 + h + 10, paint);
        }
    }
}