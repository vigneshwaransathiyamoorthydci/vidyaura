package com.school.teacherparent.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.AttendanceFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.fragment.AttendanceforParentFragment;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;

import javax.inject.Inject;


public class AttendanceActivity extends BaseActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    int usetType;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    public VidyAPI vidyauraAPI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        usetType=sharedPreferences.getInt(Constants.USERTYPE,0);
        if (usetType==1) {
            Fragment fragment = new AttendanceFragment();
            replaceFragment(fragment);
        }
        else if (usetType==2)
        {
            Fragment fragment = new AttendanceforParentFragment();
            replaceFragment(fragment);
        }


    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_attendance, fragment, fragmentTag);
            //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    @Override
    public void onBackPressed() {
        Log.e("back_arrow pressed", "back_arrow pressed");
        FragmentManager manager = getSupportFragmentManager();
        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());
        if (manager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
