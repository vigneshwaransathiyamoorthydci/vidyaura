package com.school.teacherparent.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonArray;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.MessageListAdapter;
import com.school.teacherparent.adapter.PreviewImageAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GroupMessageActivity extends AppCompatActivity {

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    ImageView ivBack,ivAttachment, ivSend, ivSearch;
    TextView tvGroupMembers,tvNoMessage,tv_name;
    RecyclerView rvGroupChatList;
    EditText tvGroupName,etMessage;

    Uri imageUri;
    FirebaseStorage storage;
    StorageReference storageReference, retrieve;
    ProgressDialog mDialog;
    ArrayList<MessageList> messageListArrayList = new ArrayList<>();
    MessageListAdapter messageListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_message);
        initViews();
        performAction();
    }

    private void initViews() {
        ivBack = findViewById(R.id.iv_back);
        tvGroupName = findViewById(R.id.tv_groupname);
        tvGroupMembers = findViewById(R.id.tv_group_members);
        rvGroupChatList = findViewById(R.id.rv_groupChatList);
        tvNoMessage = findViewById(R.id.tv_momessage);
        ivAttachment = findViewById(R.id.iv_attachment);
        etMessage = findViewById(R.id.et_message);
        ivSend = findViewById(R.id.iv_send);
        ivSearch = findViewById(R.id.search);
        tv_name = findViewById(R.id.tv_name);

        etMessage.requestFocus();

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        mDialog = new ProgressDialog(this);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        retrieve = storage.getReference();

        rvGroupChatList.setHasFixedSize(true);
        rvGroupChatList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        messageListAdapter = new MessageListAdapter(GroupMessageActivity.this, messageListArrayList, sharedPreferences.getString(Constants.USERID, null));
        rvGroupChatList.setAdapter(messageListAdapter);
        getMessageList();
        setMessageReadStatus();

        tv_name.setText(getIntent().getStringExtra("GroupName"));
        tvGroupMembers.setText(getIntent().getStringExtra("ArrayUserList"));

    }

    private void performAction() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //tvGroupName.setEnabled(true);
                tvGroupName.setVisibility(View.VISIBLE);
                tv_name.setVisibility(View.GONE);
                showKeyboard(tvGroupName);
                /*tvGroupName.setCursorVisible(true);
                tvGroupName.requestFocus();*/
                /*InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(tvGroupName,0);*/
            }
        });

        tvGroupName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // hide virtual keyboard
                    if (tvGroupName.getText().toString().toLowerCase().length()>0) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(tvGroupName.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        getMessageList(tvGroupName.getText().toString().trim().toLowerCase());
                    }

                    return true;
                }
                else
                {
                    Toast.makeText(GroupMessageActivity.this, R.string.entersomething, Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        /*tvGroupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String search = s.toString();
                if (search.length() > 0) {
                    getMessageList(search.toLowerCase());
                }
            }
        });*/

        ivAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMessage.getText().toString().trim().length()>0) {
                    sendMessage(etMessage.getText().toString().trim(), null);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);
                }
            }
        });
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    @Override
    public void onBackPressed() {

        if (tvGroupName.getVisibility() == View.VISIBLE) {
            tvGroupName.setVisibility(View.GONE);
            tvGroupName.setText("");
            tv_name.setVisibility(View.VISIBLE);
            getMessageList();
        } else {
            finish();
            super.onBackPressed();
        }

        /*if (tvGroupName.getText().toString().trim().equals(getIntent().getStringExtra("GroupName"))) {
            finish();
            super.onBackPressed();
        } else {
            tvGroupName.setText(getIntent().getStringExtra("GroupName"));
            tvGroupName.setCursorVisible(false);
            getMessageList();
        }*/
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);

    }

    private void getMessageList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            String groupID = getIntent().getStringExtra("GroupID");

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("message").child(groupID);
            Query query = ref.orderByChild("timeStamp");
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        messageListArrayList.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            //boolean isThisGroup = (boolean) dataSnapshot.child(key).child("isThisGroup").getValue();
                            String senderName = (String) dataSnapshot.child(key).child("senderName").getValue();
                            String senderImage = (String) dataSnapshot.child(key).child("senderImage").getValue();
                            String senderUserID = (String) dataSnapshot.child(key).child("senderUserID").getValue();
                            String senderFcmKey = (String) dataSnapshot.child(key).child("senderFcmKey").getValue();
                            String receiverName = (String) dataSnapshot.child(key).child("receiverName").getValue();
                            String receiverImage = (String) dataSnapshot.child(key).child("receiverImage").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            String receiverFcmKey = (String) dataSnapshot.child(key).child("receiverFcmKey").getValue();
                            String message = (String) dataSnapshot.child(key).child("message").getValue();
                            List<String> imageUrl = (List<String>) dataSnapshot.child(key).child("imageUrl").getValue();
                            Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                            String sentTime = (String) dataSnapshot.child(key).child("sentTime").getValue();
                            Long timeStamp = (Long) dataSnapshot.child(key).child("timeStamp").getValue();
                            Long state = (Long) dataSnapshot.child(key).child("state").getValue();
                            System.out.println("readStatus ==> "+sentTime);
                            messageListArrayList.add(new MessageList(true, senderName, senderImage, senderUserID, senderFcmKey,receiverName, receiverImage, receiverUserID, receiverFcmKey, message, imageUrl, Integer.parseInt(String.valueOf(readStatus)), sentTime, timeStamp, Integer.parseInt(String.valueOf(state))));
                        }
                        if (messageListArrayList.size() > 0) {
                            rvGroupChatList.setVisibility(View.VISIBLE);
                            tvNoMessage.setVisibility(View.GONE);
                            messageListAdapter.notifyDataSetChanged();
                            rvGroupChatList.scrollToPosition(messageListArrayList.size() - 1);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        } else {
                            rvGroupChatList.setVisibility(View.GONE);
                            tvNoMessage.setVisibility(View.VISIBLE);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        }
                    } else {
                        rvGroupChatList.setVisibility(View.GONE);
                        tvNoMessage.setVisibility(View.VISIBLE);
                        if (mDialog != null && mDialog.isShowing()) {
                            mDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        }

    }

    private void getMessageList(String search) {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            String groupID = getIntent().getStringExtra("GroupID");

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("message").child(groupID);
            Query query = ref.orderByChild("timeStamp");
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        messageListArrayList.clear();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String key = (String) child.getKey();
                            //boolean isThisGroup = (boolean) dataSnapshot.child(key).child("isThisGroup").getValue();
                            String senderName = (String) dataSnapshot.child(key).child("senderName").getValue();
                            String senderImage = (String) dataSnapshot.child(key).child("senderImage").getValue();
                            String senderUserID = (String) dataSnapshot.child(key).child("senderUserID").getValue();
                            String senderFcmKey = (String) dataSnapshot.child(key).child("senderFcmKey").getValue();
                            String receiverName = (String) dataSnapshot.child(key).child("receiverName").getValue();
                            String receiverImage = (String) dataSnapshot.child(key).child("receiverImage").getValue();
                            String receiverUserID = (String) dataSnapshot.child(key).child("receiverUserID").getValue();
                            String receiverFcmKey = (String) dataSnapshot.child(key).child("receiverFcmKey").getValue();
                            String message = (String) dataSnapshot.child(key).child("message").getValue();
                            List<String> imageUrl = (List<String>) dataSnapshot.child(key).child("imageUrl").getValue();
                            Long readStatus = (Long) dataSnapshot.child(key).child("readStatus").getValue();
                            String sentTime = (String) dataSnapshot.child(key).child("sentTime").getValue();
                            Long timeStamp = (Long) dataSnapshot.child(key).child("timeStamp").getValue();
                            Long state = (Long) dataSnapshot.child(key).child("state").getValue();
                            System.out.println("readStatus ==> "+sentTime);
                            /*if (message.contains(search)) {*/
                                String mess = message.replaceAll(search, "<font color='red'>"+search+"</font>");
                                messageListArrayList.add(new MessageList(true, senderName, senderImage, senderUserID, senderFcmKey, receiverName, receiverImage, receiverUserID, receiverFcmKey, mess, imageUrl, Integer.parseInt(String.valueOf(readStatus)), sentTime, timeStamp, Integer.parseInt(String.valueOf(state))));
                            /*}*/
                        }
                        if (messageListArrayList.size() > 0) {
                            rvGroupChatList.setVisibility(View.VISIBLE);
                            tvNoMessage.setVisibility(View.GONE);
                            messageListAdapter.notifyDataSetChanged();
                            //rvGroupChatList.scrollToPosition(messageListArrayList.size() - 1);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        } else {
                            rvGroupChatList.setVisibility(View.GONE);
                            tvNoMessage.setVisibility(View.VISIBLE);
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                        }
                    } else {
                        rvGroupChatList.setVisibility(View.GONE);
                        tvNoMessage.setVisibility(View.VISIBLE);
                        if (mDialog != null && mDialog.isShowing()) {
                            mDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    mDialog.dismiss();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

        }

    }

    private void setMessageReadStatus() {

        final String groupID = getIntent().getStringExtra("GroupID");
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("message").child(groupID);
        Query query = ref.orderByChild("timeStamp");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String key = (String) child.getKey();
                        String readBy = (String) dataSnapshot.child(key).child("readBy").getValue();
                        if (!readBy.contains(sharedPreferences.getString(Constants.USERID, null))) {
                            String read = readBy+", "+sharedPreferences.getString(Constants.USERID, null);
                            final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                            mDatabase.child("message").child(groupID).child(key).child("readBy").setValue(read);
                            //mDatabase.child("recent_message").child(groupID).child("readBy").setValue(read);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final DatabaseReference ref2 = database.getReference("notification").child(sharedPreferences.getString(Constants.USERID, null));
        Query query2 = ref2.orderByChild("timeStamp");
        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        String key = (String) child.getKey();
                        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                        mDatabase.child("notification").child(sharedPreferences.getString(Constants.USERID, null)).child(key).child("readStatus").setValue(1);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mDialog.dismiss();
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private void uploadImage(final String message, final List<String> mPaths) {
        final List<String> uploadedPaths = new ArrayList<>();
        for (int i=0;i<mPaths.size();i++) {
            final File imgFile = new File(mPaths.get(i));
            Uri mUploadImageUri = Uri.fromFile(imgFile);
            if (mUploadImageUri != null) {
                showProgressDialog();
                final String uid = UUID.randomUUID().toString();
                StorageReference ref = storageReference.child("message_image").child("single").child(uid);
                ref.putFile(mUploadImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        retrieve.child("message_image").child("single").child(uid).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                if (uri.toString() != null) {
                                    String image = uri.toString();
                                    uploadedPaths.add(image);
                                    if (uploadedPaths.size() == mPaths.size()) {
                                        sendMessage(message, uploadedPaths);
                                    }
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                                //progress.dismiss();
                            }
                        });

                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        })
                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                //calculating progress percentage
                                double progressD = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                //progress.setMax(100);
                                //progress.setProgress((int) progressD);
                                //displaying percentage in progress dialog
                                mDialog.setMessage("Uploaded " + ((int) progressD) + "%...");
                            }
                        });

            }
        }
    }

    private void sendMessage(String message, List<String> imageUrl) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

        int readStatus = 0;
        String sentTime = mdformat.format(calendar.getTime());
        Long timeStamp = System.currentTimeMillis();

        String senderID = sharedPreferences.getString(Constants.USERID, null);
        String senderName = sharedPreferences.getString(Constants.FNAME, "") + " " + sharedPreferences.getString(Constants.LNAME, "");
        String url = null;
        if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
            url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
        } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
            url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
        }
        String senderImage = url + sharedPreferences.getString(Constants.PROFILE_PHOTO, "");

        String senderFcmKey = FirebaseInstanceId.getInstance().getToken();
        String senderRole = "teacher";
        String senderClass = "class";

        String groupName = getIntent().getStringExtra("GroupName");
        String groupImage = getIntent().getStringExtra("GroupImage");
        String groupID = getIntent().getStringExtra("GroupID");
        ArrayList<String> receiverID = getIntent().getStringArrayListExtra("ArrayReceiverID");
        String arrayReceiverName = getIntent().getStringExtra("ArrayReceiverName");
        String arrayReceiverImage = getIntent().getStringExtra("ArrayReceiverImage");
        ArrayList<String> arrayReceiverFcmKey = getIntent().getStringArrayListExtra("ArrayReceiverFcmKey");
        String arrayReceiverRole = getIntent().getStringExtra("ArrayReceiverRole");
        String arrayReceiverClass = getIntent().getStringExtra("ArrayReceiverClass");
        arrayReceiverFcmKey.removeAll(Collections.singleton(null));

        MessageList messageList = new MessageList(true, groupID,groupName,groupImage, arrayReceiverFcmKey, senderName, senderImage, senderID,
                getIntent().getStringExtra("ArrayUserList"), arrayReceiverImage, receiverID, message, imageUrl, readStatus, sentTime,
                timeStamp, 0, sharedPreferences.getString(Constants.USERID, null));
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("message").child(groupID).push().setValue(messageList);

        //mDatabase.child("recent_message").child(groupID).setValue(messageList);

        //List<String> receiverIDArrayList = Arrays.asList(arrayReceiverID.split(","));

        ArrayList<String> arrayReceiverID = new ArrayList<>();
        for (int i = 0; i < receiverID.size(); i++) {
            if (!arrayReceiverID.contains(receiverID.get(i))) {
                arrayReceiverID.add(receiverID.get(i));
            }
        }

        for (int i=0; i<arrayReceiverID.size();i++) {
            if (!arrayReceiverID.get(i).equals(sharedPreferences.getString(Constants.USERID,"")))
            mDatabase.child("notification").child(arrayReceiverID.get(i).trim()).push().setValue(messageList);
        }

        //String receiverFcmKey = getIntent().getStringExtra("ArrayReceiverFcmKey");
        //List<String> stringArrayList = Arrays.asList(receiverFcmKey.split(","));
        final JSONArray jsonArray = new JSONArray();
        for (int i=0; i<arrayReceiverFcmKey.size();i++) {
            if (!arrayReceiverFcmKey.get(i).trim().equals(FirebaseInstanceId.getInstance().getToken()))
            jsonArray.put(arrayReceiverFcmKey.get(i).trim());
            System.out.println("fcm key ==> "+arrayReceiverFcmKey.get(i));
        }
        sendNotification(jsonArray,message,senderName,groupImage);
        mDialog.dismiss();
        etMessage.setText("");
        tvGroupName.setText("");
        imageUri = null;

    }

    private void imageChooser() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.image_chooser, null);
        final LinearLayout llCamera = alertLayout.findViewById(R.id.camera_layout);
        final LinearLayout llGallery = alertLayout.findViewById(R.id.gallery_layout);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(GroupMessageActivity.this)
                        .mode(ImagePicker.Mode.CAMERA)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .allowOnlineImages(true)
                        .build();
                dialog.dismiss();
            }
        });
        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ImagePicker.Builder(GroupMessageActivity.this)
                        .mode(ImagePicker.Mode.GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.SOFT)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.PNG)
                        .allowMultipleImages(true)
                        .enableDebuggingMode(true)
                        .allowOnlineImages(true)
                        .build();
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data != null) {
            final List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            if (mPaths.size() > 4) {
                Toast.makeText(getApplicationContext(), "Maximum 4 images allowed", Toast.LENGTH_SHORT).show();
                List<String> path = new ArrayList<>();
                for (int i = 0; i < 4; i++) {
                    path.add(mPaths.get(i));
                }
                showPreviewDialog(path);
            } else {
                showPreviewDialog(mPaths);
            }
            /*final File imgFile = new File(mPaths.get(0));
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageUri = Uri.fromFile(imgFile);
            }*/
        }

    }

    private void showPreviewDialog(final List<String> mPaths) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.preview_image, null);
        final RecyclerView gvPreview = alertLayout.findViewById(R.id.gv_preview);
        final EditText etMessage = alertLayout.findViewById(R.id.et_message);
        final ImageView ivSend = alertLayout.findViewById(R.id.iv_send);
        gvPreview.setLayoutManager(new GridLayoutManager(this,2));
        gvPreview.setAdapter(new PreviewImageAdapter(this, mPaths,1));

        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        ivSend.setEnabled(true);
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etMessage.getWindowToken(), 0);
                if (mPaths.size() > 0) {
                    /*if (mPaths.size() > 4) {

                        uploadImage(etMessage.getText().toString().trim(),path);
                    } else {*/
                    uploadImage(etMessage.getText().toString().trim(),mPaths);
                    /*}*/

                } else if (etMessage.getText().toString().trim().length() > 0) {
                    sendMessage(etMessage.getText().toString().trim(),null);
                }
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private void sendNotification(JSONArray regToken, String message, String senderName, String img) {
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    JSONObject json=new JSONObject();
                    JSONObject dataJson=new JSONObject();
                    dataJson.put("body",message);
                    dataJson.put("title",senderName+" sent you a group message");
                    dataJson.put("icon",img);
                    json.put("data",dataJson);
                    json.put("registration_ids",regToken);
                    RequestBody body = RequestBody.create(JSON, json.toString());
                    Request request = new Request.Builder()
                            .header("Authorization","key="+Constants.LEGACY_SERVER_KEY)
                            .url("https://fcm.googleapis.com/fcm/send")
                            .post(body)
                            .build();
                    Response response = client.newCall(request).execute();
                    String finalResponse = response.body().string();
                    System.out.println("finalResponse ==> "+finalResponse);
                }catch (Exception e){
                    //Log.d(TAG,e+"");
                }
                return null;
            }
        }.execute();

    }

    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }

}
