package com.school.teacherparent.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtilityOptions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.util.IOUtils;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.R;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.SelectedImageandVideoList;
import com.school.teacherparent.utils.UiUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    public static final int ERROR_SHOW_TYPE_LOG = 0;
    public static final int ERROR_SHOW_TYPE_TOAST = 1;
    public static final int ERROR_SHOW_TYPE_DIALOG = 2;
    public static final String DIALOG_API_ERROR_TAG = "apiTag";
    private static final float PICTURE_SIZE = 640;
    public TransferUtility transferUtility;
    public AmazonS3Client s3Client;
    public BasicAWSCredentials credentials;
    protected Handler mHandler = new Handler();
    protected ProgressDialog mDialog;
    int REQUEST_STORAGE = 1;
    int filestatus = 0;
    String[] PERMISSIONS = {

            Manifest.permission.INTERNET,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_SMS,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.GET_ACCOUNTS};

    // String language = "en";

    //to enable and disable all view of layout

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }


    public String getDateFormatforatten(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("MMMM dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //to Compress Single Image
    public static void compressInputImage(Intent data, Context context, ImageView newIV) {
        Bitmap bitmap;
        Uri inputImageData = data.getData();
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), inputImageData);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
                newIV.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //to Compress Multiple Images
    public static Bitmap compressInputImage(Uri uri, Context context) {
        Bitmap bitmap = null;
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialog = new ProgressDialog(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            checkLocationPermission();
        }


        credentials = new BasicAWSCredentials(getString(R.string.aws_access_key),
                getString(R.string.aws_secret_key));
        s3Client = new AmazonS3Client(credentials);


    }

    @Override
    public void onPause() {
        super.onPause();
        if (mDialog != null) {
            mDialog.dismiss();
        }

    }

    private void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void uploadFile(List<SelectedImageList>  path, String s3bucketPath, final String imageName,
                           final OnAmazonFileuploaded onAmazonFileuploaded) {


        // Toast.makeText(BaseActivity.this, "method called", Toast.LENGTH_SHORT).show();
        // showProgress();

        mDialog.setMessage("Uploading image");
        mDialog.show();
        for (int i = 0; path.size() > i; i++) {
            if (path.get(i).getisIsedit()) {
                Uri uri = Uri.fromFile(new File(path.get(i).getImage()));
//                String name = path.get
//                        (i).substring(path.get
//                        (i).lastIndexOf("/"), path.get
//                        (i).length());

                String name= path.get(i).getImage().substring(path.get(i).getImage().lastIndexOf("/"), path.get(i).getImage().length());


                if (uri != null) {
                    final File destination;
                    destination = new File(this.getExternalFilesDir(null), name);
                    createFile(this, uri, destination);
                    transferUtility =
                            TransferUtility.builder()
                                    .context(this)
                                    .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                                    .s3Client(s3Client)
                                    .build();

                    TransferObserver uploadObserver =
                            transferUtility.upload(s3bucketPath + name, destination);
                    uploadObserver.setTransferListener(new TransferListener() {

                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            if (TransferState.COMPLETED == state) {
                                filestatus = 1;
                                //  Toast.makeText(BaseActivity.this, "uploaded", Toast.LENGTH_SHORT).show();
                                destination.delete();
                            } else if (TransferState.FAILED == state) {

                                //  Toast.makeText(BaseActivity.this, "failed", Toast.LENGTH_SHORT).show();
                                destination.delete();
                                hideProgress();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                            int percentDone = (int) percentDonef;
                            if (mDialog != null) {
                                mDialog.setMessage("Uploading image");
                            }


                            //Toast.makeText(BaseActivity.this, String.valueOf(percentDone), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            ex.printStackTrace();
                            ///   hideProgress();

                            Toast.makeText(BaseActivity.this, "error", Toast.LENGTH_SHORT).show();
                        }

                    });
                } else {
                    //  onAmazonFileuploaded.FileStatus(0,imageName);
                    Toast.makeText(BaseActivity.this, "uri null", Toast.LENGTH_SHORT).show();
                }
            }

        }
        mDialog.dismiss();
        onAmazonFileuploaded.FileStatus(1, imageName);


    }


    public void uploadImageandVideo(List<SelectedImageandVideoList>  path, String s3bucketPath, final String imageName,
                                    final OnAmazonFileuploaded onAmazonFileuploaded) {


        // Toast.makeText(BaseActivity.this, "method called", Toast.LENGTH_SHORT).show();
        // showProgress();

        mDialog.setMessage("Uploading image");
        mDialog.show();
        for (int i = 0; path.size() > i; i++) {
            if (path.get(i).getisIsedit()) {
                Uri uri = Uri.fromFile(new File(path.get(i).getImage()));
//                String name = path.get
//                        (i).substring(path.get
//                        (i).lastIndexOf("/"), path.get
//                        (i).length());

                String name= path.get(i).getImage().substring(path.get(i).getImage().lastIndexOf("/"), path.get(i).getImage().length());


                if (uri != null) {
                    final File destination;
                    destination = new File(this.getExternalFilesDir(null), name);
                    createFile(this, uri, destination);
                    transferUtility =
                            TransferUtility.builder()
                                    .context(this)
                                    .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                                    .s3Client(s3Client)
                                    .build();

                    TransferObserver uploadObserver =
                            transferUtility.upload(s3bucketPath + name, destination);
                    uploadObserver.setTransferListener(new TransferListener() {

                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            if (TransferState.COMPLETED == state) {
                                filestatus = 1;
                                //  Toast.makeText(BaseActivity.this, "uploaded", Toast.LENGTH_SHORT).show();
                                destination.delete();
                            } else if (TransferState.FAILED == state) {

                                //  Toast.makeText(BaseActivity.this, "failed", Toast.LENGTH_SHORT).show();
                                destination.delete();
                                hideProgress();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                            int percentDone = (int) percentDonef;
                            if (mDialog != null) {
                                mDialog.setMessage("Uploading image");
                            }


                            //Toast.makeText(BaseActivity.this, String.valueOf(percentDone), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            ex.printStackTrace();
                            ///   hideProgress();

                            Toast.makeText(BaseActivity.this, "error", Toast.LENGTH_SHORT).show();
                        }

                    });
                } else {
                    //  onAmazonFileuploaded.FileStatus(0,imageName);
                    Toast.makeText(BaseActivity.this, "uri null", Toast.LENGTH_SHORT).show();
                }
            }

        }
        mDialog.dismiss();
        onAmazonFileuploaded.FileStatus(1, imageName);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is enabled
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    && ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.READ_SMS)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
                    Manifest.permission.READ_PHONE_STATE)) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                alertDialog.setMessage(R.string.ok);
                alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Again request permission if deny any permission
                        requestPermission();
                    }
                });
                alertDialog.show();

            } else {
                //Move to settings page to accept permissions
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseActivity.this);
                alertDialog.setMessage(R.string.ok);
                alertDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", BaseActivity.this.getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });
                alertDialog.show();
            }
        }

    }

    private void checkPermissions() {
        if (!hasPermissionGranted()) {
            requestPermission();
        }
    }

    public void checkLocationPermission() {
        if (this != null && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public boolean hasPermissionGranted() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_STORAGE);
        }
    }

    // API Call handling
    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int messageId) {
        showProgress(getString(messageId));
    }

    public void showProgress(final CharSequence message) {

        if (getApplicationContext() != null) {
            mHandler.post(new Runnable() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    UiUtils.hideKeyboard(BaseActivity.this);
                    //mDialog.setMessage(message);
                    //mDialog.setIndeterminate(true);
                    mDialog = new ProgressDialog(BaseActivity.this);
                    mDialog.getWindow().setBackgroundDrawable(new
                            ColorDrawable(android.graphics.Color.TRANSPARENT));
                    mDialog.setIndeterminate(true);
                    mDialog.setCancelable(false);
                    mDialog.show();
                    mDialog.setContentView(R.layout.custom_progress_view);

                }
            });
        }
    }

    public void hideProgress() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        });
    }

    public void dismissDialogFragment(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            fragment.dismissAllowingStateLoss();
        }
    }

////Download image using Picaso
//        public void picasoImageLoader(ImageView imageView, String url, Context context) {
//        Picasso.with(context)
//                .load(getString(R.string.falcon_image_url) + url)
//                .placeholder(R.mipmap.img_sample_pro_pic)   // optional
////                                    .error(R.drawable.ic_error_fallback)      // optional
////                                    .resize(250, 200)                        // optional
////                                    .rotate(90)                             // optional
//                .into(imageView);
//    }
//    //Download image using Universal image loader
//    public void universalImageLoader(ImageView imageView, String image) {
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                .cacheOnDisk(true).resetViewBeforeLoading(true)
//                .showImageForEmptyUri(R.mipmap.dmk_fifteen)
//                .showImageOnFail(R.mipmap.dmk_fifteen)
//                .showImageOnLoading(R.mipmap.dmk_fifteen).build();
//        imageLoader.displayImage("http://memberportal.dci.in/uploads/Timelinemedia/" + image, imageView, options);
//    }

    //to get base64 from bitmap
    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    //to Decode URI
    public Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o);

        final int REQUIRED_SIZE = 100;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(
                getContentResolver().openInputStream(selectedImage), null, o2);
    }

    // to getPath from URI
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public String getDateFormatforleave(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd MMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //to Change Dateformat
    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //Download image using AsynTask
    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
//    }
}
