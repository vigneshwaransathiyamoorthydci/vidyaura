package com.school.teacherparent.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.util.IOUtils;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.AboutFragment;
import com.school.teacherparent.fragment.AddAttendanceFragment;
import com.school.teacherparent.fragment.AddCircularFragment;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.fragment.AddEventFragment;
import com.school.teacherparent.fragment.AddExamFragment;
import com.school.teacherparent.fragment.AddGalleryFragment;
import com.school.teacherparent.fragment.AddHomeWorkTabFragment;
import com.school.teacherparent.fragment.AddNewFeedFragment;
import com.school.teacherparent.fragment.AddResultFragment;
import com.school.teacherparent.fragment.AllPhotosandVideoGalleryFragment;
import com.school.teacherparent.fragment.AssignmentDetailsFragment;
import com.school.teacherparent.fragment.FaqFragment;
import com.school.teacherparent.fragment.FeedCommentReplyFragment;
import com.school.teacherparent.fragment.FeedCommentviewFragment;
import com.school.teacherparent.fragment.FeedbackFragment;
import com.school.teacherparent.fragment.FeesFragment;
import com.school.teacherparent.fragment.GalleryFragment;
import com.school.teacherparent.fragment.HelpFragment;
import com.school.teacherparent.fragment.HomeworkDetailsFragment;
import com.school.teacherparent.fragment.ParentFeedClapFragment;
import com.school.teacherparent.fragment.ParentFeedCommentviewFragment;
import com.school.teacherparent.fragment.ParentGalleryFragment;
import com.school.teacherparent.fragment.PollsFragment;
import com.school.teacherparent.fragment.PollsViewFragment;
import com.school.teacherparent.fragment.PricavypolicyFragment;
import com.school.teacherparent.fragment.SchoolFragment;
import com.school.teacherparent.fragment.SettingFragment;
import com.school.teacherparent.fragment.SubSyllabusFragment;
import com.school.teacherparent.fragment.TeacherFeedClapFragment;
import com.school.teacherparent.fragment.TimetableFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.fragment.TimetableforParentFragment;
import com.school.teacherparent.fragment.UpdateAttendanceFragment;
import com.school.teacherparent.fragment.UpdateClassWorkFragment;
import com.school.teacherparent.fragment.UpdateExamFragment;
import com.school.teacherparent.fragment.UpdateHomeAssignmentFragment;
import com.school.teacherparent.fragment.UpdateHomeWorkFragment;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;

/**
 * Created by harini on 10/12/2018.
 */

public class SidemenuDetailActivity extends BaseActivity {
    public  static  ImageView back,calendarImageview,ivSearch,ivNoti;
    Fragment fragment;
    public static TextView title;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public  VidyAPI vidyauraAPI;
    public  static  EditText edittextleaveSearch;
    int usetType;
    NotificationReceiver notificationReceiver;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sidemenu);
        title = findViewById(R.id.title);
        back = findViewById(R.id.back);
        ivSearch = findViewById(R.id.iv_search);
        ivNoti = findViewById(R.id.noti);
        calendarImageview=findViewById(R.id.calendarImageview);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        edittextleaveSearch=(EditText)findViewById(R.id.edittextSearch);
        usetType=sharedPreferences.getInt(Constants.USERTYPE,0);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            SidemenuDetailActivity.ivNoti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            SidemenuDetailActivity.ivNoti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        Intent i = getIntent();
        if (i.hasExtra("type")) {
            calendarImageview.setVisibility(View.GONE);
            String type = i.getStringExtra("type");
            int feedID=i.getIntExtra("feedID",0);
            int commID=i.getIntExtra("commID",0);
            int circularID=i.getIntExtra("circuId",0);
            int eventId=i.getIntExtra("eventID",0);
            int examId=i.getIntExtra("examID",0);
            String className=i.getStringExtra("classname");
            String homeWorkID=i.getStringExtra("homeWorkID");
            int classID=i.getIntExtra("classID",0);
            int sectionID=i.getIntExtra("sectionID",0);
            int attendanceSessionCount=i.getIntExtra("attendanceSessionCount",5);
            int subjectID=i.getIntExtra("subjectID",0);
            String mode=i.getStringExtra("mode");
            int position=i.getIntExtra("position",0);
            int homeworkIDforedit=i.getIntExtra("homeworkID",0);
            String fdate=i.getStringExtra("updatedate");
            int classworkID=i.getIntExtra("classworkID",0);
            String chapterID=i.getStringExtra("chapterID");
            String topicID=i.getStringExtra("topicID");
            Bundle albumlist=i.getBundleExtra("albumlist");
            String albumName=i.getStringExtra("albumName");
            String title=i.getStringExtra("title");
            int studID=i.getIntExtra("studID",0);
            int selectedPollsCategoryID=i.getIntExtra("SelectedPollsCategoryID",0);
            int groupID=i.getIntExtra("groupID",0);
            int selectedSchoolID=i.getIntExtra("selectedSchoolID",0);
            boolean isGroup=i.getBooleanExtra("isGroup",false);
            switch (type) {
                case "faq":
                    fragment = new FaqFragment();
                    replaceFragment(fragment);
                    break;
                case "feedback":
                    fragment = new FeedbackFragment();
                    replaceFragment(fragment);
                    break;
                case "privacy":
                    fragment = new PricavypolicyFragment();
                    replaceFragment(fragment);
                    break;
                case "about":
                    fragment = new AboutFragment();
                    replaceFragment(fragment);
                    break;
                case "school":
                    fragment = new SchoolFragment();
                    replaceFragment(fragment);
                    break;
                case "timetable":
                    fragment = new TimetableFragment();
                    replaceFragment(fragment);
                    break;
                case "gallery":
                    fragment = new GalleryFragment();
                    replaceFragment(fragment);
                    break;

                case "parentgallery":
                    fragment = new ParentGalleryFragment();
                    replaceFragment(fragment);
                    break;

                case "timetableparent":
                    fragment = new TimetableforParentFragment();
                    replaceFragment(fragment);
                    break;

                case "fees":
                    fragment = new FeesFragment();
                    replaceFragment(fragment);
                    break;

                case "polls":
                    fragment = new PollsFragment();
                    replaceFragment(fragment);
                    break;

                case "pollsview":
                    fragment = new PollsViewFragment();
                    Bundle polls = new Bundle();
                    polls.putInt("SelectedPollsCategoryID", selectedPollsCategoryID);
                    fragment.setArguments(polls);
                    replaceFragment(fragment);
                    break;

                case "gallerydetails":
                    Bundle bundleal= new Bundle();
                    bundleal.putBundle("albumlist", albumlist);
                    bundleal.putString("title", title);
                    fragment = new AllPhotosandVideoGalleryFragment();
                    fragment.setArguments(albumlist);
                    replaceFragment(fragment);
                    break;
                case "addgallery":
                    Bundle bundlead= new Bundle();
                    bundlead.putString("albumName", albumName);
                    fragment = new AddGalleryFragment();
                    fragment.setArguments(bundlead);
                    replaceFragment(fragment);
                    break;
                case "settings":
                    fragment = new SettingFragment();
                    replaceFragment(fragment);
                    break;
                case "help":
                    fragment = new HelpFragment();
                    replaceFragment(fragment);
                    break;
                case "attendance":
                    fragment = new AddAttendanceFragment();
                    replaceFragment(fragment);
                    break;
                case "attendanceupdate":
                    fragment = new UpdateAttendanceFragment();
                    Bundle bundleau = new Bundle();
                    bundleau.putString("fdate", fdate);
                    bundleau.putInt("classID", classID);
                    bundleau.putInt("sectionID", sectionID);
                    bundleau.putInt("attendanceSessionCount", attendanceSessionCount);
                    bundleau.putInt("groupID", groupID);
                    bundleau.putBoolean("isGroup", isGroup);
                    fragment.setArguments(bundleau);
                    replaceFragment(fragment);
                    break;
                case "circular":
                    fragment = new AddCircularFragment();
                    Bundle bundlec = new Bundle();
                    bundlec.putInt("circuId", circularID);
                    fragment.setArguments(bundlec);
                    replaceFragment(fragment);
                    break;
                case "events":
                    fragment = new AddEventFragment();
                    Bundle bundleev = new Bundle();
                    bundleev.putInt("eventID", eventId);
                    fragment.setArguments(bundleev);
                    replaceFragment(fragment);
                    break;
                case "exam":
                    fragment = new AddExamFragment();

                    replaceFragment(fragment);
                    break;

                case "updateexam":
                    fragment = new UpdateExamFragment();
                    Bundle bundleex = new Bundle();
                    bundleex.putInt("examID",examId);
                    fragment.setArguments(bundleex);
                    replaceFragment(fragment);
                    break;
                case "result":
                    fragment = new AddResultFragment();
                    replaceFragment(fragment);
                    break;
                case "classwork":
                    fragment = new AddClassWorkFragment();
                    replaceFragment(fragment);
                    break;

                case "updateclasswork":
                    fragment = new UpdateClassWorkFragment();
                    Bundle bundlecw = new Bundle();
                    bundlecw.putInt("classworkID",classworkID);
                    bundlecw.putInt("classID",classID);
                    bundlecw.putInt("subjectID",subjectID);
                    bundlecw.putString("chapterID",chapterID);
                    bundlecw.putString("topicID",topicID);
                    bundlecw.putInt("sectionID",sectionID);
                    fragment.setArguments(bundlecw);
                    replaceFragment(fragment);
                    break;
                case "homework":
                    fragment = new AddHomeWorkTabFragment();
                    Bundle bundle11 = new Bundle();
                    bundle11.putString("mode", mode);
                    bundle11.putInt("homeworkIDforedit",homeworkIDforedit);
                    bundle11.putInt("position",position);
                    fragment.setArguments(bundle11);
                    replaceFragment(fragment);
                    break;

                case "homeworkupdate":
                    fragment = new UpdateHomeWorkFragment();
                    Bundle bundle1hu = new Bundle();
                    bundle1hu.putString("mode", mode);
                    bundle1hu.putInt("homeworkIDforedit",homeworkIDforedit);
                    fragment.setArguments(bundle1hu);
                    replaceFragment(fragment);
                    break;


                case "assigmentupdate":
                    fragment = new UpdateHomeAssignmentFragment();
                    Bundle bundle1as = new Bundle();
                    bundle1as.putString("mode", mode);
                    bundle1as.putInt("homeworkIDforedit",homeworkIDforedit);
                    fragment.setArguments(bundle1as);
                    replaceFragment(fragment);
                    break;
                case "feed":
                    fragment = new AddNewFeedFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("feedId", feedID);
                    fragment.setArguments(bundle1);
                    replaceFragment(fragment);
                    break;
                case "claps":
                    if (usetType==1)
                    {
                        fragment = new TeacherFeedClapFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("feedId", feedID);
                        fragment.setArguments(bundle);
                        replaceFragment(fragment);
                    }
                    else if (usetType==2)
                    {
                        fragment = new ParentFeedClapFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("feedId", feedID);
                        fragment.setArguments(bundle);
                        replaceFragment(fragment);
                    }

                    break;
                case "feedcommentview":
                    if(usetType==1) {
                        fragment = new FeedCommentviewFragment();
                        Bundle bundles = new Bundle();
                        bundles.putInt("feedId", feedID);
                        fragment.setArguments(bundles);
                        replaceFragment(fragment);
                    }
                    else if (usetType==2)
                    {
                        fragment = new ParentFeedCommentviewFragment();
                        Bundle bundles = new Bundle();
                        bundles.putInt("feedId", feedID);
                        fragment.setArguments(bundles);
                        replaceFragment(fragment);
                    }
                    break;
                case "subsyllabus":
                    fragment = new SubSyllabusFragment();
                    replaceFragment(fragment);
                    break;

                case "commentreply":
                    fragment = new FeedCommentReplyFragment();
                    Bundle bundles2 = new Bundle();
                    bundles2.putInt("feedId", feedID);
                    bundles2.putInt("feedcommentId", commID);
                    fragment.setArguments(bundles2);
                    replaceFragment(fragment);
                    break;

                case "homeworkDetails":
                    fragment = new HomeworkDetailsFragment();
                    Bundle bundles3 = new Bundle();
                    bundles3.putString("className", className);
                    bundles3.putString("homeWorkID",homeWorkID);
                    bundles3.putInt("classID",classID);
                    bundles3.putInt("sectionID",sectionID);
                    bundles3.putInt("subjectID",subjectID);
                    bundles3.putInt("studID",studID);
                    bundles3.putInt("selectedSchoolID",selectedSchoolID);
                    fragment.setArguments(bundles3);
                    replaceFragment(fragment);
                    break;


                case "assignmentDetails":
                    fragment = new AssignmentDetailsFragment();
                    Bundle bundlesad = new Bundle();
                    bundlesad.putString("className", className);
                    bundlesad.putString("homeWorkID",homeWorkID);
                    bundlesad.putInt("classID",classID);
                    bundlesad.putInt("sectionID",sectionID);
                    bundlesad.putInt("subjectID",subjectID);
                    bundlesad.putInt("studID",studID);
                    bundlesad.putInt("selectedSchoolID",selectedSchoolID);
                    fragment.setArguments(bundlesad);
                    replaceFragment(fragment);
                    break;
            }
        }



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.side_main, fragment, "");
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        JzvdStd.releaseAllVideos();
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      //  Toast.makeText(SidemenuDetailActivity.this, "called" , Toast.LENGTH_SHORT).show();
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
           // Toast.makeText(SidemenuDetailActivity.this, ""+mPaths.size(), Toast.LENGTH_SHORT).show();
            //Your Code

            if (mPaths.size()>5){
                Toast.makeText(this, R.string.maxm_five_image, Toast.LENGTH_SHORT).show();
            }
            else {
                if (fragment instanceof AddNewFeedFragment){
                    ((AddNewFeedFragment) fragment).setPickedImageDetails(null,mPaths);
                }
                if (fragment instanceof AddHomeWorkTabFragment){

                    ((AddHomeWorkTabFragment) fragment).getCurrentFragment(mPaths);
                }

                if (fragment instanceof UpdateHomeWorkFragment)
                {
                    ((UpdateHomeWorkFragment) fragment).setPickedImageDetails(null,mPaths);
                }

                if (fragment instanceof AddClassWorkFragment)
                {
                    ((AddClassWorkFragment) fragment).setPickedImageDetails(null,mPaths);
                }

                if (fragment instanceof UpdateClassWorkFragment)
                {
                    ((UpdateClassWorkFragment) fragment).setPickedImageDetails(null,mPaths);
                }
                if (fragment instanceof AddCircularFragment)
                {
                    ((AddCircularFragment) fragment).setPickedImageDetails(null,mPaths);
                }

                if (fragment instanceof AddGalleryFragment)
                {
                    ((AddGalleryFragment) fragment).setPickedImageDetails(null,mPaths);
                }


                if (fragment instanceof UpdateHomeAssignmentFragment)
                {
                    ((UpdateHomeAssignmentFragment) fragment).setPickedImageDetails(null,mPaths);
                }


            }


        }
      else   if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths =  data.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH);
            if (fragment instanceof AddGalleryFragment)
            {
                ((AddGalleryFragment) fragment).setPickedVideoDetails(null,mPaths);
            }

        }
    }

    private void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void uploadFile(List<SelectedImageList>  path, String s3bucketPath, final String imageName,
                           final OnAmazonFileuploaded onAmazonFileuploaded) {


        // Toast.makeText(BaseActivity.this, "method called", Toast.LENGTH_SHORT).show();
        // showProgress();

        mDialog.setMessage("Uploading image");
        mDialog.show();
        for (int i = 0; path.size() > i; i++) {
            if (path.get(i).getisIsedit()) {
                Uri uri = Uri.fromFile(new File(path.get(i).getImage()));
//                String name = path.get
//                        (i).substring(path.get
//                        (i).lastIndexOf("/"), path.get
//                        (i).length());

                String name= path.get(i).getImage().substring(path.get(i).getImage().lastIndexOf("/"), path.get(i).getImage().length());


                if (uri != null) {
                    final File destination;
                    destination = new File(this.getExternalFilesDir(null), name);
                    createFile(this, uri, destination);
                    transferUtility =
                            TransferUtility.builder()
                                    .context(this)
                                    .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                                    .s3Client(s3Client)
                                    .build();

                    TransferObserver uploadObserver =
                            transferUtility.upload(s3bucketPath + name, destination);
                    uploadObserver.setTransferListener(new TransferListener() {

                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            if (TransferState.COMPLETED == state) {
                                filestatus = 1;
                                //  Toast.makeText(BaseActivity.this, "uploaded", Toast.LENGTH_SHORT).show();
                                destination.delete();
                            } else if (TransferState.FAILED == state) {

                                //  Toast.makeText(BaseActivity.this, "failed", Toast.LENGTH_SHORT).show();
                                destination.delete();
                                hideProgress();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                            int percentDone = (int) percentDonef;
                            if (mDialog != null) {
                                mDialog.setMessage("Uploading image");
                            }


                            //Toast.makeText(BaseActivity.this, String.valueOf(percentDone), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            ex.printStackTrace();
                            ///   hideProgress();

                            Toast.makeText(SidemenuDetailActivity.this, "error", Toast.LENGTH_SHORT).show();
                        }

                    });
                } else {
                    //  onAmazonFileuploaded.FileStatus(0,imageName);
                    Toast.makeText(SidemenuDetailActivity.this, "uri null", Toast.LENGTH_SHORT).show();
                }
            }

        }
        mDialog.dismiss();
        onAmazonFileuploaded.FileStatus(1, imageName);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_notifi:
                startActivity(new Intent(SidemenuDetailActivity.this, NotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
