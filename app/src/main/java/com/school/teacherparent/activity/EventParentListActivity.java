package com.school.teacherparent.activity;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.R;
import com.school.teacherparent.adapter.EventsParentAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.EventList;
import com.school.teacherparent.models.EventListParms;
import com.school.teacherparent.models.EventListResponse;
import com.school.teacherparent.models.EventParentListResponse;
import com.school.teacherparent.models.TimeTableModel;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventParentListActivity extends BaseActivity {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    RecyclerView eventrecycle;
    EventsParentAdapter eventAdapter;
    ImageView back,noti,imgFilter;
    FloatingActionButton addcircular;
    private ArrayList<EventList> eventList = new ArrayList<>();
    SwipeRefreshLayout eventlistswipelayout;
    int limit=5;
    int offset=0;
    EventListResponse eventListResponse;
    public ArrayList<EventListResponse.eventsList> eventsListArrayList;
    ArrayList<EventList> eventLists= new ArrayList();
    TextView noeventListTextview;
    LinearLayoutManager linearLayoutManager;
    boolean loading=true;
    //ImageView calendarImageview;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate;
    private EventParentListResponse eventparentListResponse;
    private int eventsID;
    private String eventAttendancestatus;
    private ArrayList<EventList> eventsList;
    public TextView acceptence;
    boolean isFilter = true;
    int success;
    NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_parent_list);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        eventrecycle = (RecyclerView) findViewById(R.id.recycle_events);
        back = findViewById(R.id.back);
        noti = findViewById(R.id.noti);
        eventList = new ArrayList<>();
        addcircular = findViewById(R.id.fab);
        imgFilter = findViewById(R.id.img_filter);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        eventsListArrayList=new ArrayList<>();
        //calendarImageview=findViewById(R.id.calendarImageview);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventParentListActivity.this,NotificationActivity.class));
            }
        });
        noeventListTextview=findViewById(R.id.noeventListTextview);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        eventrecycle.setItemAnimator(new DefaultItemAnimator());
        eventrecycle.setLayoutManager(linearLayoutManager);
        eventAdapter = new EventsParentAdapter(eventLists, getApplicationContext(),EventParentListActivity.this,EventParentListActivity.this);
        eventrecycle.setAdapter(eventAdapter);

        /*eventAdapter.setOnClickListen(new AddTouchListener() {
            @Override
            public void onTouchClick(int position,String text) {
                pollForEventsAPI((eventLists.get(position).getEventID()),eventLists.get(position).getEventAttendeesStatus(),text);


            }
        });*/

        /*eventAdapter.setOnClickListen(new EventsParentAdapter.AddTouchListener() {

            public TextView acceptence;

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onTouchClick(int position, TextView text, RelativeLayout accepted_layout) {
                this.acceptence=text;
                if (acceptence.getText().toString().trim().equals("ACCEPT")) {
                    pollForEventsAPI((eventLists.get(position).getEventID()), eventLists.get(position).getEventAttendeesStatus(), text, accepted_layout);
                    //acceptence.setTextColor(getResources().getColor(R.color.white));
                    acceptence.setText("ACCEPTED");
                } else if (acceptence.getText().toString().trim().equals("MAYBE")) {
                    pollForEventsAPI((eventLists.get(position).getEventID()), eventLists.get(position).getEventAttendeesStatus(), text, accepted_layout);
                    //acceptence.setTextColor(getResources().getColor(R.color.white));
                    acceptence.setText("MAYBE'S");
                } else if (acceptence.getText().toString().trim().equals("DECLINE")) {
                    pollForEventsAPI((eventLists.get(position).getEventID()), eventLists.get(position).getEventAttendeesStatus(), text, accepted_layout);
                    //acceptence.setTextColor(getResources().getColor(R.color.white));
                    acceptence.setText("DECLINED");
                }
               //accepted_layout.setBackground(getResources().getDrawable(R.drawable.fill_border));
               // acceptence.setTextAppearance(R.style.text_style_title);


            }
        });*/

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        eventlistswipelayout=findViewById(R.id.swipe_eventlist);
        eventlistswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                eventlistswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        offset=0;
                        limit=5;
                        //eventsListArrayList.clear();
                        eventList.clear();
                        eventLists.clear();
                        getEventList();
                        eventlistswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });
        eventrecycle.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getEventList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });
        addcircular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventParentListActivity.this,
                        SidemenuDetailActivity.class).putExtra("type","events").putExtra("eventID",0));
            }
        });



        eventrecycle.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addcircular.setVisibility(View.GONE);//changed
                }
                else
                {
                    addcircular.setVisibility(View.GONE);
                }
            }
        });

        imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFilter) {

                    Collections.sort(eventLists, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            EventList p1 = (EventList) o1;
                            EventList p2 = (EventList) o2;
                            return p2.getEvent_date().compareTo(p1.getEvent_date());
                        }
                    });
                    imgFilter.setImageDrawable(getResources().getDrawable(R.mipmap.sort_asc));
                    eventAdapter.notifyDataSetChanged();
                    isFilter = false;
                } else {

                    Collections.sort(eventLists, new Comparator() {
                        @Override
                        public int compare(Object o1, Object o2) {
                            EventList p1 = (EventList) o1;
                            EventList p2 = (EventList) o2;
                            return p1.getEvent_date().compareTo(p2.getEvent_date());
                        }
                    });
                    imgFilter.setImageDrawable(getResources().getDrawable(R.mipmap.sort_dec));
                    eventAdapter.notifyDataSetChanged();
                    isFilter = true;
                }
            }
        });

        /*calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EventParentListActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");



                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                //selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                //eventsListArrayList.clear();
                                eventList.clear();
                                eventLists.clear();
                                eventAdapter.notifyDataSetChanged();
                                getEventList();


                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });*/
    }

    public int pollForEventsAPI(int eventsID, String eventAttendancestatus, final TextView textView, final int state) {
        if (Util.isNetworkAvailable()) {
            loading=false;
            showProgress();
            EventListParms eventListParms=new EventListParms();
            eventListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            eventListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            eventListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            eventListParms.setEventsID(String.valueOf(eventsID));
            eventListParms.setEventAttendeesStatus(eventAttendancestatus);
            vidyauraAPI.pollsforevents(eventListParms).enqueue(new Callback<EventParentListResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResponse(Call<EventParentListResponse> call, Response<EventParentListResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        eventparentListResponse = response.body();
                        if (eventparentListResponse.getStatus()!= Util.STATUS_TOKENEXPIRE) {
                            // eventsListArrayList.clear();
                            if (eventparentListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                success = eventparentListResponse.getStatus();
                                Toast.makeText(getApplicationContext(),eventparentListResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                if (state == 1){
                                    textView.setText("ACCEPTED");
                                } else if (state == 2) {
                                    textView.setText("MAYBE");
                                } else if (state == 3) {
                                    textView.setText("DECLINED");
                                }

                               // acceptence.setTextAppearance(R.style.text_style_title);

                            }
                            else {
                                loading=false;

                                    Toast.makeText(EventParentListActivity.this, eventparentListResponse.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                        else
                        {
                            loading=false;
                            Intent i = new Intent(EventParentListActivity.this, SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();

                        }

                    }
                    else
                    {
                        loading=false;
                        Toast.makeText(EventParentListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }


                }

                @Override
                public void onFailure(Call<EventParentListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventParentListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(EventParentListActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
        return success;
    }

    private void getEventList() {
        if (Util.isNetworkAvailable()) {
            imgFilter.setImageDrawable(getResources().getDrawable(R.mipmap.sort_dec));
            loading=false;
            showProgress();
            EventListParms eventListParms=new EventListParms();
            eventListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            eventListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            eventListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            eventListParms.setLimit(limit);
            eventListParms.setOffset(offset);
            eventListParms.setEventByDate(formattedDate);
            vidyauraAPI.getEventList(eventListParms).enqueue(new Callback<EventListResponse>() {
                @Override
                public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        eventListResponse = response.body();
                        if (eventListResponse.getStatus()!= Util.STATUS_TOKENEXPIRE) {
                            // eventsListArrayList.clear();
                            eventsListArrayList = response.body().getEventsList();
                            if (eventListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (eventsListArrayList.size()!=0) {
                                    eventrecycle.setVisibility(View.VISIBLE);
                                    noeventListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<eventsListArrayList.size();y++)
                                    {
                                        ArrayList<String> classNamelist=new ArrayList<>();
                                        if (eventsListArrayList.get(y).getAuthor_type().equals("Admin"))
                                        {
                                            classNamelist.clear();
                                            for (int i=0;i<eventsListArrayList.get(y).getClassName().size();i++)
                                            {
                                                classNamelist.add(eventsListArrayList.get(y).getClassName().get(i).getClassName());
                                            }
                                            eventLists.add(new EventList(eventsListArrayList.get(y).getEventID(),eventsListArrayList.get(y).getId(),eventsListArrayList.get(y).getRSVP(),eventsListArrayList.get(y).getTitle(),
                                                    eventsListArrayList.get(y).getDescription(),eventsListArrayList.get(y).getEvent_date(),eventsListArrayList.get(y).getVenue(),eventsListArrayList.get(y).getClass_id(),
                                                    eventsListArrayList.get(y).getAdminName(),eventsListArrayList.get(y).getEventAcceptedCount(),eventsListArrayList.get(y).getEventMaybeCount(),
                                                    eventsListArrayList.get(y).getEventDeclinedCount(),eventsListArrayList.get(y).getNotResponded(),eventsListArrayList.get(y).getIsUserPostedEvent(),

                                                    eventsListArrayList.get(y).getUserDetails(),eventsListArrayList.get(y).getClassName(),classNamelist,eventsListArrayList.get(y).getEventAttendeesStatus()));


                                            // eventsListArrayList.get(y).getEventID();

                                          /*  Bundle bundle = new Bundle();
                                            if (bundle != null) {
                                                eventsID = bundle.getInt("eventsID", 0);
                                            }*/


//                                            bundle.putString("id", String.valueOf(eventsListArrayList.get(0).getEventID()));

                                        }
                                        else
                                        {
                                            for (int i=0;i<eventsListArrayList.get(y).getClassName().size();i++)
                                            {
                                                classNamelist.add(eventsListArrayList.get(y).getClassName().get(i).getClassName());
                                            }
                                            eventLists.add(new EventList(eventsListArrayList.get(y).getEventID(),eventsListArrayList.get(y).getId(),eventsListArrayList.get(y).getRSVP(),eventsListArrayList.get(y).getTitle(),
                                                    eventsListArrayList.get(y).getDescription(),eventsListArrayList.get(y).getEvent_date(),eventsListArrayList.get(y).getVenue(),eventsListArrayList.get(y).getClass_id(),
                                                    eventsListArrayList.get(y).getAdminName(),eventsListArrayList.get(y).getEventAcceptedCount(),eventsListArrayList.get(y).getEventMaybeCount(),
                                                    eventsListArrayList.get(y).getEventDeclinedCount(),eventsListArrayList.get(y).getNotResponded(),eventsListArrayList.get(y).getIsUserPostedEvent(),
                                                    eventsListArrayList.get(y).getUserDetails(),eventsListArrayList.get(y).getClassName(),classNamelist,eventsListArrayList.get(y).getEventAttendeesStatus()));

                                        }

                                    }

//
                                    //eventAdapter = new EventsAdapter(eventsListArrayList,EventListActivity.this,EventListActivity.this,EventListActivity.this);
//
//
                                    /// eventrecycle.setAdapter(eventAdapter);
                                    if (eventLists.size() > 1) {
                                        imgFilter.setVisibility(View.VISIBLE);
                                    }
                                    eventAdapter.notifyDataSetChanged();
                                    loading=true;
                                    eventrecycle.setVisibility(View.VISIBLE);
                                    noeventListTextview.setVisibility(View.GONE);
                                }

                                else if (eventLists.size()!=0 && eventsListArrayList.size()!=0)
                                {
                                    noeventListTextview.setVisibility(View.VISIBLE);
                                    eventrecycle.setVisibility(View.GONE);
                                    loading=false;
                                }
                                else {
                                    loading=false;
                                    if (eventLists.size()==0)
                                    {
                                        Toast.makeText(EventParentListActivity.this, eventListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                            else {
                                loading=false;
                                if (eventLists.size()==0)
                                {
                                    Toast.makeText(EventParentListActivity.this, eventListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                        else
                        {
                            loading=false;
                            Intent i = new Intent(EventParentListActivity.this, SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();

                        }

                    }
                    else
                    {
                        loading=false;
                        Toast.makeText(EventParentListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<EventListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventParentListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            imgFilter.setVisibility(View.GONE);
            Toast.makeText(EventParentListActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("DANNY","onStart");
        if (loading) {
            eventLists.clear();
            getEventList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("DANNY","Onresume");
        if (loading) {
            eventLists.clear();
            getEventList();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d("DANNY","onStop");
//        offset=0;
//        limit=5;
//        eventsListArrayList.clear();
//        eventList.clear();
//        eventLists.clear();
        eventLists.clear();
        loading=true;
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
