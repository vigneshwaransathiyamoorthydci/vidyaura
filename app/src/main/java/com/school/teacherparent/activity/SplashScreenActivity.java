package com.school.teacherparent.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.school.teacherparent.BuildConfig;
import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.ParentsUserLoginResponse;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.AppPreferences;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 8/23/2018.
 */

public class SplashScreenActivity extends BaseActivity implements Animation.AnimationListener {
    Button getstarted;
    TextView tv_about;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    public VidyAPI vidyauraAPI;
    private SharedPreferences fcmSharedPrefrences;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    UserLoginResponse userLoginResponse;
    ParentsUserLoginResponse parentsUserLoginResponse;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        getstarted = (Button) findViewById(R.id.get_started);
        tv_about = findViewById(R.id.tv_about);

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Service not provided",Toast.LENGTH_SHORT).show();
            }
        });

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            //  AppPreferences.setversioncode(getApplicationContext(), String.valueOf(verCode));


        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }


        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID)).commit();
        editor.putString(Constants.APPID, getApplication().getPackageName()).commit();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).commit();
        editor.putInt(Constants.OS_VERSION, Build.VERSION.SDK_INT).commit();
        fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        getstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getString(Constants.USERID,"").toString().length()>0 && sharedPreferences.getString(Constants.PHONE,"").toString().length()>0)
                {

                    int securityPin=sharedPreferences.getInt(Constants.SECURITYPINSTATUS,0);
                    int usetType=sharedPreferences.getInt(Constants.USERTYPE,0);
                    if (securityPin<=0)
                    {
                        if (usetType==1) {
                            userLogin();
                        }
                        else if (usetType==2)
                        {
                            parentsuserLogin();
                        }
                    }
                    else
                    {
                        Intent i = new Intent(SplashScreenActivity.this, SecurityPinActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }



                }
                else
                {
                    Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    // }
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();

                }

            }
        });
       /* UpdateMe.with(this, 30).setDialogVisibility(true)
                .continueButtonVisibility(true)
                .setPositiveButtonText("Update")
                //.setNegativeButtonText("Oh No...")
                .setDialogIcon(R.drawable.logo_copy)
                .onNegativeButtonClick(new OnNegativeButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Later Button Clicked");
                        dialog.dismiss();
                    }
                })
                .onPositiveButtonClick(new OnPositiveButtonClickListener() {

                    @Override
                    public void onClick(LovelyStandardDialog dialog) {

                        Log.d(UpdateMe.TAG, "Update Button Clicked");
                        dialog.dismiss();
                    }
                })
                .check();*/

    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {

       /* Intent i;
        if (AppPreferences.isLoggedIn(SplashScreenActivity.this)) {
            i = new Intent(SplashScreenActivity.this, MainActivity.class);

        } else {*/

/*
        if (Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase("")) {
            new UserRepository().accessTokenValidation(this, iUserDataHandler);
            new UserRepository().requestAppSettings(mCurrentActivity, iUserDataHandler);
        } else if (!Utils.isConnected(mCurrentActivity) &&
                !AppPreferences.getUserProfile(mCurrentActivity).getAccessToken().equalsIgnoreCase(""))
            iUserDataHandler.onTokenValidated();
        else
            iUserDataHandler.onError("");*/
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }



    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus()!= 401) {
                        if (userLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, userLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, userLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, userLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, userLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, userLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, userLoginResponse.getData().get(0).getEmail());
                                editor.putString(Constants.GENDER, userLoginResponse.getData().get(0).getGender());
                                editor.putString(Constants.EMP_ID, userLoginResponse.getData().get(0).getEmp_id());
                                editor.putInt(Constants.ID, userLoginResponse.getData().get(0).getId());
                                editor.putString(Constants.DOB, userLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.JOINING_DATE, userLoginResponse.getData().get(0).getJoining_date());
                                editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                if (userLoginResponse.getAppSettings() != null) {
                                if (userLoginResponse.getAppSettings().size() > 0) {
                                    editor.putInt(Constants.NOTIFICATIONSTATUS, userLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                    editor.putInt(Constants.MESSAGESTATUS, userLoginResponse.getAppSettings().get(0).getMessageStatus());
                                    editor.putInt(Constants.SECURITYPINSTATUS, userLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                }
                                }
                                editor.putInt(Constants.HASCHILDREN, userLoginResponse.getHasChildren());
                                editor.putString(Constants.SCHOOL_LOGO, userLoginResponse.getSchoolLogo());
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    editor.putInt(Constants.TOTAL_LEVEL_POINTS, userLoginResponse.getTotalLevelPoints());
                                    editor.putString(Constants.POINTS_NAME, userLoginResponse.getTeacherLevel().getName());
                                    editor.putString(Constants.BADGE_IMAGE, userLoginResponse.getTeacherLevel().getBadge_image());
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, userLoginResponse.getData().get(0).getType());
                                }
                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,userLoginResponse.getData().getPhone());
                                editor.commit();

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname()+" "+userLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+userLoginResponse.getData().get(0).getEmp_photo(),
                                        userLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                startActivity(new Intent(SplashScreenActivity.this, TestDrawerActivity.class));
                                finish();

                            } else {
                                Toast.makeText(SplashScreenActivity.this, userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                            userLogin();
                        }} else
                    {
                        hideProgress();
                        startActivity(new Intent(SplashScreenActivity.this,LoginActivity.class));
                    }
                    } else {
                        Toast.makeText(SplashScreenActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(SplashScreenActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(SplashScreenActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }


    private void parentsuserLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            Log.d("tag", "userLogin: "+sharedPreferences.getString(Constants.PHONE, "")+
                    sharedPreferences.getString(Constants.APPVERSION, "")+sharedPreferences.getString(Constants.DEVICEID, "")+
                    String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0))+String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.parentsuserLogin(userLoginParam).enqueue(new Callback<ParentsUserLoginResponse>() {
                @Override
                public void onResponse(Call<ParentsUserLoginResponse> call, Response<ParentsUserLoginResponse> response) {
                    hideProgress();
                    parentsUserLoginResponse = response.body();
                    if (parentsUserLoginResponse != null) {
                        if (parentsUserLoginResponse.getStatus()!= 401) {
                        if (parentsUserLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (parentsUserLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, parentsUserLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, parentsUserLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, parentsUserLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, parentsUserLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, parentsUserLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, parentsUserLoginResponse.getData().get(0).getEmail());
                                //editor.putString(Constants.SCHOOL_LOGO, parentsUserLoginResponse.getSchoolLogo());
                                //editor.putString(Constants.GENDER, parentsUserLoginResponse.getData().get(0).getGender());
                                //editor.putString(Constants.EMP_ID, parentsUserLoginResponse.getData().get(0).getEmp_id());
                                //editor.putString(Constants.DOB, parentsUserLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.PROFILE_PHOTO, parentsUserLoginResponse.getData().get(0).getPhoto());
                                if (parentsUserLoginResponse.getAppSettings() != null) {
                                    if (parentsUserLoginResponse.getAppSettings().size() > 0) {
                                        editor.putInt(Constants.NOTIFICATIONSTATUS, parentsUserLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                        editor.putInt(Constants.MESSAGESTATUS, parentsUserLoginResponse.getAppSettings().get(0).getMessageStatus());
                                        editor.putInt(Constants.SECURITYPINSTATUS, parentsUserLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                    }
                                }

                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, parentsUserLoginResponse.getData().get(0).getType());
                                }

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((parentsUserLoginResponse.getData().get(0).getFname()+" "+parentsUserLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+parentsUserLoginResponse.getData().get(0).getPhoto(),
                                        parentsUserLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(parentsUserLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, parentsUserLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,parentsUserLoginResponse.getData().getPhone());
                                editor.commit();
                                startActivity(new Intent(SplashScreenActivity.this, TestDrawerActivity.class));
                                finish();
                                Log.d("device", "onResponse: " + parentsUserLoginResponse.getSecuredToken());
                            } else {
                                Toast.makeText(SplashScreenActivity.this, parentsUserLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, parentsUserLoginResponse.getToken()).commit();
                            parentsuserLogin();
                        }} else
                        {
                            hideProgress();
                            startActivity(new Intent(SplashScreenActivity.this,LoginActivity.class));
                        }
                    } else {
                        Toast.makeText(SplashScreenActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ParentsUserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(SplashScreenActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(SplashScreenActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }
}
