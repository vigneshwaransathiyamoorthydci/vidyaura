package com.school.teacherparent.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.ExamDetailListFragment;
import com.school.teacherparent.fragment.ExamDetailListFragment1;
import com.school.teacherparent.models.ExamUpcomingResponse;
import com.school.teacherparent.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;

public class ExamDetailListActivity extends BaseActivity {

    public static ImageView back,noti;
    public static TextView title;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_detail_list);

        back=(ImageView)findViewById(R.id.back);
        noti=(ImageView)findViewById(R.id.noti);
        title=(TextView)findViewById(R.id.title);
        VidyauraApplication.getContext().getComponent().inject(this);
        //Fragment fragment = new ExamDetailListFragment();
        Fragment fragment=new ExamDetailListFragment1();

        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();

        /*if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
            Bundle b = intent.getExtras();
            bundle.putSerializable("classlist", (Serializable) b.getSerializable("classlist"));
            bundle.putInt("termid",b.getInt("termid"));
            bundle.putString("exam_title",b.getString("exam_title"));
            fragment.setArguments(bundle);
            replaceFragment(fragment);
        } else */if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
            Bundle b = intent.getExtras();
            bundle.putInt("termid",b.getInt("termid"));
            bundle.putString("exam_title",b.getString("exam_title"));
            bundle.putInt("classid",b.getInt("classid"));
            bundle.putInt("sectionid", b.getInt("sectionid"));
            bundle.putInt("studIDD", b.getInt("studIDD"));
            bundle.putInt("selectedSchoolID", b.getInt("selectedSchoolID"));
            fragment.setArguments(bundle);
            replaceFragment(fragment);
        }



        //replaceFragment(fragment);

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_examm, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
    @Override
    public void onBackPressed() {
        Log.e("back_arrow pressed", "back_arrow pressed");
        FragmentManager manager = getSupportFragmentManager();
        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());
        if (manager.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

}
