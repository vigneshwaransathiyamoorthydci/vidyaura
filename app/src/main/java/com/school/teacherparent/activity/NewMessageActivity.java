package com.school.teacherparent.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.school.teacherparent.adapter.ChatAppMsgAdapter;

import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChatAppMsgDTO;
import com.school.teacherparent.R;
import com.school.teacherparent.models.ChatMessageParams;
import com.school.teacherparent.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;


public class NewMessageActivity extends Activity {

    private static final String LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif";
    private static final String MESSAGE_URL = "https://vidyaura-e4835.firebaseio.com/";
    private static final int REQUEST_IMAGE = 1;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private static int RESULT_LOAD_IMG = 1;
    public int PICK_CONTACT = 1;
    public EditText contact_edit;
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;
    public String RECENT_LIST_CHILD_SENDER, RECENT_LIST_CHILD_RECEIVER;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String ONE_TO_MANY_CHAT_MES_CHILD = "ONE_TO_MANY_CHAT_MESSAGES";
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    String imgDecodableString;
    ImageView imgBack;
    RecyclerView msgRecyclerView;
    EditText msgInputText;
    private ImageView img, contact, attach, search, back;
    private Intent chatTypeIntent;
    private String chatWith;
    private String chatWithUserID;
    private String senderUserName;
    private String senderUniqueID, senderRoleName;
    private String senderUserID;
    private DatabaseReference messagesRef;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseRecyclerAdapter<ChatMessageParams, ChatAppMsgViewHolder> mFirebaseAdapter;
    private String chatMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isForGroundExist, isTransparentModeOn;
    private String imageUrl;
    private String downloadChatImageUrl;
    private int GALLERY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        search = (ImageView) findViewById(R.id.search);
        back = (ImageView) findViewById(R.id.back);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        contact_edit = (EditText) findViewById(R.id.contact_edit);
        attach = (ImageView) findViewById(R.id.attach);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//
//        attach.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                // Start the Intent
//
//                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("*/*");
//                startActivityForResult(intent, 7);
//            }
//        });


        //   setTitle("dev2qa.com - Android Chat App Example");

        // Get RecyclerView object.
        msgRecyclerView = (RecyclerView) findViewById(R.id.new_message_recycle);
        contact = (ImageView) findViewById(R.id.contact);


        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        // Set RecyclerView layout manager.


        // Create the initial data list.
        final List<ChatAppMsgDTO> msgDtoList = new ArrayList<ChatAppMsgDTO>();
        ChatAppMsgDTO msgDto = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_RECEIVED, "hello");
        msgDtoList.add(msgDto);

        // Create the data adapter with above data list.
        final ChatAppMsgAdapter chatAppMsgAdapter = new ChatAppMsgAdapter(msgDtoList);

        // Set data adapter to RecyclerView.
        msgRecyclerView.setAdapter(chatAppMsgAdapter);
        mLinearLayoutManager=new LinearLayoutManager(this);

        msgInputText = (EditText) findViewById(R.id.edit);

        img = (ImageView) findViewById(R.id.send);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msgContent = msgInputText.getText().toString();
                if (!TextUtils.isEmpty(msgContent)) {
                    sendMessage();
                    // Add a new sent message to the list.
//                    ChatAppMsgDTO msgDto = new ChatAppMsgDTO(ChatAppMsgDTO.MSG_TYPE_SENT, msgContent);
//                    msgDtoList.add(msgDto);
//
//                    int newMsgPosition = msgDtoList.size() - 1;
//
//                    // Notify recycler view insert one new data.
//                    chatAppMsgAdapter.notifyItemInserted(newMsgPosition);
//
//                    // Scroll RecyclerView to the last message.
//                    msgRecyclerView.scrollToPosition(newMsgPosition);
//
//                    // Empty the input edit text box.
//                    msgInputText.setText("");
                }
            }
        });
        confiqFirebaseChat();



    }
    public void confiqFirebaseChat(){
        chatWith=getIntent().getStringExtra("receivename");
        chatWithUserID="dWY5a2xPaXE1Mm44VUpUVWlBRGdNUT09";
        contact_edit.setText("To: "+chatWith);
        contact_edit.setFocusable(false);
      //  chatWith = "Nagaraj";
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
     //   chatWithUserID = "3";
        senderUserID = sharedPreferences.getString(Constants.USERID, "");
        senderUserName = "dinesh";
        //My node
        MESSAGES_CHILD_SENDER = senderUserID + "to" + chatWithUserID;
        //Receiver node
        MESSAGES_CHILD_RECEIVER = chatWithUserID + "to" + senderUserID;
        //Inbox node
        RECENT_LIST_CHILD_SENDER = "" + senderUserID;
        //Sent node
        RECENT_LIST_CHILD_RECEIVER = "" + chatWithUserID;
        messagesRef = mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).
                child(MESSAGES_CHILD_SENDER);
        mFirebaseDatabaseReference.child("IS_FORE_GROUND").push().setValue(RECENT_LIST_CHILD_SENDER);
        SnapshotParser<ChatMessageParams> parser = new SnapshotParser<ChatMessageParams>() {
            @Override
            public ChatMessageParams parseSnapshot(DataSnapshot dataSnapshot) {
                ChatMessageParams chatMessageParams = dataSnapshot.getValue(ChatMessageParams.class);
                if (chatMessageParams != null) {
                    chatMessageParams.setId(dataSnapshot.getKey());
                }
                return chatMessageParams;
            }
        };

        FirebaseRecyclerOptions<ChatMessageParams> options =
                new FirebaseRecyclerOptions.Builder<ChatMessageParams>()
                        .setQuery(messagesRef, parser)
                        .build();

        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessageParams, ChatAppMsgViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ChatAppMsgViewHolder viewHolder,
                                            final int position, @NonNull ChatMessageParams chatMessageParams) {

                Gson gson = new Gson();
                String input = gson.toJson(chatMessageParams);
                Log.d("DataFcm", "onBindViewHolder: "+input);
               // Toast.makeText(NewMessageActivity.this, "called", Toast.LENGTH_SHORT).show();


                if (chatMessageParams.getText() != null) {
                    chatMessage = chatMessageParams.getText();
                } else if (chatMessageParams.getImageUrl() != null) {
                    imageUrl = chatMessageParams.getImageUrl();
                    if (imageUrl.startsWith("gs://")) {
                        StorageReference storageReference = FirebaseStorage.getInstance()
                                .getReferenceFromUrl(imageUrl);
                        storageReference.getDownloadUrl().addOnCompleteListener(
                                new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadChatImageUrl = task.getResult().toString();
                                        } else {
                                            Log.w("chat", "Getting download url was not successful.",
                                                    task.getException());
                                        }
                                    }
                                });
                    } else {
                        downloadChatImageUrl = imageUrl;
                    }
                }

                if (chatMessageParams.getName().equalsIgnoreCase(senderUserName)) {
                    viewHolder.card_left.setVisibility(View.GONE);
                    viewHolder.card_right.setVisibility(View.VISIBLE);
                    viewHolder.rightMsgTextView.setText(chatMessageParams.getName());
                    if (chatMessageParams.getText() != null) {
                        //viewHolder.messageImageViewRight.setVisibility(View.GONE);
                        viewHolder.rightMsgTextView.setVisibility(View.VISIBLE);
                        viewHolder.rightMsgTextView.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                      //  viewHolder.messageImageViewRight.setVisibility(View.VISIBLE);
                      //  viewHolder.textMesssgeRight.setVisibility(View.GONE);
//                        Glide.with(viewHolder.messageImageViewRight.getContext())
//                                .load(downloadChatImageUrl)
//                                .into(viewHolder.messageImageViewRight);
                    }
                } else {
                    viewHolder.card_left.setVisibility(View.VISIBLE);
                    viewHolder.card_right.setVisibility(View.GONE);
                    viewHolder.leftMsgTextView.setText(chatMessageParams.getToName());
                    if (chatMessageParams.getText() != null) {
                      //  viewHolder.messageImageViewLeft.setVisibility(View.GONE);
                        viewHolder.leftMsgTextView.setVisibility(View.VISIBLE);
                        viewHolder.leftMsgTextView.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                     //   viewHolder.messageImageViewLeft.setVisibility(View.VISIBLE);
                        viewHolder.leftMsgTextView.setVisibility(View.GONE);
//                        Glide.with(viewHolder.messageImageViewLeft.getContext())
//                                .load(downloadChatImageUrl)
//                                .into(viewHolder.messageImageViewLeft);
                    }

                }
                // log a view action on it
                FirebaseUserActions.getInstance().end(getMessageViewAction(chatMessageParams));
//                viewHolder.messageImageViewLeft.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatActivity.this);
//                        LayoutInflater inflater = getLayoutInflater();
//                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
//                        dialogBuilder.setView(dialogView);
//                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
//                        AlertDialog alertDialog = dialogBuilder.create();
//                        Glide.with(imageView.getContext())
//                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
//                                .into(imageView);
//                        alertDialog.show();
//                    }
//                });
//                viewHolder.messageImageViewRight.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatActivity.this);
//                        LayoutInflater inflater = getLayoutInflater();
//                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
//                        dialogBuilder.setView(dialogView);
//                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
//                        AlertDialog alertDialog = dialogBuilder.create();
//                        Glide.with(imageView.getContext())
//                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
//                                .into(imageView);
//                        alertDialog.show();
//                    }
//                });


            }

            @NonNull
            @Override
            public ChatAppMsgViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                //  showProgress();
                return new ChatAppMsgViewHolder(inflater.inflate(R.layout.newmessage_chat_item,
                        viewGroup, false));
            }
        };

        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    msgRecyclerView.scrollToPosition(positionStart);
                }
            }
        });
        msgRecyclerView.setLayoutManager(mLinearLayoutManager);
        msgRecyclerView.setAdapter(mFirebaseAdapter);
    }
    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAdapter.startListening();
    }
    @Override
    protected void onPause() {
        mFirebaseAdapter.stopListening();
        mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot1) {
                        for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                            if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_SENDER)) {
                                dataSnapshot2.getRef().removeValue();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        super.onPause();
    }
    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        // TODO Fetch other Contact details as you want to use
                        contact_edit.setText("" + name);
                    }
                }
                break;
        }
    }
    public String getCurrentTimeandDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentTimeandTime = simpleDateFormat.format(currentDate);
        return currentTimeandTime;
    }

    private void sendMessage() {

        /*dWY5a2xPaXE1Mm44VUpUVWlBRGdNUT09*/

        if (msgInputText.getText() != null && msgInputText.getText().toString().length() > 0) {
            String senderfcm=sharedPreferences.getString(Constants.FCMTOKEN,"");
            String receiverfcm="wfwf";
            //One to One
            //Chat list
            ChatMessageParams chatMessageParams = new
                    ChatMessageParams(senderUserID, msgInputText.getText().toString(), senderUserName,
                    "", null, getCurrentTimeandDate(), "Karthick", "12",
                    "", "",senderfcm
                    , "", "", receiverfcm, "", 0);
            //My node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push().
                    setValue(chatMessageParams);
            //Receiver node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push().
                    setValue(chatMessageParams);

            //notification
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Notifications").push()
                    .setValue(chatMessageParams);

            //Inbox list
            final ChatMessageParams chatMessageParams3 = new
                    ChatMessageParams(senderUserID, msgInputText.getText().toString().trim(), senderUserName,
                    "",
                    null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", "", senderfcm,
                    "", "", receiverfcm, "", 0);
            final ChatMessageParams chatMessageParams4 = new
                    ChatMessageParams(chatWithUserID, msgInputText.getText().toString().trim(), chatWith,
                    "",
                    null, getCurrentTimeandDate(), senderUserName, senderUserID,
                    "", "", senderfcm,
                    "", "", receiverfcm, "", 0);


            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {

                            //Old User
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                    child(RECENT_LIST_CHILD_RECEIVER)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshot) {
                                            //Old User
                                            for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                if (chatMessageParams3.getUniqueID().
                                                        equals(dataSnapshot.child("uniqueID").
                                                                getValue())) {
                                                    //Check chat with user is foreground or background
                                                    mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                            addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                    for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                        if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                            isForGroundExist = true;
                                                                        } else {
                                                                            isForGroundExist = false;
                                                                        }
                                                                        break;
                                                                    }
                                                                    if (!isForGroundExist) {
                                                                        dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });
                                                    break;

                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                            //                                        New
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_SENDER).
                                    child(RECENT_LIST_CHILD_RECEIVER).
                                    setValue(chatMessageParams3);
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_RECEIVER).
                                    child(RECENT_LIST_CHILD_SENDER).
                                    setValue(chatMessageParams4);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


        } else {
            msgInputText.setError("empty text");
        }
        msgInputText.setText("");
    }

    private Action getMessageViewAction(ChatMessageParams chatMessageParams) {
        return new Action.Builder(Action.Builder.VIEW_ACTION)
                .setObject(chatMessageParams.getName(), MESSAGE_URL.concat(chatMessageParams.getId()))
                .setMetadata(new Action.Metadata.Builder().setUpload(false))
                .build();
    }

    public class ChatAppMsgViewHolder extends RecyclerView.ViewHolder {

        LinearLayout leftMsgLayout;
        LinearLayout rightMsgLayout;
        TextView leftMsgTextView;
        TextView rightMsgTextView;
        private CardView card_left, card_right;

        public ChatAppMsgViewHolder(View itemView) {
            super(itemView);

            if (itemView != null) {

                card_left = (CardView) itemView.findViewById(R.id.card_left);
                card_right = (CardView) itemView.findViewById(R.id.card_right);
                leftMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_left_msg_layout);
                rightMsgLayout = (LinearLayout) itemView.findViewById(R.id.chat_right_msg_layout);
                leftMsgTextView = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
                rightMsgTextView = (TextView) itemView.findViewById(R.id.chat_right_msg_text_view);

                card_left.setCardBackgroundColor(Color.parseColor("#ffffff"));
                card_right.setCardBackgroundColor(Color.parseColor("#F3ECE7"));
            }
        }
    }
}
