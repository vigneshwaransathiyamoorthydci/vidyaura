package com.school.teacherparent.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.adapter.CircularAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.CircularList;
import com.school.teacherparent.models.CircularListResponse;
import com.school.teacherparent.R;
import com.school.teacherparent.models.FeedCommentreplyListResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.util.ArrayList;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CircularActivity extends BaseActivity {

    private RecyclerView recycle;
    private ArrayList<CircularListResponse> circularlist = new ArrayList<>();
    private CircularAdapter circularAdapter;
    private ImageView back,noti;
    private ImageView menuu;
    private View v;
    private Context context;
    private PopupWindow pwindo;
    private int mViewSpacingLeft;
    private int mViewSpacingTop;
    private int mViewSpacingRight;
    private int mViewSpacingBottom;
    private boolean mViewSpacingSpecified = false;
    FloatingActionButton addcircular;
    SwipeRefreshLayout circularswipelayout;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private boolean loading = true;
    int limit=5;
    int offset=0;
    TextView nocircularListTextview;
    LinearLayoutManager linearLayoutManager;
    ArrayList<CircularListResponse.circularList> circularListResponseArrayList;
    CircularListResponse circularListResponse;
    ArrayList<CircularList> circularLists;
        boolean isLoading=true;
    NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular);


        recycle = (RecyclerView) findViewById(R.id.recycle_circular);
        circularlist = new ArrayList<>();
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        back = (ImageView) findViewById(R.id.back);
        addcircular = findViewById(R.id.fab);
        nocircularListTextview=(TextView)findViewById(R.id.nocircularListTextview);
        nocircularListTextview.setVisibility(View.INVISIBLE);
        recycle.setVisibility(View.INVISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        circularListResponseArrayList=new ArrayList<>();
        circularLists=new ArrayList<>();
        noti=findViewById(R.id.noti);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        circularAdapter = new CircularAdapter(circularLists, this);
        recycle.setAdapter(circularAdapter);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setLayoutManager(linearLayoutManager);
        //getcircularList();

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        circularswipelayout=findViewById(R.id.swipe_circular);
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CircularActivity.this,NotificationActivity.class));
            }
        });
        circularswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                circularswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        circularLists.clear();
                        circularAdapter.notifyDataSetChanged();
                        offset=0;
                        limit=5;
                        getcircularList();
                        circularswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });

        recycle.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getcircularList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

        addcircular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CircularActivity.this, SidemenuDetailActivity.class).putExtra("type","circular").putExtra("circuId", 0));
            }
        });

        recycle.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addcircular.setVisibility(View.VISIBLE);
                }
                else
                {
                    addcircular.setVisibility(View.GONE);
                }
            }
        });
    }

    public void getcircularList()
    {
        isLoading=true;
        if (Util.isNetworkAvailable()) {
            showProgress();
            vidyauraAPI.getcircularByfield(sharedPreferences.getString(Constants.USERID,""),String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)),
                    String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)),
                   limit,offset).enqueue(new Callback<CircularListResponse>() {
                @Override
                public void onResponse(Call<CircularListResponse> call, Response<CircularListResponse> response) {
                    hideProgress();
                    if (response.body()!=null)
                    {
                        circularListResponse=response.body();
                        if (circularListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE)
                        {
                            circularListResponseArrayList = response.body().getCircularList();
                            if (circularListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (circularListResponseArrayList.size()!=0) {
                                    recycle.setVisibility(View.VISIBLE);
                                    nocircularListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<circularListResponseArrayList.size();y++)
                                    {
                                        circularLists.add(new CircularList(circularListResponseArrayList.get(y).getSchoolID(),
                                                circularListResponseArrayList.get(y).getSchoolName(),
                                                circularListResponseArrayList.get(y).getSchoolLogo(),circularListResponseArrayList.get(y).getId(),
                                                circularListResponseArrayList.get(y).getCircular_no(),
                                                circularListResponseArrayList.get(y).getTitle(),circularListResponseArrayList.get(y).getDescription(),
                                                circularListResponseArrayList.get(y).getClassroom_id(),
                                                circularListResponseArrayList.get(y).getAuthor_type(),circularListResponseArrayList.get(y).getCreated_by(),
                                                circularListResponseArrayList.get(y).getIsUserPostedCircular(),
                                                circularListResponseArrayList.get(y).getCreated_at(),circularListResponseArrayList.get(y).getAttachmentsList()));
                                    }

                                    circularAdapter.notifyDataSetChanged();
                                    loading=true;
                                }
                                else if (circularListResponseArrayList.size()!=0)
                                {
                                    nocircularListTextview.setVisibility(View.VISIBLE);
                                    recycle.setVisibility(View.GONE);

                                    loading=false;
                                }
                            } else {
                                loading=false;
                                //Toast.makeText(CircularActivity.this, circularListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else 
                        {
                            Intent i = new Intent(getApplicationContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(CircularActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CircularListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(CircularActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;
                }
            });


        }
        else
        {
            Toast.makeText(CircularActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        circularLists.clear();
        circularAdapter.notifyDataSetChanged();
        getcircularList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        circularLists.clear();
        circularAdapter.notifyDataSetChanged();

    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
