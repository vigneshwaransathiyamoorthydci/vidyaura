package com.school.teacherparent.activity;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.school.teacherparent.adapter.EventsAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.EventList;
import com.school.teacherparent.models.EventListParms;
import com.school.teacherparent.models.EventListResponse;
import com.school.teacherparent.R;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 9/12/2018.
 */

public class EventListActivity extends BaseActivity {
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
     RecyclerView eventrecycle;
     EventsAdapter eventAdapter;
    ImageView back,noti;
    FloatingActionButton addcircular;
    private ArrayList<EventList> eventList = new ArrayList<>();
    SwipeRefreshLayout eventlistswipelayout;
    int limit=5;
    int offset=0;
    EventListResponse eventListResponse;
    public ArrayList<EventListResponse.eventsList> eventsListArrayList;
    ArrayList<EventList> eventLists= new ArrayList();
    TextView noeventListTextview;
    LinearLayoutManager linearLayoutManager;
    boolean loading=true;
    ImageView calendarImageview;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String formattedDate;
    TextView selectedDateTextview;
    int mode=0;
    NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        eventrecycle = (RecyclerView) findViewById(R.id.recycle_events);

        back = findViewById(R.id.back);
        noti = findViewById(R.id.noti);
        eventList = new ArrayList<>();
        addcircular = findViewById(R.id.fab);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            noti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        eventsListArrayList=new ArrayList<>();
        calendarImageview=findViewById(R.id.calendarImageview);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(EventListActivity.this,NotificationActivity.class));
            }
        });
        noeventListTextview=findViewById(R.id.noeventListTextview);
        selectedDateTextview=findViewById(R.id.selectedDateTextview);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        eventrecycle.setItemAnimator(new DefaultItemAnimator());
        eventrecycle.setLayoutManager(linearLayoutManager);
        eventrecycle.setHasFixedSize(true);
        eventAdapter = new EventsAdapter(eventLists, getApplicationContext(),EventListActivity.this,EventListActivity.this);
        eventrecycle.setAdapter(eventAdapter);
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(date);
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        eventlistswipelayout=findViewById(R.id.swipe_eventlist);
        selectedDateTextview.setText(getString(R.string.today));
        selectedDateTextview.setVisibility(View.GONE);
        eventlistswipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                eventlistswipelayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Date c = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        formattedDate = df.format(c);
                        selectedDateTextview.setText(getString(R.string.today));
                        selectedDateTextview.setVisibility(View.GONE);
                            offset=0;
                            limit=5;
                        //eventsListArrayList.clear();
                        eventList.clear();
                        eventLists.clear();
                        eventAdapter.notifyDataSetChanged();
                        mode=0;
                            getEventList(mode);
                            eventlistswipelayout.setRefreshing(false);

                    }
                }, 000);
            }
        });
        eventrecycle.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            offset=offset+limit;

                            getEventList(mode);

                        }
                    }
                }
            }
        });
        addcircular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EventListActivity.this,
                        SidemenuDetailActivity.class).putExtra("type","events").putExtra("eventID",0));
            }
        });



        eventrecycle.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0)
                {
                    addcircular.setVisibility(View.VISIBLE);
                }
                else
                {
                    addcircular.setVisibility(View.GONE);
                }
            }
        });

        calendarImageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                String selected[] = formattedDate.split("-");
                mYear = Integer.parseInt(selected[0]);
                mMonth = Integer.parseInt(selected[1])-1;
                mDay = Integer.parseInt(selected[2]);

                DatePickerDialog datePickerDialog = new DatePickerDialog(EventListActivity.this,
                        new DatePickerDialog.OnDateSetListener() {


                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
                                String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                try {
                                    dateObj = curFormater.parse(a);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat formt = new SimpleDateFormat("yyyy-MM-dd");

                                selectedDateTextview.setVisibility(View.VISIBLE);

                                Date c = Calendar.getInstance().getTime();


                                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                formattedDate = df.format(c);
                                formattedDate=formt.format(dateObj);
                                // dateTextview.setText(formattedDate);
                                //selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                //eventsListArrayList.clear();
                                eventList.clear();
                                eventLists.clear();
                                eventAdapter.notifyDataSetChanged();
                                String today = formt.format(c);
                                offset=0;
                                if (formattedDate.equals(today)) {
                                    selectedDateTextview.setText("Today");
                                    mode=1;
                                    getEventList(mode);
                                } else {
                                    selectedDateTextview.setText(getDateFormatforatten(formattedDate));
                                    mode=1;
                                    getEventList(mode);
                                }





                            }

                        }, mYear, mMonth, mDay);
                datePickerDialog.show();










            }
        });
    }
    private void getEventList(int mode) {
        if (Util.isNetworkAvailable()) {
            loading=false;
            showProgress();
            EventListParms eventListParms=new EventListParms();
            eventListParms.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID,0)));
            eventListParms.setUserID(sharedPreferences.getString(Constants.USERID,""));
            eventListParms.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            eventListParms.setLimit(limit);
            eventListParms.setOffset(offset);
            eventListParms.setEventByDate(formattedDate);
            eventListParms.setIsDateSearch(mode);
            vidyauraAPI.getEventList(eventListParms).enqueue(new Callback<EventListResponse>() {
                @Override
                public void onResponse(Call<EventListResponse> call, Response<EventListResponse> response) {
                    hideProgress();

                    if (response.body() != null) {
                        eventListResponse = response.body();
                        Gson gson = new Gson();
                        System.out.println("response ==> "+gson.toJson(response.body()));
                        if (eventListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                           // eventsListArrayList.clear();
                            eventsListArrayList = response.body().getEventsList();
                            if (eventListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (eventsListArrayList.size()!=0) {
                                    eventrecycle.setVisibility(View.VISIBLE);
                                    noeventListTextview.setVisibility(View.GONE);
                                    for (int y=0;y<eventsListArrayList.size();y++)
                                    {
                                        ArrayList<String> classNamelist=new ArrayList<>();
                                        if (eventsListArrayList.get(y).getAuthor_type().equals("Admin"))
                                        {
                                            classNamelist.clear();
                                            for (int i=0;i<eventsListArrayList.get(y).getClassName().size();i++)
                                            {
                                                classNamelist.add(eventsListArrayList.get(y).getClassName().get(i).getClassName());
                                            }
                                            eventLists.add(new EventList(eventsListArrayList.get(y).getEventID(),eventsListArrayList.get(y).getId(),eventsListArrayList.get(y).getRSVP(),eventsListArrayList.get(y).getTitle(),
                                                    eventsListArrayList.get(y).getDescription(),eventsListArrayList.get(y).getEvent_date(),eventsListArrayList.get(y).getVenue(),eventsListArrayList.get(y).getClass_id(),
                                                    eventsListArrayList.get(y).getAdminName(),eventsListArrayList.get(y).getEventAcceptedCount(),eventsListArrayList.get(y).getEventMaybeCount(),
                                                    eventsListArrayList.get(y).getEventDeclinedCount(),eventsListArrayList.get(y).getNotResponded(),eventsListArrayList.get(y).getIsUserPostedEvent(),
                                                    eventsListArrayList.get(y).getUserDetails(),eventsListArrayList.get(y).getClassName(),classNamelist));

                                        }
                                        else
                                        {
                                            for (int i=0;i<eventsListArrayList.get(y).getClassName().size();i++)
                                            {
                                                classNamelist.add(eventsListArrayList.get(y).getClassName().get(i).getClassName());
                                            }
                                            eventLists.add(new EventList(eventsListArrayList.get(y).getEventID(),eventsListArrayList.get(y).getId(),eventsListArrayList.get(y).getRSVP(),eventsListArrayList.get(y).getTitle(),
                                                    eventsListArrayList.get(y).getDescription(),eventsListArrayList.get(y).getEvent_date(),eventsListArrayList.get(y).getVenue(),eventsListArrayList.get(y).getClass_id(),
                                                    eventsListArrayList.get(y).getAdminName(),eventsListArrayList.get(y).getEventAcceptedCount(),eventsListArrayList.get(y).getEventMaybeCount(),
                                                    eventsListArrayList.get(y).getEventDeclinedCount(),eventsListArrayList.get(y).getNotResponded(),eventsListArrayList.get(y).getIsUserPostedEvent(),
                                                    eventsListArrayList.get(y).getUserDetails(),eventsListArrayList.get(y).getClassName(),classNamelist));
                                        }

                                    }

//
                                    //eventAdapter = new EventsAdapter(eventsListArrayList,EventListActivity.this,EventListActivity.this,EventListActivity.this);
//
//
                                   /// eventrecycle.setAdapter(eventAdapter);
                                    if (eventLists.size() > 0) {
                                        eventAdapter.notifyDataSetChanged();
                                        loading = true;
                                        eventrecycle.setVisibility(View.VISIBLE);
                                        noeventListTextview.setVisibility(View.GONE);
                                    }
                                }

                                else if (eventLists.size()!=0 && eventsListArrayList.size()!=0)
                                {
                                    noeventListTextview.setVisibility(View.VISIBLE);
                                    eventrecycle.setVisibility(View.GONE);
                                    loading=false;
                                }
                                else {
                                    loading=false;
                                    if (eventLists.size()==0)
                                    {
                                        Toast.makeText(EventListActivity.this, eventListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                            else {
                                loading=false;
                                if (eventLists.size()==0)
                                {
                                    Toast.makeText(EventListActivity.this, eventListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }
                        }
                        else
                        {
                            loading=false;
                            Intent i = new Intent(EventListActivity.this, SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();

                        }

                    }
                    else
                    {
                        loading=false;
                        Toast.makeText(EventListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<EventListResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(EventListActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(EventListActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (loading) {
            eventLists.clear();
            eventAdapter.notifyDataSetChanged();
            selectedDateTextview.setVisibility(View.GONE);
            getEventList(mode);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (loading) {
            eventLists.clear();
            eventAdapter.notifyDataSetChanged();
            selectedDateTextview.setVisibility(View.GONE);
            getEventList(mode);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

//        offset=0;
//        limit=5;
//        eventsListArrayList.clear();
//        eventList.clear();
//        eventLists.clear();
        eventLists.clear();
        loading=true;
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                noti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
