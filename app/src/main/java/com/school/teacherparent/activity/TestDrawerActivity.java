package com.school.teacherparent.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.util.IOUtils;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.Interface.OnAmazonFileuploaded;
import com.school.teacherparent.adapter.DrawerAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.AddCircularFragment;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.fragment.AddGalleryFragment;
import com.school.teacherparent.fragment.AddHomeWorkTabFragment;
import com.school.teacherparent.fragment.AddNewFeedFragment;
import com.school.teacherparent.fragment.ChatWithParentListFragment;
import com.school.teacherparent.fragment.DiaryFragment;
import com.school.teacherparent.fragment.FeedsFragment;
import com.school.teacherparent.fragment.MenuFragment;
import com.school.teacherparent.fragment.MessageFragment;
import com.school.teacherparent.fragment.ParentDiaryFragment;
import com.school.teacherparent.fragment.ProfileFragment;
import com.school.teacherparent.R;
import com.school.teacherparent.fragment.ProfileforStudentsFragment;
import com.school.teacherparent.fragment.UpdateClassWorkFragment;
import com.school.teacherparent.fragment.UpdateHomeAssignmentFragment;
import com.school.teacherparent.fragment.UpdateHomeWorkFragment;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.SelectedImageList;
import com.school.teacherparent.models.UserLoginParam;
import com.school.teacherparent.models.UserLoginResponse;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.AppPreferences;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.squareup.picasso.Picasso;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import cn.jzvd.JzvdStd;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by harini on 8/22/2018.
 */

public class TestDrawerActivity extends BaseActivity implements AHBottomNavigation.OnTabSelectedListener {
    String sidemenu[];
    String sidemenuParent[];
    private DrawerLayout drawer;
    ListView listView;
    public static Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    public static AHBottomNavigation bottomNavigationView;
    FabSpeedDial fabSpeedDial;
    public static ImageView userImageview;
    Fragment fragment;
    boolean doubleBackToExitPressedOnce = false;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    TextView userNametextview,teachersubjectTextview;
    ImageView switch_domain,beginnerImageview;
    LinearLayout profileLinearlayout;
    public  static EditText edittextleaveSearch;
    int usetType;
    LinearLayout activityLinearlayout;
    TextView pointTextview;
    DrawerAdapter adapter1;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;
    UserLoginResponse userLoginResponse;
    //NotificationReceiver notificationReceiver;
    //Menu menu;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_drawer);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();
        sidemenu = getResources().getStringArray(R.array.system);
        sidemenuParent = getResources().getStringArray(R.array.system_parent);
        listView = (ListView) findViewById(R.id.navdrawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        userImageview = findViewById(R.id.userImageview);
        userNametextview=findViewById(R.id.teachernameTextview);
        pointTextview=findViewById(R.id.pointTextview);
        teachersubjectTextview=findViewById(R.id.teachersubjectTextview);
        userNametextview.setText(sharedPreferences.getString(Constants.FNAME,"")+" "+sharedPreferences.getString(Constants.LNAME,""));
        bottomNavigationView = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnTabSelectedListener(this);
        bottomNavigationView.setAccentColor(getResources().getColor(R.color.colorPrimary));
        switch_domain=findViewById(R.id.switch_domain);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        profileLinearlayout=findViewById(R.id.profileLinearlayout);
        edittextleaveSearch=(EditText)findViewById(R.id.edittextleaveSearch);
        activityLinearlayout=(LinearLayout)findViewById(R.id.activityLinearlayout);
        pointTextview.setText(sharedPreferences.getString(Constants.POINTS_NAME,""));
        beginnerImageview=findViewById(R.id.beginnerImageview);

        /*notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        logout();

        if (sharedPreferences.getInt(Constants.USERTYPE_REAL,0)==3)
        {
            switch_domain.setVisibility(View.VISIBLE);
        }
        else
        {
            switch_domain.setVisibility(View.GONE);
        }
        switch_domain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (usetType==1)
                {
                    usetType=2;
                    editor.putInt(Constants.USERTYPE, usetType);
                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, secureTokenSharedPreferences.getString(Constants.SECURE_TOKEN_PAREENTS,""));
                    editor.commit();
                    secureTokenSharedPreferenceseditor.commit();
                }
                else if (usetType==2)
                {
                    usetType=1;
                    editor.putInt(Constants.USERTYPE, usetType);
                    secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, secureTokenSharedPreferences.getString(Constants.SECURE_TOKEN_TEACHER,""));
                    editor.commit();
                    secureTokenSharedPreferenceseditor.commit();

                }
                userLogin();
                drawer.closeDrawers();
            }
        });
        if (sharedPreferences.getString(Constants.BADGE_IMAGE,"")!=null)
        {
            String url = getString(R.string.s3_baseurl) + getString(R.string.s3_badge_path) + "/" +
                    sharedPreferences.getString(Constants.BADGE_IMAGE, "");


            //picassoImageHolder(holder.feed_image,context.getString(R.string.s3_feeds_path),"/"+fData.getAttachmentsList().get(0));
            Picasso.get().load(url).


                    into(beginnerImageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {


                        }

                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onError(Exception e) {
                            beginnerImageview.setImageDrawable(getDrawable(R.mipmap.beginner_level));
                        }
                    });
        }
        else
        {
            beginnerImageview.setImageResource(R.mipmap.beginner_level);
        }

        usetType=sharedPreferences.getInt(Constants.USERTYPE,0);
        if (usetType==1)
        {
            teachersubjectTextview.setText(getString(R.string.teacher));
            activityLinearlayout.setVisibility(View.VISIBLE);
        }
        else if (usetType==2)
        {
            String myString = sharedPreferences.getString(Constants.TYPE,"");
            if (myString.length()>0) {
                String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1);
                teachersubjectTextview.setText(getString(R.string.parent) + " ( " + upperString + " )");
            }
            activityLinearlayout.setVisibility(View.INVISIBLE);
        }
        edittextleaveSearch.setInputType(0);
        edittextleaveSearch.setEnabled(false);
        if (sharedPreferences.getInt(Constants.OPEN_DRAWER,0)==1)
        {
            drawer.openDrawer(Gravity.START);
            editor.putInt(Constants.OPEN_DRAWER, 0);
            editor.commit();
        }
        if (sharedPreferences.getString(Constants.PROFILE_PHOTO,"")!=null) {


            String url = null;
            if (usetType==1) {
                url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" +
                        sharedPreferences.getString(Constants.PROFILE_PHOTO, "");
            }
            else if (usetType==2)
            {
                url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" +
                        sharedPreferences.getString(Constants.PROFILE_PHOTO, "");
            }

            Picasso.get().load(url).


                    into(userImageview, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {


                        }

                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onError(Exception e) {
                            userImageview.setImageDrawable(getDrawable(R.drawable.ic_user));
                        }
                    });
        }
        else
        {
            userImageview.setImageDrawable(getDrawable(R.drawable.ic_user));
        }
       /* drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);
*/

        //  mDrawerList = (ListView) findViewById(R.id.navdrawer);

        profileLinearlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                bottomNavigationView.setCurrentItem(AHBottomNavigation.CURRENT_ITEM_NONE);
                if (usetType == 1) {
                    Fragment fragment2 = new ProfileFragment();
                    replaceFragment(fragment2);
                } else if (usetType == 2) {
                    Fragment fragment2 = new ProfileforStudentsFragment();
                    replaceFragment(fragment2);
                }

            }
        });
//        profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        createitems();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentManager fragmentManager = getFragmentManager();
                TextView text = view.findViewById(R.id.text);
                String name = text.getText().toString().trim();

                switch (name) {
                    case "School":
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","school"));
                      /*  Fragment fragment = new HomePageFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        title.setText(getResources().getString(R.string.app_name));*/

                        break;

                    case "Time Table":
                        drawer.closeDrawers();
                        if (usetType == 1) {
                            startActivity(new Intent(TestDrawerActivity.this, SidemenuDetailActivity.class).putExtra("type", "timetable"));
                        } else if (usetType == 2) {
                            startActivity(new Intent(TestDrawerActivity.this, SidemenuDetailActivity.class).putExtra("type", "timetableparent"));
                        }
                       /* Fragment fragment1 = new TimetableFragment();
                        replaceFragment(fragment1);*/

                        break;
                    case "Gallery":
                        drawer.closeDrawers();
                        if (usetType == 1) {
                            startActivity(new Intent(TestDrawerActivity.this, SidemenuDetailActivity.class).putExtra("type", "gallery"));
                        } else if (usetType == 2) {
                            startActivity(new Intent(TestDrawerActivity.this, SidemenuDetailActivity.class).putExtra("type", "parentgallery"));
                        }
                       /* Fragment fragmentt = new RecentActivities();
                        replaceFragment(fragmentt);*/
                        break;
                    case "Fees":
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,PaymentActivity.class));
                        //startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","fees"));
                        //Toast.makeText(getApplicationContext(),"Option Not available right now..",Toast.LENGTH_LONG).show();
                        break;
                    case "Polls":
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","polls"));
                        //Toast.makeText(getApplicationContext(),"Option Not available right now..",Toast.LENGTH_LONG).show();
                        break;
                    case "Settings":
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","settings"));
                       /* Fragment fragment2 = new SettingFragment();
                        replaceFragment(fragment2);*/
                        break;
                    case "Help":
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","help"));
                       /* Fragment result = new HelpFragment();
                        replaceFragment(result);*/
                        break;

                }

                /*switch (position) {
                    case 0:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","school"));
                      *//*  Fragment fragment = new HomePageFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        title.setText(getResources().getString(R.string.app_name));*//*

                        break;

                    case 1:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","timetable"));
                       *//* Fragment fragment1 = new TimetableFragment();
                        replaceFragment(fragment1);*//*

                        break;
                    case 2:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","gallery"));
                       *//* Fragment fragmentt = new RecentActivities();
                        replaceFragment(fragmentt);*//*
                        break;
                    case 3:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","settings"));
                       *//* Fragment fragment2 = new SettingFragment();
                        replaceFragment(fragment2);*//*
                        break;
                    case 4:
                        drawer.closeDrawers();
                        startActivity(new Intent(TestDrawerActivity.this,SidemenuDetailActivity.class).putExtra("type","help"));
                       *//* Fragment result = new HelpFragment();
                        replaceFragment(result);*//*
                        break;

                }*/
            }
        });

        Integer[] imgs = new Integer[]
                {
                        R.mipmap.school,
                        R.mipmap.timetable,
                        R.mipmap.gallery,
                        R.mipmap.settings,
                        R.mipmap.help,
                };

        Integer[] imgsParent = new Integer[]
                {
                        R.mipmap.school,
                        R.mipmap.timetable,
                        R.mipmap.gallery,
                        R.mipmap.fees,
                        R.mipmap.polls,
                        R.mipmap.settings,
                        R.mipmap.help,
                };

        if (usetType == 1) {
            adapter1 = new DrawerAdapter(TestDrawerActivity.this, sidemenu,imgs);
        } else if (usetType == 2) {
            adapter1 = new DrawerAdapter(TestDrawerActivity.this, sidemenuParent,imgsParent);
        }
        listView.setAdapter(adapter1);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawerToggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);

     /*   if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.mipmap.noti_white_icon);
            toolbar.setTitleTextColor(getResources().getColor(R.color.abc_primary_text_material_dark));
            toolbar.setTitle("Edukool Parent");
        }*/

    }

    private void createitems() {

        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.feed), R.mipmap.feed_icon);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.diary), R.mipmap.diary_icon);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.message), R.mipmap.message_icon);
        AHBottomNavigationItem settingitem = new AHBottomNavigationItem(getString(R.string.menu), R.mipmap.menu_icon);

        //      AHBottomNavigationItem dashItem=new AHBottomNavigationItem(getString(R.string.dashboard),R.drawable.dashboard_icon);
        //     AHBottomNavigationItem contactItem=new
        bottomNavigationView.addItem(homeitem);
        bottomNavigationView.addItem(theatreitem);
        bottomNavigationView.addItem(searchitem);
        bottomNavigationView.addItem(settingitem);

        bottomNavigationView.setCurrentItem(0);

        bottomNavigationView.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigationView.setTranslucentNavigationEnabled(true);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.feed));
        }
        //centerToolbarTitle(toolbar);
    }


    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        TestDrawerActivity.edittextleaveSearch.setText("");
        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
        if (position == 0) {
            fragment = new FeedsFragment();
            replaceFragment(fragment);
        } else if (position == 1) {
            if (usetType==1) {
                fragment = new DiaryFragment();
                replaceFragment(fragment);
            }
            else if (usetType==2)
            {
                fragment = new ParentDiaryFragment();
                replaceFragment(fragment);
            }

        } else if (position == 2) {
            if (usetType == 1) {
                fragment = new MessageFragment();
                replaceFragment(fragment);
            } else if (usetType == 2) {
                fragment = new MessageFragment();
                replaceFragment(fragment);
            }
           //Toast.makeText(getApplicationContext(),"Option Not available right now..",Toast.LENGTH_LONG).show();
        } else if (position == 3) {
            fragment = new MenuFragment();
            replaceFragment(fragment);

        }
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

   /* @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
*/

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main, fragment, "");
            fragmentTransaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //this.menu = menu;

        return true;
    }
    public void showKeyboard(final EditText ettext){
        ettext.requestFocus();
        ettext.postDelayed(new Runnable(){
                               @Override public void run(){
                                   InputMethodManager keyboard=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext,0);
                               }
                           }
                ,200);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_notifi:

                startActivity(new Intent(TestDrawerActivity.this, NotificationActivity.class));

                case R.id.action_notification:

                startActivity(new Intent(TestDrawerActivity.this, NotificationActivity.class));

//                    edittextleaveSearch.setInputType(InputType.TYPE_CLASS_TEXT);
//                    edittextleaveSearch.setEnabled(true);
//                    edittextleaveSearch.requestFocus();
//                    showKeyboard(edittextleaveSearch);
//                    edittextleaveSearch.setText("");

                return true;
//            case R.id.action_search:
//                //startActivity(new Intent(TestDrawerActivity.this, NotificationActivity.class));
//                //Toast.makeText(getApplicationContext(),"Search",Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void centerToolbarTitle(final Toolbar toolbar) {
        final CharSequence title = toolbar.getTitle();
        final ArrayList<View> outViews = new ArrayList<>(1);
        toolbar.findViewsWithText(outViews, title, View.FIND_VIEWS_WITH_TEXT);
        if (!outViews.isEmpty()) {
            final TextView titleView = (TextView) outViews.get(0);
            titleView.setGravity(Gravity.CENTER_HORIZONTAL);
            final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) titleView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.setMargins(0, 0, 60, 0);
            toolbar.requestLayout();
        }
    }
    @Override
    public void onBackPressed() {
        JzvdStd.releaseAllVideos();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (TestDrawerActivity.edittextleaveSearch.getVisibility() == View.VISIBLE) {
                if (fragment instanceof FeedsFragment) {
                    TestDrawerActivity.edittextleaveSearch.setText("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                    TestDrawerActivity.toolbar.setTitle(getString(R.string.feed));
                } else if (fragment instanceof DiaryFragment) {
                    TestDrawerActivity.edittextleaveSearch.setText("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                    TestDrawerActivity.toolbar.setTitle(getString(R.string.diary));
                } else if (fragment instanceof ParentDiaryFragment) {
                    TestDrawerActivity.edittextleaveSearch.setText("");
                    TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                    TestDrawerActivity.toolbar.setTitle(getString(R.string.diary));
                } else if (fragment instanceof MessageFragment) {
                    int state = sharedPreferences.getInt(Constants.STATE,0);
                    System.out.println("click state ==> "+state);
                    if (state == 1) {
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        TestDrawerActivity.toolbar.setTitle(getString(R.string.message));
                    } else if (state == 2) {
                        TestDrawerActivity.edittextleaveSearch.setText("");
                        TestDrawerActivity.edittextleaveSearch.setVisibility(View.GONE);
                        TestDrawerActivity.toolbar.setTitle(getString(R.string.contact_list));
                    }
                }
            } else {
                if (fragment instanceof FeedsFragment) {
                    finish();
                } else {
                    fragment = new FeedsFragment();
                    bottomNavigationView.setCurrentItem(0);
                }
            }
//
//                if (bottomNavigationView.getCurrentItem()!=0)
//                {
//                    bottomNavigationView.setCurrentItem(0);
//                    fragment = new FeedsFragment();
//                    replaceFragment(fragment);
//                }
//                else
//                {
//                    finish();
//                }

            }
            //super.onBackPressed();
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//          //Toast.makeText(TestDrawerActivity.this, "called" , Toast.LENGTH_SHORT).show();
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//             //Toast.makeText(TestDrawerActivity.this, ""+mPaths.size(), Toast.LENGTH_SHORT).show();
//            //Your Code
//
//
//                fragment=new ProfileFragment();
//                if (fragment instanceof ProfileFragment){
//                    ((ProfileFragment) fragment).setPickedImageDetails(null,mPaths);
//                }
//                else
//                {
//                    Log.d("TTTT","path"+mPaths.get(0));
//                }
//
//
//
//
//            }
//
//        }
private void createFile(Context context, Uri srcUri, File dstFile) {
    try {
        InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
        if (inputStream == null) return;
        OutputStream outputStream = new FileOutputStream(dstFile);
        IOUtils.copy(inputStream, outputStream);
        inputStream.close();
        outputStream.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
}

    public void uploadFile(List<SelectedImageList>  path, String s3bucketPath, final String imageName,
                           final OnAmazonFileuploaded onAmazonFileuploaded) {


        // Toast.makeText(BaseActivity.this, "method called", Toast.LENGTH_SHORT).show();
        // showProgress();

        mDialog.setMessage("Uploading image");
        mDialog.show();
        for (int i = 0; path.size() > i; i++) {
            if (path.get(i).getisIsedit()) {
                Uri uri = Uri.fromFile(new File(path.get(i).getImage()));
//                String name = path.get
//                        (i).substring(path.get
//                        (i).lastIndexOf("/"), path.get
//                        (i).length());

                String name= path.get(i).getImage().substring(path.get(i).getImage().lastIndexOf("/"), path.get(i).getImage().length());


                if (uri != null) {
                    final File destination;
                    destination = new File(this.getExternalFilesDir(null), name);
                    createFile(this, uri, destination);
                    transferUtility =
                            TransferUtility.builder()
                                    .context(this)
                                    .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                                    .s3Client(s3Client)
                                    .build();

                    TransferObserver uploadObserver =
                            transferUtility.upload(s3bucketPath + name, destination);
                    uploadObserver.setTransferListener(new TransferListener() {

                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            if (TransferState.COMPLETED == state) {
                                filestatus = 1;
                                //  Toast.makeText(BaseActivity.this, "uploaded", Toast.LENGTH_SHORT).show();
                                destination.delete();
                            } else if (TransferState.FAILED == state) {

                                //  Toast.makeText(BaseActivity.this, "failed", Toast.LENGTH_SHORT).show();
                                destination.delete();
                                hideProgress();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                            int percentDone = (int) percentDonef;
                            if (mDialog != null) {
                                mDialog.setMessage("Uploading image");
                            }


                            //Toast.makeText(BaseActivity.this, String.valueOf(percentDone), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(int id, Exception ex) {
                            ex.printStackTrace();
                            ///   hideProgress();

                            Toast.makeText(TestDrawerActivity.this, "error", Toast.LENGTH_SHORT).show();
                        }

                    });
                } else {
                    //  onAmazonFileuploaded.FileStatus(0,imageName);
                    Toast.makeText(TestDrawerActivity.this, "uri null", Toast.LENGTH_SHORT).show();
                }
            }

        }
        mDialog.dismiss();
        onAmazonFileuploaded.FileStatus(1, imageName);


    }

    private void logout() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus() == 404) {
                            sharedPreferences.edit().clear().commit();
                            Intent i = new Intent(TestDrawerActivity.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();
                        }
                    } else {
                        hideProgress();
                        startActivity(new Intent(TestDrawerActivity.this, LoginActivity.class));
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TestDrawerActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(TestDrawerActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void userLogin() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            UserLoginParam userLoginParam = new UserLoginParam();
            userLoginParam.setPhoneNumber(sharedPreferences.getString(Constants.PHONE, ""));
            userLoginParam.setAppVersion(sharedPreferences.getString(Constants.APPVERSION, ""));
            userLoginParam.setDeviceIMEI(sharedPreferences.getString(Constants.DEVICEID, ""));
            userLoginParam.setDeviceOS(String.valueOf(sharedPreferences.getInt(Constants.OS_VERSION, 0)));
            userLoginParam.setFcmKey(FirebaseInstanceId.getInstance().getToken());
            userLoginParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            vidyauraAPI.userLogin(userLoginParam).enqueue(new Callback<UserLoginResponse>() {
                @Override
                public void onResponse(Call<UserLoginResponse> call, Response<UserLoginResponse> response) {
                    hideProgress();
                    userLoginResponse = response.body();
                    Gson gson = new Gson();
                    if (userLoginResponse != null) {
                        if (userLoginResponse.getStatus()!= 401) {
                        if (userLoginResponse.getStatus()!=Util.STATUS_TOKENEXPIRE) {
                            if (userLoginResponse.getStatus() == Util.STATUS_SUCCESS) {
                                // Toast.makeText(getContext(),userLoginResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                editor.putString(Constants.SECURITYPIN, userLoginResponse.getData().get(0).getSecurityPin());
                                editor.putString(Constants.USERID, userLoginResponse.getEncyptedUserID());
                                editor.putInt(Constants.SCHOOLID, userLoginResponse.getData().get(0).getSchool_id());
                                editor.putString(Constants.FNAME, userLoginResponse.getData().get(0).getFname());
                                editor.putString(Constants.LNAME, userLoginResponse.getData().get(0).getLname());
                                editor.putString(Constants.EMAIL, userLoginResponse.getData().get(0).getEmail());
                                editor.putString(Constants.GENDER, userLoginResponse.getData().get(0).getGender());
                                editor.putString(Constants.EMP_ID, userLoginResponse.getData().get(0).getEmp_id());
                                editor.putInt(Constants.ID, userLoginResponse.getData().get(0).getId());
                                editor.putString(Constants.DOB, userLoginResponse.getData().get(0).getDob());
                                editor.putString(Constants.PROFILE_PHOTO, userLoginResponse.getData().get(0).getEmp_photo());
                                editor.putString(Constants.JOINING_DATE, userLoginResponse.getData().get(0).getJoining_date());
                                if (userLoginResponse.getAppSettings() != null) {
                                    if (userLoginResponse.getAppSettings().size() > 0) {
                                        editor.putInt(Constants.NOTIFICATIONSTATUS, userLoginResponse.getAppSettings().get(0).getNotificationStatus());
                                        editor.putInt(Constants.MESSAGESTATUS, userLoginResponse.getAppSettings().get(0).getMessageStatus());
                                        editor.putInt(Constants.SECURITYPINSTATUS, userLoginResponse.getAppSettings().get(0).getSecurityPinStatus());
                                    }
                                }
                                editor.putInt(Constants.HASCHILDREN, userLoginResponse.getHasChildren());
                                editor.putString(Constants.SCHOOL_LOGO, userLoginResponse.getSchoolLogo());
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    editor.putInt(Constants.TOTAL_LEVEL_POINTS, userLoginResponse.getTotalLevelPoints());
                                    editor.putString(Constants.POINTS_NAME, userLoginResponse.getTeacherLevel().getName());
                                    editor.putString(Constants.BADGE_IMAGE, userLoginResponse.getTeacherLevel().getBadge_image());
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2) {
                                    editor.putString(Constants.TYPE, userLoginResponse.getData().get(0).getType());
                                }
                                editor.putInt(Constants.OPEN_DRAWER, 1);
                                //secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getSecuredToken()).commit();
                                //editor.putString(Constants.PHONE,userLoginResponse.getData().getPhone());
                                editor.commit();

                                String url = null;
                                if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 1) {
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/" ;
                                } else if (sharedPreferences.getInt(Constants.USERTYPE, 0) == 2){
                                    url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/" ;;
                                }

                                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                MessageList messageList1 = new MessageList((userLoginResponse.getData().get(0).getFname()+" "+userLoginResponse.getData().get(0).getLname()).replace("null","")
                                        ,url+userLoginResponse.getData().get(0).getEmp_photo(),
                                        userLoginResponse.getEncyptedUserID(),
                                        FirebaseInstanceId.getInstance().getToken(),
                                        "senderRole",
                                        "senderClass");
                                mDatabase.child("users").child(userLoginResponse.getEncyptedUserID()).setValue(messageList1);

                                startActivity(new Intent(TestDrawerActivity.this, TestDrawerActivity.class));
                                finishAffinity();


                            } else {
                                Toast.makeText(TestDrawerActivity.this, userLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            hideProgress();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, userLoginResponse.getToken()).commit();
                            userLogin();
                        }} else
                    {
                        hideProgress();
                        startActivity(new Intent(TestDrawerActivity.this,LoginActivity.class));
                    }
                    } else {
                        Toast.makeText(TestDrawerActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                    hideProgress();
                    Toast.makeText(TestDrawerActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            Toast.makeText(TestDrawerActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    /*public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                System.out.println("state ==> 1 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(TestDrawerActivity.this, R.mipmap.noti_orange_icon));
            } else {
                System.out.println("state ==> 2 "+state);
                menu.getItem(3).setIcon(ContextCompat.getDrawable(TestDrawerActivity.this, R.mipmap.noti_white_icon));
            }
        }

    }*/

}
