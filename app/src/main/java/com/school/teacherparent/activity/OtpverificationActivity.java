package com.school.teacherparent.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.CountDownTimer;

import com.chaos.view.PinView;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;


import com.school.teacherparent.R;

import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.CustomTextView;
import com.school.teacherparent.utils.FontTextViewRegular;
import com.school.teacherparent.utils.UiUtils;
import com.school.teacherparent.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by harini on 8/25/2018.
 */


public class OtpverificationActivity extends BaseActivity implements VerificationListener {

    private static final String TAG = Verification.class.getSimpleName();
    private Verification mVerification;
    Button verify;
    ImageView resend;
    PinView otp_pin;
    String mobile_number,securityPin;
    TextView timer, text_resend_otp;
    protected Handler mHandler = new Handler();
    protected ProgressDialog mDialog;
    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_screen);
        mDialog = new ProgressDialog(this);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        Intent i = getIntent();
        mobile_number = i.getStringExtra("phn_number");

       // sharedPreferences = getSharedPreferences(Constants.FCMTOKEN, MODE_PRIVATE);

        verify = findViewById(R.id.button_otp_verify);
        resend = findViewById(R.id.image_resend);
        otp_pin = findViewById(R.id.otp_pinView);
        timer = findViewById(R.id.text_timer);
        text_resend_otp = findViewById(R.id.text_resend_otp);

       // callApi();
         initiateVerification();
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateVerification();
                Toast.makeText(OtpverificationActivity.this, "OTP sent to your number", Toast.LENGTH_SHORT).show();
                new CountDownTimer(30000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        timer.setVisibility(View.VISIBLE);
                        timer.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                        resend.setVisibility(View.GONE);
                        text_resend_otp.setVisibility(View.GONE);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        timer.setVisibility(View.GONE);
                        resend.setVisibility(View.VISIBLE);
                        text_resend_otp.setVisibility(View.VISIBLE);
                    }

                }.start();

            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otp_pin.getText().toString().length() == 0) {
                    otp_pin.setError("Please Enter OTP");
                } else {
                    if (mVerification != null) {
                        //showProgress();
                        hideKeypad();
                        showProgress();
                        mVerification.verify(otp_pin.getText().toString());
                    }

                        //startActivity(new Intent(OtpverificationActivity.this, SecurityPinActivity.class));
                   // onSubmitClicked();
                }
            }
        });


        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setVisibility(View.VISIBLE);
                timer.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                resend.setVisibility(View.GONE);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timer.setVisibility(View.GONE);
                resend.setVisibility(View.VISIBLE);
            }

        }.start();

        showOtpSentDialog();

    }

    private void showOtpSentDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.custom_alert_dialog, null);
        final Button btnSubmit = alertLayout.findViewById(R.id.submit);
        final AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.actionSheetTheme1));
        alert.setView(alertLayout);
        alert.setCancelable(true);

        final AlertDialog dialog = alert.create();
        dialog.show();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    public void onSubmitClicked() {
        String code = otp_pin.getText().toString();
        if (!code.isEmpty()) {
            hideKeypad();
            if (mVerification != null) {
                mVerification.verify(code);
               /* TextView messageText = (TextView) findViewById(R.id.textView);
                messageText.setText("Verification in progress");*/
                // enableInputField(false);
            }
        }
    }

    void initiateVerification(boolean skipPermissionCheck) {
        //set user number in textview

        if (mobile_number != null) {
            // otpphonenumber.setText("We have sent the verification code to " + "+91" + numb + ". Please Verify.");
            createVerification(mobile_number, skipPermissionCheck, "+91");
        }

    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
       /* if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
            hideProgressBar();
        } else {*/
        mVerification = SendOtpVerification.createSmsVerification
                (SendOtpVerification
                        .config(countryCode + phoneNumber)
                        .context(OtpverificationActivity.this)
                        .autoVerification(true)
                        .otplength("4")
                        .httpsConnection(false)
                        .expiry("5")
                        .senderId("VIDYURA")
                        .build(), this);
        mVerification.initiate();// finall build will be submitted on 22
        //  }
    }


    @Override
    public void onInitiationFailed(Exception paramException) {
        Log.e(TAG, "Verification initialization failed: " + paramException.getMessage());
        Toast.makeText(OtpverificationActivity.this, "Verification Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVerified(String response) {
        // hideProgressBar();
        hideKeypad();
        // callApi();
        hideProgress();
        //checkIfUserExists(mobile_number);
        editor.putString(Constants.SECURITYPIN, "");
        editor.apply();
        startActivity(new Intent(OtpverificationActivity.this, SecurityPinActivity.class));
        finish();
    }

    @Override
    public void onInitiated(String response) {
        Log.d(TAG, "Initialized!" + response+mobile_number);
        //OTP successfully resent/sent.
    }

    @Override
    public void onVerificationFailed(Exception exception) {
        try {
        Log.e(TAG, "Verification failed: " + exception.getMessage());
        hideProgress();
        JSONObject jsonObject = new JSONObject(exception.getMessage().toString());
        System.out.println("message ==> "+jsonObject.getString("message"));
        if (jsonObject.getString("message").equals("max_limit_reached_for_this_otp_verification")) {

            Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_otp_failed);
            ImageView ivClose = dialog.findViewById(R.id.iv_close);
            dialog.show();
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        } else {
            Toast.makeText(OtpverificationActivity.this, "OTP Verification Failed", Toast.LENGTH_SHORT).show();
        }
    } catch (
    JSONException e) {
        e.printStackTrace();
    }
        //OTP  verification failed.
    }

    private void hideKeypad() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }





}
