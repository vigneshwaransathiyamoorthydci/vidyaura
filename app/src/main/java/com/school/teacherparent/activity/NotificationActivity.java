package com.school.teacherparent.activity;

import android.app.Activity;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.school.teacherparent.adapter.NotiAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.fragment.ExamDetailListFragment;
import com.school.teacherparent.fragment.ExamUpcomingFragment;
import com.school.teacherparent.fragment.ParentExamUpcomingFragment;
import com.school.teacherparent.models.AddFeedResponse;
import com.school.teacherparent.models.FeedList;
import com.school.teacherparent.models.FeedListParams;
import com.school.teacherparent.models.FeedListResponse;
import com.school.teacherparent.models.MessageList;
import com.school.teacherparent.models.NotiDTO;
import com.school.teacherparent.R;
import com.school.teacherparent.models.NotificationList;
import com.school.teacherparent.models.SendNotification;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.school.teacherparent.fragment.HomeWorkFragment.selected;


public class NotificationActivity extends BaseActivity {

    private RecyclerView recycle;
    private ArrayList<NotiDTO> notiList = new ArrayList<>();
    private ImageView back;
    private TextView noData;
    private NotiAdapter mAdapter;
    ArrayList<NotiDTO> NotificationArrayList = new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    ProgressDialog mDialog;
    LinearLayoutManager linearLayoutManager;
    int limit = 10;
    int offset = 10;
    private boolean loading = true;
    @Inject
    public VidyAPI vidyauraAPI;
    NotificationList notificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        recycle = (RecyclerView) findViewById(R.id.recycle_notifications);
        noData = (TextView) findViewById(R.id.text_no_network);
        back = (ImageView) findViewById(R.id.back);
        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        mDialog = new ProgressDialog(this);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        notiList = new ArrayList<>();
        //PreData();
        getNotificationList();
        //getNotificationListDB();

        back =findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // recycle.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mAdapter = new NotiAdapter(NotificationArrayList, this);

        /*RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());*/
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycle.setItemAnimator(new DefaultItemAnimator());
        recycle.setLayoutManager(linearLayoutManager);
        // recycle.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 5));
        recycle.setAdapter(mAdapter);
        performAction();
    }

    private void performAction() {

        mAdapter.OnsetClickListen(new NotiAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                if (NotificationArrayList.get(position).getType().equals("feeds")) {
                    startActivity(new Intent(NotificationActivity.this, SidemenuDetailActivity.class).
                            putExtra("type", "feedcommentview").putExtra("feedID", NotificationArrayList.get(position).getType_id()));
                } else if (NotificationArrayList.get(position).getType().equals("circulars")) {
                    if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                        startActivity(new Intent(NotificationActivity.this,CircularActivity.class));
                    } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                        startActivity(new Intent(NotificationActivity.this,ParentCircularActivity.class));
                    }
                } else if (NotificationArrayList.get(position).getType().equals("galleries")) {
                    if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                        startActivity(new Intent(NotificationActivity.this, SidemenuDetailActivity.class).putExtra("type", "gallery"));
                    } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                        startActivity(new Intent(NotificationActivity.this, SidemenuDetailActivity.class).putExtra("type", "parentgallery"));
                    }
                } else if (NotificationArrayList.get(position).getType().equals("exam_schedules")) {
                    Intent intent = new Intent(NotificationActivity.this,ExamDetailListActivity.class);
                    Bundle bundle=new Bundle();
                    if (NotificationArrayList.get(position).getExamwiseClassList() != null) {
                        System.out.println("getExamwiseClassList ==> " + NotificationArrayList.get(position).getExamwiseClassList().getClassName());
                    }
                    /*if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                        bundle.putSerializable("classlist", (Serializable) NotificationArrayList.get(position).getExamwiseClassList());
                        bundle.putInt("termid",NotificationArrayList.get(position).getType_id());
                        bundle.putString("exam_title",NotificationArrayList.get(position).getExam_title());
                        intent.putExtras(bundle);
                    } else */if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                        bundle.putInt("termid",NotificationArrayList.get(position).getType_id());
                        bundle.putString("exam_title",NotificationArrayList.get(position).getExam_title());
                        bundle.putInt("classid",NotificationArrayList.get(position).getClassroom_id());
                        bundle.putInt("sectionid", Integer.parseInt(NotificationArrayList.get(position).getSection_id()));
                        bundle.putInt("studIDD",NotificationArrayList.get(position).getStudentID());
                        bundle.putInt("selectedSchoolID",NotificationArrayList.get(position).getSchool_id());
                        intent.putExtras(bundle);
                    }
                    startActivity(intent);
                } else if (NotificationArrayList.get(position).getType().equals("leave")) {
                    if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                        startActivity(new Intent(NotificationActivity.this,LeaveListActivity.class));
                    } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                        startActivity(new Intent(NotificationActivity.this,ParentLeaveApplyActivity.class));
                    }
                } else if (NotificationArrayList.get(position).getType().equals("homework")) {

                    startActivity(new Intent(NotificationActivity.this, SidemenuDetailActivity.class).putExtra("type", "homeworkDetails")
                            .putExtra("classname", getString(R.string.class_name) + " " + NotificationArrayList.get(position).getClassroom_name()+ "" + NotificationArrayList.get(position).getSection_name())
                            .putExtra("homeWorkID", String.valueOf(NotificationArrayList.get(position).getType_id()))
                            .putExtra("classID", NotificationArrayList.get(position).getClassroom_id())
                            .putExtra("sectionID", Integer.parseInt(NotificationArrayList.get(position).getSection_id()))
                            .putExtra("subjectID", NotificationArrayList.get(position).getSubject_id())
                            .putExtra("selectedSchoolID", NotificationArrayList.get(position).getSchool_id())
                            .putExtra("studID", NotificationArrayList.get(position).getStudentID()));

                } else if (NotificationArrayList.get(position).getType().equals("assignments")) {

                    startActivity(new Intent(NotificationActivity.this, SidemenuDetailActivity.class).putExtra("type", "assignmentDetails")
                            .putExtra("classname", getString(R.string.class_name) + " " + NotificationArrayList.get(position).getClassroom_name() + NotificationArrayList.get(position).getSection_name())
                            .putExtra("homeWorkID", String.valueOf(NotificationArrayList.get(position).getType_id()))
                            .putExtra("classID", NotificationArrayList.get(position).getClassroom_id())
                            .putExtra("sectionID", Integer.parseInt(NotificationArrayList.get(position).getSection_id()))
                            .putExtra("subjectID", NotificationArrayList.get(position).getSubject_id())
                            .putExtra("selectedSchoolID", NotificationArrayList.get(position).getSchool_id())
                            .putExtra("studID", NotificationArrayList.get(position).getStudentID()));

                } else if (NotificationArrayList.get(position).getType().equals("event")) {
                    if (sharedPreferences.getInt(Constants.USERTYPE,0) == 1) {
                        startActivity(new Intent(NotificationActivity.this,EventListActivity.class));
                    } else if (sharedPreferences.getInt(Constants.USERTYPE,0) == 2) {
                        startActivity(new Intent(NotificationActivity.this,EventParentListActivity.class));
                    }
                } else if (NotificationArrayList.get(position).getType().equals("message")) {

                    final FirebaseDatabase database = FirebaseDatabase.getInstance();
                    if (!NotificationArrayList.get(position).isGroup()) {
                        final DatabaseReference ref = database.getReference("users").child(NotificationArrayList.get(position).getSenderUserID());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    String receiverName = (String) dataSnapshot.child("receiverName").getValue();
                                    String receiverImage = (String) dataSnapshot.child("receiverImage").getValue();
                                    String receiverUserID = (String) dataSnapshot.child("receiverUserID").getValue();
                                    String receiverFcmKey = (String) dataSnapshot.child("receiverFcmKey").getValue();
                                    String receiverRole = (String) dataSnapshot.child("receiverRole").getValue();
                                    String receiverClass = (String) dataSnapshot.child("receiverClass").getValue();

                                    Intent intent = new Intent(NotificationActivity.this, MessageActivity.class);
                                    intent.putExtra("ReceiverID", receiverUserID);
                                    intent.putExtra("ReceiverName", receiverName);
                                    intent.putExtra("ReceiverImage", receiverImage);
                                    intent.putExtra("ReceiverFcmKey", receiverFcmKey);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                } else {
                        Intent intent = new Intent(NotificationActivity.this, GroupMessageActivity.class);
                        intent.putExtra("GroupName", NotificationArrayList.get(position).getGroupName());
                        intent.putExtra("GroupImage", NotificationArrayList.get(position).getGroupImage());
                        intent.putExtra("GroupID", NotificationArrayList.get(position).getGroupID());
                        intent.putExtra("ArrayReceiverID", NotificationArrayList.get(position).getReceiverUserIDD());
                        intent.putExtra("ArrayReceiverName", NotificationArrayList.get(position).getReceiverName());
                        intent.putExtra("ArrayUserList", NotificationArrayList.get(position).getReceiverName());
                        intent.putExtra("ArrayReceiverImage", NotificationArrayList.get(position).getReceiverImage());
                        intent.putStringArrayListExtra("ArrayReceiverFcmKey", NotificationArrayList.get(position).getReceiverFcmKeyy());
                        startActivity(intent);
                    }


                }
            }
        });

        /*recycle.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            //loading = false;
                            Log.d("...", "Last Item Wow !");
                            //offset=offset+limit;
                            limit = offset+limit;
                            getNotificationList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });*/
    }

    public void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, "");
            fragmentTransaction.commit();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void dismissProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private void getNotificationList() {

        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref_message = database.getReference("notification").child(Objects.requireNonNull(sharedPreferences.getString(Constants.USERID, null)));
            //Query query = ref_message.limitToLast(limit);
            ref_message.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Gson gson = new Gson();
                    System.out.println("notification ====> "+gson.toJson(dataSnapshot.getValue()));
                    NotificationArrayList.clear();
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            String id = (String) child.getKey();
                            String chatID[] = id.split("to");
                            boolean thisGroup = (boolean) dataSnapshot.child(id).child("thisGroup").getValue();

                            String message = (String) dataSnapshot.child(id).child("message").getValue();
                            Long readStatus = (Long) dataSnapshot.child(id).child("readStatus").getValue();
                            String receiverFcmKey = (String) dataSnapshot.child(id).child("receiverFcmKey").getValue();
                            String receiverImage = (String) dataSnapshot.child(id).child("receiverImage").getValue();
                            String receiverName = (String) dataSnapshot.child(id).child("receiverName").getValue();
                            String receiverUserID = (String) dataSnapshot.child(id).child("receiverUserID").getValue();
                            String senderImage = (String) dataSnapshot.child(id).child("senderImage").getValue();
                            String senderName = (String) dataSnapshot.child(id).child("senderName").getValue();
                            String senderUserID = (String) dataSnapshot.child(id).child("senderUserID").getValue();
                            String senderFcmKey = (String) dataSnapshot.child(id).child("senderFcmKey").getValue();
                            String sentTime = (String) dataSnapshot.child(id).child("sentTime").getValue();
                            Long state = (Long) dataSnapshot.child(id).child("state").getValue();
                            Long timeStamp = (Long) dataSnapshot.child(id).child("timeStamp").getValue();
                            Long unreadCount = (Long) dataSnapshot.child(id).child("unreadCount").getValue();
                            String readBy = (String) dataSnapshot.child(id).child("readBy").getValue();
                            String groupID = (String) dataSnapshot.child(id).child("groupID").getValue();
                            String groupImage = (String) dataSnapshot.child(id).child("groupImage").getValue();
                            String groupName = (String) dataSnapshot.child(id).child("groupName").getValue();
                            List<String> imageUrl = (List<String>) dataSnapshot.child(id).child("imageUrl").getValue();
                            ArrayList<String> receiverFcmKeyy = (ArrayList<String>) dataSnapshot.child(id).child("receiverFcmKeyy").getValue();
                            ArrayList<String> receiverUserIDD = (ArrayList<String>) dataSnapshot.child(id).child("receiverUserIDD").getValue();
                            String image = null;
                            if (imageUrl != null) {
                                image = Objects.requireNonNull(imageUrl).get(0);
                            }

                            System.out.println("receiverUserID ==> "+image);

                            NotificationArrayList.add(new NotiDTO(
                                    "",
                                    "message",
                                    0,
                                    "",
                                    message,
                                    0,
                                    sentTime,
                                    0,
                                    "",
                                    "",
                                    0,
                                    0,
                                    "",
                                    "",
                                    senderImage,
                                    "",
                                    message,
                                    image,
                                    thisGroup,
                                    receiverUserID,
                                    receiverName,
                                    receiverImage,
                                    receiverFcmKey,
                                    senderImage,
                                    senderName,
                                    senderUserID,
                                    senderFcmKey,
                                    groupName,
                                    groupImage,
                                    groupID,
                                    receiverFcmKeyy,
                                    receiverUserIDD

                            ));

                        }

                        if (NotificationArrayList.size() > 0) {
                            getNotificationListDB();
                        } else {
                            getNotificationListDB();
                        }

                        /*if (NotificationArrayList.size() > 0) {
                            System.out.println("NotificationArrayList ==> "+NotificationArrayList.size());
                            //Collections.reverse(NotificationArrayList);
                            mDialog.dismiss();
                            recycle.setVisibility(View.VISIBLE);
                            noData.setVisibility(View.GONE);
                            recycle.invalidate();
                            mAdapter.notifyDataSetChanged();
                            loading = true;
                        } else {
                            loading = false;
                            mDialog.dismiss();
                            recycle.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                        }*/

                    } else {
                        //loading = false;
                        //mDialog.dismiss();
                        //recycle.setVisibility(View.GONE);
                        //noData.setVisibility(View.VISIBLE);
                        getNotificationListDB();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    //mDialog.dismiss();
                    //loading = false;
                    getNotificationListDB();
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        } else {
            loading = false;
            Toast.makeText(this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    public void getNotificationListDB()
    {
        if (Util.isNetworkAvailable())
        {
            showProgressDialog();
            SendNotification sendNotification=new SendNotification();
            sendNotification.setUserID(sharedPreferences.getString(Constants.USERID,""));
            sendNotification.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE,0)));
            //sendNotification.setLimit(limit);
            //sendNotification.setOffset(offset);
            sendNotification.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));
            vidyauraAPI.getNotification(sendNotification).enqueue(new Callback<NotificationList>() {
                @Override
                public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        notificationList = response.body();
                        Gson gson = new Gson();
                        System.out.println("notificationList =====> "+gson.toJson(notificationList));

                            if (notificationList.getStatus() == Util.STATUS_SUCCESS) {

                                for (int i=0; i<notificationList.getNotification().size(); i++) {

                                    String url = "";
                                    if (notificationList.getNotification().get(i).getUser_type().equals("parent")) {
                                        url = getString(R.string.s3_baseurl) + getString(R.string.s3_student_path) + "/";
                                    } else if (notificationList.getNotification().get(i).getUser_type().equals("teacher")) {
                                        url = getString(R.string.s3_baseurl) + getString(R.string.s3_employee) + "/";
                                    } else if (notificationList.getNotification().get(i).getUser_type().equals("admin")) {
                                        url = getString(R.string.s3_baseurl) + getString(R.string.s3_schools);
                                    }

                                    String urlPost = "";
                                    if (notificationList.getNotification().get(i).getType_document() != null) {
                                    if (!notificationList.getNotification().get(i).getType_document().equals("")) {
                                        urlPost = getString(R.string.s3_baseurl) + getString(R.string.s3_feeds_path) + "/";
                                    }
                                    }

                                        NotificationArrayList.add(new NotiDTO(
                                                notificationList.getNotification().get(i).getId(),
                                                notificationList.getNotification().get(i).getType(),
                                                notificationList.getNotification().get(i).getType_id(),
                                                notificationList.getNotification().get(i).getSection_id(),
                                                notificationList.getNotification().get(i).getNotification_message(),
                                                notificationList.getNotification().get(i).getSchool_id(),
                                                notificationList.getNotification().get(i).getCreated_at(),
                                                notificationList.getNotification().get(i).getClassroom_id(),
                                                notificationList.getNotification().get(i).getClassroom_name(),
                                                notificationList.getNotification().get(i).getSection_name(),
                                                notificationList.getNotification().get(i).getSubject_id(),
                                                notificationList.getNotification().get(i).getStudentID(),
                                                notificationList.getNotification().get(i).getExam_title(),
                                                notificationList.getNotification().get(i).getUser_type(),
                                                url + notificationList.getNotification().get(i).getImageurl(),
                                                notificationList.getNotification().get(i).getTitle(),
                                                notificationList.getNotification().get(i).getDescription(),
                                                urlPost + notificationList.getNotification().get(i).getType_document(),
                                                false,
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                "",
                                                null,
                                                null

                                        ));

                                }

                                if (NotificationArrayList.size() > 0) {
                                    System.out.println("notificationList =====> 1 "+NotificationArrayList.size());
                                    sortArrayList();
                                } else {
                                    mDialog.dismiss();
                                    recycle.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);
                                }

                            } else {
                                if (NotificationArrayList.size() > 0) {
                                    sortArrayList();
                                } else {
                                    mDialog.dismiss();
                                    recycle.setVisibility(View.GONE);
                                    noData.setVisibility(View.VISIBLE);
                                }
                            }

                    }
                    else
                    {
                        if (NotificationArrayList.size() > 0) {
                            sortArrayList();
                        } else {
                            mDialog.dismiss();
                            recycle.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                        }

                    }
                }

                @Override
                public void onFailure(Call<NotificationList> call, Throwable t) {
                    dismissProgressDialog();
                    //isLoading=true;
                    if (NotificationArrayList.size() > 0) {
                        sortArrayList();
                    } else {
                        mDialog.dismiss();
                        recycle.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                    }
                    System.out.println("errrrrroooorrrrr ==> "+t.getMessage());
                    Toast.makeText(NotificationActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;

                }
            });

        }
        else
        {
            dismissProgressDialog();
            //isLoading=true;
            Toast.makeText(this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void sortArrayList() {
        /*Collections.sort(NotificationArrayList, new Comparator<NotiDTO>() {
            @Override
            public int compare(NotiDTO o1, NotiDTO o2) {
                try {
                    DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);

                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

                    SimpleDateFormat dateFormatParse = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String targetDate1 = o1.getCreated_at();
                    String targetDate2 = o2.getCreated_at();
                    Date date1 = dateFormatParse.parse(targetDate1);
                    Date date2 = dateFormatParse.parse(targetDate2);

                    String dateString1 = formatter.format(new Date(Long.parseLong(String.valueOf(date1.getTime()))));
                    String dateString2 = formatter.format(new Date(Long.parseLong(String.valueOf(date2.getTime()))));

                    return Long.valueOf(format.parse(dateString1).getTime()).compareTo(format.parse(dateString2).getTime());
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });*/

        Collections.sort(NotificationArrayList, new Comparator<NotiDTO>() {
            DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            @Override
            public int compare(NotiDTO lhs, NotiDTO rhs) {
                try {
                    return f.parse(lhs.getCreated_at()).compareTo(f.parse(rhs.getCreated_at()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });

        for (int i=0;i<NotificationArrayList.size();i++) {
            System.out.println("getCreated_at ===> 1 "+NotificationArrayList.get(i).getCreated_at());
        }

        Collections.reverse(NotificationArrayList);
        mDialog.dismiss();
        recycle.setVisibility(View.VISIBLE);
        noData.setVisibility(View.GONE);
        mAdapter.notifyDataSetChanged();

        Intent intentNo = new Intent("notificationListener");
        intentNo.putExtra("notify", false);
        sendBroadcast(intentNo);
        editor.putBoolean(Constants.NOTIFICATION_STATE,false);

    }

    /*private void showProgressDialog() {
        if (mDialog == null) {
            mDialog = new ProgressDialog(this);
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mDialog.setIndeterminate(true);
            mDialog.setCancelable(false);
        }
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }*/



   /* private void PreData() {

        NotiDTO s = new NotiDTO();
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setTitle("Circular sent by admin");
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("10 min ago");
        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Juan sent you a message");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("Hello teacher I would know about my son in classroom and his performance in his studies");
        s.setTime("5 min ago");
        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Circular sent by Emy werly");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("15 min ago");
        notiList.add(s);


        s = new NotiDTO();
        s.setTitle("Michael posted a feed");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("2 min ago");
        notiList.add(s);

        s = new NotiDTO();
        s.setTitle("John posted a feed");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an NCC Camp trip to Kashmir for student of class XII");
        s.setTime("59 min ago");
        notiList.add(s);

        s = new NotiDTO();
        s.setTitle("Circular sent by Principal");
        s.setImg(String.valueOf(R.mipmap.student_image));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("Holiday Declared on May 1 2019 and Wish You all Happy Workers Day");
        s.setTime("1 hr ago");
        notiList.add(s);

    }*/
}
