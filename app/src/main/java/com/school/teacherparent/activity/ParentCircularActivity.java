package com.school.teacherparent.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.school.teacherparent.R;
import com.school.teacherparent.adapter.CircularAdapter;
import com.school.teacherparent.adapter.StudentListAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.models.ChildListResponseResponse;
import com.school.teacherparent.models.CircularList;
import com.school.teacherparent.models.CircularListResponse;
import com.school.teacherparent.models.GetStudentListParam;
import com.school.teacherparent.retrofit.VidyAPI;
import com.school.teacherparent.utils.Constants;
import com.school.teacherparent.utils.Util;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentCircularActivity extends AppCompatActivity implements
        DiscreteScrollView.ScrollStateChangeListener<StudentListAdapter.MyViewHolder>,
        DiscreteScrollView.OnItemChangedListener<StudentListAdapter.MyViewHolder> {

    @Inject
    public VidyAPI vidyauraAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public SharedPreferences.Editor secureTokenSharedPreferenceseditor;
    SharedPreferences secureTokenSharedPreferences;

    ImageView ivBack,ivNoti;
    DiscreteScrollView dsvStudentList;
    SwipeRefreshLayout swipeCircular;
    RecyclerView rvCircular;
    TextView tvNoCircular;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog mDialog;
    private boolean loading = true;
    int limit=5;
    int offset=0;
    int count = 0;
    ChildListResponseResponse childListResponseResponse;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList = new ArrayList<>();
    int classID, studentID, sectionID,studentSchoolID;
    ArrayList<CircularListResponse.circularList> circularListResponseArrayList = new ArrayList<>();
    CircularListResponse circularListResponse;
    ArrayList<CircularList> circularLists = new ArrayList<>();
    boolean isLoading=true;
    CircularAdapter circularAdapter;
    NotificationReceiver notificationReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_circular);
        initViews();
        performAction();
    }

    private void initViews() {
        ivBack = findViewById(R.id.back);
        ivNoti = findViewById(R.id.noti);
        dsvStudentList = findViewById(R.id.dsv_student_list);
        swipeCircular = findViewById(R.id.swipe_circular);
        rvCircular = findViewById(R.id.recycle_circular);
        tvNoCircular = findViewById(R.id.nocircularListTextview);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setAutoMeasureEnabled(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        circularAdapter = new CircularAdapter(circularLists, this);
        rvCircular.setAdapter(circularAdapter);
        rvCircular.setItemAnimator(new DefaultItemAnimator());
        rvCircular.setLayoutManager(linearLayoutManager);

        VidyauraApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        secureTokenSharedPreferences = getSharedPreferences(Constants.SECURE_TOKEN, Context.MODE_PRIVATE);
        secureTokenSharedPreferenceseditor = secureTokenSharedPreferences.edit();

        mDialog = new ProgressDialog(this);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setIndeterminate(true);
        mDialog.setCancelable(false);

        notificationReceiver = new NotificationReceiver();
        try {
            IntentFilter intentFilter = new IntentFilter("notificationListener");
            registerReceiver(notificationReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreferences.getBoolean(Constants.NOTIFICATION_STATE,false)) {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_orange_icon));
        } else {
            ivNoti.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.noti_white_icon));
        }

        getStudentList();
    }

    private void performAction() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ParentCircularActivity.this,NotificationActivity.class));
            }
        });

        swipeCircular.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeCircular.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        circularLists.clear();
                        circularAdapter.notifyDataSetChanged();
                        offset=0;
                        limit=5;
                        getCircularList(studentID);
                        swipeCircular.setRefreshing(false);

                    }
                }, 000);
            }
        });

        rvCircular.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {

                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.d("...", "Last Item Wow !");
                            offset=offset+limit;
                            getCircularList(studentID);
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

    }

    private void setDiscreteScrollViewData() {
        dsvStudentList.setSlideOnFling(true);
        dsvStudentList.setAdapter(new StudentListAdapter(this,parentsStudListArrayList));
        dsvStudentList.addOnItemChangedListener(this);
        dsvStudentList.addScrollStateChangeListener(this);
        if (parentsStudListArrayList.size()>2) {
            dsvStudentList.scrollToPosition(1);
            classID = childListResponseResponse.getParentsStudList().get(1).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(1).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(1).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(1).getSchool_id();
        } else {
            dsvStudentList.scrollToPosition(0);
            classID = childListResponseResponse.getParentsStudList().get(0).getClass_id();
            sectionID = childListResponseResponse.getParentsStudList().get(0).getSection_id();
            studentID = childListResponseResponse.getParentsStudList().get(0).getStudID();
            studentSchoolID = childListResponseResponse.getParentsStudList().get(0).getSchool_id();
        }
        dsvStudentList.setItemTransitionTimeMillis(150);
        dsvStudentList.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.8f).build());
    }

    private void getStudentList() {
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            GetStudentListParam getStudentListParam = new GetStudentListParam();
            getStudentListParam.setUserID(sharedPreferences.getString(Constants.USERID, ""));
            getStudentListParam.setUserType(String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)));
            getStudentListParam.setSchoolID(String.valueOf(sharedPreferences.getInt(Constants.SCHOOLID, 0)));

            vidyauraAPI.getParentsStudList(getStudentListParam).enqueue(new Callback<ChildListResponseResponse>() {
                @Override
                public void onResponse(Call<ChildListResponseResponse> call, Response<ChildListResponseResponse> response) {
                    mDialog.dismiss();
                    if (response.body() != null) {
                        childListResponseResponse = response.body();
                        if (childListResponseResponse.getStatus() != Util.STATUS_TOKENEXPIRE) {
                            if (childListResponseResponse.getParentsStudList() != null) {
                            if (childListResponseResponse.getParentsStudList().size() > 0) {
                                count = response.body().getParentsStudList().size();
                                parentsStudListArrayList = childListResponseResponse.getParentsStudList();
                                setDiscreteScrollViewData();
                                circularLists.clear();
                                getCircularList(studentID);
                            } else {
                                Toast.makeText(ParentCircularActivity.this, childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }} else {
                                Toast.makeText(ParentCircularActivity.this, childListResponseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mDialog.dismiss();
                            secureTokenSharedPreferenceseditor.putString(Constants.SECURE_TOKEN, childListResponseResponse.getToken()).commit();
                            getStudentList();
                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<ChildListResponseResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(ParentCircularActivity.this, getString(R.string.SomethingError), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            mDialog.dismiss();
            Toast.makeText(ParentCircularActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        mDialog.show();
        mDialog.setContentView(R.layout.custom_progress_view);
    }

    private void getCircularList(int selectedStudentID) {
        //Toast.makeText(this,"Student ID is "+selectedStudentID,Toast.LENGTH_SHORT).show();
        //tvNoCircular.setVisibility(View.VISIBLE);
        //swipeCircular.setVisibility(View.GONE);

        isLoading=true;
        if (Util.isNetworkAvailable()) {
            showProgressDialog();
            vidyauraAPI.getcircularByfieldParent(sharedPreferences.getString(Constants.USERID, ""),
                    String.valueOf(sharedPreferences.getInt(Constants.USERTYPE, 0)),
                    String.valueOf(studentSchoolID),
                    limit,
                    offset,selectedStudentID).enqueue(new Callback<CircularListResponse>() {
                @Override
                public void onResponse(Call<CircularListResponse> call, Response<CircularListResponse> response) {
                    mDialog.dismiss();
                    if (response.body()!=null)
                    {
                        circularListResponse=response.body();
                        if (circularListResponse.getStatus()!=Util.STATUS_TOKENEXPIRE)
                        {
                            circularListResponseArrayList = response.body().getCircularList();
                            if (circularListResponse.getStatus() == Util.STATUS_SUCCESS) {
                                if (circularListResponseArrayList.size()!=0) {
                                    rvCircular.setVisibility(View.VISIBLE);
                                    tvNoCircular.setVisibility(View.GONE);
                                    for (int y=0;y<circularListResponseArrayList.size();y++)
                                    {
                                        circularLists.add(new CircularList(circularListResponseArrayList.get(y).getSchoolID(),
                                                circularListResponseArrayList.get(y).getSchoolName(),
                                                circularListResponseArrayList.get(y).getSchoolLogo(),circularListResponseArrayList.get(y).getId(),
                                                circularListResponseArrayList.get(y).getCircular_no(),
                                                circularListResponseArrayList.get(y).getTitle(),circularListResponseArrayList.get(y).getDescription(),
                                                circularListResponseArrayList.get(y).getClassroom_id(),
                                                circularListResponseArrayList.get(y).getAuthor_type(),circularListResponseArrayList.get(y).getCreated_by(),
                                                circularListResponseArrayList.get(y).getIsUserPostedCircular(),
                                                circularListResponseArrayList.get(y).getCreated_at(),circularListResponseArrayList.get(y).getAttachmentsList()));
                                    }

                                    circularAdapter.notifyDataSetChanged();
                                    loading=true;
                                }
                                else if (circularListResponseArrayList.size()!=0)
                                {
                                    tvNoCircular.setVisibility(View.VISIBLE);
                                    rvCircular.setVisibility(View.GONE);
                                    loading=false;
                                }
                            } else {
                                loading=false;
                                //Toast.makeText(CircularActivity.this, circularListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(getApplicationContext(), SecurityPinActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finishAffinity();
                        }
                    }
                    else
                    {
                        Toast.makeText(ParentCircularActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<CircularListResponse> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(ParentCircularActivity.this,getString(R.string.SomethingError),Toast.LENGTH_SHORT).show();
                    loading=false;
                }
            });


        }
        else
        {
            Toast.makeText(ParentCircularActivity.this, getString(R.string.PleaseheckyourNetwork), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        circularLists.clear();
        circularAdapter.notifyDataSetChanged();
        getCircularList(studentID);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        circularLists.clear();
        circularAdapter.notifyDataSetChanged();

    }

    @Override
    public void onScrollStart(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {
        currentItemHolder.hideText();
    }

    @Override
    public void onScrollEnd(@NonNull StudentListAdapter.MyViewHolder currentItemHolder, int adapterPosition) {

    }

    @Override
    public void onScroll(float currentPosition, int currentIndex, int newIndex, @Nullable StudentListAdapter.MyViewHolder currentHolder, @Nullable StudentListAdapter.MyViewHolder newCurrent) {

    }

    @Override
    public void onCurrentItemChanged(@Nullable StudentListAdapter.MyViewHolder viewHolder, int adapterPosition) {
        if (viewHolder != null) {
            viewHolder.showText();
            classID=parentsStudListArrayList.get(adapterPosition).getClass_id();
            sectionID= parentsStudListArrayList.get(adapterPosition).getSection_id();
            studentID= parentsStudListArrayList.get(adapterPosition).getStudID();
            studentSchoolID= parentsStudListArrayList.get(adapterPosition).getSchool_id();
            circularLists.clear();
            circularAdapter.notifyDataSetChanged();
            offset = 0;
            limit = 5;
            getCircularList(studentID);
        }
    }

    public class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("log", ">>" + intent.getBooleanExtra("notify", false));
            boolean state = intent.getBooleanExtra("notify", false);
            if (state) {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_orange_icon));
            } else {
                ivNoti.setImageDrawable(getResources().getDrawable(R.mipmap.noti_white_icon));
            }
        }

    }

}
