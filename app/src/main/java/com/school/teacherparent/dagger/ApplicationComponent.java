package com.school.teacherparent.dagger;

import android.content.SharedPreferences;

import com.school.teacherparent.activity.ApplyLeaveActivity;
import com.school.teacherparent.activity.AttendanceActivity;
import com.school.teacherparent.activity.CircularActivity;
import com.school.teacherparent.activity.ClassDetailListActivity;
import com.school.teacherparent.activity.ClassworkActivity;
import com.school.teacherparent.activity.EventListActivity;
import com.school.teacherparent.activity.EventParentListActivity;
import com.school.teacherparent.activity.ExamDetailListActivity;
import com.school.teacherparent.activity.ExamsActivity;
import com.school.teacherparent.activity.GroupMessageActivity;
import com.school.teacherparent.activity.HomeWorkActivity;
import com.school.teacherparent.activity.LeaveListActivity;
import com.school.teacherparent.activity.MessageActivity;
import com.school.teacherparent.activity.NotificationActivity;
import com.school.teacherparent.activity.ParentCircularActivity;
import com.school.teacherparent.activity.ParentLeaveApplyActivity;
import com.school.teacherparent.activity.NewMessageActivity;
import com.school.teacherparent.activity.OtpverificationActivity;
import com.school.teacherparent.activity.PaymentActivity;
import com.school.teacherparent.activity.ResultActivity;
import com.school.teacherparent.activity.SecurityPinActivity;
import com.school.teacherparent.activity.SidemenuDetailActivity;
import com.school.teacherparent.activity.SplashScreenActivity;
import com.school.teacherparent.activity.TestDrawerActivity;
import com.school.teacherparent.adapter.AddAttenAdapter;
import com.school.teacherparent.adapter.AllGalleryAdapter;
import com.school.teacherparent.adapter.AssignmentDetailsAdapter;
import com.school.teacherparent.adapter.AssignmentMainAdapter;
import com.school.teacherparent.adapter.CircularAdapter;
import com.school.teacherparent.adapter.ClassworkAdapter;
import com.school.teacherparent.adapter.DiaryactivitiesTodayclassworkAdapter;
import com.school.teacherparent.adapter.DiaryactivitiesTodayhomeworkworkAdapter;
import com.school.teacherparent.adapter.EventsAdapter;
import com.school.teacherparent.adapter.EventsParentAdapter;
import com.school.teacherparent.adapter.ExamDetailAdapter;
import com.school.teacherparent.adapter.FeedsAdapter;
import com.school.teacherparent.adapter.FeedscommentviewAdapter;
import com.school.teacherparent.adapter.HomeWorkDetailsAdapter;
import com.school.teacherparent.adapter.HomeworkAdapter;
import com.school.teacherparent.adapter.MessageAdapter;
import com.school.teacherparent.adapter.PollsViewListAdapter;
import com.school.teacherparent.adapter.StudentsAdapter;
import com.school.teacherparent.adapter.SubClassAdapter;
import com.school.teacherparent.adapter.UpdateAttenAdapter;
import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.firebase.FirebaseMessaging;
import com.school.teacherparent.fragment.AboutFragment;
import com.school.teacherparent.fragment.AddAttendanceFragment;
import com.school.teacherparent.fragment.AddCircularFragment;
import com.school.teacherparent.fragment.AddClassWorkFragment;
import com.school.teacherparent.fragment.AddEventFragment;
import com.school.teacherparent.fragment.AddExamFragment;
import com.school.teacherparent.fragment.AddGalleryFragment;
import com.school.teacherparent.fragment.AddHomeAssignmentFragment;
import com.school.teacherparent.fragment.AddHomeWorkFragment;
import com.school.teacherparent.fragment.AddNewFeedFragment;
import com.school.teacherparent.fragment.AddResultFragment;
import com.school.teacherparent.fragment.AllGalleryFragment;
import com.school.teacherparent.fragment.AssignmentDetailsFragment;
import com.school.teacherparent.fragment.AssignmentHomeFragment;
import com.school.teacherparent.fragment.AttendanceFragment;
import com.school.teacherparent.fragment.AttendanceforParentFragment;
import com.school.teacherparent.fragment.ChatWithParentListFragment;
import com.school.teacherparent.fragment.ChatWithTeacherListFragment;
import com.school.teacherparent.fragment.ClassDetailListFragment1;
import com.school.teacherparent.fragment.ClassWorkFragment;
import com.school.teacherparent.fragment.ClassWorkTab;
import com.school.teacherparent.fragment.DiaryFragment;
import com.school.teacherparent.fragment.ExamDetailListFragment;
import com.school.teacherparent.fragment.ExamDetailListFragment1;
import com.school.teacherparent.fragment.ExamFragment;
import com.school.teacherparent.fragment.ExamHistoryDetailListFragment;
import com.school.teacherparent.fragment.ExamHistoryFragment;
import com.school.teacherparent.fragment.ExamUpcomingFragment;
import com.school.teacherparent.fragment.FaqFragment;
import com.school.teacherparent.fragment.ParentFeedClapFragment;
import com.school.teacherparent.fragment.FeedCommentReplyFragment;
import com.school.teacherparent.fragment.FeedCommentviewFragment;
import com.school.teacherparent.fragment.FeedbackFragment;
import com.school.teacherparent.fragment.FeedsFragment;
import com.school.teacherparent.fragment.FeesFragment;
import com.school.teacherparent.fragment.HomeWorkFragment;
import com.school.teacherparent.fragment.HomeworkDetailsFragment;
import com.school.teacherparent.fragment.LoginNumberFragment;
import com.school.teacherparent.fragment.LoginUserTypeFragment;
import com.school.teacherparent.fragment.MenuFragment;
import com.school.teacherparent.fragment.MessageFragment;
import com.school.teacherparent.fragment.MyclassesFragment;

import com.school.teacherparent.fragment.ParentAllGalleryFragment;
import com.school.teacherparent.fragment.ParentAssignmentHomeFragment;
import com.school.teacherparent.fragment.ParentClassWorkFragment;
import com.school.teacherparent.fragment.ParentDiaryFragment;
import com.school.teacherparent.fragment.ParentExamHistoryFragment;
import com.school.teacherparent.fragment.ParentExamUpcomingFragment;

import com.school.teacherparent.fragment.ParentFeedCommentviewFragment;
import com.school.teacherparent.fragment.ParentGalleryFragment;
import com.school.teacherparent.fragment.ParentPhotosGalleryFragment;
import com.school.teacherparent.fragment.ParentResultFragment;
import com.school.teacherparent.fragment.ParentSubHomeFragment;
import com.school.teacherparent.fragment.ParentSyllabusFragment;

import com.school.teacherparent.fragment.ParentVideosGalleryFragment;
import com.school.teacherparent.fragment.ParentsOfFragment;
import com.school.teacherparent.fragment.PhotosGalleryFragment;
import com.school.teacherparent.fragment.PollsFragment;
import com.school.teacherparent.fragment.PollsViewFragment;
import com.school.teacherparent.fragment.PricavypolicyFragment;
import com.school.teacherparent.fragment.ProfileFragment;
import com.school.teacherparent.fragment.ProfileforStudentsFragment;
import com.school.teacherparent.fragment.ResultClassListFragment;
import com.school.teacherparent.fragment.ResultFragment;
import com.school.teacherparent.fragment.ResultStudentFragment;
import com.school.teacherparent.fragment.SchoolFragment;
import com.school.teacherparent.fragment.SecurityPinFragment;
import com.school.teacherparent.fragment.SettingFragment;
import com.school.teacherparent.fragment.SubAttendanceFragment;
import com.school.teacherparent.fragment.SubClassFragment;
import com.school.teacherparent.fragment.SubClassforParentFragment;
import com.school.teacherparent.fragment.SubHomeFragment;
import com.school.teacherparent.fragment.SubSyllabusFragment;
import com.school.teacherparent.fragment.SyllabusFragment;
import com.school.teacherparent.fragment.TeacherFeedClapFragment;
import com.school.teacherparent.fragment.TimetableFragment;
import com.school.teacherparent.fragment.TimetableforParentFragment;
import com.school.teacherparent.fragment.UpdateAttendanceFragment;
import com.school.teacherparent.fragment.UpdateClassWorkFragment;
import com.school.teacherparent.fragment.UpdateExamFragment;
import com.school.teacherparent.fragment.UpdateHomeAssignmentFragment;
import com.school.teacherparent.fragment.UpdateHomeWorkFragment;
import com.school.teacherparent.fragment.VideosGalleryFragment;
import com.school.teacherparent.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    void inject(VidyauraApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(OtpverificationActivity otpverificationActivity);

    void inject(MenuFragment menuFragment);

    void inject(TestDrawerActivity testDrawerActivity);

    void inject(ApplyLeaveActivity applyLeaveActivity);


    void inject(LoginNumberFragment loginNumberFragment);

    void inject(SecurityPinFragment securityPinFragment);
    void inject(LoginUserTypeFragment loginUserFragment);

    void inject (SecurityPinActivity securityPinActivity);

    void inject (SplashScreenActivity splashScreenActivity);
    void inject (SettingFragment settingFragment);

    void inject (AddNewFeedFragment addNewFeedFragment);
    void inject (LeaveListActivity leaveListActivity);

    void inject (FeedsFragment feedsFragment);

    void inject (FeedsAdapter feedsAdapter);

    void inject (ParentFeedClapFragment feedClapFragment);

    void inject (FeedCommentviewFragment feedCommentviewFragment);
    void inject (RetrofitModule retrofitModule);

    void inject (ProfileFragment profileFragment);

    void inject (FeedscommentviewAdapter feedscommentviewAdapter);

    void inject (FeedCommentReplyFragment feedCommentReplyFragment);
    void inject (CircularActivity circularActivity);

    void inject (AddCircularFragment addCircularFragment);

    void inject (CircularAdapter circularAdapter);

    void inject (ExamHistoryFragment examHistoryFragment);
    void inject (ExamUpcomingFragment examHistoryFragment);
    void inject (ExamDetailListFragment examDetailListFragment);

    void inject (AddExamFragment addExamFragment);

    void inject (ExamHistoryDetailListFragment examHistoryDetailListFragment);
    void inject (EventListActivity eventListActivity);

    void inject (AddEventFragment addEventFragment);

    void inject (EventsAdapter eventsAdapter);
    void inject (ExamDetailAdapter examDetailAdapter);
    void inject (ClassWorkFragment classWorkFragment);
    void inject (SubHomeFragment subHomeFragment);

    void inject (HomeworkAdapter homeworkAdapter);
    void inject (AddHomeWorkFragment homeWorkFragment);

    void inject (SidemenuDetailActivity sidemenuDetailActivity);

    void inject (AddHomeAssignmentFragment addHomeAssignmentFragment);

    void inject (AssignmentHomeFragment assignmentHomeFragment);

    void inject (AssignmentMainAdapter assignmentMainAdapter);

    void inject (HomeworkDetailsFragment homeworkDetailsFragment);

    void inject (HomeWorkDetailsAdapter homeWorkDetailsAdapter);

    void inject (UpdateHomeWorkFragment updateHomeWorkFragment);

    void inject (UpdateHomeAssignmentFragment updateHomeWorkFragment);

    void inject (AssignmentDetailsFragment updateHomeWorkFragment);

    void inject (AssignmentDetailsAdapter assignmentDetailsAdapter);

    void inject (UpdateExamFragment updateExamFragment);
    void inject (AddClassWorkFragment addClassWorkFragment);

    void inject (ClassworkAdapter classworkAdapter);

    void inject (AttendanceFragment attendanceFragment);

    void inject (SubAttendanceFragment subAttendanceFragment);
    void inject (AddAttendanceFragment addAttendanceFragment);

    void inject (UpdateAttendanceFragment addAttendanceFragment);
    void inject (SyllabusFragment profileFragment);
    void inject (SubSyllabusFragment subSyllabusFragment);
    void inject (SubClassFragment subClassFragment);
    void inject(NewMessageActivity newMessageActivity);
    void inject(MessageFragment messageFragment);

    void inject(HomeWorkActivity homeWorkActivity);

    void inject(SubClassAdapter subClassAdapter);
    void inject(UpdateClassWorkFragment updateClassWorkFragment);

    void inject(ResultClassListFragment resultClassListFragment);

    void inject(AddResultFragment addResultFragment);

    void inject(ResultFragment resultFragment);

    void inject(ResultStudentFragment resultStudentFragment);

    void inject (SchoolFragment schoolFragment);
    void inject(TimetableFragment timetableFragment);

    void inject(DiaryFragment diaryFragment);

    void inject(MyclassesFragment myclassesFragment);

    void inject(ParentsOfFragment parentsOfFragment);

    void inject(AllGalleryFragment allGalleryFragment);

    void inject(PhotosGalleryFragment photosGalleryFragment);

    void inject(VideosGalleryFragment videosGalleryFragment);

    void inject(FaqFragment faqFragment);
    void inject(FeedbackFragment feedbackFragment);
    void inject(PricavypolicyFragment pricavypolicyFragment);
    void inject(AboutFragment aboutFragment);

    void inject(AddGalleryFragment addGalleryFragment);

    void inject(AllGalleryAdapter allGalleryAdapter);

    void inject(ParentLeaveApplyActivity mainActivityte);

    void inject(AttendanceforParentFragment attendanceforParentFragment);

    void inject(AttendanceActivity attendanceActivity);

    void inject(ClassworkActivity classworkActivity);

    void inject(ClassWorkTab classWorkTab);

    void inject(ParentClassWorkFragment parentClassWorkFragment);

    void inject(ParentSyllabusFragment parentSyllabusFragment);

    void inject(HomeWorkFragment homeWorkFragment);

    void inject(ParentSubHomeFragment parentSubHomeFragment);

    void inject(ParentAssignmentHomeFragment parentAssignmentHomeFragment);

    void inject(ParentCircularActivity parentCircularActivity);

    void inject(EventsParentAdapter eventsParentAdapter);

    void inject(EventParentListActivity eventParentListActivity);

    void inject(ParentExamUpcomingFragment parentExamUpcomingFragment);

    void inject(ParentExamHistoryFragment parentExamHistoryFragment);

    void inject(ParentResultFragment parentResultFragment);

    void inject(ResultActivity resultActivity);

    void inject(FeesFragment feesFragment);

    void inject(ParentAllGalleryFragment parentAllGalleryFragment);

    void inject(ParentPhotosGalleryFragment parentPhotosGalleryFragment);

    void inject(ParentVideosGalleryFragment parentVideosGalleryFragment);

    void inject(ParentGalleryFragment parentGalleryFragment);

    void inject(SubClassforParentFragment feesFragment);

    void inject(ProfileforStudentsFragment feesFragment);

    void inject(TimetableforParentFragment timetableforParentFragment);

    void inject(ExamFragment timetableforParentFragment);

    void inject(ParentDiaryFragment timetableforParentFragment);

    void inject(PollsFragment pollsFragment);

    void inject(PollsViewFragment pollsViewFragment);

    void inject(ParentFeedCommentviewFragment pollsViewFragment);

    void inject(TeacherFeedClapFragment pollsViewFragment);

    void inject(MessageActivity messageActivity);

    void inject(ChatWithParentListFragment chatWithParentListFragment);

    void inject(ChatWithTeacherListFragment chatWithTeacherListFragment);

    void inject(GroupMessageActivity groupMessageActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(StudentsAdapter studentsAdapter);

    void inject(MessageAdapter messageAdapter);

    void inject (PollsViewListAdapter pollsViewListAdapter);

    void inject (UpdateAttenAdapter updateAttenAdapter);

    void inject (AddAttenAdapter addAttenAdapter);

    void inject (PaymentActivity paymentActivity);

    void inject (ExamsActivity examsActivity);

    void inject (FirebaseMessaging firebaseMessaging);

    void inject (ExamDetailListFragment1 examDetailListFragment1);

    void inject (ExamDetailListActivity examDetailListActivity);

    void inject (DiaryactivitiesTodayhomeworkworkAdapter diaryactivitiesTodayhomeworkworkAdapter);

    void inject (DiaryactivitiesTodayclassworkAdapter diaryactivitiesTodayclassworkAdapter);
    void inject (ClassDetailListActivity classDetailListActivity);
    void inject (ClassDetailListFragment1 classDetailListFragment1);

}
