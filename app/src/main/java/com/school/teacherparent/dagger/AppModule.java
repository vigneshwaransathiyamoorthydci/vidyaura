package com.school.teacherparent.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.school.teacherparent.app.VidyauraApplication;
import com.school.teacherparent.retrofit.VidyAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;
@Module
public class AppModule {


    public static final String DMK_PREFS = "dmk";

    private final VidyauraApplication dmkApp;

    public AppModule(VidyauraApplication app) {
        this.dmkApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return dmkApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return dmkApp.getSharedPreferences(DMK_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public VidyAPI provideViduApiInterface(Retrofit retrofit) {
        return retrofit.create(VidyAPI.class);
    }
}
