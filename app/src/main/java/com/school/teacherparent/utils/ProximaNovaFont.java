package com.school.teacherparent.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by harini on 6/12/2018.
 */

public class ProximaNovaFont {
    private static ProximaNovaFont instance;
    private static Typeface typeface;
    private static Typeface typeface_semi_bold;
    private static Typeface typeface_bold;
    private static Typeface typeface_light;


    public static ProximaNovaFont getInstance(Context context) {
        synchronized (ProximaNovaFont.class) {
            if (instance == null) {
                instance = new ProximaNovaFont();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "proximanova_light.otf");
                typeface_semi_bold = Typeface.createFromAsset(context.getResources().getAssets(), "proximanova_regular.otf");
                typeface_light = Typeface.createFromAsset(context.getResources().getAssets(), "proximanova_semibold.otf");
                typeface_bold = Typeface.createFromAsset(context.getResources().getAssets(), "proximanova_bold.TTF");
            }
            return instance;
        }
    }

    public Typeface getRegularTypeFace() {
        return typeface;
    }

    public Typeface getSemiBoldTypeFace() {
        return typeface_semi_bold;
    }

    public Typeface getLightTypeFace() {
        return typeface_light;
    }

    public Typeface getBoldTypeFace() {
        return typeface_bold;
    }
}
