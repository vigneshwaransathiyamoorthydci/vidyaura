package com.school.teacherparent.utils;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.school.teacherparent.R;
import com.school.teacherparent.activity.ParentLeaveApplyActivity;
import com.school.teacherparent.models.ChildListResponseResponse;

import java.util.ArrayList;


public class MyPagerAdapter extends PagerAdapter{

    Context context;
   int count;
    int adapterType;
    ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList=new ArrayList<>();
    public MyPagerAdapter(Context context,  int count, int adapterType,  ArrayList<ChildListResponseResponse.parentsStudList> parentsStudListArrayList) {
        this.context = context;
        this.count = count;
        this.adapterType=adapterType;
        this.parentsStudListArrayList=parentsStudListArrayList;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cover, null);
        try {

            LinearLayout linMain = (LinearLayout) view.findViewById(R.id.linMain);
            ImageView imageCover = (ImageView) view.findViewById(R.id.imageCover);
            TextView studentNameTextview=(TextView)view.findViewById(R.id.unameTextview);
            TextView studentclassTextview=(TextView)view.findViewById(R.id.classnameTextview);
            linMain.setTag(position);

            switch (adapterType)
            {
                case ParentLeaveApplyActivity.ADAPTER_TYPE_TOP:
                    studentNameTextview.setText(parentsStudListArrayList.get(position).getFname());
                    studentclassTextview.setText(parentsStudListArrayList.get(position).getClassName()+" "
                            +parentsStudListArrayList.get(position).getSection());
                    imageCover.setBackgroundResource(R.mipmap.student_image);
                    break;
                case ParentLeaveApplyActivity.ADAPTER_TYPE_BOTTOM:
                    imageCover.setBackgroundResource(R.mipmap.school);
                    studentNameTextview.setText("");
                    studentclassTextview.setText("");
                    break;
            }

//            GlideApp.with(context)
//                    .load(listItems[position])
//                    .into(imageCover);

            container.addView(view);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}