package com.school.teacherparent.utils;

import com.school.teacherparent.R;

import java.util.ArrayList;

/**
 * Created by harini on 9/24/2018.
 */

public class Constants {

    public static String BASE_URL = "http://schoolerp.dci.in/api/";
    public static String USERTYPE = "USERTYPE";
    public static String DOB = "DOB";
    public static String PROFILE_PHOTO = "PHOTO";
    public static int DATALIMIT = 5;
    public static String USERID = "USERID";
    public static String SCHOOLID = "SCHOOLID";
    public static String MON = "MON";
    public static String TUES = "TUES";
    public static String WED = "WED";
    public static String THURS = "THURS";
    public static String FRI = "FRI";
    public static String SAT = "SAT";
    public static String SUN = "SUN";
    public static String SUBID = "SUBID";
    public static String SECID = "SECID";
    public static String CLASSID = "CLASSID";
    public static String FNAME = "FNAME";
    public static String LNAME = "LNAME";
    public static String SALUTATION = "SALUTATION";
    public static String PHONE = "PHONE";
    public static String EMAIL = "EMAIL";
    public static String SECURITYPIN = "SECURITYPIN";
    public static String NOTIFICATIONSTATUS = "notification";
    public static String MESSAGESTATUS = "Message";
    public static String SECURITYPINSTATUS = "security";
    public static String HASCHILDREN = "HASCHILDREN";
    public static final String FCMTOKEN="FcmToken";
    public static final String FCMKEYSHAREDPERFRENCES="FCMKEYSHAREDPERFRENCES";
    public static final String DEVICEID="DeviceID";
    public static final String JOINING_DATE="JOINING_DATE";
    public static final String APPID="APPID";
    public static final String APPVERSION="APPVERSION";
    public static final String OS_VERSION="OS_VERSION";
    public static final String GENDER="GENDER";
    public static final String EMP_ID="EMP_ID";
    public static final String SECURE_TOKEN="SECURE_TOKEN";
    public static final String HEADER_KEY="HEADER_KEY";
    public static final String BEARER="BEARER";
    public static final String FROMADDFEED="FROMADDFEED";
    public static Integer[] imgid = {
            R.mipmap.pink_icon,
            R.mipmap.green_icon,
            R.mipmap.yellow,
            R.mipmap.violet,
            R.mipmap.skyblue,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.green,
            R.mipmap.yellow,
            R.mipmap.violet,
            R.mipmap.skyblue,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.pink_icon,
            R.mipmap.green_icon,
            R.mipmap.yellow,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.pink_icon,
            R.mipmap.green_icon,
            R.mipmap.yellow,
            R.mipmap.violet,
            R.mipmap.skyblue,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.green,
            R.mipmap.yellow,
            R.mipmap.violet,
            R.mipmap.skyblue,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.pink_icon,
            R.mipmap.green_icon,
            R.mipmap.yellow,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.yellow,
            R.mipmap.violet,
            R.mipmap.skyblue,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.pink_icon,
            R.mipmap.green_icon,
            R.mipmap.yellow,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,
            R.mipmap.lightgreen,
            R.mipmap.litegreen,
            R.mipmap.darkblue,
            R.mipmap.grey,
            R.mipmap.orange,





    };
    public static ArrayList<String> SingleType= new ArrayList<>();
    public static final String CLASSIMAGE="CLASS_IMAGE";
    public static final String SCHOOL_LOGO="SCHOOL_LOGO";
    public static final String TOTAL_LEVEL_POINTS="TOTAL_LEVEL_POINTS";
    public static final String POINTS_NAME="POINTS_NAME";
    public static final String BADGE_IMAGE="BADGE_IMAGE";

    public static final String LEGACY_SERVER_KEY = "AIzaSyDq6coDpfdYkhO3snlOBK--KE5lWU2xeMY";

    public static final String SECURE_TOKEN_PAREENTS="SECURE_TOKEN_PAREENTS";
    public static final String SECURE_TOKEN_TEACHER="SECURE_TOKEN_TEACHER";
    public static final String OPEN_DRAWER="OPEN_DRAWER";
    public static final String USERTYPE_REAL="USERTYPE_REAL";
    public static final String TYPE="TYPE";
    public static final String STATE="STATE";
    public static final String ID="ID";
    public static final String NOTIFICATION_STATE="NOTIFICATION_STATE";

}
